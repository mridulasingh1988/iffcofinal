-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 01, 2017 at 07:33 AM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos_intermediate_10`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `emptyIntermdb` ()  BEGIN
UPDATE  `intrm$usr` SET POS_USR_ID='',CREATE_FLG='NULL' WHERE 1;
UPDATE  `intrm$curr` SET POS_CURR_ID='' WHERE 1;
UPDATE `intrm$eo` SET POS_EO_ID='' WHERE 1;
UPDATE `intrm$eo$add` SET POS_EO_ID='' WHERE 1;
UPDATE `intrm$eo$add$org` SET POS_EO_ID='' WHERE 1;
UPDATE `intrm$eo$org` SET POS_EO_ID='' WHERE 1;
UPDATE `intrm$itm$grp` SET POS_GRP_ID='' WHERE 1;
UPDATE `intrm$itm$grp$org` SET POS_GRP_ID='' WHERE 1;
UPDATE `intrm$org` SET POS_ORG_ID='' WHERE 1;
UPDATE `intrm$prod$org` SET POS_ITM_ID='' WHERE 1;
TRUNCATE TABLE `intrm$sls$inv`;
TRUNCATE TABLE `intrm$sls$inv$itm`;
TRUNCATE TABLE `intrm$sls$rma`;
TRUNCATE TABLE `intrm$sls$rma$itm`;
TRUNCATE TABLE `intrm$payment$dtl`;
TRUNCATE TABLE `intrm$mm$mtl$rcpt`;
TRUNCATE TABLE `intrm$mm$mtl$rcpt$src`;
TRUNCATE TABLE `intrm$mm$mtl$rcpt$itm`;
TRUNCATE TABLE `intrm$mm$mtl$rcpt$lot`;
TRUNCATE TABLE `intrm$mm$mtl$rcpt$bin`;
UPDATE `intrm$bin` SET POS_BIN_ID='' WHERE 1;
UPDATE `intrm$drft$po` SET POS_PO_ID='' WHERE 1;
UPDATE `intrm$drft$po$dlv$schdl` SET POS_PO_SCHDL_ID='' WHERE 1;
UPDATE `intrm$ds$att` SET POS_ATT_ID='' WHERE 1;
UPDATE `intrm$ds$att$reln` SET POS_ATT_ID_RELN='' WHERE 1;
UPDATE `intrm$ds$att$type` SET POS_ATT_TYPE_ID='' WHERE 1;
UPDATE `intrm$mm$emrs` SET POS_MM_EMRS_ID='' WHERE 1;
UPDATE `intrm$mm$emrs$doc$src` SET POS_MM_EMRS_DOC_SRC_ID='' WHERE 1;
UPDATE `intrm$mm$emrs$itm` SET POS_MM_EMRS_ITM_ID='' WHERE 1;
UPDATE `intrm$mm$estk$summ$bin` SET POS_BIN_ID='' WHERE 1;
UPDATE `intrm$mm$estk$summ$itm` SET POS_STK_ITM_ID='' WHERE 1;
UPDATE `intrm$mm$estk$summ$lot` SET POS_STK_LOT_ID='' WHERE 1;
UPDATE `intrm$mm$stk$summ$bin` SET POS_BIN_ID='' WHERE 1;
UPDATE `intrm$mm$stk$summ$itm` SET POS_STK_ITM_ID='' WHERE 1;
UPDATE `intrm$mm$stk$summ$lot` SET POS_STK_LOT_ID='' WHERE 1;
UPDATE `intrm$segment` SET POS_PRJ_ID='' WHERE 1;
UPDATE `intrm$uom$cls` SET POS_UOM_CLASS_ID='' WHERE 1;
UPDATE `intrm$uom$conv$std` SET POS_UOM_ID='' WHERE 1;
UPDATE `intrm$wh$org` SET POS_WH_ID='' WHERE 1;
UPDATE `intrm$app$wh$org$ext` SET POS_APP_WH_ORG_ID='' WHERE 1;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `emp`
--

CREATE TABLE `emp` (
  `NAME` varchar(20) DEFAULT NULL,
  `ROLL_NO` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp`
--

INSERT INTO `emp` (`NAME`, `ROLL_NO`) VALUES
('DINESH', '1'),
('SURESH', '2');

-- --------------------------------------------------------

--
-- Table structure for table `intrm$app$wh$org$ext`
--

CREATE TABLE `intrm$app$wh$org$ext` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `ORG_ID` varchar(20) DEFAULT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `POS_APP_WH_ORG_ID` bigint(21) DEFAULT NULL,
  `USR_ID_CREATE` int(4) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(4) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL,
  `WH_NM` varchar(50) DEFAULT NULL,
  `WH_ONRSHP_TYPE` int(5) DEFAULT NULL,
  `WH_STRG_TYPE` int(5) DEFAULT NULL,
  `ADDS_ID` varchar(20) DEFAULT NULL,
  `WH_DESC` varchar(1000) DEFAULT NULL,
  `ACTV` varchar(1) DEFAULT NULL,
  `INACTV_RESN` varchar(200) DEFAULT NULL,
  `INACTV_DT` date DEFAULT NULL,
  `WH_ENT_ID` int(20) DEFAULT NULL,
  `HO_ORG_ID` varchar(2) DEFAULT NULL,
  `PRJ_ID` varchar(40) DEFAULT NULL,
  `UPD_FLG` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$app$wh$org$ext`
--

INSERT INTO `intrm$app$wh$org$ext` (`CLD_ID`, `SLOC_ID`, `ORG_ID`, `WH_ID`, `POS_APP_WH_ORG_ID`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `WH_NM`, `WH_ONRSHP_TYPE`, `WH_STRG_TYPE`, `ADDS_ID`, `WH_DESC`, `ACTV`, `INACTV_RESN`, `INACTV_DT`, `WH_ENT_ID`, `HO_ORG_ID`, `PRJ_ID`, `UPD_FLG`) VALUES
('0000', 1, '04', 'WH00002', 0, 1, '2016-11-08', NULL, NULL, 'GOV IFFCO STORAGE ', 958, 210, 'ADDS0000000001', NULL, 'Y', NULL, NULL, NULL, '01', '0000.01.04.0001.00XR.00.1UKxc44uJN', NULL),
('0000', 1, '04', 'WH00003', 0, 1, '2016-11-08', NULL, NULL, 'GOV CONSIGN SALE', 229, 210, 'ADDS0000000001', NULL, 'Y', NULL, NULL, NULL, '01', '0000.01.04.0001.00XR.00.1UKxc44uJN', NULL),
('0000', 1, '04', 'EWH00004', 0, 1, '2016-12-21', NULL, NULL, 'TAT-EXT ', 958, 210, 'ADDS0000000023', NULL, 'Y', NULL, NULL, NULL, '01', '0000.01.04.0001.00XR.00.1UKxc4ge73', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$bin`
--

CREATE TABLE `intrm$bin` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `ORG_ID` varchar(20) DEFAULT NULL,
  `POS_BIN_ID` bigint(20) DEFAULT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `BIN_ID` varchar(20) DEFAULT NULL,
  `BIN_NM` varchar(50) DEFAULT NULL,
  `BIN_DESC` varchar(200) DEFAULT NULL,
  `STORAGE_TYPE` int(20) DEFAULT NULL,
  `BLOCKED` varchar(1) DEFAULT NULL,
  `BLK_RESN` varchar(200) DEFAULT NULL,
  `BLK_DT_FRM` date DEFAULT NULL,
  `BLK_DT_TO` date DEFAULT NULL,
  `BIN_ENT_ID` int(20) DEFAULT NULL,
  `USR_ID_CREATE` int(4) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(4) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL,
  `UPD_FLG` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$bin`
--

INSERT INTO `intrm$bin` (`CLD_ID`, `SLOC_ID`, `ORG_ID`, `POS_BIN_ID`, `WH_ID`, `BIN_ID`, `BIN_NM`, `BIN_DESC`, `STORAGE_TYPE`, `BLOCKED`, `BLK_RESN`, `BLK_DT_FRM`, `BLK_DT_TO`, `BIN_ENT_ID`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `UPD_FLG`) VALUES
('0000', 1, '04', 2, 'WH00004', 'BIN00005', 'SHOWROO11M', 'SHOWROOM', 125, 'N', NULL, NULL, NULL, 1, 1, '2016-11-08', NULL, '2016-11-08', NULL),
('0000', 1, '04', 3, 'WH00001', 'BIN00001', 'SHOWROOM11', 'SHOWROOM', 125, 'N', NULL, NULL, NULL, 1, 1, '2016-11-08', NULL, '2016-11-08', NULL),
('0000', 1, '04', 1, 'WH00005', 'BIN00006', 'STORAGE', 'Storage', 125, 'N', NULL, NULL, NULL, 1, 1, '2017-01-19', NULL, '2017-01-19', NULL),
('0000', 1, '04', 6, 'WH00003', 'BIN00003', 'STORAGE', 'STORAGE', 125, 'N', NULL, NULL, NULL, 1, 1, '2016-11-08', NULL, '2016-11-08', NULL),
('0000', 1, '04', 5, 'WH00002', 'BIN00004', 'STORAGE', 'STORAGE', 125, 'N', NULL, NULL, NULL, 1, 1, '2016-11-08', NULL, '2016-11-08', NULL),
('0000', 1, '04', 3, 'WH00001', 'BIN00002', 'STORAGE11', 'STORAGE', 125, 'N', NULL, NULL, NULL, 1, 1, '2016-11-08', NULL, '2016-11-08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$cntry`
--

CREATE TABLE `intrm$cntry` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `CNTRY_ID` int(20) DEFAULT NULL,
  `POS_CNTRY_ID` int(5) DEFAULT NULL,
  `CNTRY_DESC` varchar(50) NOT NULL,
  `CURR_ID` varchar(5) NOT NULL,
  `CREATE_FLG` varchar(1) DEFAULT NULL,
  `UPD_FLG` varchar(1) DEFAULT NULL,
  `DEL_FLG` varchar(1) DEFAULT NULL,
  `USR_ID_CREATE` int(10) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(10) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$cntry`
--

INSERT INTO `intrm$cntry` (`CLD_ID`, `SLOC_ID`, `CNTRY_ID`, `POS_CNTRY_ID`, `CNTRY_DESC`, `CURR_ID`, `CREATE_FLG`, `UPD_FLG`, `DEL_FLG`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`) VALUES
('0000', 1, 106, NULL, 'INDIA', '73', NULL, NULL, NULL, 1, '2013-11-21', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$curr`
--

CREATE TABLE `intrm$curr` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `CURR_ID` int(20) DEFAULT NULL,
  `POS_CURR_ID` int(20) DEFAULT NULL,
  `CURR_NM` varchar(50) NOT NULL,
  `CURR_NOTATION` varchar(40) NOT NULL,
  `CREATE_FLG` varchar(1) DEFAULT NULL,
  `UPD_FLG` varchar(1) DEFAULT NULL,
  `DEL_FLG` varchar(1) DEFAULT NULL,
  `USR_ID_CREATE` int(10) NOT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(10) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$curr`
--

INSERT INTO `intrm$curr` (`CLD_ID`, `SLOC_ID`, `CURR_ID`, `POS_CURR_ID`, `CURR_NM`, `CURR_NOTATION`, `CREATE_FLG`, `UPD_FLG`, `DEL_FLG`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`) VALUES
('0000', 1, 73, 1, 'INDIAN RUPEE', 'INR', '1', NULL, NULL, 1, '2011-01-01', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$curr$conv`
--

CREATE TABLE `intrm$curr$conv` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `CURR_ID` varchar(20) DEFAULT NULL,
  `POS_CURR_ID` int(20) DEFAULT NULL,
  `CC_EFF_DATE` date DEFAULT NULL,
  `CC_EFF_TO_DATE` date DEFAULT NULL,
  `POS_CURR_ID_TXN` int(20) DEFAULT NULL,
  `CURR_ID_TXN` int(5) DEFAULT NULL,
  `CC_BUY` int(26) DEFAULT NULL,
  `CC_SELL` int(26) DEFAULT NULL,
  `CREATE_FLG` varchar(1) DEFAULT NULL,
  `UPD_FLG` varchar(1) DEFAULT NULL,
  `DEL_FLG` varchar(1) DEFAULT NULL,
  `USR_ID_CREATE` int(10) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(10) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$curr$conv`
--

INSERT INTO `intrm$curr$conv` (`CLD_ID`, `SLOC_ID`, `CURR_ID`, `POS_CURR_ID`, `CC_EFF_DATE`, `CC_EFF_TO_DATE`, `POS_CURR_ID_TXN`, `CURR_ID_TXN`, `CC_BUY`, `CC_SELL`, `CREATE_FLG`, `UPD_FLG`, `DEL_FLG`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`) VALUES
('0000', 1, '73', NULL, '2016-04-01', '2016-04-01', NULL, 73, 1, 1, NULL, NULL, NULL, 1, '2016-10-27', 1, '2016-10-27'),
('0000', 1, '73', NULL, '0016-04-01', '0016-04-01', NULL, 73, 1, 1, NULL, NULL, NULL, 1, '2016-10-27', 1, '2016-10-27'),
('0000', 1, '73', NULL, '2015-04-01', '2015-04-01', NULL, 73, 1, 1, NULL, NULL, NULL, 1, '2016-10-28', 1, '2016-10-28'),
('0000', 1, '73', NULL, '2015-04-01', '2015-04-01', NULL, 73, 1, 1, NULL, NULL, NULL, 1, '2016-10-28', 1, '2016-10-28'),
('0000', 1, '73', NULL, '2015-04-01', '2015-04-01', NULL, 73, 1, 1, NULL, NULL, NULL, 1, '2016-10-27', 1, '2016-10-27');

-- --------------------------------------------------------

--
-- Table structure for table `intrm$drft$po`
--

CREATE TABLE `intrm$drft$po` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `ORG_ID` varchar(20) DEFAULT NULL,
  `POS_PO_ID` bigint(20) NOT NULL,
  `DOC_ID` varchar(40) DEFAULT NULL,
  `PO_ID` varchar(20) DEFAULT NULL,
  `PO_DT` date DEFAULT NULL,
  `EO_ID` bigint(20) DEFAULT NULL,
  `BILL_ADDS_ID` varchar(20) DEFAULT NULL,
  `TLRNC_DAYS` int(1) DEFAULT NULL,
  `CURR_ID_SP` int(200) DEFAULT NULL,
  `CURR_CONV_FCTR` decimal(26,6) DEFAULT NULL,
  `TLRNC_QTY_TYPE` varchar(1) DEFAULT NULL,
  `TLRNC_QTY_VAL` decimal(26,6) DEFAULT NULL,
  `USR_ID_CREATE` decimal(4,0) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` decimal(4,0) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL,
  `AUTH_PO_NO` varchar(20) DEFAULT NULL,
  `FY_ID` decimal(5,0) DEFAULT NULL,
  `PRJ_ID` varchar(40) DEFAULT NULL,
  `PO_STATUS` int(5) DEFAULT NULL,
  `PO_MODE` int(5) DEFAULT NULL,
  `UPD_FLG` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$drft$po`
--

INSERT INTO `intrm$drft$po` (`CLD_ID`, `SLOC_ID`, `ORG_ID`, `POS_PO_ID`, `DOC_ID`, `PO_ID`, `PO_DT`, `EO_ID`, `BILL_ADDS_ID`, `TLRNC_DAYS`, `CURR_ID_SP`, `CURR_CONV_FCTR`, `TLRNC_QTY_TYPE`, `TLRNC_QTY_VAL`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `AUTH_PO_NO`, `FY_ID`, `PRJ_ID`, `PO_STATUS`, `PO_MODE`, `UPD_FLG`) VALUES
('0000', 1, '04', 1, '0000.01.04.0001.04oS.00.1UNWIBA7YT', 'DPO00012', '2017-01-21', 3, 'ADDS0000000003', 0, 73, '1.000000', 'P', '0.000000', '1', '2017-01-21', NULL, NULL, 'PO00000007', '1', '0000.01.04.0001.00XR.00.1UKxc44uJN', 217, 231, '1'),
('0000', 1, '04', 2, '0000.01.04.0001.04oS.00.1UNWPsHU2Z', 'DPO00016', '2017-01-28', 3, 'ADDS0000000003', 0, 73, '1.000000', 'P', '0.000000', '1', '2017-01-28', NULL, NULL, 'PO00000011', '1', '0000.01.04.0001.00XR.00.1UKxc44uJN', 217, 231, '1'),
('0000', 1, '04', 3, '0000.01.04.0001.04oS.00.1UNWPszKWo', 'DPO00018', '2017-01-28', 3, 'ADDS0000000003', 0, 73, '1.000000', 'P', '0.000000', '1', '2017-01-28', NULL, NULL, 'PO00000013', '1', '0000.01.04.0001.00XR.00.1UKxc4ge73', 217, 231, '1'),
('0000', 1, '04', 4, '0000.01.04.0001.04oS.00.1UNWPszKUX', 'DPO00017', '2017-01-28', 3, 'ADDS0000000003', 0, 73, '1.000000', 'P', '0.000000', '1', '2017-01-28', NULL, NULL, 'PO00000012', '1', '0000.01.04.0001.00XR.00.1UKxc44uJN', 217, 231, '1'),
('0000', 1, '04', 5, '0000.01.04.0001.04oS.00.1UKzcbLcux', 'DPO00009', '2016-12-22', 3, 'ADDS0000000003', 0, 73, '1.000000', 'P', '0.000000', '1', '2016-12-22', NULL, NULL, 'PO00000004', '1', '0000.01.04.0001.00XR.00.1UKxc44uJN', 217, 231, '1'),
('0000', 1, '04', 6, '0000.01.04.0001.04oS.00.1UKzcbj9UC', 'DPO00010', '2016-12-22', 3, 'ADDS0000000003', 0, 73, '1.000000', 'P', '0.000000', '1', '2016-12-22', NULL, NULL, 'PO00000005', '1', '0000.01.04.0001.00XR.00.1UKxc44uJN', 217, 231, '1'),
('0000', 1, '04', 11, '0000.01.04.0001.04oS.00.1UNWPrRWZd', 'DPO00014', '2017-01-28', 10, 'ADDS0000000003', 0, 73, '1.000000', 'P', '0.000000', '1', '2017-01-28', NULL, NULL, 'PO00000009', '1', '0000.01.04.0001.00XR.00.1UKxc44uJN', 217, 231, '1'),
('0000', 1, '04', 7, '0000.01.04.0001.04oS.00.1UNWPrTGrX', 'DPO00015', '2017-01-28', 3, 'ADDS0000000003', 0, 73, '1.000000', 'P', '0.000000', '1', '2017-01-28', NULL, NULL, 'PO00000010', '1', '0000.01.04.0001.00XR.00.1UKxc4ge73', 218, 231, '1'),
('0000', 1, '04', 8, '0000.01.04.0001.04oS.00.1UKzcbj9X6', 'DPO00011', '2016-12-22', 3, 'ADDS0000000003', 0, 73, '1.000000', 'P', '0.000000', '1', '2016-12-22', NULL, NULL, 'PO00000006', '1', '0000.01.04.0001.00XR.00.1UKxc4ge73', 217, 231, '1'),
('0000', 1, '04', 12, '0000.01.04.0001.04oS.00.1UKzbV2hTw', 'DPO00007', '2016-12-21', 10, 'ADDS0000000003', 0, 73, '1.000000', 'P', '0.000000', '1', '2016-12-21', NULL, NULL, 'PO00000002', '1', '0000.01.04.0001.00XR.00.1UKxc44uJN', 217, 231, '1'),
('0000', 1, '04', 13, '0000.01.04.0001.04oS.00.1UKzaNmk5W', 'DPO00006', '2016-12-20', 10, 'ADDS0000000003', 0, 73, '1.000000', 'P', '0.000000', '1', '2016-12-20', NULL, NULL, 'PO00000001', '1', '0000.01.04.0001.00XR.00.1UKxc44uJN', 217, 231, '1'),
('0000', 1, '04', 14, '0000.01.04.0001.04oS.00.1UKzaNhHEz', 'DPO00005', '2016-12-20', 10, 'ADDS0000000003', 0, 73, '1.000000', 'P', '0.000000', '1', '2016-12-20', NULL, NULL, 'DPO0000001', '1', '0000.01.04.0001.00XR.00.1UKxc44uJN', 217, 231, '1'),
('0000', 1, '04', 15, '0000.01.04.0001.04oS.00.1UNWPtmdZE', 'DPO00019', '2017-01-28', 10, 'ADDS0000000003', 0, 73, '1.000000', 'P', '0.000000', '1', '2017-01-28', NULL, NULL, 'PO00000014', '1', '0000.01.04.0001.00XR.00.1UKxc4ge73', 217, 231, '1'),
('0000', 1, '04', 9, '0000.01.04.0001.04oS.00.1UKzcXqKQ9', 'DPO00008', '2016-12-22', 3, 'ADDS0000000003', 10, 73, '1.000000', 'P', '0.000000', '1', '2016-12-22', '1', '2016-12-22', 'PO00000003', '1', '0000.01.04.0001.00XR.00.1UKxc44uJN', 217, 231, '1'),
('0000', 1, '04', 10, '0000.01.04.0001.04oS.00.1UNWIBA7gH', 'DPO00013', '2017-01-21', 3, 'ADDS0000000003', 0, 73, '1.000000', 'P', '0.000000', '1', '2017-01-21', NULL, NULL, 'PO00000008', '1', '0000.01.04.0001.00XR.00.1UKxc4ge73', 217, 231, '1'),
('0000', 1, '04', 16, '0000.01.04.0001.04oS.00.1UNWS2HA7U', 'DPO00020', '2017-01-30', 10, 'ADDS0000000003', 0, 73, '1.000000', 'P', '0.000000', '1', '2017-01-30', NULL, NULL, 'PO00000015', '1', '0000.01.04.0001.00XR.00.1UKxc44uJN', 217, 231, '1');

-- --------------------------------------------------------

--
-- Table structure for table `intrm$drft$po$dlv$schdl`
--

CREATE TABLE `intrm$drft$po$dlv$schdl` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `ORG_ID` varchar(20) DEFAULT NULL,
  `POS_PO_SCHDL_ID` bigint(20) DEFAULT NULL,
  `DOC_ID` varchar(40) DEFAULT NULL,
  `ITM_ID` varchar(50) DEFAULT NULL,
  `TOT_QTY` decimal(26,6) DEFAULT NULL,
  `DLV_MODE` bigint(20) DEFAULT NULL,
  `DLV_QTY` decimal(26,6) DEFAULT NULL,
  `TLRNC_QTY_TYPE` varchar(1) DEFAULT NULL,
  `TLRNC_QTY_VAL` decimal(26,6) DEFAULT NULL,
  `DLV_DT` date DEFAULT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `USE_RENT_WH` varchar(1) DEFAULT NULL,
  `DLV_ADDS_ID` varchar(20) DEFAULT NULL,
  `USR_ID_CREATE` int(4) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(4) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL,
  `DLV_SCHDL_NO` int(5) DEFAULT NULL,
  `ITM_UOM` varchar(20) DEFAULT NULL,
  `ITM_UOM_DESC` varchar(200) DEFAULT NULL,
  `BAL_QTY` decimal(26,6) DEFAULT NULL,
  `TMP_RCPT_QTY` decimal(26,6) DEFAULT NULL,
  `TLRNC_DAYS_VAL` int(5) DEFAULT NULL,
  `PRJ_ID` varchar(40) DEFAULT NULL,
  `RO_NO` varchar(100) DEFAULT NULL,
  `RO_DT` date DEFAULT NULL,
  `PO_MODE` int(11) DEFAULT NULL,
  `UPD_FLG` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$drft$po$dlv$schdl`
--

INSERT INTO `intrm$drft$po$dlv$schdl` (`CLD_ID`, `SLOC_ID`, `ORG_ID`, `POS_PO_SCHDL_ID`, `DOC_ID`, `ITM_ID`, `TOT_QTY`, `DLV_MODE`, `DLV_QTY`, `TLRNC_QTY_TYPE`, `TLRNC_QTY_VAL`, `DLV_DT`, `WH_ID`, `USE_RENT_WH`, `DLV_ADDS_ID`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `DLV_SCHDL_NO`, `ITM_UOM`, `ITM_UOM_DESC`, `BAL_QTY`, `TMP_RCPT_QTY`, `TLRNC_DAYS_VAL`, `PRJ_ID`, `RO_NO`, `RO_DT`, `PO_MODE`, `UPD_FLG`) VALUES
('0000', 1, '04', 7, '0000.01.04.0001.04oS.00.1UNWIBA7gH', 'AF.0000002', '1500.000000', 163, '1500.000000', NULL, '0.000000', '2017-01-21', 'WH00005', 'Y', 'ADDS0000000023', 1, '2017-01-21', NULL, NULL, 1, 'UOM0000000063', 'PIECES', '1500.000000', '0.000000', 0, '0000.01.04.0001.00XR.00.1UKxc4ge73', '7687', '2017-01-21', 231, '1'),
('0000', 1, '04', 1, '0000.01.04.0001.04oS.00.1UKzcXqKQ9', 'AF.0000002', '1000.000000', 163, '1000.000000', 'P', '0.000000', '2016-12-22', 'WH00001', NULL, 'ADDS0000000001', 1, '2016-12-22', NULL, '2016-12-22', 1, 'UOM0000000063', 'PIECES', '1000.000000', '0.000000', 10, '0000.01.04.0001.00XR.00.1UKxc44uJN', 'RO98', '2016-12-16', 231, '1'),
('0000', 1, '04', 18, '0000.01.04.0001.04oS.00.1UNWPszKWo', 'AF.0000005', '20.000000', 163, '20.000000', NULL, '0.000000', '2017-01-28', 'WH00005', 'Y', 'ADDS0000000023', 1, '2017-01-28', NULL, NULL, 1, 'UOM0000000063', 'PIECES', '20.000000', '0.000000', 0, '0000.01.04.0001.00XR.00.1UKxc4ge73', '46', '2017-01-28', 231, '1'),
('0000', 1, '04', 15, '0000.01.04.0001.04oS.00.1UNWPrRWZd', 'AP03.0000001', '500.000000', 163, '500.000000', NULL, NULL, '2017-01-28', 'WH00001', NULL, 'ADDS0000000001', 39, '2017-01-28', NULL, NULL, 1, 'UOM0000000068', '250 ML', '500.000000', '0.000000', NULL, 'PROJ0000', '45', '2017-01-28', 231, '1'),
('0000', 1, '04', 13, '0000.01.04.0001.04oS.00.1UNWIBA7gH', 'AP02.0000001', '100.000000', 163, '100.000000', NULL, '0.000000', '2017-01-21', 'WH00005', NULL, 'ADDS0000000023', 1, '2017-01-21', NULL, NULL, 1, 'UOM0000000063', 'PIECES', '80.000000', '0.000000', 0, '0000.01.04.0001.00XR.00.1UKxc4ge73', '344', '2017-01-21', 231, '1'),
('0000', 1, '04', 2, '0000.01.04.0001.04oS.00.1UKzcbLcux', 'AF.0000002', '1000.000000', 163, '1000.000000', NULL, '0.000000', '2016-12-22', 'WH00001', NULL, 'ADDS0000000001', 1, '2016-12-22', NULL, NULL, 1, 'UOM0000000063', 'PIECES', '1000.000000', '0.000000', 0, '0000.01.04.0001.00XR.00.1UKxc44uJN', 'RO00', '2016-12-19', 231, '1'),
('0000', 1, '04', 16, '0000.01.04.0001.04oS.00.1UKzaNmk5W', 'AP03.0000001', '1500.000000', 163, '100.000000', NULL, NULL, '2016-12-21', 'WH00001', NULL, 'ADDS0000000001', 39, '2016-12-20', NULL, NULL, 1, 'UOM0000000068', '250 ML', '100.000000', '0.000000', NULL, 'PROJ0000', '1215', '2016-12-21', 231, '1'),
('0000', 1, '04', 17, '0000.01.04.0001.04oS.00.1UNWPszKUX', 'AF.0000005', '50.000000', 163, '50.000000', NULL, '0.000000', '2017-01-28', 'WH00001', NULL, 'ADDS0000000001', 1, '2017-01-28', NULL, NULL, 1, 'UOM0000000063', 'PIECES', '50.000000', '0.000000', 0, '0000.01.04.0001.00XR.00.1UKxc44uJN', '324', '2017-01-28', 231, '1'),
('0000', 1, '04', 10, '0000.01.04.0001.04oS.00.1UNWIBA7gH', 'AP01.0000001', '2000.000000', 163, '2000.000000', NULL, '0.000000', '2017-01-21', 'WH00005', NULL, 'ADDS0000000023', 1, '2017-01-21', NULL, NULL, 1, 'UOM0000000049', 'Liter', '1925.000000', '0.000000', 0, '0000.01.04.0001.00XR.00.1UKxc4ge73', '344', '2017-01-21', 231, '1'),
('0000', 1, '04', 8, '0000.01.04.0001.04oS.00.1UNWPrTGrX', 'AF.0000002', '1500.000000', 163, '1500.000000', NULL, '0.000000', '2017-01-28', 'WH00005', 'Y', 'ADDS0000000023', 1, '2017-01-28', NULL, NULL, 1, 'UOM0000000063', 'PIECES', '1500.000000', '0.000000', 0, '0000.01.04.0001.00XR.00.1UKxc4ge73', 'RO34', '2017-01-28', 231, '1'),
('0000', 1, '04', 3, '0000.01.04.0001.04oS.00.1UKzcbLcux', 'AF.0000003', '2000.000000', 163, '2000.000000', NULL, '0.000000', '2016-12-22', 'WH00001', NULL, 'ADDS0000000001', 1, '2016-12-22', NULL, NULL, 1, 'UOM0000000063', 'PIECES', '2000.000000', '0.000000', 0, '0000.01.04.0001.00XR.00.1UKxc44uJN', 'RO100', '2016-12-19', 231, '1'),
('0000', 1, '04', 9, '0000.01.04.0001.04oS.00.1UKzaNmk5W', 'AP01.0000001', '1000.000000', 163, '1000.000000', NULL, NULL, '2016-12-21', 'WH00001', 'Y', 'ADDS0000000001', 39, '2016-12-20', NULL, NULL, 1, 'UOM0000000049', 'Liter', '1000.000000', '0.000000', NULL, 'PROJ0000', '1313', '2016-12-20', 231, '1'),
('0000', 1, '04', 4, '0000.01.04.0001.04oS.00.1UKzbV2hTw', 'AF.0000003', '8.000000', 163, '8.000000', NULL, NULL, '2016-12-22', 'WH00001', NULL, 'ADDS0000000001', 39, '2016-12-21', NULL, NULL, 1, 'UOM0000000063', 'PIECES', '8.000000', '0.000000', NULL, 'PROJ0000', 'RO96', '2016-12-22', 231, '1'),
('0000', 1, '04', 5, '0000.01.04.0001.04oS.00.1UKzcbj9X6', 'AF.0000004', '1000.000000', 163, '50.000000', NULL, NULL, '2016-12-23', 'WH00001', NULL, 'ADDS0000000001', 39, '2016-12-22', NULL, NULL, 1, 'UOM0000000063', 'PIECES', '50.000000', '0.000000', NULL, 'PROJ0000', '57677', '2016-12-22', 231, '1'),
('0000', 1, '04', 6, '0000.01.04.0001.04oS.00.1UKzcbj9UC', 'AF.0000004', '800.000000', 163, '100.000000', NULL, NULL, '2016-12-23', 'WH00001', NULL, 'ADDS0000000001', 39, '2016-12-22', NULL, NULL, 1, 'UOM0000000063', 'PIECES', '100.000000', '0.000000', NULL, 'PROJ0000', 'RO324', '2016-12-22', 231, '1'),
('0000', 1, '04', 12, '0000.01.04.0001.04oS.00.1UNWIBA7YT', 'AP02.0000001', '1500.000000', 163, '1500.000000', NULL, '0.000000', '2017-01-21', 'WH00001', 'Y', 'ADDS0000000001', 1, '2017-01-21', NULL, NULL, 1, 'UOM0000000063', 'PIECES', '1500.000000', '0.000000', 0, '0000.01.04.0001.00XR.00.1UKxc44uJN', '476', '2017-01-21', 231, '1'),
('0000', 1, '04', 11, '0000.01.04.0001.04oS.00.1UNWPrTGrX', 'AP01.0000001', '2000.000000', 163, '2000.000000', NULL, '0.000000', '2017-01-28', 'WH00005', NULL, 'ADDS0000000023', 1, '2017-01-28', NULL, NULL, 1, 'UOM0000000049', 'Liter', '1900.000000', '0.000000', 0, '0000.01.04.0001.00XR.00.1UKxc4ge73', 'RO12', '2017-01-28', 231, '1'),
('0000', 1, '04', 14, '0000.01.04.0001.04oS.00.1UNWPrTGrX', 'AP02.0000001', '100.000000', 163, '100.000000', NULL, '0.000000', '2017-01-28', 'WH00005', NULL, 'ADDS0000000023', 1, '2017-01-28', NULL, NULL, 1, 'UOM0000000063', 'PIECES', '100.000000', '0.000000', 0, '0000.01.04.0001.00XR.00.1UKxc4ge73', 'R12', '2017-01-28', 231, '1');

-- --------------------------------------------------------

--
-- Table structure for table `intrm$ds$att`
--

CREATE TABLE `intrm$ds$att` (
  `ATT_ID` int(5) DEFAULT NULL,
  `POS_ATT_ID` int(11) DEFAULT NULL,
  `ATT_NM` varchar(100) DEFAULT NULL,
  `ATT_TYPE_ID` int(5) DEFAULT NULL,
  `ATT_ACTV` varchar(1) DEFAULT NULL,
  `SYNC_FLG` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$ds$att`
--

INSERT INTO `intrm$ds$att` (`ATT_ID`, `POS_ATT_ID`, `ATT_NM`, `ATT_TYPE_ID`, `ATT_ACTV`, `SYNC_FLG`) VALUES
(942, 1, 'Forwarded', 164, 'Y', '1'),
(136, 2, 'RADIATION', 29, 'Y', '1'),
(572, 3, 'RM', 118, 'Y', '1'),
(86, 4, 'SUPPLIER', 21, 'Y', '1'),
(891, 5, 'Supplier-Purchase Return', 72, 'Y', '1'),
(41, 6, 'ROOT NODE', 11, 'Y', '1'),
(900, 7, 'RECURRING ACCOUNT', 167, 'Y', '1'),
(172, 8, 'Scheduled Purchase Order', 40, 'Y', '1'),
(311, 9, 'RATE CONTRACT', 68, 'Y', '1'),
(397, 10, 'DRAFT', 64, 'Y', '1'),
(810, 11, 'CnI', 145, 'Y', '1'),
(221, 12, 'STANDARD PRICE', 52, 'Y', '1'),
(424, 13, 'PENDING WITH STORES', 94, 'Y', '1'),
(855, 14, 'Transportation and logistics Expense', 31, 'Y', '1'),
(828, 15, 'Adhoc Subcontracting Order', 151, 'Y', '1'),
(916, 16, 'Material Requisition Slip', 171, 'Y', '1'),
(345, 17, 'BY RTGS', 37, 'Y', '1'),
(365, 18, 'Item', 81, 'Y', '1'),
(651, 19, 'Top Suppliers', 121, 'Y', '1'),
(871, 20, 'Approved', 163, 'Y', '1'),
(256, 21, 'SUPPLIER', 56, 'Y', '1'),
(33, 22, 'QUERY NO EDIT (VIEW)', 10, 'Y', '1'),
(971, 23, 'Forwarded', 183, 'Y', '1'),
(488, 24, 'Price', 108, 'N', '1'),
(468, 25, 'Total Sales', 104, 'Y', '1'),
(993, 26, 'DIRECT ORDER (DISPATCH)', 68, 'Y', '1'),
(854, 27, 'Job Execution', 157, 'Y', '1'),
(983, 28, 'High Sea Sales', 186, 'Y', '1'),
(1057, 29, 'Previous Quotation', 204, 'Y', '1'),
(602, 30, 'Material Requisitoins', 118, 'Y', '1'),
(129, 31, 'LENGTH', 29, 'Y', '1'),
(880, 32, 'Cancelled', 65, 'Y', '1'),
(228, 33, 'INCOMPLETE', 53, 'Y', '1'),
(463, 34, 'SUGGESTED ORDER', 103, 'Y', '1'),
(948, 35, 'QC', 179, 'Y', '1'),
(528, 36, 'All Product Group', 116, 'Y', '1'),
(1056, 37, 'Manual', 204, 'Y', '1'),
(484, 38, 'Manual pricing', 106, 'Y', '1'),
(809, 39, 'CnF', 145, 'Y', '1'),
(923, 40, 'STOCK TRANSFER INVOICE', 76, 'Y', '1'),
(188, 41, 'CURRENT LIABILITIES', 45, 'Y', '1'),
(116, 42, 'IMPORT SUB-CONTRACTOR', 26, 'Y', '1'),
(724, 43, 'Distributer', 74, 'Y', '1'),
(933, 44, 'Sales Rejection Reason', 176, 'Y', '1'),
(63, 45, 'XLS', 16, 'Y', '1'),
(1059, 46, 'Month to Date', 115, 'Y', '1'),
(688, 47, 'Top Warehouse', 123, 'Y', '1'),
(298, 48, 'TRANSFERRED TO STORE', 65, 'Y', '1'),
(187, 49, 'CURRENT ASSET', 45, 'Y', '1'),
(1030, 50, 'Swift Transfer', 37, 'Y', '1'),
(544, 51, 'Top Products', 111, 'Y', '1'),
(401, 52, 'RMA WITH CREDIT ONLY', 90, 'Y', '1'),
(43, 53, 'CHILD NODE - MODULE', 11, 'N', '1'),
(159, 54, 'DIRECT', 36, 'Y', '1'),
(392, 55, 'LANDED PRICE', 88, 'Y', '1'),
(788, 56, 'Event Based', 140, 'Y', '1'),
(658, 57, 'Below Re-order Level', 121, 'Y', '1'),
(790, 58, 'Manual', 140, 'Y', '1'),
(886, 59, 'Export Sales', 31, 'Y', '1'),
(1032, 60, 'Packets', 196, 'Y', '1'),
(845, 61, 'Form C', 155, 'Y', '1'),
(203, 62, 'FORWARDED', 33, 'Y', '1'),
(524, 63, 'Product Ageing', 111, 'Y', '1'),
(1033, 64, 'SHORT CLOSED', 85, 'Y', '1'),
(536, 65, 'One Region', 116, 'Y', '1'),
(581, 66, 'Top Product Group', 118, 'Y', '1'),
(570, 67, 'Rejected', 118, 'Y', '1'),
(1007, 68, 'Created', 191, 'Y', '1'),
(309, 69, 'RUSH ORDER', 68, 'Y', '1'),
(843, 70, 'Subcontracting - Inwards', 154, 'Y', '1'),
(425, 71, 'CLOSED', 94, 'Y', '1'),
(687, 72, 'Top Organization', 123, 'Y', '1'),
(863, 73, 'Posted to finance', 159, 'Y', '1'),
(631, 74, 'Top Warehouse', 120, 'Y', '1'),
(717, 75, 'BY CREDIT CARD', 37, 'Y', '1'),
(1020, 76, 'Realized Amount', 194, 'Y', '1'),
(482, 77, 'Weighted average', 106, 'Y', '1'),
(840, 78, 'Route card', 151, 'Y', '1'),
(776, 79, 'Stock Swap (Issue)', 72, 'Y', '1'),
(821, 80, 'Forwarded', 149, 'Y', '1'),
(181, 81, 'Manual', 34, 'Y', '1'),
(914, 82, 'Source', 170, 'Y', '1'),
(514, 83, 'Order Analysis', 111, 'Y', '1'),
(799, 84, 'Information', 143, 'Y', '1'),
(281, 85, 'JOB EXECUTION', 59, 'Y', '1'),
(1061, 86, 'DRAFT', 205, 'Y', '1'),
(206, 87, 'JOHANSON C.P.', 48, 'Y', '1'),
(431, 88, 'CLOSED', 95, 'Y', '1'),
(229, 89, 'PRIVATE', 54, 'Y', '1'),
(164, 90, 'SUGGESTED ORDER', 39, 'Y', '1'),
(342, 91, 'CANCELLED', 63, 'Y', '1'),
(984, 92, 'Direct Dispatch', 186, 'N', '1'),
(795, 93, 'DIRECT INVOICE (SERVICES)', 76, 'Y', '1'),
(390, 94, 'QUOTATION', 87, 'Y', '1'),
(245, 95, 'APP_USER_LIMIT', 47, 'Y', '1'),
(201, 96, 'OPERATING EXPENSE', 45, 'Y', '1'),
(500, 97, 'Consignment Sales Analysis', 109, 'Y', '1'),
(1050, 98, 'INCOMPLETE', 201, 'Y', '1'),
(689, 99, 'Top Product', 123, 'Y', '1'),
(947, 100, 'Expired', 178, 'Y', '1'),
(832, 101, 'Sales Item', 152, 'Y', '1'),
(217, 102, 'OPEN', 51, 'Y', '1'),
(927, 103, 'TRANSFERRED TO STORE', 173, 'Y', '1'),
(655, 104, 'Receipt vs Rejection', 121, 'Y', '1'),
(935, 105, 'Sales Inactive Reason', 176, 'Y', '1'),
(909, 106, 'Stock in Hand (FG)', 169, 'Y', '1'),
(177, 107, 'DISCOUNT', 42, 'N', '1'),
(198, 108, 'EQUITY', 45, 'Y', '1'),
(656, 109, 'Receipt Pattern', 121, 'Y', '1'),
(149, 110, 'THROUGH WEBPAGE', 32, 'Y', '1'),
(542, 111, 'KIT WORKSHOP', 73, 'Y', '1'),
(960, 112, 'VALUE BASED', 181, 'Y', '1'),
(356, 113, 'STOCK ADJUSTED', 79, 'Y', '1'),
(1096, 114, 'MATERIAL ISSUE', 211, 'Y', '1'),
(350, 115, 'STOCK TAKE', 77, 'Y', '1'),
(834, 116, 'Pending Stock Release', 51, 'Y', '1'),
(48, 117, 'ASSOCIATED LAYER AND BELOW', 12, 'Y', '1'),
(797, 118, 'Critical', 143, 'Y', '1'),
(520, 119, 'Opportunity Analysis', 113, 'Y', '1'),
(113, 120, 'DOMESTIC SERVICE CONTRACTOR', 26, 'Y', '1'),
(216, 121, 'CANCELLED', 50, 'Y', '1'),
(831, 122, 'Output Item', 152, 'Y', '1'),
(130, 123, 'TEMPERATURE', 29, 'Y', '1'),
(47, 124, 'ASSOCIATED LAYER', 12, 'Y', '1'),
(258, 125, 'SERVICE CONTRACTOR', 56, 'N', '1'),
(998, 126, 'DIRECT INVOICE (HIGH SEA SALES)', 76, 'Y', '1'),
(908, 127, 'Positive Round Off', 168, 'Y', '1'),
(555, 128, 'Top Organizations', 112, 'Y', '1'),
(462, 129, 'PURCHASE REQUISITION', 103, 'Y', '1'),
(686, 130, 'Purchase Requisitions', 122, 'Y', '1'),
(416, 131, 'OTHER PURCHASE INVOICE', 92, 'N', '1'),
(906, 132, 'Natural Round Off', 168, 'Y', '1'),
(711, 133, 'Duplicate', 127, 'Y', '1'),
(525, 134, 'All Oraganization', 116, 'Y', '1'),
(582, 135, 'Top Suppliers', 118, 'Y', '1'),
(97, 136, 'SET UPS/PROFILES', 22, 'Y', '1'),
(534, 137, 'One Product Group', 116, 'Y', '1'),
(1044, 138, 'APPROVED', 200, 'Y', '1'),
(162, 139, 'BY RAIL', 38, 'Y', '1'),
(932, 140, 'SUPPLIER', 175, 'Y', '1'),
(133, 141, 'ENERGY', 29, 'Y', '1'),
(986, 142, 'Quantity', 187, 'Y', '1'),
(321, 143, 'ORDER GENERATED', 70, 'Y', '1'),
(913, 144, 'Document Source', 170, 'Y', '1'),
(573, 145, 'FG', 118, 'Y', '1'),
(1031, 146, 'Box', 196, 'Y', '1'),
(1081, 147, 'External Material - OUT', 209, 'Y', '1'),
(772, 148, 'Swap Bin', 137, 'Y', '1'),
(988, 149, 'A-Display', 188, 'Y', '1'),
(197, 150, 'DEBT', 45, 'Y', '1'),
(968, 151, 'IMPORT PURCHASE', 184, 'Y', '1'),
(683, 152, 'Issue Pattern', 122, 'Y', '1'),
(25, 153, 'DESCRIPTION', 8, 'Y', '1'),
(553, 154, 'Top Sales Executive', 112, 'Y', '1'),
(317, 155, 'FORWARDED', 69, 'Y', '1'),
(875, 156, 'Approved', 164, 'Y', '1'),
(234, 157, 'APPROVED', 55, 'Y', '1'),
(994, 158, 'DIRECT ORDER(HIGH SEA SALES)', 68, 'Y', '1'),
(446, 159, 'LOST', 99, 'Y', '1'),
(598, 160, 'Transferred stock (IN)', 118, 'Y', '1'),
(949, 161, 'MANUFACTURING', 179, 'Y', '1'),
(413, 162, 'INTREST/EXPENSES AND TAX PAID', 45, 'Y', '1'),
(669, 163, 'Finished Goods', 122, 'Y', '1'),
(589, 164, 'Receipt Pattern', 118, 'Y', '1'),
(624, 165, 'RM', 120, 'Y', '1'),
(586, 166, 'Rejections', 118, 'Y', '1'),
(1043, 167, 'DRAFT', 199, 'Y', '1'),
(623, 168, 'Purchase Requisitions', 119, 'Y', '1'),
(322, 169, 'CANCELLED', 70, 'Y', '1'),
(126, 170, 'ALMIRAH', 28, 'Y', '1'),
(741, 171, 'Import Purchase Order', 40, 'Y', '1'),
(910, 172, 'WIP Stock', 169, 'Y', '1'),
(612, 173, 'Top Organization', 119, 'Y', '1'),
(60, 174, 'PDF', 16, 'Y', '1'),
(677, 175, 'Top Product Group', 122, 'Y', '1'),
(123, 176, 'BOX', 28, 'Y', '1'),
(235, 177, 'APPROVED', 50, 'Y', '1'),
(860, 178, 'Other Charges', 158, 'Y', '1'),
(921, 179, 'Intra-Branch Purchase', 31, 'Y', '1'),
(95, 180, 'REFERRED CODE', 8, 'Y', '1'),
(23, 181, '3', 7, 'Y', '1'),
(464, 182, 'Gross Profit', 104, 'Y', '1'),
(803, 183, 'FORWARDED', 63, 'Y', '1'),
(68, 184, 'LEGACY CODE', 18, 'Y', '1'),
(1051, 185, 'DRAFT', 201, 'Y', '1'),
(1068, 186, 'All Supplier', 206, 'Y', '1'),
(638, 187, 'Top PO', 120, 'Y', '1'),
(749, 188, 'Stock Adjustment - Reduce', 72, 'Y', '1'),
(670, 189, 'WIP', 122, 'Y', '1'),
(486, 190, 'Weighted average of Entity', 107, 'Y', '1'),
(1088, 191, 'Material Receipt Date', 210, 'Y', '1'),
(78, 192, 'RADIO GROUP - NON MANDATORY', 20, 'Y', '1'),
(841, 193, 'Own Stock', 154, 'Y', '1'),
(22, 194, '2', 7, 'Y', '1'),
(941, 195, 'Cancelled', 164, 'Y', '1'),
(567, 196, 'Top Organizations', 114, 'Y', '1'),
(889, 197, 'TRANSFER ORDER', 62, 'Y', '1'),
(787, 198, 'Create Log (not to be displayed)', 139, 'Y', '1'),
(822, 199, 'Approved', 149, 'Y', '1'),
(701, 200, 'Visit', 124, 'Y', '1'),
(601, 201, 'Top PO', 118, 'Y', '1'),
(766, 202, 'CLOSE', 136, 'Y', '1'),
(455, 203, 'Purchase Invoice', 100, 'Y', '1'),
(578, 204, 'Top Organization', 118, 'Y', '1'),
(552, 205, 'Top Customer', 112, 'Y', '1'),
(1066, 206, 'On sight', 37, 'Y', '1'),
(332, 207, 'CUSTOMER', 73, 'Y', '1'),
(360, 208, 'BI-MONTHLY', 80, 'Y', '1'),
(1053, 209, 'PROJECT MANAGEMENT', 62, 'Y', '1'),
(313, 210, 'INCOMPLETE', 69, 'Y', '1'),
(680, 211, 'Issues', 122, 'Y', '1'),
(328, 212, 'PACK LIST', 72, 'Y', '1'),
(645, 213, 'Services', 121, 'Y', '1'),
(437, 214, 'CLOSED', 97, 'Y', '1'),
(647, 215, 'Top Organization', 121, 'Y', '1'),
(743, 216, 'Insurance', 43, 'Y', '1'),
(364, 217, 'Warehouse', 81, 'Y', '1'),
(597, 218, 'In Transit Stock', 118, 'Y', '1'),
(1079, 219, 'APPROVED', 208, 'Y', '1'),
(241, 220, 'APP_LISENCE_TILL', 47, 'Y', '1'),
(888, 221, 'Through MRN', 165, 'Y', '1'),
(811, 222, 'Draft', 146, 'Y', '1'),
(314, 223, 'APPROVED', 69, 'Y', '1'),
(1049, 224, 'FORWARDED', 201, 'Y', '1'),
(702, 225, 'Telephone', 124, 'Y', '1'),
(138, 226, 'FORCE', 29, 'Y', '1'),
(532, 227, 'One Customer', 116, 'Y', '1'),
(738, 228, 'Transfer Invoice (on Receipt)', 100, 'N', '1'),
(671, 229, 'Trading', 122, 'Y', '1'),
(509, 230, 'Top Customer', 110, 'Y', '1'),
(729, 231, 'Category 5', 74, 'N', '1'),
(868, 232, 'Email', 161, 'Y', '1'),
(715, 233, 'Departments', 128, 'Y', '1'),
(75, 234, 'DISPLAY ITEM - MANDATORY', 20, 'Y', '1'),
(851, 235, 'STOCK UPDATED', 156, 'Y', '1'),
(616, 236, 'Top Suppliers', 119, 'Y', '1'),
(882, 237, 'CANCELLED', 33, 'Y', '1'),
(186, 238, 'RE-VALIDATE', 24, 'Y', '1'),
(92, 239, 'BANK', 21, 'Y', '1'),
(713, 240, 'Quadruplicate', 127, 'Y', '1'),
(523, 241, 'Rejection Analysis', 114, 'Y', '1'),
(922, 242, 'Intra-Branch Sales', 31, 'Y', '1'),
(363, 243, 'AD-HOC', 80, 'Y', '1'),
(808, 244, 'CIF', 145, 'Y', '1'),
(118, 245, 'IMPORT SUPPLIER', 26, 'Y', '1'),
(591, 246, 'Rejection Pattern', 118, 'Y', '1'),
(308, 247, 'DIRECT ORDER', 68, 'Y', '1'),
(746, 248, 'Route Card', 130, 'Y', '1'),
(64, 249, 'PHONE', 17, 'Y', '1'),
(87, 250, 'SUB CONTRACTOR', 21, 'Y', '1'),
(858, 251, 'Subcontracting - Outwards', 57, 'Y', '1'),
(382, 252, 'FORWARDED FOR APPROVAL', 86, 'Y', '1'),
(1077, 253, 'DRAFT', 208, 'Y', '1'),
(71, 254, 'URL', 19, 'Y', '1'),
(757, 255, 'CHARACTER', 133, 'Y', '1'),
(381, 256, 'PENDING FOR REWORK', 86, 'Y', '1'),
(775, 257, 'Stock Swapping (Issue)', 71, 'Y', '1'),
(481, 258, 'OPEN CONTRACT', 68, 'Y', '1'),
(282, 259, 'COMPLETE CHECK', 60, 'Y', '1'),
(199, 260, 'CAPITAL ACCOUNT', 45, 'Y', '1'),
(804, 261, 'Import Purchase Check List', 144, 'Y', '1'),
(289, 262, 'JOB CARD', 62, 'N', '1'),
(912, 263, 'Item', 170, 'Y', '1'),
(320, 264, 'DRAFT', 70, 'Y', '1'),
(279, 265, 'WEEKLY', 23, 'Y', '1'),
(1065, 266, 'availzed', 37, 'Y', '1'),
(561, 267, 'Top Organizations', 113, 'Y', '1'),
(761, 268, 'SUPPLIER', 134, 'Y', '1'),
(104, 269, 'QUATERLY', 23, 'Y', '1'),
(1036, 270, 'TREO', 197, 'Y', '1'),
(628, 271, 'Services', 120, 'Y', '1'),
(429, 272, 'DRAFT', 95, 'Y', '1'),
(1013, 273, 'Sales Order', 171, 'Y', '1'),
(398, 274, 'FORWARDED', 64, 'Y', '1'),
(510, 275, 'Top Sales Executive', 110, 'Y', '1'),
(753, 276, 'Work Order', 130, 'Y', '1'),
(1017, 277, 'Sales Order', 193, 'Y', '1'),
(543, 278, 'Kit Workshop', 72, 'Y', '1'),
(31, 279, 'DETAIL LEVEL WITH LOT', 9, 'Y', '1'),
(1039, 280, 'CLOSE', 198, 'Y', '1'),
(405, 281, 'RMA WITH RECEIPT AND CREDIT ', 90, 'Y', '1'),
(208, 282, 'CLIMATE CONTROLLED WAREHOUSE', 49, 'Y', '1'),
(249, 283, 'MAX_USER_PROFILE_PER_SLOC', 47, 'Y', '1'),
(143, 284, 'QOTATION', 30, 'Y', '1'),
(103, 285, 'BI MONTHLY', 23, 'Y', '1'),
(358, 286, 'HALF YEARLY', 80, 'Y', '1'),
(579, 287, 'Top Warehouse', 118, 'Y', '1'),
(632, 288, 'Top Product', 120, 'Y', '1'),
(119, 289, 'NOIDA', 27, 'Y', '1'),
(959, 290, 'Letter of Credit', 37, 'Y', '1'),
(304, 291, 'FIFO', 66, 'Y', '1'),
(829, 292, 'Job Card', 151, 'Y', '1'),
(231, 293, 'APPROVED', 53, 'Y', '1'),
(939, 294, 'As per Application Profile (Natural)', 168, 'N', '1'),
(577, 295, 'Capital goods', 118, 'Y', '1'),
(940, 296, 'Pending\nFor Transport', 51, 'Y', '1'),
(610, 297, 'Services', 119, 'Y', '1'),
(180, 298, 'OTHER CHARGE', 43, 'Y', '1'),
(404, 299, 'RMA WITH RECEIPT AND NO CREDIT', 90, 'Y', '1'),
(652, 300, 'Top Consumers', 121, 'Y', '1'),
(6, 301, 'CHAR', 2, 'Y', '1'),
(1070, 302, 'To Deliver', 207, 'Y', '1'),
(566, 303, 'Top Regions', 114, 'Y', '1'),
(152, 304, 'SENT', 33, 'Y', '1'),
(268, 305, 'PROCESS ORDER', 57, 'Y', '1'),
(460, 306, 'PENDING', 46, 'Y', '1'),
(996, 307, 'PRODUCTION TRANSFER', 57, 'Y', '1'),
(495, 308, 'Top Customer', 109, 'Y', '1'),
(727, 309, 'Category 3', 74, 'N', '1'),
(489, 310, 'Value', 108, 'Y', '1'),
(77, 311, 'RADIO GROUP - MANDATORY', 20, 'Y', '1'),
(732, 312, 'Category 8', 74, 'N', '1'),
(621, 313, 'Top PO', 119, 'Y', '1'),
(288, 314, 'PRODUCTION ADVICE', 62, 'N', '1'),
(21, 315, '3', 6, 'Y', '1'),
(415, 316, 'SERVICE PO INVOICE', 92, 'Y', '1'),
(10, 317, 'dd-MM-yyyy', 3, 'Y', '1'),
(79, 318, 'LIST BOX - MANDATORY', 20, 'Y', '1'),
(842, 319, 'Subcontracting -  Outwards', 154, 'Y', '1'),
(869, 320, 'All Products', 162, 'Y', '1'),
(389, 321, 'PREVIOUS ORDER', 87, 'Y', '1'),
(633, 322, 'Top Product Group', 120, 'Y', '1'),
(526, 323, 'All Customer', 116, 'Y', '1'),
(529, 324, 'All Sales Executive', 116, 'Y', '1'),
(156, 325, 'Quotation', 34, 'Y', '1'),
(459, 326, 'CASH PURCHASE ORDER', 57, 'Y', '1'),
(355, 327, 'DATA UPDATED', 79, 'Y', '1'),
(120, 328, 'DELHI', 27, 'Y', '1'),
(1009, 329, 'Approved', 191, 'Y', '1'),
(338, 330, 'WAREHOUSE - OTHER ORGANIZATION', 73, 'Y', '1'),
(38, 331, 'CLEAR', 10, 'Y', '1'),
(517, 332, 'Target Analysis', 111, 'Y', '1'),
(667, 333, 'Outstanding Returnables', 121, 'Y', '1'),
(135, 334, 'PRESSURE/STRESS', 29, 'Y', '1'),
(267, 335, 'FINISHED GOODS RECEIPT', 57, 'Y', '1'),
(967, 336, 'WIRE TRANSFER', 25, 'Y', '1'),
(1074, 337, 'Item Group', 81, 'Y', '1'),
(84, 338, 'ROW - EVEN', 20, 'Y', '1'),
(619, 339, 'Below Safety Level', 119, 'Y', '1'),
(663, 340, 'Top PO', 121, 'Y', '1'),
(700, 341, 'Pending - Short Close/Cancel', 51, 'Y', '1'),
(604, 342, 'Purchase Returns', 118, 'Y', '1'),
(937, 343, 'MRP', 177, 'Y', '1'),
(773, 344, 'Stock Swapping (Receipt)', 73, 'Y', '1'),
(423, 345, 'DRAFT', 94, 'Y', '1'),
(441, 346, 'DRAFT', 98, 'Y', '1'),
(70, 347, 'NAME FOR CHEQUE PRINTING', 18, 'Y', '1'),
(433, 348, 'PURCHASE RETURN', 96, 'Y', '1'),
(49, 349, 'MASTER DOCUMENT', 13, 'Y', '1'),
(710, 350, 'Original', 127, 'Y', '1'),
(1006, 351, 'Security Deduction', 190, 'Y', '1'),
(609, 352, 'Trading', 119, 'Y', '1'),
(370, 353, 'RECEIVE WITHOUT PO', 57, 'Y', '1'),
(547, 354, 'Top Sales Executive', 111, 'Y', '1'),
(955, 355, 'SITE WAREHOUSE', 49, 'Y', '1'),
(98, 356, 'DOCUMENTS', 22, 'Y', '1'),
(794, 357, 'Use', 142, 'Y', '1'),
(789, 358, 'Time Based', 140, 'Y', '1'),
(359, 359, 'QUARTERLY', 80, 'Y', '1'),
(756, 360, 'DATE', 133, 'Y', '1'),
(318, 361, 'CONSIGNMENT ORDER', 68, 'Y', '1'),
(748, 362, 'Stock Adjustment - Add', 73, 'Y', '1'),
(985, 363, 'First Expiry First Out', 67, 'Y', '1'),
(1045, 364, 'FORWARDED', 200, 'Y', '1'),
(348, 365, 'DIRECT INVOICE', 76, 'Y', '1'),
(892, 366, 'CASH SALES ORDER (SERVICES)', 68, 'Y', '1'),
(825, 367, 'Gate Pass Return', 57, 'Y', '1'),
(449, 368, 'Stock', 31, 'Y', '1'),
(980, 369, 'Maintenance Job Card', 130, 'Y', '1'),
(805, 370, 'Domestic Purchase Check List', 144, 'Y', '1'),
(331, 371, 'SUPPLIER', 73, 'Y', '1'),
(457, 372, 'Cash Purchase Invoice', 100, 'Y', '1'),
(1015, 373, 'Direct Secondary Sales', 192, 'Y', '1'),
(1001, 374, 'Business Portfolio', 189, 'Y', '1'),
(692, 375, 'Credit Ageing', 123, 'Y', '1'),
(759, 376, 'BOOLEAN', 133, 'Y', '1'),
(325, 377, 'STOCK TRANSFER', 71, 'Y', '1'),
(662, 378, 'Quotations', 121, 'Y', '1'),
(588, 379, 'Receipt vs Rejection', 118, 'Y', '1'),
(926, 380, 'APPROVED', 173, 'Y', '1'),
(278, 381, 'CONTINUOUS', 23, 'Y', '1'),
(91, 382, 'TRANSPORTER', 21, 'Y', '1'),
(807, 383, 'FOB', 145, 'Y', '1'),
(189, 384, 'STOCK IN HAND', 45, 'Y', '1'),
(110, 385, 'RE-ISSUED', 24, 'Y', '1'),
(716, 386, 'Supplementary Transfer Invoice (Issue)', 100, 'N', '1'),
(122, 387, 'KOLKATA', 27, 'Y', '1'),
(1018, 388, 'Pick Order', 193, 'Y', '1'),
(402, 389, 'RMA WITH REPAIR', 90, 'N', '1'),
(992, 390, 'Sales Order', 34, 'Y', '1'),
(751, 391, 'Forwarded', 131, 'Y', '1'),
(362, 392, 'FORTNIGHTLY', 80, 'Y', '1'),
(1069, 393, 'Item Specific', 206, 'Y', '1'),
(388, 394, 'TEMPLATE', 87, 'Y', '1'),
(584, 395, 'Receipts', 118, 'Y', '1'),
(1078, 396, 'FORWARDED', 208, 'Y', '1'),
(377, 397, 'CLOSED', 85, 'Y', '1'),
(848, 398, 'OPEN', 156, 'Y', '1'),
(961, 399, 'MANDATORY', 181, 'Y', '1'),
(111, 400, 'CHEQUE', 25, 'Y', '1'),
(548, 401, 'Top Regions', 111, 'Y', '1'),
(1040, 402, 'APPROVED', 199, 'Y', '1'),
(847, 403, 'Form G', 155, 'Y', '1'),
(257, 404, 'CUSTOMER', 56, 'Y', '1'),
(499, 405, 'Sales Analysis', 109, 'Y', '1'),
(815, 406, 'Forwarded', 147, 'Y', '1'),
(74, 407, 'TEXT ITEM - NON MANDATORY', 20, 'Y', '1'),
(965, 408, 'APPORVED', 182, 'Y', '1'),
(399, 409, 'MATERIAL RETURN NOTE', 57, 'Y', '1'),
(977, 410, 'Offloading Charges', 43, 'N', '1'),
(1022, 411, 'PDC', 194, 'Y', '1'),
(121, 412, 'MUMBAI', 27, 'Y', '1'),
(154, 413, 'Previous Purchase Order', 34, 'Y', '1'),
(352, 414, 'PENDING', 78, 'Y', '1'),
(626, 415, 'WIP', 120, 'Y', '1'),
(403, 416, 'RMA WITH REPLACEMENT', 90, 'Y', '1'),
(383, 417, 'STOCK UPDATED', 86, 'Y', '1'),
(150, 418, 'BY MAIL', 32, 'Y', '1'),
(600, 419, 'Quotations', 118, 'Y', '1'),
(302, 420, 'WEIGHTED AVERAGE PRICE (WAP)', 66, 'Y', '1'),
(944, 421, 'Draft', 178, 'Y', '1'),
(205, 422, 'CASH OUT FLOW', 45, 'Y', '1'),
(873, 423, 'Forwarded', 163, 'Y', '1'),
(1000, 424, 'PENDING AT QC', 51, 'Y', '1'),
(735, 425, 'DIRECT DEBIT', 25, 'Y', '1'),
(412, 426, 'GEN AND ADMIN EXPENSES', 45, 'Y', '1'),
(1052, 427, 'Landing Cost', 106, 'Y', '1'),
(337, 428, 'WAREHOUSE WITHIN ORGANIZATION', 73, 'Y', '1'),
(719, 429, 'Item', 129, 'Y', '1'),
(346, 430, 'RECEIVED AT STORE', 75, 'Y', '1'),
(472, 431, 'Organization', 105, 'Y', '1'),
(270, 432, 'TRANSFER ORDER', 57, 'Y', '1'),
(1071, 433, 'To Collect', 207, 'Y', '1'),
(1076, 434, 'CANCELLED', 173, 'Y', '1'),
(1054, 435, 'FORM C-56', 203, 'Y', '1'),
(57, 436, 'TIE OPTION FORWARD CONTRACT', 15, 'Y', '1'),
(1064, 437, 'Mpesa', 37, 'Y', '1'),
(139, 438, 'VOLUME', 29, 'Y', '1'),
(739, 439, 'Purchase Invoice - Consumables', 100, 'Y', '1'),
(240, 440, 'APPROVED', 33, 'Y', '1'),
(422, 441, 'QC COMPLETE', 86, 'Y', '1'),
(497, 442, 'Top Regions', 109, 'Y', '1'),
(901, 443, 'FIXED DEPOSITS', 167, 'Y', '1'),
(878, 444, 'Closed', 164, 'Y', '1'),
(1011, 445, 'Reverted', 191, 'Y', '1'),
(37, 446, 'SAVE/COMMIT', 10, 'Y', '1'),
(85, 447, 'ROW - ODD', 20, 'Y', '1'),
(737, 448, 'Purchase Invoice - Services', 100, 'Y', '1'),
(496, 449, 'Top Sales Executive', 109, 'Y', '1'),
(108, 450, 'BOUNCED', 24, 'Y', '1'),
(194, 451, 'SUCCESS', 46, 'Y', '1'),
(193, 452, 'CREATED', 46, 'Y', '1'),
(771, 453, 'Swap Item', 137, 'Y', '1'),
(1073, 454, 'Airtel Money', 37, 'Y', '1'),
(505, 455, 'Custom', 115, 'Y', '1'),
(678, 456, 'Top Suppliers', 122, 'Y', '1'),
(767, 457, 'CANCELLED', 136, 'Y', '1'),
(587, 458, 'Receipt vs Issue', 118, 'Y', '1'),
(190, 459, 'EXTERNAL EQUITIES', 45, 'Y', '1'),
(502, 460, 'Quarter', 115, 'Y', '1'),
(745, 461, 'Job Card', 130, 'Y', '1'),
(856, 462, 'Transportation and logistics Income', 31, 'Y', '1'),
(629, 463, 'Capital goods', 120, 'Y', '1'),
(576, 464, 'Services', 118, 'Y', '1'),
(859, 465, 'Tax', 158, 'Y', '1'),
(883, 466, 'EXPORT SALES INVOICE', 76, 'Y', '1'),
(936, 467, 'Item Base Price', 177, 'Y', '1'),
(945, 468, 'Forwarded', 178, 'Y', '1'),
(54, 469, 'BRANCH OFFICE (TRADING HOUSE)', 14, 'Y', '1'),
(956, 470, 'YARD WAREHOUSE', 49, 'Y', '1'),
(209, 471, 'REFRIGERATED WAREHOUSE', 49, 'Y', '1'),
(736, 472, 'DEPOSIT THROUGH RECEIPT', 25, 'Y', '1'),
(1067, 473, '/LC/DA', 37, 'Y', '1'),
(605, 474, 'Outstanding Returnables', 118, 'Y', '1'),
(991, 475, 'RMA', 83, 'Y', '1'),
(1055, 476, 'FORM C-60', 203, 'Y', '1'),
(391, 477, 'MANUAL', 87, 'Y', '1'),
(839, 478, 'Subcontracting Order', 153, 'Y', '1'),
(24, 479, 'AUTO GEN. CODE', 8, 'Y', '1'),
(511, 480, 'Top Regions', 110, 'Y', '1'),
(483, 481, 'Current days price', 106, 'Y', '1'),
(5, 482, 'ALPHANUMERIC', 2, 'Y', '1'),
(8, 483, 'DATE', 2, 'Y', '1'),
(978, 484, 'Cancelled', 156, 'Y', '1'),
(862, 485, 'Forwarded', 159, 'Y', '1'),
(752, 486, 'Approved', 131, 'Y', '1'),
(928, 487, 'DIRECT', 174, 'Y', '1'),
(768, 488, 'TERMINATED', 136, 'Y', '1'),
(585, 489, 'Issues', 118, 'Y', '1'),
(545, 490, 'Top Product Group', 111, 'Y', '1'),
(690, 491, 'Top Product Group', 123, 'Y', '1'),
(148, 492, 'BY POST', 32, 'Y', '1'),
(1046, 493, 'INCOMPLETE', 200, 'Y', '1'),
(386, 494, 'EXPORT ORDER', 68, 'Y', '1'),
(114, 495, 'DOMESTIC SUB-CONTRACTOR', 26, 'Y', '1'),
(29, 496, 'DETAIL LEVEL 3', 9, 'Y', '1'),
(963, 497, 'CLOSED', 147, 'Y', '1'),
(492, 498, 'Supplementary Invoice', 100, 'Y', '1'),
(501, 499, 'Month', 115, 'Y', '1'),
(982, 500, 'Deduction Scheme', 185, 'Y', '1'),
(698, 501, 'Sales Discount', 31, 'Y', '1'),
(1063, 502, 'APPROVED', 205, 'Y', '1'),
(153, 503, 'CLOSED', 33, 'Y', '1'),
(72, 504, 'MAIL ADDRESS', 19, 'Y', '1'),
(237, 505, 'AMENDED', 53, 'Y', '1'),
(109, 506, 'CLEARED', 24, 'Y', '1'),
(1042, 507, 'INCOMPLETE', 199, 'Y', '1'),
(646, 508, 'Capital goods', 121, 'Y', '1'),
(395, 509, 'AS PER ITEM VALUATION METHOD', 88, 'Y', '1'),
(907, 510, 'Negative Round Off', 168, 'Y', '1'),
(145, 511, 'Sales', 31, 'Y', '1'),
(1024, 512, 'BULK ORDER UPLOAD', 195, 'Y', '1'),
(430, 513, 'PENDING', 95, 'Y', '1'),
(551, 514, 'Top Product Group', 112, 'Y', '1'),
(225, 515, 'LIFO', 52, 'Y', '1'),
(691, 516, 'Top Suppliers', 123, 'Y', '1'),
(1047, 517, 'DRAFT', 200, 'Y', '1'),
(7, 518, 'NUMERIC', 2, 'Y', '1'),
(453, 519, 'CLOSED', 65, 'Y', '1'),
(171, 520, 'Direct Purchase Order', 40, 'Y', '1'),
(522, 521, 'Top Sales Executive', 113, 'Y', '1'),
(262, 522, 'WAREHOUSE WITHIN ORGANIZATION', 56, 'Y', '1'),
(635, 523, 'Top Consumers', 120, 'Y', '1'),
(254, 524, 'MAX_GRAPH_ROLES', 47, 'Y', '1'),
(779, 525, 'Employee', 138, 'Y', '1'),
(1075, 526, 'Item Group Including Child Groups', 81, 'Y', '1'),
(535, 527, 'One Sales Executive', 116, 'Y', '1'),
(306, 528, 'FIFO', 67, 'Y', '1'),
(450, 529, 'Stock Adjustment', 31, 'Y', '1'),
(1008, 530, 'Forwarded', 191, 'Y', '1'),
(251, 531, 'MAX_ADMIN_USR', 47, 'Y', '1'),
(184, 532, 'CANCELLED', 24, 'Y', '1'),
(518, 533, 'Credit Control', 109, 'Y', '1'),
(28, 534, 'DETAIL LEVEL 2', 9, 'Y', '1'),
(1023, 535, 'MAINTENANCE JOB CARD', 59, 'Y', '1'),
(755, 536, 'Non Consignee', 132, 'Y', '1'),
(527, 537, 'All Product', 116, 'Y', '1'),
(929, 538, 'RATE CONTRACT', 174, 'Y', '1'),
(90, 539, 'AGENT', 21, 'Y', '1'),
(530, 540, 'All Region', 116, 'Y', '1'),
(467, 541, 'Sales Order Booked', 104, 'Y', '1'),
(958, 542, 'RENTAL', 54, 'Y', '1'),
(896, 543, 'Sales Invoice', 166, 'Y', '1'),
(695, 544, 'SAMPLE ORDER', 68, 'Y', '1'),
(191, 545, 'INTERNAL EQUITIES', 45, 'Y', '1'),
(32, 546, 'ADD', 10, 'Y', '1'),
(504, 547, 'Year', 115, 'Y', '1'),
(560, 548, 'Top Regions', 113, 'Y', '1'),
(1038, 549, 'OPEN', 198, 'Y', '1'),
(972, 550, 'Approved', 183, 'Y', '1'),
(950, 551, 'MAINTENANCE', 179, 'Y', '1'),
(371, 552, 'REQUIREMENT AREA', 83, 'Y', '1'),
(925, 553, 'FORWARDED', 173, 'Y', '1'),
(685, 554, 'Material Requisitoins', 122, 'Y', '1'),
(934, 555, 'Sales Cancellation Reason', 176, 'Y', '1'),
(396, 556, 'OPENING BALANCE', 73, 'Y', '1'),
(137, 557, 'ELECTROMAGNETISM', 29, 'Y', '1'),
(220, 558, 'CANCELLED', 51, 'Y', '1'),
(367, 559, 'PENDING', 82, 'Y', '1'),
(1025, 560, 'Form E', 155, 'Y', '1'),
(170, 561, 'Standard Purchase Order', 40, 'Y', '1'),
(82, 562, 'CHECK BOX - NON MANDATORY', 20, 'Y', '1'),
(244, 563, 'APP_LISENCE_ALERT_DATE', 47, 'Y', '1'),
(242, 564, 'APP_INSTALL_END_DATE', 47, 'Y', '1'),
(714, 565, 'Employee Groups', 128, 'Y', '1'),
(1080, 566, 'External Material - IN', 209, 'Y', '1'),
(549, 567, 'Top Organizations', 111, 'Y', '1'),
(590, 568, 'Issue Pattern', 118, 'Y', '1'),
(568, 569, 'Available', 118, 'Y', '1'),
(731, 570, 'Category 7', 74, 'N', '1'),
(248, 571, 'MAX_USER_PROFILE ', 47, 'Y', '1'),
(474, 572, 'Product', 105, 'Y', '1'),
(176, 573, 'CASH PURCHASE ORDER', 41, 'Y', '1'),
(973, 574, 'CHARACTER BASED', 58, 'Y', '1'),
(280, 575, 'RECEIPT', 59, 'Y', '1'),
(1003, 576, 'Direct Purchase Order (High Sea Sales)', 40, 'Y', '1'),
(333, 577, 'SUB CONTRACTOR', 73, 'Y', '1'),
(770, 578, 'JOB EXECUTION', 57, 'Y', '1'),
(475, 579, 'Sales Executive', 105, 'Y', '1'),
(222, 580, 'MOVING AVERAGE PRICE', 52, 'Y', '1'),
(837, 581, 'MRS - External Entity', 72, 'Y', '1'),
(349, 582, 'CONSIGNMENT INVOICE', 76, 'Y', '1'),
(911, 583, 'Form F', 155, 'Y', '1'),
(1019, 584, 'Shipment', 193, 'N', '1'),
(59, 585, 'LIMIT ORDER', 15, 'Y', '1'),
(409, 586, 'FIXED ASSETS', 45, 'Y', '1'),
(672, 587, 'Services', 122, 'Y', '1'),
(920, 588, 'Purchase Return', 71, 'Y', '1'),
(292, 589, 'PROCUREMENT PROCESS INITIATED', 63, 'Y', '1'),
(218, 590, 'CLOSE', 51, 'Y', '1'),
(14, 591, 'UPPER', 4, 'Y', '1'),
(1029, 592, 'Reworkable Stock Requisition', 72, 'Y', '1'),
(643, 593, 'WIP', 121, 'Y', '1'),
(117, 594, 'DOMESTIC SUPPLIER', 26, 'Y', '1'),
(434, 595, 'RMDA', 96, 'Y', '1'),
(224, 596, 'LOT PRICE', 52, 'Y', '1'),
(630, 597, 'Top Organization', 120, 'Y', '1'),
(707, 598, 'CLOSED', 125, 'Y', '1'),
(833, 599, 'Purchase Item', 152, 'Y', '1'),
(393, 600, 'MOVING AVERAGE PRICE', 88, 'N', '1'),
(166, 601, 'RE-ORDER LEVEL', 39, 'Y', '1'),
(339, 602, 'DEALER', 74, 'Y', '1'),
(666, 603, 'Purchase Returns', 121, 'Y', '1'),
(435, 604, 'PENDING FOR DISPATCH', 97, 'Y', '1'),
(155, 605, 'Suggested Order', 34, 'Y', '1'),
(102, 606, 'MONTHLY', 23, 'Y', '1'),
(895, 607, 'Sales Shipment', 166, 'Y', '1'),
(693, 608, 'Purchase Returns', 123, 'Y', '1'),
(1027, 609, 'Scrap Stock', 169, 'Y', '1'),
(490, 610, 'Open Purchase Agreement', 34, 'Y', '1'),
(742, 611, 'Freight', 43, 'Y', '1'),
(58, 612, 'FORWARD CURRENCY CONTRACT', 15, 'Y', '1'),
(319, 613, 'APPROVED', 70, 'Y', '1'),
(705, 614, 'DRAFT', 125, 'Y', '1'),
(312, 615, 'BACK ORDER', 68, 'Y', '1'),
(253, 616, 'MAX_TICKER_ROLES', 47, 'Y', '1'),
(826, 617, 'Inward Subcontracting', 150, 'Y', '1'),
(443, 618, 'CLOSED', 98, 'Y', '1'),
(215, 619, 'ORDER GENERATED', 50, 'Y', '1'),
(36, 620, 'DELETE', 10, 'Y', '1'),
(722, 621, 'Sales Executive', 129, 'Y', '1'),
(777, 622, 'Gate Pass Return', 73, 'Y', '1'),
(1028, 623, 'Consumables  Stock', 169, 'Y', '1'),
(709, 624, 'Material Receipt', 126, 'Y', '1'),
(260, 625, 'CASH PURCHASE', 56, 'Y', '1'),
(899, 626, 'SAVINGS ACCOUNT', 167, 'Y', '1'),
(512, 627, 'Top Organizations', 110, 'Y', '1'),
(515, 628, 'Shipment Analysis', 111, 'Y', '1'),
(1095, 629, 'MATERIAL RECEIPT', 211, 'Y', '1'),
(286, 630, 'REJECTED', 61, 'Y', '1'),
(69, 631, 'NAME FOR OUTGOING DOCUMENTS', 18, 'Y', '1'),
(989, 632, 'B-Storage', 188, 'Y', '1'),
(361, 633, 'MONTHLY', 80, 'Y', '1'),
(703, 634, 'Internet', 124, 'Y', '1'),
(13, 635, 'yyyy/MM/dd', 3, 'Y', '1'),
(857, 636, 'Fuel expense', 31, 'Y', '1'),
(516, 637, 'Delivery Schedule Analysis', 111, 'Y', '1'),
(124, 638, 'RACK', 28, 'Y', '1'),
(1037, 639, 'EGPSP', 197, 'Y', '1'),
(898, 640, 'CURRENT ACCOUNT', 167, 'Y', '1'),
(34, 641, 'QUERY WITH EDIT', 10, 'Y', '1'),
(407, 642, 'REJECTED', 91, 'Y', '1'),
(867, 643, 'Telephonic', 161, 'Y', '1'),
(236, 644, 'APP_INSTALL_DATE', 47, 'Y', '1'),
(478, 645, 'Currency', 105, 'Y', '1'),
(15, 646, 'INITCAP', 4, 'Y', '1'),
(366, 647, 'TRANSFER ORDER - OTHER ORGANIZATION', 72, 'Y', '1'),
(300, 648, 'STANDARD PRICE', 66, 'Y', '1'),
(673, 649, 'Capital goods', 122, 'Y', '1'),
(664, 650, 'Material Requisitoins', 121, 'Y', '1'),
(4, 651, 'DATETIME', 1, 'Y', '1'),
(780, 652, 'JOB CARD', 59, 'Y', '1'),
(161, 653, 'BY CASH', 37, 'Y', '1'),
(679, 654, 'Top Consumers', 122, 'Y', '1'),
(814, 655, 'Draft', 147, 'Y', '1'),
(953, 656, 'Forwarded', 180, 'Y', '1'),
(324, 657, 'EXTERNAL ISSUE', 71, 'Y', '1'),
(378, 658, 'CANCELLED', 85, 'Y', '1'),
(56, 659, 'SPOT CONTRACT', 15, 'Y', '1'),
(347, 660, 'RECEIVED AFTER QC', 75, 'Y', '1'),
(838, 661, 'External Entity', 96, 'Y', '1'),
(599, 662, 'Transferred stock (Out)', 118, 'Y', '1'),
(19, 663, '4', 5, 'Y', '1'),
(733, 664, 'Category 9', 74, 'N', '1'),
(96, 665, 'PROFILES', 22, 'Y', '1'),
(448, 666, 'Provisional Payable', 31, 'Y', '1'),
(1041, 667, 'FORWARDED', 199, 'Y', '1'),
(995, 668, 'SALES ORDER(DIRECT DISPATCH)', 57, 'Y', '1'),
(620, 669, 'Quotations', 119, 'Y', '1'),
(765, 670, 'OPEN', 136, 'Y', '1'),
(798, 671, 'Warning', 143, 'Y', '1'),
(411, 672, 'SELLING EXPENSES', 45, 'Y', '1'),
(498, 673, 'Top Organizations', 109, 'Y', '1'),
(67, 674, 'ALIAS', 18, 'Y', '1'),
(158, 675, 'THROUGH RFQ', 36, 'Y', '1'),
(1026, 676, 'RM Stock', 169, 'Y', '1'),
(650, 677, 'Top Product Group', 121, 'Y', '1'),
(558, 678, 'Top Customer', 113, 'Y', '1'),
(508, 679, 'Top Product Group', 110, 'Y', '1'),
(540, 680, 'Manual', 117, 'Y', '1'),
(196, 681, 'COST OF GOOD SOLD', 45, 'Y', '1'),
(334, 682, 'REQUIREMENT AREA', 73, 'Y', '1'),
(210, 683, 'STORAGE WAREHOUSES', 49, 'Y', '1'),
(380, 684, 'QC PENDING', 86, 'Y', '1'),
(506, 685, 'Customer Outstanding', 109, 'Y', '1'),
(293, 686, 'MATERIALS RECEIVED AT STORES', 63, 'Y', '1'),
(785, 687, 'SMS', 139, 'Y', '1'),
(76, 688, 'DISPLAY ITEM - NON MANDATORY', 20, 'Y', '1'),
(801, 689, 'DIRECT', 62, 'Y', '1'),
(1010, 690, 'Rejected', 191, 'Y', '1'),
(451, 691, 'Depreciation', 31, 'Y', '1'),
(781, 692, 'ROUTE CARD', 59, 'Y', '1'),
(853, 693, 'Subcontracting Receipt', 157, 'Y', '1'),
(329, 694, 'SALES PICK', 72, 'Y', '1'),
(606, 695, 'RM', 119, 'Y', '1'),
(183, 696, 'OFFICIAL', 44, 'Y', '1'),
(246, 697, 'APP_CONC_USER_LIMIT', 47, 'Y', '1'),
(1083, 698, 'Sale - IN/OUT', 209, 'Y', '1'),
(659, 699, 'Below Safety Level', 121, 'Y', '1'),
(442, 700, 'PENDING', 98, 'Y', '1'),
(924, 701, 'DRAFT', 173, 'Y', '1'),
(61, 702, 'HTML', 16, 'Y', '1'),
(101, 703, 'FORTNIGHTLY', 23, 'Y', '1'),
(374, 704, 'TRANSFER TO OTHER ORGANIZATION', 84, 'Y', '1'),
(1034, 705, 'Reworkable Stock Requisition', 130, 'Y', '1'),
(305, 706, 'LIFO', 67, 'Y', '1'),
(697, 707, 'Sales Return', 31, 'Y', '1'),
(957, 708, 'CLIENT OWNED', 54, 'Y', '1'),
(903, 709, 'TRANSFER ORDER', 96, 'Y', '1'),
(219, 710, 'SHORT CLOSE', 51, 'Y', '1'),
(565, 711, 'Top Sales Executive', 114, 'Y', '1'),
(406, 712, 'REWORKABLE', 91, 'Y', '1'),
(796, 713, 'PACK LIST', 96, 'Y', '1'),
(874, 714, 'Cancelled', 163, 'Y', '1'),
(427, 715, 'DRAFT', 85, 'Y', '1'),
(782, 716, 'Cash against document', 37, 'Y', '1'),
(255, 717, 'MAX_REPORT_ROLES', 47, 'Y', '1'),
(195, 718, 'SALES', 45, 'Y', '1'),
(174, 719, 'Template', 34, 'Y', '1'),
(461, 720, 'DIRECT', 103, 'Y', '1'),
(53, 721, 'BRANCH OFFICE (PRODUCTION HOUSE)', 14, 'Y', '1'),
(369, 722, 'CLOSED', 82, 'Y', '1'),
(182, 723, 'PERSONAL', 44, 'Y', '1'),
(918, 724, 'Reserve', 172, 'Y', '1'),
(962, 725, 'CHARACTER BASED', 181, 'Y', '1'),
(852, 726, 'Subcontracting Issue', 157, 'Y', '1'),
(783, 727, 'Dashboard', 139, 'Y', '1'),
(887, 728, 'From Stores', 165, 'Y', '1'),
(179, 729, 'BUNDLE ONE OR MORE DISCOUNTED PRODUCTS', 42, 'Y', '1'),
(827, 730, 'Outward Subcontracting', 150, 'Y', '1'),
(372, 731, 'WAREHOUSE', 83, 'Y', '1'),
(987, 732, 'Amount', 187, 'Y', '1'),
(131, 733, 'MASS', 29, 'Y', '1'),
(169, 734, 'PIECE', 29, 'Y', '1'),
(865, 735, 'Bill of Entry', 160, 'Y', '1'),
(419, 736, 'RETURNABLE DISPATCH', 93, 'Y', '1'),
(452, 737, 'Purchase Discount', 31, 'Y', '1'),
(603, 738, 'Purchase Requisitions', 118, 'Y', '1'),
(952, 739, 'Draft', 180, 'Y', '1'),
(456, 740, 'Transfer Invoice (Issue)', 100, 'Y', '1'),
(394, 741, 'WEIGHTED AVERAGE PRICE', 88, 'N', '1'),
(879, 742, 'Draft', 164, 'Y', '1'),
(107, 743, 'CASH ACCOUNT', 21, 'Y', '1'),
(239, 744, 'NEFT', 25, 'Y', '1'),
(128, 745, 'TIME', 29, 'Y', '1'),
(694, 746, 'Outstanding Returnables', 123, 'Y', '1'),
(250, 747, 'MAX_USER_PER_SLOC', 47, 'Y', '1'),
(55, 748, 'BRANCH OFFICE (DELIVERY/RECEIVE HOUSE)', 14, 'Y', '1'),
(850, 749, 'PENDING STOCK RELEASE', 156, 'Y', '1'),
(607, 750, 'FG', 119, 'Y', '1'),
(744, 751, 'BY SEA', 38, 'Y', '1'),
(140, 752, 'AREA', 29, 'Y', '1'),
(168, 753, 'MANUAL ENTRY', 39, 'Y', '1'),
(1062, 754, 'FORWARDED', 205, 'Y', '1'),
(445, 755, 'CLOSED', 99, 'Y', '1'),
(141, 756, 'SUPPLIER', 30, 'Y', '1'),
(323, 757, 'INTERNAL ISSUE', 71, 'Y', '1'),
(410, 758, 'SALES RETURN', 45, 'Y', '1'),
(562, 759, 'Top Products', 114, 'Y', '1'),
(291, 760, 'PENDING FOR PROCUREMENT', 63, 'Y', '1'),
(214, 761, 'EVALUATED', 50, 'N', '1'),
(80, 762, 'LIST BOX - NON MANDATORY', 20, 'Y', '1'),
(465, 763, 'Opportunity Closed', 104, 'Y', '1'),
(247, 764, 'APP_ORG_LIMIT', 47, 'Y', '1'),
(39, 765, 'CANCEL', 10, 'Y', '1'),
(173, 766, 'Rate Contract', 40, 'Y', '1'),
(454, 767, 'CASH SALE INVOICE', 76, 'Y', '1'),
(813, 768, 'Approved', 146, 'Y', '1'),
(66, 769, 'FAX', 17, 'Y', '1'),
(275, 770, 'BOOLEAN', 58, 'Y', '1'),
(595, 771, 'Product Ageing', 118, 'Y', '1'),
(336, 772, 'SERVICE CONTRACTOR', 73, 'N', '1'),
(1087, 773, 'DEPOSIT INVOICE', 76, 'Y', '1'),
(726, 774, 'Category 2', 74, 'N', '1'),
(202, 775, 'Scrap', 31, 'Y', '1'),
(800, 776, 'Rental Sales Agreement', 72, 'Y', '1'),
(835, 777, 'EMAIL', 124, 'Y', '1'),
(762, 778, 'BOTH', 134, 'Y', '1'),
(946, 779, 'Effective', 178, 'Y', '1'),
(18, 780, '2', 5, 'Y', '1'),
(830, 781, 'Input Item', 152, 'Y', '1'),
(675, 782, 'Top Warehouse', 122, 'Y', '1'),
(917, 783, 'Shipment', 171, 'Y', '1'),
(521, 784, 'Target Analysis', 113, 'Y', '1'),
(46, 785, 'CHILD NODE - DOCUMENT', 11, 'Y', '1'),
(385, 786, 'BACK ORDER', 39, 'Y', '1'),
(905, 787, 'As per Application Profile (Natural)', 168, 'Y', '1'),
(487, 788, 'Quantity', 108, 'Y', '1'),
(844, 789, 'Rental Sales Agreement', 154, 'Y', '1'),
(902, 790, 'PARTIAL - TRANSFERRED', 85, 'Y', '1'),
(750, 791, 'Draft', 131, 'Y', '1'),
(513, 792, 'Profit Analysis', 110, 'Y', '1'),
(872, 793, 'Draft', 163, 'Y', '1'),
(642, 794, 'FG', 121, 'Y', '1'),
(625, 795, 'FG', 120, 'Y', '1'),
(640, 796, 'Purchase Requisitions', 120, 'Y', '1'),
(259, 797, 'SUB CONTRACTOR', 56, 'N', '1'),
(596, 798, 'Credit Ageing', 118, 'Y', '1'),
(1072, 799, 'Materials Receipt Check List', 176, 'Y', '1'),
(330, 800, 'TRANSFER ORDER - WITHIN ORGANIZATION', 72, 'Y', '1'),
(93, 801, 'BANK BRANCH', 21, 'Y', '1'),
(897, 802, 'CREDIT SALES ORDER (SERVICES)', 68, 'Y', '1'),
(94, 803, 'FIELD ATTRIBUTES', 8, 'Y', '1'),
(890, 804, 'PENDING/PR RAISED', 85, 'Y', '1'),
(127, 805, 'FRIDGE', 28, 'Y', '1'),
(167, 806, 'SAFETY STOCK', 39, 'Y', '1'),
(668, 807, 'Raw Material', 122, 'Y', '1'),
(849, 808, 'CLOSE', 156, 'Y', '1'),
(769, 809, 'Requirement Area', 130, 'Y', '1'),
(274, 810, 'VALUE BASED', 58, 'Y', '1'),
(485, 811, 'Weighted average of Organization', 107, 'Y', '1'),
(432, 812, 'SHIPMENT', 72, 'Y', '1'),
(723, 813, 'Super Stockist', 74, 'Y', '1'),
(778, 814, 'Department', 138, 'Y', '1'),
(546, 815, 'Top Customer', 111, 'Y', '1'),
(379, 816, 'PENDING', 86, 'Y', '1'),
(885, 817, 'Import Purchase', 31, 'Y', '1'),
(981, 818, 'Sales Scheme', 185, 'Y', '1'),
(99, 819, 'STATEMENTS', 22, 'Y', '1'),
(142, 820, 'CUSTOMER', 30, 'Y', '1'),
(760, 821, 'CUSTOMER', 134, 'Y', '1'),
(661, 822, 'Credit Ageing', 121, 'Y', '1'),
(519, 823, 'Quotation Analysis', 112, 'Y', '1'),
(299, 824, 'RETURNED', 65, 'Y', '1'),
(479, 825, 'Deleted', 33, 'Y', '1'),
(89, 826, 'CUSTOMER', 21, 'Y', '1'),
(1004, 827, 'Purchase Invoice (High Sea Sales)', 100, 'Y', '1'),
(284, 828, 'OK', 61, 'Y', '1'),
(426, 829, 'PENDING STOCK UPDATE', 86, 'Y', '1'),
(20, 830, '6', 5, 'Y', '1'),
(466, 831, 'Current Month Return', 104, 'Y', '1'),
(1089, 832, 'Material Issue', 210, 'Y', '1'),
(27, 833, 'DETAIL LEVEL (1)', 9, 'Y', '1'),
(904, 834, 'BY TT', 37, 'Y', '1'),
(1005, 835, 'Loan Deduction', 190, 'Y', '1'),
(979, 836, 'CANCELLED', 86, 'Y', '1'),
(125, 837, 'OPEN', 28, 'Y', '1'),
(175, 838, 'Rate Contract', 34, 'Y', '1'),
(649, 839, 'Top Product', 121, 'Y', '1'),
(3, 840, 'DATE', 1, 'Y', '1'),
(65, 841, 'MOBILE', 17, 'Y', '1'),
(163, 842, 'BY ROAD', 38, 'Y', '1'),
(541, 843, 'KIT WORKSHOP', 57, 'Y', '1'),
(303, 844, 'LIFO', 66, 'Y', '1'),
(473, 845, 'Status', 105, 'Y', '1'),
(919, 846, 'Release', 172, 'Y', '1'),
(73, 847, 'TEXT ITEM - MANDATORY', 20, 'Y', '1'),
(969, 848, 'DOMESTIC PURCHASE', 184, 'Y', '1'),
(428, 849, 'FORWARDED', 85, 'Y', '1'),
(699, 850, 'Cost of Goods Sold', 31, 'Y', '1'),
(354, 851, 'INITIATED', 79, 'Y', '1'),
(243, 852, 'APP_LISENCE_ALERT_DAYS', 47, 'Y', '1'),
(144, 853, 'Purchase', 31, 'Y', '1'),
(812, 854, 'Forwarded', 146, 'Y', '1'),
(1097, 855, 'NONE', 211, 'Y', '1'),
(966, 856, 'CLOSED', 182, 'Y', '1'),
(550, 857, 'Top Products', 112, 'Y', '1'),
(557, 858, 'Top Product Group', 113, 'Y', '1'),
(52, 859, 'BRANCH OFFICE (GENERIC)', 14, 'Y', '1'),
(718, 860, 'Customer', 129, 'Y', '1'),
(9, 861, 'DATETIME', 2, 'Y', '1'),
(271, 862, 'CONSIGNMENT ORDER', 57, 'Y', '1'),
(533, 863, 'One Product', 116, 'Y', '1'),
(792, 864, 'Mail Generated and Sent', 141, 'Y', '1'),
(491, 865, 'Open Purchase Order', 40, 'Y', '1'),
(310, 866, 'CASH SALES ORDER', 68, 'Y', '1'),
(592, 867, 'Below Re-order Level', 118, 'Y', '1'),
(230, 868, 'PUBLIC', 54, 'Y', '1'),
(503, 869, 'Half Year', 115, 'Y', '1'),
(326, 870, 'MRS - Requirement Area', 72, 'Y', '1'),
(538, 871, 'Shipment', 117, 'Y', '1'),
(1012, 872, 'SALES ORDER', 72, 'Y', '1'),
(593, 873, 'Below Safety Level', 118, 'Y', '1'),
(151, 874, 'DRAFT', 33, 'Y', '1'),
(204, 875, 'CASH IN FLOW', 45, 'Y', '1'),
(721, 876, 'Item Group', 129, 'Y', '1'),
(283, 877, 'SAMPLE CHECK', 60, 'Y', '1'),
(160, 878, 'BY CHEQUE', 37, 'Y', '1'),
(296, 879, 'CLOSED', 64, 'Y', '1'),
(706, 880, 'PENDING', 125, 'Y', '1'),
(970, 881, 'Draft', 183, 'Y', '1'),
(806, 882, 'Export Sales Check List', 144, 'Y', '1'),
(641, 883, 'RM', 121, 'Y', '1'),
(824, 884, 'Stock Adjustment (Issue)', 71, 'Y', '1'),
(62, 885, 'XML', 16, 'Y', '1'),
(681, 886, 'Rejections', 122, 'Y', '1'),
(343, 887, 'BY AIR', 38, 'Y', '1'),
(134, 888, 'POWER', 29, 'Y', '1'),
(684, 889, 'Product Ageing', 122, 'Y', '1'),
(622, 890, 'Material Requisitoins', 119, 'Y', '1'),
(269, 891, 'RMA', 57, 'Y', '1'),
(644, 892, 'Trading', 121, 'Y', '1'),
(42, 893, 'CHILD NODE - PACKAGE', 11, 'N', '1'),
(226, 894, 'FIFO', 52, 'Y', '1'),
(774, 895, 'Stock Swap (Receipt)', 57, 'Y', '1'),
(1021, 896, 'Unrealized Amount', 194, 'Y', '1'),
(261, 897, 'DISTRIBUTOR', 56, 'N', '1'),
(708, 898, 'Material Return Note', 126, 'Y', '1'),
(375, 899, 'PENDING FOR ISSUE/RECEIPT', 85, 'Y', '1'),
(351, 900, 'AD-HOC', 77, 'Y', '1'),
(884, 901, 'SUBCONTRACTING RECEIPT', 59, 'Y', '1'),
(263, 902, 'WAREHOUSE OTHER ORGANIZATIONS', 56, 'Y', '1'),
(556, 903, 'Top Products', 113, 'Y', '1'),
(458, 904, 'CASH PURCHASE', 73, 'Y', '1'),
(636, 905, 'Below Re-order Level', 120, 'Y', '1'),
(307, 906, 'MANUAL', 67, 'Y', '1'),
(11, 907, 'dd/MM/yyyy', 3, 'Y', '1'),
(657, 908, 'Rejection Pattern', 121, 'Y', '1'),
(682, 909, 'Receipt vs Issue', 122, 'Y', '1'),
(720, 910, 'Customer Category', 129, 'Y', '1'),
(864, 911, 'Purchase Order', 160, 'Y', '1'),
(51, 912, 'HEAD OFFICE', 14, 'Y', '1'),
(30, 913, 'DETAIL LEVEL 4', 9, 'Y', '1'),
(648, 914, 'Top Warehouse', 121, 'Y', '1'),
(836, 915, 'External Entity', 130, 'Y', '1'),
(930, 916, 'OPEN PURCHASE AGREEMENT', 174, 'Y', '1'),
(728, 917, 'Category 4', 74, 'N', '1'),
(583, 918, 'Top Consumers', 118, 'Y', '1'),
(471, 919, 'Customer', 105, 'Y', '1'),
(674, 920, 'Top Organization', 122, 'Y', '1'),
(83, 921, 'LOV BUTTON', 20, 'Y', '1'),
(617, 922, 'Top Consumers', 119, 'Y', '1'),
(376, 923, 'TRANSFERRED', 85, 'Y', '1'),
(615, 924, 'Top Product Group', 119, 'Y', '1'),
(1058, 925, 'Sales Executive Incentives', 185, 'Y', '1'),
(997, 926, 'DIRECT CREDIT', 25, 'Y', '1'),
(627, 927, 'Trading', 120, 'Y', '1'),
(132, 928, 'WEIGHT', 29, 'Y', '1'),
(639, 929, 'Material Requisitoins', 120, 'Y', '1'),
(575, 930, 'Trading', 118, 'Y', '1'),
(353, 931, 'STOCK UPDATED', 78, 'Y', '1'),
(213, 932, 'DRAFT', 50, 'Y', '1'),
(223, 933, 'WEIGHTED AVERAGE PRICE', 52, 'Y', '1'),
(35, 934, 'QUERY FOR FLOW', 10, 'Y', '1'),
(81, 935, 'CHECK BOX - MANDATORY', 20, 'Y', '1'),
(915, 936, 'Transfer Order', 171, 'Y', '1'),
(730, 937, 'Category 6', 74, 'N', '1'),
(436, 938, 'RETURNABLES RECEIPT PENDING', 97, 'Y', '1'),
(559, 939, 'Top Sales Executive', 113, 'Y', '1'),
(146, 940, 'Consumption', 31, 'Y', '1'),
(637, 941, 'Below Safety Level', 120, 'Y', '1'),
(340, 942, 'RETAILER', 74, 'Y', '1'),
(614, 943, 'Top Product', 119, 'Y', '1'),
(938, 944, 'Just-in-Time Scheduled Receipt', 57, 'Y', '1'),
(335, 945, 'SHOP FLOOR', 73, 'Y', '1'),
(931, 946, 'CASH', 175, 'Y', '1'),
(740, 947, 'WITHDRAWAL RECEIPT', 25, 'Y', '1'),
(417, 948, 'DISPATCH ONLY', 93, 'Y', '1'),
(212, 949, 'CROSS DOCKING AND TRANSLOADING WAREHOUSE', 49, 'Y', '1'),
(531, 950, 'One Organization', 116, 'Y', '1'),
(357, 951, 'ANNUAL', 80, 'Y', '1'),
(676, 952, 'Top Product', 122, 'Y', '1'),
(802, 953, 'DRAFT', 63, 'Y', '1'),
(266, 954, 'PURCHASE ORDER', 57, 'Y', '1'),
(277, 955, 'PURCHASE REQUISITION', 39, 'Y', '1'),
(418, 956, 'FREEZED', 79, 'Y', '1'),
(200, 957, 'OPERATING INCOME', 45, 'Y', '1'),
(613, 958, 'Top Warehouse', 119, 'Y', '1'),
(877, 959, 'Expenses Entered', 164, 'Y', '1'),
(16, 960, 'LOWER', 4, 'Y', '1'),
(476, 961, 'Product Group', 105, 'Y', '1'),
(764, 962, 'SUPPLIER', 135, 'Y', '1'),
(211, 963, 'DISTRIBUTION WAREHOUSE', 49, 'Y', '1'),
(316, 964, 'HOLD', 69, 'Y', '1'),
(571, 965, 'Scrap', 118, 'Y', '1'),
(791, 966, 'Pending', 141, 'Y', '1'),
(563, 967, 'Top Product Group', 114, 'Y', '1'),
(387, 968, 'CONSIGNEE', 74, 'Y', '1'),
(653, 969, 'Receipts', 121, 'Y', '1'),
(233, 970, 'FORWARDED', 55, 'Y', '1'),
(494, 971, 'Top Product Group', 109, 'Y', '1'),
(368, 972, 'GOODS IN TRANSIT', 82, 'Y', '1'),
(480, 973, 'SUPPLEMENTARY INVOICE', 76, 'Y', '1'),
(1082, 974, 'PMS SHIPMENT', 72, 'Y', '1'),
(105, 975, 'HALF YEARLY', 23, 'Y', '1'),
(297, 976, 'PENDING', 65, 'Y', '1'),
(881, 977, 'Cash Purchase Order', 40, 'Y', '1'),
(554, 978, 'Top Regions', 112, 'Y', '1'),
(763, 979, 'CUSTOMER', 135, 'Y', '1'),
(580, 980, 'Top Product', 118, 'Y', '1'),
(115, 981, 'IMPORT SERVICE CONTRACTOR', 26, 'Y', '1'),
(414, 982, 'PURCHASE INVOICE', 92, 'Y', '1'),
(758, 983, 'NUMBER', 133, 'Y', '1'),
(157, 984, 'D:\\IMAGES\\ITEM\\', 35, 'Y', '1'),
(444, 985, 'PENDING', 99, 'Y', '1'),
(88, 986, 'SERVICE PROVIDER', 21, 'Y', '1'),
(493, 987, 'Top Products', 109, 'Y', '1'),
(17, 988, 'MIXED', 4, 'Y', '1'),
(574, 989, 'WIP', 118, 'Y', '1'),
(820, 990, 'Draft', 149, 'Y', '1'),
(45, 991, 'CHILD NODE - SEPERATOR NODE', 11, 'Y', '1'),
(287, 992, 'MATERIAL REQUISITION SLIP', 62, 'Y', '1'),
(295, 993, 'PENDING - PR RAISED', 64, 'Y', '1'),
(611, 994, 'Capital goods', 119, 'Y', '1'),
(294, 995, 'PENDING FOR MATERIALS ISSUE', 64, 'Y', '1'),
(252, 996, 'MAX_MENU_ROLES', 47, 'Y', '1'),
(876, 997, 'Advance Paid', 164, 'Y', '1'),
(725, 998, 'Category 1', 74, 'N', '1'),
(793, 999, 'Generate', 142, 'Y', '1'),
(44, 1000, 'CHILD NODE - SUB MODULE', 11, 'N', '1'),
(232, 1001, 'DRAFT', 55, 'Y', '1'),
(539, 1002, 'Invoice', 117, 'Y', '1'),
(846, 1003, 'Form H', 155, 'Y', '1'),
(964, 1004, 'CANCELLED', 147, 'Y', '1'),
(608, 1005, 'WIP', 119, 'Y', '1'),
(990, 1006, 'C-Others', 188, 'Y', '1'),
(861, 1007, 'Draft', 159, 'Y', '1'),
(1048, 1008, 'APPROVED', 201, 'Y', '1'),
(569, 1009, 'Reserved', 118, 'Y', '1'),
(894, 1010, 'Sales Order', 166, 'Y', '1'),
(704, 1011, 'External Meeting', 124, 'Y', '1'),
(327, 1012, 'Subcontracting - Outwards', 72, 'Y', '1'),
(870, 1013, 'Customer Products', 162, 'Y', '1'),
(696, 1014, 'Stock Adjustment Income', 31, 'N', '1'),
(238, 1015, 'RTGS', 25, 'Y', '1'),
(178, 1016, 'BUNDLE ONE OR MORE FREE PRODUCTS', 42, 'Y', '1'),
(784, 1017, 'e-mail', 139, 'Y', '1'),
(384, 1018, 'STOCK UPDATED AND INVOICE GENERATED', 86, 'Y', '1'),
(1060, 1019, 'Cancelled', 78, 'Y', '1'),
(341, 1020, 'CANCELLED', 64, 'Y', '1'),
(290, 1021, 'SALES ORDER', 62, 'N', '1'),
(594, 1022, 'Price History', 118, 'Y', '1'),
(816, 1023, 'Approved', 147, 'Y', '1'),
(165, 1024, 'PURCHASE ORDER', 39, 'Y', '1'),
(564, 1025, 'Top Customer', 114, 'Y', '1'),
(660, 1026, 'Price History', 121, 'Y', '1'),
(747, 1027, 'Sales Order', 130, 'Y', '1'),
(618, 1028, 'Below Re-order Level', 119, 'Y', '1'),
(786, 1029, 'Mobile App', 139, 'Y', '1'),
(207, 1030, 'PRIME RETAIL GROUP', 48, 'Y', '1'),
(276, 1031, 'Purchase Requisition', 34, 'Y', '1'),
(344, 1032, 'BY NEFT', 37, 'Y', '1'),
(712, 1033, 'Triplicate', 127, 'Y', '1'),
(112, 1034, 'DRAFT', 25, 'Y', '1'),
(665, 1035, 'Purchase Requisitions', 121, 'Y', '1'),
(26, 1036, 'HEADER', 9, 'Y', '1'),
(954, 1037, 'Approved', 180, 'Y', '1'),
(373, 1038, 'TRANSFER WITHIN ORGANIZATION', 84, 'Y', '1'),
(866, 1039, 'Verbal', 161, 'Y', '1'),
(819, 1040, 'IMPORT PURCHASE ORDER', 57, 'Y', '1'),
(507, 1041, 'Top Products', 110, 'Y', '1'),
(1, 1042, 'VARCHAR', 1, 'Y', '1'),
(2, 1043, 'NUMBER', 1, 'Y', '1'),
(634, 1044, 'Top Suppliers', 120, 'Y', '1'),
(893, 1045, 'CASH SALE INVOICE (SERVICES)', 76, 'Y', '1'),
(106, 1046, 'YEARLY', 23, 'Y', '1'),
(477, 1047, 'Customer Category', 105, 'Y', '1'),
(227, 1048, 'DRAFT', 53, 'Y', '1'),
(734, 1049, 'Category 10', 74, 'N', '1'),
(285, 1050, 'REWORKABLE', 61, 'Y', '1'),
(301, 1051, 'MOVING AVERAGE PRICE (MAP)', 66, 'Y', '1'),
(447, 1052, 'Provisional Stock', 31, 'N', '1'),
(50, 1053, 'TRANSACTION DOCUMENT', 13, 'Y', '1'),
(12, 1054, 'yyyy-MM-dd', 3, 'Y', '1'),
(192, 1055, 'OPERATIVE EXPENSE', 45, 'Y', '1'),
(817, 1056, 'Bill Of Entry', 148, 'Y', '1'),
(754, 1057, 'Consignee', 132, 'Y', '1'),
(1016, 1058, 'Cancelled', 98, 'Y', '1'),
(100, 1059, 'DAILY', 23, 'Y', '1'),
(1002, 1060, 'Category', 189, 'Y', '1'),
(147, 1061, 'Rejection', 31, 'Y', '1'),
(315, 1062, 'AMENDED', 69, 'Y', '1'),
(1092, 1063, 'CC Collection', 207, 'Y', '1'),
(40, 1064, 'EXIT', 10, 'Y', '1'),
(1014, 1065, 'Sales Shipment', 192, 'Y', '1'),
(654, 1066, 'Rejections', 121, 'Y', '1');

-- --------------------------------------------------------

--
-- Table structure for table `intrm$ds$att$reln`
--

CREATE TABLE `intrm$ds$att$reln` (
  `ATT_ID` int(5) DEFAULT NULL,
  `ATT_TYPE_ID` int(5) DEFAULT NULL,
  `ATT_ID_RELN` int(5) DEFAULT NULL,
  `POS_ATT_ID_RELN` int(11) DEFAULT NULL,
  `ATT_TYPE_ID_RELN` int(5) DEFAULT NULL,
  `ACTV` varchar(1) DEFAULT NULL,
  `SYNC_FLG` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$ds$att$reln`
--

INSERT INTO `intrm$ds$att$reln` (`ATT_ID`, `ATT_TYPE_ID`, `ATT_ID_RELN`, `POS_ATT_ID_RELN`, `ATT_TYPE_ID_RELN`, `ACTV`, `SYNC_FLG`) VALUES
(170, 40, 156, 1, 34, 'Y', '1'),
(170, 40, 175, 2, 34, 'Y', '1'),
(171, 40, 154, 3, 34, 'Y', '1'),
(171, 40, 155, 4, 34, 'Y', '1'),
(171, 40, 174, 5, 34, 'Y', '1'),
(171, 40, 181, 6, 34, 'Y', '1'),
(171, 40, 276, 7, 34, 'Y', '1'),
(171, 40, 490, 8, 34, 'Y', '1'),
(171, 40, 992, 9, 34, 'Y', '1'),
(172, 40, 156, 10, 34, 'Y', '1'),
(172, 40, 175, 11, 34, 'Y', '1'),
(173, 40, 156, 12, 34, 'N', '1'),
(173, 40, 181, 13, 34, 'Y', '1'),
(173, 40, 276, 14, 34, 'Y', '1'),
(256, 56, 266, 15, 57, 'Y', '1'),
(256, 56, 267, 16, 57, 'N', '1'),
(256, 56, 370, 17, 57, 'Y', '1'),
(256, 56, 819, 18, 57, 'Y', '1'),
(256, 56, 938, 19, 57, 'Y', '1'),
(256, 56, 995, 20, 57, 'Y', '1'),
(257, 56, 268, 21, 57, 'Y', '1'),
(257, 56, 269, 22, 57, 'Y', '1'),
(260, 56, 459, 23, 57, 'Y', '1'),
(262, 56, 270, 24, 57, 'Y', '1'),
(263, 56, 270, 25, 57, 'Y', '1'),
(323, 71, 326, 26, 72, 'Y', '1'),
(323, 71, 543, 27, 72, 'Y', '1'),
(323, 71, 1029, 28, 72, 'Y', '1'),
(324, 71, 327, 29, 72, 'Y', '1'),
(324, 71, 328, 30, 72, 'Y', '1'),
(324, 71, 329, 31, 72, 'Y', '1'),
(324, 71, 432, 32, 72, 'Y', '1'),
(324, 71, 800, 33, 72, 'Y', '1'),
(324, 71, 837, 34, 72, 'Y', '1'),
(324, 71, 891, 35, 72, 'Y', '1'),
(324, 71, 1082, 36, 72, 'Y', '1'),
(325, 71, 330, 37, 72, 'Y', '1'),
(325, 71, 366, 38, 72, 'Y', '1'),
(331, 73, 266, 39, 57, 'Y', '1'),
(331, 73, 370, 40, 57, 'Y', '1'),
(331, 73, 819, 41, 57, 'Y', '1'),
(331, 73, 938, 42, 57, 'Y', '1'),
(331, 73, 995, 43, 57, 'Y', '1'),
(332, 73, 268, 44, 57, 'N', '1'),
(332, 73, 269, 45, 57, 'Y', '1'),
(332, 73, 825, 46, 57, 'Y', '1'),
(333, 73, 858, 47, 57, 'Y', '1'),
(334, 73, 267, 48, 57, 'Y', '1'),
(334, 73, 399, 49, 57, 'Y', '1'),
(335, 73, 770, 50, 57, 'Y', '1'),
(335, 73, 996, 51, 57, 'Y', '1'),
(337, 73, 270, 52, 57, 'Y', '1'),
(338, 73, 270, 53, 57, 'Y', '1'),
(396, 73, 396, 54, 57, 'Y', '1'),
(458, 73, 459, 55, 57, 'Y', '1'),
(491, 40, 181, 56, 34, 'Y', '1'),
(542, 73, 541, 57, 57, 'Y', '1'),
(741, 40, 155, 58, 34, 'Y', '1'),
(741, 40, 156, 59, 34, 'Y', '1'),
(741, 40, 181, 60, 34, 'Y', '1'),
(741, 40, 276, 61, 34, 'Y', '1'),
(773, 73, 774, 62, 57, 'Y', '1'),
(775, 71, 776, 63, 72, 'Y', '1'),
(777, 73, 266, 64, 57, 'Y', '1'),
(777, 73, 825, 65, 57, 'Y', '1'),
(824, 71, 749, 66, 72, 'Y', '1'),
(881, 40, 155, 67, 34, 'Y', '1'),
(881, 40, 181, 68, 34, 'Y', '1'),
(881, 40, 276, 69, 34, 'Y', '1'),
(920, 71, 824, 70, 72, 'Y', '1'),
(1003, 40, 181, 71, 34, 'Y', '1');

-- --------------------------------------------------------

--
-- Table structure for table `intrm$ds$att$type`
--

CREATE TABLE `intrm$ds$att$type` (
  `ATT_TYPE_ID` int(5) DEFAULT NULL COMMENT 'Attribute Type like 73 for ''Receipt Source Type''',
  `POS_ATT_TYPE_ID` int(11) DEFAULT NULL,
  `ATT_TYPE_NM` varchar(50) DEFAULT NULL,
  `ATT_TYPE_ACTV` varchar(1) DEFAULT NULL,
  `SYNC_FLG` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$ds$att$type`
--

INSERT INTO `intrm$ds$att$type` (`ATT_TYPE_ID`, `POS_ATT_TYPE_ID`, `ATT_TYPE_NM`, `ATT_TYPE_ACTV`, `SYNC_FLG`) VALUES
(136, 1, 'LC Definition Status', 'Y', '1'),
(86, 2, 'MATERIAL RECEIPT STATUS', 'Y', '1'),
(41, 3, 'CPO ', 'Y', '1'),
(104, 4, 'Sales Ticker Type', 'Y', '1'),
(184, 5, 'Expense Invoice Type', 'Y', '1'),
(28, 6, 'BIN STORAGE TYPE', 'Y', '1'),
(172, 7, 'Stock Reserve Modification Type', 'Y', '1'),
(99, 8, 'OPPORTUNITY STATUS', 'Y', '1'),
(142, 9, 'Barcode profile usage type', 'Y', '1'),
(90, 10, 'SALES RETURN TYPE', 'Y', '1'),
(31, 11, 'ITEM COA MAPPING', 'Y', '1'),
(208, 12, 'Batch Purchase Order Status', 'Y', '1'),
(143, 13, 'Alert Severity Level', 'Y', '1'),
(103, 14, 'RFQ SOURCE TYPE', 'Y', '1'),
(191, 15, 'Interim Voucher Status', 'Y', '1'),
(119, 16, 'Ordered Stock MIS', 'Y', '1'),
(32, 17, 'REPLY METHOD', 'Y', '1'),
(89, 18, 'Sample', 'Y', '1'),
(33, 19, 'RFQ STATUS', 'Y', '1'),
(180, 20, 'Supplier Price Setup Document Status', 'Y', '1'),
(20, 21, 'CI ITEM TYPES', 'Y', '1'),
(6, 22, 'NO. OF PLACES BEFORE FIRST COMMA', 'Y', '1'),
(152, 23, 'Subcontracting Item Type', 'Y', '1'),
(137, 24, 'Stock Swapping Type', 'Y', '1'),
(27, 25, 'TDS DIVISION', 'Y', '1'),
(125, 26, 'SCRAP SALE STATUS', 'Y', '1'),
(170, 27, 'Stock Reserve Search Source', 'Y', '1'),
(175, 28, 'JIT Receipt Transaction Type', 'Y', '1'),
(77, 29, 'STOCK ADJUSTMENT TYPE', 'Y', '1'),
(3, 30, 'DATA FORMAT - DATE1', 'Y', '1'),
(82, 31, 'MATERIAL ISSUE STATUS', 'Y', '1'),
(65, 32, 'GATE ENTRY STATUS', 'Y', '1'),
(21, 33, 'EXT ENTITY TYPES', 'Y', '1'),
(10, 34, 'DOCUMENT OPERATION MODES', 'Y', '1'),
(129, 35, 'Discount Policy Basis', 'Y', '1'),
(79, 36, 'STOCK TAKE STATUS', 'Y', '1'),
(163, 37, 'Purchase Invoice Status', 'Y', '1'),
(73, 38, 'MATERIAL RECEIPT SOURCE - STORES', 'Y', '1'),
(176, 39, 'App Parameter Setup', 'Y', '1'),
(188, 40, 'Item for Display/Storage', 'Y', '1'),
(116, 41, 'BI Pages Type', 'Y', '1'),
(144, 42, 'Document Check List Type', 'Y', '1'),
(59, 43, 'QC ITEM TYPE', 'Y', '1'),
(156, 44, 'Subcontracting Document Status', 'Y', '1'),
(52, 45, 'COSTING METHOD', 'Y', '1'),
(63, 46, 'PURCHASE REQUISITION STATUS', 'Y', '1'),
(14, 47, 'ORG TYPE', 'Y', '1'),
(120, 48, 'Requirements MIS', 'Y', '1'),
(9, 49, 'DOCUMENT DISPLAY LEVELS', 'Y', '1'),
(187, 50, 'Scheme Applicable On', 'Y', '1'),
(38, 51, 'DELIVERY MODE', 'Y', '1'),
(117, 52, 'Invoice Currency Conversion Type', 'Y', '1'),
(135, 53, 'LC Definition Type', 'Y', '1'),
(43, 54, 'OTHER CHARGES', 'Y', '1'),
(159, 55, 'Procurement Expense Invoice Status', 'Y', '1'),
(84, 56, 'TRANSFER ORDER TYPE', 'Y', '1'),
(166, 57, 'Document Confirmation Type', 'Y', '1'),
(151, 58, 'Subcontracting Order Basis', 'Y', '1'),
(204, 59, 'SALES DIRECT QUOTATION TYPE', 'Y', '1'),
(203, 60, 'TREO FORM NO.', 'Y', '1'),
(70, 61, 'SALES QUOTATION STATUS', 'Y', '1'),
(160, 62, 'Expense invoice source document type', 'Y', '1'),
(155, 63, 'Form Type', 'Y', '1'),
(102, 64, 'New attribute', 'Y', '1'),
(49, 65, 'WAREHOUSE STORAGE TYPE', 'Y', '1'),
(185, 66, 'Scheme Category', 'Y', '1'),
(58, 67, 'QC PARAMETER TYPE', 'Y', '1'),
(98, 68, 'INTIMATION SLIP STATUS', 'Y', '1'),
(62, 69, 'PURCHASE REQUISITION TYPE', 'Y', '1'),
(134, 70, 'LC Parameter Basis', 'Y', '1'),
(42, 71, 'SCHEME TYPE', 'Y', '1'),
(36, 72, 'QOT SOURCE', 'Y', '1'),
(69, 73, 'SO STATUS', 'Y', '1'),
(181, 74, 'Parameter Value Type', 'Y', '1'),
(13, 75, 'DOCUMENT TYPES', 'Y', '1'),
(206, 76, 'SUPPLIER BASIS', 'Y', '1'),
(11, 77, 'MENU TYPE', 'Y', '1'),
(124, 78, 'CUSTOMER CONTACT TYPE', 'Y', '1'),
(164, 79, 'Finance Requisition Status', 'Y', '1'),
(34, 80, 'PURCHASE ORDER BASIS', 'Y', '1'),
(91, 81, 'ITEM REJECTION TYPE', 'Y', '1'),
(189, 82, 'Customer Sub Category Type', 'Y', '1'),
(110, 83, 'Profit MIS Type', 'Y', '1'),
(15, 84, 'CURRENCY TRANSACTION TYPE', 'Y', '1'),
(201, 85, 'QUOTA STATUS', 'Y', '1'),
(122, 86, 'Consumption MIS', 'Y', '1'),
(51, 87, 'PO STATUS', 'Y', '1'),
(30, 88, 'EVAL PARAM CAT FLG ', 'Y', '1'),
(4, 89, 'DATA FORMAT - CHARACTER1', 'Y', '1'),
(161, 90, 'Sales Order Referece Type', 'Y', '1'),
(177, 91, 'Item Price Type on Shipment', 'Y', '1'),
(198, 92, 'RECONCILIATION STATUS', 'Y', '1'),
(83, 93, 'TRANSFER ORDER SOURCE TYPE', 'Y', '1'),
(149, 94, 'Insurance Document Status', 'Y', '1'),
(56, 95, 'MATERIAL RECEIPT SOURCE - GATE ENTRY', 'Y', '1'),
(111, 96, 'Order MIS Type', 'Y', '1'),
(48, 97, 'ACCOUNT ALIAS', 'Y', '1'),
(19, 98, 'WEB COMM TYPES', 'Y', '1'),
(132, 99, 'External Entity Type Behaviour', 'Y', '1'),
(96, 100, 'GATE PASS SOURCE DOCUMENT TYPE', 'Y', '1'),
(113, 101, 'Opportunity MIS Type', 'Y', '1'),
(130, 102, 'Material Requisition Source Type', 'Y', '1'),
(47, 103, 'PROFILE TABS', 'Y', '1'),
(67, 104, 'MATERIAL PICK ORDER', 'Y', '1'),
(158, 105, 'Expense type for expense invoice (tax/oc)', 'Y', '1'),
(74, 106, 'CUSTOMER CATEGORY', 'Y', '1'),
(35, 107, 'ITEM IMAGE PATH', 'Y', '1'),
(81, 108, 'STOCK TAKE MATERIAL SELECTION CRITERIA', 'Y', '1'),
(97, 109, 'GATE PASS OUT STATUS', 'Y', '1'),
(162, 110, 'Sales Order Item Search Basis', 'Y', '1'),
(121, 111, 'Purchases MIS', 'Y', '1'),
(133, 112, 'LC Parameter Type', 'Y', '1'),
(154, 113, 'Stock Ownership Type (Requirement Area)', 'Y', '1'),
(196, 114, 'Pack Type', 'Y', '1'),
(146, 115, 'Shipment Advice Document Status', 'Y', '1'),
(210, 116, 'Warranty Start Type for Purchased Item', 'Y', '1'),
(76, 117, 'SALES INVOICE TYPE', 'Y', '1'),
(197, 118, 'QUOTA TYPE', 'Y', '1'),
(150, 119, 'Subcontracting Type', 'Y', '1'),
(25, 120, 'INSTRUMENT TYPE', 'Y', '1'),
(205, 121, 'FA REGISTRATION STATUS', 'Y', '1'),
(200, 122, 'TREO APPLICATION STATUS', 'Y', '1'),
(183, 123, 'Entity - Transporter Mapping Document Status', 'Y', '1'),
(16, 124, 'REPORT OUTPUT', 'Y', '1'),
(211, 125, 'Warranty Start From', 'Y', '1'),
(57, 126, 'MATERIAL RECEIPT SOURCE DOCUMENTS', 'Y', '1'),
(139, 127, 'Application Alert Medium', 'Y', '1'),
(126, 128, 'Purchase Return Types', 'Y', '1'),
(61, 129, 'QC RESULT', 'Y', '1'),
(60, 130, 'QC CHECK TYPE', 'Y', '1'),
(101, 131, 'Delivery schedule', 'Y', '1'),
(123, 132, 'Credit MIS', 'Y', '1'),
(95, 133, 'PURCHASE RETURN STATUS', 'Y', '1'),
(105, 134, 'Search Parameters', 'Y', '1'),
(23, 135, 'DOCUMENT RESET TYPES', 'Y', '1'),
(68, 136, 'SO TYPE', 'Y', '1'),
(115, 137, 'Data Range Type', 'Y', '1'),
(37, 138, 'PAYMENT MODE', 'Y', '1'),
(195, 139, 'Sales Order Source', 'Y', '1'),
(174, 140, 'JIT Receipt Source Type', 'Y', '1'),
(157, 141, 'Requirement Area Issue/Receipt Source', 'Y', '1'),
(85, 142, 'TRANSFER ORDER STATUS', 'Y', '1'),
(53, 143, 'PO MODE', 'Y', '1'),
(88, 144, 'INVENTORY ANALYSIS VALUATION METHOD', 'Y', '1'),
(17, 145, 'COMMUNICATION DEVICES', 'Y', '1'),
(182, 146, 'RMDA Status', 'Y', '1'),
(45, 147, 'REPORT CATEGORY TYPE', 'Y', '1'),
(108, 148, 'Open Contract Basis', 'Y', '1'),
(194, 149, 'Credit Limit Basis', 'Y', '1'),
(193, 150, 'Credit Note Document', 'Y', '1'),
(78, 151, 'STOCK ADJUSTMENT STATUS', 'Y', '1'),
(179, 152, 'Parameter Profile Type', 'Y', '1'),
(22, 153, 'MENU SEPERATOR TYPES', 'Y', '1'),
(131, 154, 'IDF Application Status', 'Y', '1'),
(190, 155, 'Deduction Type', 'Y', '1'),
(169, 156, 'Stock Value Type', 'Y', '1'),
(44, 157, 'COMMUNICATION TYPE', 'Y', '1'),
(54, 158, 'WAREHOUSE OWNERSHIP TYPE', 'Y', '1'),
(209, 159, 'External Material Requisition Type', 'Y', '1'),
(107, 160, 'Price Policy Average Method', 'Y', '1'),
(128, 161, 'HCM Parameters', 'Y', '1'),
(24, 162, 'CHEQUE STATUS', 'Y', '1'),
(55, 163, 'QUOTATION ANALYSIS STATUS', 'Y', '1'),
(140, 164, 'Application Alert Generation Method', 'Y', '1'),
(168, 165, 'Amount Round Off Logic', 'Y', '1'),
(178, 166, 'Procurement Limit Document Status', 'Y', '1'),
(141, 167, 'Application Alert Display Status', 'Y', '1'),
(5, 168, 'NO. OF PLACES OF DECIMAL1', 'Y', '1'),
(8, 169, 'GENERIC1', 'Y', '1'),
(80, 170, 'STOCK TAKE CYCLE', 'Y', '1'),
(165, 171, 'Purchase Return Type', 'Y', '1'),
(148, 172, 'Bill of Entry Type', 'Y', '1'),
(138, 173, 'Requirement Area Type', 'Y', '1'),
(39, 174, 'SUGGESTED ORDER BASIS', 'Y', '1'),
(173, 175, 'JIT Receipt Status', 'Y', '1'),
(207, 176, 'SALES DELIVERY TYPE', 'Y', '1'),
(114, 177, 'Rejection MIS Type', 'Y', '1'),
(29, 178, 'UOM TYPE', 'Y', '1'),
(66, 179, 'MATERIAL VALUATION METHOD', 'Y', '1'),
(112, 180, 'Quotation MIS Type', 'Y', '1'),
(26, 181, 'SUPPLIER CATEGORY', 'Y', '1'),
(75, 182, 'MATERIAL RECEIPT STAGE', 'Y', '1'),
(18, 183, 'EXT ENTITY NAME TYPES', 'Y', '1'),
(153, 184, 'Subcontracting Issue/Reciept Source', 'Y', '1'),
(72, 185, 'MATERIAL ISSUE DOCUMENTS', 'Y', '1'),
(186, 186, 'High Sea Sales Type', 'Y', '1'),
(92, 187, 'PLASTIC MATERIAL', 'Y', '1'),
(109, 188, 'Sales MIS Type', 'Y', '1'),
(46, 189, 'AP BATCH STATUS', 'Y', '1'),
(1, 190, 'DATA TYPE - DATABASE1', 'Y', '1'),
(2, 191, 'DATA TYPE - APPLICATION1', 'Y', '1'),
(106, 192, 'Price Ploicy Type', 'Y', '1'),
(145, 193, 'Shipment Basis', 'Y', '1'),
(50, 194, 'QUOTATION STATUS', 'Y', '1'),
(118, 195, 'Stock in Hand MIS', 'Y', '1'),
(64, 196, 'MATERIAL REQUISITION SLIP STATUS', 'Y', '1'),
(87, 197, 'SO SOURCE', 'Y', '1'),
(12, 198, 'MENU ACCESSABILITY', 'Y', '1'),
(93, 199, 'RMDA TYPE', 'Y', '1'),
(94, 200, 'MRN STATUS', 'Y', '1'),
(192, 201, 'Secondary Sales Source', 'Y', '1'),
(7, 202, 'NO. OF PLACES AFTER FIRST COMMA', 'Y', '1'),
(171, 203, 'Stock Reserve Document Type', 'Y', '1'),
(127, 204, 'Sales Invocie Print Type', 'Y', '1'),
(167, 205, 'Entity Setup', 'Y', '1'),
(71, 206, 'MATERIAL ISSUE TYPE', 'Y', '1'),
(100, 207, 'PURCHASE INVOICE TYPE', 'Y', '1'),
(147, 208, 'Bill of Entry/Estimated Cost Doc Status', 'Y', '1'),
(40, 209, 'PO TYPE', 'Y', '1'),
(199, 210, 'RECONCILIATION MODE', 'Y', '1');

-- --------------------------------------------------------

--
-- Table structure for table `intrm$eo`
--

CREATE TABLE `intrm$eo` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(2) DEFAULT NULL,
  `HO_ORG_ID` varchar(2) DEFAULT NULL,
  `EO_ORG_ID` varchar(2) DEFAULT NULL,
  `EO_TYPE` varchar(1) DEFAULT NULL,
  `EO_ID` int(20) DEFAULT NULL,
  `LOYALITY_EO_ID` varchar(20) DEFAULT NULL,
  `POS_EO_ID` int(20) DEFAULT NULL,
  `LOYALITY_POS_EO_ID` varchar(20) DEFAULT NULL,
  `EO_CATG_ID` varchar(40) DEFAULT NULL,
  `EO_ALIAS` varchar(20) DEFAULT NULL,
  `EO_LEG_CODE` varchar(20) DEFAULT NULL,
  `EO_NM` varchar(250) NOT NULL,
  `EO_CNTRY_ID` varchar(50) DEFAULT NULL,
  `EO_ACC_ID` varchar(20) DEFAULT NULL,
  `AVG_CR_DAYS` int(5) DEFAULT NULL,
  `CR_LIMIT` int(15) DEFAULT NULL,
  `AVG_DLV_DAYS` int(5) DEFAULT NULL,
  `EO_CURR_ID` varchar(20) DEFAULT NULL,
  `TDS_DIVISION` varchar(50) DEFAULT NULL,
  `TDS_COLLECTORATE` varchar(50) DEFAULT NULL,
  `TDS_RANGE` varchar(50) DEFAULT NULL,
  `PAN_GIR_NO` varchar(50) DEFAULT NULL,
  `REGN_CERTI_NO` varchar(50) DEFAULT NULL,
  `ECC_NO` varchar(50) DEFAULT NULL,
  `SERVICE_TAX_NO` varchar(50) DEFAULT NULL,
  `TIN_NO` varchar(50) DEFAULT NULL,
  `VAT_NO` varchar(50) DEFAULT NULL,
  `ON_HOLD` varchar(1) DEFAULT NULL,
  `BLACK_LISTED` varchar(1) DEFAULT NULL,
  `BLACKLIST_RESN` varchar(200) DEFAULT NULL,
  `BLACKLIST_FRM_DT` date DEFAULT NULL,
  `BLACKLIST_TO_DT` date DEFAULT NULL,
  `ONHOLD_RESN` varchar(200) DEFAULT NULL,
  `ONHOLD_FRM_DT` date DEFAULT NULL,
  `ONHOLD_TO_DT` date DEFAULT NULL,
  `EO_SMAN_ID` varchar(20) DEFAULT NULL,
  `ALLOW_RET_GOODS` varchar(1) DEFAULT NULL,
  `EO_CHECK_ORD_ADV` varchar(2) DEFAULT NULL,
  `EO_ORD_ADV_TYPE` varchar(2) DEFAULT NULL,
  `EO_ORD_ADV_VAL` int(26) DEFAULT NULL,
  `CST_NO` varchar(50) DEFAULT NULL,
  `CST_DT` date DEFAULT NULL,
  `CIN_NO` varchar(50) DEFAULT NULL,
  `USR_ID_CREATE` int(4) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(4) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL,
  `EO_PHONE` varchar(200) DEFAULT NULL,
  `UPD_FLG` varchar(2) DEFAULT NULL,
  `SN` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$eo`
--

INSERT INTO `intrm$eo` (`CLD_ID`, `SLOC_ID`, `HO_ORG_ID`, `EO_ORG_ID`, `EO_TYPE`, `EO_ID`, `LOYALITY_EO_ID`, `POS_EO_ID`, `LOYALITY_POS_EO_ID`, `EO_CATG_ID`, `EO_ALIAS`, `EO_LEG_CODE`, `EO_NM`, `EO_CNTRY_ID`, `EO_ACC_ID`, `AVG_CR_DAYS`, `CR_LIMIT`, `AVG_DLV_DAYS`, `EO_CURR_ID`, `TDS_DIVISION`, `TDS_COLLECTORATE`, `TDS_RANGE`, `PAN_GIR_NO`, `REGN_CERTI_NO`, `ECC_NO`, `SERVICE_TAX_NO`, `TIN_NO`, `VAT_NO`, `ON_HOLD`, `BLACK_LISTED`, `BLACKLIST_RESN`, `BLACKLIST_FRM_DT`, `BLACKLIST_TO_DT`, `ONHOLD_RESN`, `ONHOLD_FRM_DT`, `ONHOLD_TO_DT`, `EO_SMAN_ID`, `ALLOW_RET_GOODS`, `EO_CHECK_ORD_ADV`, `EO_ORD_ADV_TYPE`, `EO_ORD_ADV_VAL`, `CST_NO`, `CST_DT`, `CIN_NO`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `EO_PHONE`, `UPD_FLG`, `SN`) VALUES
('0000', 1, '01', '01', 'T', 13, '13', 3, NULL, NULL, NULL, NULL, 'SELF TRANSPORT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-01-28', 1, '2017-01-28', NULL, NULL, 1),
('0000', 1, '01', '04', 'S', 10, '10', 4, NULL, '735', NULL, '4010101001', 'IFFCO SUPPLIER (UP SO)', NULL, NULL, NULL, NULL, NULL, '73', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-11-07', NULL, NULL, NULL, NULL, 2),
('0000', 1, '01', '01', 'C', 1, '1', 0, NULL, '723', 'IEBL', NULL, 'IEBL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-11-03', NULL, NULL, NULL, NULL, 3),
('0000', 1, '01', '01', 'S', 3, '3', 2, NULL, '735', NULL, '4010101001', 'IFFCO SUPPLIER', NULL, NULL, NULL, NULL, NULL, '73', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-11-07', NULL, NULL, NULL, NULL, 4),
('0000', 1, '01', '04', 'C', 9, '9', 0, NULL, '387', NULL, NULL, 'IFFCO CLIENT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-11-25', 1, '2016-11-25', NULL, NULL, 5),
('0000', 1, '01', '01', 'C', 12, '12', 5, NULL, '724', NULL, NULL, 'CASH CUSTOMER', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-01-28', 1, '2017-01-28', NULL, NULL, 6),
('0000', 1, '01', '04', 'C', 14, NULL, 6, NULL, 'T002', NULL, NULL, 'ankit01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-01-30', 1, '2017-01-30', '9845634567', NULL, 7);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$eo$add`
--

CREATE TABLE `intrm$eo$add` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(2) DEFAULT NULL,
  `HO_ORG_ID` varchar(2) DEFAULT NULL,
  `EO_ID` int(20) DEFAULT NULL,
  `EO_TYPE` varchar(1) NOT NULL,
  `POS_EO_ID` int(20) DEFAULT NULL,
  `EO_ADD_TYPE` varchar(1) DEFAULT NULL,
  `ADDRESS_ID` varchar(40) DEFAULT NULL,
  `POS_ADDRESS_ID` int(20) DEFAULT NULL,
  `ADDRESS` varchar(250) NOT NULL,
  `DEF_ADD_FLG` varchar(1) NOT NULL,
  `USR_ID_CREATE` int(4) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(4) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL,
  `UPD_FLG` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$eo$add`
--

INSERT INTO `intrm$eo$add` (`CLD_ID`, `SLOC_ID`, `HO_ORG_ID`, `EO_ID`, `EO_TYPE`, `POS_EO_ID`, `EO_ADD_TYPE`, `ADDRESS_ID`, `POS_ADDRESS_ID`, `ADDRESS`, `DEF_ADD_FLG`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `UPD_FLG`) VALUES
('0000', 1, '01', 3, 'S', 2, NULL, 'ADDS0000000003', NULL, 'C-1, District Centre, Saket Place, South Delhi, New Delhi\nTehsil - South Delhi \nDistrict - South Delhi \nState - Delhi \nINDIA\n110017', 'Y', 1, '2016-11-07', NULL, NULL, NULL),
('0000', 1, '01', 13, 'T', 3, NULL, 'ADDS0000000003', NULL, 'C-1, District Centre, Saket Place, South Delhi, New Delhi\nTehsil - South Delhi \nDistrict - South Delhi \nState - Delhi \nINDIA\n110017', 'Y', 1, '2017-01-28', 1, '2017-01-28', NULL),
('0000', 1, '01', 10, 'S', 4, NULL, 'ADDS0000000003', NULL, 'C-1, District Centre, Saket Place, South Delhi, New Delhi\nTehsil - South Delhi \nDistrict - South Delhi \nState - Delhi \nINDIA\n110017', 'Y', 1, '2016-11-07', NULL, NULL, NULL),
('0000', 1, '01', 12, 'C', 5, NULL, 'ADDS0000000003', NULL, 'C-1, District Centre, Saket Place, South Delhi, New Delhi\nTehsil - South Delhi \nDistrict - South Delhi \nState - Delhi \nINDIA\n110017', 'Y', 1, '2017-01-28', 1, '2017-01-28', NULL),
('0000', 1, '01', NULL, 'C', 0, NULL, NULL, NULL, 'asdbhaj asdkajs', 'N', 1, '2017-01-30', 1, '2017-01-30', '1'),
('0000', 1, '01', 14, 'C', 6, NULL, 'ADDS0000000032', NULL, 'asdbhaj asdkajs\nINDIA\n', 'N', 1, '2017-01-30', NULL, NULL, NULL),
('0000', 1, '01', 14, 'C', 6, NULL, 'ADDS0000000034', NULL, 'asdbhaj asdkajs\nINDIA\n', 'N', 1, '2017-01-30', NULL, NULL, NULL),
('0000', 1, '01', 14, 'C', 6, NULL, 'ADDS0000000031', NULL, 'asdbhaj asdkajs\nINDIA\n', 'N', 1, '2017-01-30', NULL, NULL, NULL),
('0000', 1, '01', 14, 'C', 6, NULL, 'ADDS0000000033', NULL, 'asdbhaj asdkajs\nINDIA\n', 'N', 1, '2017-01-30', NULL, NULL, NULL),
('0000', 1, '01', 14, 'C', 6, NULL, 'ADDS0000000036', NULL, 'asdbhaj asdkajs\nINDIA\n', 'N', 1, '2017-01-30', NULL, NULL, NULL),
('0000', 1, '01', 14, 'C', 6, NULL, 'ADDS0000000030', NULL, 'asdbhaj asdkajs\nINDIA\n', 'N', 1, '2017-01-30', NULL, NULL, NULL),
('0000', 1, '01', 14, 'C', 6, NULL, 'ADDS0000000035', NULL, 'asdbhaj asdkajs\nINDIA\n', 'N', 1, '2017-01-30', NULL, NULL, NULL),
('0000', 1, '01', 14, 'C', 6, NULL, 'ADDS0000000037', NULL, 'asdbhaj asdkajs\nINDIA\n', 'N', 1, '2017-01-30', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$eo$add$org`
--

CREATE TABLE `intrm$eo$add$org` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(2) NOT NULL,
  `HO_ORG_ID` varchar(2) NOT NULL,
  `ORG_ID` varchar(20) NOT NULL,
  `EO_ID` int(20) NOT NULL,
  `EO_TYPE` varchar(1) NOT NULL,
  `POS_EO_ID` int(20) DEFAULT NULL,
  `ADDRESS_ID` varchar(40) DEFAULT NULL,
  `POS_ADDRESS_ID` int(20) DEFAULT NULL,
  `CREATE_FLG` varchar(1) DEFAULT NULL,
  `UPD_FLG` varchar(1) DEFAULT NULL,
  `DEL_FLG` varchar(1) DEFAULT NULL,
  `USR_ID_CREATE` int(4) NOT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(4) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$eo$add$org`
--

INSERT INTO `intrm$eo$add$org` (`CLD_ID`, `SLOC_ID`, `HO_ORG_ID`, `ORG_ID`, `EO_ID`, `EO_TYPE`, `POS_EO_ID`, `ADDRESS_ID`, `POS_ADDRESS_ID`, `CREATE_FLG`, `UPD_FLG`, `DEL_FLG`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`) VALUES
('0000', 1, '01', '01', 3, 'S', 2, 'ADDS0000000003', NULL, '1', NULL, NULL, 1, '2016-11-07', NULL, NULL),
('0000', 1, '01', '04', 13, 'T', 3, 'ADDS0000000003', NULL, '1', NULL, NULL, 1, '2017-01-28', 1, '2017-01-28'),
('0000', 1, '01', '04', 13, 'T', 3, 'ADDS0000000003', NULL, '1', NULL, NULL, 1, '2017-01-28', 1, '2017-01-28'),
('0000', 1, '01', '04', 13, 'T', 3, 'ADDS0000000003', NULL, '1', NULL, NULL, 1, '2017-01-28', 1, '2017-01-28'),
('0000', 1, '01', '01', 12, 'C', 5, 'ADDS0000000003', NULL, '1', NULL, NULL, 1, '2017-01-28', 1, '2017-01-28'),
('0000', 1, '01', '04', 10, 'S', 4, 'ADDS0000000003', NULL, '1', NULL, NULL, 1, '2016-11-07', NULL, NULL),
('0000', 1, '01', '04', 10, 'S', 4, 'ADDS0000000003', NULL, '1', NULL, NULL, 1, '2016-11-07', NULL, NULL),
('0000', 1, '01', '04', 10, 'S', 4, 'ADDS0000000003', NULL, '1', NULL, NULL, 1, '2016-11-07', NULL, NULL),
('0000', 1, '01', '01', 13, 'T', 3, 'ADDS0000000003', NULL, '1', NULL, NULL, 1, '2017-01-28', 1, '2017-01-28'),
('0000', 1, '01', '04', 3, 'S', 2, 'ADDS0000000003', NULL, '1', NULL, NULL, 1, '2016-11-07', NULL, NULL),
('0000', 1, '01', '04', 3, 'S', 2, 'ADDS0000000003', NULL, '1', NULL, NULL, 1, '2016-11-07', NULL, NULL),
('0000', 1, '01', '04', 3, 'S', 2, 'ADDS0000000003', NULL, '1', NULL, NULL, 1, '2016-11-07', NULL, NULL),
('0000', 1, '01', '04', 12, 'C', 5, 'ADDS0000000003', NULL, '1', NULL, NULL, 1, '2017-01-28', 1, '2017-01-28'),
('0000', 1, '01', '04', 12, 'C', 5, 'ADDS0000000003', NULL, '1', NULL, NULL, 1, '2017-01-28', 1, '2017-01-28'),
('0000', 1, '01', '04', 12, 'C', 5, 'ADDS0000000003', NULL, '1', NULL, NULL, 1, '2017-01-28', 1, '2017-01-28'),
('0000', 1, '01', '01', 10, 'S', 4, 'ADDS0000000003', NULL, '1', NULL, NULL, 1, '2016-11-07', NULL, NULL),
('0000', 1, '01', '04', 0, 'C', 0, NULL, NULL, '1', '1', NULL, 1, '2017-01-30', 1, '2017-01-30'),
('0000', 1, '01', '04', 0, 'C', 0, NULL, NULL, '1', '1', NULL, 1, '2017-01-30', 1, '2017-01-30'),
('0000', 1, '01', '04', 0, 'C', 0, NULL, NULL, '1', '1', NULL, 1, '2017-01-30', 1, '2017-01-30'),
('0000', 1, '01', '01', 0, 'C', 0, NULL, NULL, '1', '1', NULL, 1, '2017-01-30', 1, '2017-01-30'),
('0000', 1, '01', '04', 0, 'C', 0, NULL, NULL, '1', '1', NULL, 1, '2017-01-30', 1, '2017-01-30'),
('0000', 1, '01', '04', 0, 'C', 0, NULL, NULL, '1', '1', NULL, 1, '2017-01-30', 1, '2017-01-30'),
('0000', 1, '01', '04', 0, 'C', 0, NULL, NULL, '1', '1', NULL, 1, '2017-01-30', 1, '2017-01-30'),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000030', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000030', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000030', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000031', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000031', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000031', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '01', 14, 'C', 6, 'ADDS0000000037', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000037', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000037', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000037', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '01', 14, 'C', 6, 'ADDS0000000031', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000035', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000035', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000035', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '01', 14, 'C', 6, 'ADDS0000000032', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '01', 14, 'C', 6, 'ADDS0000000033', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '01', 14, 'C', 6, 'ADDS0000000035', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000036', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000036', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000036', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000032', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000032', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000032', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '01', 14, 'C', 6, 'ADDS0000000036', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000033', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000033', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000033', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000034', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000034', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 14, 'C', 6, 'ADDS0000000034', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '01', 14, 'C', 6, 'ADDS0000000034', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '01', 14, 'C', 6, 'ADDS0000000030', NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$eo$catg`
--

CREATE TABLE `intrm$eo$catg` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(5) NOT NULL,
  `HO_ORG_ID` varchar(2) NOT NULL,
  `EO_TYPE` varchar(5) NOT NULL,
  `EO_CATG_ID` varchar(50) DEFAULT NULL,
  `POS_EO_CATG_ID` int(5) DEFAULT NULL,
  `CATG_NM` varchar(50) DEFAULT NULL,
  `CREATE_FLG` varchar(1) DEFAULT NULL,
  `UPD_FLG` varchar(1) DEFAULT NULL,
  `DEL_FLG` varchar(1) DEFAULT NULL,
  `USR_ID_CREATE` int(10) DEFAULT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(10) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$eo$catg`
--

INSERT INTO `intrm$eo$catg` (`CLD_ID`, `SLOC_ID`, `HO_ORG_ID`, `EO_TYPE`, `EO_CATG_ID`, `POS_EO_CATG_ID`, `CATG_NM`, `CREATE_FLG`, `UPD_FLG`, `DEL_FLG`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`) VALUES
('0000', 1, '01', '68', '387', NULL, 'CONSIGNEE', NULL, NULL, NULL, 1, '2016-11-25', 1, '2016-11-25'),
('0000', 1, '01', '69', '735', NULL, 'Goods', NULL, NULL, NULL, 1, '2016-11-07', 1, '2016-11-07'),
('0000', 1, '01', '69', '735', NULL, 'Goods', NULL, NULL, NULL, 1, '2016-11-07', 1, '2016-11-07'),
('0000', 1, '01', '68', '723', NULL, 'Super Stockist', NULL, NULL, NULL, 1, '2016-11-03', 1, '2016-11-03'),
('0000', 1, '01', '68', '724', NULL, 'Distributer', NULL, NULL, NULL, 1, '2017-01-28', 1, '2017-01-28');

-- --------------------------------------------------------

--
-- Table structure for table `intrm$eo$itm`
--

CREATE TABLE `intrm$eo$itm` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) NOT NULL,
  `ORG_ID` varchar(10) DEFAULT NULL,
  `EO_ID` int(20) DEFAULT NULL,
  `POS_EO_ITM_ID` bigint(20) DEFAULT NULL,
  `SRC_DOC_ID` int(10) DEFAULT NULL,
  `SRC_DOC_TXN_ID` varchar(40) DEFAULT NULL,
  `ITM_ID` varchar(50) DEFAULT NULL,
  `ITM_UOM` varchar(20) DEFAULT NULL,
  `ITM_PRICE` decimal(26,2) DEFAULT NULL,
  `DISC_TYPE` varchar(1) DEFAULT NULL,
  `DISC_VAL` decimal(26,6) DEFAULT NULL,
  `TLRNC_TYPE` varchar(1) DEFAULT NULL,
  `TLRNC_VAL` decimal(26,6) DEFAULT NULL,
  `LEAD_TIME` int(5) DEFAULT NULL,
  `ORD_QTY` decimal(26,6) DEFAULT NULL,
  `ACTV` varchar(1) DEFAULT NULL,
  `INACTV_RESN` varchar(200) DEFAULT NULL,
  `INACTV_DT` date DEFAULT NULL,
  `ENTITY_ID` int(20) DEFAULT NULL,
  `USR_ID_CREATE` int(5) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(5) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL,
  `ITM_UOM_BS` varchar(20) DEFAULT NULL,
  `ITM_PRICE_UOM_BS` decimal(26,6) DEFAULT NULL,
  `UOM_CONV_FCTR` decimal(26,6) DEFAULT NULL,
  `CURR_ID_SP` int(10) DEFAULT NULL,
  `DFLT_SUPP_FLG` varchar(1) DEFAULT NULL,
  `EO_PART_NO` varchar(100) DEFAULT NULL,
  `REMARKS` varchar(300) DEFAULT NULL,
  `EO_ITM_SIZE` varchar(50) DEFAULT NULL,
  `EO_ITM_COLOR_CODE` varchar(50) DEFAULT NULL,
  `TLRNC_QTY_TYPE` varchar(20) DEFAULT NULL,
  `TLRNC_QTY` int(20) DEFAULT NULL,
  `TLRNC_DAY` int(20) DEFAULT NULL,
  `SYNC_FLG` varchar(2) DEFAULT NULL,
  `UPD_FLG` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$eo$itm`
--

INSERT INTO `intrm$eo$itm` (`CLD_ID`, `SLOC_ID`, `ORG_ID`, `EO_ID`, `POS_EO_ITM_ID`, `SRC_DOC_ID`, `SRC_DOC_TXN_ID`, `ITM_ID`, `ITM_UOM`, `ITM_PRICE`, `DISC_TYPE`, `DISC_VAL`, `TLRNC_TYPE`, `TLRNC_VAL`, `LEAD_TIME`, `ORD_QTY`, `ACTV`, `INACTV_RESN`, `INACTV_DT`, `ENTITY_ID`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `ITM_UOM_BS`, `ITM_PRICE_UOM_BS`, `UOM_CONV_FCTR`, `CURR_ID_SP`, `DFLT_SUPP_FLG`, `EO_PART_NO`, `REMARKS`, `EO_ITM_SIZE`, `EO_ITM_COLOR_CODE`, `TLRNC_QTY_TYPE`, `TLRNC_QTY`, `TLRNC_DAY`, `SYNC_FLG`, `UPD_FLG`) VALUES
('0000', 1, '04', 3, NULL, 18504, '0000.01.04.0001.04oS.00.1UKzcXqKQ9', 'AF.0000002', 'UOM0000000063', '340.00', NULL, '0.000000', 'A', '0.000000', 10, '1.000000', 'Y', NULL, NULL, 1, 1, '2016-12-22', 1, '2016-12-22', 'UOM0000000063', '340.000000', '1.000000', 73, 'Y', '1226', NULL, NULL, NULL, 'A', 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$eo$org`
--

CREATE TABLE `intrm$eo$org` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(2) DEFAULT NULL,
  `HO_ORG_ID` varchar(2) DEFAULT NULL,
  `ORG_ID` varchar(20) DEFAULT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `EO_ID` int(20) DEFAULT NULL,
  `EO_ORG_ACTV` varchar(1) NOT NULL,
  `EO_TYPE` varchar(1) NOT NULL,
  `LOYALITY_EO_ID` varchar(20) DEFAULT NULL,
  `POS_EO_ID` int(20) DEFAULT NULL,
  `LOYALITY_POS_EO_ID` varchar(20) DEFAULT NULL,
  `CREATE_FLG` varchar(1) DEFAULT NULL,
  `UPD_FLG` varchar(1) DEFAULT NULL,
  `DEL_FLG` varchar(1) DEFAULT NULL,
  `USR_ID_CREATE` int(4) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(4) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$eo$org`
--

INSERT INTO `intrm$eo$org` (`CLD_ID`, `SLOC_ID`, `HO_ORG_ID`, `ORG_ID`, `WH_ID`, `EO_ID`, `EO_ORG_ACTV`, `EO_TYPE`, `LOYALITY_EO_ID`, `POS_EO_ID`, `LOYALITY_POS_EO_ID`, `CREATE_FLG`, `UPD_FLG`, `DEL_FLG`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`) VALUES
('0000', 1, '01', '04', 'WH00005', 1, '', 'C', '1', 0, NULL, NULL, NULL, NULL, 1, '2016-11-03', NULL, NULL),
('0000', 1, '01', '04', 'WH00005', 9, '', 'C', '9', 0, NULL, NULL, NULL, NULL, 1, '2016-11-25', 1, '2016-11-25'),
('0000', 1, '01', '04', 'WH00004', 10, '', 'S', '10', 4, NULL, '1', NULL, NULL, 1, '2016-11-07', NULL, NULL),
('0000', 1, '01', '04', 'WH00004', 1, '', 'C', '1', 0, NULL, NULL, NULL, NULL, 1, '2016-11-03', NULL, NULL),
('0000', 1, '01', '04', 'WH00004', 13, '', 'T', '13', 3, NULL, '1', NULL, NULL, 1, '2017-01-28', 1, '2017-01-28'),
('0000', 1, '01', '04', 'WH00005', 10, '', 'S', '10', 4, NULL, '1', NULL, NULL, 1, '2016-11-07', NULL, NULL),
('0000', 1, '01', '01', 'WH00001', 10, '', 'S', '10', 4, NULL, '1', NULL, NULL, 1, '2016-11-07', NULL, NULL),
('0000', 1, '01', '04', 'WH00001', 13, '', 'T', '13', 3, NULL, '1', NULL, NULL, 1, '2017-01-28', 1, '2017-01-28'),
('0000', 1, '01', '01', 'WH00001', 9, '', 'C', '9', 0, NULL, NULL, NULL, NULL, 1, '2016-11-25', 1, '2016-11-25'),
('0000', 1, '01', '04', 'WH00001', 10, '', 'S', '10', 4, NULL, '1', NULL, NULL, 1, '2016-11-07', NULL, NULL),
('0000', 1, '01', '04', 'WH00005', 13, '', 'T', '13', 3, NULL, '1', NULL, NULL, 1, '2017-01-28', 1, '2017-01-28'),
('0000', 1, '01', '04', 'WH00004', 12, '', 'C', '12', 5, NULL, '1', NULL, NULL, 1, '2017-01-28', 1, '2017-01-28'),
('0000', 1, '01', '01', 'WH00001', 13, '', 'T', '13', 3, NULL, '1', NULL, NULL, 1, '2017-01-28', 1, '2017-01-28'),
('0000', 1, '01', '04', 'WH00004', 9, '', 'C', '9', 0, NULL, NULL, NULL, NULL, 1, '2016-11-25', 1, '2016-11-25'),
('0000', 1, '01', '04', 'WH00001', 12, '', 'C', '12', 5, NULL, '1', NULL, NULL, 1, '2017-01-28', 1, '2017-01-28'),
('0000', 1, '01', '04', 'WH00001', 1, '', 'C', '1', 0, NULL, NULL, NULL, NULL, 1, '2016-11-03', NULL, NULL),
('0000', 1, '01', '01', 'WH00001', 1, '', 'C', '1', 0, NULL, NULL, NULL, NULL, 1, '2016-11-03', NULL, NULL),
('0000', 1, '01', '04', 'WH00005', 12, '', 'C', '12', 5, NULL, '1', NULL, NULL, 1, '2017-01-28', 1, '2017-01-28'),
('0000', 1, '01', '04', 'WH00005', 3, '', 'S', '3', 2, NULL, '1', NULL, NULL, 1, '2016-11-07', NULL, NULL),
('0000', 1, '01', '04', 'WH00001', 3, '', 'S', '3', 2, NULL, '1', NULL, NULL, 1, '2016-11-07', NULL, NULL),
('0000', 1, '01', '01', 'WH00001', 12, '', 'C', '12', 5, NULL, '1', NULL, NULL, 1, '2017-01-28', 1, '2017-01-28'),
('0000', 1, '01', '01', 'WH00001', 3, '', 'S', '3', 2, NULL, '1', NULL, NULL, 1, '2016-11-07', NULL, NULL),
('0000', 1, '01', '04', 'WH00001', 9, '', 'C', '9', 0, NULL, NULL, NULL, NULL, 1, '2016-11-25', 1, '2016-11-25'),
('0000', 1, '01', '04', 'WH00004', 3, '', 'S', '3', 2, NULL, '1', NULL, NULL, 1, '2016-11-07', NULL, NULL),
('0000', 1, '01', '04', NULL, NULL, '', 'C', NULL, 0, NULL, '1', NULL, NULL, 1, '2017-01-30', 1, '2017-01-30'),
('0000', 1, '01', '04', NULL, NULL, '', 'C', NULL, 0, NULL, '1', NULL, NULL, 1, '2017-01-30', 1, '2017-01-30'),
('0000', 1, '01', '04', NULL, NULL, '', 'C', NULL, 0, NULL, '1', NULL, NULL, 1, '2017-01-30', 1, '2017-01-30'),
('0000', 1, '01', '01', NULL, NULL, '', 'C', NULL, 0, NULL, '1', NULL, NULL, 1, '2017-01-30', 1, '2017-01-30'),
('0000', 1, '01', '04', NULL, NULL, '', 'C', NULL, 0, NULL, '1', NULL, NULL, 1, '2017-01-30', 1, '2017-01-30'),
('0000', 1, '01', '04', NULL, NULL, '', 'C', NULL, 0, NULL, '1', NULL, NULL, 1, '2017-01-30', 1, '2017-01-30'),
('0000', 1, '01', '04', NULL, NULL, '', 'C', NULL, 0, NULL, '1', NULL, NULL, 1, '2017-01-30', 1, '2017-01-30'),
('0000', 1, '01', '04', 'WH00004', 14, '', 'C', '14', 6, NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '01', 'WH00001', 14, '', 'C', '14', 6, NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 'WH00001', 14, '', 'C', '14', 6, NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL),
('0000', 1, '01', '04', 'WH00005', 14, '', 'C', '14', 6, NULL, '1', NULL, NULL, 1, '2017-01-30', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$itm$grp`
--

CREATE TABLE `intrm$itm$grp` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(5) NOT NULL,
  `HO_ORG_ID` varchar(2) NOT NULL,
  `POS_GRP_ID` int(20) DEFAULT NULL,
  `GRP_ID` varchar(20) NOT NULL,
  `GRP_NM` varchar(50) NOT NULL,
  `GRP_ID_PARENT` varchar(20) DEFAULT NULL,
  `USR_ID_CREATE` int(5) NOT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(5) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL,
  `UPD_FLG` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$itm$grp`
--

INSERT INTO `intrm$itm$grp` (`CLD_ID`, `SLOC_ID`, `HO_ORG_ID`, `POS_GRP_ID`, `GRP_ID`, `GRP_NM`, `GRP_ID_PARENT`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `UPD_FLG`) VALUES
('0000', 1, '01', 1, '0011010100005', 'HERBICIDE', '0007010100003', 1, '2016-11-08', NULL, '2016-11-08', NULL),
('0000', 1, '01', 2, '0009010100004', 'WATER SOLUBLE', '0003010100001', 1, '2016-11-08', NULL, '2016-11-08', NULL),
('0000', 1, '01', 3, '0014010100007', 'INSECTICIDE', '0007010100003', 1, '2016-11-08', NULL, '2016-11-08', NULL),
('0000', 1, '01', 4, '0041010100011', 'Software', '0021010100009', 1, '2017-01-17', NULL, '2017-01-17', NULL),
('0000', 1, '01', 5, '0061010100012', 'Fertilizers', '0006010100002', 1, '2017-01-28', NULL, '2017-01-28', NULL),
('0000', 1, '01', 6, '0022010100010', 'Hardware', '0021010100009', 1, '2016-12-19', NULL, '2016-12-19', NULL),
('0000', 1, '01', 7, '0007010100003', 'PESTICIDES', '0003010100001', 1, '2016-11-08', NULL, '2016-11-08', NULL),
('0000', 1, '01', 8, '0021010100009', 'Capital Goods', '0', 1, '2016-12-19', NULL, '2016-12-19', NULL),
('0000', 1, '01', 9, '0015010100008', 'WSF', '0009010100004', 1, '2016-11-08', NULL, '2016-11-08', NULL),
('0000', 1, '01', 10, '0006010100002', 'FERTILISERS', '0003010100001', 1, '2016-11-08', NULL, '2016-11-08', NULL),
('0000', 1, '01', 11, '0012010100006', 'FUNGICIDE', '0007010100003', 1, '2016-11-08', NULL, '2016-11-08', NULL),
('0000', 1, '01', 12, '0003010100001', 'AGRI INPUTS', '0', 1, '2016-11-08', NULL, '2016-11-08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$itm$grp$org`
--

CREATE TABLE `intrm$itm$grp$org` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `HO_ORG_ID` varchar(2) DEFAULT NULL,
  `ORG_ID` varchar(20) DEFAULT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `POS_GRP_ID` int(20) DEFAULT NULL,
  `GRP_ID` varchar(20) DEFAULT NULL,
  `CREATE_FLG` varchar(1) DEFAULT NULL,
  `UPD_FLG` varchar(1) DEFAULT NULL,
  `DEL_FLG` varchar(1) DEFAULT NULL,
  `USR_ID_CREATE` int(5) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(5) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL,
  `TAX_VAL` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$itm$grp$org`
--

INSERT INTO `intrm$itm$grp$org` (`CLD_ID`, `SLOC_ID`, `HO_ORG_ID`, `ORG_ID`, `WH_ID`, `POS_GRP_ID`, `GRP_ID`, `CREATE_FLG`, `UPD_FLG`, `DEL_FLG`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `TAX_VAL`) VALUES
('0000', 1, '01', '01', 'WH00001', 12, '0003010100001', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '01', 'WH00001', 10, '0006010100002', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '01', 'WH00001', 7, '0007010100003', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '01', 'WH00001', 2, '0009010100004', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '01', 'WH00001', 1, '0011010100005', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '01', 'WH00001', 11, '0012010100006', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '01', 'WH00001', 3, '0014010100007', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '01', 'WH00001', 9, '0015010100008', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '01', 'WH00001', 8, '0021010100009', '1', 'N', NULL, 1, '2016-12-19', NULL, '2016-12-19', '10'),
('0000', 1, '01', '01', 'WH00001', 6, '0022010100010', '1', 'N', NULL, 1, '2016-12-19', NULL, '2016-12-19', '10'),
('0000', 1, '01', '01', 'WH00001', 4, '0041010100011', '1', 'N', NULL, 1, '2017-01-17', NULL, '2017-01-17', '10'),
('0000', 1, '01', '01', 'WH00001', 5, '0061010100012', '1', 'N', NULL, 1, '2017-01-28', NULL, '2017-01-28', '10'),
('0000', 1, '01', '04', 'EWH00004', 12, '0003010100001', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00001', 12, '0003010100001', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00002', 12, '0003010100001', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00003', 12, '0003010100001', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00004', 12, '0003010100001', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00005', 12, '0003010100001', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'EWH00004', 10, '0006010100002', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00001', 10, '0006010100002', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00002', 10, '0006010100002', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00003', 10, '0006010100002', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00004', 10, '0006010100002', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00005', 10, '0006010100002', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'EWH00004', 7, '0007010100003', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00001', 7, '0007010100003', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00002', 7, '0007010100003', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00003', 7, '0007010100003', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00004', 7, '0007010100003', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00005', 7, '0007010100003', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'EWH00004', 2, '0009010100004', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00001', 2, '0009010100004', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00002', 2, '0009010100004', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00003', 2, '0009010100004', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00004', 2, '0009010100004', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00005', 2, '0009010100004', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'EWH00004', 1, '0011010100005', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00001', 1, '0011010100005', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00002', 1, '0011010100005', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00003', 1, '0011010100005', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00004', 1, '0011010100005', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00005', 1, '0011010100005', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'EWH00004', 11, '0012010100006', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00001', 11, '0012010100006', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00002', 11, '0012010100006', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00003', 11, '0012010100006', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00004', 11, '0012010100006', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00005', 11, '0012010100006', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'EWH00004', 3, '0014010100007', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00001', 3, '0014010100007', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00002', 3, '0014010100007', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00003', 3, '0014010100007', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00004', 3, '0014010100007', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00005', 3, '0014010100007', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'EWH00004', 9, '0015010100008', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00001', 9, '0015010100008', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00002', 9, '0015010100008', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00003', 9, '0015010100008', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00004', 9, '0015010100008', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'WH00005', 9, '0015010100008', '1', 'N', NULL, 1, '2016-11-08', NULL, '2016-11-08', '10'),
('0000', 1, '01', '04', 'EWH00004', 8, '0021010100009', '1', 'N', NULL, 1, '2016-12-19', NULL, '2016-12-19', '10'),
('0000', 1, '01', '04', 'WH00001', 8, '0021010100009', '1', 'N', NULL, 1, '2016-12-19', NULL, '2016-12-19', '10'),
('0000', 1, '01', '04', 'WH00002', 8, '0021010100009', '1', 'N', NULL, 1, '2016-12-19', NULL, '2016-12-19', '10'),
('0000', 1, '01', '04', 'WH00003', 8, '0021010100009', '1', 'N', NULL, 1, '2016-12-19', NULL, '2016-12-19', '10'),
('0000', 1, '01', '04', 'WH00004', 8, '0021010100009', '1', 'N', NULL, 1, '2016-12-19', NULL, '2016-12-19', '10'),
('0000', 1, '01', '04', 'WH00005', 8, '0021010100009', '1', 'N', NULL, 1, '2016-12-19', NULL, '2016-12-19', '10'),
('0000', 1, '01', '04', 'EWH00004', 6, '0022010100010', '1', 'N', NULL, 1, '2016-12-19', NULL, '2016-12-19', '10'),
('0000', 1, '01', '04', 'WH00001', 6, '0022010100010', '1', 'N', NULL, 1, '2016-12-19', NULL, '2016-12-19', '10'),
('0000', 1, '01', '04', 'WH00002', 6, '0022010100010', '1', 'N', NULL, 1, '2016-12-19', NULL, '2016-12-19', '10'),
('0000', 1, '01', '04', 'WH00003', 6, '0022010100010', '1', 'N', NULL, 1, '2016-12-19', NULL, '2016-12-19', '10'),
('0000', 1, '01', '04', 'WH00004', 6, '0022010100010', '1', 'N', NULL, 1, '2016-12-19', NULL, '2016-12-19', '10'),
('0000', 1, '01', '04', 'WH00005', 6, '0022010100010', '1', 'N', NULL, 1, '2016-12-19', NULL, '2016-12-19', '10'),
('0000', 1, '01', '04', 'EWH00004', 4, '0041010100011', '1', 'N', NULL, 1, '2017-01-17', NULL, '2017-01-17', '10'),
('0000', 1, '01', '04', 'WH00001', 4, '0041010100011', '1', 'N', NULL, 1, '2017-01-17', NULL, '2017-01-17', '10'),
('0000', 1, '01', '04', 'WH00002', 4, '0041010100011', '1', 'N', NULL, 1, '2017-01-17', NULL, '2017-01-17', '10'),
('0000', 1, '01', '04', 'WH00003', 4, '0041010100011', '1', 'N', NULL, 1, '2017-01-17', NULL, '2017-01-17', '10'),
('0000', 1, '01', '04', 'WH00004', 4, '0041010100011', '1', 'N', NULL, 1, '2017-01-17', NULL, '2017-01-17', '10'),
('0000', 1, '01', '04', 'WH00005', 4, '0041010100011', '1', 'N', NULL, 1, '2017-01-17', NULL, '2017-01-17', '10'),
('0000', 1, '01', '04', 'EWH00004', 5, '0061010100012', '1', 'N', NULL, 1, '2017-01-28', NULL, '2017-01-28', '10'),
('0000', 1, '01', '04', 'WH00001', 5, '0061010100012', '1', 'N', NULL, 1, '2017-01-28', NULL, '2017-01-28', '10'),
('0000', 1, '01', '04', 'WH00002', 5, '0061010100012', '1', 'N', NULL, 1, '2017-01-28', NULL, '2017-01-28', '10'),
('0000', 1, '01', '04', 'WH00003', 5, '0061010100012', '1', 'N', NULL, 1, '2017-01-28', NULL, '2017-01-28', '10'),
('0000', 1, '01', '04', 'WH00004', 5, '0061010100012', '1', 'N', NULL, 1, '2017-01-28', NULL, '2017-01-28', '10'),
('0000', 1, '01', '04', 'WH00005', 5, '0061010100012', '1', 'N', NULL, 1, '2017-01-28', NULL, '2017-01-28', '10');

-- --------------------------------------------------------

--
-- Table structure for table `intrm$itm$schm`
--

CREATE TABLE `intrm$itm$schm` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(11) NOT NULL,
  `HO_ORG_ID` varchar(2) NOT NULL,
  `POS_ITM_ID` int(11) NOT NULL,
  `ITM_ID` varchar(20) NOT NULL,
  `DOC_ID` varchar(50) NOT NULL,
  `POS_SCHM_ID` int(11) NOT NULL,
  `SCHM_ID` varchar(20) NOT NULL,
  `SCHM_NM` varchar(50) NOT NULL,
  `SCHM_DESC` varchar(200) NOT NULL,
  `SCHM_TYP` varchar(1) NOT NULL,
  `SCHM_FLG` varchar(20) NOT NULL,
  `GRP_ITM_FLG` varchar(1) NOT NULL,
  `GRP_ID` varchar(50) NOT NULL,
  `POS_GRP_ID` int(11) NOT NULL,
  `ITM_UOM` varchar(20) NOT NULL,
  `POS_ITM_UOM_ID` int(11) NOT NULL,
  `MIN_QTY` int(11) NOT NULL,
  `MAX_QTY` int(11) NOT NULL,
  `MIN_AMT` int(11) NOT NULL,
  `MAX_AMT` int(11) NOT NULL,
  `VALID_FROM` date NOT NULL,
  `VALID_TO` date NOT NULL,
  `SCHM_CALC_TYP` varchar(1) NOT NULL,
  `USR_ID_CREATE` int(11) NOT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(11) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$itm$schm`
--

INSERT INTO `intrm$itm$schm` (`CLD_ID`, `SLOC_ID`, `HO_ORG_ID`, `POS_ITM_ID`, `ITM_ID`, `DOC_ID`, `POS_SCHM_ID`, `SCHM_ID`, `SCHM_NM`, `SCHM_DESC`, `SCHM_TYP`, `SCHM_FLG`, `GRP_ITM_FLG`, `GRP_ID`, `POS_GRP_ID`, `ITM_UOM`, `POS_ITM_UOM_ID`, `MIN_QTY`, `MAX_QTY`, `MIN_AMT`, `MAX_AMT`, `VALID_FROM`, `VALID_TO`, `SCHM_CALC_TYP`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`) VALUES
('0000', 1, '01', 1, 'AF.0000002', '0000.01.01.0001.00X6.00.1UNWPoq2CC', 1, '01SCH000000001', 'BULK DISCOUNT', 'BULK DISCOUNT', 'Y', 'y', 'I', '0006010100002', 1, 'UOM0000000063', 1, 1, 1, 400, 1, '2016-04-01', '2017-03-31', 'A', 1, '2017-01-28', 1, '2017-01-28');

-- --------------------------------------------------------

--
-- Table structure for table `intrm$itm$schm$dtl`
--

CREATE TABLE `intrm$itm$schm$dtl` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(11) NOT NULL,
  `HO_ORG_ID` varchar(2) NOT NULL,
  `POS_ITM_ID` int(11) NOT NULL,
  `ITM_ID` varchar(50) NOT NULL,
  `SR_NO` varchar(20) NOT NULL,
  `DOC_ID` varchar(40) NOT NULL,
  `POS_SCHM_ID` int(11) NOT NULL,
  `FREE_GRP_ID` varchar(50) NOT NULL,
  `FREE_POS_GRP_ID` int(11) NOT NULL,
  `FREE_ITM_ID` varchar(20) NOT NULL,
  `FREE_POS_ITM_ID` int(11) NOT NULL,
  `FREE_ITM_UOM` varchar(20) NOT NULL,
  `FREE_POS_ITM_UOM` int(11) NOT NULL,
  `FREE_ITM_UOM_BS` varchar(20) NOT NULL,
  `FREE_POS_ITM_UOM_BS` int(11) NOT NULL,
  `FREE_ITM_QTY` int(11) NOT NULL,
  `FREE_ITM_PRICE` int(11) NOT NULL,
  `DISC_TYPE` varchar(1) NOT NULL,
  `DISC_VAL` int(11) NOT NULL,
  `FOC_FLG` varchar(1) NOT NULL,
  `CONV_FCTR` int(11) NOT NULL,
  `MIN_AMT` int(11) NOT NULL,
  `MAX_AMT` int(11) NOT NULL,
  `UNLMT_AMT` varchar(1) NOT NULL,
  `DFLT_ITM_FLG` varchar(1) NOT NULL,
  `USR_ID_CREATE` int(11) NOT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(11) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$itm$schm$dtl`
--

INSERT INTO `intrm$itm$schm$dtl` (`CLD_ID`, `SLOC_ID`, `HO_ORG_ID`, `POS_ITM_ID`, `ITM_ID`, `SR_NO`, `DOC_ID`, `POS_SCHM_ID`, `FREE_GRP_ID`, `FREE_POS_GRP_ID`, `FREE_ITM_ID`, `FREE_POS_ITM_ID`, `FREE_ITM_UOM`, `FREE_POS_ITM_UOM`, `FREE_ITM_UOM_BS`, `FREE_POS_ITM_UOM_BS`, `FREE_ITM_QTY`, `FREE_ITM_PRICE`, `DISC_TYPE`, `DISC_VAL`, `FOC_FLG`, `CONV_FCTR`, `MIN_AMT`, `MAX_AMT`, `UNLMT_AMT`, `DFLT_ITM_FLG`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`) VALUES
('0000', 1, '01', 1, 'AF.0000002', '1', '0000.01.01.0001.00X6.00.1UNWPoq2CC', 1, '0006010100002', 1, 'AF.0000002', 1, 'UOM0000000063', 1, 'UOM0000000063', 1, 0, 400, 'A', 10, 'N', 0, 0, 0, 'N', 'Y', 1, '2017-01-28', 1, '2017-01-28');

-- --------------------------------------------------------

--
-- Table structure for table `intrm$itm$stock$dtl`
--

CREATE TABLE `intrm$itm$stock$dtl` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(11) NOT NULL,
  `HO_ORG_ID` varchar(2) NOT NULL,
  `ORG_ID` varchar(20) NOT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `POS_ITM_ID` int(20) DEFAULT NULL,
  `ITM_ID` varchar(50) NOT NULL,
  `SR_NO` varchar(1) DEFAULT NULL,
  `BAR_CODE` varchar(50) DEFAULT NULL,
  `LOT_NO` varchar(50) NOT NULL,
  `UOM_BASIC` varchar(20) NOT NULL,
  `UOM_SP` varchar(20) NOT NULL,
  `conv_FCTF` varchar(20) DEFAULT NULL,
  `PRICE_SLS` int(11) NOT NULL,
  `AVAIL_QTY` int(11) NOT NULL,
  `USR_ID_CREATE` int(11) NOT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(11) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL,
  `USE_QTY` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$itm$stock$dtl`
--

INSERT INTO `intrm$itm$stock$dtl` (`CLD_ID`, `SLOC_ID`, `HO_ORG_ID`, `ORG_ID`, `WH_ID`, `POS_ITM_ID`, `ITM_ID`, `SR_NO`, `BAR_CODE`, `LOT_NO`, `UOM_BASIC`, `UOM_SP`, `conv_FCTF`, `PRICE_SLS`, `AVAIL_QTY`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `USE_QTY`) VALUES
('0000', 1, '01', '04', 'WH00001', NULL, 'ITM.0000002', NULL, NULL, 'LOT#00001', 'UOM0000000063', 'UOM0000000063', NULL, 490, 2000, 1, '2017-01-28', 1, '2017-01-28', NULL),
('0000', 1, '01', '04', 'WH00001', NULL, 'AP02.0000001', NULL, NULL, 'LOT00001', 'UOM0000000063', 'UOM0000000063', NULL, 1090, 500, 1, '2017-01-28', 1, '2017-01-28', NULL),
('0000', 1, '01', '04', 'WH00005', NULL, 'AP02.0000001', NULL, NULL, 'LOT00001', 'UOM0000000063', 'UOM0000000063', NULL, 1090, 100, 1, '2017-01-19', 1, '2017-01-19', NULL),
('0000', 1, '01', '04', 'WH00005', NULL, 'AF.0000003', NULL, NULL, 'LOT00001', 'UOM0000000063', 'UOM0000000063', NULL, 300, 3000, 1, '2017-01-19', 1, '2017-01-19', NULL),
('0000', 1, '01', '04', 'WH00001', NULL, 'AF.0000002', NULL, NULL, 'LOT00001', 'UOM0000000063', 'UOM0000000063', NULL, 400, 1000, 1, '2017-01-19', 1, '2017-01-19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$mm$emrs`
--

CREATE TABLE `intrm$mm$emrs` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(2) DEFAULT NULL,
  `HO_ORG_ID` varchar(2) DEFAULT NULL,
  `ORG_ID` varchar(20) DEFAULT NULL,
  `DOC_ID` varchar(40) DEFAULT NULL,
  `FY_ID` int(5) DEFAULT NULL,
  `EMRS_TYPE` int(5) DEFAULT NULL,
  `EMRS_NO` varchar(40) DEFAULT NULL,
  `EMRS_DT` date DEFAULT NULL,
  `ORG_ID_REQ_TO` varchar(2) DEFAULT NULL,
  `WH_ID_REQ_TO` varchar(20) DEFAULT NULL,
  `REQD_DT` date DEFAULT NULL,
  `EMRS_STAT` int(5) DEFAULT NULL,
  `EMRS_STAT_DT` date DEFAULT NULL,
  `USR_ID_CREATE` int(10) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(10) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL,
  `REMARKS` varchar(300) DEFAULT NULL,
  `EO_ID` int(10) DEFAULT NULL,
  `PRJ_ID` varchar(40) DEFAULT NULL,
  `EMRS_MODE` int(5) DEFAULT NULL,
  `SYNC_FLG` varchar(1) DEFAULT NULL,
  `POS_MM_EMRS_ID` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$mm$emrs`
--

INSERT INTO `intrm$mm$emrs` (`CLD_ID`, `SLOC_ID`, `HO_ORG_ID`, `ORG_ID`, `DOC_ID`, `FY_ID`, `EMRS_TYPE`, `EMRS_NO`, `EMRS_DT`, `ORG_ID_REQ_TO`, `WH_ID_REQ_TO`, `REQD_DT`, `EMRS_STAT`, `EMRS_STAT_DT`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `REMARKS`, `EO_ID`, `PRJ_ID`, `EMRS_MODE`, `SYNC_FLG`, `POS_MM_EMRS_ID`) VALUES
('0000', 1, '01', '04', '0000.01.04.0001.0000.00.1UKzcblKyn', 1, 1080, 'EMRS00004', '2016-12-22', '04', 'WH00002', '2016-12-22', 1079, NULL, 1, '2016-12-22', 1, '2016-12-22', 'NA', 10, 'PROJ0000', 1038, NULL, 4),
('0000', 1, '01', '04', '0000.01.04.0001.0000.00.1UNWFzBjoZ', 1, 1080, 'EMRS00006', '2017-01-19', '04', 'WH00002', '2017-01-19', 1079, NULL, 1, '2017-01-19', 1, '2017-01-19', 'na', 10, 'PROJ0000', 1038, NULL, 5),
('0000', 1, '01', '04', '0000.01.04.0001.0000.00.1UNWPrUrHV', 1, 1080, 'EMRS00008', '2017-01-28', '04', 'WH00002', '2017-01-28', 1079, NULL, 1, '2017-01-28', 1, '2017-01-28', 'n', 3, 'PROJ0000', 1038, NULL, 1),
('0000', 1, '01', '04', '0000.01.04.0001.0000.00.1UNWFzEjrx', 1, 1081, 'EMRS00007', '2017-01-19', '04', 'WH00002', '2017-01-19', 1079, NULL, 1, '2017-01-19', 1, '2017-01-19', 'A', 10, 'PROJ0000', 1038, NULL, 6),
('0000', 1, '01', '04', '0000.01.04.0001.0000.00.1UNWS2PlfN', 1, 1081, 'EMRS00009', '2017-01-30', '04', 'WH00003', '2017-01-30', 1079, NULL, 1, '2017-01-30', 1, '2017-01-30', 'qwerty', 10, 'PROJ0000', 1038, NULL, 7),
('0000', 1, '01', '04', '0000.01.04.0001.0000.00.1UNWS2Svxp', 1, 1080, 'EMRS00010', '2017-01-30', '04', 'WH00002', '2017-01-30', 1079, NULL, 1, '2017-01-30', 1, '2017-01-30', 'EMRS', 10, 'PROJ0000', 1038, NULL, 8),
('0000', 1, '01', '04', '0000.01.04.0001.0000.00.1UKzcZD9Vn', 1, 1080, 'EMRS00002', '2016-12-22', '04', 'WH00002', '2016-12-22', 1079, NULL, 1, '2016-12-22', 1, NULL, 'r', 3, 'PROJ0000', NULL, NULL, 2),
('0000', 1, '01', '04', '0000.01.04.0001.0000.00.1UKzbVVDaX', 1, 1080, 'EMRS00001', '2016-12-21', '04', 'WH00002', '2016-12-21', 1079, NULL, 1, '2016-12-21', 1, '2016-12-21', 'in', 3, 'PROJ0000', NULL, NULL, 3),
('0000', 1, '01', '04', '0000.01.04.0001.0000.00.1UNWS4PzzY', 1, 1081, 'EMRS00013', '2017-01-30', '04', 'EWH00004', '2017-01-30', 1079, NULL, 1, '2017-01-30', 1, '2017-01-30', 'EMRS4', 10, 'PROJ0000', 1038, NULL, 9),
('0000', 1, '01', '04', '0000.01.04.0001.0000.00.1UNWS4PYhQ', 1, 1080, 'EMRS00012', '2017-01-30', '04', 'EWH00004', '2017-01-30', 1079, NULL, 1, '2017-01-30', 1, '2017-01-30', 'EMRS3', 10, 'PROJ0000', 1038, NULL, 10);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$mm$emrs$doc$src`
--

CREATE TABLE `intrm$mm$emrs$doc$src` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(2) DEFAULT NULL,
  `HO_ORG_ID` varchar(2) DEFAULT NULL,
  `ORG_ID` varchar(20) DEFAULT NULL,
  `DOC_ID` varchar(40) DEFAULT NULL,
  `DOC_NO_SRC` varchar(40) DEFAULT NULL,
  `DOC_DT_SRC` date DEFAULT NULL,
  `USR_ID_CREATE` int(10) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(10) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL,
  `SYNC_FLG` varchar(1) DEFAULT NULL,
  `POS_MM_EMRS_DOC_SRC_ID` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$mm$emrs$doc$src`
--

INSERT INTO `intrm$mm$emrs$doc$src` (`CLD_ID`, `SLOC_ID`, `HO_ORG_ID`, `ORG_ID`, `DOC_ID`, `DOC_NO_SRC`, `DOC_DT_SRC`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `SYNC_FLG`, `POS_MM_EMRS_DOC_SRC_ID`) VALUES
('0000', 1, '01', '04', '0000.01.04.0001.0000.00.1UKzi1LLoj', '324', '2016-12-27', 1, '2016-12-27', NULL, NULL, NULL, 0),
('0000', 1, '01', '04', '0000.01.04.0001.0000.00.1UKzcZD9Vn', 'er', '2016-12-22', 1, '2016-12-22', NULL, NULL, NULL, 1),
('0000', 1, '01', '04', '0000.01.04.0001.0000.00.1UKzbVVDaX', '1515', '2016-12-21', 1, '2016-12-21', NULL, NULL, NULL, 2),
('0000', 1, '01', '04', '0000.01.04.0001.0000.00.1UNWFzEjrx', 'CBMZ03000655OP', '2017-01-19', 1, '2017-01-19', NULL, NULL, NULL, 3),
('0000', 1, '01', '04', '0000.01.04.0001.0000.00.1UNWFzBjoZ', 'CBMZ03000655', '2017-01-18', 1, '2017-01-19', NULL, NULL, NULL, 4),
('0000', 1, '01', '04', '0000.01.04.0001.0000.00.1UKzcblKyn', '56465', '2016-12-22', 1, '2016-12-22', NULL, NULL, NULL, 5),
('0000', 1, '01', '04', '0000.01.04.0001.0000.00.1UKzcZGM6f', '34253', '2016-12-22', 1, '2016-12-22', NULL, NULL, NULL, 0),
('0000', 1, '01', '04', '0000.01.04.0001.0000.00.1UNWPrUrHV', 'STO11', '2017-01-28', 1, '2017-01-28', NULL, NULL, NULL, 6),
('0000', 1, '01', '04', '0000.01.04.0001.0000.00.1UNWS2PlfN', 'EMRS001', '2017-01-30', 1, '2017-01-30', NULL, NULL, NULL, 7),
('0000', 1, '01', '04', '0000.01.04.0001.0000.00.1UNWS2Svxp', 'EMRS0002', '2017-01-30', 1, '2017-01-30', NULL, NULL, NULL, 8),
('0000', 1, '01', '04', '0000.01.04.0001.0000.00.1UNWS4PYhQ', 'EMRS003', '2017-01-30', 1, '2017-01-30', NULL, NULL, NULL, 9),
('0000', 1, '01', '04', '0000.01.04.0001.0000.00.1UNWS4PzzY', 'EMRS004', '2017-01-30', 1, '2017-01-30', NULL, NULL, NULL, 10),
('0000', 1, '01', '04', '0000.01.04.0001.0000.00.1UNWS4LQR3', 'EMRS0003', '2017-01-30', 1, '2017-01-30', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$mm$emrs$itm`
--

CREATE TABLE `intrm$mm$emrs$itm` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(2) DEFAULT NULL,
  `HO_ORG_ID` varchar(2) DEFAULT NULL,
  `ORG_ID` varchar(20) DEFAULT NULL,
  `WH_ID_REQ_TO` varchar(20) DEFAULT NULL,
  `DOC_ID` varchar(40) DEFAULT NULL,
  `DOC_NO_SRC` varchar(40) DEFAULT NULL,
  `ITM_ID` varchar(50) DEFAULT NULL,
  `ITM_UOM` varchar(20) DEFAULT NULL,
  `REQ_QTY` decimal(26,6) DEFAULT NULL,
  `ITM_UOM_BS` varchar(20) DEFAULT NULL,
  `UOM_CONV_FCTR` decimal(26,6) DEFAULT NULL,
  `REQ_QTY_BS` decimal(26,6) DEFAULT NULL,
  `ITM_PRICE_BS` decimal(26,6) DEFAULT NULL,
  `ITM_AMT_BS` decimal(26,6) DEFAULT NULL,
  `LINE_SEQ_NO` decimal(26,6) DEFAULT NULL,
  `SYNC_FLG` varchar(1) DEFAULT NULL,
  `POS_MM_EMRS_ITM_ID` int(5) DEFAULT NULL,
  `EMRS_STAT` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$mm$emrs$itm`
--

INSERT INTO `intrm$mm$emrs$itm` (`CLD_ID`, `SLOC_ID`, `HO_ORG_ID`, `ORG_ID`, `WH_ID_REQ_TO`, `DOC_ID`, `DOC_NO_SRC`, `ITM_ID`, `ITM_UOM`, `REQ_QTY`, `ITM_UOM_BS`, `UOM_CONV_FCTR`, `REQ_QTY_BS`, `ITM_PRICE_BS`, `ITM_AMT_BS`, `LINE_SEQ_NO`, `SYNC_FLG`, `POS_MM_EMRS_ITM_ID`, `EMRS_STAT`) VALUES
('0000', 1, '01', '04', NULL, '0000.01.04.0001.0000.00.1UNWFzBjoZ', 'CBMZ03000655', 'AP02.0000001', 'UOM0000000063', '5000.000000', NULL, NULL, '0.000000', NULL, NULL, '103.000000', NULL, 9, 1079),
('0000', 1, '01', '04', NULL, '0000.01.04.0001.0000.00.1UKzcblKyn', '56465', 'AF.0000001', 'UOM0000000063', '2000.000000', NULL, NULL, '0.000000', NULL, NULL, '45.000000', NULL, 1, 1079),
('0000', 1, '01', '04', NULL, '0000.01.04.0001.0000.00.1UNWFzBjoZ', 'CBMZ03000655', 'AF.0000004', 'UOM0000000063', '2000.000000', NULL, NULL, '0.000000', NULL, NULL, '102.000000', NULL, 7, 1079),
('0000', 1, '01', '04', NULL, '0000.01.04.0001.0000.00.1UNWPrUrHV', 'STO11', 'AF.0000001', 'UOM0000000063', '2000.000000', NULL, NULL, '0.000000', NULL, NULL, '121.000000', NULL, 2, 1079),
('0000', 1, '01', '04', NULL, '0000.01.04.0001.0000.00.1UNWFzBjoZ', 'CBMZ03000655', 'AF.0000001', 'UOM0000000063', '1000.000000', NULL, NULL, '0.000000', NULL, NULL, '101.000000', NULL, 3, 1079),
('0000', 1, '01', '04', NULL, '0000.01.04.0001.0000.00.1UNWFzEjrx', 'CBMZ03000655OP', 'AF.0000004', 'UOM0000000063', '100.000000', NULL, NULL, '0.000000', NULL, NULL, '104.000000', NULL, 8, 1079),
('0000', 1, '01', '04', NULL, '0000.01.04.0001.0000.00.1UKzcblKyn', '56465', 'AF.0000002', 'UOM0000000063', '200.000000', NULL, NULL, '0.000000', NULL, NULL, '46.000000', NULL, 4, 1079),
('0000', 1, '01', '04', NULL, '0000.01.04.0001.0000.00.1UNWPrUrHV', 'STO11', 'AF.0000005', 'UOM0000000063', '200.000000', NULL, NULL, '0.000000', NULL, NULL, '122.000000', NULL, 14, 1079),
('0000', 1, '01', '04', NULL, '0000.01.04.0001.0000.00.1UNWS2PlfN', 'EMRS001', 'ITM.0000003', 'UOM0000000063', '5.000000', NULL, NULL, '0.000000', NULL, NULL, '143.000000', NULL, 13, 1079),
('0000', 1, '01', '04', NULL, '0000.01.04.0001.0000.00.1UNWS2Svxp', 'EMRS0002', 'ITM.0000003', 'UOM0000000063', '5.000000', NULL, NULL, '0.000000', NULL, NULL, '144.000000', NULL, 12, 1079),
('0000', 1, '01', '04', NULL, '0000.01.04.0001.0000.00.1UKzbVVDaX', '1515', 'AF.0000003', 'UOM0000000063', '200.000000', NULL, NULL, '0.000000', NULL, NULL, '22.000000', NULL, 6, 1079),
('0000', 1, '01', '04', NULL, '0000.01.04.0001.0000.00.1UKzbVVDaX', '1515', 'AF.0000002', 'UOM0000000063', '100.000000', NULL, NULL, '0.000000', NULL, NULL, '21.000000', NULL, 5, 1079),
('0000', 1, '01', '04', NULL, '0000.01.04.0001.0000.00.1UNWS4PzzY', 'EMRS004', 'ITM.0000003', 'UOM0000000063', '20.000000', NULL, NULL, '0.000000', NULL, NULL, '147.000000', NULL, 10, 1079),
('0000', 1, '01', '04', NULL, '0000.01.04.0001.0000.00.1UNWS4PYhQ', 'EMRS003', 'ITM.0000003', 'UOM0000000063', '20.000000', NULL, NULL, '0.000000', NULL, NULL, '146.000000', NULL, 11, 1079);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$mm$estk$summ$bin`
--

CREATE TABLE `intrm$mm$estk$summ$bin` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `ORG_ID` varchar(20) DEFAULT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `POS_BIN_ID` bigint(20) DEFAULT NULL,
  `ITM_ID` varchar(50) DEFAULT NULL,
  `LOT_ID` varchar(20) DEFAULT NULL,
  `BIN_ID` varchar(20) DEFAULT NULL,
  `ITM_UOM_BS` varchar(20) DEFAULT NULL,
  `TOT_STK` decimal(26,6) DEFAULT NULL,
  `USR_ID_MOD` int(10) DEFAULT NULL,
  `USR_ID_MOD_DT` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `REJ_STK` decimal(26,6) DEFAULT NULL,
  `RWK_STK` decimal(26,6) DEFAULT NULL,
  `REJ_CNT_QTY` decimal(26,6) DEFAULT NULL,
  `RWK_CNT_QTY` decimal(26,6) DEFAULT NULL,
  `CNT_QTY` decimal(26,6) DEFAULT NULL,
  `CREATE_FLG` varchar(2) DEFAULT NULL,
  `UPD_FLG` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$mm$estk$summ$itm`
--

CREATE TABLE `intrm$mm$estk$summ$itm` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(5) NOT NULL,
  `ORG_ID` varchar(20) NOT NULL,
  `WH_ID` varchar(20) NOT NULL,
  `POS_STK_ITM_ID` bigint(20) DEFAULT NULL,
  `FY_ID` int(5) NOT NULL,
  `ITM_ID` varchar(50) NOT NULL,
  `ITM_UOM_BS` varchar(20) NOT NULL,
  `OP_STK` decimal(26,6) NOT NULL,
  `OP_AVL_STK` decimal(26,6) DEFAULT NULL,
  `OP_REQ_STK` decimal(26,6) DEFAULT NULL,
  `OP_ORD_STK` decimal(26,6) DEFAULT NULL,
  `OP_REJ_STK` decimal(26,6) DEFAULT NULL,
  `TOT_STK` decimal(26,6) DEFAULT NULL,
  `AVL_STK` decimal(26,6) DEFAULT NULL,
  `REQ_STK` decimal(26,6) NOT NULL,
  `ORD_STK` decimal(26,6) NOT NULL,
  `REJ_STK` decimal(26,6) NOT NULL,
  `USR_ID_MOD` int(10) NOT NULL,
  `USR_ID_MOD_DT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `OP_SCRP_STK` decimal(26,6) NOT NULL,
  `OP_RWK_STK` decimal(26,6) NOT NULL,
  `SCRP_STK` decimal(26,6) NOT NULL,
  `RWK_STK` decimal(26,6) NOT NULL,
  `MAP_PRICE` decimal(26,6) NOT NULL,
  `WAP_PRICE` decimal(26,6) NOT NULL,
  `STD_VAL` decimal(26,6) NOT NULL,
  `MAP_VAL` decimal(26,6) NOT NULL,
  `WAP_VAL` decimal(26,6) NOT NULL,
  `LIFO_VAL` decimal(26,6) NOT NULL,
  `FIFO_VAL` decimal(26,6) NOT NULL,
  `CUM_TOT_REJ_QTY` decimal(26,6) DEFAULT NULL,
  `CUM_TOT_RWK_QTY` decimal(26,6) DEFAULT NULL,
  `CUM_TOT_SCRP_QTY` decimal(26,6) DEFAULT NULL,
  `CUM_TOT_RCPT_QTY` decimal(26,6) DEFAULT NULL,
  `CUM_TOT_RCPT_LND_VAL` decimal(26,6) DEFAULT NULL,
  `CUM_TOT_ISSU_QTY` decimal(26,6) DEFAULT NULL,
  `CUM_TOT_ISSU_LND_VAL` decimal(26,6) DEFAULT NULL,
  `LIFO_PRICE` decimal(26,6) DEFAULT NULL,
  `FIFO_PRICE` decimal(26,6) DEFAULT NULL,
  `GIT_STK` decimal(26,6) DEFAULT NULL,
  `REJ_CNT_QTY` decimal(26,6) DEFAULT NULL,
  `RWK_CNT_QTY` decimal(26,6) DEFAULT NULL,
  `CNT_QTY` decimal(26,6) DEFAULT NULL,
  `CREATE_FLG` varchar(2) DEFAULT NULL,
  `UPD_FLG` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$mm$estk$summ$lot`
--

CREATE TABLE `intrm$mm$estk$summ$lot` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `ORG_ID` varchar(20) DEFAULT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `POS_STK_LOT_ID` bigint(20) DEFAULT NULL,
  `ITM_ID` varchar(50) DEFAULT NULL,
  `LOT_ID` varchar(20) DEFAULT NULL,
  `ITM_UOM_BS` varchar(20) DEFAULT NULL,
  `OP_STK` decimal(26,6) DEFAULT NULL,
  `TOT_STK` decimal(26,6) DEFAULT NULL,
  `BASIC_PRICE` decimal(26,6) DEFAULT NULL,
  `LND_PRICE` decimal(26,6) DEFAULT NULL,
  `LND_VAL` decimal(26,6) DEFAULT NULL,
  `WARRANTY_DT` date DEFAULT NULL,
  `EXPIRY_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(10) DEFAULT NULL,
  `USR_ID_MOD_DT` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LOT_DT` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `SCRP_STK` decimal(26,6) DEFAULT NULL,
  `REJ_STK` decimal(26,6) DEFAULT NULL,
  `RWK_STK` decimal(26,6) DEFAULT NULL,
  `LOT_PRICE_OP` decimal(26,6) DEFAULT NULL,
  `LOT_PRICE_CRNT` decimal(26,6) DEFAULT NULL,
  `MFG_DT` date DEFAULT NULL,
  `BATCH_NO` varchar(20) DEFAULT NULL,
  `REJ_CNT_QTY` decimal(26,6) DEFAULT NULL,
  `RWK_CNT_QTY` decimal(26,6) DEFAULT NULL,
  `CNT_QTY` decimal(26,6) DEFAULT NULL,
  `REMARKS` varchar(4000) DEFAULT NULL,
  `CREATE_FLG` varchar(2) DEFAULT NULL,
  `UPD_FLG` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$mm$estk$summ$sr`
--

CREATE TABLE `intrm$mm$estk$summ$sr` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `ORG_ID` varchar(20) DEFAULT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `ITM_ID` varchar(50) DEFAULT NULL,
  `LOT_ID` varchar(20) DEFAULT NULL,
  `BIN_ID` varchar(20) DEFAULT NULL,
  `SR_NO` varchar(100) DEFAULT NULL,
  `TOT_STK` decimal(26,6) DEFAULT NULL,
  `USR_ID_MOD` int(10) DEFAULT NULL,
  `USR_ID_MOD_DT` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `SCRP_STK` decimal(26,6) DEFAULT NULL,
  `REJ_STK` decimal(26,6) DEFAULT NULL,
  `RWK_STK` decimal(26,6) DEFAULT NULL,
  `SR_NO1` varchar(100) DEFAULT NULL,
  `SR_NO2` varchar(100) DEFAULT NULL,
  `REJ_CNT_QTY` decimal(26,6) DEFAULT NULL,
  `RWK_CNT_QTY` decimal(26,6) DEFAULT NULL,
  `CNT_QTY` decimal(26,6) DEFAULT NULL,
  `UPD_FLG` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$mm$mtl$rcpt`
--

CREATE TABLE `intrm$mm$mtl$rcpt` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `ORG_ID` varchar(2) DEFAULT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `POS_MTL_RCPT_ID` bigint(20) DEFAULT NULL,
  `DOC_ID` varchar(40) DEFAULT NULL,
  `DOC_DT` timestamp(6) NULL DEFAULT NULL,
  `FY_ID` int(5) DEFAULT NULL,
  `RCPT_NO` varchar(20) DEFAULT NULL,
  `RCPT_DT` date DEFAULT NULL,
  `RCPT_SRC_TYPE` int(5) DEFAULT NULL,
  `RCPT_STG` int(5) DEFAULT NULL,
  `GE_DOC_ID` varchar(40) DEFAULT NULL,
  `EO_ID_SRC` int(5) DEFAULT NULL,
  `RQMT_AREA_ID_SRC` int(5) DEFAULT NULL,
  `ORG_ID_SRC` varchar(2) DEFAULT NULL,
  `WH_ID_SRC` varchar(20) DEFAULT NULL,
  `INVOICE_NO_SRC` varchar(100) DEFAULT NULL,
  `INVOICE_DT_SRC` date DEFAULT NULL,
  `DN_NO_SRC` varchar(100) DEFAULT NULL,
  `DN_DT_SRC` date DEFAULT NULL,
  `EO_ID_TPT` int(5) DEFAULT NULL,
  `TPT_BILL_NO` varchar(30) DEFAULT NULL,
  `TPT_BILL_DT` date DEFAULT NULL,
  `VEHICLE_NO` varchar(30) DEFAULT NULL,
  `RCPT_STAT` int(5) DEFAULT NULL,
  `INVOICE_REQD` varchar(2) DEFAULT NULL,
  `QC_DONE_FLG` varchar(2) DEFAULT NULL,
  `AUTH_STAT` varchar(2) DEFAULT NULL,
  `USR_ID_CREATE` int(4) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(20) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL,
  `ADDL_AMT` int(26) DEFAULT NULL,
  `CURR_ID_SP` int(10) DEFAULT NULL,
  `RMDA_STAT` varchar(1) DEFAULT NULL,
  `REMARKS` varchar(4000) DEFAULT NULL,
  `COA_ID` int(10) DEFAULT NULL,
  `CURR_CONV_FCTR` int(26) DEFAULT NULL,
  `RCPT_FRZ` varchar(1) DEFAULT NULL,
  `FILE_NO` varchar(40) DEFAULT NULL,
  `PRJ_ID` varchar(40) DEFAULT NULL,
  `SUPP_INVC_RCVD_ORIG` varchar(1) DEFAULT NULL,
  `SUPP_INVC_RCVD_TPT` varchar(1) DEFAULT NULL,
  `SUPP_INVC_RCVD_ORIG_DT` date DEFAULT NULL,
  `SUPP_INVC_RCVD_TPT_DT` date DEFAULT NULL,
  `TPT_DTL` varchar(500) DEFAULT NULL,
  `DOC_ID_SHPMT` varchar(40) DEFAULT NULL,
  `GROSS_WT` int(26) DEFAULT NULL,
  `EMPTY_WT` int(26) DEFAULT NULL,
  `NET_WT` int(26) DEFAULT NULL,
  `SYNC_FLG` varchar(2) DEFAULT NULL,
  `UPD_FLG` varchar(2) DEFAULT NULL,
  `SN` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$mm$mtl$rcpt`
--

INSERT INTO `intrm$mm$mtl$rcpt` (`CLD_ID`, `SLOC_ID`, `ORG_ID`, `WH_ID`, `POS_MTL_RCPT_ID`, `DOC_ID`, `DOC_DT`, `FY_ID`, `RCPT_NO`, `RCPT_DT`, `RCPT_SRC_TYPE`, `RCPT_STG`, `GE_DOC_ID`, `EO_ID_SRC`, `RQMT_AREA_ID_SRC`, `ORG_ID_SRC`, `WH_ID_SRC`, `INVOICE_NO_SRC`, `INVOICE_DT_SRC`, `DN_NO_SRC`, `DN_DT_SRC`, `EO_ID_TPT`, `TPT_BILL_NO`, `TPT_BILL_DT`, `VEHICLE_NO`, `RCPT_STAT`, `INVOICE_REQD`, `QC_DONE_FLG`, `AUTH_STAT`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `ADDL_AMT`, `CURR_ID_SP`, `RMDA_STAT`, `REMARKS`, `COA_ID`, `CURR_CONV_FCTR`, `RCPT_FRZ`, `FILE_NO`, `PRJ_ID`, `SUPP_INVC_RCVD_ORIG`, `SUPP_INVC_RCVD_TPT`, `SUPP_INVC_RCVD_ORIG_DT`, `SUPP_INVC_RCVD_TPT_DT`, `TPT_DTL`, `DOC_ID_SHPMT`, `GROSS_WT`, `EMPTY_WT`, `NET_WT`, `SYNC_FLG`, `UPD_FLG`, `SN`) VALUES
('0000', 1, '04', 'WH00005', 1, NULL, NULL, 1, '2/MRN/1', '2017-01-31', 1, NULL, '', 3, NULL, NULL, NULL, NULL, NULL, '', '1970-01-01', 13, '', '1970-01-01', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, 73, '0', NULL, NULL, NULL, NULL, NULL, '0000.01.04.0001.00XR.00.1UKxc4ge73', NULL, NULL, NULL, '2017-01-31', NULL, NULL, NULL, NULL, NULL, '1', '5', 1),
('0000', 1, '04', 'WH00005', 2, NULL, NULL, 1, '2/MRN/1', '2017-01-31', 1, NULL, '', 3, NULL, NULL, NULL, NULL, NULL, '', '1970-01-01', 13, '', '1970-01-01', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 33, 73, '0', NULL, NULL, NULL, NULL, NULL, '0000.01.04.0001.00XR.00.1UKxc4ge73', NULL, NULL, NULL, '2017-01-31', NULL, NULL, NULL, NULL, NULL, '1', '5', 2);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$mm$mtl$rcpt$bin`
--

CREATE TABLE `intrm$mm$mtl$rcpt$bin` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `ORG_ID` varchar(2) DEFAULT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `POS_MTL_RCPT_BIN_ID` bigint(20) DEFAULT NULL,
  `LOT_SRC_ID` bigint(20) DEFAULT NULL,
  `DOC_ID` varchar(40) DEFAULT NULL,
  `DOC_ID_SRC` varchar(40) DEFAULT NULL,
  `DOC_DT_SRC` date DEFAULT NULL,
  `DLV_SCHDL_NO` int(5) DEFAULT NULL,
  `LOT_ID` varchar(20) DEFAULT NULL,
  `BIN_ID` varchar(20) DEFAULT NULL,
  `ITM_ID` varchar(50) DEFAULT NULL,
  `ITM_UOM` varchar(20) DEFAULT NULL,
  `BIN_QTY` decimal(26,6) DEFAULT NULL,
  `ITM_UOM_BS` varchar(20) DEFAULT NULL,
  `BIN_QTY_BS` decimal(26,6) DEFAULT NULL,
  `REJ_QTY_SP` decimal(26,6) DEFAULT NULL,
  `REJ_QTY_BS` decimal(26,6) DEFAULT NULL,
  `RWK_QTY_SP` decimal(26,6) DEFAULT NULL,
  `RWK_QTY_BS` decimal(26,6) DEFAULT NULL,
  `SCRP_QTY` decimal(26,6) DEFAULT NULL,
  `SCRP_QTY_BS` decimal(26,6) DEFAULT NULL,
  `CNT_QTY` decimal(26,6) DEFAULT NULL,
  `REJ_CNT_QTY` decimal(26,6) DEFAULT NULL,
  `RWK_CNT_QTY` decimal(26,6) DEFAULT NULL,
  `SYNC_FLG` varchar(2) DEFAULT NULL,
  `UPD_FLG` varchar(2) DEFAULT NULL,
  `SN` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$mm$mtl$rcpt$bin`
--

INSERT INTO `intrm$mm$mtl$rcpt$bin` (`CLD_ID`, `SLOC_ID`, `ORG_ID`, `WH_ID`, `POS_MTL_RCPT_BIN_ID`, `LOT_SRC_ID`, `DOC_ID`, `DOC_ID_SRC`, `DOC_DT_SRC`, `DLV_SCHDL_NO`, `LOT_ID`, `BIN_ID`, `ITM_ID`, `ITM_UOM`, `BIN_QTY`, `ITM_UOM_BS`, `BIN_QTY_BS`, `REJ_QTY_SP`, `REJ_QTY_BS`, `RWK_QTY_SP`, `RWK_QTY_BS`, `SCRP_QTY`, `SCRP_QTY_BS`, `CNT_QTY`, `REJ_CNT_QTY`, `RWK_CNT_QTY`, `SYNC_FLG`, `UPD_FLG`, `SN`) VALUES
('0000', 1, '04', 'WH00005', 1, 1, NULL, '0000.01.04.0001.04oS.00.1UNWIBA7gH', '2017-01-21', NULL, '2-LOTC9FJ48', 'BIN00006', 'AP01.0000001', NULL, '55.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '5', 1),
('0000', 1, '04', 'WH00005', 2, 2, NULL, '0000.01.04.0001.04oS.00.1UNWIBA7gH', '2017-01-21', NULL, '2-LOT80B5B7', 'BIN00006', 'AP02.0000001', NULL, '20.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '5', 2),
('0000', 1, '04', 'WH00005', 3, 3, NULL, '0000.01.04.0001.04oS.00.1UNWIBA7gH', '2017-01-21', NULL, '2-LOT1B004F', 'BIN00006', 'AP01.0000001', NULL, '20.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '5', 3);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$mm$mtl$rcpt$itm`
--

CREATE TABLE `intrm$mm$mtl$rcpt$itm` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(2) DEFAULT NULL,
  `ORG_ID` varchar(2) DEFAULT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `POS_MTL_RCPT_ITM_ID` varchar(2) DEFAULT NULL,
  `SRC_RCPT_ID` bigint(20) DEFAULT NULL,
  `DOC_ID` varchar(40) DEFAULT NULL,
  `DOC_ID_SRC` varchar(40) DEFAULT NULL,
  `DOC_DT_SRC` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `DLV_SCHDL_NO` int(5) DEFAULT NULL,
  `ITM_ID` varchar(50) DEFAULT NULL,
  `ITM_UOM` varchar(20) DEFAULT NULL,
  `PEND_QTY` decimal(26,6) DEFAULT NULL,
  `DLV_NOTE_QTY` decimal(26,6) DEFAULT NULL,
  `FOC_FLG` varchar(1) DEFAULT NULL,
  `RWK_QTY` decimal(26,6) DEFAULT NULL,
  `REJ_QTY` decimal(26,6) DEFAULT NULL,
  `RCPT_QTY` decimal(26,6) DEFAULT NULL,
  `REJ_RESN` text,
  `QC_REQD_FLG` varchar(1) DEFAULT NULL,
  `QC_DONE_FLG` varchar(1) DEFAULT NULL,
  `QC_TXN_ID` varchar(40) DEFAULT NULL,
  `QC_RWK_QTY` decimal(26,6) DEFAULT NULL,
  `QC_REJ_QTY` decimal(26,6) DEFAULT NULL,
  `QC_REJ_RESN` text,
  `FINAL_RCPT_QTY` decimal(26,6) DEFAULT NULL,
  `ITM_UOM_BS` varchar(20) DEFAULT NULL,
  `UOM_CONV_FCTR` decimal(26,6) DEFAULT NULL,
  `RWK_QTY_BS` decimal(26,6) DEFAULT NULL,
  `REJ_QTY_BS` decimal(26,6) DEFAULT NULL,
  `RCPT_QTY_BS` decimal(26,6) DEFAULT NULL,
  `QC_RWK_QTY_BS` decimal(26,6) DEFAULT NULL,
  `QC_REJ_QTY_BS` decimal(26,6) DEFAULT NULL,
  `FINAL_RCPT_QTY_BS` decimal(26,6) DEFAULT NULL,
  `LND_PRICE` decimal(26,6) DEFAULT NULL,
  `QC_OVERRIDE_FLG` varchar(1) DEFAULT NULL,
  `QC_OVERRIDE_RESN` text,
  `PUR_PRICE` decimal(26,6) DEFAULT NULL,
  `PUR_PRICE_BS` decimal(26,6) DEFAULT NULL,
  `LND_PRICE_BS` decimal(26,6) DEFAULT NULL,
  `TOT_RCPT_QTY` decimal(26,6) DEFAULT NULL,
  `TOT_RCPT_QTY_BS` decimal(26,6) DEFAULT NULL,
  `DISC_AMT_SP` decimal(26,6) DEFAULT NULL,
  `DISC_AMT_BS` decimal(26,6) DEFAULT NULL,
  `TAXABLE_AMT_SP` decimal(26,6) DEFAULT NULL,
  `TAXABLE_AMT_BS` decimal(26,6) DEFAULT NULL,
  `TOT_TAX_AMT_SP` decimal(26,6) DEFAULT NULL,
  `TOT_TAX_AMT_BS` decimal(26,6) DEFAULT NULL,
  `REC_TAX_AMT_SP` decimal(26,6) DEFAULT NULL,
  `REC_TAX_AMT_BS` decimal(26,6) DEFAULT NULL,
  `TAXABLE_AMT_SP_ORIG` decimal(26,6) DEFAULT NULL,
  `TAXABLE_AMT_BS_ORIG` decimal(26,6) DEFAULT NULL,
  `EXPIRY_DT` date DEFAULT NULL,
  `MFG_DT` date DEFAULT NULL,
  `BATCH_NO` varchar(100) DEFAULT NULL,
  `ITM_CURR_STK` decimal(26,6) DEFAULT NULL,
  `SCRP_QTY` decimal(26,6) DEFAULT NULL,
  `SCRP_QTY_BS` decimal(26,6) DEFAULT NULL,
  `ITM_REMARKS` text,
  `SHORT_ACCESS_TYPE` varchar(1) DEFAULT NULL,
  `SHORT_ACCESS_QTY_SP` decimal(26,6) DEFAULT NULL,
  `SHORT_ACCESS_QTY_BS` decimal(26,6) DEFAULT NULL,
  `INCL_TAX_RULE_FLG` varchar(1) DEFAULT NULL,
  `LINE_SEQ_NO` decimal(26,6) DEFAULT NULL,
  `CNT_QTY` decimal(26,6) DEFAULT NULL,
  `REJ_CNT_QTY` decimal(26,6) DEFAULT NULL,
  `RWK_CNT_QTY` decimal(26,6) DEFAULT NULL,
  `MAKE` varchar(255) DEFAULT NULL,
  `BILL_PRICE` decimal(26,6) DEFAULT NULL,
  `SYNC_FLG` varchar(2) DEFAULT NULL,
  `UPD_FLG` varchar(2) DEFAULT NULL,
  `SN` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$mm$mtl$rcpt$itm`
--

INSERT INTO `intrm$mm$mtl$rcpt$itm` (`CLD_ID`, `SLOC_ID`, `ORG_ID`, `WH_ID`, `POS_MTL_RCPT_ITM_ID`, `SRC_RCPT_ID`, `DOC_ID`, `DOC_ID_SRC`, `DOC_DT_SRC`, `DLV_SCHDL_NO`, `ITM_ID`, `ITM_UOM`, `PEND_QTY`, `DLV_NOTE_QTY`, `FOC_FLG`, `RWK_QTY`, `REJ_QTY`, `RCPT_QTY`, `REJ_RESN`, `QC_REQD_FLG`, `QC_DONE_FLG`, `QC_TXN_ID`, `QC_RWK_QTY`, `QC_REJ_QTY`, `QC_REJ_RESN`, `FINAL_RCPT_QTY`, `ITM_UOM_BS`, `UOM_CONV_FCTR`, `RWK_QTY_BS`, `REJ_QTY_BS`, `RCPT_QTY_BS`, `QC_RWK_QTY_BS`, `QC_REJ_QTY_BS`, `FINAL_RCPT_QTY_BS`, `LND_PRICE`, `QC_OVERRIDE_FLG`, `QC_OVERRIDE_RESN`, `PUR_PRICE`, `PUR_PRICE_BS`, `LND_PRICE_BS`, `TOT_RCPT_QTY`, `TOT_RCPT_QTY_BS`, `DISC_AMT_SP`, `DISC_AMT_BS`, `TAXABLE_AMT_SP`, `TAXABLE_AMT_BS`, `TOT_TAX_AMT_SP`, `TOT_TAX_AMT_BS`, `REC_TAX_AMT_SP`, `REC_TAX_AMT_BS`, `TAXABLE_AMT_SP_ORIG`, `TAXABLE_AMT_BS_ORIG`, `EXPIRY_DT`, `MFG_DT`, `BATCH_NO`, `ITM_CURR_STK`, `SCRP_QTY`, `SCRP_QTY_BS`, `ITM_REMARKS`, `SHORT_ACCESS_TYPE`, `SHORT_ACCESS_QTY_SP`, `SHORT_ACCESS_QTY_BS`, `INCL_TAX_RULE_FLG`, `LINE_SEQ_NO`, `CNT_QTY`, `REJ_CNT_QTY`, `RWK_CNT_QTY`, `MAKE`, `BILL_PRICE`, `SYNC_FLG`, `UPD_FLG`, `SN`) VALUES
('0000', 1, '04', 'WH00005', '1', 1, NULL, '0000.01.04.0001.04oS.00.1UNWIBA7gH', '2017-01-20 18:30:00', NULL, 'AP01.0000001', NULL, '-1945.000000', '2000.000000', NULL, NULL, '0.000000', '55.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2000.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '5', 1),
('0000', 1, '04', 'WH00005', '2', 1, NULL, '0000.01.04.0001.04oS.00.1UNWIBA7gH', '2017-01-20 18:30:00', NULL, 'AP02.0000001', NULL, '-80.000000', '100.000000', NULL, NULL, '0.000000', '20.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '100.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '5', 2),
('0000', 1, '04', 'WH00005', '3', 2, NULL, '0000.01.04.0001.04oS.00.1UNWIBA7gH', '2017-01-20 18:30:00', NULL, 'AP01.0000001', NULL, '-60.000000', '80.000000', NULL, NULL, '0.000000', '20.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '80.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '5', 3);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$mm$mtl$rcpt$lot`
--

CREATE TABLE `intrm$mm$mtl$rcpt$lot` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `ORG_ID` varchar(2) DEFAULT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `POS_MTL_RCPT_LOT_ID` bigint(20) DEFAULT NULL,
  `ITM_SRC_ID` bigint(20) DEFAULT NULL,
  `DOC_ID` varchar(20) DEFAULT NULL,
  `DOC_ID_SRC` varchar(40) DEFAULT NULL,
  `DOC_DT_SRC` timestamp NULL DEFAULT NULL,
  `DLV_SCHDL_NO` int(5) DEFAULT NULL,
  `LOT_ID` varchar(20) DEFAULT NULL,
  `ITM_ID` varchar(50) DEFAULT NULL,
  `ITM_UOM` varchar(20) DEFAULT NULL,
  `LOT_QTY` decimal(26,6) DEFAULT NULL,
  `LOT_PRICE` decimal(26,2) DEFAULT NULL,
  `WARRANTY_DT` date DEFAULT NULL,
  `EXPIRY_DT` date DEFAULT NULL,
  `ITM_UOM_BS` varchar(20) DEFAULT NULL,
  `LOT_QTY_BS` decimal(26,6) DEFAULT NULL,
  `LOT_QTY_SP` decimal(26,2) DEFAULT NULL,
  `REJ_QTY_SP` decimal(26,6) DEFAULT NULL,
  `REJ_QTY_BS` decimal(26,6) DEFAULT NULL,
  `RWK_QTY_SP` decimal(26,6) DEFAULT NULL,
  `RWK_QTY_BS` decimal(26,6) DEFAULT NULL,
  `MFG_DT` date DEFAULT NULL,
  `BATCH_NO` varchar(100) DEFAULT NULL,
  `SCRP_QTY` decimal(26,6) DEFAULT NULL,
  `SCRP_QTY_BS` decimal(26,6) DEFAULT NULL,
  `CNT_QTY` decimal(26,6) DEFAULT NULL,
  `REJ_CNT_QTY` decimal(26,6) DEFAULT NULL,
  `RWK_CNT_QTY` decimal(26,6) DEFAULT NULL,
  `REMARKS` varchar(4000) DEFAULT NULL,
  `WTY_STRT_FRM` int(10) DEFAULT NULL,
  `WTY_DAYS` int(10) DEFAULT NULL,
  `WTY_STRT_FRM_DT` date DEFAULT NULL,
  `RG_23D_NO` varchar(40) DEFAULT NULL,
  `SYNC_FLG` varchar(2) DEFAULT NULL,
  `UPD_FLG` varchar(2) DEFAULT NULL,
  `SN` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$mm$mtl$rcpt$lot`
--

INSERT INTO `intrm$mm$mtl$rcpt$lot` (`CLD_ID`, `SLOC_ID`, `ORG_ID`, `WH_ID`, `POS_MTL_RCPT_LOT_ID`, `ITM_SRC_ID`, `DOC_ID`, `DOC_ID_SRC`, `DOC_DT_SRC`, `DLV_SCHDL_NO`, `LOT_ID`, `ITM_ID`, `ITM_UOM`, `LOT_QTY`, `LOT_PRICE`, `WARRANTY_DT`, `EXPIRY_DT`, `ITM_UOM_BS`, `LOT_QTY_BS`, `LOT_QTY_SP`, `REJ_QTY_SP`, `REJ_QTY_BS`, `RWK_QTY_SP`, `RWK_QTY_BS`, `MFG_DT`, `BATCH_NO`, `SCRP_QTY`, `SCRP_QTY_BS`, `CNT_QTY`, `REJ_CNT_QTY`, `RWK_CNT_QTY`, `REMARKS`, `WTY_STRT_FRM`, `WTY_DAYS`, `WTY_STRT_FRM_DT`, `RG_23D_NO`, `SYNC_FLG`, `UPD_FLG`, `SN`) VALUES
('0000', 1, '04', 'WH00005', 1, 1, NULL, '0000.01.04.0001.04oS.00.1UNWIBA7gH', '2017-01-20 18:30:00', 0, '2-LOTC9FJ48', 'AP01.0000001', NULL, '55.000000', '250.00', NULL, '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '5', 1),
('0000', 1, '04', 'WH00005', 2, 2, NULL, '0000.01.04.0001.04oS.00.1UNWIBA7gH', '2017-01-20 18:30:00', 0, '2-LOT80B5B7', 'AP02.0000001', NULL, '20.000000', '1090.00', NULL, '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '5', 2),
('0000', 1, '04', 'WH00005', 3, 3, NULL, '0000.01.04.0001.04oS.00.1UNWIBA7gH', '2017-01-20 18:30:00', 0, '2-LOT1B004F', 'AP01.0000001', NULL, '20.000000', '250.00', NULL, '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '5', 3);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$mm$mtl$rcpt$sr`
--

CREATE TABLE `intrm$mm$mtl$rcpt$sr` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(2) DEFAULT NULL,
  `ORG_ID` varchar(2) DEFAULT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `DOC_ID` varchar(40) DEFAULT NULL,
  `DOC_ID_SRC` varchar(40) DEFAULT NULL,
  `DOC_DT_SRC` timestamp(6) NULL DEFAULT NULL,
  `DLV_SCHDL_NO` int(5) DEFAULT NULL,
  `LOT_ID` varchar(20) DEFAULT NULL,
  `BIN_ID` varchar(20) DEFAULT NULL,
  `ITM_ID` varchar(50) DEFAULT NULL,
  `ITM_UOM` varchar(20) DEFAULT NULL,
  `SR_NO` varchar(100) DEFAULT NULL,
  `SR_QTY` varchar(26) DEFAULT NULL,
  `ITM_UOM_BS` varchar(20) DEFAULT NULL,
  `SR_QTY_BS` int(26) DEFAULT NULL,
  `REJ_QTY_SP` int(26) DEFAULT NULL,
  `REJ_QTY_BS` int(26) DEFAULT NULL,
  `RWK_QTY_SP` int(26) DEFAULT NULL,
  `RWK_QTY_BS` int(26) DEFAULT NULL,
  `SCRP_QTY` int(26) DEFAULT NULL,
  `SCRP_QTY_BS` int(26) DEFAULT NULL,
  `SR_NO1` varchar(100) DEFAULT NULL,
  `SR_NO2` varchar(100) DEFAULT NULL,
  `CNT_QTY` int(26) DEFAULT NULL,
  `REJ_CNT_QTY` int(26) DEFAULT NULL,
  `RWK_CNT_QTY` int(26) DEFAULT NULL,
  `WTY_STRT_FRM` int(26) DEFAULT NULL,
  `WTY_DAYS` int(26) DEFAULT NULL,
  `SYNC_FLG` varchar(2) DEFAULT NULL,
  `UPD_FLG` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$mm$mtl$rcpt$src`
--

CREATE TABLE `intrm$mm$mtl$rcpt$src` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(2) DEFAULT NULL,
  `ORG_ID` varchar(2) DEFAULT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `POS_MTL_RCPT_SRC_ID` bigint(20) DEFAULT NULL,
  `RCPT_ID` bigint(20) DEFAULT NULL,
  `DOC_TYPE_SRC` int(5) DEFAULT NULL,
  `DOC_ID` varchar(40) DEFAULT NULL,
  `DOC_ID_SRC` varchar(40) DEFAULT NULL,
  `DOC_DT_SRC` date DEFAULT NULL,
  `DLV_SCHDL_NO` int(5) DEFAULT NULL,
  `TAX_RULE_FLG` varchar(1) DEFAULT NULL,
  `TAX_AFTER_DISC_FLG` varchar(1) DEFAULT NULL,
  `DISC_AMT_SP` decimal(26,6) DEFAULT NULL,
  `DISC_AMT_BS` decimal(26,6) DEFAULT NULL,
  `PO_AMT_SP` decimal(26,6) DEFAULT NULL,
  `PO_AMT_BS` decimal(26,6) DEFAULT NULL,
  `TAXABLE_AMT_SP` decimal(26,6) DEFAULT NULL,
  `TAXABLE_AMT_BS` decimal(26,6) DEFAULT NULL,
  `AMD_NO` int(2) DEFAULT NULL,
  `SHPMNT_BASIS` int(5) DEFAULT NULL,
  `DOC_ID_ISSU` varchar(40) DEFAULT NULL,
  `BDG_COA_ID` int(10) DEFAULT NULL,
  `SYNC_FLG` varchar(2) DEFAULT NULL,
  `UPD_FLG` varchar(2) DEFAULT NULL,
  `SN` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$mm$mtl$rcpt$src`
--

INSERT INTO `intrm$mm$mtl$rcpt$src` (`CLD_ID`, `SLOC_ID`, `ORG_ID`, `WH_ID`, `POS_MTL_RCPT_SRC_ID`, `RCPT_ID`, `DOC_TYPE_SRC`, `DOC_ID`, `DOC_ID_SRC`, `DOC_DT_SRC`, `DLV_SCHDL_NO`, `TAX_RULE_FLG`, `TAX_AFTER_DISC_FLG`, `DISC_AMT_SP`, `DISC_AMT_BS`, `PO_AMT_SP`, `PO_AMT_BS`, `TAXABLE_AMT_SP`, `TAXABLE_AMT_BS`, `AMD_NO`, `SHPMNT_BASIS`, `DOC_ID_ISSU`, `BDG_COA_ID`, `SYNC_FLG`, `UPD_FLG`, `SN`) VALUES
('0000', 1, '04', 'WH00005', 1, 1, NULL, NULL, '0000.01.04.0001.04oS.00.1UNWIBA7gH', '2017-01-21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '5', 1),
('0000', 1, '04', 'WH00005', 2, 2, NULL, NULL, '0000.01.04.0001.04oS.00.1UNWIBA7gH', '2017-01-21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '5', 2);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$mm$stk$summ$bin`
--

CREATE TABLE `intrm$mm$stk$summ$bin` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `ORG_ID` varchar(20) DEFAULT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `POS_BIN_ID` bigint(20) DEFAULT NULL,
  `ITM_ID` varchar(50) DEFAULT NULL,
  `LOT_ID` varchar(20) DEFAULT NULL,
  `BIN_ID` varchar(20) DEFAULT NULL,
  `ITM_UOM_BS` varchar(20) DEFAULT NULL,
  `TOT_STK` decimal(26,6) DEFAULT NULL,
  `USR_ID_MOD` int(10) DEFAULT NULL,
  `USR_ID_MOD_DT` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `REJ_STK` decimal(26,6) DEFAULT NULL,
  `RWK_STK` decimal(26,6) DEFAULT NULL,
  `REJ_CNT_QTY` decimal(26,6) DEFAULT NULL,
  `RWK_CNT_QTY` decimal(26,6) DEFAULT NULL,
  `CNT_QTY` decimal(26,6) DEFAULT NULL,
  `CREATE_FLG` varchar(1) DEFAULT NULL,
  `UPD_FLG` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$mm$stk$summ$bin`
--

INSERT INTO `intrm$mm$stk$summ$bin` (`CLD_ID`, `SLOC_ID`, `ORG_ID`, `WH_ID`, `POS_BIN_ID`, `ITM_ID`, `LOT_ID`, `BIN_ID`, `ITM_UOM_BS`, `TOT_STK`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `REJ_STK`, `RWK_STK`, `REJ_CNT_QTY`, `RWK_CNT_QTY`, `CNT_QTY`, `CREATE_FLG`, `UPD_FLG`) VALUES
('0000', 1, '04', 'WH00001', 3, 'AP02.0000001', 'LOT00001', 'BIN00002', 'UOM0000000063', '500.000000', 1, '2017-01-31 14:21:48', NULL, NULL, '0.000000', '0.000000', '0.000000', NULL, NULL),
('0000', 1, '04', 'WH00001', 1, 'AF.0000002', 'LOT00001', 'BIN00002', 'UOM0000000063', '1000.000000', 1, '2017-01-31 14:21:48', NULL, NULL, '0.000000', '0.000000', '0.000000', NULL, NULL),
('0000', 1, '04', 'WH00005', 5, 'AP02.0000001', 'LOT00001', 'BIN00006', 'UOM0000000063', '100.000000', 1, '2017-01-31 14:21:48', NULL, NULL, '0.000000', '0.000000', '0.000000', NULL, NULL),
('0000', 1, '04', 'WH00005', 2, 'AF.0000003', 'LOT00001', 'BIN00006', 'UOM0000000063', '3000.000000', 1, '2017-01-31 14:21:48', NULL, NULL, '0.000000', '0.000000', '0.000000', NULL, NULL),
('0000', 1, '04', 'WH00001', 7, 'ITM.0000002', 'LOT#00001', 'BIN00002', 'UOM0000000063', '2000.000000', 1, '2017-01-31 14:21:48', NULL, NULL, '0.000000', '0.000000', '0.000000', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$mm$stk$summ$itm`
--

CREATE TABLE `intrm$mm$stk$summ$itm` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(5) NOT NULL,
  `ORG_ID` varchar(20) NOT NULL,
  `WH_ID` varchar(20) NOT NULL,
  `POS_STK_ITM_ID` bigint(20) DEFAULT NULL,
  `FY_ID` int(5) NOT NULL,
  `ITM_ID` varchar(50) NOT NULL,
  `ITM_UOM_BS` varchar(20) NOT NULL,
  `OP_STK` decimal(26,6) NOT NULL,
  `OP_AVL_STK` decimal(26,6) DEFAULT NULL,
  `OP_REQ_STK` decimal(26,6) DEFAULT NULL,
  `OP_ORD_STK` decimal(26,6) DEFAULT NULL,
  `OP_REJ_STK` decimal(26,6) DEFAULT NULL,
  `TOT_STK` decimal(26,6) DEFAULT NULL,
  `AVL_STK` decimal(26,6) DEFAULT NULL,
  `REQ_STK` decimal(26,6) NOT NULL,
  `ORD_STK` decimal(26,6) NOT NULL,
  `REJ_STK` decimal(26,6) NOT NULL,
  `USR_ID_MOD` int(10) NOT NULL,
  `USR_ID_MOD_DT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `OP_SCRP_STK` decimal(26,6) NOT NULL,
  `OP_RWK_STK` decimal(26,6) NOT NULL,
  `SCRP_STK` decimal(26,6) NOT NULL,
  `RWK_STK` decimal(26,6) NOT NULL,
  `MAP_PRICE` decimal(26,6) NOT NULL,
  `WAP_PRICE` decimal(26,6) NOT NULL,
  `STD_VAL` decimal(26,6) NOT NULL,
  `MAP_VAL` decimal(26,6) NOT NULL,
  `WAP_VAL` decimal(26,6) NOT NULL,
  `LIFO_VAL` decimal(26,6) NOT NULL,
  `FIFO_VAL` decimal(26,6) NOT NULL,
  `CUM_TOT_REJ_QTY` decimal(26,6) DEFAULT NULL,
  `CUM_TOT_RWK_QTY` decimal(26,6) DEFAULT NULL,
  `CUM_TOT_SCRP_QTY` decimal(26,6) DEFAULT NULL,
  `CUM_TOT_RCPT_QTY` decimal(26,6) DEFAULT NULL,
  `CUM_TOT_RCPT_LND_VAL` decimal(26,6) DEFAULT NULL,
  `CUM_TOT_ISSU_QTY` decimal(26,6) DEFAULT NULL,
  `CUM_TOT_ISSU_LND_VAL` decimal(26,6) DEFAULT NULL,
  `LIFO_PRICE` decimal(26,6) DEFAULT NULL,
  `FIFO_PRICE` decimal(26,6) DEFAULT NULL,
  `GIT_STK` decimal(26,6) DEFAULT NULL,
  `REJ_CNT_QTY` decimal(26,6) DEFAULT NULL,
  `RWK_CNT_QTY` decimal(26,6) DEFAULT NULL,
  `CNT_QTY` decimal(26,6) DEFAULT NULL,
  `CREATE_FLG` varchar(2) DEFAULT NULL,
  `UPD_FLG` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$mm$stk$summ$itm`
--

INSERT INTO `intrm$mm$stk$summ$itm` (`CLD_ID`, `SLOC_ID`, `ORG_ID`, `WH_ID`, `POS_STK_ITM_ID`, `FY_ID`, `ITM_ID`, `ITM_UOM_BS`, `OP_STK`, `OP_AVL_STK`, `OP_REQ_STK`, `OP_ORD_STK`, `OP_REJ_STK`, `TOT_STK`, `AVL_STK`, `REQ_STK`, `ORD_STK`, `REJ_STK`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `OP_SCRP_STK`, `OP_RWK_STK`, `SCRP_STK`, `RWK_STK`, `MAP_PRICE`, `WAP_PRICE`, `STD_VAL`, `MAP_VAL`, `WAP_VAL`, `LIFO_VAL`, `FIFO_VAL`, `CUM_TOT_REJ_QTY`, `CUM_TOT_RWK_QTY`, `CUM_TOT_SCRP_QTY`, `CUM_TOT_RCPT_QTY`, `CUM_TOT_RCPT_LND_VAL`, `CUM_TOT_ISSU_QTY`, `CUM_TOT_ISSU_LND_VAL`, `LIFO_PRICE`, `FIFO_PRICE`, `GIT_STK`, `REJ_CNT_QTY`, `RWK_CNT_QTY`, `CNT_QTY`, `CREATE_FLG`, `UPD_FLG`) VALUES
('0000', 1, '04', 'WH00005', 2, 1, 'AP01.0000001', 'UOM0000000049', '0.000000', '0.000000', NULL, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '2000.000000', '0.000000', 0, '2017-01-31 14:21:48', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.000000', '0.000000', '0.000000', NULL, NULL, NULL, NULL, NULL),
('0000', 1, '04', 'WH00005', 13, 1, 'AP02.0000001', 'UOM0000000063', '100.000000', '100.000000', '0.000000', '0.000000', '0.000000', '100.000000', '100.000000', '0.000000', '100.000000', '0.000000', 1, '2017-01-31 14:21:48', '0.000000', '0.000000', '0.000000', '0.000000', '1090.000000', '1090.000000', '109000.000000', '109000.000000', '109000.000000', '0.000000', '0.000000', NULL, NULL, NULL, '100.000000', '109000.000000', NULL, NULL, NULL, NULL, NULL, '0.000000', '0.000000', '0.000000', NULL, NULL),
('0000', 1, '04', 'WH00005', 11, 1, 'AF.0000004', 'UOM0000000063', '0.000000', '0.000000', NULL, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '1000.000000', '0.000000', 0, '2017-01-31 14:21:48', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.000000', '0.000000', '0.000000', NULL, NULL, NULL, NULL, NULL),
('0000', 1, '04', 'WH00001', 1, 1, 'AP01.0000001', 'UOM0000000049', '0.000000', '0.000000', NULL, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '1500.000000', '0.000000', 0, '2017-01-31 14:21:48', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.000000', '0.000000', '0.000000', NULL, NULL, NULL, NULL, NULL),
('0000', 1, '04', 'WH00001', 14, 1, 'ITM.0000002', 'UOM0000000063', '2000.000000', '2000.000000', '0.000000', '0.000000', '0.000000', '2000.000000', '2000.000000', '0.000000', '0.000000', '0.000000', 1, '2017-01-31 14:21:48', '0.000000', '0.000000', '0.000000', '0.000000', '490.000000', '490.000000', '980000.000000', '980000.000000', '980000.000000', '0.000000', '0.000000', NULL, NULL, NULL, '2000.000000', '980000.000000', NULL, NULL, NULL, NULL, NULL, '0.000000', '0.000000', '0.000000', NULL, NULL),
('0000', 1, '04', 'WH00001', 12, 1, 'AP02.0000001', 'UOM0000000063', '500.000000', '500.000000', NULL, '0.000000', '0.000000', '500.000000', '500.000000', '0.000000', '1500.000000', '0.000000', 0, '2017-01-31 14:21:48', '0.000000', '0.000000', '0.000000', '0.000000', '1090.000000', '1090.000000', '545000.000000', '545000.000000', '545000.000000', '0.000000', '0.000000', NULL, NULL, NULL, '500.000000', '545000.000000', NULL, NULL, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', NULL, NULL),
('0000', 1, '04', 'WH00004', 4, 1, 'AP03.0000001', 'UOM0000000068', '0.000000', '0.000000', NULL, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '1500.000000', '0.000000', 0, '2017-01-31 14:21:48', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.000000', '0.000000', '0.000000', NULL, NULL, NULL, NULL, NULL),
('0000', 1, '04', 'WH00005', 16, 1, 'AF.0000005', 'UOM0000000063', '0.000000', '0.000000', NULL, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '20.000000', '0.000000', 0, '2017-01-31 14:21:48', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.000000', '0.000000', '0.000000', NULL, NULL, NULL, NULL, NULL),
('0000', 1, '04', 'WH00001', 6, 1, 'AF.0000002', 'UOM0000000063', '1000.000000', '1000.000000', NULL, '0.000000', '0.000000', '1000.000000', '1000.000000', '0.000000', '2000.000000', '0.000000', 0, '2017-01-31 14:21:48', '0.000000', '0.000000', '0.000000', '0.000000', '400.000000', '400.000000', '400000.000000', '400000.000000', '400000.000000', '0.000000', '0.000000', NULL, NULL, NULL, '1000.000000', '400000.000000', NULL, NULL, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', NULL, NULL),
('0000', 1, '04', 'WH00001', 15, 1, 'AF.0000005', 'UOM0000000063', '0.000000', '0.000000', NULL, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '50.000000', '0.000000', 0, '2017-01-31 14:21:48', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.000000', '0.000000', '0.000000', NULL, NULL, NULL, NULL, NULL),
('0000', 1, '04', 'WH00001', 7, 1, 'AF.0000003', 'UOM0000000063', '0.000000', '0.000000', NULL, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '2008.000000', '0.000000', 0, '2017-01-31 14:21:48', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.000000', '0.000000', '0.000000', NULL, NULL, NULL, NULL, NULL),
('0000', 1, '04', 'WH00005', 9, 1, 'AF.0000003', 'UOM0000000063', '3000.000000', '3000.000000', '0.000000', '0.000000', '0.000000', '3000.000000', '3000.000000', '0.000000', '0.000000', '0.000000', 1, '2017-01-31 14:21:48', '0.000000', '0.000000', '0.000000', '0.000000', '300.000000', '300.000000', '900000.000000', '900000.000000', '900000.000000', '0.000000', '0.000000', NULL, NULL, NULL, '3000.000000', '900000.000000', NULL, NULL, NULL, NULL, NULL, '0.000000', '0.000000', '0.000000', NULL, NULL),
('0000', 1, '04', 'WH00005', 5, 1, 'AP03.0000001', 'UOM0000000068', '0.000000', '0.000000', NULL, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '8000.000000', '0.000000', 0, '2017-01-31 14:21:48', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.000000', '0.000000', '0.000000', NULL, NULL, NULL, NULL, NULL),
('0000', 1, '04', 'WH00001', 3, 1, 'AP03.0000001', 'UOM0000000068', '0.000000', '0.000000', NULL, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '100.000000', '0.000000', 0, '2017-01-31 14:21:48', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.000000', '0.000000', '0.000000', NULL, NULL, NULL, NULL, NULL),
('0000', 1, '04', 'WH00001', 8, 1, 'AF.0000004', 'UOM0000000063', '0.000000', '0.000000', NULL, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '800.000000', '0.000000', 0, '2017-01-31 14:21:48', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.000000', '0.000000', '0.000000', NULL, NULL, NULL, NULL, NULL),
('0000', 1, '04', 'WH00005', 10, 1, 'AF.0000002', 'UOM0000000063', '0.000000', '0.000000', NULL, '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '1500.000000', '0.000000', 0, '2017-01-31 14:21:48', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', '0.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.000000', '0.000000', '0.000000', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$mm$stk$summ$lot`
--

CREATE TABLE `intrm$mm$stk$summ$lot` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `ORG_ID` varchar(20) DEFAULT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `POS_STK_LOT_ID` bigint(20) DEFAULT NULL,
  `ITM_ID` varchar(50) DEFAULT NULL,
  `LOT_ID` varchar(20) DEFAULT NULL,
  `ITM_UOM_BS` varchar(20) DEFAULT NULL,
  `OP_STK` decimal(26,6) DEFAULT NULL,
  `TOT_STK` decimal(26,6) DEFAULT NULL,
  `BASIC_PRICE` decimal(26,6) DEFAULT NULL,
  `LND_PRICE` decimal(26,6) DEFAULT NULL,
  `LND_VAL` decimal(26,6) DEFAULT NULL,
  `WARRANTY_DT` date DEFAULT NULL,
  `EXPIRY_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(10) DEFAULT NULL,
  `USR_ID_MOD_DT` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LOT_DT` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `SCRP_STK` decimal(26,6) DEFAULT NULL,
  `REJ_STK` decimal(26,6) DEFAULT NULL,
  `RWK_STK` decimal(26,6) DEFAULT NULL,
  `LOT_PRICE_OP` decimal(26,6) DEFAULT NULL,
  `LOT_PRICE_CRNT` decimal(26,6) DEFAULT NULL,
  `MFG_DT` date DEFAULT NULL,
  `BATCH_NO` varchar(20) DEFAULT NULL,
  `REJ_CNT_QTY` decimal(26,6) DEFAULT NULL,
  `RWK_CNT_QTY` decimal(26,6) DEFAULT NULL,
  `CNT_QTY` decimal(26,6) DEFAULT NULL,
  `REMARKS` varchar(2000) DEFAULT NULL,
  `UPD_FLG` varchar(2) DEFAULT NULL,
  `CREATE_FLG` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$mm$stk$summ$lot`
--

INSERT INTO `intrm$mm$stk$summ$lot` (`CLD_ID`, `SLOC_ID`, `ORG_ID`, `WH_ID`, `POS_STK_LOT_ID`, `ITM_ID`, `LOT_ID`, `ITM_UOM_BS`, `OP_STK`, `TOT_STK`, `BASIC_PRICE`, `LND_PRICE`, `LND_VAL`, `WARRANTY_DT`, `EXPIRY_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `LOT_DT`, `SCRP_STK`, `REJ_STK`, `RWK_STK`, `LOT_PRICE_OP`, `LOT_PRICE_CRNT`, `MFG_DT`, `BATCH_NO`, `REJ_CNT_QTY`, `RWK_CNT_QTY`, `CNT_QTY`, `REMARKS`, `UPD_FLG`, `CREATE_FLG`) VALUES
('0000', 1, '04', 'WH00005', 4, 'AP02.0000001', 'LOT00001', 'UOM0000000063', '100.000000', '100.000000', '1090.000000', '1090.000000', '109000.000000', NULL, NULL, 1, '2017-01-31 14:21:48', '2017-01-19 11:19:43', NULL, NULL, NULL, NULL, NULL, NULL, '3489', NULL, NULL, NULL, NULL, NULL, NULL),
('0000', 1, '04', 'WH00005', 2, 'AF.0000003', 'LOT00001', 'UOM0000000063', '3000.000000', '3000.000000', '300.000000', '300.000000', '900000.000000', NULL, NULL, 1, '2017-01-31 14:21:48', '2017-01-19 11:19:43', NULL, NULL, NULL, NULL, NULL, NULL, '3425', NULL, NULL, NULL, NULL, NULL, NULL),
('0000', 1, '04', 'WH00001', 1, 'AF.0000002', 'LOT00001', 'UOM0000000063', '1000.000000', '1000.000000', '400.000000', '400.000000', '400000.000000', NULL, NULL, 1, '2017-01-31 14:21:48', '2017-01-19 10:43:07', NULL, NULL, NULL, NULL, NULL, NULL, '234', NULL, NULL, NULL, NULL, NULL, NULL),
('0000', 1, '04', 'WH00001', 3, 'AP02.0000001', 'LOT00001', 'UOM0000000063', '500.000000', '500.000000', '1090.000000', '1090.000000', '545000.000000', NULL, NULL, 1, '2017-01-31 14:21:48', '2017-01-28 09:45:16', NULL, NULL, NULL, NULL, NULL, NULL, '8765', NULL, NULL, NULL, NULL, NULL, NULL),
('0000', 1, '04', 'WH00001', 5, 'ITM.0000002', 'LOT#00001', 'UOM0000000063', '2000.000000', '2000.000000', '490.000000', '490.000000', '980000.000000', NULL, NULL, 1, '2017-01-31 14:21:48', '2017-01-28 13:18:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.000000', '0.000000', '0.000000', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$mm$stk$summ$sr`
--

CREATE TABLE `intrm$mm$stk$summ$sr` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `ORG_ID` varchar(20) DEFAULT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `ITM_ID` varchar(50) DEFAULT NULL,
  `LOT_ID` varchar(20) DEFAULT NULL,
  `BIN_ID` varchar(20) DEFAULT NULL,
  `SR_NO` varchar(100) DEFAULT NULL,
  `TOT_STK` decimal(26,6) DEFAULT NULL,
  `USR_ID_MOD` int(10) DEFAULT NULL,
  `USR_ID_MOD_DT` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `SCRP_STK` decimal(26,6) DEFAULT NULL,
  `REJ_STK` decimal(26,6) DEFAULT NULL,
  `RWK_STK` decimal(26,6) DEFAULT NULL,
  `SR_NO1` varchar(100) DEFAULT NULL,
  `SR_NO2` varchar(100) DEFAULT NULL,
  `REJ_CNT_QTY` decimal(26,6) DEFAULT NULL,
  `RWK_CNT_QTY` decimal(26,6) DEFAULT NULL,
  `CNT_QTY` decimal(26,6) DEFAULT NULL,
  `UPD_FLG` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$mm$trf$ord`
--

CREATE TABLE `intrm$mm$trf$ord` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `ORG_ID` varchar(10) DEFAULT NULL,
  `DOC_ID` varchar(40) DEFAULT NULL,
  `DOC_DT` timestamp NULL DEFAULT NULL,
  `POS_TRF_ORD_ID` bigint(21) DEFAULT NULL,
  `FY_ID` int(11) DEFAULT NULL,
  `TRF_NO` varchar(20) DEFAULT NULL,
  `TRF_DT` date DEFAULT NULL,
  `TRF_SRC_TYPE` int(5) DEFAULT NULL,
  `TRF_TYPE` int(5) DEFAULT NULL,
  `DOC_TYPE_SRC` int(5) DEFAULT NULL,
  `DOC_ID_SRC` varchar(40) DEFAULT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `RQMT_AREA_ID` int(5) DEFAULT NULL,
  `ORG_ID_SRC` varchar(2) DEFAULT NULL,
  `WH_ID_SRC` varchar(20) DEFAULT NULL,
  `REQD_DT` date DEFAULT NULL,
  `ORG_ID_DEST` varchar(2) DEFAULT NULL,
  `WH_ID_DEST` varchar(20) DEFAULT NULL,
  `TRF_STAT` int(5) DEFAULT NULL,
  `TRF_STAT_DT` date DEFAULT NULL,
  `INV_REQD` varchar(2) DEFAULT NULL,
  `AUTH_STAT` varchar(2) DEFAULT NULL,
  `USR_ID_CREATE` int(10) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(10) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL,
  `REMARKS` varchar(300) DEFAULT NULL,
  `PRJ_ID` varchar(40) DEFAULT NULL,
  `PRJ_ID_DEST` varchar(40) DEFAULT NULL,
  `PRJ_ID_MAIN` varchar(40) DEFAULT NULL,
  `DRFT_TRF_NO` varchar(20) DEFAULT NULL,
  `SYNC_FLG` varchar(2) DEFAULT NULL,
  `UPD_FLG` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$mm$trf$ord`
--

INSERT INTO `intrm$mm$trf$ord` (`CLD_ID`, `SLOC_ID`, `ORG_ID`, `DOC_ID`, `DOC_DT`, `POS_TRF_ORD_ID`, `FY_ID`, `TRF_NO`, `TRF_DT`, `TRF_SRC_TYPE`, `TRF_TYPE`, `DOC_TYPE_SRC`, `DOC_ID_SRC`, `WH_ID`, `RQMT_AREA_ID`, `ORG_ID_SRC`, `WH_ID_SRC`, `REQD_DT`, `ORG_ID_DEST`, `WH_ID_DEST`, `TRF_STAT`, `TRF_STAT_DT`, `INV_REQD`, `AUTH_STAT`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `REMARKS`, `PRJ_ID`, `PRJ_ID_DEST`, `PRJ_ID_MAIN`, `DRFT_TRF_NO`, `SYNC_FLG`, `UPD_FLG`) VALUES
('0000', 1, '04', '0000.01.04.0001.04oh.00.1UNWG34CqR', '2017-01-19 10:37:48', NULL, 1, 'ETO0000001', '2017-01-19', 372, 373, 0, NULL, 'WH00001', NULL, '04', 'WH00001', '2017-01-19', '04', 'WH00004', 375, '2017-01-19', 'Y', 'Y', 1, '2017-01-19', 1, '2017-01-19', NULL, '0000.01.04.0001.00XR.00.1UKxc44uJN', '0000.01.04.0001.00XR.00.1UKxc44uJN', '0000.01.04.0001.00XR.00.1UKxc44uJN', 'DTO00003', NULL, NULL),
('0000', 1, '04', '0000.01.04.0001.04oh.00.1UNWPrWw6a', '2017-01-28 09:46:08', NULL, 1, 'ETO0000002', '2017-01-28', 372, 373, 0, NULL, 'WH00001', NULL, '04', 'WH00001', '2017-01-28', '04', 'WH00004', 375, '2017-01-28', 'Y', 'Y', 1, '2017-01-28', 1, '2017-01-28', NULL, '0000.01.04.0001.00XR.00.1UKxc44uJN', '0000.01.04.0001.00XR.00.1UKxc44uJN', '0000.01.04.0001.00XR.00.1UKxc44uJN', 'DTO00004', NULL, NULL),
('0000', 1, '04', '0000.01.04.0001.04oh.00.1UNWG32K4v', '2017-01-19 10:33:00', NULL, 1, 'DTO00002', '2017-01-19', 372, 373, 0, NULL, 'WH00001', NULL, '04', 'WH00001', '2017-01-19', '04', 'WH00004', 375, '2017-01-19', 'Y', 'Y', 1, '2017-01-19', 1, '2017-01-19', 'N', '0000.01.04.0001.00XR.00.1UKxc44uJN', '0000.01.04.0001.00XR.00.1UKxc44uJN', '0000.01.04.0001.00XR.00.1UKxc44uJN', 'DTO00002', NULL, NULL),
('0000', 1, '04', '0000.01.04.0001.04oh.00.1UKzcbAIFW', '2016-12-22 12:55:30', NULL, 1, 'DTO00001', '2016-12-22', 372, 373, 0, NULL, 'WH00001', NULL, '04', 'WH00001', '2016-12-22', '04', 'WH00004', 375, '2016-12-22', 'Y', 'Y', 1, '2016-12-22', 1, '2016-12-22', NULL, '0000.01.04.0001.00XR.00.1UKxc44uJN', '0000.01.04.0001.00XR.00.1UKxc44uJN', '0000.01.04.0001.00XR.00.1UKxc44uJN', 'DTO00001', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$mm$trf$ord$itm`
--

CREATE TABLE `intrm$mm$trf$ord$itm` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(2) DEFAULT NULL,
  `ORG_ID` varchar(10) DEFAULT NULL,
  `POS_TRF_ORD_ITM_ID` bigint(20) DEFAULT NULL,
  `DOC_ID` varchar(40) DEFAULT NULL,
  `ITM_ID` varchar(50) DEFAULT NULL,
  `ITM_UOM` varchar(20) DEFAULT NULL,
  `ORD_QTY` decimal(26,6) DEFAULT NULL,
  `AUTH_QTY` decimal(26,6) DEFAULT NULL,
  `STK_RESV_QTY` decimal(26,6) DEFAULT NULL,
  `TRF_QTY` decimal(26,6) DEFAULT NULL,
  `PEND_QTY` decimal(26,6) DEFAULT NULL,
  `CNCL_ITM` varchar(1) DEFAULT NULL,
  `ITM_UOM_BS` varchar(20) DEFAULT NULL,
  `UOM_CONV_FCTR` decimal(26,6) DEFAULT NULL,
  `ORD_QTY_BS` decimal(26,6) DEFAULT NULL,
  `AUTH_QTY_BS` decimal(26,6) DEFAULT NULL,
  `STK_RESV_QTY_BS` decimal(26,6) DEFAULT NULL,
  `TRF_QTY_BS` decimal(26,6) DEFAULT NULL,
  `PEND_QTY_BS` decimal(26,6) DEFAULT NULL,
  `TOT_TRF_QTY` decimal(26,6) DEFAULT NULL,
  `TOT_TRF_QTY_BS` decimal(26,6) DEFAULT NULL,
  `ITM_REMARKS` varchar(300) DEFAULT NULL,
  `LINE_SEQ_NO` int(10) DEFAULT NULL,
  `ITM_PRICE` decimal(26,2) DEFAULT NULL,
  `ITM_PRICE_BS` decimal(26,6) DEFAULT NULL,
  `SYNC_FLG` varchar(2) DEFAULT NULL,
  `UPD_FLG` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$mm$trf$ord$itm`
--

INSERT INTO `intrm$mm$trf$ord$itm` (`CLD_ID`, `SLOC_ID`, `ORG_ID`, `POS_TRF_ORD_ITM_ID`, `DOC_ID`, `ITM_ID`, `ITM_UOM`, `ORD_QTY`, `AUTH_QTY`, `STK_RESV_QTY`, `TRF_QTY`, `PEND_QTY`, `CNCL_ITM`, `ITM_UOM_BS`, `UOM_CONV_FCTR`, `ORD_QTY_BS`, `AUTH_QTY_BS`, `STK_RESV_QTY_BS`, `TRF_QTY_BS`, `PEND_QTY_BS`, `TOT_TRF_QTY`, `TOT_TRF_QTY_BS`, `ITM_REMARKS`, `LINE_SEQ_NO`, `ITM_PRICE`, `ITM_PRICE_BS`, `SYNC_FLG`, `UPD_FLG`) VALUES
('0000', 1, '04', NULL, '0000.01.04.0001.04oh.00.1UKzcbAIFW', 'AF.0000002', 'UOM0000000063', '100.000000', '100.000000', '0.000000', '0.000000', '100.000000', 'N', 'UOM0000000063', '1.000000', '100.000000', '100.000000', '0.000000', '0.000000', NULL, NULL, '0.000000', NULL, 44, NULL, NULL, NULL, NULL),
('0000', 1, '04', NULL, '0000.01.04.0001.04oh.00.1UNWG34CqR', 'AF.0000003', 'UOM0000000063', '200.000000', '200.000000', '0.000000', '0.000000', '200.000000', 'N', 'UOM0000000063', '1.000000', '200.000000', '200.000000', '0.000000', '0.000000', NULL, NULL, '0.000000', NULL, 106, NULL, NULL, NULL, NULL),
('0000', 1, '04', NULL, '0000.01.04.0001.04oh.00.1UNWG32K4v', 'AF.0000002', 'UOM0000000063', '100.000000', '100.000000', '0.000000', '0.000000', '100.000000', 'N', 'UOM0000000063', '1.000000', '100.000000', '100.000000', '0.000000', '0.000000', NULL, NULL, '0.000000', NULL, 105, NULL, NULL, NULL, NULL),
('0000', 1, '04', NULL, '0000.01.04.0001.04oh.00.1UNWPrWw6a', 'AP02.0000001', 'UOM0000000063', '10.000000', '10.000000', '0.000000', '0.000000', '10.000000', 'N', 'UOM0000000063', '1.000000', '10.000000', '10.000000', '0.000000', '0.000000', NULL, NULL, '0.000000', NULL, 124, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$org`
--

CREATE TABLE `intrm$org` (
  `ORG_CLD_ID` varchar(4) DEFAULT '0000',
  `ORG_ID` varchar(20) DEFAULT NULL,
  `POS_ORG_ID` int(5) DEFAULT NULL,
  `ORG_TYPE` varchar(2) DEFAULT NULL,
  `ORG_DESC` varchar(250) DEFAULT NULL,
  `ORG_ALIAS` varchar(10) NOT NULL,
  `ORG_ID_PARENT` varchar(2) DEFAULT NULL,
  `ORG_ID_PARENT_L0` varchar(2) DEFAULT NULL,
  `ORG_COUNTRY_ID` int(11) NOT NULL,
  `ORG_CREATE_REF_SLOC_ID` int(2) NOT NULL,
  `ORG_CURR_ID_BS` int(5) NOT NULL,
  `ORG_TRF_ACC` int(11) NOT NULL,
  `ORG_VAT_NO` varchar(20) NOT NULL,
  `USR_ID_CREATE` int(5) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(5) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL,
  `SEGMNT_ID` varchar(200) NOT NULL,
  `UPD_FLG` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$org`
--

INSERT INTO `intrm$org` (`ORG_CLD_ID`, `ORG_ID`, `POS_ORG_ID`, `ORG_TYPE`, `ORG_DESC`, `ORG_ALIAS`, `ORG_ID_PARENT`, `ORG_ID_PARENT_L0`, `ORG_COUNTRY_ID`, `ORG_CREATE_REF_SLOC_ID`, `ORG_CURR_ID_BS`, `ORG_TRF_ACC`, `ORG_VAT_NO`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `SEGMNT_ID`, `UPD_FLG`) VALUES
('0000', '02', 0, '52', 'Haryana State Office', 'HR SO', '01', '01', 106, 1, 73, 0, '0', 1, '2016-10-27', NULL, NULL, '', NULL),
('0000', '04', 0, '52', 'UP SO', 'UPSO', '01', '01', 106, 1, 73, 0, '0', 1, '2016-10-28', NULL, NULL, '', NULL),
('0000', '03', 0, '52', 'MADHYA PRADESH SO', 'MPSO', '01', '01', 106, 1, 73, 0, '0', 1, '2016-10-28', 1, '2016-11-03', '', NULL),
('0000', '01', 0, '51', 'IFFCO eBAZAR LTD.', 'IeBL', NULL, NULL, 106, 1, 73, 0, '0', 1, '2014-04-01', 1, '2016-10-27', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$payment$dtl`
--

CREATE TABLE `intrm$payment$dtl` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(11) DEFAULT NULL,
  `HO_ORG_ID` varchar(2) DEFAULT NULL,
  `ORG_ID` varchar(50) NOT NULL,
  `PAYMNT_ID` int(11) DEFAULT NULL,
  `POS_PAYMNT_ID` int(11) DEFAULT NULL,
  `POS_INV_NO` varchar(40) NOT NULL,
  `PAYMNT_DT` date NOT NULL,
  `EO_ID` int(11) DEFAULT NULL,
  `POS_EO_ID` int(11) DEFAULT NULL,
  `PAID_BY` varchar(20) NOT NULL,
  `CARD_TYPE` varchar(20) DEFAULT NULL,
  `CC_NM` int(25) DEFAULT NULL,
  `CC_HOLDER` varchar(25) DEFAULT NULL,
  `CC_MONTH` varchar(2) DEFAULT NULL,
  `CC_YEAR` varchar(4) DEFAULT NULL,
  `CC_TYPE` varchar(20) DEFAULT NULL,
  `AMOUNT` decimal(25,4) NOT NULL,
  `SYNC_FLG` varchar(20) DEFAULT NULL,
  `USR_ID_CREATE` int(11) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(11) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$prod`
--

CREATE TABLE `intrm$prod` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) DEFAULT NULL,
  `HO_ORG_ID` varchar(2) DEFAULT NULL,
  `POS_ITM_ID` int(20) DEFAULT NULL,
  `ITM_ID` varchar(50) DEFAULT NULL,
  `UNIVERSAL_ITM_ID` varchar(250) NOT NULL,
  `ITM_DESC` varchar(100) NOT NULL,
  `ITM_LONG_DESC` varchar(500) NOT NULL,
  `ITM_LEGACY_CODE` varchar(50) NOT NULL,
  `GRP_ID` varchar(20) DEFAULT NULL,
  `UOM_BASIC` varchar(20) NOT NULL,
  `UOM_SLS` varchar(20) NOT NULL,
  `PRICE_BASIC` int(26) NOT NULL,
  `PRICE_SLS` int(26) NOT NULL,
  `SERIALIZED_FLG` varchar(1) DEFAULT NULL,
  `USR_ID_CREATE` int(5) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(5) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL,
  `UPD_FLG` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$prod`
--

INSERT INTO `intrm$prod` (`CLD_ID`, `SLOC_ID`, `HO_ORG_ID`, `POS_ITM_ID`, `ITM_ID`, `UNIVERSAL_ITM_ID`, `ITM_DESC`, `ITM_LONG_DESC`, `ITM_LEGACY_CODE`, `GRP_ID`, `UOM_BASIC`, `UOM_SLS`, `PRICE_BASIC`, `PRICE_SLS`, `SERIALIZED_FLG`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `UPD_FLG`) VALUES
('0000', 1, '01', NULL, 'ITM.0000002', 'ITM.0000002', 'DAP Plus urea', 'DAP Plus urea', 'ITM.0000002', '0061010100012', 'UOM0000000063', 'UOM0000000063', 490, 490, 'N', 1, '2017-01-28', NULL, NULL, NULL),
('0000', 1, '01', NULL, 'AF.0000005', 'AF.0000005', 'NPK 12:10:16', 'NPK 12:10:16', 'AF.0000005', '0061010100012', 'UOM0000000063', 'UOM0000000063', 340, 340, 'N', 1, '2017-01-28', 1, '2017-01-28', NULL),
('0000', 1, '01', NULL, 'AP02.0000001', 'AP02.0000001', 'NPK 20:20:013', 'NPK 20:20:013', '45', '0012010100006', 'UOM0000000063', 'UOM0000000063', 1090, 1090, 'N', 1, '2017-01-17', NULL, NULL, NULL),
('0000', 1, '01', NULL, 'AP03.0000001', 'AP03.0000001', 'HIBIKI 250 ML', 'HIBIKI 250 ML\n', 'AP01', '0014010100007', 'UOM0000000068', 'UOM0000000068', 110, 0, 'N', 1, '2016-11-08', NULL, NULL, NULL),
('0000', 1, '01', NULL, 'AF.0000001', 'AF.0000001', 'Neem Coated Urea AM1', 'Neem Coated Urea AM1', 'NM0001', '0006010100002', 'UOM0000000063', 'UOM0000000063', 300, 300, 'N', 1, '2016-12-19', 1, '2016-12-22', NULL),
('0000', 1, '01', NULL, 'AF.0000003', 'AF.0000003', 'Neem Urea AM -II', 'Neem Urea AM -II', '47680', '0006010100002', 'UOM0000000063', 'UOM0000000063', 300, 300, 'N', 1, '2016-12-21', NULL, NULL, NULL),
('0000', 1, '01', NULL, 'ITM.0000001', 'ITM.0000001', 'Laptop HP240', 'Laptop HP240', '09979', '0022010100010', 'UOM0000000063', 'UOM0000000063', 40000, 40000, 'N', 1, '2016-12-19', NULL, NULL, NULL),
('0000', 1, '01', NULL, 'AP01.0000001', 'AP01.0000001', 'GENKI 1 litre', 'GENKI 1 litre', 'AP01.0000001', '0011010100005', 'UOM0000000049', 'UOM0000000049', 250, 250, 'N', 1, '2016-11-08', NULL, NULL, NULL),
('0000', 1, '01', NULL, 'AF.0000002', 'AF.0000002', 'Neem Urea AM -I', 'Neem Urea AM -I', '9878', '0006010100002', 'UOM0000000063', 'UOM0000000063', 400, 400, 'N', 1, '2016-12-21', NULL, NULL, NULL),
('0000', 1, '01', NULL, 'AF.0000004', 'AF.0000004', 'NPK 10:16:32', 'NPK 10:16:32', '8745', '0006010100002', 'UOM0000000063', 'UOM0000000063', 240, 240, 'N', 1, '2016-12-22', NULL, NULL, NULL),
('0000', 1, '01', NULL, 'ITM.0000003', 'ITM.0000003', 'Hp-4420', 'Hp-4420', 'Hp-4420', '0022010100010', 'UOM0000000063', 'UOM0000000063', 100, 100, 'N', 1, '2017-01-30', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$prod$org`
--

CREATE TABLE `intrm$prod$org` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(2) DEFAULT NULL,
  `HO_ORG_ID` varchar(2) DEFAULT NULL,
  `ORG_ID` varchar(10) DEFAULT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `POS_ITM_ID` int(20) DEFAULT NULL,
  `ITM_ID` varchar(50) DEFAULT NULL,
  `UNIVERSAL_ITM_ID` varchar(50) NOT NULL,
  `CREATE_FLG` varchar(1) DEFAULT NULL,
  `UPD_FLG` varchar(1) DEFAULT NULL,
  `DEL_FLG` varchar(1) DEFAULT NULL,
  `USR_ID_CREATE` int(5) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(5) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$prod$org`
--

INSERT INTO `intrm$prod$org` (`CLD_ID`, `SLOC_ID`, `HO_ORG_ID`, `ORG_ID`, `WH_ID`, `POS_ITM_ID`, `ITM_ID`, `UNIVERSAL_ITM_ID`, `CREATE_FLG`, `UPD_FLG`, `DEL_FLG`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`) VALUES
('0000', 1, '01', '01', 'WH00001', 1, 'AF.0000001', 'AF.0000001', '1', NULL, NULL, 1, '2016-12-19', NULL, NULL),
('0000', 1, '01', '01', 'WH00001', 2, 'AF.0000002', 'AF.0000002', '1', NULL, NULL, 1, '2016-12-21', NULL, NULL),
('0000', 1, '01', '01', 'WH00001', 3, 'AF.0000003', 'AF.0000003', '1', NULL, NULL, 1, '2016-12-21', NULL, NULL),
('0000', 1, '01', '01', 'WH00001', 4, 'AF.0000004', 'AF.0000004', '1', NULL, NULL, 1, '2016-12-22', NULL, NULL),
('0000', 1, '01', '01', 'WH00001', 10, 'AF.0000005', 'AF.0000005', '1', NULL, NULL, 1, '2017-01-28', NULL, NULL),
('0000', 1, '01', '01', 'WH00001', 5, 'AP01.0000001', 'AP01.0000001', '1', NULL, NULL, 1, '2016-11-08', NULL, NULL),
('0000', 1, '01', '01', 'WH00001', 6, 'AP02.0000001', 'AP02.0000001', '1', NULL, NULL, 1, '2017-01-17', NULL, NULL),
('0000', 1, '01', '01', 'WH00001', 7, 'AP03.0000001', 'AP03.0000001', '1', NULL, NULL, 1, '2016-11-08', NULL, NULL),
('0000', 1, '01', '01', 'WH00001', 8, 'ITM.0000001', 'ITM.0000001', '1', NULL, NULL, 1, '2016-12-19', NULL, NULL),
('0000', 1, '01', '01', 'WH00001', 11, 'ITM.0000002', 'ITM.0000002', '1', NULL, NULL, 1, '2017-01-28', NULL, NULL),
('0000', 1, '01', '01', 'WH00001', 9, 'ITM.0000003', 'ITM.0000003', '1', NULL, NULL, 1, '2017-01-30', NULL, '2017-01-30'),
('0000', 1, '01', '04', 'EWH00004', 12, 'AF.0000001', 'AF.0000001', '1', NULL, NULL, 1, '2016-12-19', NULL, NULL),
('0000', 1, '01', '04', 'WH00001', 17, 'AF.0000001', 'AF.0000001', '1', NULL, NULL, 1, '2016-12-19', NULL, NULL),
('0000', 1, '01', '04', 'WH00002', 20, 'AF.0000001', 'AF.0000001', '1', NULL, NULL, 1, '2016-12-19', NULL, NULL),
('0000', 1, '01', '04', 'WH00003', 24, 'AF.0000001', 'AF.0000001', '1', NULL, NULL, 1, '2016-12-19', NULL, NULL),
('0000', 1, '01', '04', 'WH00004', 28, 'AF.0000001', 'AF.0000001', '1', NULL, NULL, 1, '2016-12-19', NULL, NULL),
('0000', 1, '01', '04', 'WH00005', 33, 'AF.0000001', 'AF.0000001', '1', NULL, NULL, 1, '2016-12-19', NULL, NULL),
('0000', 1, '01', '04', 'EWH00004', 13, 'AF.0000002', 'AF.0000002', '1', NULL, NULL, 1, '2016-12-21', NULL, NULL),
('0000', 1, '01', '04', 'WH00001', 16, 'AF.0000002', 'AF.0000002', '1', NULL, NULL, 1, '2016-12-21', NULL, NULL),
('0000', 1, '01', '04', 'WH00002', 21, 'AF.0000002', 'AF.0000002', '1', NULL, NULL, 1, '2016-12-21', NULL, NULL),
('0000', 1, '01', '04', 'WH00003', 25, 'AF.0000002', 'AF.0000002', '1', NULL, NULL, 1, '2016-12-21', NULL, NULL),
('0000', 1, '01', '04', 'WH00004', 29, 'AF.0000002', 'AF.0000002', '1', NULL, NULL, 1, '2016-12-21', NULL, NULL),
('0000', 1, '01', '04', 'WH00005', 34, 'AF.0000002', 'AF.0000002', '1', NULL, NULL, 1, '2016-12-21', NULL, NULL),
('0000', 1, '01', '04', 'EWH00004', 14, 'AF.0000003', 'AF.0000003', '1', NULL, NULL, 1, '2016-12-21', NULL, NULL),
('0000', 1, '01', '04', 'WH00001', 18, 'AF.0000003', 'AF.0000003', '1', NULL, NULL, 1, '2016-12-21', NULL, NULL),
('0000', 1, '01', '04', 'WH00002', 22, 'AF.0000003', 'AF.0000003', '1', NULL, NULL, 1, '2016-12-21', NULL, NULL),
('0000', 1, '01', '04', 'WH00003', 26, 'AF.0000003', 'AF.0000003', '1', NULL, NULL, 1, '2016-12-21', NULL, NULL),
('0000', 1, '01', '04', 'WH00004', 30, 'AF.0000003', 'AF.0000003', '1', NULL, NULL, 1, '2016-12-21', NULL, NULL),
('0000', 1, '01', '04', 'WH00005', 32, 'AF.0000003', 'AF.0000003', '1', NULL, NULL, 1, '2016-12-21', NULL, NULL),
('0000', 1, '01', '04', 'EWH00004', 15, 'AF.0000004', 'AF.0000004', '1', NULL, NULL, 1, '2016-12-22', NULL, NULL),
('0000', 1, '01', '04', 'WH00001', 19, 'AF.0000004', 'AF.0000004', '1', NULL, NULL, 1, '2016-12-22', NULL, NULL),
('0000', 1, '01', '04', 'WH00002', 23, 'AF.0000004', 'AF.0000004', '1', NULL, NULL, 1, '2016-12-22', NULL, NULL),
('0000', 1, '01', '04', 'WH00003', 27, 'AF.0000004', 'AF.0000004', '1', NULL, NULL, 1, '2016-12-22', NULL, NULL),
('0000', 1, '01', '04', 'WH00004', 31, 'AF.0000004', 'AF.0000004', '1', NULL, NULL, 1, '2016-12-22', NULL, NULL),
('0000', 1, '01', '04', 'WH00005', 35, 'AF.0000004', 'AF.0000004', '1', NULL, NULL, 1, '2016-12-22', NULL, NULL),
('0000', 1, '01', '04', 'EWH00004', 60, 'AF.0000005', 'AF.0000005', '1', NULL, NULL, 1, '2017-01-28', NULL, NULL),
('0000', 1, '01', '04', 'WH00001', 63, 'AF.0000005', 'AF.0000005', '1', NULL, NULL, 1, '2017-01-28', NULL, NULL),
('0000', 1, '01', '04', 'WH00002', 64, 'AF.0000005', 'AF.0000005', '1', NULL, NULL, 1, '2017-01-28', NULL, NULL),
('0000', 1, '01', '04', 'WH00003', 66, 'AF.0000005', 'AF.0000005', '1', NULL, NULL, 1, '2017-01-28', NULL, NULL),
('0000', 1, '01', '04', 'WH00004', 68, 'AF.0000005', 'AF.0000005', '1', NULL, NULL, 1, '2017-01-28', NULL, NULL),
('0000', 1, '01', '04', 'WH00005', 70, 'AF.0000005', 'AF.0000005', '1', NULL, NULL, 1, '2017-01-28', NULL, NULL),
('0000', 1, '01', '04', 'EWH00004', 36, 'AP01.0000001', 'AP01.0000001', '1', NULL, NULL, 1, '2016-11-08', NULL, '2016-11-08'),
('0000', 1, '01', '04', 'WH00001', 37, 'AP01.0000001', 'AP01.0000001', '1', NULL, NULL, 1, '2016-11-08', NULL, '2016-11-08'),
('0000', 1, '01', '04', 'WH00002', 38, 'AP01.0000001', 'AP01.0000001', '1', NULL, NULL, 1, '2016-11-08', NULL, '2016-11-08'),
('0000', 1, '01', '04', 'WH00003', 39, 'AP01.0000001', 'AP01.0000001', '1', NULL, NULL, 1, '2016-11-08', NULL, '2016-11-08'),
('0000', 1, '01', '04', 'WH00004', 40, 'AP01.0000001', 'AP01.0000001', '1', NULL, NULL, 1, '2016-11-08', NULL, '2016-11-08'),
('0000', 1, '01', '04', 'WH00005', 41, 'AP01.0000001', 'AP01.0000001', '1', NULL, NULL, 1, '2016-11-08', NULL, '2016-11-08'),
('0000', 1, '01', '04', 'EWH00004', 42, 'AP02.0000001', 'AP02.0000001', '1', NULL, NULL, 1, '2017-01-17', NULL, NULL),
('0000', 1, '01', '04', 'WH00001', 43, 'AP02.0000001', 'AP02.0000001', '1', NULL, NULL, 1, '2017-01-17', NULL, NULL),
('0000', 1, '01', '04', 'WH00002', 44, 'AP02.0000001', 'AP02.0000001', '1', NULL, NULL, 1, '2017-01-17', NULL, NULL),
('0000', 1, '01', '04', 'WH00003', 45, 'AP02.0000001', 'AP02.0000001', '1', NULL, NULL, 1, '2017-01-17', NULL, NULL),
('0000', 1, '01', '04', 'WH00004', 46, 'AP02.0000001', 'AP02.0000001', '1', NULL, NULL, 1, '2017-01-17', NULL, NULL),
('0000', 1, '01', '04', 'WH00005', 47, 'AP02.0000001', 'AP02.0000001', '1', NULL, NULL, 1, '2017-01-17', NULL, NULL),
('0000', 1, '01', '04', 'EWH00004', 48, 'AP03.0000001', 'AP03.0000001', '1', NULL, NULL, 1, '2016-11-08', NULL, '2016-11-08'),
('0000', 1, '01', '04', 'WH00001', 49, 'AP03.0000001', 'AP03.0000001', '1', NULL, NULL, 1, '2016-11-08', NULL, '2016-11-08'),
('0000', 1, '01', '04', 'WH00002', 50, 'AP03.0000001', 'AP03.0000001', '1', NULL, NULL, 1, '2016-11-08', NULL, '2016-11-08'),
('0000', 1, '01', '04', 'WH00003', 51, 'AP03.0000001', 'AP03.0000001', '1', NULL, NULL, 1, '2016-11-08', NULL, '2016-11-08'),
('0000', 1, '01', '04', 'WH00004', 52, 'AP03.0000001', 'AP03.0000001', '1', NULL, NULL, 1, '2016-11-08', NULL, '2016-11-08'),
('0000', 1, '01', '04', 'WH00005', 53, 'AP03.0000001', 'AP03.0000001', '1', NULL, NULL, 1, '2016-11-08', NULL, '2016-11-08'),
('0000', 1, '01', '04', 'EWH00004', 61, 'ITM.0000002', 'ITM.0000002', '1', NULL, NULL, 1, '2017-01-28', NULL, '2017-01-28'),
('0000', 1, '01', '04', 'WH00001', 62, 'ITM.0000002', 'ITM.0000002', '1', NULL, NULL, 1, '2017-01-28', NULL, '2017-01-28'),
('0000', 1, '01', '04', 'WH00002', 65, 'ITM.0000002', 'ITM.0000002', '1', NULL, NULL, 1, '2017-01-28', NULL, '2017-01-28'),
('0000', 1, '01', '04', 'WH00003', 67, 'ITM.0000002', 'ITM.0000002', '1', NULL, NULL, 1, '2017-01-28', NULL, '2017-01-28'),
('0000', 1, '01', '04', 'WH00004', 69, 'ITM.0000002', 'ITM.0000002', '1', NULL, NULL, 1, '2017-01-28', NULL, '2017-01-28'),
('0000', 1, '01', '04', 'WH00005', 71, 'ITM.0000002', 'ITM.0000002', '1', NULL, NULL, 1, '2017-01-28', NULL, '2017-01-28'),
('0000', 1, '01', '04', 'EWH00004', 54, 'ITM.0000003', 'ITM.0000003', '1', NULL, NULL, 1, '2017-01-30', NULL, '2017-01-30'),
('0000', 1, '01', '04', 'WH00001', 55, 'ITM.0000003', 'ITM.0000003', '1', NULL, NULL, 1, '2017-01-30', NULL, '2017-01-30'),
('0000', 1, '01', '04', 'WH00002', 56, 'ITM.0000003', 'ITM.0000003', '1', NULL, NULL, 1, '2017-01-30', NULL, '2017-01-30'),
('0000', 1, '01', '04', 'WH00003', 57, 'ITM.0000003', 'ITM.0000003', '1', NULL, NULL, 1, '2017-01-30', NULL, '2017-01-30'),
('0000', 1, '01', '04', 'WH00004', 58, 'ITM.0000003', 'ITM.0000003', '1', NULL, NULL, 1, '2017-01-30', NULL, '2017-01-30'),
('0000', 1, '01', '04', 'WH00005', 59, 'ITM.0000003', 'ITM.0000003', '1', NULL, NULL, 1, '2017-01-30', NULL, '2017-01-30');

-- --------------------------------------------------------

--
-- Table structure for table `intrm$segment`
--

CREATE TABLE `intrm$segment` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(2) NOT NULL,
  `HO_ORG_ID` varchar(2) NOT NULL,
  `ORG_ID` varchar(20) DEFAULT NULL,
  `PRJ_DOC_ID` varchar(40) DEFAULT NULL,
  `PARENT_PROJ_ID` varchar(40) DEFAULT NULL,
  `PROJ_NM` varchar(100) DEFAULT NULL,
  `USR_ID_CREATE` int(4) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` bigint(20) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL,
  `PRJ_ACTV` varchar(1) DEFAULT NULL,
  `POS_PRJ_ID` bigint(20) DEFAULT NULL,
  `CREATE_FLG` varchar(2) DEFAULT NULL,
  `UPD_FLG` varchar(2) DEFAULT NULL,
  `DEL_FLG` varchar(2) DEFAULT NULL,
  `SYNC_FLG` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$segment`
--

INSERT INTO `intrm$segment` (`CLD_ID`, `SLOC_ID`, `HO_ORG_ID`, `ORG_ID`, `PRJ_DOC_ID`, `PARENT_PROJ_ID`, `PROJ_NM`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `PRJ_ACTV`, `POS_PRJ_ID`, `CREATE_FLG`, `UPD_FLG`, `DEL_FLG`, `SYNC_FLG`) VALUES
('0000', 1, '01', '01', '0000.01.01.0001.00XR.00.1UNWOpVKdM', '0', 'PROJECT IFFCO', 1, '2017-01-27', NULL, NULL, 'Y', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '04', '0000.01.04.0001.00XR.00.1UKxc4ge73', '0000.01.04.0001.00XR.00.1UKxc43tG9', 'TITARI POS', 1, '2016-11-08', NULL, NULL, 'Y', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '04', '0000.01.04.0001.00XR.00.1UKxc44uJN', '0000.01.04.0001.00XR.00.1UKxc43tG9', 'Govindpuram POS', 1, '2016-11-08', NULL, NULL, 'Y', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '04', '0000.01.04.0001.00XR.00.1UKxc4hs1H', '0', 'FAIZABAD CO', 1, '2016-11-08', NULL, NULL, 'Y', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '02', 'PROJ0000', '0', 'Default Project', 1, '2016-10-27', NULL, NULL, 'Y', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '02', '0000.01.02.0001.00XR.00.1UKxc4GDO1', '0000.01.02.0001.00XR.00.1UKxc4FOiL', 'NIGHDU POS', 1, '2016-11-08', NULL, NULL, 'N', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '04', 'PROJ0000', '0', 'Default Project', 1, '2016-10-28', NULL, NULL, 'Y', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '03', 'PROJ0000', '0', 'Default Project', 1, '2016-10-28', NULL, NULL, 'Y', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '02', '0000.01.02.0001.00XR.00.1UKxc4DuWc', '0000.01.02.0001.00XR.00.1UKxc4CS5i', 'BAWANIKHERA POS', 1, '2016-11-08', NULL, NULL, 'N', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '04', '0000.01.04.0001.00XR.00.1UKxc4l8uW', '0000.01.04.0001.00XR.00.1UKxc4iCfs', 'NAUJHIL POS', 1, '2016-11-08', NULL, NULL, 'Y', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '02', '0000.01.02.0001.00XR.00.1UKxc4CS5i', '0', 'ROHTAK CO', 1, '2016-11-08', NULL, NULL, 'N', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '01', '0000.01.01.0001.00XR.00.1UNWOpFkcA', '0', 'PROJECT CONSTRUCTION', 1, '2017-01-27', NULL, NULL, 'Y', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '05', 'PROJ0000', '0', 'Default Project', 1, '2016-11-30', NULL, NULL, 'Y', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '04', '0000.01.04.0001.00XR.00.1UKxc4hShD', '0000.01.04.0001.00XR.00.1UKxc43tG9', 'SISOU POS', 1, '2016-11-08', NULL, NULL, 'Y', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '02', '0000.01.02.0001.00XR.00.1UKxc4Gd0w', '0', 'REWARI CO ', 1, '2016-11-08', NULL, NULL, 'N', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '04', '0000.01.04.0001.00XR.00.1UKxc4h4GW', '0000.01.04.0001.00XR.00.1UKxc43tG9', 'AMINAGAR POS', 1, '2016-11-08', NULL, NULL, 'Y', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '02', '0000.01.02.0001.00XR.00.1UKxc4YZ3L', '0000.01.02.0001.00XR.00.1UKxc4Gd0w', 'HODAL POS', 1, '2016-11-08', NULL, NULL, 'N', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '02', '0000.01.02.0001.00XR.00.1UKxc4E6RA', '0', 'FATEHBAD CO', 1, '2016-11-08', NULL, NULL, 'N', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '04', '0000.01.04.0001.00XR.00.1UKxc4jZRJ', '0000.01.04.0001.00XR.00.1UKxc4hs1H', 'RAMPUR HALWARA POS', 1, '2016-11-08', NULL, NULL, 'Y', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '02', '0000.01.02.0001.00XR.00.1UKxc4FALg', '0000.01.02.0001.00XR.00.1UKxc4E6RA', 'KULLAN POS', 1, '2016-11-08', NULL, NULL, 'N', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '04', '0000.01.04.0001.00XR.00.1UKxc4iCfs', '0', 'AGRA CO', 1, '2016-11-08', NULL, NULL, 'Y', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '04', '0000.01.04.0001.00XR.00.1UKxc43tG9', '0', 'Meerut CO', 1, '2016-11-08', NULL, NULL, 'Y', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '02', '0000.01.02.0001.00XR.00.1UKxc4Yye5', '0000.01.02.0001.00XR.00.1UKxc4Gd0w', 'BERI POS', 1, '2016-11-08', NULL, NULL, 'N', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '04', '0000.01.04.0001.00XR.00.1UKxc4jxdZ', '0000.01.04.0001.00XR.00.1UKxc4iCfs', 'MATHURA MANDI POS', 1, '2016-11-08', NULL, NULL, 'Y', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '02', '0000.01.02.0001.00XR.00.1UKxc4FOiL', '0', 'KARNAL CO', 1, '2016-11-08', NULL, NULL, 'N', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '01', 'PROJ0000', '0', 'Default Project', 1, '2015-08-12', NULL, NULL, 'Y', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '02', '0000.01.02.0001.00XR.00.1UKxc4YDGv', '0000.01.02.0001.00XR.00.1UKxc4Gd0w', 'HATHIN POS', 1, '2016-11-08', NULL, NULL, 'N', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '04', '0000.01.04.0001.00XR.00.1UKxc4jP3m', '0000.01.04.0001.00XR.00.1UKxc4hs1H', 'PARASPUR GONDA POS', 1, '2016-11-08', NULL, NULL, 'Y', 0, 'Y', 'Y', 'Y', '1'),
('0000', 1, '01', '02', '0000.01.02.0001.00XR.00.1UKxc4ZOpX', '0000.01.02.0001.00XR.00.1UKxc4FOiL', 'KHIZRABAD POS', 1, '2016-11-08', NULL, NULL, 'N', 0, 'Y', 'Y', 'Y', '1');

-- --------------------------------------------------------

--
-- Table structure for table `intrm$segment$ent`
--

CREATE TABLE `intrm$segment$ent` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(2) NOT NULL,
  `HO_ORG_ID` varchar(2) NOT NULL,
  `ORG_ID` varchar(20) DEFAULT NULL,
  `PRJ_DOC_ID` varchar(2) DEFAULT NULL,
  `PRJ_ENT_TYP_ID` varchar(2) DEFAULT NULL,
  `PRJ_ENT_ID` int(10) DEFAULT NULL,
  `USR_ID_CREATE` int(5) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(5) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL,
  `POS_PRJ_ID` bigint(20) DEFAULT NULL,
  `SYNC_FLG` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$sls$inv`
--

CREATE TABLE `intrm$sls$inv` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(11) NOT NULL,
  `HO_ORG_ID` varchar(2) NOT NULL,
  `ORG_ID` varchar(20) NOT NULL,
  `POS_DOC_ID` int(11) NOT NULL,
  `DOC_ID` varchar(40) NOT NULL,
  `ERP_DOC_ID` varchar(50) DEFAULT NULL,
  `DOC_DT` date NOT NULL,
  `EO_ID` int(11) NOT NULL,
  `POS_EO_ID` int(11) NOT NULL,
  `SLS_REP_ID` int(11) NOT NULL,
  `POS_SLS_REP_ID` int(11) NOT NULL,
  `POS_INV_TOTAL` decimal(25,4) NOT NULL,
  `TAX_BASIS` varchar(10) NOT NULL,
  `TAX_ID` int(11) NOT NULL,
  `TAX_VAL` int(11) NOT NULL,
  `DISC_BASIS` varchar(10) NOT NULL,
  `DISC_TYPE` varchar(10) NOT NULL,
  `DISC_VAL` int(11) NOT NULL,
  `DISC_TOTAL_VAL` int(11) NOT NULL,
  `SYNC_FLG` varchar(20) DEFAULT NULL,
  `USR_ID_CREATE` int(11) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(11) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `SN` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$sls$inv$itm`
--

CREATE TABLE `intrm$sls$inv$itm` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(11) NOT NULL,
  `HO_ORG_ID` varchar(2) NOT NULL,
  `ORG_ID` varchar(20) NOT NULL,
  `POS_DOC_ID` int(11) NOT NULL,
  `ERP_DOC_ID` varchar(50) DEFAULT NULL,
  `DOC_ID` varchar(20) NOT NULL,
  `ITM_ID` varchar(20) NOT NULL,
  `POS_ITM_ID` int(11) NOT NULL,
  `SR_NO` varchar(20) NOT NULL,
  `LOT_NO` varchar(20) NOT NULL,
  `ITM_QTY` int(11) NOT NULL,
  `ITM_UOM` varchar(20) NOT NULL,
  `POS_ITM_UOM` int(11) NOT NULL,
  `ITM_UOM_BS` varchar(20) NOT NULL,
  `POS_ITM_UOM_BS` int(11) NOT NULL,
  `SLS_PRICE` int(11) NOT NULL,
  `TAX_ID` int(11) NOT NULL,
  `TAX_VAL` int(11) NOT NULL,
  `DISC_TYPE` varchar(20) NOT NULL,
  `DISC_VAL` int(11) NOT NULL,
  `DISC_TOTAL_VAL` int(11) NOT NULL,
  `ITM_TOTAL_VAL` int(11) NOT NULL,
  `SYNC_FLG` varchar(20) DEFAULT NULL,
  `USR_ID_CREATE` int(11) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(11) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL,
  `DOC_DATE` date DEFAULT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `SN` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$sls$rma`
--

CREATE TABLE `intrm$sls$rma` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(11) NOT NULL,
  `HO_ORG_ID` varchar(2) NOT NULL,
  `ORG_ID` varchar(2) NOT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `POS_DOC_ID` int(11) NOT NULL,
  `ERP_DOC_ID` varchar(50) DEFAULT NULL,
  `DOC_ID` varchar(40) NOT NULL,
  `DOC_DT` date NOT NULL,
  `REF_INV_DOC_ID` varchar(40) NOT NULL,
  `EO_ID` int(11) NOT NULL,
  `POS_EO_ID` int(11) NOT NULL,
  `REF_VOUCHER_ID` varchar(40) NOT NULL,
  `SYNC_FLG` varchar(2) NOT NULL,
  `USR_ID_CREATE` int(11) NOT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(11) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$sls$rma$itm`
--

CREATE TABLE `intrm$sls$rma$itm` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(11) NOT NULL,
  `HO_ORG_ID` varchar(2) NOT NULL,
  `ORG_ID` varchar(2) NOT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `POS_DOC_ID` int(11) NOT NULL,
  `ERP_DOC_ID` varchar(50) DEFAULT NULL,
  `DOC_ID` varchar(40) NOT NULL,
  `ITM_ID` varchar(40) NOT NULL,
  `POS_ITM_ID` int(11) NOT NULL,
  `SR_NO` varchar(20) NOT NULL,
  `LOT_NO` varchar(40) NOT NULL,
  `ITM_QTY` int(11) NOT NULL,
  `ITM_UOM` varchar(20) NOT NULL,
  `POS_ITM_UOM` int(11) NOT NULL,
  `ITM_UOM_BS` varchar(20) NOT NULL,
  `POS_ITM_UOM_BS` int(11) NOT NULL,
  `SLS_PRICE` int(11) NOT NULL,
  `TAX_REVERSAL_FLG` varchar(1) NOT NULL,
  `TAX_ID` int(11) NOT NULL,
  `TAX_VAL` int(11) NOT NULL,
  `DISC_TYPE` varchar(1) NOT NULL,
  `DISC_VAL` int(11) NOT NULL,
  `DISC_TOTAL_VAL` int(11) NOT NULL,
  `ITM_TOTAL_VAL` int(11) NOT NULL,
  `SYNC_FLG` varchar(1) NOT NULL,
  `USR_ID_CREATE` int(11) NOT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(11) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `intrm$uom$cls`
--

CREATE TABLE `intrm$uom$cls` (
  `CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(5) NOT NULL,
  `UOM_CLASS_ID` bigint(20) DEFAULT NULL,
  `POS_UOM_CLASS_ID` bigint(20) DEFAULT NULL,
  `UOM_CLASS_NM` varchar(50) DEFAULT NULL,
  `USR_ID_CREATE` int(4) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(4) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL,
  `SYNC_FLG` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$uom$cls`
--

INSERT INTO `intrm$uom$cls` (`CLD_ID`, `SLOC_ID`, `UOM_CLASS_ID`, `POS_UOM_CLASS_ID`, `UOM_CLASS_NM`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `SYNC_FLG`) VALUES
('0000', 1, 9, 1, 'FORCE', 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 11, 2, 'QUANTITY', 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 6, 3, 'POWER', 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 2, 4, 'LENGTH', 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 7, 5, 'PRESSURE', 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 4, 6, 'WEIGHT', 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 5, 7, 'ENERGY', 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 10, 8, 'VOLUME', 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 3, 9, 'TEMPERATURE', 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 8, 10, 'ELECTRIC CHARGE', 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 1, 11, 'TIME', 1, '2014-08-21', NULL, NULL, '1');

-- --------------------------------------------------------

--
-- Table structure for table `intrm$uom$conv$std`
--

CREATE TABLE `intrm$uom$conv$std` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(5) NOT NULL,
  `UOM_ID` varchar(20) DEFAULT NULL,
  `POS_UOM_ID` bigint(20) DEFAULT NULL,
  `UOM_NM` varchar(2) DEFAULT NULL,
  `UOM_DESC` varchar(50) DEFAULT NULL,
  `UOM_CLASS` bigint(20) DEFAULT NULL,
  `BASE_UOM` varchar(1) DEFAULT NULL,
  `CONV_FCTR` decimal(26,6) DEFAULT NULL,
  `ACTV` varchar(1) DEFAULT NULL,
  `INACTV_RESN` varchar(200) DEFAULT NULL,
  `INACTV_DT` date DEFAULT NULL,
  `UOM_STD_ENT_ID` bigint(20) DEFAULT NULL,
  `USR_ID_CREATE` int(4) DEFAULT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(4) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL,
  `SYNC_FLG` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$uom$conv$std`
--

INSERT INTO `intrm$uom$conv$std` (`CLD_ID`, `SLOC_ID`, `UOM_ID`, `POS_UOM_ID`, `UOM_NM`, `UOM_DESC`, `UOM_CLASS`, `BASE_UOM`, `CONV_FCTR`, `ACTV`, `INACTV_RESN`, `INACTV_DT`, `UOM_STD_ENT_ID`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `SYNC_FLG`) VALUES
('0000', 1, 'UOM0000000033', 1, 'KW', 'Kilowatt', 6, 'N', '1000.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000072', 2, '25', '250 GM', 4, 'N', '0.250000', 'Y', NULL, NULL, 1, 1, '2016-11-08', NULL, NULL, '1'),
('0000', 1, 'UOM0000000011', 3, 'CM', 'Centimeter', 2, 'N', '0.010000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000049', 4, 'L', 'Liter', 10, 'Y', '1.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000035', 5, 'BA', 'Bar', 7, 'N', '100000.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000075', 6, '10', '10 GM', 4, 'N', '0.010000', 'Y', NULL, NULL, 1, 1, '2016-11-08', NULL, NULL, '1'),
('0000', 1, 'UOM0000000076', 7, '50', '500 GM', 4, 'N', '0.500000', 'Y', NULL, NULL, 1, 1, '2016-11-08', NULL, NULL, '1'),
('0000', 1, 'UOM0000000069', 8, '50', '500 ML', 10, 'N', '0.500000', 'Y', NULL, NULL, 1, 1, '2016-11-08', NULL, NULL, '1'),
('0000', 1, 'UOM0000000022', 9, 'On', 'Ounces', 4, 'N', '0.028300', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000012', 10, 'MM', 'Millimeters', 2, 'N', '0.001000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000073', 11, '10', '100 GM', 4, 'N', '0.100000', 'Y', NULL, NULL, 1, 1, '2016-11-08', NULL, NULL, '1'),
('0000', 1, 'UOM0000000013', 12, 'NM', 'Nautical Miles', 2, 'N', '1852.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000048', 13, 'PN', 'Poundal', 9, 'N', '0.138000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000056', 14, 'CM', 'Cubic Centimeters', 10, 'N', '0.001000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000041', 15, 'TO', 'Torr', 7, 'N', '1.333000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000046', 16, 'KG', 'Kiligram Force', 9, 'N', '9.807000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000020', 17, 'KG', 'Kilogram', 4, 'Y', '1.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000058', 18, 'IN', 'Cubic Inches', 10, 'N', '0.016300', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000043', 19, 'AB', 'AbCoulomb', 8, 'N', '10.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000074', 20, '50', '50 GM', 4, 'N', '0.050000', 'Y', NULL, NULL, 1, 1, '2016-11-08', NULL, NULL, '1'),
('0000', 1, 'UOM0000000045', 21, 'DY', 'Dyne', 9, 'N', '0.000010', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000028', 22, 'CA', 'Calorie', 5, 'N', '4.187000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000024', 23, 'Pn', 'Pounds', 4, 'N', '0.454000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000016', 24, 'F', 'Fahrenheit', 3, 'N', '-17.770000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000065', 25, 'Dz', 'HALF DOZEN', 11, 'N', '6.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000047', 26, 'KP', 'Kip', 9, 'N', '4448.222000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000004', 27, 'DA', 'Day', 1, 'Y', '1.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000010', 28, 'M', 'Meter', 2, 'Y', '1.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000001', 29, 'SE', 'Second', 1, 'N', '0.000012', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000021', 30, 'MG', 'Miligram', 4, 'N', '0.000001', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000032', 31, 'MW', 'Megawatt', 6, 'N', '1000000.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000071', 32, '5K', '5 KG', 4, 'N', '5.000000', 'Y', NULL, NULL, 1, 1, '2016-11-08', NULL, NULL, '1'),
('0000', 1, 'UOM0000000059', 33, 'M3', 'Cubic Meters', 10, 'N', '1000.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000039', 34, 'MM', 'Millimeter Mercury', 7, 'N', '133.322000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000051', 35, 'PI', 'Pint(U.S. Liq)', 10, 'N', '0.473000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000031', 36, 'HP', 'Horse Power', 6, 'N', '746.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000037', 37, 'KP', 'Kilopascal', 7, 'N', '1000.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000060', 38, 'ML', 'Milliliters', 10, 'N', '0.001000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000030', 39, 'W', 'Watt', 6, 'Y', '1.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000034', 40, 'PA', 'Pascal', 7, 'Y', '1.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000064', 41, 'Pr', 'PAIR', 11, 'N', '2.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000008', 42, 'MI', 'Mile', 2, 'N', '1609.344000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000005', 43, 'WE', 'Week', 1, 'N', '7.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000018', 44, 'K', 'Kelvin', 3, 'N', '-272.150000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000068', 45, '25', '250 ML', 10, 'N', '0.250000', 'Y', NULL, NULL, 1, 1, '2016-11-08', NULL, NULL, '1'),
('0000', 1, 'UOM0000000055', 46, 'BB', 'Barrels', 10, 'N', '158.980000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000003', 47, 'HR', 'Hour', 1, 'N', '0.041667', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000029', 48, 'KW', 'Kilowatt Hours', 5, 'N', '3600000.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000054', 49, 'CU', 'Cups', 10, 'N', '0.237000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000050', 50, 'GA', 'Gallon', 10, 'N', '3.785000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000042', 51, 'CO', 'Coulomb', 8, 'Y', '1.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000063', 52, 'Pc', 'PIECES', 11, 'Y', '1.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000062', 53, 'QT', 'Quarts', 10, 'N', '0.946000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000023', 54, 'Pe', 'Pennyweight', 4, 'N', '0.001550', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000067', 55, '50', '50 KG', 4, 'N', '50.000000', 'Y', NULL, NULL, 1, 1, '2016-11-08', 1, '2016-11-08', '1'),
('0000', 1, 'UOM0000000017', 56, 'R', 'Rankine', 3, 'N', '-272.594000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000019', 57, 'G', 'Gram', 4, 'N', '0.001000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000014', 58, 'YD', 'Yards', 2, 'N', '0.914400', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000066', 59, 'Dz', 'DOZEN', 11, 'N', '12.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000027', 60, 'J', 'Joule', 5, 'Y', '1.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000077', 61, '50', '50 ML', 10, 'N', '0.050000', 'Y', NULL, NULL, 1, 1, '2017-01-17', NULL, NULL, '1'),
('0000', 1, 'UOM0000000044', 62, 'N', 'Newton', 9, 'Y', '1.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000015', 63, 'C', 'Celsius', 3, 'Y', '1.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000053', 64, 'TS', 'Teaspoon', 10, 'N', '0.005000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000070', 65, '10', '100 ML', 10, 'N', '0.100000', 'Y', NULL, NULL, 1, 1, '2016-11-08', NULL, NULL, '1'),
('0000', 1, 'UOM0000000057', 66, 'FT', 'Cubic Feet', 10, 'N', '28.316000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000026', 67, 'To', 'Tones-Metric', 4, 'N', '1000.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000025', 68, 'St', 'Stones', 4, 'N', '6.350000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000052', 69, 'Ta', 'Tablespoon', 10, 'N', '0.015000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000002', 70, 'MI', 'Minute', 1, 'N', '0.000694', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000009', 71, 'KM', 'Kilometer', 2, 'N', '1000.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000007', 72, 'IN', 'Inch', 2, 'N', '0.025000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000038', 73, 'MP', 'Megapascal', 7, 'N', '1000000.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000036', 74, 'AT', 'Atmosphere', 7, 'N', '101325.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000006', 75, 'FT', 'Foot', 2, 'N', '0.305000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000061', 76, 'OZ', 'Ounces', 10, 'N', '0.029500', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1'),
('0000', 1, 'UOM0000000040', 77, 'DC', 'Decibar', 7, 'N', '10000.000000', 'Y', NULL, NULL, NULL, 1, '2014-08-21', NULL, NULL, '1');

-- --------------------------------------------------------

--
-- Table structure for table `intrm$usr`
--

CREATE TABLE `intrm$usr` (
  `SLOC_ID` int(2) NOT NULL,
  `USR_ID` int(11) NOT NULL,
  `POS_USR_ID` int(10) DEFAULT NULL,
  `USR_NAME` varchar(100) NOT NULL,
  `USR_FST_NAME` varchar(20) DEFAULT NULL,
  `USR_LST_NAME` varchar(20) DEFAULT NULL,
  `USR_PHN_NO` int(10) DEFAULT NULL,
  `USR_GNDR` varchar(10) DEFAULT NULL,
  `USR_IMG` varchar(30) DEFAULT NULL,
  `USR_ALIAS` varchar(50) DEFAULT NULL,
  `USR_PWD` varchar(100) NOT NULL,
  `USR_ACTV` varchar(1) NOT NULL,
  `USR_CONTACT_NO` varchar(20) NOT NULL,
  `CREATE_FLG` varchar(1) DEFAULT NULL,
  `UPD_FLG` varchar(1) DEFAULT NULL,
  `DEL_FLG` varchar(1) DEFAULT NULL,
  `USR_ID_CREATE` int(10) NOT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(10) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL,
  `USR_DISC` int(11) DEFAULT NULL,
  `ORG_ID` varchar(20) NOT NULL,
  `WH_ID` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$usr`
--

INSERT INTO `intrm$usr` (`SLOC_ID`, `USR_ID`, `POS_USR_ID`, `USR_NAME`, `USR_FST_NAME`, `USR_LST_NAME`, `USR_PHN_NO`, `USR_GNDR`, `USR_IMG`, `USR_ALIAS`, `USR_PWD`, `USR_ACTV`, `USR_CONTACT_NO`, `CREATE_FLG`, `UPD_FLG`, `DEL_FLG`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `USR_DISC`, `ORG_ID`, `WH_ID`) VALUES
(1, 1, 0, 'SUPERVISOR', 'SUPERVISOR', 'SUPERVISOR', 999999999, 'M', NULL, 'Supervisor', 'piu6', 'Y', '9999999', 'N', NULL, NULL, 1, '2011-01-01', NULL, '2016-11-03', 0, '04', 'WH00004'),
(1, 1, 0, 'SUPERVISOR', 'SUPERVISOR', 'SUPERVISOR', 999999999, 'M', NULL, 'Supervisor', 'piu6', 'Y', '9999999', 'N', NULL, NULL, 1, '2011-01-01', NULL, '2016-11-03', 0, '01', 'WH00001'),
(1, 1, 0, 'SUPERVISOR', 'SUPERVISOR', 'SUPERVISOR', 999999999, 'M', NULL, 'Supervisor', 'piu6', 'Y', '9999999', 'N', NULL, NULL, 1, '2011-01-01', NULL, '2016-11-03', 0, '04', 'WH00005'),
(1, 1, 0, 'SUPERVISOR', 'SUPERVISOR', 'SUPERVISOR', 999999999, 'M', NULL, 'Supervisor', 'piu6', 'Y', '9999999', 'N', NULL, NULL, 1, '2011-01-01', NULL, '2016-11-03', 0, '04', 'WH00001');

-- --------------------------------------------------------

--
-- Table structure for table `intrm$usr$role`
--

CREATE TABLE `intrm$usr$role` (
  `USR_ROLE_CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(2) NOT NULL,
  `USR_ROLE_ID` int(5) NOT NULL,
  `POS_USR_ROLE_ID` int(5) NOT NULL,
  `USR_ROLE_NM` varchar(50) NOT NULL,
  `USR_ROLE_ACTV` varchar(1) NOT NULL,
  `CREATE_FLG` varchar(1) NOT NULL,
  `UPD_FLG` varchar(1) NOT NULL,
  `DEL_FLG` varchar(1) NOT NULL,
  `USR_ID_CREATE` int(10) NOT NULL,
  `USR_ID_CREATE_DT` date DEFAULT NULL,
  `USR_ID_MOD` int(10) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$usr$role`
--

INSERT INTO `intrm$usr$role` (`USR_ROLE_CLD_ID`, `SLOC_ID`, `USR_ROLE_ID`, `POS_USR_ROLE_ID`, `USR_ROLE_NM`, `USR_ROLE_ACTV`, `CREATE_FLG`, `UPD_FLG`, `DEL_FLG`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`) VALUES
('0000', 1, 3, 3, 'HCMUSER', 'Y', 'Y', 'Y', 'Y', 1, '2016-11-03', 1, '2016-11-03'),
('0000', 1, 2, 2, 'FINANCE1', 'Y', 'Y', 'Y', 'Y', 1, '2016-11-03', 1, '2016-11-03'),
('0000', 1, 1, 1, 'DEFAULT ROLE', 'Y', 'Y', 'Y', 'Y', 1, '2014-03-28', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `intrm$usr$role$lnk`
--

CREATE TABLE `intrm$usr$role$lnk` (
  `USR_ROLE_CLD_ID` varchar(4) NOT NULL,
  `SLOC_ID` int(2) NOT NULL,
  `USR_ROLE_ID` int(5) NOT NULL,
  `USR_ORG_ID` varchar(20) DEFAULT NULL,
  `USR_ID` int(10) NOT NULL,
  `CREATE_FLG` varchar(1) NOT NULL,
  `UPD_FLG` varchar(1) NOT NULL,
  `DEL_FLG` varchar(1) NOT NULL,
  `USR_ID_CREATE` int(10) NOT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(10) NOT NULL,
  `USR_ID_MOD_DT` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$usr$role$lnk`
--

INSERT INTO `intrm$usr$role$lnk` (`USR_ROLE_CLD_ID`, `SLOC_ID`, `USR_ROLE_ID`, `USR_ORG_ID`, `USR_ID`, `CREATE_FLG`, `UPD_FLG`, `DEL_FLG`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`) VALUES
('0000', 1, 1, '01', 1, 'Y', 'Y', 'Y', 1, '2014-03-28', 1, '2014-03-28'),
('0000', 1, 2, '01', 1, 'Y', 'Y', 'Y', 1, '2016-11-03', 1, '2016-11-03'),
('0000', 1, 1, '04', 1, 'Y', 'Y', 'Y', 1, '2014-03-28', 1, '2014-03-28'),
('0000', 1, 1, '04', 1, 'Y', 'Y', 'Y', 1, '2014-03-28', 1, '2014-03-28'),
('0000', 1, 1, '04', 1, 'Y', 'Y', 'Y', 1, '2014-03-28', 1, '2014-03-28');

-- --------------------------------------------------------

--
-- Table structure for table `intrm$wh$org`
--

CREATE TABLE `intrm$wh$org` (
  `CLD_ID` varchar(4) NOT NULL DEFAULT '0000',
  `SLOC_ID` int(5) NOT NULL,
  `ORG_ID` varchar(20) NOT NULL COMMENT 'Organization Id like ''01'', ''02''',
  `WH_ID` varchar(20) NOT NULL COMMENT 'Warehouse Id like ''WH00001''',
  `POS_WH_ID` bigint(20) DEFAULT NULL,
  `USR_ID_CREATE` int(4) NOT NULL,
  `USR_ID_CREATE_DT` date NOT NULL,
  `USR_ID_MOD` int(4) DEFAULT NULL,
  `USR_ID_MOD_DT` date DEFAULT NULL,
  `WH_NM` varchar(50) NOT NULL COMMENT 'warehouse name',
  `WH_ONRSHP_TYPE` int(5) DEFAULT NULL COMMENT 'Ownership Type',
  `WH_STRG_TYPE` int(5) DEFAULT NULL COMMENT 'storage type',
  `ADDS_ID` varchar(20) DEFAULT NULL COMMENT 'Address Id',
  `WH_DESC` varchar(1000) DEFAULT NULL COMMENT 'Warehouse Description',
  `ACTV` varchar(1) NOT NULL COMMENT 'Active Flag',
  `INACTV_RESN` varchar(200) DEFAULT NULL COMMENT 'Inactive Reason',
  `INACTV_DT` date DEFAULT NULL COMMENT 'Inactive Date',
  `WH_ENT_ID` int(20) DEFAULT NULL COMMENT 'not in use',
  `HO_ORG_ID` varchar(2) DEFAULT NULL COMMENT 'Head Office Org',
  `PRJ_ID` varchar(40) DEFAULT NULL COMMENT 'POS ID',
  `SYNC_FLG` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intrm$wh$org`
--

INSERT INTO `intrm$wh$org` (`CLD_ID`, `SLOC_ID`, `ORG_ID`, `WH_ID`, `POS_WH_ID`, `USR_ID_CREATE`, `USR_ID_CREATE_DT`, `USR_ID_MOD`, `USR_ID_MOD_DT`, `WH_NM`, `WH_ONRSHP_TYPE`, `WH_STRG_TYPE`, `ADDS_ID`, `WH_DESC`, `ACTV`, `INACTV_RESN`, `INACTV_DT`, `WH_ENT_ID`, `HO_ORG_ID`, `PRJ_ID`, `SYNC_FLG`) VALUES
('0000', 1, '04', 'WH00005', 0, 1, '2016-12-21', NULL, NULL, 'TAT-POS', 229, 210, 'ADDS0000000023', NULL, 'Y', NULL, NULL, NULL, '01', '0000.01.04.0001.00XR.00.1UKxc4ge73', '1'),
('0000', 1, '01', 'WH00001', 0, 1, '2016-12-21', NULL, NULL, 'HO WAREHOUSE', 229, 210, 'ADDS0000000003', NULL, 'Y', NULL, NULL, NULL, '01', 'PROJ0000', '1'),
('0000', 1, '04', 'WH00004', 0, 1, '2016-11-08', NULL, NULL, 'GOV EXB', 229, 210, 'ADDS0000000001', NULL, 'Y', NULL, NULL, NULL, '01', '0000.01.04.0001.00XR.00.1UKxc44uJN', '1'),
('0000', 1, '04', 'WH00001', 0, 1, '2016-11-08', 1, '2016-11-08', 'GOV GODOWN', 229, 210, 'ADDS0000000001', NULL, 'Y', NULL, NULL, NULL, '01', '0000.01.04.0001.00XR.00.1UKxc44uJN', '1');

-- --------------------------------------------------------

--
-- Table structure for table `wh$sec$usr`
--

CREATE TABLE `wh$sec$usr` (
  `CLD_ID` varchar(4) DEFAULT NULL,
  `SLOC_ID` int(2) DEFAULT NULL,
  `ORG_ID` varchar(2) DEFAULT NULL,
  `USR_ID` int(10) DEFAULT NULL,
  `WH_ID` varchar(20) DEFAULT NULL,
  `DFLT_WH` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wh$sec$usr`
--

INSERT INTO `wh$sec$usr` (`CLD_ID`, `SLOC_ID`, `ORG_ID`, `USR_ID`, `WH_ID`, `DFLT_WH`) VALUES
('0000', 1, '04', 1, 'WH00004', 'N'),
('0000', 1, '04', 1, 'WH00001', 'N'),
('0000', 1, '04', 1, 'WH00005', NULL),
('0000', 1, '01', 1, 'WH00001', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `intrm$eo`
--
ALTER TABLE `intrm$eo`
  ADD PRIMARY KEY (`SN`);

--
-- Indexes for table `intrm$mm$mtl$rcpt`
--
ALTER TABLE `intrm$mm$mtl$rcpt`
  ADD PRIMARY KEY (`SN`);

--
-- Indexes for table `intrm$mm$mtl$rcpt$bin`
--
ALTER TABLE `intrm$mm$mtl$rcpt$bin`
  ADD PRIMARY KEY (`SN`);

--
-- Indexes for table `intrm$mm$mtl$rcpt$itm`
--
ALTER TABLE `intrm$mm$mtl$rcpt$itm`
  ADD PRIMARY KEY (`SN`);

--
-- Indexes for table `intrm$mm$mtl$rcpt$lot`
--
ALTER TABLE `intrm$mm$mtl$rcpt$lot`
  ADD PRIMARY KEY (`SN`);

--
-- Indexes for table `intrm$mm$mtl$rcpt$src`
--
ALTER TABLE `intrm$mm$mtl$rcpt$src`
  ADD PRIMARY KEY (`SN`);

--
-- Indexes for table `intrm$sls$inv`
--
ALTER TABLE `intrm$sls$inv`
  ADD PRIMARY KEY (`SN`);

--
-- Indexes for table `intrm$sls$inv$itm`
--
ALTER TABLE `intrm$sls$inv$itm`
  ADD PRIMARY KEY (`SN`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `intrm$eo`
--
ALTER TABLE `intrm$eo`
  MODIFY `SN` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `intrm$mm$mtl$rcpt`
--
ALTER TABLE `intrm$mm$mtl$rcpt`
  MODIFY `SN` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `intrm$mm$mtl$rcpt$bin`
--
ALTER TABLE `intrm$mm$mtl$rcpt$bin`
  MODIFY `SN` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `intrm$mm$mtl$rcpt$itm`
--
ALTER TABLE `intrm$mm$mtl$rcpt$itm`
  MODIFY `SN` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `intrm$mm$mtl$rcpt$lot`
--
ALTER TABLE `intrm$mm$mtl$rcpt$lot`
  MODIFY `SN` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `intrm$mm$mtl$rcpt$src`
--
ALTER TABLE `intrm$mm$mtl$rcpt$src`
  MODIFY `SN` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `intrm$sls$inv`
--
ALTER TABLE `intrm$sls$inv`
  MODIFY `SN` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `intrm$sls$inv$itm`
--
ALTER TABLE `intrm$sls$inv$itm`
  MODIFY `SN` bigint(20) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
