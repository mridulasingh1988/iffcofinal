-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2017 at 08:19 AM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iffco_final`
--

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS `emptyPOSdb`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `emptyPOSdb` ()  BEGIN
TRUNCATE TABLE `sma_currencies`;
TRUNCATE TABLE `sma_warehouses`;
DELETE FROM `sma_users` WHERE id>2;
TRUNCATE TABLE `sma_tax_rates`;
TRUNCATE TABLE `sma_categories`;
DELETE FROM `sma_companies` WHERE id>1;
TRUNCATE TABLE `sma_products`;
TRUNCATE TABLE `sma_purchase_items`;
TRUNCATE TABLE `sma_warehouses_products`;
TRUNCATE TABLE `sma_sales`;
TRUNCATE TABLE `sma_sale_items`;
TRUNCATE TABLE `sma_payments`;
TRUNCATE TABLE `sma_costing`;
TRUNCATE TABLE `sma_user_logins`;    
TRUNCATE TABLE `sma_transfers`;
TRUNCATE TABLE `sma_transfer_items`;
TRUNCATE TABLE `sma_till`;
TRUNCATE TABLE `sma_suspended_items`;        
TRUNCATE TABLE `sma_suspended_bills`;
TRUNCATE TABLE `sma_subcategories`;
TRUNCATE TABLE `sma_sessions`;             
UPDATE `sma_order_ref` SET `pos` = '1', `pay` = '1' WHERE `sma_order_ref`.`ref_id` = 1;
TRUNCATE TABLE `sma_return_sales`;
TRUNCATE TABLE `sma_return_items`;
TRUNCATE TABLE `sma_purchases`;
TRUNCATE TABLE `sma_pos_register`;
TRUNCATE TABLE `sma_pos_cash_drawer`;      
TRUNCATE TABLE `sma_gift_cards`;
TRUNCATE TABLE `sma_expenses`;
TRUNCATE TABLE `sma_pos_cash_drawer`;
TRUNCATE TABLE `sma_combo_items`;
TRUNCATE TABLE `sma_adjustments`;
TRUNCATE TABLE `sma_quotes`;
TRUNCATE TABLE `sma_quote_items`;
TRUNCATE TABLE `sma_quotes`;
TRUNCATE TABLE `sma_quotes`;
TRUNCATE TABLE `sma_quotes`;
TRUNCATE TABLE `sma_companies`;
TRUNCATE TABLE `sma_users`;
TRUNCATE TABLE `sma_bin`;
TRUNCATE TABLE `sma_drft_po`;
TRUNCATE TABLE `sma_drft_po_dlv_schdl`;
TRUNCATE TABLE `sma_ds_att`;
TRUNCATE TABLE `sma_ds_att_reln`;
TRUNCATE TABLE `sma_ds_att_type`;
TRUNCATE TABLE `sma_emrs`;
TRUNCATE TABLE `sma_emrs_doc_src`;
TRUNCATE TABLE `sma_emrs_item`;
TRUNCATE TABLE `sma_gp`;
TRUNCATE TABLE `sma_gp_item`;
TRUNCATE TABLE `sma_gp_item_lot`;
TRUNCATE TABLE `sma_gp_lot_bin`;
TRUNCATE TABLE `sma_gp_src`;
TRUNCATE TABLE `sma_gp_srl`;
TRUNCATE TABLE `sma_item_profile`;
TRUNCATE TABLE `sma_item_stk_lot`;
TRUNCATE TABLE `sma_item_stk_lot_bin`;
TRUNCATE TABLE `sma_item_stk_profile`;
TRUNCATE TABLE `sma_item_stk_srl`;
TRUNCATE TABLE `sma_item_warehouse_details`;
TRUNCATE TABLE `sma_mrn_rcpt_item`;
TRUNCATE TABLE `sma_mrn_rcpt_item_lot`;
TRUNCATE TABLE `sma_mrn_rcpt_lot_bin`;
TRUNCATE TABLE `sma_mrn_rcpt_src`;
TRUNCATE TABLE `sma_mrn_receipt`;
TRUNCATE TABLE `sma_segment`;
TRUNCATE TABLE `sma_segment_ent`;
TRUNCATE TABLE `sma_stock_bin`;
TRUNCATE TABLE `sma_stock_item`;
TRUNCATE TABLE `sma_stock_lot`;
TRUNCATE TABLE `sma_uom_cls`;
TRUNCATE TABLE `sma_uom_conv_std`;
TRUNCATE TABLE `sma_cons_item_stk_lot`;
TRUNCATE TABLE `sma_cons_item_stk_lot_bin`;
TRUNCATE TABLE `sma_cons_item_stk_profile`;
TRUNCATE TABLE `sma_sale_item_lot`;
TRUNCATE TABLE `sma_sale_item_bin`;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sma_adjustments`
--

DROP TABLE IF EXISTS `sma_adjustments`;
CREATE TABLE `sma_adjustments` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_bin`
--

DROP TABLE IF EXISTS `sma_bin`;
CREATE TABLE `sma_bin` (
  `id` bigint(20) NOT NULL,
  `org_id` varchar(20) NOT NULL,
  `segment_id` bigint(20) DEFAULT NULL,
  `warehouse_id` bigint(20) DEFAULT NULL,
  `wh_id` varchar(20) NOT NULL,
  `bin_id` varchar(20) NOT NULL,
  `bin_nm` varchar(50) NOT NULL,
  `bin_desc` varchar(200) NOT NULL,
  `storage_type` int(20) NOT NULL,
  `blocked` varchar(20) NOT NULL,
  `blk_resn` varchar(200) NOT NULL,
  `blk_dt_frm` date NOT NULL,
  `blk_dt_to` date NOT NULL,
  `bin_ent_id` int(20) NOT NULL,
  `create_date` date NOT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_calendar`
--

DROP TABLE IF EXISTS `sma_calendar`;
CREATE TABLE `sma_calendar` (
  `date` date NOT NULL,
  `data` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_captcha`
--

DROP TABLE IF EXISTS `sma_captcha`;
CREATE TABLE `sma_captcha` (
  `captcha_id` bigint(13) UNSIGNED NOT NULL,
  `captcha_time` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(16) CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `word` varchar(20) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_categories`
--

DROP TABLE IF EXISTS `sma_categories`;
CREATE TABLE `sma_categories` (
  `id` int(11) NOT NULL,
  `code` varchar(55) NOT NULL,
  `name` varchar(55) NOT NULL,
  `parent_id` int(55) NOT NULL DEFAULT '0',
  `level` tinyint(2) NOT NULL DEFAULT '1',
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `grp_id_parent` varchar(55) DEFAULT NULL,
  `image` varchar(55) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_categories_new`
--

DROP TABLE IF EXISTS `sma_categories_new`;
CREATE TABLE `sma_categories_new` (
  `id` int(11) NOT NULL,
  `code` varchar(55) NOT NULL,
  `name` varchar(55) NOT NULL,
  `parent_id` bigint(20) NOT NULL DEFAULT '0',
  `level` tinyint(2) NOT NULL DEFAULT '1',
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `image` varchar(55) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_combo_items`
--

DROP TABLE IF EXISTS `sma_combo_items`;
CREATE TABLE `sma_combo_items` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `item_code` varchar(20) NOT NULL,
  `quantity` decimal(12,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_companies`
--

DROP TABLE IF EXISTS `sma_companies`;
CREATE TABLE `sma_companies` (
  `id` bigint(20) NOT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `group_name` varchar(20) NOT NULL,
  `customer_group_id` int(11) DEFAULT NULL,
  `customer_group_name` varchar(100) DEFAULT NULL,
  `name` varchar(55) NOT NULL,
  `father_name` varchar(30) NOT NULL,
  `company` varchar(255) NOT NULL,
  `vat_no` varchar(100) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(55) NOT NULL,
  `state` varchar(55) DEFAULT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `id_proof` varchar(20) NOT NULL,
  `id_proof_no` varchar(20) NOT NULL,
  `loyalty_card_id` bigint(16) NOT NULL,
  `nominees1` varchar(30) NOT NULL,
  `dob_nominees1` date NOT NULL,
  `nominees2` varchar(30) DEFAULT NULL,
  `dob_nominees2` date DEFAULT NULL,
  `nominees3` varchar(30) DEFAULT NULL,
  `dob_nominees3` date DEFAULT NULL,
  `nominees4` varchar(30) DEFAULT NULL,
  `dob_nominees4` date DEFAULT NULL,
  `nominees5` varchar(30) DEFAULT NULL,
  `dob_nominees5` date DEFAULT NULL,
  `cf1` varchar(100) DEFAULT NULL,
  `cf2` varchar(100) DEFAULT NULL,
  `cf3` varchar(100) DEFAULT NULL,
  `cf4` varchar(100) DEFAULT NULL,
  `cf5` varchar(100) DEFAULT NULL,
  `cf6` varchar(100) DEFAULT NULL,
  `invoice_footer` text,
  `payment_term` int(11) DEFAULT '0',
  `logo` varchar(255) DEFAULT 'logo.png',
  `award_points` int(11) DEFAULT '0',
  `upd_flg` tinyint(1) DEFAULT NULL,
  `eo_type` varchar(1) DEFAULT NULL,
  `eo_id` varchar(20) DEFAULT NULL,
  `org_id` varchar(50) DEFAULT NULL,
  `wh_id` varchar(20) DEFAULT NULL,
  `biller_id` bigint(20) DEFAULT NULL,
  `segment_id` int(11) DEFAULT NULL,
  `sync_flg` varchar(1) DEFAULT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `sma_companies`
--
DROP TRIGGER IF EXISTS `delete_sma_companies`;
DELIMITER $$
CREATE TRIGGER `delete_sma_companies` BEFORE DELETE ON `sma_companies` FOR EACH ROW BEGIN IF OLD.id = 1 THEN

SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot delete locked record'; 

END IF; 

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sma_cons_item_stk_lot`
--

DROP TABLE IF EXISTS `sma_cons_item_stk_lot`;
CREATE TABLE `sma_cons_item_stk_lot` (
  `id` bigint(20) NOT NULL,
  `org_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `segment_id` int(11) NOT NULL,
  `wh_id` int(11) NOT NULL,
  `lot_no` varchar(50) DEFAULT NULL,
  `item_stk_id` int(11) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `batch_no` varchar(255) NOT NULL,
  `item_uom_id` varchar(100) DEFAULT NULL,
  `total_stk` decimal(26,6) NOT NULL,
  `rejected_stk` decimal(26,6) NOT NULL,
  `mfg_date` datetime NOT NULL,
  `exp_date` datetime NOT NULL,
  `reworkable_stk` decimal(26,6) NOT NULL,
  `created_date` datetime NOT NULL,
  `create_flg` varchar(1) DEFAULT '1',
  `upd_flg` varchar(1) DEFAULT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_cons_item_stk_lot_bin`
--

DROP TABLE IF EXISTS `sma_cons_item_stk_lot_bin`;
CREATE TABLE `sma_cons_item_stk_lot_bin` (
  `id` bigint(20) NOT NULL,
  `org_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `segment_id` int(11) NOT NULL,
  `wh_id` int(11) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `lot_id` bigint(20) NOT NULL,
  `bin_id` varchar(255) NOT NULL,
  `item_uom_id` varchar(200) DEFAULT NULL,
  `total_stk` decimal(26,6) NOT NULL,
  `rejected_stk` decimal(26,6) NOT NULL,
  `reworkable_stk` decimal(26,6) NOT NULL,
  `created_date` datetime NOT NULL,
  `create_flg` varchar(1) DEFAULT '1',
  `upd_flg` varchar(1) DEFAULT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_cons_item_stk_profile`
--

DROP TABLE IF EXISTS `sma_cons_item_stk_profile`;
CREATE TABLE `sma_cons_item_stk_profile` (
  `id` bigint(20) NOT NULL,
  `org_id` varchar(20) DEFAULT NULL,
  `warehouse_id` int(11) NOT NULL,
  `segment_id` int(11) NOT NULL,
  `wh_id` varchar(20) DEFAULT NULL,
  `fy_id` int(11) DEFAULT NULL,
  `item_id` bigint(20) NOT NULL,
  `item_uom_id` varchar(100) NOT NULL,
  `total_stk` decimal(26,6) NOT NULL,
  `available_stk` decimal(26,6) NOT NULL,
  `rejected_stk` decimal(26,6) NOT NULL,
  `rework_stk` decimal(26,6) NOT NULL,
  `ordered_stk` decimal(26,6) NOT NULL,
  `purchase_price` decimal(26,6) NOT NULL,
  `sales_price` decimal(26,6) NOT NULL,
  `created_date` datetime NOT NULL,
  `create_flg` varchar(1) DEFAULT '1',
  `upd_flg` varchar(1) DEFAULT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_costing`
--

DROP TABLE IF EXISTS `sma_costing`;
CREATE TABLE `sma_costing` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `sale_item_id` int(11) NOT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `purchase_item_id` int(11) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `purchase_net_unit_cost` decimal(25,4) DEFAULT NULL,
  `purchase_unit_cost` decimal(25,4) DEFAULT NULL,
  `sale_net_unit_price` decimal(25,4) NOT NULL,
  `sale_unit_price` decimal(25,4) NOT NULL,
  `quantity_balance` decimal(15,4) DEFAULT NULL,
  `inventory` tinyint(1) DEFAULT '0',
  `overselling` tinyint(1) DEFAULT '0',
  `option_id` int(11) DEFAULT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_country`
--

DROP TABLE IF EXISTS `sma_country`;
CREATE TABLE `sma_country` (
  `country_id` int(11) NOT NULL,
  `country_url` varchar(255) NOT NULL,
  `country_name` varchar(128) NOT NULL,
  `country_code` varchar(200) NOT NULL,
  `country_status` tinyint(1) NOT NULL DEFAULT '1',
  `currency_symbol` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `currency_code` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `country_default` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `sma_country` (`country_id`, `country_url`, `country_name`, `country_code`, `country_status`, `currency_symbol`, `currency_code`, `country_default`) VALUES
(1, '', 'Afghanistan', 'AF', 1, '', '', 0),
(2, '', 'Albania', 'AL', 1, '', '', 0),
(3, '', 'Algeria', 'DZ', 1, '', '', 0),
(4, '', 'American Samoa', 'AS', 1, '', '', 0),
(5, '', 'Andorra', 'AD', 1, '', '', 0),
(6, '', 'Angola', 'AO', 1, '', '', 0),
(7, '', 'Anguilla', 'AI', 1, '', '', 0),
(8, '', 'Antarctica', 'AQ', 1, '', '', 0),
(9, '', 'Antigua and Barbuda', 'AG', 1, '', '', 0),
(10, '', 'Argentina', 'AR', 1, '', '', 0),
(11, '', 'Armenia', 'AM', 1, '', '', 0),
(12, '', 'Aruba', 'AW', 1, '', '', 0),
(13, '', 'Australia', 'AU', 1, '', '', 0),
(14, '', 'Austria', 'AT', 1, '', '', 0),
(15, '', 'Azerbaijan', 'AZ', 1, '', '', 0),
(16, '', 'Bahamas', 'BS', 1, '', '', 0),
(17, '', 'Bahrain', 'BH', 1, '', '', 0),
(18, '', 'Bangladesh', 'BD', 1, '', '', 0),
(19, '', 'Barbados', 'BB', 1, '', '', 0),
(20, '', 'Belarus', 'BY', 1, '', '', 0),
(21, '', 'Belgium', 'BE', 1, '', '', 0),
(22, '', 'Belize', 'BZ', 1, '', '', 0),
(23, '', 'Benin', 'BJ', 1, '', '', 0),
(24, '', 'Bermuda', 'BM', 1, '', '', 0),
(25, '', 'Bhutan', 'BT', 1, '', '', 0),
(26, '', 'Bolivia', 'BO', 1, '', '', 0),
(27, '', 'Bosnia and Herzegovina', 'BA', 1, '', '', 0),
(28, '', 'Botswana', 'BW', 1, '', '', 0),
(29, '', 'Bouvet Island', 'BV', 1, '', '', 0),
(30, '', 'Brazil', 'BR', 1, '', '', 0),
(31, '', 'British Indian Ocean Territory', 'IO', 1, '', '', 0),
(32, '', 'Brunei Darussalam', 'BN', 1, '', '', 0),
(33, '', 'Bulgaria', 'BG', 1, '', '', 0),
(34, '', 'Burkina Faso', 'BF', 1, '', '', 0),
(35, '', 'Burundi', 'BI', 1, '', '', 0),
(36, '', 'Cambodia', 'KH', 1, '', '', 0),
(37, '', 'Cameroon', 'CM', 1, '', '', 0),
(38, '', 'Canada', 'CA', 1, '', '', 0),
(39, '', 'Cape Verde', 'CV', 1, '', '', 0),
(40, '', 'Cayman Islands', 'KY', 1, '', '', 0),
(41, '', 'Central African Republic', 'CF', 1, '', '', 0),
(42, '', 'Chad', 'TD', 1, '', '', 0),
(43, '', 'Chile', 'CL', 1, '', '', 0),
(44, '', 'China', 'CN', 1, '', '', 0),
(45, '', 'Christmas Island', 'CX', 1, '', '', 0),
(46, '', 'Cocos (Keeling) Islands', 'CC', 1, '', '', 0),
(47, '', 'Colombia', 'CO', 1, '', '', 0),
(48, '', 'Comoros', 'KM', 1, '', '', 0),
(49, '', 'Congo', 'CG', 1, '', '', 0),
(50, '', 'Cook Islands', 'CK', 1, '', '', 0),
(51, '', 'Costa Rica', 'CR', 1, '', '', 0),
(52, '', 'Cote D''Ivoire', 'CI', 1, '', '', 0),
(53, '', 'Croatia', 'HR', 1, '', '', 0),
(54, '', 'Cuba', 'CU', 1, '', '', 0),
(55, '', 'Cyprus', 'CY', 1, '', '', 0),
(56, '', 'Czech Republic', 'CZ', 1, '', '', 0),
(57, '', 'Denmark', 'DK', 1, '', '', 0),
(58, '', 'Djibouti', 'DJ', 1, '', '', 0),
(59, '', 'Dominica', 'DM', 1, '', '', 0),
(60, '', 'Dominican Republic', 'DO', 1, '', '', 0),
(61, '', 'East Timor', 'TL', 1, '', '', 0),
(62, '', 'Ecuador', 'EC', 1, '', '', 0),
(63, '', 'Egypt', 'EG', 1, '', '', 0),
(64, '', 'El Salvador', 'SV', 1, '', '', 0),
(65, '', 'Equatorial Guinea', 'GQ', 1, '', '', 0),
(66, '', 'Eritrea', 'ER', 1, '', '', 0),
(67, '', 'Estonia', 'EE', 1, '', '', 0),
(68, '', 'Ethiopia', 'ET', 1, '', '', 0),
(69, '', 'Falkland Islands (Malvinas)', 'FK', 1, '', '', 0),
(70, '', 'Faroe Islands', 'FO', 1, '', '', 0),
(71, '', 'Fiji', 'FJ', 1, '', '', 0),
(72, '', 'Finland', 'FI', 1, '', '', 0),
(74, '', 'France, Metropolitan', 'FR', 1, '', '', 0),
(75, '', 'French Guiana', 'GF', 1, '', '', 0),
(76, '', 'French Polynesia', 'PF', 1, '', '', 0),
(77, '', 'French Southern Territories', 'TF', 1, '', '', 0),
(78, '', 'Gabon', 'GA', 1, '', '', 0),
(79, '', 'Gambia', 'GM', 1, '', '', 0),
(80, '', 'Georgia', 'GE', 1, '', '', 0),
(81, '', 'Germany', 'DE', 1, '', '', 0),
(82, '', 'Ghana', 'GH', 1, '', '', 0),
(83, '', 'Gibraltar', 'GI', 1, '', '', 0),
(84, '', 'Greece', 'GR', 1, '', '', 0),
(85, '', 'Greenland', 'GL', 1, '', '', 0),
(86, '', 'Grenada', 'GD', 1, '', '', 0),
(87, '', 'Guadeloupe', 'GP', 1, '', '', 0),
(88, '', 'Guam', 'GU', 1, '', '', 0),
(89, '', 'Guatemala', 'GT', 1, '', '', 0),
(90, '', 'Guinea', 'GN', 1, '', '', 0),
(91, '', 'Guinea-Bissau', 'GW', 1, '', '', 0),
(92, '', 'Guyana', 'GY', 1, '', '', 0),
(93, '', 'Haiti', 'HT', 1, '', '', 0),
(94, '', 'Heard and Mc Donald Islands', 'HM', 1, '', '', 0),
(95, '', 'Honduras', 'HN', 1, '', '', 0),
(96, '', 'Hong Kong', 'HK', 1, '', '', 0),
(97, '', 'Hungary', 'HU', 1, '', '', 0),
(98, '', 'Iceland', 'IS', 1, '', '', 0),
(99, '', 'India', 'IN', 1, '', '', 1),
(100, '', 'Indonesia', 'ID', 1, '', '', 0),
(101, '', 'Iran (Islamic Republic of)', 'IR', 1, '', '', 0),
(102, '', 'Iraq', 'IQ', 1, '', '', 0),
(103, '', 'Ireland', 'IE', 1, '', '', 0),
(104, '', 'Israel', 'IL', 1, '', '', 0),
(105, '', 'Italy', 'IT', 1, '', '', 0),
(106, '', 'Jamaica', 'JM', 1, '', '', 0),
(107, '', 'Japan', 'JP', 1, '', '', 0),
(108, '', 'Jordan', 'JO', 1, '', '', 0),
(109, '', 'Kazakhstan', 'KZ', 1, '', '', 0),
(110, '', 'Kenya', 'KE', 1, '', '', 0),
(111, '', 'Kiribati', 'KI', 1, '', '', 0),
(112, '', 'North Korea', 'KP', 1, '', '', 0),
(113, '', 'Korea, Republic of', 'KR', 1, '', '', 0),
(114, '', 'Kuwait', 'KW', 1, '', '', 0),
(115, '', 'Kyrgyzstan', 'KG', 1, '', '', 0),
(116, '', 'Lao People''s Democratic Republic', 'LA', 1, '', '', 0),
(117, '', 'Latvia', 'LV', 1, '', '', 0),
(118, '', 'Lebanon', 'LB', 1, '', '', 0),
(119, '', 'Lesotho', 'LS', 1, '', '', 0),
(120, '', 'Liberia', 'LR', 1, '', '', 0),
(121, '', 'Libyan Arab Jamahiriya', 'LY', 1, '', '', 0),
(122, '', 'Liechtenstein', 'LI', 1, '', '', 0),
(123, '', 'Lithuania', 'LT', 1, '', '', 0),
(124, '', 'Luxembourg', 'LU', 1, '', '', 0),
(125, '', 'Macau', 'MO', 1, '', '', 0),
(126, '', 'FYROM', 'MK', 1, '', '', 0),
(127, '', 'Madagascar', 'MG', 1, '', '', 0),
(128, '', 'Malawi', 'MW', 1, '', '', 0),
(129, '', 'Malaysia', 'MY', 1, '', '', 0),
(130, '', 'Maldives', 'MV', 1, '', '', 0),
(131, '', 'Mali', 'ML', 1, '', '', 0),
(132, '', 'Malta', 'MT', 1, '', '', 0),
(133, '', 'Marshall Islands', 'MH', 1, '', '', 0),
(134, '', 'Martinique', 'MQ', 1, '', '', 0),
(135, '', 'Mauritania', 'MR', 1, '', '', 0),
(136, '', 'Mauritius', 'MU', 1, '', '', 0),
(137, '', 'Mayotte', 'YT', 1, '', '', 0),
(138, '', 'Mexico', 'MX', 1, '', '', 0),
(139, '', 'Micronesia, Federated States of', 'FM', 1, '', '', 0),
(140, '', 'Moldova, Republic of', 'MD', 1, '', '', 0),
(141, '', 'Monaco', 'MC', 1, '', '', 0),
(142, '', 'Mongolia', 'MN', 1, '', '', 0),
(143, '', 'Montserrat', 'MS', 1, '', '', 0),
(144, '', 'Morocco', 'MA', 1, '', '', 0),
(145, '', 'Mozambique', 'MZ', 1, '', '', 0),
(146, '', 'Myanmar', 'MM', 1, '', '', 0),
(147, '', 'Namibia', 'NA', 1, '', '', 0),
(148, '', 'Nauru', 'NR', 1, '', '', 0),
(149, '', 'Nepal', 'NP', 1, '', '', 0),
(150, '', 'Netherlands', 'NL', 1, '', '', 0),
(151, '', 'Netherlands Antilles', 'AN', 1, '', '', 0),
(152, '', 'New Caledonia', 'NC', 1, '', '', 0),
(153, '', 'New Zealand', 'NZ', 1, '', '', 0),
(154, '', 'Nicaragua', 'NI', 1, '', '', 0),
(155, '', 'Niger', 'NE', 1, '', '', 0),
(156, '', 'Nigeria', 'NG', 1, '', '', 0),
(157, '', 'Niue', 'NU', 1, '', '', 0),
(158, '', 'Norfolk Island', 'NF', 1, '', '', 0),
(159, '', 'Northern Mariana Islands', 'MP', 1, '', '', 0),
(160, '', 'Norway', 'NO', 1, '', '', 0),
(161, '', 'Oman', 'OM', 1, '', '', 0),
(162, '', 'Pakistan', 'PK', 1, '', '', 0),
(163, '', 'Palau', 'PW', 1, '', '', 0),
(164, '', 'Panama', 'PA', 1, '', '', 0),
(165, '', 'Papua New Guinea', 'PG', 1, '', '', 0),
(166, '', 'Paraguay', 'PY', 1, '', '', 0),
(167, '', 'Peru', 'PE', 1, '', '', 0),
(168, '', 'Philippines', 'PH', 1, '', '', 0),
(169, '', 'Pitcairn', 'PN', 1, '', '', 0),
(170, '', 'Poland', 'PL', 1, '', '', 0),
(171, '', 'Portugal', 'PT', 1, '', '', 0),
(172, '', 'Puerto Rico', 'PR', 1, '', '', 0),
(173, '', 'Qatar', 'QA', 1, '', '', 0),
(174, '', 'Reunion', 'RE', 1, '', '', 0),
(175, '', 'Romania', 'RO', 1, '', '', 0),
(176, '', 'Russian Federation', 'RU', 1, '', '', 0),
(177, '', 'Rwanda', 'RW', 1, '', '', 0),
(178, '', 'Saint Kitts and Nevis', 'KN', 1, '', '', 0),
(179, '', 'Saint Lucia', 'LC', 1, '', '', 0),
(180, '', 'Saint Vincent and the Grenadines', 'VC', 1, '', '', 0),
(181, '', 'Samoa', 'WS', 1, '', '', 0),
(182, '', 'San Marino', 'SM', 1, '', '', 0),
(183, '', 'Sao Tome and Principe', 'ST', 1, '', '', 0),
(184, '', 'Saudi Arabia', 'SA', 1, '', '', 0),
(185, '', 'Senegal', 'SN', 1, '', '', 0),
(186, '', 'Seychelles', 'SC', 1, '', '', 0),
(187, '', 'Sierra Leone', 'SL', 1, '', '', 0),
(188, '', 'Singapore', 'SG', 1, '', '', 0),
(189, '', 'Slovak Republic', 'SK', 1, '', '', 0),
(190, '', 'Slovenia', 'SI', 1, '', '', 0),
(191, '', 'Solomon Islands', 'SB', 1, '', '', 0),
(192, '', 'Somalia', 'SO', 1, '', '', 0),
(193, '', 'South Africa', 'ZA', 1, '', '', 0),
(194, '', 'South Georgia &amp; South Sandwich Islands', 'GS', 1, '', '', 0),
(195, '', 'Spain', 'ES', 1, '', '', 0),
(196, '', 'Sri Lanka', 'LK', 1, '', '', 0),
(197, '', 'St. Helena', 'SH', 1, '', '', 0),
(198, '', 'St. Pierre and Miquelon', 'PM', 1, '', '', 0),
(199, '', 'Sudan', 'SD', 1, '', '', 0),
(200, '', 'Suriname', 'SR', 1, '', '', 0),
(201, '', 'Svalbard and Jan Mayen Islands', 'SJ', 1, '', '', 0),
(202, '', 'Swaziland', 'SZ', 1, '', '', 0),
(203, '', 'Sweden', 'SE', 1, '', '', 0),
(204, '', 'Switzerland', 'CH', 1, '', '', 0),
(205, '', 'Syrian Arab Republic', 'SY', 1, '', '', 0),
(206, '', 'Taiwan', 'TW', 1, '', '', 0),
(207, '', 'Tajikistan', 'TJ', 1, '', '', 0),
(208, '', 'Tanzania, United Republic of', 'TZ', 1, '', '', 0),
(209, '', 'Thailand', 'TH', 1, '', '', 0),
(210, '', 'Togo', 'TG', 1, '', '', 0),
(211, '', 'Tokelau', 'TK', 1, '', '', 0),
(212, '', 'Tonga', 'TO', 1, '', '', 0),
(213, '', 'Trinidad and Tobago', 'TT', 1, '', '', 0),
(214, '', 'Tunisia', 'TN', 1, '', '', 0),
(215, '', 'Turkey', 'TR', 1, '', '', 0),
(216, '', 'Turkmenistan', 'TM', 1, '', '', 0),
(217, '', 'Turks and Caicos Islands', 'TC', 1, '', '', 0),
(218, '', 'Tuvalu', 'TV', 1, '', '', 0),
(219, '', 'Uganda', 'UG', 1, '', '', 0),
(220, '', 'Ukraine', 'UA', 1, '', '', 0),
(221, '', 'United Arab Emirates', 'AE', 1, '', '', 0),
(222, '', 'United Kingdom', 'GB', 1, '', '', 0),
(223, '', 'United States', 'US', 1, '$', 'USD', 0),
(224, '', 'United States Minor Outlying Islands', 'UM', 1, '', '', 0),
(225, '', 'Uruguay', 'UY', 1, '', '', 0),
(226, '', 'Uzbekistan', 'UZ', 1, '', '', 0),
(227, '', 'Vanuatu', 'VU', 1, '', '', 0),
(228, '', 'Vatican City State (Holy See)', 'VA', 1, '', '', 0),
(229, '', 'Venezuela', 'VE', 1, '', '', 0),
(230, '', 'Viet Nam', 'VN', 1, '', '', 0),
(231, '', 'Virgin Islands (British)', 'VG', 1, '', '', 0),
(232, '', 'Virgin Islands (U.S.)', 'VI', 1, '', '', 0),
(233, '', 'Wallis and Futuna Islands', 'WF', 1, '', '', 0),
(234, '', 'Western Sahara', 'EH', 1, '', '', 0),
(235, '', 'Yemen', 'YE', 1, '', '', 0),
(237, '', 'Democratic Republic of Congo', 'CD', 1, '', '', 0),
(238, '', 'Zambia', 'ZM', 1, '', '', 0),
(239, '', 'Zimbabwe', 'ZW', 1, '', '', 0),
(242, '', 'Montenegro', 'ME', 1, '', '', 0),
(243, '', 'Serbia', 'RS', 1, '', '', 0),
(244, '', 'Aaland Islands', 'AX', 1, '', '', 0),
(245, '', 'Bonaire, Sint Eustatius and Saba', 'BQ', 1, '', '', 0),
(246, '', 'Curacao', 'CW', 1, '', '', 0),
(247, '', 'Palestinian Territory, Occupied', 'PS', 1, '', '', 0),
(248, '', 'South Sudan', 'SS', 1, '', '', 0),
(249, '', 'St. Barthelemy', 'BL', 1, '', '', 0),
(250, '', 'St. Martin (French part)', 'MF', 1, '', '', 0),
(251, '', 'Canary Islands', 'IC', 1, '', '', 0),
(252, '', 'Ascension Island (British)', 'AC', 1, '', '', 0),
(253, '', 'Kosovo, Republic of', 'XK', 1, '', '', 0),
(254, '', 'Isle of Man', 'IM', 1, '', '', 0),
(255, '', 'Tristan da Cunha', 'TA', 1, '', '', 0),
(256, '', 'Guernsey', 'GG', 1, '', '', 0),
(257, '', 'Jersey', 'JE', 1, '', '', 0);


-- --------------------------------------------------------

--
-- Table structure for table `sma_credit_voucher`
--

DROP TABLE IF EXISTS `sma_credit_voucher`;
CREATE TABLE `sma_credit_voucher` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `card_no` varchar(20) NOT NULL,
  `value` decimal(25,4) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `balance` decimal(25,4) NOT NULL,
  `expiry` date DEFAULT NULL,
  `created_by` varchar(55) NOT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_currencies`
--

DROP TABLE IF EXISTS `sma_currencies`;
CREATE TABLE `sma_currencies` (
  `id` int(11) NOT NULL,
  `code` varchar(5) NOT NULL,
  `name` varchar(55) NOT NULL,
  `rate` decimal(12,4) NOT NULL,
  `auto_update` tinyint(1) NOT NULL DEFAULT '0',
  `curr_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------


--
-- Table structure for table `sma_customer_groups`
--

DROP TABLE IF EXISTS `sma_customer_groups`;
CREATE TABLE `sma_customer_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `percent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sma_customer_groups` (`id`, `name`, `percent`) VALUES
(1, 'General', 0),
(2, 'Reseller', -5),
(3, 'Distributor', -15),
(4, 'New Customer (+10)', 10);


--
-- Triggers `sma_customer_groups`
--
DROP TRIGGER IF EXISTS `delete_sma_customer_groups`;
DELIMITER $$
CREATE TRIGGER `delete_sma_customer_groups` BEFORE DELETE ON `sma_customer_groups` FOR EACH ROW BEGIN IF OLD.id = 1 OR OLD.id = 2 OR OLD.id = 3 OR OLD.id = 4 THEN

SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot delete locked record'; 

END IF; 

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sma_date_format`
--

DROP TABLE IF EXISTS `sma_date_format`;
CREATE TABLE `sma_date_format` (
  `id` int(11) NOT NULL,
  `js` varchar(20) NOT NULL,
  `php` varchar(20) NOT NULL,
  `sql` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sma_date_format` (`id`, `js`, `php`, `sql`) VALUES
(1, 'mm-dd-yyyy', 'm-d-Y', '%m-%d-%Y'),
(2, 'mm/dd/yyyy', 'm/d/Y', '%m/%d/%Y'),
(3, 'mm.dd.yyyy', 'm.d.Y', '%m.%d.%Y'),
(4, 'dd-mm-yyyy', 'd-m-Y', '%d-%m-%Y'),
(5, 'dd/mm/yyyy', 'd/m/Y', '%d/%m/%Y'),
(6, 'dd.mm.yyyy', 'd.m.Y', '%d.%m.%Y');
--
-- Triggers `sma_date_format`
--
DROP TRIGGER IF EXISTS `delete_sma_date_format`;
DELIMITER $$
CREATE TRIGGER `delete_sma_date_format` BEFORE DELETE ON `sma_date_format` FOR EACH ROW BEGIN IF OLD.id = 1 OR OLD.id = 2 OR OLD.id = 3 OR OLD.id = 4 OR OLD.id = 5 OR OLD.id = 6 THEN

SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot delete locked record'; 

END IF; 

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sma_deliveries`
--

DROP TABLE IF EXISTS `sma_deliveries`;
CREATE TABLE `sma_deliveries` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sale_id` int(11) NOT NULL,
  `do_reference_no` varchar(50) NOT NULL,
  `sale_reference_no` varchar(50) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_drft_po`
--

DROP TABLE IF EXISTS `sma_drft_po`;
CREATE TABLE `sma_drft_po` (
  `id` bigint(20) NOT NULL,
  `org_id` varchar(20) NOT NULL,
  `po_id` varchar(20) DEFAULT NULL,
  `po_dt` date DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `bill_adds_id` varchar(20) DEFAULT NULL,
  `tlrnc_days` int(1) DEFAULT NULL,
  `curr_id_sp` int(200) DEFAULT NULL,
  `curr_name` varchar(255) DEFAULT NULL,
  `curr_conv_fctr` decimal(26,6) DEFAULT NULL,
  `tlrnc_qty_type` varchar(1) DEFAULT NULL,
  `tlrnc_qty_val` decimal(26,6) DEFAULT NULL,
  `usr_id_create_dt` date DEFAULT NULL,
  `auth_po_no` varchar(20) DEFAULT NULL,
  `fy_id` decimal(5,0) DEFAULT NULL,
  `flg` varchar(2) DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0- pending, 1-completed, 2-waiting to close',
  `po_status` int(5) DEFAULT NULL,
  `po_mode` int(5) DEFAULT NULL,
  `doc_id` varchar(40) DEFAULT NULL,
  `modify_date` date DEFAULT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_drft_po_dlv_schdl`
--

DROP TABLE IF EXISTS `sma_drft_po_dlv_schdl`;
CREATE TABLE `sma_drft_po_dlv_schdl` (
  `id` bigint(20) NOT NULL,
  `org_id` varchar(20) DEFAULT NULL,
  `po_id` bigint(20) DEFAULT NULL,
  `itm_id` varchar(50) DEFAULT NULL,
  `itm_name` varchar(255) NOT NULL,
  `tot_qty` decimal(26,6) DEFAULT NULL,
  `dlv_mode` bigint(20) DEFAULT NULL,
  `dlv_qty` decimal(26,6) DEFAULT NULL,
  `tlrnc_qty_type` int(1) DEFAULT NULL,
  `tlrnc_qty_val` decimal(26,6) DEFAULT NULL,
  `dlv_dt` date DEFAULT NULL,
  `wh_id` varchar(20) DEFAULT NULL,
  `dlv_adds_id` varchar(20) DEFAULT NULL,
  `usr_id_create_dt` date DEFAULT NULL,
  `dlv_schdl_no` int(5) DEFAULT NULL,
  `itm_uom_desc` varchar(200) DEFAULT NULL,
  `bal_qty` decimal(26,6) DEFAULT NULL,
  `tmp_rcpt_qty` decimal(26,6) DEFAULT NULL,
  `tlrnc_days_val` int(5) DEFAULT NULL,
  `segment_id` bigint(20) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `prj_id` varchar(40) DEFAULT NULL,
  `ro_no` varchar(100) DEFAULT NULL,
  `ro_dt` date DEFAULT NULL,
  `item_id` varchar(50) DEFAULT NULL,
  `flg` varchar(2) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0-pending,1-completed, 2-in-progress,3-short closed',
  `use_rented_wh` tinyint(2) NOT NULL DEFAULT '0',
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_ds_att`
--

DROP TABLE IF EXISTS `sma_ds_att`;
CREATE TABLE `sma_ds_att` (
  `id` int(11) NOT NULL,
  `att_id` int(11) NOT NULL,
  `att_name` varchar(100) NOT NULL,
  `att_type_id` int(11) NOT NULL,
  `att_actv` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_ds_att_reln`
--

DROP TABLE IF EXISTS `sma_ds_att_reln`;
CREATE TABLE `sma_ds_att_reln` (
  `id` int(11) NOT NULL,
  `att_id` int(5) NOT NULL,
  `att_type_id` int(5) NOT NULL,
  `att_id_reln` int(5) NOT NULL,
  `att_type_id_reln` int(5) NOT NULL,
  `actv` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_ds_att_type`
--

DROP TABLE IF EXISTS `sma_ds_att_type`;
CREATE TABLE `sma_ds_att_type` (
  `id` int(11) NOT NULL,
  `att_type_id` int(5) NOT NULL,
  `att_type_name` varchar(50) NOT NULL,
  `att_type_actv` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_emrs`
--

DROP TABLE IF EXISTS `sma_emrs`;
CREATE TABLE `sma_emrs` (
  `id` bigint(11) NOT NULL,
  `org_id` varchar(10) NOT NULL,
  `segment_id` varchar(10) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `wh_id` varchar(20) NOT NULL,
  `fy_id` int(5) DEFAULT NULL,
  `doc_id` varchar(40) NOT NULL,
  `emrs_type` int(5) NOT NULL COMMENT '1081 - IN, 1080 - OUT',
  `supplier_id` int(5) DEFAULT NULL,
  `emrs_no` varchar(40) DEFAULT NULL,
  `emrs_dt` date DEFAULT NULL,
  `org_id_req_to` varchar(2) NOT NULL,
  `wh_id_required_to` varchar(20) NOT NULL,
  `reqd_dt` date NOT NULL,
  `emrs_status` int(5) NOT NULL,
  `emrs_status_dt` date NOT NULL,
  `remarks` varchar(300) NOT NULL,
  `emrs_mode` int(5) DEFAULT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_emrs_doc_src`
--

DROP TABLE IF EXISTS `sma_emrs_doc_src`;
CREATE TABLE `sma_emrs_doc_src` (
  `id` bigint(11) NOT NULL,
  `org_id` varchar(4) NOT NULL,
  `segment_id` varchar(10) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `wh_id` varchar(20) NOT NULL,
  `doc_id` varchar(40) NOT NULL,
  `doc_no` varchar(40) NOT NULL,
  `doc_date` date NOT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_emrs_item`
--

DROP TABLE IF EXISTS `sma_emrs_item`;
CREATE TABLE `sma_emrs_item` (
  `id` bigint(20) NOT NULL,
  `org_id` varchar(4) NOT NULL,
  `segment_id` varchar(10) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `wh_id` varchar(20) DEFAULT NULL,
  `doc_id` varchar(40) NOT NULL,
  `doc_no_src_id` varchar(40) NOT NULL,
  `item_id` varchar(50) NOT NULL,
  `req_qty` decimal(26,6) NOT NULL DEFAULT '0.000000',
  `bal_qty` decimal(26,6) NOT NULL,
  `itm_uom_bs` varchar(20) NOT NULL,
  `uom_conv_fctr` decimal(26,6) NOT NULL,
  `req_qty_bs` decimal(26,6) NOT NULL,
  `itm_price_bs` decimal(26,6) NOT NULL,
  `itm_amt_bs` decimal(26,6) NOT NULL,
  `line_seq_no` int(10) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0-pending,1-completed, 2-in-progress,3-short closed',
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_expenses`
--

DROP TABLE IF EXISTS `sma_expenses`;
CREATE TABLE `sma_expenses` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference` varchar(50) NOT NULL,
  `amount` decimal(25,4) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `created_by` varchar(55) NOT NULL,
  `attachment` varchar(55) DEFAULT NULL,
  `register_id` int(5) NOT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_gatepass_types`
--

DROP TABLE IF EXISTS `sma_gatepass_types`;
CREATE TABLE `sma_gatepass_types` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `type_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `status` enum('1','0') NOT NULL COMMENT '1=> active, 0=> Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_gift_cards`
--

DROP TABLE IF EXISTS `sma_gift_cards`;
CREATE TABLE `sma_gift_cards` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `card_no` varchar(20) NOT NULL,
  `value` decimal(25,4) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `balance` decimal(25,4) NOT NULL,
  `expiry` date DEFAULT NULL,
  `created_by` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_gp`
--

DROP TABLE IF EXISTS `sma_gp`;
CREATE TABLE `sma_gp` (
  `id` bigint(11) NOT NULL,
  `ho_id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL,
  `segment_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) NOT NULL,
  `fy_id` int(5) DEFAULT NULL,
  `gp_type` int(5) NOT NULL,
  `supplier_id` int(5) DEFAULT NULL,
  `doc_no` varchar(20) DEFAULT NULL,
  `doc_dt` date DEFAULT NULL,
  `gp_no` varchar(20) NOT NULL,
  `gp_date` date NOT NULL,
  `tp_id` int(5) NOT NULL,
  `tpt_lr_no` varchar(30) NOT NULL,
  `tpt_lr_dt` date DEFAULT NULL,
  `vehicle_no` varchar(30) NOT NULL,
  `status` int(5) NOT NULL,
  `addl_amt` decimal(26,6) NOT NULL,
  `cust_name` varchar(20) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `curr_id` int(10) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `mod_date` datetime DEFAULT NULL,
  `json_lotdata` varchar(300) DEFAULT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_gp_item`
--

DROP TABLE IF EXISTS `sma_gp_item`;
CREATE TABLE `sma_gp_item` (
  `id` bigint(20) NOT NULL,
  `org_id` varchar(4) NOT NULL,
  `segment_id` bigint(20) DEFAULT NULL,
  `warehouse_id` int(11) NOT NULL,
  `gp_src_id` varchar(40) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `item_uom_id` varchar(100) DEFAULT NULL,
  `pending_qty` decimal(26,6) NOT NULL DEFAULT '0.000000',
  `dlv_note_qty` decimal(26,6) NOT NULL,
  `act_rcpt_qty` decimal(26,6) NOT NULL,
  `item_price` decimal(26,6) DEFAULT NULL,
  `item_amt` decimal(26,6) NOT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_gp_item_lot`
--

DROP TABLE IF EXISTS `sma_gp_item_lot`;
CREATE TABLE `sma_gp_item_lot` (
  `id` bigint(20) NOT NULL,
  `org_id` varchar(4) NOT NULL,
  `segment_id` varchar(255) DEFAULT NULL,
  `warehouse_id` int(11) NOT NULL,
  `mrn_gp_item_id` bigint(40) NOT NULL,
  `lot_no` varchar(255) DEFAULT NULL,
  `item_id` bigint(50) NOT NULL,
  `item_uom_id` varchar(100) DEFAULT NULL,
  `lot_qty` decimal(26,6) NOT NULL,
  `mfg_dt` date DEFAULT NULL,
  `expiry_dt` date DEFAULT NULL,
  `batch_no` varchar(255) DEFAULT NULL,
  `itm_uom_bs` varchar(20) DEFAULT NULL,
  `pending_qty` decimal(26,6) DEFAULT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_gp_lot_bin`
--

DROP TABLE IF EXISTS `sma_gp_lot_bin`;
CREATE TABLE `sma_gp_lot_bin` (
  `id` bigint(20) NOT NULL,
  `org_id` int(4) NOT NULL,
  `segment_id` bigint(20) DEFAULT NULL,
  `warehouse_id` int(11) NOT NULL,
  `lot_id` bigint(40) NOT NULL,
  `item_id` bigint(50) NOT NULL,
  `item_uom_id` varchar(100) DEFAULT NULL,
  `bin_id` bigint(20) DEFAULT NULL,
  `bin_qty` decimal(26,6) NOT NULL,
  `itm_uom_bs` varchar(20) NOT NULL,
  `pending_qty` decimal(26,6) NOT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_gp_src`
--

DROP TABLE IF EXISTS `sma_gp_src`;
CREATE TABLE `sma_gp_src` (
  `id` bigint(11) NOT NULL,
  `org_id` varchar(4) NOT NULL,
  `segment_id` varchar(2) DEFAULT NULL,
  `warehouse_id` int(11) NOT NULL,
  `gp_id` bigint(11) NOT NULL,
  `doc_no` varchar(20) NOT NULL,
  `doc_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_gp_srl`
--

DROP TABLE IF EXISTS `sma_gp_srl`;
CREATE TABLE `sma_gp_srl` (
  `id` bigint(20) NOT NULL,
  `org_id` int(11) NOT NULL,
  `sloc_id` int(11) NOT NULL,
  `segment_id` int(11) NOT NULL,
  `wh_id` int(11) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `lot_id` bigint(20) NOT NULL,
  `bin_id` bigint(20) NOT NULL,
  `itm_uom_id` int(11) DEFAULT NULL,
  `itm_qty` decimal(26,6) NOT NULL,
  `pending_qty` decimal(26,6) NOT NULL,
  `sr_no` varchar(255) NOT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_groups`
--

DROP TABLE IF EXISTS `sma_groups`;
CREATE TABLE `sma_groups` (
  `id` bigint(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sma_groups` (`id`, `name`, `description`) VALUES
(1, 'owner', 'Owner'),
(2, 'admin', 'Administrator'),
(3, 'customer', 'Customer'),
(4, 'supplier', 'Supplier'),
(5, 'sales', 'Sales Staff'),
(6, 'manager', 'Manager'),
(7, 'transporter', 'Transporter');


--
-- Triggers `sma_groups`
--
DROP TRIGGER IF EXISTS `delete_sma_groups`;
DELIMITER $$
CREATE TRIGGER `delete_sma_groups` BEFORE DELETE ON `sma_groups` FOR EACH ROW BEGIN IF OLD.id = 1 OR OLD.id = 2 OR OLD.id = 3 OR OLD.id = 4 OR OLD.id = 5 OR OLD.id = 6 THEN

SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot delete locked record'; 

END IF; 

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sma_item_profile`
--

DROP TABLE IF EXISTS `sma_item_profile`;
CREATE TABLE `sma_item_profile` (
  `id` bigint(50) NOT NULL,
  `ho_id` bigint(20) NOT NULL,
  `org_id` bigint(20) NOT NULL,
  `segment_id` bigint(20) NOT NULL,
  `code` varchar(50) NOT NULL,
  `desc` varchar(100) DEFAULT NULL,
  `long_desc` varchar(200) DEFAULT NULL,
  `tech_name` varchar(100) NOT NULL,
  `cat_id` bigint(20) NOT NULL,
  `uom_id` int(10) NOT NULL,
  `basic_price` decimal(25,4) DEFAULT NULL,
  `purchase_price` decimal(25,4) NOT NULL,
  `sales_price` decimal(25,4) NOT NULL,
  `taxable` tinyint(2) NOT NULL,
  `attribute` int(11) NOT NULL,
  `is_serialized` tinyint(2) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `status` tinyint(2) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_item_stk_lot`
--

DROP TABLE IF EXISTS `sma_item_stk_lot`;
CREATE TABLE `sma_item_stk_lot` (
  `id` bigint(20) NOT NULL,
  `org_id` varchar(20) DEFAULT NULL,
  `segment_id` int(11) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `wh_id` varchar(20) DEFAULT NULL,
  `lot_no` varchar(50) DEFAULT NULL,
  `item_stk_id` int(11) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `batch_no` varchar(255) DEFAULT NULL,
  `itm_uom_id` varchar(50) DEFAULT NULL,
  `total_stk` decimal(26,6) NOT NULL,
  `rejected_stk` decimal(26,6) NOT NULL,
  `mfg_date` datetime DEFAULT NULL,
  `exp_date` datetime DEFAULT NULL,
  `reworkable_stk` decimal(26,6) NOT NULL,
  `created_date` datetime NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  `create_flg` varchar(1) DEFAULT NULL,
  `upd_flg` varchar(1) DEFAULT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_item_stk_lot_bin`
--

DROP TABLE IF EXISTS `sma_item_stk_lot_bin`;
CREATE TABLE `sma_item_stk_lot_bin` (
  `id` bigint(20) NOT NULL,
  `org_id` varchar(11) DEFAULT NULL,
  `segment_id` int(11) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `wh_id` varchar(20) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `lot_id` varchar(20) NOT NULL,
  `bin_id` varchar(255) NOT NULL,
  `itm_uom_id` varchar(20) NULL,
  `total_stk` decimal(26,6) NOT NULL,
  `rejected_stk` decimal(26,6) NOT NULL,
  `reworkable_stk` decimal(26,6) NOT NULL,
  `created_date` datetime NOT NULL,
  `code` varchar(50) NOT NULL,
  `create_flg` varchar(1) DEFAULT NULL,
  `upd_flg` varchar(1) DEFAULT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_item_stk_profile`
--

DROP TABLE IF EXISTS `sma_item_stk_profile`;
CREATE TABLE `sma_item_stk_profile` (
  `id` bigint(20) NOT NULL,
  `org_id` varchar(20) NOT NULL,
  `segment_id` int(11) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `wh_id` varchar(20) DEFAULT NULL,
  `fy_id` int(11) DEFAULT NULL,
  `item_id` bigint(20) NOT NULL,
  `item_uom_id` varchar(20) DEFAULT NULL,
  `total_stk` decimal(26,6) NOT NULL,
  `available_stk` decimal(26,6) NOT NULL,
  `rejected_stk` decimal(26,6) NOT NULL,
  `rework_stk` decimal(26,6) NOT NULL,
  `ordered_stk` decimal(26,6) NOT NULL,
  `purchase_price` decimal(26,6) NOT NULL,
  `sales_price` decimal(26,6) NOT NULL,
  `created_date` datetime NOT NULL,
  `code` varchar(50) NOT NULL,
  `create_flg` varchar(1) DEFAULT NULL,
  `upd_flg` varchar(1) DEFAULT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_item_stk_srl`
--

DROP TABLE IF EXISTS `sma_item_stk_srl`;
CREATE TABLE `sma_item_stk_srl` (
  `id` bigint(20) NOT NULL,
  `org_id` int(11) NOT NULL,
  `segment_id` int(11) NOT NULL,
  `wh_id` int(11) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `lot_id` bigint(20) NOT NULL,
  `bin_id` bigint(20) NOT NULL,
  `itm_uom_id` int(11) DEFAULT NULL,
  `total_stk` decimal(26,6) NOT NULL,
  `rejected_stk` decimal(26,6) NOT NULL,
  `reworkable_stk` decimal(26,6) NOT NULL,
  `sr_no` varchar(255) NOT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_item_warehouse_details`
--

DROP TABLE IF EXISTS `sma_item_warehouse_details`;
CREATE TABLE `sma_item_warehouse_details` (
  `id` bigint(20) NOT NULL,
  `ho_id` bigint(20) NOT NULL,
  `org_id` bigint(20) NOT NULL,
  `segment_id` bigint(20) NOT NULL,
  `store_id` bigint(20) NOT NULL,
  `warehouse_id` bigint(20) DEFAULT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  `max_stk` decimal(20,0) NOT NULL,
  `min_stk` decimal(20,0) NOT NULL,
  `alert_qty` decimal(20,0) NOT NULL,
  `reorder_level` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_location`
--

DROP TABLE IF EXISTS `sma_location`;
CREATE TABLE IF NOT EXISTS `sma_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `org_id` varchar(20) NOT NULL,
  `prj_doc_id` varchar(40) NOT NULL,
  `parent_proj_id` varchar(40) NOT NULL,
  `proj_name` varchar(100) NOT NULL,
  `prj_actv` varchar(1) NOT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=293 ;

-- --------------------------------------------------------

--
-- Table structure for table `sma_login_attempts`
--

DROP TABLE IF EXISTS `sma_login_attempts`;
CREATE TABLE `sma_login_attempts` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_migrations`
--

DROP TABLE IF EXISTS `sma_migrations`;
CREATE TABLE `sma_migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_mrn_rcpt_item`
--

DROP TABLE IF EXISTS `sma_mrn_rcpt_item`;
CREATE TABLE `sma_mrn_rcpt_item` (
 `id` bigint(20) NOT NULL AUTO_INCREMENT,
 `org_id` varchar(4) DEFAULT NULL,
 `segment_id` bigint(20) DEFAULT NULL,
 `wh_id` bigint(20) NOT NULL,
 `warehouse_id` varchar(20) DEFAULT NULL,
 `mrn_rcpt_src_id` varchar(40) NOT NULL,
 `po_id` varchar(40) NOT NULL,
 `po_date` datetime DEFAULT NULL,
 `dlv_schdl_no` varchar(255) DEFAULT NULL,
 `item_id` bigint(20) NOT NULL,
 `code` varchar(50) DEFAULT NULL,
 `item_uom_id` bigint(20) DEFAULT NULL,
 `pending_qty` decimal(26,6) NOT NULL DEFAULT '0.000000',
 `dlv_note_qty` decimal(26,6) NOT NULL,
 `tot_rcpt_qty` decimal(26,6) NOT NULL,
 `rwk_qty` decimal(26,6) DEFAULT NULL,
 `rej_qty` decimal(26,6) NOT NULL,
 `rcpt_qty` decimal(26,6) NOT NULL,
 `purchase_price` decimal(26,6) DEFAULT NULL,
 `sync_flg` varchar(2) DEFAULT NULL,
 `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `pos_create_flg` varchar(2) DEFAULT NULL,
 `pos_upd_flg` varchar(2) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

--
-- Table structure for table `sma_mrn_rcpt_item_lot`
--

DROP TABLE IF EXISTS `sma_mrn_rcpt_item_lot`;
CREATE TABLE `sma_mrn_rcpt_item_lot` (
 `id` bigint(11) NOT NULL AUTO_INCREMENT,
 `org_id` varchar(11) DEFAULT NULL,
 `segment_id` varchar(255) DEFAULT NULL,
 `wh_id` varchar(20) NOT NULL,
 `warehouse_id` varchar(20) DEFAULT NULL,
 `mrn_rcpt_item_id` bigint(40) NOT NULL,
 `lot_no` varchar(255) DEFAULT NULL,
 `dlv_schdl_no` int(5) DEFAULT '0',
 `item_id` bigint(50) NOT NULL,
 `code` varchar(50) DEFAULT NULL,
 `itm_uom_id` bigint(20) DEFAULT NULL,
 `lot_qty` decimal(26,6) NOT NULL,
 `rej_qty` decimal(26,6) NOT NULL DEFAULT '0.000000',
 `rewrk_qty` decimal(26,6) NOT NULL DEFAULT '0.000000',
 `mfg_dt` date NOT NULL,
 `expiry_dt` date NOT NULL,
 `batch_no` varchar(255) NOT NULL,
 `itm_uom_bs` varchar(255) DEFAULT NULL,
 `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `sync_flg` varchar(2) DEFAULT NULL,
 `pos_create_flg` varchar(2) DEFAULT NULL,
 `pos_upd_flg` varchar(2) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_mrn_rcpt_lot_bin`
--

DROP TABLE IF EXISTS `sma_mrn_rcpt_lot_bin`;
CREATE TABLE `sma_mrn_rcpt_lot_bin` (
 `id` bigint(11) NOT NULL AUTO_INCREMENT,
 `org_id` varchar(11) DEFAULT NULL,
 `segment_id` bigint(20) DEFAULT NULL,
 `wh_id` bigint(20) NOT NULL,
 `warehouse_id` varchar(20) DEFAULT NULL,
 `lot_id` bigint(40) NOT NULL,
 `item_id` bigint(50) NOT NULL,
 `code` varchar(50) DEFAULT NULL,
 `itm_uom_id` bigint(20) DEFAULT NULL,
 `bin_id` bigint(20) NOT NULL,
 `bin_qty` decimal(26,6) NOT NULL,
 `itm_uom_bs` varchar(20) NOT NULL,
 `rej_qty` decimal(26,6) NOT NULL,
 `rewrk_qty` decimal(26,6) NOT NULL,
 `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `sync_flg` varchar(2) DEFAULT NULL,
 `pos_create_flg` varchar(2) DEFAULT NULL,
 `pos_upd_flg` varchar(2) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

--
-- Table structure for table `sma_mrn_rcpt_src`
--

DROP TABLE IF EXISTS `sma_mrn_rcpt_src`;
CREATE TABLE `sma_mrn_rcpt_src` (
 `id` bigint(11) NOT NULL AUTO_INCREMENT,
 `org_id` varchar(10) DEFAULT NULL,
 `segment_id` varchar(2) DEFAULT NULL,
 `wh_id` varchar(20) NOT NULL,
 `warehouse_id` varchar(20) DEFAULT NULL,
 `mrn_rcpt_id` bigint(11) NOT NULL,
 `po_id` varchar(40) NOT NULL,
 `po_date` datetime DEFAULT NULL,
 `ro_no` varchar(255) NOT NULL,
 `ro_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `gatepass_no` varchar(255) DEFAULT NULL,
 `gatepass_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
 `use_rented_wh` tinyint(2) NOT NULL DEFAULT '0',
 `sync_flg` varchar(2) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

--
-- Table structure for table `sma_mrn_receipt`
--

DROP TABLE IF EXISTS `sma_mrn_receipt`;
CREATE TABLE `sma_mrn_receipt` (
 `id` bigint(11) NOT NULL AUTO_INCREMENT,
 `ho_id` int(11) NOT NULL,
 `org_id` varchar(10) DEFAULT NULL,
 `segment_id` int(11) DEFAULT NULL,
 `wh_id` bigint(20) NOT NULL,
 `warehouse_id` varchar(20) DEFAULT NULL,
 `fy_id` int(5) DEFAULT NULL,
 `rcpt_no` varchar(20) DEFAULT NULL,
 `rcpt_dt` date NOT NULL,
 `rcpt_src_type` int(5) NOT NULL,
 `ge_doc_id` varchar(40) NOT NULL,
 `supplier_id` int(11) DEFAULT NULL,
 `org_id_src` varchar(2) DEFAULT NULL,
 `wh_id_src` varchar(20) DEFAULT NULL,
 `dn_no` varchar(20) DEFAULT NULL,
 `dn_dt` date DEFAULT NULL,
 `sto_no` varchar(250) DEFAULT NULL,
 `sto_date` int(11) DEFAULT NULL,
 `tp_id` int(5) NOT NULL,
 `tpt_lr_no` varchar(30) NOT NULL,
 `tpt_lr_dt` date NOT NULL,
 `vehicle_no` varchar(30) NOT NULL,
 `status` int(5) NOT NULL,
 `rmda_stat` varchar(1) NOT NULL,
 `addl_amt` decimal(26,6) NOT NULL,
 `curr_id` int(10) DEFAULT NULL,
 `sync_flg` varchar(2) DEFAULT NULL,
 `remarks` text,
 `user_id` int(10) DEFAULT NULL,
 `created_date` date DEFAULT NULL,
 `mod_date` date NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

--
-- Table structure for table `sma_notifications`
--

DROP TABLE IF EXISTS `sma_notifications`;
CREATE TABLE `sma_notifications` (
  `id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `from_date` datetime DEFAULT NULL,
  `till_date` datetime DEFAULT NULL,
  `scope` tinyint(1) NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_order_ref`
--

DROP TABLE IF EXISTS `sma_order_ref`;
CREATE TABLE `sma_order_ref` (
  `ref_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `so` int(11) NOT NULL DEFAULT '1',
  `qu` int(11) NOT NULL DEFAULT '1',
  `po` int(11) NOT NULL DEFAULT '1',
  `to` int(11) NOT NULL DEFAULT '1',
  `pos` int(11) NOT NULL DEFAULT '1',
  `do` int(11) NOT NULL DEFAULT '1',
  `pay` int(11) NOT NULL DEFAULT '1',
  `re` int(11) NOT NULL DEFAULT '1',
  `ex` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `sma_order_ref` (`ref_id`, `date`, `so`, `qu`, `po`, `to`, `pos`, `do`, `pay`, `re`, `ex`) VALUES
(1, '2015-03-01', 1, 1, 1, 1, 1, 1, 1, 4, 1);

--
-- Triggers `sma_order_ref`
--
DROP TRIGGER IF EXISTS `delete_sma_order_ref`;
DELIMITER $$
CREATE TRIGGER `delete_sma_order_ref` BEFORE DELETE ON `sma_order_ref` FOR EACH ROW BEGIN IF OLD.ref_id = 1 THEN

SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot delete locked record'; 

END IF; 

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sma_payments`
--

DROP TABLE IF EXISTS `sma_payments`;
CREATE TABLE `sma_payments` (
  `id` int(11) NOT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sale_id` int(11) DEFAULT NULL,
  `return_id` int(11) DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `reference_no` varchar(50) NOT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `paid_by` varchar(20) NOT NULL,
  `cheque_no` varchar(20) DEFAULT NULL,
  `cc_no` varchar(20) DEFAULT NULL,
  `cv_no` varchar(20) DEFAULT NULL,
  `cc_holder` varchar(25) DEFAULT NULL,
  `cc_month` varchar(2) DEFAULT NULL,
  `cc_year` varchar(4) DEFAULT NULL,
  `cc_type` varchar(20) DEFAULT NULL,
  `amount` decimal(25,4) NOT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `attachment` varchar(55) DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `pos_paid` decimal(25,4) DEFAULT '0.0000',
  `pos_balance` decimal(25,4) DEFAULT '0.0000',
  `sync_flg` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_paypal`
--

DROP TABLE IF EXISTS `sma_paypal`;
CREATE TABLE `sma_paypal` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `account_email` varchar(255) NOT NULL,
  `paypal_currency` varchar(3) NOT NULL DEFAULT 'USD',
  `fixed_charges` decimal(25,4) NOT NULL DEFAULT '2.0000',
  `extra_charges_my` decimal(25,4) NOT NULL DEFAULT '3.9000',
  `extra_charges_other` decimal(25,4) NOT NULL DEFAULT '4.4000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `sma_paypal` (`id`, `active`, `account_email`, `paypal_currency`, `fixed_charges`, `extra_charges_my`, `extra_charges_other`) VALUES
(1, 1, 'mypaypal@paypal.com', 'USD', '0.0000', '0.0000', '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `sma_permissions`
--

DROP TABLE IF EXISTS `sma_permissions`;
CREATE TABLE `sma_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) UNSIGNED NOT NULL,
  `products-index` tinyint(1) DEFAULT '0',
  `products-add` tinyint(1) DEFAULT '0',
  `products-edit` tinyint(1) DEFAULT '0',
  `products-delete` tinyint(1) DEFAULT '0',
  `products-cost` tinyint(1) DEFAULT '0',
  `products-price` tinyint(1) DEFAULT '0',
  `quotes-index` tinyint(1) DEFAULT '0',
  `quotes-add` tinyint(1) DEFAULT '0',
  `quotes-edit` tinyint(1) DEFAULT '0',
  `quotes-pdf` tinyint(1) DEFAULT '0',
  `quotes-email` tinyint(1) DEFAULT '0',
  `quotes-delete` tinyint(1) DEFAULT '0',
  `sales-index` tinyint(1) DEFAULT '0',
  `sales-add` tinyint(1) DEFAULT '0',
  `sales-edit` tinyint(1) DEFAULT '0',
  `sales-pdf` tinyint(1) DEFAULT '0',
  `sales-email` tinyint(1) DEFAULT '0',
  `sales-delete` tinyint(1) DEFAULT '0',
  `purchases-index` tinyint(1) DEFAULT '0',
  `purchases-add` tinyint(1) DEFAULT '0',
  `purchases-edit` tinyint(1) DEFAULT '0',
  `purchases-pdf` tinyint(1) DEFAULT '0',
  `purchases-email` tinyint(1) DEFAULT '0',
  `purchases-delete` tinyint(1) DEFAULT '0',
  `transfers-index` tinyint(1) DEFAULT '0',
  `transfers-add` tinyint(1) DEFAULT '0',
  `transfers-edit` tinyint(1) DEFAULT '0',
  `transfers-pdf` tinyint(1) DEFAULT '0',
  `transfers-email` tinyint(1) DEFAULT '0',
  `transfers-delete` tinyint(1) DEFAULT '0',
  `customers-index` tinyint(1) DEFAULT '0',
  `customers-add` tinyint(1) DEFAULT '0',
  `customers-edit` tinyint(1) DEFAULT '0',
  `customers-delete` tinyint(1) DEFAULT '0',
  `suppliers-index` tinyint(1) DEFAULT '0',
  `suppliers-add` tinyint(1) DEFAULT '0',
  `suppliers-edit` tinyint(1) DEFAULT '0',
  `suppliers-delete` tinyint(1) DEFAULT '0',
  `sales-deliveries` tinyint(1) DEFAULT '0',
  `sales-add_delivery` tinyint(1) DEFAULT '0',
  `sales-edit_delivery` tinyint(1) DEFAULT '0',
  `sales-delete_delivery` tinyint(1) DEFAULT '0',
  `sales-email_delivery` tinyint(1) DEFAULT '0',
  `sales-pdf_delivery` tinyint(1) DEFAULT '0',
  `sales-gift_cards` tinyint(1) DEFAULT '0',
  `sales-add_gift_card` tinyint(1) DEFAULT '0',
  `sales-edit_gift_card` tinyint(1) DEFAULT '0',
  `sales-delete_gift_card` tinyint(1) DEFAULT '0',
  `pos-index` tinyint(1) DEFAULT '0',
  `sales-return_sales` tinyint(1) DEFAULT '0',
  `reports-index` tinyint(1) DEFAULT '0',
  `reports-warehouse_stock` tinyint(1) DEFAULT '0',
  `reports-quantity_alerts` tinyint(1) DEFAULT '0',
  `reports-expiry_alerts` tinyint(1) DEFAULT '0',
  `reports-products` tinyint(1) DEFAULT '0',
  `reports-daily_sales` tinyint(1) DEFAULT '0',
  `reports-monthly_sales` tinyint(1) DEFAULT '0',
  `reports-sales` tinyint(1) DEFAULT '0',
  `reports-payments` tinyint(1) DEFAULT '0',
  `reports-purchases` tinyint(1) DEFAULT '0',
  `reports-profit_loss` tinyint(1) DEFAULT '0',
  `reports-customers` tinyint(1) DEFAULT '0',
  `reports-suppliers` tinyint(1) DEFAULT '0',
  `reports-staff` tinyint(1) DEFAULT '0',
  `reports-register` tinyint(1) DEFAULT '0',
  `sales-payments` tinyint(1) DEFAULT '0',
  `purchases-payments` tinyint(1) DEFAULT '0',
  `purchases-expenses` tinyint(1) DEFAULT '0',
  `till-addTill` tinyint(1) NOT NULL DEFAULT '0',
  `till-manageTill` tinyint(1) NOT NULL DEFAULT '0',
  `products-kit` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `sma_permissions`
--
DROP TRIGGER IF EXISTS `delete_permissions`;
DELIMITER $$
CREATE TRIGGER `delete_permissions` BEFORE DELETE ON `sma_permissions` FOR EACH ROW BEGIN IF OLD.id = 1 OR OLD.id = 2 THEN

SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot delete locked record'; 

END IF; 

END
$$
DELIMITER ;

INSERT INTO `sma_permissions` (`id`, `group_id`, `products-index`, `products-add`, `products-edit`, `products-delete`, `products-cost`, `products-price`, `quotes-index`, `quotes-add`, `quotes-edit`, `quotes-pdf`, `quotes-email`, `quotes-delete`, `sales-index`, `sales-add`, `sales-edit`, `sales-pdf`, `sales-email`, `sales-delete`, `purchases-index`, `purchases-add`, `purchases-edit`, `purchases-pdf`, `purchases-email`, `purchases-delete`, `transfers-index`, `transfers-add`, `transfers-edit`, `transfers-pdf`, `transfers-email`, `transfers-delete`, `customers-index`, `customers-add`, `customers-edit`, `customers-delete`, `suppliers-index`, `suppliers-add`, `suppliers-edit`, `suppliers-delete`, `sales-deliveries`, `sales-add_delivery`, `sales-edit_delivery`, `sales-delete_delivery`, `sales-email_delivery`, `sales-pdf_delivery`, `sales-gift_cards`, `sales-add_gift_card`, `sales-edit_gift_card`, `sales-delete_gift_card`, `pos-index`, `sales-return_sales`, `reports-index`, `reports-warehouse_stock`, `reports-quantity_alerts`, `reports-expiry_alerts`, `reports-products`, `reports-daily_sales`, `reports-monthly_sales`, `reports-sales`, `reports-payments`, `reports-purchases`, `reports-profit_loss`, `reports-customers`, `reports-suppliers`, `reports-staff`, `reports-register`, `sales-payments`, `purchases-payments`, `purchases-expenses`, `till-addTill`, `till-manageTill`, `products-kit`) VALUES
(1, 5, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, 1, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, 1, 1, 1, NULL, NULL, 1, 1, 0, 0, 1, NULL, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, NULL, 1, 0, 0, NULL),
(2, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_pos_cash_drawer`
--

DROP TABLE IF EXISTS `sma_pos_cash_drawer`;
CREATE TABLE `sma_pos_cash_drawer` (
  `id` int(11) NOT NULL,
  `pos_register_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `thousand` int(5) NOT NULL,
  `five_hundred` int(5) NOT NULL,
  `hundred` int(5) NOT NULL,
  `fifty` int(5) NOT NULL,
  `twenty` int(5) NOT NULL,
  `ten` int(5) NOT NULL,
  `five` int(5) NOT NULL,
  `two` int(5) NOT NULL,
  `one` int(5) NOT NULL,
  `status` varchar(30) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cash_in_hand` decimal(25,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_pos_register`
--

DROP TABLE IF EXISTS `sma_pos_register`;
CREATE TABLE `sma_pos_register` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `cash_in_hand` decimal(25,4) NOT NULL,
  `status` varchar(10) NOT NULL,
  `total_cash` decimal(25,4) DEFAULT NULL,
  `total_cheques` int(11) DEFAULT NULL,
  `total_cc_slips` int(11) DEFAULT NULL,
  `total_cash_submitted` decimal(25,4) DEFAULT NULL,
  `total_cheques_submitted` int(11) DEFAULT NULL,
  `total_cc_slips_submitted` int(11) DEFAULT NULL,
  `note` text,
  `closed_at` timestamp NULL DEFAULT NULL,
  `transfer_opened_bills` varchar(50) DEFAULT NULL,
  `closed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_pos_settings`
--

DROP TABLE IF EXISTS `sma_pos_settings`;
CREATE TABLE `sma_pos_settings` (
  `pos_id` int(1) NOT NULL,
  `cat_limit` int(11) NOT NULL,
  `pro_limit` int(11) NOT NULL,
  `default_category` int(11) NOT NULL,
  `default_customer` int(11) NOT NULL,
  `default_biller` int(11) DEFAULT NULL,
  `display_time` varchar(3) NOT NULL DEFAULT 'yes',
  `cf_title1` varchar(255) DEFAULT NULL,
  `cf_title2` varchar(255) DEFAULT NULL,
  `cf_value1` varchar(255) DEFAULT NULL,
  `cf_value2` varchar(255) DEFAULT NULL,
  `receipt_printer` varchar(55) DEFAULT NULL,
  `cash_drawer_codes` varchar(55) DEFAULT NULL,
  `focus_add_item` varchar(55) DEFAULT NULL,
  `add_manual_product` varchar(55) DEFAULT NULL,
  `customer_selection` varchar(55) DEFAULT NULL,
  `add_customer` varchar(55) DEFAULT NULL,
  `toggle_category_slider` varchar(55) DEFAULT NULL,
  `toggle_subcategory_slider` varchar(55) DEFAULT NULL,
  `cancel_sale` varchar(55) DEFAULT NULL,
  `suspend_sale` varchar(55) DEFAULT NULL,
  `print_items_list` varchar(55) DEFAULT NULL,
  `finalize_sale` varchar(55) DEFAULT NULL,
  `today_sale` varchar(55) DEFAULT NULL,
  `open_hold_bills` varchar(55) DEFAULT NULL,
  `close_register` varchar(55) DEFAULT NULL,
  `keyboard` tinyint(1) NOT NULL,
  `pos_printers` varchar(255) DEFAULT NULL,
  `java_applet` tinyint(1) NOT NULL,
  `enable_discount` tinyint(1) NOT NULL,
  `product_button_color` varchar(20) NOT NULL DEFAULT 'default',
  `tooltips` tinyint(1) DEFAULT '1',
  `paypal_pro` tinyint(1) DEFAULT '0',
  `stripe` tinyint(1) DEFAULT '0',
  `rounding` tinyint(1) DEFAULT '0',
  `char_per_line` tinyint(4) DEFAULT '42',
  `pin_code` varchar(20) DEFAULT NULL,
  `purchase_code` varchar(100) DEFAULT 'purchase_code',
  `envato_username` varchar(50) DEFAULT 'envato_username',
  `version` varchar(10) DEFAULT '3.0.1.21'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `sma_pos_settings` (`pos_id`, `cat_limit`, `pro_limit`, `default_category`, `default_customer`, `default_biller`, `display_time`, `cf_title1`, `cf_title2`, `cf_value1`, `cf_value2`, `receipt_printer`, `cash_drawer_codes`, `focus_add_item`, `add_manual_product`, `customer_selection`, `add_customer`, `toggle_category_slider`, `toggle_subcategory_slider`, `cancel_sale`, `suspend_sale`, `print_items_list`, `finalize_sale`, `today_sale`, `open_hold_bills`, `close_register`, `keyboard`, `pos_printers`, `java_applet`, `enable_discount`, `product_button_color`, `tooltips`, `paypal_pro`, `stripe`, `rounding`, `char_per_line`, `pin_code`, `purchase_code`, `envato_username`, `version`) VALUES
(1, 22, 20, 10, 1, 2601, '1', 'GST Reg', 'VAT Reg', '123456789', '987654321', 'BIXOLON SRP-350II', 'x1C', 'Ctrl+F3', 'Ctrl+Shift+M', 'Ctrl+Shift+C', 'Ctrl+Shift+A', 'Ctrl+F11', 'Ctrl+F12', 'F4', 'F7', 'F9', 'F8', 'Ctrl+F1', 'Ctrl+F2', 'Ctrl+F10', 1, 'BIXOLON SRP-350II, BIXOLON SRP-350II', 0, 0, 'default', 1, 0, 0, 0, 42, NULL, 'purchase_code', 'envato_username', '3.0.1.21');

--
-- Triggers `sma_pos_settings`
--
DROP TRIGGER IF EXISTS `delete_pos_setting`;
DELIMITER $$
CREATE TRIGGER `delete_pos_setting` BEFORE DELETE ON `sma_pos_settings` FOR EACH ROW BEGIN if OLD.pos_id THEN 



SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot delete locked record'; 

END IF; 

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sma_products`
--

DROP TABLE IF EXISTS `sma_products`;
CREATE TABLE `sma_products` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `name` char(255) NOT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `cost` decimal(25,4) DEFAULT NULL,
  `price` decimal(25,4) DEFAULT NULL,
  `alert_quantity` decimal(15,4) DEFAULT '1.0000',
  `image` varchar(255) DEFAULT 'no_image.png',
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `cf1` varchar(255) DEFAULT NULL,
  `cf2` varchar(255) DEFAULT NULL,
  `cf3` varchar(255) DEFAULT NULL,
  `cf4` varchar(255) DEFAULT NULL,
  `cf5` varchar(255) DEFAULT NULL,
  `cf6` varchar(255) DEFAULT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `tax_rate` int(11) DEFAULT NULL,
  `track_quantity` tinyint(1) DEFAULT '1',
  `details` varchar(1000) DEFAULT NULL,
  `warehouse` int(11) DEFAULT NULL,
  `barcode_symbology` varchar(55) NOT NULL DEFAULT 'code128',
  `file` varchar(100) DEFAULT NULL,
  `product_details` text,
  `tax_method` tinyint(1) DEFAULT '0',
  `type` varchar(55) NOT NULL DEFAULT 'standard',
  `supplier1` int(11) DEFAULT NULL,
  `supplier1price` decimal(25,4) DEFAULT NULL,
  `supplier2` int(11) DEFAULT NULL,
  `supplier2price` decimal(25,4) DEFAULT NULL,
  `supplier3` int(11) DEFAULT NULL,
  `supplier3price` decimal(25,4) DEFAULT NULL,
  `supplier4` int(11) DEFAULT NULL,
  `supplier4price` decimal(25,4) DEFAULT NULL,
  `supplier5` int(11) DEFAULT NULL,
  `supplier5price` decimal(25,4) DEFAULT NULL,
  `max_discount` int(11) DEFAULT NULL,
  `serialized` varchar(10) DEFAULT NULL,
  `serial_number` int(11) DEFAULT NULL,
  `lot_no` varchar(20) DEFAULT NULL,
  `sr_no` varchar(20) DEFAULT NULL,
  `org_id` varchar(20) DEFAULT NULL,
  `wh_id` varchar(20) DEFAULT NULL,
  `grp_id` varchar(20) DEFAULT NULL,
  `trsfr_flg` varchar(1) DEFAULT NULL,
  `tupd_flg` varchar(1) DEFAULT NULL,
  `psale` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_products_old`
--

DROP TABLE IF EXISTS `sma_products_old`;
CREATE TABLE `sma_products_old` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `name` char(255) NOT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `cost` decimal(25,4) DEFAULT NULL,
  `price` decimal(25,4) DEFAULT NULL,
  `alert_quantity` decimal(15,4) DEFAULT '1.0000',
  `image` varchar(255) DEFAULT 'no_image.png',
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `cf1` varchar(255) DEFAULT NULL,
  `cf2` varchar(255) DEFAULT NULL,
  `cf3` varchar(255) DEFAULT NULL,
  `cf4` varchar(255) DEFAULT NULL,
  `cf5` varchar(255) DEFAULT NULL,
  `cf6` varchar(255) DEFAULT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `tax_rate` int(11) DEFAULT NULL,
  `track_quantity` tinyint(1) DEFAULT '1',
  `details` varchar(1000) DEFAULT NULL,
  `warehouse` int(11) DEFAULT NULL,
  `barcode_symbology` varchar(55) NOT NULL DEFAULT 'code128',
  `file` varchar(100) DEFAULT NULL,
  `product_details` text,
  `tax_method` tinyint(1) DEFAULT '0',
  `type` varchar(55) NOT NULL DEFAULT 'standard',
  `supplier1` int(11) DEFAULT NULL,
  `supplier1price` decimal(25,4) DEFAULT NULL,
  `supplier2` int(11) DEFAULT NULL,
  `supplier2price` decimal(25,4) DEFAULT NULL,
  `supplier3` int(11) DEFAULT NULL,
  `supplier3price` decimal(25,4) DEFAULT NULL,
  `supplier4` int(11) DEFAULT NULL,
  `supplier4price` decimal(25,4) DEFAULT NULL,
  `supplier5` int(11) DEFAULT NULL,
  `supplier5price` decimal(25,4) DEFAULT NULL,
  `max_discount` int(11) DEFAULT NULL,
  `serialized` varchar(10) DEFAULT NULL,
  `serial_number` int(11) DEFAULT NULL,
  `lot_no` varchar(20) DEFAULT NULL,
  `sr_no` varchar(20) DEFAULT NULL,
  `org_id` varchar(20) DEFAULT NULL,
  `wh_id` varchar(20) DEFAULT NULL,
  `grp_id` varchar(20) DEFAULT NULL,
  `trsfr_flg` varchar(1) DEFAULT NULL,
  `tupd_flg` varchar(1) DEFAULT NULL,
  `psale` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_photos`
--

DROP TABLE IF EXISTS `sma_product_photos`;
CREATE TABLE `sma_product_photos` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `photo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_variants`
--

DROP TABLE IF EXISTS `sma_product_variants`;
CREATE TABLE `sma_product_variants` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `cost` decimal(25,4) DEFAULT NULL,
  `price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_purchases`
--

DROP TABLE IF EXISTS `sma_purchases`;
CREATE TABLE `sma_purchases` (
  `id` int(11) NOT NULL,
  `reference_no` varchar(55) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `supplier_id` int(11) NOT NULL,
  `supplier` varchar(55) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` varchar(1000) NOT NULL,
  `total` decimal(25,4) DEFAULT NULL,
  `product_discount` decimal(25,4) DEFAULT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `order_discount` decimal(25,4) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT NULL,
  `product_tax` decimal(25,4) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `paid` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `status` varchar(55) DEFAULT '',
  `payment_status` varchar(20) DEFAULT 'pending',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attachment` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_purchase_items`
--

DROP TABLE IF EXISTS `sma_purchase_items`;
CREATE TABLE `sma_purchase_items` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `transfer_id` int(11) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `itm_desc` varchar(100) DEFAULT NULL,
  `long_desc` varchar(200) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_cost` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `alert_quantity` decimal(15,4) DEFAULT '1.0000',
  `track_quantity` tinyint(1) DEFAULT '1',
  `type` varchar(55) NOT NULL DEFAULT 'standard',
  `warehouse_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `taxable` tinyint(2) DEFAULT '0',
  `attribute` int(11) NOT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate` int(11) DEFAULT NULL,
  `tax_method` int(11) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(20) DEFAULT NULL,
  `discount` varchar(20) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `quantity_balance` decimal(15,4) DEFAULT '0.0000',
  `date` date NOT NULL,
  `is_serialized` tinyint(2) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `status` varchar(50) NOT NULL,
  `unit_cost` decimal(25,4) DEFAULT NULL,
  `real_unit_cost` decimal(25,4) DEFAULT NULL,
  `lot_no` varchar(20) DEFAULT NULL,
  `sr_no` varchar(20) DEFAULT NULL,
  `org_id` varchar(20) DEFAULT NULL,
  `wh_id` varchar(20) DEFAULT NULL,
  `grp_id` varchar(20) DEFAULT NULL,
  `psale` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_quotes`
--

DROP TABLE IF EXISTS `sma_quotes`;
CREATE TABLE `sma_quotes` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `biller_id` int(11) DEFAULT NULL,
  `biller` varchar(55) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `internal_note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount` decimal(25,4) DEFAULT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT '0.0000',
  `product_tax` decimal(25,4) DEFAULT '0.0000',
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT NULL,
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attachment` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_quote_items`
--

DROP TABLE IF EXISTS `sma_quote_items`;
CREATE TABLE `sma_quote_items` (
  `id` int(11) NOT NULL,
  `quote_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_return_items`
--

DROP TABLE IF EXISTS `sma_return_items`;
CREATE TABLE `sma_return_items` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) UNSIGNED NOT NULL,
  `return_id` int(11) UNSIGNED NOT NULL,
  `sale_item_id` int(11) DEFAULT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `item_uom_id` varchar(100) DEFAULT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `expiry` date DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_return_sales`
--

DROP TABLE IF EXISTS `sma_return_sales`;
CREATE TABLE `sma_return_sales` (
  `id` int(11) UNSIGNED NOT NULL,
  `sale_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `biller_id` int(11) DEFAULT NULL,
  `biller` varchar(55) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount_id` varchar(20) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount` decimal(25,4) DEFAULT '0.0000',
  `product_tax` decimal(25,4) DEFAULT '0.0000',
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT '0.0000',
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `surcharge` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attachment` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_sales`
--

DROP TABLE IF EXISTS `sma_sales`;
CREATE TABLE `sma_sales` (
  `id` int(11) UNSIGNED NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `biller_id` int(11) DEFAULT NULL,
  `biller` varchar(55) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `staff_note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount_id` varchar(20) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount` decimal(25,4) DEFAULT '0.0000',
  `product_tax` decimal(25,4) DEFAULT '0.0000',
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT '0.0000',
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `sale_status` varchar(20) DEFAULT NULL,
  `payment_status` varchar(20) DEFAULT NULL,
  `payment_term` tinyint(4) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total_items` tinyint(4) DEFAULT NULL,
  `pos` tinyint(1) NOT NULL DEFAULT '0',
  `paid` decimal(25,4) DEFAULT '0.0000',
  `return_id` int(11) DEFAULT NULL,
  `surcharge` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `attachment` varchar(55) DEFAULT NULL,
  `sync_flg` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_sale_items`
--

DROP TABLE IF EXISTS `sma_sale_items`;
CREATE TABLE `sma_sale_items` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `item_uom_id` varchar(100) DEFAULT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `expiry` date DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `lot_no` varchar(255) DEFAULT NULL,
  `bin_id` bigint(20) DEFAULT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL,
  `sync_flg` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_segment`
--

DROP TABLE IF EXISTS `sma_segment`;
CREATE TABLE `sma_segment` (
  `id` int(11) NOT NULL,
  `org_id` varchar(20) NOT NULL,
  `prj_doc_id` varchar(40) NOT NULL,
  `parent_proj_id` varchar(40) NOT NULL,
  `proj_name` varchar(100) NOT NULL,
  `prj_actv` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `sma_segment`
--
DROP TRIGGER IF EXISTS `delete_sma_location`;
DELIMITER $$
CREATE TRIGGER `delete_sma_location` BEFORE DELETE ON `sma_segment` FOR EACH ROW BEGIN if OLD.id THEN 

SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot delete locked record'; 

END IF; 

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sma_segment_ent`
--

DROP TABLE IF EXISTS `sma_segment_ent`;
CREATE TABLE `sma_segment_ent` (
  `id` int(11) NOT NULL,
  `org_id` varchar(20) NOT NULL,
  `prj_doc_id` varchar(2) NOT NULL,
  `prj_ent_type_id` varchar(2) NOT NULL,
  `prj_ent_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_sessions`
--

DROP TABLE IF EXISTS `sma_sessions`;
CREATE TABLE `sma_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_settings`
--

DROP TABLE IF EXISTS `sma_settings`;
CREATE TABLE `sma_settings` (
  `setting_id` int(1) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `logo2` varchar(255) NOT NULL,
  `site_name` varchar(55) NOT NULL,
  `language` varchar(20) NOT NULL,
  `default_warehouse` int(2) NOT NULL,
  `accounting_method` tinyint(4) NOT NULL DEFAULT '0',
  `default_currency` varchar(3) NOT NULL,
  `default_tax_rate` int(2) NOT NULL,
  `rows_per_page` int(2) NOT NULL,
  `version` varchar(10) NOT NULL DEFAULT '1.0',
  `default_tax_rate2` int(11) NOT NULL DEFAULT '0',
  `dateformat` int(11) NOT NULL,
  `sales_prefix` varchar(20) DEFAULT NULL,
  `quote_prefix` varchar(20) DEFAULT NULL,
  `purchase_prefix` varchar(20) DEFAULT NULL,
  `transfer_prefix` varchar(20) DEFAULT NULL,
  `delivery_prefix` varchar(20) DEFAULT NULL,
  `payment_prefix` varchar(20) DEFAULT NULL,
  `return_prefix` varchar(20) DEFAULT NULL,
  `expense_prefix` varchar(20) DEFAULT NULL,
  `item_addition` tinyint(1) NOT NULL DEFAULT '0',
  `theme` varchar(20) NOT NULL,
  `product_serial` tinyint(4) NOT NULL,
  `default_discount` int(11) NOT NULL,
  `product_discount` tinyint(1) NOT NULL DEFAULT '0',
  `discount_method` tinyint(4) NOT NULL,
  `tax1` tinyint(4) NOT NULL,
  `tax2` tinyint(4) NOT NULL,
  `overselling` tinyint(1) NOT NULL DEFAULT '0',
  `restrict_user` tinyint(4) NOT NULL DEFAULT '0',
  `restrict_calendar` tinyint(4) NOT NULL DEFAULT '0',
  `timezone` varchar(100) DEFAULT NULL,
  `iwidth` int(11) NOT NULL DEFAULT '0',
  `iheight` int(11) NOT NULL,
  `twidth` int(11) NOT NULL,
  `theight` int(11) NOT NULL,
  `watermark` tinyint(1) DEFAULT NULL,
  `reg_ver` tinyint(1) DEFAULT NULL,
  `allow_reg` tinyint(1) DEFAULT NULL,
  `reg_notification` tinyint(1) DEFAULT NULL,
  `auto_reg` tinyint(1) DEFAULT NULL,
  `protocol` varchar(20) NOT NULL DEFAULT 'mail',
  `mailpath` varchar(55) DEFAULT '/usr/sbin/sendmail',
  `smtp_host` varchar(100) DEFAULT NULL,
  `smtp_user` varchar(100) DEFAULT NULL,
  `smtp_pass` varchar(255) DEFAULT NULL,
  `smtp_port` varchar(10) DEFAULT '25',
  `smtp_crypto` varchar(10) DEFAULT NULL,
  `corn` datetime DEFAULT NULL,
  `customer_group` int(11) NOT NULL,
  `default_email` varchar(100) NOT NULL,
  `mmode` tinyint(1) NOT NULL,
  `bc_fix` tinyint(4) NOT NULL DEFAULT '0',
  `auto_detect_barcode` tinyint(1) NOT NULL DEFAULT '0',
  `captcha` tinyint(1) NOT NULL DEFAULT '1',
  `reference_format` tinyint(1) NOT NULL DEFAULT '1',
  `racks` tinyint(1) DEFAULT '0',
  `attributes` tinyint(1) NOT NULL DEFAULT '0',
  `product_expiry` tinyint(1) NOT NULL DEFAULT '0',
  `decimals` tinyint(2) NOT NULL DEFAULT '2',
  `qty_decimals` tinyint(2) NOT NULL DEFAULT '2',
  `decimals_sep` varchar(2) NOT NULL DEFAULT '.',
  `thousands_sep` varchar(2) NOT NULL DEFAULT ',',
  `invoice_view` tinyint(1) DEFAULT '0',
  `default_biller` int(11) DEFAULT NULL,
  `envato_username` varchar(50) DEFAULT NULL,
  `purchase_code` varchar(100) DEFAULT NULL,
  `rtl` tinyint(1) DEFAULT '0',
  `each_spent` decimal(15,4) DEFAULT NULL,
  `ca_point` tinyint(4) DEFAULT NULL,
  `each_sale` decimal(15,4) DEFAULT NULL,
  `sa_point` tinyint(4) DEFAULT NULL,
  `update` tinyint(1) DEFAULT '0',
  `sac` tinyint(1) DEFAULT '0',
  `display_all_products` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `sma_settings`
--
DROP TRIGGER IF EXISTS `delete_sma_settings`;
DELIMITER $$
CREATE TRIGGER `delete_sma_settings` BEFORE DELETE ON `sma_settings` FOR EACH ROW BEGIN if OLD.setting_id THEN 

SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot delete locked record'; 

END IF; 

END
$$
DELIMITER ;
INSERT INTO `sma_settings` (`setting_id`, `logo`, `logo2`, `site_name`, `language`, `default_warehouse`, `accounting_method`, `default_currency`, `default_tax_rate`, `rows_per_page`, `version`, `default_tax_rate2`, `dateformat`, `sales_prefix`, `quote_prefix`, `purchase_prefix`, `transfer_prefix`, `delivery_prefix`, `payment_prefix`, `return_prefix`, `expense_prefix`, `item_addition`, `theme`, `product_serial`, `default_discount`, `product_discount`, `discount_method`, `tax1`, `tax2`, `overselling`, `restrict_user`, `restrict_calendar`, `timezone`, `iwidth`, `iheight`, `twidth`, `theight`, `watermark`, `reg_ver`, `allow_reg`, `reg_notification`, `auto_reg`, `protocol`, `mailpath`, `smtp_host`, `smtp_user`, `smtp_pass`, `smtp_port`, `smtp_crypto`, `corn`, `customer_group`, `default_email`, `mmode`, `bc_fix`, `auto_detect_barcode`, `captcha`, `reference_format`, `racks`, `attributes`, `product_expiry`, `decimals`, `qty_decimals`, `decimals_sep`, `thousands_sep`, `invoice_view`, `default_biller`, `envato_username`, `purchase_code`, `rtl`, `each_spent`, `ca_point`, `each_sale`, `sa_point`, `update`, `sac`, `display_all_products`) VALUES
(1, 'logo2.png', 'images.png', 'ESS INDIA', 'english', 2, 2, 'SAR', 1, 10, '1.0', 1, 5, 'SALE', 'QUOTE', 'PO', 'TR', 'DO', 'IPAY', 'RETURNSL', '', 0, 'default', 1, 1, 1, 1, 1, 1, 0, 1, 0, 'Asia/Kolkata', 800, 800, 60, 60, 0, 0, 0, 0, NULL, 'mail', '/usr/sbin/sendmail', 'pop.gmail.com', 'contact@tecdiary.com', 'jEFTM4T63AiQ9dsidxhPKt9CIg4HQjCN58n/RW9vmdC/UDXCzRLR469ziZ0jjpFlbOg43LyoSmpJLBkcAHh0Yw==', '25', NULL, NULL, 1, 'contact@tecdiary.com', 0, 4, 1, 0, 2, 1, 1, 0, 2, 2, '.', ',', 0, 3, 'luhartripathi', 'f8a1a55d-0d35-475d-a88c-a61dac988052', 0, NULL, NULL, NULL, NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sma_skrill`
--

DROP TABLE IF EXISTS `sma_skrill`;
CREATE TABLE `sma_skrill` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `account_email` varchar(255) NOT NULL DEFAULT 'testaccount2@moneybookers.com',
  `secret_word` varchar(20) NOT NULL DEFAULT 'mbtest',
  `skrill_currency` varchar(3) NOT NULL DEFAULT 'USD',
  `fixed_charges` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `extra_charges_my` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `extra_charges_other` decimal(25,4) NOT NULL DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_state`
--

DROP TABLE IF EXISTS `sma_state`;
CREATE TABLE `sma_state` (
  `state_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_name` varchar(128) NOT NULL,
  `state_url` varchar(32) NOT NULL,
  `state_latitude` varchar(100) NOT NULL,
  `state_longitude` varchar(100) NOT NULL,
  `default` int(10) NOT NULL DEFAULT '0',
  `state_status` int(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

INSERT INTO `sma_state` (`state_id`, `country_id`, `state_name`, `state_url`, `state_latitude`, `state_longitude`, `default`, `state_status`) VALUES
(1, 1, 'Badakhshan', 'BDS', '', '', 0, 1),
(2, 1, 'Badghis', 'BDG', '', '', 0, 1),
(3, 1, 'Baghlan', 'BGL', '', '', 0, 1),
(4, 1, 'Balkh', 'BAL', '', '', 0, 1),
(5, 1, 'Bamian', 'BAM', '', '', 0, 1),
(6, 1, 'Farah', 'FRA', '', '', 0, 1),
(7, 1, 'Faryab', 'FYB', '', '', 0, 1),
(8, 1, 'Ghazni', 'GHA', '', '', 0, 1),
(9, 1, 'Ghowr', 'GHO', '', '', 0, 1),
(10, 1, 'Helmand', 'HEL', '', '', 0, 1),
(11, 1, 'Herat', 'HER', '', '', 0, 1),
(12, 1, 'Jowzjan', 'JOW', '', '', 0, 1),
(13, 1, 'Kabul', 'KAB', '', '', 0, 1),
(14, 1, 'Kandahar', 'KAN', '', '', 0, 1),
(15, 1, 'Kapisa', 'KAP', '', '', 0, 1),
(16, 1, 'Khost', 'KHO', '', '', 0, 1),
(17, 1, 'Konar', 'KNR', '', '', 0, 1),
(18, 1, 'Kondoz', 'KDZ', '', '', 0, 1),
(19, 1, 'Laghman', 'LAG', '', '', 0, 1),
(20, 1, 'Lowgar', 'LOW', '', '', 0, 1),
(21, 1, 'Nangrahar', 'NAN', '', '', 0, 1),
(22, 1, 'Nimruz', 'NIM', '', '', 0, 1),
(23, 1, 'Nurestan', 'NUR', '', '', 0, 1),
(24, 1, 'Oruzgan', 'ORU', '', '', 0, 1),
(25, 1, 'Paktia', 'PIA', '', '', 0, 1),
(26, 1, 'Paktika', 'PKA', '', '', 0, 1),
(27, 1, 'Parwan', 'PAR', '', '', 0, 1),
(28, 1, 'Samangan', 'SAM', '', '', 0, 1),
(29, 1, 'Sar-e Pol', 'SAR', '', '', 0, 1),
(30, 1, 'Takhar', 'TAK', '', '', 0, 1),
(31, 1, 'Wardak', 'WAR', '', '', 0, 1),
(32, 1, 'Zabol', 'ZAB', '', '', 0, 1),
(33, 2, 'Berat', 'BR', '', '', 0, 1),
(34, 2, 'Bulqize', 'BU', '', '', 0, 1),
(35, 2, 'Delvine', 'DL', '', '', 0, 1),
(36, 2, 'Devoll', 'DV', '', '', 0, 1),
(37, 2, 'Diber', 'DI', '', '', 0, 1),
(38, 2, 'Durres', 'DR', '', '', 0, 1),
(39, 2, 'Elbasan', 'EL', '', '', 0, 1),
(40, 2, 'Kolonje', 'ER', '', '', 0, 1),
(41, 2, 'Fier', 'FR', '', '', 0, 1),
(42, 2, 'Gjirokaster', 'GJ', '', '', 0, 1),
(43, 2, 'Gramsh', 'GR', '', '', 0, 1),
(44, 2, 'Has', 'HA', '', '', 0, 1),
(45, 2, 'Kavaje', 'KA', '', '', 0, 1),
(46, 2, 'Kurbin', 'KB', '', '', 0, 1),
(47, 2, 'Kucove', 'KC', '', '', 0, 1),
(48, 2, 'Korce', 'KO', '', '', 0, 1),
(49, 2, 'Kruje', 'KR', '', '', 0, 1),
(50, 2, 'Kukes', 'KU', '', '', 0, 1),
(51, 2, 'Librazhd', 'LB', '', '', 0, 1),
(52, 2, 'Lezhe', 'LE', '', '', 0, 1),
(53, 2, 'Lushnje', 'LU', '', '', 0, 1),
(54, 2, 'Malesi e Madhe', 'MM', '', '', 0, 1),
(55, 2, 'Mallakaster', 'MK', '', '', 0, 1),
(56, 2, 'Mat', 'MT', '', '', 0, 1),
(57, 2, 'Mirdite', 'MR', '', '', 0, 1),
(58, 2, 'Peqin', 'PQ', '', '', 0, 1),
(59, 2, 'Permet', 'PR', '', '', 0, 1),
(60, 2, 'Pogradec', 'PG', '', '', 0, 1),
(61, 2, 'Puke', 'PU', '', '', 0, 1),
(62, 2, 'Shkoder', 'SH', '', '', 0, 1),
(63, 2, 'Skrapar', 'SK', '', '', 0, 1),
(64, 2, 'Sarande', 'SR', '', '', 0, 1),
(65, 2, 'Tepelene', 'TE', '', '', 0, 1),
(66, 2, 'Tropoje', 'TP', '', '', 0, 1),
(67, 2, 'Tirane', 'TR', '', '', 0, 1),
(68, 2, 'Vlore', 'VL', '', '', 0, 1),
(69, 3, 'Adrar', 'ADR', '', '', 0, 1),
(70, 3, 'Ain Defla', 'ADE', '', '', 0, 1),
(71, 3, 'Ain Temouchent', 'ATE', '', '', 0, 1),
(72, 3, 'Alger', 'ALG', '', '', 0, 1),
(73, 3, 'Annaba', 'ANN', '', '', 0, 1),
(74, 3, 'Batna', 'BAT', '', '', 0, 1),
(75, 3, 'Bechar', 'BEC', '', '', 0, 1),
(76, 3, 'Bejaia', 'BEJ', '', '', 0, 1),
(77, 3, 'Biskra', 'BIS', '', '', 0, 1),
(78, 3, 'Blida', 'BLI', '', '', 0, 1),
(79, 3, 'Bordj Bou Arreridj', 'BBA', '', '', 0, 1),
(80, 3, 'Bouira', 'BOA', '', '', 0, 1),
(81, 3, 'Boumerdes', 'BMD', '', '', 0, 1),
(82, 3, 'Chlef', 'CHL', '', '', 0, 1),
(83, 3, 'Constantine', 'CON', '', '', 0, 1),
(84, 3, 'Djelfa', 'DJE', '', '', 0, 1),
(85, 3, 'El Bayadh', 'EBA', '', '', 0, 1),
(86, 3, 'El Oued', 'EOU', '', '', 0, 1),
(87, 3, 'El Tarf', 'ETA', '', '', 0, 1),
(88, 3, 'Ghardaia', 'GHA', '', '', 0, 1),
(89, 3, 'Guelma', 'GUE', '', '', 0, 1),
(90, 3, 'Illizi', 'ILL', '', '', 0, 1),
(91, 3, 'Jijel', 'JIJ', '', '', 0, 1),
(92, 3, 'Khenchela', 'KHE', '', '', 0, 1),
(93, 3, 'Laghouat', 'LAG', '', '', 0, 1),
(94, 3, 'Muaskar', 'MUA', '', '', 0, 1),
(95, 3, 'Medea', 'MED', '', '', 0, 1),
(96, 3, 'Mila', 'MIL', '', '', 0, 1),
(97, 3, 'Mostaganem', 'MOS', '', '', 0, 1),
(98, 3, 'M''Sila', 'MSI', '', '', 0, 1),
(99, 3, 'Naama', 'NAA', '', '', 0, 1),
(100, 3, 'Oran', 'ORA', '', '', 0, 1),
(101, 3, 'Ouargla', 'OUA', '', '', 0, 1),
(102, 3, 'Oum el-Bouaghi', 'OEB', '', '', 0, 1),
(103, 3, 'Relizane', 'REL', '', '', 0, 1),
(104, 3, 'Saida', 'SAI', '', '', 0, 1),
(105, 3, 'Setif', 'SET', '', '', 0, 1),
(106, 3, 'Sidi Bel Abbes', 'SBA', '', '', 0, 1),
(107, 3, 'Skikda', 'SKI', '', '', 0, 1),
(108, 3, 'Souk Ahras', 'SAH', '', '', 0, 1),
(109, 3, 'Tamanghasset', 'TAM', '', '', 0, 1),
(110, 3, 'Tebessa', 'TEB', '', '', 0, 1),
(111, 3, 'Tiaret', 'TIA', '', '', 0, 1),
(112, 3, 'Tindouf', 'TIN', '', '', 0, 1),
(113, 3, 'Tipaza', 'TIP', '', '', 0, 1),
(114, 3, 'Tissemsilt', 'TIS', '', '', 0, 1),
(115, 3, 'Tizi Ouzou', 'TOU', '', '', 0, 1),
(116, 3, 'Tlemcen', 'TLE', '', '', 0, 1),
(117, 4, 'Eastern', 'E', '', '', 0, 1),
(118, 4, 'Manu''a', 'M', '', '', 0, 1),
(119, 4, 'Rose Island', 'R', '', '', 0, 1),
(120, 4, 'Swains Island', 'S', '', '', 0, 1),
(121, 4, 'Western', 'W', '', '', 0, 1),
(122, 5, 'Andorra la Vella', 'ALV', '', '', 0, 1),
(123, 5, 'Canillo', 'CAN', '', '', 0, 1),
(124, 5, 'Encamp', 'ENC', '', '', 0, 1),
(125, 5, 'Escaldes-Engordany', 'ESE', '', '', 0, 1),
(126, 5, 'La Massana', 'LMA', '', '', 0, 1),
(127, 5, 'Ordino', 'ORD', '', '', 0, 1),
(128, 5, 'Sant Julia de Loria', 'SJL', '', '', 0, 1),
(129, 6, 'Bengo', 'BGO', '', '', 0, 1),
(130, 6, 'Benguela', 'BGU', '', '', 0, 1),
(131, 6, 'Bie', 'BIE', '', '', 0, 1),
(132, 6, 'Cabinda', 'CAB', '', '', 0, 1),
(133, 6, 'Cuando-Cubango', 'CCU', '', '', 0, 1),
(134, 6, 'Cuanza Norte', 'CNO', '', '', 0, 1),
(135, 6, 'Cuanza Sul', 'CUS', '', '', 0, 1),
(136, 6, 'Cunene', 'CNN', '', '', 0, 1),
(137, 6, 'Huambo', 'HUA', '', '', 0, 1),
(138, 6, 'Huila', 'HUI', '', '', 0, 1),
(139, 6, 'Luanda', 'LUA', '', '', 0, 1),
(140, 6, 'Lunda Norte', 'LNO', '', '', 0, 1),
(141, 6, 'Lunda Sul', 'LSU', '', '', 0, 1),
(142, 6, 'Malange', 'MAL', '', '', 0, 1),
(143, 6, 'Moxico', 'MOX', '', '', 0, 1),
(144, 6, 'Namibe', 'NAM', '', '', 0, 1),
(145, 6, 'Uige', 'UIG', '', '', 0, 1),
(146, 6, 'Zaire', 'ZAI', '', '', 0, 1),
(147, 9, 'Saint George', 'ASG', '', '', 0, 1),
(148, 9, 'Saint John', 'ASJ', '', '', 0, 1),
(149, 9, 'Saint Mary', 'ASM', '', '', 0, 1),
(150, 9, 'Saint Paul', 'ASL', '', '', 0, 1),
(151, 9, 'Saint Peter', 'ASR', '', '', 0, 1),
(152, 9, 'Saint Philip', 'ASH', '', '', 0, 1),
(153, 9, 'Barbuda', 'BAR', '', '', 0, 1),
(154, 9, 'Redonda', 'RED', '', '', 0, 1),
(155, 10, 'Antartida e Islas del Atlantico', 'AN', '', '', 0, 1),
(156, 10, 'Buenos Aires', 'BA', '', '', 0, 1),
(157, 10, 'Catamarca', 'CA', '', '', 0, 1),
(158, 10, 'Chaco', 'CH', '', '', 0, 1),
(159, 10, 'Chubut', 'CU', '', '', 0, 1),
(160, 10, 'Cordoba', 'CO', '', '', 0, 1),
(161, 10, 'Corrientes', 'CR', '', '', 0, 1),
(162, 10, 'Distrito Federal', 'DF', '', '', 0, 1),
(163, 10, 'Entre Rios', 'ER', '', '', 0, 1),
(164, 10, 'Formosa', 'FO', '', '', 0, 1),
(165, 10, 'Jujuy', 'JU', '', '', 0, 1),
(166, 10, 'La Pampa', 'LP', '', '', 0, 1),
(167, 10, 'La Rioja', 'LR', '', '', 0, 1),
(168, 10, 'Mendoza', 'ME', '', '', 0, 1),
(169, 10, 'Misiones', 'MI', '', '', 0, 1),
(170, 10, 'Neuquen', 'NE', '', '', 0, 1),
(171, 10, 'Rio Negro', 'RN', '', '', 0, 1),
(172, 10, 'Salta', 'SA', '', '', 0, 1),
(173, 10, 'San Juan', 'SJ', '', '', 0, 1),
(174, 10, 'San Luis', 'SL', '', '', 0, 1),
(175, 10, 'Santa Cruz', 'SC', '', '', 0, 1),
(176, 10, 'Santa Fe', 'SF', '', '', 0, 1),
(177, 10, 'Santiago del Estero', 'SD', '', '', 0, 1),
(178, 10, 'Tierra del Fuego', 'TF', '', '', 0, 1),
(179, 10, 'Tucuman', 'TU', '', '', 0, 1),
(180, 11, 'Aragatsotn', 'AGT', '', '', 0, 1),
(181, 11, 'Ararat', 'ARR', '', '', 0, 1),
(182, 11, 'Armavir', 'ARM', '', '', 0, 1),
(183, 11, 'Geghark''unik''', 'GEG', '', '', 0, 1),
(184, 11, 'Kotayk''', 'KOT', '', '', 0, 1),
(185, 11, 'Lorri', 'LOR', '', '', 0, 1),
(186, 11, 'Shirak', 'SHI', '', '', 0, 1),
(187, 11, 'Syunik''', 'SYU', '', '', 0, 1),
(188, 11, 'Tavush', 'TAV', '', '', 0, 1),
(189, 11, 'Vayots'' Dzor', 'VAY', '', '', 0, 1),
(190, 11, 'Yerevan', 'YER', '', '', 0, 1),
(191, 13, 'Australian Capital Territory', 'ACT', '', '', 0, 1),
(192, 13, 'New South Wales', 'NSW', '', '', 0, 1),
(193, 13, 'Northern Territory', 'NT', '', '', 0, 1),
(194, 13, 'Queensland', 'QLD', '', '', 0, 1),
(195, 13, 'South Australia', 'SA', '', '', 0, 1),
(196, 13, 'Tasmania', 'TAS', '', '', 0, 1),
(197, 13, 'Victoria', 'VIC', '', '', 0, 1),
(198, 13, 'Western Australia', 'WA', '', '', 0, 1),
(199, 14, 'Burgenland', 'BUR', '', '', 0, 1),
(200, 14, 'Kärnten', 'KAR', '', '', 0, 1),
(201, 14, 'Nieder&ouml;sterreich', 'NOS', '', '', 0, 1),
(202, 14, 'Ober&ouml;sterreich', 'OOS', '', '', 0, 1),
(203, 14, 'Salzburg', 'SAL', '', '', 0, 1),
(204, 14, 'Steiermark', 'STE', '', '', 0, 1),
(205, 14, 'Tirol', 'TIR', '', '', 0, 1),
(206, 14, 'Vorarlberg', 'VOR', '', '', 0, 1),
(207, 14, 'Wien', 'WIE', '', '', 0, 1),
(208, 15, 'Ali Bayramli', 'AB', '', '', 0, 1),
(209, 15, 'Abseron', 'ABS', '', '', 0, 1),
(210, 15, 'AgcabAdi', 'AGC', '', '', 0, 1),
(211, 15, 'Agdam', 'AGM', '', '', 0, 1),
(212, 15, 'Agdas', 'AGS', '', '', 0, 1),
(213, 15, 'Agstafa', 'AGA', '', '', 0, 1),
(214, 15, 'Agsu', 'AGU', '', '', 0, 1),
(215, 15, 'Astara', 'AST', '', '', 0, 1),
(216, 15, 'Baki', 'BA', '', '', 0, 1),
(217, 15, 'BabAk', 'BAB', '', '', 0, 1),
(218, 15, 'BalakAn', 'BAL', '', '', 0, 1),
(219, 15, 'BArdA', 'BAR', '', '', 0, 1),
(220, 15, 'Beylaqan', 'BEY', '', '', 0, 1),
(221, 15, 'Bilasuvar', 'BIL', '', '', 0, 1),
(222, 15, 'Cabrayil', 'CAB', '', '', 0, 1),
(223, 15, 'Calilabab', 'CAL', '', '', 0, 1),
(224, 15, 'Culfa', 'CUL', '', '', 0, 1),
(225, 15, 'Daskasan', 'DAS', '', '', 0, 1),
(226, 15, 'Davaci', 'DAV', '', '', 0, 1),
(227, 15, 'Fuzuli', 'FUZ', '', '', 0, 1),
(228, 15, 'Ganca', 'GA', '', '', 0, 1),
(229, 15, 'Gadabay', 'GAD', '', '', 0, 1),
(230, 15, 'Goranboy', 'GOR', '', '', 0, 1),
(231, 15, 'Goycay', 'GOY', '', '', 0, 1),
(232, 15, 'Haciqabul', 'HAC', '', '', 0, 1),
(233, 15, 'Imisli', 'IMI', '', '', 0, 1),
(234, 15, 'Ismayilli', 'ISM', '', '', 0, 1),
(235, 15, 'Kalbacar', 'KAL', '', '', 0, 1),
(236, 15, 'Kurdamir', 'KUR', '', '', 0, 1),
(237, 15, 'Lankaran', 'LA', '', '', 0, 1),
(238, 15, 'Lacin', 'LAC', '', '', 0, 1),
(239, 15, 'Lankaran', 'LAN', '', '', 0, 1),
(240, 15, 'Lerik', 'LER', '', '', 0, 1),
(241, 15, 'Masalli', 'MAS', '', '', 0, 1),
(242, 15, 'Mingacevir', 'MI', '', '', 0, 1),
(243, 15, 'Naftalan', 'NA', '', '', 0, 1),
(244, 15, 'Neftcala', 'NEF', '', '', 0, 1),
(245, 15, 'Oguz', 'OGU', '', '', 0, 1),
(246, 15, 'Ordubad', 'ORD', '', '', 0, 1),
(247, 15, 'Qabala', 'QAB', '', '', 0, 1),
(248, 15, 'Qax', 'QAX', '', '', 0, 1),
(249, 15, 'Qazax', 'QAZ', '', '', 0, 1),
(250, 15, 'Qobustan', 'QOB', '', '', 0, 1),
(251, 15, 'Quba', 'QBA', '', '', 0, 1),
(252, 15, 'Qubadli', 'QBI', '', '', 0, 1),
(253, 15, 'Qusar', 'QUS', '', '', 0, 1),
(254, 15, 'Saki', 'SA', '', '', 0, 1),
(255, 15, 'Saatli', 'SAT', '', '', 0, 1),
(256, 15, 'Sabirabad', 'SAB', '', '', 0, 1),
(257, 15, 'Sadarak', 'SAD', '', '', 0, 1),
(258, 15, 'Sahbuz', 'SAH', '', '', 0, 1),
(259, 15, 'Saki', 'SAK', '', '', 0, 1),
(260, 15, 'Salyan', 'SAL', '', '', 0, 1),
(261, 15, 'Sumqayit', 'SM', '', '', 0, 1),
(262, 15, 'Samaxi', 'SMI', '', '', 0, 1),
(263, 15, 'Samkir', 'SKR', '', '', 0, 1),
(264, 15, 'Samux', 'SMX', '', '', 0, 1),
(265, 15, 'Sarur', 'SAR', '', '', 0, 1),
(266, 15, 'Siyazan', 'SIY', '', '', 0, 1),
(267, 15, 'Susa', 'SS', '', '', 0, 1),
(268, 15, 'Susa', 'SUS', '', '', 0, 1),
(269, 15, 'Tartar', 'TAR', '', '', 0, 1),
(270, 15, 'Tovuz', 'TOV', '', '', 0, 1),
(271, 15, 'Ucar', 'UCA', '', '', 0, 1),
(272, 15, 'Xankandi', 'XA', '', '', 0, 1),
(273, 15, 'Xacmaz', 'XAC', '', '', 0, 1),
(274, 15, 'Xanlar', 'XAN', '', '', 0, 1),
(275, 15, 'Xizi', 'XIZ', '', '', 0, 1),
(276, 15, 'Xocali', 'XCI', '', '', 0, 1),
(277, 15, 'Xocavand', 'XVD', '', '', 0, 1),
(278, 15, 'Yardimli', 'YAR', '', '', 0, 1),
(279, 15, 'Yevlax', 'YEV', '', '', 0, 1),
(280, 15, 'Zangilan', 'ZAN', '', '', 0, 1),
(281, 15, 'Zaqatala', 'ZAQ', '', '', 0, 1),
(282, 15, 'Zardab', 'ZAR', '', '', 0, 1),
(283, 15, 'Naxcivan', 'NX', '', '', 0, 1),
(284, 16, 'Acklins', 'ACK', '', '', 0, 1),
(285, 16, 'Berry Islands', 'BER', '', '', 0, 1),
(286, 16, 'Bimini', 'BIM', '', '', 0, 1),
(287, 16, 'Black Point', 'BLK', '', '', 0, 1),
(288, 16, 'Cat Island', 'CAT', '', '', 0, 1),
(289, 16, 'Central Abaco', 'CAB', '', '', 0, 1),
(290, 16, 'Central Andros', 'CAN', '', '', 0, 1),
(291, 16, 'Central Eleuthera', 'CEL', '', '', 0, 1),
(292, 16, 'City of Freeport', 'FRE', '', '', 0, 1),
(293, 16, 'Crooked Island', 'CRO', '', '', 0, 1),
(294, 16, 'East Grand Bahama', 'EGB', '', '', 0, 1),
(295, 16, 'Exuma', 'EXU', '', '', 0, 1),
(296, 16, 'Grand Cay', 'GRD', '', '', 0, 1),
(297, 16, 'Harbour Island', 'HAR', '', '', 0, 1),
(298, 16, 'Hope Town', 'HOP', '', '', 0, 1),
(299, 16, 'Inagua', 'INA', '', '', 0, 1),
(300, 16, 'Long Island', 'LNG', '', '', 0, 1),
(301, 16, 'Mangrove Cay', 'MAN', '', '', 0, 1),
(302, 16, 'Mayaguana', 'MAY', '', '', 0, 1),
(303, 16, 'Moore''s Island', 'MOO', '', '', 0, 1),
(304, 16, 'North Abaco', 'NAB', '', '', 0, 1),
(305, 16, 'North Andros', 'NAN', '', '', 0, 1),
(306, 16, 'North Eleuthera', 'NEL', '', '', 0, 1),
(307, 16, 'Ragged Island', 'RAG', '', '', 0, 1),
(308, 16, 'Rum Cay', 'RUM', '', '', 0, 1),
(309, 16, 'San Salvador', 'SAL', '', '', 0, 1),
(310, 16, 'South Abaco', 'SAB', '', '', 0, 1),
(311, 16, 'South Andros', 'SAN', '', '', 0, 1),
(312, 16, 'South Eleuthera', 'SEL', '', '', 0, 1),
(313, 16, 'Spanish Wells', 'SWE', '', '', 0, 1),
(314, 16, 'West Grand Bahama', 'WGB', '', '', 0, 1),
(315, 17, 'Capital', 'CAP', '', '', 0, 1),
(316, 17, 'Central', 'CEN', '', '', 0, 1),
(317, 17, 'Muharraq', 'MUH', '', '', 0, 1),
(318, 17, 'Northern', 'NOR', '', '', 0, 1),
(319, 17, 'Southern', 'SOU', '', '', 0, 1),
(320, 18, 'Barisal', 'BAR', '', '', 0, 1),
(321, 18, 'Chittagong', 'CHI', '', '', 0, 1),
(322, 18, 'Dhaka', 'DHA', '', '', 0, 1),
(323, 18, 'Khulna', 'KHU', '', '', 0, 1),
(324, 18, 'Rajshahi', 'RAJ', '', '', 0, 1),
(325, 18, 'Sylhet', 'SYL', '', '', 0, 1),
(326, 19, 'Christ Church', 'CC', '', '', 0, 1),
(327, 19, 'Saint Andrew', 'AND', '', '', 0, 1),
(328, 19, 'Saint George', 'GEO', '', '', 0, 1),
(329, 19, 'Saint James', 'JAM', '', '', 0, 1),
(330, 19, 'Saint John', 'JOH', '', '', 0, 1),
(331, 19, 'Saint Joseph', 'JOS', '', '', 0, 1),
(332, 19, 'Saint Lucy', 'LUC', '', '', 0, 1),
(333, 19, 'Saint Michael', 'MIC', '', '', 0, 1),
(334, 19, 'Saint Peter', 'PET', '', '', 0, 1),
(335, 19, 'Saint Philip', 'PHI', '', '', 0, 1),
(336, 19, 'Saint Thomas', 'THO', '', '', 0, 1),
(337, 20, 'Brestskaya (Brest)', 'BR', '', '', 0, 1),
(338, 20, 'Homyel''skaya (Homyel'')', 'HO', '', '', 0, 1),
(339, 20, 'Horad Minsk', 'HM', '', '', 0, 1),
(340, 20, 'Hrodzyenskaya (Hrodna)', 'HR', '', '', 0, 1),
(341, 20, 'Mahilyowskaya (Mahilyow)', 'MA', '', '', 0, 1),
(342, 20, 'Minskaya', 'MI', '', '', 0, 1),
(343, 20, 'Vitsyebskaya (Vitsyebsk)', 'VI', '', '', 0, 1),
(344, 21, 'Antwerpen', 'VAN', '', '', 0, 1),
(345, 21, 'Brabant Wallon', 'WBR', '', '', 0, 1),
(346, 21, 'Hainaut', 'WHT', '', '', 0, 1),
(347, 21, 'Liège', 'WLG', '', '', 0, 1),
(348, 21, 'Limburg', 'VLI', '', '', 0, 1),
(349, 21, 'Luxembourg', 'WLX', '', '', 0, 1),
(350, 21, 'Namur', 'WNA', '', '', 0, 1),
(351, 21, 'Oost-Vlaanderen', 'VOV', '', '', 0, 1),
(352, 21, 'Vlaams Brabant', 'VBR', '', '', 0, 1),
(353, 21, 'West-Vlaanderen', 'VWV', '', '', 0, 1),
(354, 22, 'Belize', 'BZ', '', '', 0, 1),
(355, 22, 'Cayo', 'CY', '', '', 0, 1),
(356, 22, 'Corozal', 'CR', '', '', 0, 1),
(357, 22, 'Orange Walk', 'OW', '', '', 0, 1),
(358, 22, 'Stann Creek', 'SC', '', '', 0, 1),
(359, 22, 'Toledo', 'TO', '', '', 0, 1),
(360, 23, 'Alibori', 'AL', '', '', 0, 1),
(361, 23, 'Atakora', 'AK', '', '', 0, 1),
(362, 23, 'Atlantique', 'AQ', '', '', 0, 1),
(363, 23, 'Borgou', 'BO', '', '', 0, 1),
(364, 23, 'Collines', 'CO', '', '', 0, 1),
(365, 23, 'Donga', 'DO', '', '', 0, 1),
(366, 23, 'Kouffo', 'KO', '', '', 0, 1),
(367, 23, 'Littoral', 'LI', '', '', 0, 1),
(368, 23, 'Mono', 'MO', '', '', 0, 1),
(369, 23, 'Oueme', 'OU', '', '', 0, 1),
(370, 23, 'Plateau', 'PL', '', '', 0, 1),
(371, 23, 'Zou', 'ZO', '', '', 0, 1),
(372, 24, 'Devonshire', 'DS', '', '', 0, 1),
(373, 24, 'Hamilton City', 'HC', '', '', 0, 1),
(374, 24, 'Hamilton', 'HA', '', '', 0, 1),
(375, 24, 'Paget', 'PG', '', '', 0, 1),
(376, 24, 'Pembroke', 'PB', '', '', 0, 1),
(377, 24, 'Saint George City', 'GC', '', '', 0, 1),
(378, 24, 'Saint George''s', 'SG', '', '', 0, 1),
(379, 24, 'Sandys', 'SA', '', '', 0, 1),
(380, 24, 'Smith''s', 'SM', '', '', 0, 1),
(381, 24, 'Southampton', 'SH', '', '', 0, 1),
(382, 24, 'Warwick', 'WA', '', '', 0, 1),
(383, 25, 'Bumthang', 'BUM', '', '', 0, 1),
(384, 25, 'Chukha', 'CHU', '', '', 0, 1),
(385, 25, 'Dagana', 'DAG', '', '', 0, 1),
(386, 25, 'Gasa', 'GAS', '', '', 0, 1),
(387, 25, 'Haa', 'HAA', '', '', 0, 1),
(388, 25, 'Lhuntse', 'LHU', '', '', 0, 1),
(389, 25, 'Mongar', 'MON', '', '', 0, 1),
(390, 25, 'Paro', 'PAR', '', '', 0, 1),
(391, 25, 'Pemagatshel', 'PEM', '', '', 0, 1),
(392, 25, 'Punakha', 'PUN', '', '', 0, 1),
(393, 25, 'Samdrup Jongkhar', 'SJO', '', '', 0, 1),
(394, 25, 'Samtse', 'SAT', '', '', 0, 1),
(395, 25, 'Sarpang', 'SAR', '', '', 0, 1),
(396, 25, 'Thimphu', 'THI', '', '', 0, 1),
(397, 25, 'Trashigang', 'TRG', '', '', 0, 1),
(398, 25, 'Trashiyangste', 'TRY', '', '', 0, 1),
(399, 25, 'Trongsa', 'TRO', '', '', 0, 1),
(400, 25, 'Tsirang', 'TSI', '', '', 0, 1),
(401, 25, 'Wangdue Phodrang', 'WPH', '', '', 0, 1),
(402, 25, 'Zhemgang', 'ZHE', '', '', 0, 1),
(403, 26, 'Beni', 'BEN', '', '', 0, 1),
(404, 26, 'Chuquisaca', 'CHU', '', '', 0, 1),
(405, 26, 'Cochabamba', 'COC', '', '', 0, 1),
(406, 26, 'La Paz', 'LPZ', '', '', 0, 1),
(407, 26, 'Oruro', 'ORU', '', '', 0, 1),
(408, 26, 'Pando', 'PAN', '', '', 0, 1),
(409, 26, 'Potosi', 'POT', '', '', 0, 1),
(410, 26, 'Santa Cruz', 'SCZ', '', '', 0, 1),
(411, 26, 'Tarija', 'TAR', '', '', 0, 1),
(412, 27, 'Brcko district', 'BRO', '', '', 0, 1),
(413, 27, 'Unsko-Sanski Kanton', 'FUS', '', '', 0, 1),
(414, 27, 'Posavski Kanton', 'FPO', '', '', 0, 1),
(415, 27, 'Tuzlanski Kanton', 'FTU', '', '', 0, 1),
(416, 27, 'Zenicko-Dobojski Kanton', 'FZE', '', '', 0, 1),
(417, 27, 'Bosanskopodrinjski Kanton', 'FBP', '', '', 0, 1),
(418, 27, 'Srednjebosanski Kanton', 'FSB', '', '', 0, 1),
(419, 27, 'Hercegovacko-neretvanski Kanton', 'FHN', '', '', 0, 1),
(420, 27, 'Zapadnohercegovacka Zupanija', 'FZH', '', '', 0, 1),
(421, 27, 'Kanton Sarajevo', 'FSA', '', '', 0, 1),
(422, 27, 'Zapadnobosanska', 'FZA', '', '', 0, 1),
(423, 27, 'Banja Luka', 'SBL', '', '', 0, 1),
(424, 27, 'Doboj', 'SDO', '', '', 0, 1),
(425, 27, 'Bijeljina', 'SBI', '', '', 0, 1),
(426, 27, 'Vlasenica', 'SVL', '', '', 0, 1),
(427, 27, 'Sarajevo-Romanija or Sokolac', 'SSR', '', '', 0, 1),
(428, 27, 'Foca', 'SFO', '', '', 0, 1),
(429, 27, 'Trebinje', 'STR', '', '', 0, 1),
(430, 28, 'Central', 'CE', '', '', 0, 1),
(431, 28, 'Ghanzi', 'GH', '', '', 0, 1),
(432, 28, 'Kgalagadi', 'KD', '', '', 0, 1),
(433, 28, 'Kgatleng', 'KT', '', '', 0, 1),
(434, 28, 'Kweneng', 'KW', '', '', 0, 1),
(435, 28, 'Ngamiland', 'NG', '', '', 0, 1),
(436, 28, 'North East', 'NE', '', '', 0, 1),
(437, 28, 'North West', 'NW', '', '', 0, 1),
(438, 28, 'South East', 'SE', '', '', 0, 1),
(439, 28, 'Southern', 'SO', '', '', 0, 1),
(440, 30, 'Acre', 'AC', '', '', 0, 1),
(441, 30, 'Alagoas', 'AL', '', '', 0, 1),
(442, 30, 'Amapá', 'AP', '', '', 0, 1),
(443, 30, 'Amazonas', 'AM', '', '', 0, 1),
(444, 30, 'Bahia', 'BA', '', '', 0, 1),
(445, 30, 'Ceará', 'CE', '', '', 0, 1),
(446, 30, 'Distrito Federal', 'DF', '', '', 0, 1),
(447, 30, 'Espírito Santo', 'ES', '', '', 0, 1),
(448, 30, 'Goiás', 'GO', '', '', 0, 1),
(449, 30, 'Maranhão', 'MA', '', '', 0, 1),
(450, 30, 'Mato Grosso', 'MT', '', '', 0, 1),
(451, 30, 'Mato Grosso do Sul', 'MS', '', '', 0, 1),
(452, 30, 'Minas Gerais', 'MG', '', '', 0, 1),
(453, 30, 'Pará', 'PA', '', '', 0, 1),
(454, 30, 'Paraíba', 'PB', '', '', 0, 1),
(455, 30, 'Paraná', 'PR', '', '', 0, 1),
(456, 30, 'Pernambuco', 'PE', '', '', 0, 1),
(457, 30, 'Piauí', 'PI', '', '', 0, 1),
(458, 30, 'Rio de Janeiro', 'RJ', '', '', 0, 1),
(459, 30, 'Rio Grande do Norte', 'RN', '', '', 0, 1),
(460, 30, 'Rio Grande do Sul', 'RS', '', '', 0, 1),
(461, 30, 'Rondônia', 'RO', '', '', 0, 1),
(462, 30, 'Roraima', 'RR', '', '', 0, 1),
(463, 30, 'Santa Catarina', 'SC', '', '', 0, 1),
(464, 30, 'São Paulo', 'SP', '', '', 0, 1),
(465, 30, 'Sergipe', 'SE', '', '', 0, 1),
(466, 30, 'Tocantins', 'TO', '', '', 0, 1),
(467, 31, 'Peros Banhos', 'PB', '', '', 0, 1),
(468, 31, 'Salomon Islands', 'SI', '', '', 0, 1),
(469, 31, 'Nelsons Island', 'NI', '', '', 0, 1),
(470, 31, 'Three Brothers', 'TB', '', '', 0, 1),
(471, 31, 'Eagle Islands', 'EA', '', '', 0, 1),
(472, 31, 'Danger Island', 'DI', '', '', 0, 1),
(473, 31, 'Egmont Islands', 'EG', '', '', 0, 1),
(474, 31, 'Diego Garcia', 'DG', '', '', 0, 1),
(475, 32, 'Belait', 'BEL', '', '', 0, 1),
(476, 32, 'Brunei and Muara', 'BRM', '', '', 0, 1),
(477, 32, 'Temburong', 'TEM', '', '', 0, 1),
(478, 32, 'Tutong', 'TUT', '', '', 0, 1),
(479, 33, 'Blagoevgrad', '', '', '', 0, 1),
(480, 33, 'Burgas', '', '', '', 0, 1),
(481, 33, 'Dobrich', '', '', '', 0, 1),
(482, 33, 'Gabrovo', '', '', '', 0, 1),
(483, 33, 'Haskovo', '', '', '', 0, 1),
(484, 33, 'Kardjali', '', '', '', 0, 1),
(485, 33, 'Kyustendil', '', '', '', 0, 1),
(486, 33, 'Lovech', '', '', '', 0, 1),
(487, 33, 'Montana', '', '', '', 0, 1),
(488, 33, 'Pazardjik', '', '', '', 0, 1),
(489, 33, 'Pernik', '', '', '', 0, 1),
(490, 33, 'Pleven', '', '', '', 0, 1),
(491, 33, 'Plovdiv', '', '', '', 0, 1),
(492, 33, 'Razgrad', '', '', '', 0, 1),
(493, 33, 'Shumen', '', '', '', 0, 1),
(494, 33, 'Silistra', '', '', '', 0, 1),
(495, 33, 'Sliven', '', '', '', 0, 1),
(496, 33, 'Smolyan', '', '', '', 0, 1),
(497, 33, 'Sofia', '', '', '', 0, 1),
(498, 33, 'Sofia - town', '', '', '', 0, 1),
(499, 33, 'Stara Zagora', '', '', '', 0, 1),
(500, 33, 'Targovishte', '', '', '', 0, 1),
(501, 33, 'Varna', '', '', '', 0, 1),
(502, 33, 'Veliko Tarnovo', '', '', '', 0, 1),
(503, 33, 'Vidin', '', '', '', 0, 1),
(504, 33, 'Vratza', '', '', '', 0, 1),
(505, 33, 'Yambol', '', '', '', 0, 1),
(506, 34, 'Bale', 'BAL', '', '', 0, 1),
(507, 34, 'Bam', 'BAM', '', '', 0, 1),
(508, 34, 'Banwa', 'BAN', '', '', 0, 1),
(509, 34, 'Bazega', 'BAZ', '', '', 0, 1),
(510, 34, 'Bougouriba', 'BOR', '', '', 0, 1),
(511, 34, 'Boulgou', 'BLG', '', '', 0, 1),
(512, 34, 'Boulkiemde', 'BOK', '', '', 0, 1),
(513, 34, 'Comoe', 'COM', '', '', 0, 1),
(514, 34, 'Ganzourgou', 'GAN', '', '', 0, 1),
(515, 34, 'Gnagna', 'GNA', '', '', 0, 1),
(516, 34, 'Gourma', 'GOU', '', '', 0, 1),
(517, 34, 'Houet', 'HOU', '', '', 0, 1),
(518, 34, 'Ioba', 'IOA', '', '', 0, 1),
(519, 34, 'Kadiogo', 'KAD', '', '', 0, 1),
(520, 34, 'Kenedougou', 'KEN', '', '', 0, 1),
(521, 34, 'Komondjari', 'KOD', '', '', 0, 1),
(522, 34, 'Kompienga', 'KOP', '', '', 0, 1),
(523, 34, 'Kossi', 'KOS', '', '', 0, 1),
(524, 34, 'Koulpelogo', 'KOL', '', '', 0, 1),
(525, 34, 'Kouritenga', 'KOT', '', '', 0, 1),
(526, 34, 'Kourweogo', 'KOW', '', '', 0, 1),
(527, 34, 'Leraba', 'LER', '', '', 0, 1),
(528, 34, 'Loroum', 'LOR', '', '', 0, 1),
(529, 34, 'Mouhoun', 'MOU', '', '', 0, 1),
(530, 34, 'Nahouri', 'NAH', '', '', 0, 1),
(531, 34, 'Namentenga', 'NAM', '', '', 0, 1),
(532, 34, 'Nayala', 'NAY', '', '', 0, 1),
(533, 34, 'Noumbiel', 'NOU', '', '', 0, 1),
(534, 34, 'Oubritenga', 'OUB', '', '', 0, 1),
(535, 34, 'Oudalan', 'OUD', '', '', 0, 1),
(536, 34, 'Passore', 'PAS', '', '', 0, 1),
(537, 34, 'Poni', 'PON', '', '', 0, 1),
(538, 34, 'Sanguie', 'SAG', '', '', 0, 1),
(539, 34, 'Sanmatenga', 'SAM', '', '', 0, 1),
(540, 34, 'Seno', 'SEN', '', '', 0, 1),
(541, 34, 'Sissili', 'SIS', '', '', 0, 1),
(542, 34, 'Soum', 'SOM', '', '', 0, 1),
(543, 34, 'Sourou', 'SOR', '', '', 0, 1),
(544, 34, 'Tapoa', 'TAP', '', '', 0, 1),
(545, 34, 'Tuy', 'TUY', '', '', 0, 1),
(546, 34, 'Yagha', 'YAG', '', '', 0, 1),
(547, 34, 'Yatenga', 'YAT', '', '', 0, 1),
(548, 34, 'Ziro', 'ZIR', '', '', 0, 1),
(549, 34, 'Zondoma', 'ZOD', '', '', 0, 1),
(550, 34, 'Zoundweogo', 'ZOW', '', '', 0, 1),
(551, 35, 'Bubanza', 'BB', '', '', 0, 1),
(552, 35, 'Bujumbura', 'BJ', '', '', 0, 1),
(553, 35, 'Bururi', 'BR', '', '', 0, 1),
(554, 35, 'Cankuzo', 'CA', '', '', 0, 1),
(555, 35, 'Cibitoke', 'CI', '', '', 0, 1),
(556, 35, 'Gitega', 'GI', '', '', 0, 1),
(557, 35, 'Karuzi', 'KR', '', '', 0, 1),
(558, 35, 'Kayanza', 'KY', '', '', 0, 1),
(559, 35, 'Kirundo', 'KI', '', '', 0, 1),
(560, 35, 'Makamba', 'MA', '', '', 0, 1),
(561, 35, 'Muramvya', 'MU', '', '', 0, 1),
(562, 35, 'Muyinga', 'MY', '', '', 0, 1),
(563, 35, 'Mwaro', 'MW', '', '', 0, 1),
(564, 35, 'Ngozi', 'NG', '', '', 0, 1),
(565, 35, 'Rutana', 'RT', '', '', 0, 1),
(566, 35, 'Ruyigi', 'RY', '', '', 0, 1),
(567, 36, 'Phnom Penh', 'PP', '', '', 0, 1),
(568, 36, 'Preah Seihanu (Kompong Som or Sihanoukville)', 'PS', '', '', 0, 1),
(569, 36, 'Pailin', 'PA', '', '', 0, 1),
(570, 36, 'Keb', 'KB', '', '', 0, 1),
(571, 36, 'Banteay Meanchey', 'BM', '', '', 0, 1),
(572, 36, 'Battambang', 'BA', '', '', 0, 1),
(573, 36, 'Kampong Cham', 'KM', '', '', 0, 1),
(574, 36, 'Kampong Chhnang', 'KN', '', '', 0, 1),
(575, 36, 'Kampong Speu', 'KU', '', '', 0, 1),
(576, 36, 'Kampong Som', 'KO', '', '', 0, 1),
(577, 36, 'Kampong Thom', 'KT', '', '', 0, 1),
(578, 36, 'Kampot', 'KP', '', '', 0, 1),
(579, 36, 'Kandal', 'KL', '', '', 0, 1),
(580, 36, 'Kaoh Kong', 'KK', '', '', 0, 1),
(581, 36, 'Kratie', 'KR', '', '', 0, 1),
(582, 36, 'Mondul Kiri', 'MK', '', '', 0, 1),
(583, 36, 'Oddar Meancheay', 'OM', '', '', 0, 1),
(584, 36, 'Pursat', 'PU', '', '', 0, 1),
(585, 36, 'Preah Vihear', 'PR', '', '', 0, 1),
(586, 36, 'Prey Veng', 'PG', '', '', 0, 1),
(587, 36, 'Ratanak Kiri', 'RK', '', '', 0, 1),
(588, 36, 'Siemreap', 'SI', '', '', 0, 1),
(589, 36, 'Stung Treng', 'ST', '', '', 0, 1),
(590, 36, 'Svay Rieng', 'SR', '', '', 0, 1),
(591, 36, 'Takeo', 'TK', '', '', 0, 1),
(592, 37, 'Adamawa (Adamaoua)', 'ADA', '', '', 0, 1),
(593, 37, 'Centre', 'CEN', '', '', 0, 1),
(594, 37, 'East (Est)', 'EST', '', '', 0, 1),
(595, 37, 'Extreme North (Extreme-Nord)', 'EXN', '', '', 0, 1),
(596, 37, 'Littoral', 'LIT', '', '', 0, 1),
(597, 37, 'North (Nord)', 'NOR', '', '', 0, 1),
(598, 37, 'Northwest (Nord-Ouest)', 'NOT', '', '', 0, 1),
(599, 37, 'West (Ouest)', 'OUE', '', '', 0, 1),
(600, 37, 'South (Sud)', 'SUD', '', '', 0, 1),
(601, 37, 'Southwest (Sud-Ouest).', 'SOU', '', '', 0, 1),
(602, 38, 'Alberta', 'AB', '', '', 0, 1),
(603, 38, 'British Columbia', 'BC', '', '', 0, 1),
(604, 38, 'Manitoba', 'MB', '', '', 0, 1),
(605, 38, 'New Brunswick', 'NB', '', '', 0, 1),
(606, 38, 'Newfoundland and Labrador', 'NL', '', '', 0, 1),
(607, 38, 'Northwest Territories', 'NT', '', '', 0, 1),
(608, 38, 'Nova Scotia', 'NS', '', '', 0, 1),
(609, 38, 'Nunavut', 'NU', '', '', 0, 1),
(610, 38, 'Ontario', 'ON', '', '', 0, 1),
(611, 38, 'Prince Edward Island', 'PE', '', '', 0, 1),
(612, 38, 'Qu&eacute;bec', 'QC', '', '', 0, 1),
(613, 38, 'Saskatchewan', 'SK', '', '', 0, 1),
(614, 38, 'Yukon Territory', 'YT', '', '', 0, 1),
(615, 39, 'Boa Vista', 'BV', '', '', 0, 1),
(616, 39, 'Brava', 'BR', '', '', 0, 1),
(617, 39, 'Calheta de Sao Miguel', 'CS', '', '', 0, 1),
(618, 39, 'Maio', 'MA', '', '', 0, 1),
(619, 39, 'Mosteiros', 'MO', '', '', 0, 1),
(620, 39, 'Paul', 'PA', '', '', 0, 1),
(621, 39, 'Porto Novo', 'PN', '', '', 0, 1),
(622, 39, 'Praia', 'PR', '', '', 0, 1),
(623, 39, 'Ribeira Grande', 'RG', '', '', 0, 1),
(624, 39, 'Sal', 'SL', '', '', 0, 1),
(625, 39, 'Santa Catarina', 'CA', '', '', 0, 1),
(626, 39, 'Santa Cruz', 'CR', '', '', 0, 1),
(627, 39, 'Sao Domingos', 'SD', '', '', 0, 1),
(628, 39, 'Sao Filipe', 'SF', '', '', 0, 1),
(629, 39, 'Sao Nicolau', 'SN', '', '', 0, 1),
(630, 39, 'Sao Vicente', 'SV', '', '', 0, 1),
(631, 39, 'Tarrafal', 'TA', '', '', 0, 1),
(632, 40, 'Creek', 'CR', '', '', 0, 1),
(633, 40, 'Eastern', 'EA', '', '', 0, 1),
(634, 40, 'Midland', 'ML', '', '', 0, 1),
(635, 40, 'South Town', 'ST', '', '', 0, 1),
(636, 40, 'Spot Bay', 'SP', '', '', 0, 1),
(637, 40, 'Stake Bay', 'SK', '', '', 0, 1),
(638, 40, 'West End', 'WD', '', '', 0, 1),
(639, 40, 'Western', 'WN', '', '', 0, 1),
(640, 41, 'Bamingui-Bangoran', 'BBA', '', '', 0, 1),
(641, 41, 'Basse-Kotto', 'BKO', '', '', 0, 1),
(642, 41, 'Haute-Kotto', 'HKO', '', '', 0, 1),
(643, 41, 'Haut-Mbomou', 'HMB', '', '', 0, 1),
(644, 41, 'Kemo', 'KEM', '', '', 0, 1),
(645, 41, 'Lobaye', 'LOB', '', '', 0, 1),
(646, 41, 'Mambere-KadeÔ', 'MKD', '', '', 0, 1),
(647, 41, 'Mbomou', 'MBO', '', '', 0, 1),
(648, 41, 'Nana-Mambere', 'NMM', '', '', 0, 1),
(649, 41, 'Ombella-M''Poko', 'OMP', '', '', 0, 1),
(650, 41, 'Ouaka', 'OUK', '', '', 0, 1),
(651, 41, 'Ouham', 'OUH', '', '', 0, 1),
(652, 41, 'Ouham-Pende', 'OPE', '', '', 0, 1),
(653, 41, 'Vakaga', 'VAK', '', '', 0, 1),
(654, 41, 'Nana-Grebizi', 'NGR', '', '', 0, 1),
(655, 41, 'Sangha-Mbaere', 'SMB', '', '', 0, 1),
(656, 41, 'Bangui', 'BAN', '', '', 0, 1),
(657, 42, 'Batha', 'BA', '', '', 0, 1),
(658, 42, 'Biltine', 'BI', '', '', 0, 1),
(659, 42, 'Borkou-Ennedi-Tibesti', 'BE', '', '', 0, 1),
(660, 42, 'Chari-Baguirmi', 'CB', '', '', 0, 1),
(661, 42, 'Guera', 'GU', '', '', 0, 1),
(662, 42, 'Kanem', 'KA', '', '', 0, 1),
(663, 42, 'Lac', 'LA', '', '', 0, 1),
(664, 42, 'Logone Occidental', 'LC', '', '', 0, 1),
(665, 42, 'Logone Oriental', 'LR', '', '', 0, 1),
(666, 42, 'Mayo-Kebbi', 'MK', '', '', 0, 1),
(667, 42, 'Moyen-Chari', 'MC', '', '', 0, 1),
(668, 42, 'Ouaddai', 'OU', '', '', 0, 1),
(669, 42, 'Salamat', 'SA', '', '', 0, 1),
(670, 42, 'Tandjile', 'TA', '', '', 0, 1),
(671, 43, 'Aisen del General Carlos Ibanez', 'AI', '', '', 0, 1),
(672, 43, 'Antofagasta', 'AN', '', '', 0, 1),
(673, 43, 'Araucania', 'AR', '', '', 0, 1),
(674, 43, 'Atacama', 'AT', '', '', 0, 1),
(675, 43, 'Bio-Bio', 'BI', '', '', 0, 1),
(676, 43, 'Coquimbo', 'CO', '', '', 0, 1),
(677, 43, 'Libertador General Bernardo O''Higgins', 'LI', '', '', 0, 1),
(678, 43, 'Los Lagos', 'LL', '', '', 0, 1),
(679, 43, 'Magallanes y de la Antartica Chilena', 'MA', '', '', 0, 1),
(680, 43, 'Maule', 'ML', '', '', 0, 1),
(681, 43, 'Region Metropolitana', 'RM', '', '', 0, 1),
(682, 43, 'Tarapaca', 'TA', '', '', 0, 1),
(683, 43, 'Valparaiso', 'VS', '', '', 0, 1),
(684, 44, 'Anhui', 'AN', '', '', 0, 1),
(685, 44, 'Beijing', 'BE', '', '', 0, 1),
(686, 44, 'Chongqing', 'CH', '', '', 0, 1),
(687, 44, 'Fujian', 'FU', '', '', 0, 1),
(688, 44, 'Gansu', 'GA', '', '', 0, 1),
(689, 44, 'Guangdong', 'GU', '', '', 0, 1),
(690, 44, 'Guangxi', 'GX', '', '', 0, 1),
(691, 44, 'Guizhou', 'GZ', '', '', 0, 1),
(692, 44, 'Hainan', 'HA', '', '', 0, 1),
(693, 44, 'Hebei', 'HB', '', '', 0, 1),
(694, 44, 'Heilongjiang', 'HL', '', '', 0, 1),
(695, 44, 'Henan', 'HE', '', '', 0, 1),
(696, 44, 'Hong Kong', 'HK', '', '', 0, 1),
(697, 44, 'Hubei', 'HU', '', '', 0, 1),
(698, 44, 'Hunan', 'HN', '', '', 0, 1),
(699, 44, 'Inner Mongolia', 'IM', '', '', 0, 1),
(700, 44, 'Jiangsu', 'JI', '', '', 0, 1),
(701, 44, 'Jiangxi', 'JX', '', '', 0, 1),
(702, 44, 'Jilin', 'JL', '', '', 0, 1),
(703, 44, 'Liaoning', 'LI', '', '', 0, 1),
(704, 44, 'Macau', 'MA', '', '', 0, 1),
(705, 44, 'Ningxia', 'NI', '', '', 0, 1),
(706, 44, 'Shaanxi', 'SH', '', '', 0, 1),
(707, 44, 'Shandong', 'SA', '', '', 0, 1),
(708, 44, 'Shanghai', 'SG', '', '', 0, 1),
(709, 44, 'Shanxi', 'SX', '', '', 0, 1),
(710, 44, 'Sichuan', 'SI', '', '', 0, 1),
(711, 44, 'Tianjin', 'TI', '', '', 0, 1),
(712, 44, 'Xinjiang', 'XI', '', '', 0, 1),
(713, 44, 'Yunnan', 'YU', '', '', 0, 1),
(714, 44, 'Zhejiang', 'ZH', '', '', 0, 1),
(715, 46, 'Direction Island', 'D', '', '', 0, 1),
(716, 46, 'Home Island', 'H', '', '', 0, 1),
(717, 46, 'Horsburgh Island', 'O', '', '', 0, 1),
(718, 46, 'South Island', 'S', '', '', 0, 1),
(719, 46, 'West Island', 'W', '', '', 0, 1),
(720, 47, 'Amazonas', 'AMZ', '', '', 0, 1),
(721, 47, 'Antioquia', 'ANT', '', '', 0, 1),
(722, 47, 'Arauca', 'ARA', '', '', 0, 1),
(723, 47, 'Atlantico', 'ATL', '', '', 0, 1),
(724, 47, 'Bogota D.C.', 'BDC', '', '', 0, 1),
(725, 47, 'Bolivar', 'BOL', '', '', 0, 1),
(726, 47, 'Boyaca', 'BOY', '', '', 0, 1),
(727, 47, 'Caldas', 'CAL', '', '', 0, 1),
(728, 47, 'Caqueta', 'CAQ', '', '', 0, 1),
(729, 47, 'Casanare', 'CAS', '', '', 0, 1),
(730, 47, 'Cauca', 'CAU', '', '', 0, 1),
(731, 47, 'Cesar', 'CES', '', '', 0, 1),
(732, 47, 'Choco', 'CHO', '', '', 0, 1),
(733, 47, 'Cordoba', 'COR', '', '', 0, 1),
(734, 47, 'Cundinamarca', 'CAM', '', '', 0, 1),
(735, 47, 'Guainia', 'GNA', '', '', 0, 1),
(736, 47, 'Guajira', 'GJR', '', '', 0, 1),
(737, 47, 'Guaviare', 'GVR', '', '', 0, 1),
(738, 47, 'Huila', 'HUI', '', '', 0, 1),
(739, 47, 'Magdalena', 'MAG', '', '', 0, 1),
(740, 47, 'Meta', 'MET', '', '', 0, 1),
(741, 47, 'Narino', 'NAR', '', '', 0, 1),
(742, 47, 'Norte de Santander', 'NDS', '', '', 0, 1),
(743, 47, 'Putumayo', 'PUT', '', '', 0, 1),
(744, 47, 'Quindio', 'QUI', '', '', 0, 1),
(745, 47, 'Risaralda', 'RIS', '', '', 0, 1),
(746, 47, 'San Andres y Providencia', 'SAP', '', '', 0, 1),
(747, 47, 'Santander', 'SAN', '', '', 0, 1),
(748, 47, 'Sucre', 'SUC', '', '', 0, 1),
(749, 47, 'Tolima', 'TOL', '', '', 0, 1),
(750, 47, 'Valle del Cauca', 'VDC', '', '', 0, 1),
(751, 47, 'Vaupes', 'VAU', '', '', 0, 1),
(752, 47, 'Vichada', 'VIC', '', '', 0, 1),
(753, 48, 'Grande Comore', 'G', '', '', 0, 1),
(754, 48, 'Anjouan', 'A', '', '', 0, 1),
(755, 48, 'Moheli', 'M', '', '', 0, 1),
(756, 49, 'Bouenza', 'BO', '', '', 0, 1),
(757, 49, 'Brazzaville', 'BR', '', '', 0, 1),
(758, 49, 'Cuvette', 'CU', '', '', 0, 1),
(759, 49, 'Cuvette-Ouest', 'CO', '', '', 0, 1),
(760, 49, 'Kouilou', 'KO', '', '', 0, 1),
(761, 49, 'Lekoumou', 'LE', '', '', 0, 1),
(762, 49, 'Likouala', 'LI', '', '', 0, 1),
(763, 49, 'Niari', 'NI', '', '', 0, 1),
(764, 49, 'Plateaux', 'PL', '', '', 0, 1),
(765, 49, 'Pool', 'PO', '', '', 0, 1),
(766, 49, 'Sangha', 'SA', '', '', 0, 1),
(767, 50, 'Pukapuka', 'PU', '', '', 0, 1),
(768, 50, 'Rakahanga', 'RK', '', '', 0, 1),
(769, 50, 'Manihiki', 'MK', '', '', 0, 1),
(770, 50, 'Penrhyn', 'PE', '', '', 0, 1),
(771, 50, 'Nassau Island', 'NI', '', '', 0, 1),
(772, 50, 'Surwarrow', 'SU', '', '', 0, 1),
(773, 50, 'Palmerston', 'PA', '', '', 0, 1),
(774, 50, 'Aitutaki', 'AI', '', '', 0, 1),
(775, 50, 'Manuae', 'MA', '', '', 0, 1),
(776, 50, 'Takutea', 'TA', '', '', 0, 1),
(777, 50, 'Mitiaro', 'MT', '', '', 0, 1),
(778, 50, 'Atiu', 'AT', '', '', 0, 1),
(779, 50, 'Mauke', 'MU', '', '', 0, 1),
(780, 50, 'Rarotonga', 'RR', '', '', 0, 1),
(781, 50, 'Mangaia', 'MG', '', '', 0, 1),
(782, 51, 'Alajuela', 'AL', '', '', 0, 1),
(783, 51, 'Cartago', 'CA', '', '', 0, 1),
(784, 51, 'Guanacaste', 'GU', '', '', 0, 1),
(785, 51, 'Heredia', 'HE', '', '', 0, 1),
(786, 51, 'Limon', 'LI', '', '', 0, 1),
(787, 51, 'Puntarenas', 'PU', '', '', 0, 1),
(788, 51, 'San Jose', 'SJ', '', '', 0, 1),
(789, 52, 'Abengourou', 'ABE', '', '', 0, 1),
(790, 52, 'Abidjan', 'ABI', '', '', 0, 1),
(791, 52, 'Aboisso', 'ABO', '', '', 0, 1),
(792, 52, 'Adiake', 'ADI', '', '', 0, 1),
(793, 52, 'Adzope', 'ADZ', '', '', 0, 1),
(794, 52, 'Agboville', 'AGB', '', '', 0, 1),
(795, 52, 'Agnibilekrou', 'AGN', '', '', 0, 1),
(796, 52, 'Alepe', 'ALE', '', '', 0, 1),
(797, 52, 'Bocanda', 'BOC', '', '', 0, 1),
(798, 52, 'Bangolo', 'BAN', '', '', 0, 1),
(799, 52, 'Beoumi', 'BEO', '', '', 0, 1),
(800, 52, 'Biankouma', 'BIA', '', '', 0, 1),
(801, 52, 'Bondoukou', 'BDK', '', '', 0, 1),
(802, 52, 'Bongouanou', 'BGN', '', '', 0, 1),
(803, 52, 'Bouafle', 'BFL', '', '', 0, 1),
(804, 52, 'Bouake', 'BKE', '', '', 0, 1),
(805, 52, 'Bouna', 'BNA', '', '', 0, 1),
(806, 52, 'Boundiali', 'BDL', '', '', 0, 1),
(807, 52, 'Dabakala', 'DKL', '', '', 0, 1),
(808, 52, 'Dabou', 'DBU', '', '', 0, 1),
(809, 52, 'Daloa', 'DAL', '', '', 0, 1),
(810, 52, 'Danane', 'DAN', '', '', 0, 1),
(811, 52, 'Daoukro', 'DAO', '', '', 0, 1),
(812, 52, 'Dimbokro', 'DIM', '', '', 0, 1),
(813, 52, 'Divo', 'DIV', '', '', 0, 1),
(814, 52, 'Duekoue', 'DUE', '', '', 0, 1),
(815, 52, 'Ferkessedougou', 'FER', '', '', 0, 1),
(816, 52, 'Gagnoa', 'GAG', '', '', 0, 1),
(817, 52, 'Grand-Bassam', 'GBA', '', '', 0, 1),
(818, 52, 'Grand-Lahou', 'GLA', '', '', 0, 1),
(819, 52, 'Guiglo', 'GUI', '', '', 0, 1),
(820, 52, 'Issia', 'ISS', '', '', 0, 1),
(821, 52, 'Jacqueville', 'JAC', '', '', 0, 1),
(822, 52, 'Katiola', 'KAT', '', '', 0, 1),
(823, 52, 'Korhogo', 'KOR', '', '', 0, 1),
(824, 52, 'Lakota', 'LAK', '', '', 0, 1),
(825, 52, 'Man', 'MAN', '', '', 0, 1),
(826, 52, 'Mankono', 'MKN', '', '', 0, 1),
(827, 52, 'Mbahiakro', 'MBA', '', '', 0, 1),
(828, 52, 'Odienne', 'ODI', '', '', 0, 1),
(829, 52, 'Oume', 'OUM', '', '', 0, 1),
(830, 52, 'Sakassou', 'SAK', '', '', 0, 1),
(831, 52, 'San-Pedro', 'SPE', '', '', 0, 1),
(832, 52, 'Sassandra', 'SAS', '', '', 0, 1),
(833, 52, 'Seguela', 'SEG', '', '', 0, 1),
(834, 52, 'Sinfra', 'SIN', '', '', 0, 1),
(835, 52, 'Soubre', 'SOU', '', '', 0, 1),
(836, 52, 'Tabou', 'TAB', '', '', 0, 1),
(837, 52, 'Tanda', 'TAN', '', '', 0, 1),
(838, 52, 'Tiebissou', 'TIE', '', '', 0, 1),
(839, 52, 'Tingrela', 'TIN', '', '', 0, 1),
(840, 52, 'Tiassale', 'TIA', '', '', 0, 1),
(841, 52, 'Touba', 'TBA', '', '', 0, 1),
(842, 52, 'Toulepleu', 'TLP', '', '', 0, 1),
(843, 52, 'Toumodi', 'TMD', '', '', 0, 1),
(844, 52, 'Vavoua', 'VAV', '', '', 0, 1),
(845, 52, 'Yamoussoukro', 'YAM', '', '', 0, 1),
(846, 52, 'Zuenoula', 'ZUE', '', '', 0, 1),
(847, 53, 'Bjelovarsko-bilogorska', 'BB', '', '', 0, 1),
(848, 53, 'Grad Zagreb', 'GZ', '', '', 0, 1),
(849, 53, 'Dubrovačko-neretvanska', 'DN', '', '', 0, 1),
(850, 53, 'Istarska', 'IS', '', '', 0, 1),
(851, 53, 'Karlovačka', 'KA', '', '', 0, 1),
(852, 53, 'Koprivničko-križevačka', 'KK', '', '', 0, 1),
(853, 53, 'Krapinsko-zagorska', 'KZ', '', '', 0, 1),
(854, 53, 'Ličko-senjska', 'LS', '', '', 0, 1),
(855, 53, 'Međimurska', 'ME', '', '', 0, 1),
(856, 53, 'Osječko-baranjska', 'OB', '', '', 0, 1),
(857, 53, 'Požeško-slavonska', 'PS', '', '', 0, 1),
(858, 53, 'Primorsko-goranska', 'PG', '', '', 0, 1),
(859, 53, 'Šibensko-kninska', 'SK', '', '', 0, 1),
(860, 53, 'Sisačko-moslavačka', 'SM', '', '', 0, 1),
(861, 53, 'Brodsko-posavska', 'BP', '', '', 0, 1),
(862, 53, 'Splitsko-dalmatinska', 'SD', '', '', 0, 1),
(863, 53, 'Varaždinska', 'VA', '', '', 0, 1),
(864, 53, 'Virovitičko-podravska', 'VP', '', '', 0, 1),
(865, 53, 'Vukovarsko-srijemska', 'VS', '', '', 0, 1),
(866, 53, 'Zadarska', 'ZA', '', '', 0, 1),
(867, 53, 'Zagrebačka', 'ZG', '', '', 0, 1),
(868, 54, 'Camaguey', 'CA', '', '', 0, 1),
(869, 54, 'Ciego de Avila', 'CD', '', '', 0, 1),
(870, 54, 'Cienfuegos', 'CI', '', '', 0, 1),
(871, 54, 'Ciudad de La Habana', 'CH', '', '', 0, 1),
(872, 54, 'Granma', 'GR', '', '', 0, 1),
(873, 54, 'Guantanamo', 'GU', '', '', 0, 1),
(874, 54, 'Holguin', 'HO', '', '', 0, 1),
(875, 54, 'Isla de la Juventud', 'IJ', '', '', 0, 1),
(876, 54, 'La Habana', 'LH', '', '', 0, 1),
(877, 54, 'Las Tunas', 'LT', '', '', 0, 1),
(878, 54, 'Matanzas', 'MA', '', '', 0, 1),
(879, 54, 'Pinar del Rio', 'PR', '', '', 0, 1),
(880, 54, 'Sancti Spiritus', 'SS', '', '', 0, 1),
(881, 54, 'Santiago de Cuba', 'SC', '', '', 0, 1),
(882, 54, 'Villa Clara', 'VC', '', '', 0, 1),
(883, 55, 'Famagusta', 'F', '', '', 0, 1),
(884, 55, 'Kyrenia', 'K', '', '', 0, 1),
(885, 55, 'Larnaca', 'A', '', '', 0, 1),
(886, 55, 'Limassol', 'I', '', '', 0, 1),
(887, 55, 'Nicosia', 'N', '', '', 0, 1),
(888, 55, 'Paphos', 'P', '', '', 0, 1),
(889, 56, 'Ústecký', 'U', '', '', 0, 1),
(890, 56, 'Jihočeský', 'C', '', '', 0, 1),
(891, 56, 'Jihomoravský', 'B', '', '', 0, 1),
(892, 56, 'Karlovarský', 'K', '', '', 0, 1),
(893, 56, 'Královehradecký', 'H', '', '', 0, 1),
(894, 56, 'Liberecký', 'L', '', '', 0, 1),
(895, 56, 'Moravskoslezský', 'T', '', '', 0, 1),
(896, 56, 'Olomoucký', 'M', '', '', 0, 1),
(897, 56, 'Pardubický', 'E', '', '', 0, 1),
(898, 56, 'Plzeňský', 'P', '', '', 0, 1),
(899, 56, 'Praha', 'A', '', '', 0, 1),
(900, 56, 'Středočeský', 'S', '', '', 0, 1),
(901, 56, 'Vysočina', 'J', '', '', 0, 1),
(902, 56, 'Zlínský', 'Z', '', '', 0, 1),
(903, 57, 'Arhus', 'AR', '', '', 0, 1),
(904, 57, 'Bornholm', 'BH', '', '', 0, 1),
(905, 57, 'Copenhagen', 'CO', '', '', 0, 1),
(906, 57, 'Faroe Islands', 'FO', '', '', 0, 1),
(907, 57, 'Frederiksborg', 'FR', '', '', 0, 1),
(908, 57, 'Fyn', 'FY', '', '', 0, 1),
(909, 57, 'Kobenhavn', 'KO', '', '', 0, 1),
(910, 57, 'Nordjylland', 'NO', '', '', 0, 1),
(911, 57, 'Ribe', 'RI', '', '', 0, 1),
(912, 57, 'Ringkobing', 'RK', '', '', 0, 1),
(913, 57, 'Roskilde', 'RO', '', '', 0, 1),
(914, 57, 'Sonderjylland', 'SO', '', '', 0, 1),
(915, 57, 'Storstrom', 'ST', '', '', 0, 1),
(916, 57, 'Vejle', 'VK', '', '', 0, 1),
(917, 57, 'Vestj&aelig;lland', 'VJ', '', '', 0, 1),
(918, 57, 'Viborg', 'VB', '', '', 0, 1),
(919, 58, 'Ali Sabih', 'S', '', '', 0, 1),
(920, 58, 'Dikhil', 'K', '', '', 0, 1),
(921, 58, 'Djibouti', 'J', '', '', 0, 1),
(922, 58, 'Obock', 'O', '', '', 0, 1),
(923, 58, 'Tadjoura', 'T', '', '', 0, 1),
(924, 59, 'Saint Andrew Parish', 'AND', '', '', 0, 1),
(925, 59, 'Saint David Parish', 'DAV', '', '', 0, 1),
(926, 59, 'Saint George Parish', 'GEO', '', '', 0, 1),
(927, 59, 'Saint John Parish', 'JOH', '', '', 0, 1),
(928, 59, 'Saint Joseph Parish', 'JOS', '', '', 0, 1),
(929, 59, 'Saint Luke Parish', 'LUK', '', '', 0, 1),
(930, 59, 'Saint Mark Parish', 'MAR', '', '', 0, 1),
(931, 59, 'Saint Patrick Parish', 'PAT', '', '', 0, 1),
(932, 59, 'Saint Paul Parish', 'PAU', '', '', 0, 1),
(933, 59, 'Saint Peter Parish', 'PET', '', '', 0, 1),
(934, 60, 'Distrito Nacional', 'DN', '', '', 0, 1),
(935, 60, 'Azua', 'AZ', '', '', 0, 1),
(936, 60, 'Baoruco', 'BC', '', '', 0, 1),
(937, 60, 'Barahona', 'BH', '', '', 0, 1),
(938, 60, 'Dajabon', 'DJ', '', '', 0, 1),
(939, 60, 'Duarte', 'DU', '', '', 0, 1),
(940, 60, 'Elias Pina', 'EL', '', '', 0, 1),
(941, 60, 'El Seybo', 'SY', '', '', 0, 1),
(942, 60, 'Espaillat', 'ET', '', '', 0, 1),
(943, 60, 'Hato Mayor', 'HM', '', '', 0, 1),
(944, 60, 'Independencia', 'IN', '', '', 0, 1),
(945, 60, 'La Altagracia', 'AL', '', '', 0, 1),
(946, 60, 'La Romana', 'RO', '', '', 0, 1),
(947, 60, 'La Vega', 'VE', '', '', 0, 1),
(948, 60, 'Maria Trinidad Sanchez', 'MT', '', '', 0, 1),
(949, 60, 'Monsenor Nouel', 'MN', '', '', 0, 1),
(950, 60, 'Monte Cristi', 'MC', '', '', 0, 1),
(951, 60, 'Monte Plata', 'MP', '', '', 0, 1),
(952, 60, 'Pedernales', 'PD', '', '', 0, 1),
(953, 60, 'Peravia (Bani)', 'PR', '', '', 0, 1),
(954, 60, 'Puerto Plata', 'PP', '', '', 0, 1),
(955, 60, 'Salcedo', 'SL', '', '', 0, 1),
(956, 60, 'Samana', 'SM', '', '', 0, 1),
(957, 60, 'Sanchez Ramirez', 'SH', '', '', 0, 1),
(958, 60, 'San Cristobal', 'SC', '', '', 0, 1),
(959, 60, 'San Jose de Ocoa', 'JO', '', '', 0, 1),
(960, 60, 'San Juan', 'SJ', '', '', 0, 1),
(961, 60, 'San Pedro de Macoris', 'PM', '', '', 0, 1),
(962, 60, 'Santiago', 'SA', '', '', 0, 1),
(963, 60, 'Santiago Rodriguez', 'ST', '', '', 0, 1),
(964, 60, 'Santo Domingo', 'SD', '', '', 0, 1),
(965, 60, 'Valverde', 'VA', '', '', 0, 1),
(966, 61, 'Aileu', 'AL', '', '', 0, 1),
(967, 61, 'Ainaro', 'AN', '', '', 0, 1),
(968, 61, 'Baucau', 'BA', '', '', 0, 1),
(969, 61, 'Bobonaro', 'BO', '', '', 0, 1),
(970, 61, 'Cova Lima', 'CO', '', '', 0, 1),
(971, 61, 'Dili', 'DI', '', '', 0, 1),
(972, 61, 'Ermera', 'ER', '', '', 0, 1),
(973, 61, 'Lautem', 'LA', '', '', 0, 1),
(974, 61, 'Liquica', 'LI', '', '', 0, 1),
(975, 61, 'Manatuto', 'MT', '', '', 0, 1),
(976, 61, 'Manufahi', 'MF', '', '', 0, 1),
(977, 61, 'Oecussi', 'OE', '', '', 0, 1),
(978, 61, 'Viqueque', 'VI', '', '', 0, 1),
(979, 62, 'Azuay', 'AZU', '', '', 0, 1),
(980, 62, 'Bolivar', 'BOL', '', '', 0, 1),
(981, 62, 'Ca&ntilde;ar', 'CAN', '', '', 0, 1),
(982, 62, 'Carchi', 'CAR', '', '', 0, 1),
(983, 62, 'Chimborazo', 'CHI', '', '', 0, 1),
(984, 62, 'Cotopaxi', 'COT', '', '', 0, 1),
(985, 62, 'El Oro', 'EOR', '', '', 0, 1),
(986, 62, 'Esmeraldas', 'ESM', '', '', 0, 1),
(987, 62, 'Gal&aacute;pagos', 'GPS', '', '', 0, 1),
(988, 62, 'Guayas', 'GUA', '', '', 0, 1),
(989, 62, 'Imbabura', 'IMB', '', '', 0, 1),
(990, 62, 'Loja', 'LOJ', '', '', 0, 1),
(991, 62, 'Los Rios', 'LRO', '', '', 0, 1),
(992, 62, 'Manab&iacute;', 'MAN', '', '', 0, 1),
(993, 62, 'Morona Santiago', 'MSA', '', '', 0, 1),
(994, 62, 'Napo', 'NAP', '', '', 0, 1),
(995, 62, 'Orellana', 'ORE', '', '', 0, 1),
(996, 62, 'Pastaza', 'PAS', '', '', 0, 1),
(997, 62, 'Pichincha', 'PIC', '', '', 0, 1),
(998, 62, 'Sucumb&iacute;os', 'SUC', '', '', 0, 1),
(999, 62, 'Tungurahua', 'TUN', '', '', 0, 1),
(1000, 62, 'Zamora Chinchipe', 'ZCH', '', '', 0, 1),
(1001, 63, 'Ad Daqahliyah', 'DHY', '', '', 0, 1),
(1002, 63, 'Al Bahr al Ahmar', 'BAM', '', '', 0, 1),
(1003, 63, 'Al Buhayrah', 'BHY', '', '', 0, 1),
(1004, 63, 'Al Fayyum', 'FYM', '', '', 0, 1),
(1005, 63, 'Al Gharbiyah', 'GBY', '', '', 0, 1),
(1006, 63, 'Al Iskandariyah', 'IDR', '', '', 0, 1),
(1007, 63, 'Al Isma''iliyah', 'IML', '', '', 0, 1),
(1008, 63, 'Al Jizah', 'JZH', '', '', 0, 1),
(1009, 63, 'Al Minufiyah', 'MFY', '', '', 0, 1),
(1010, 63, 'Al Minya', 'MNY', '', '', 0, 1),
(1011, 63, 'Al Qahirah', 'QHR', '', '', 0, 1),
(1012, 63, 'Al Qalyubiyah', 'QLY', '', '', 0, 1),
(1013, 63, 'Al Wadi al Jadid', 'WJD', '', '', 0, 1),
(1014, 63, 'Ash Sharqiyah', 'SHQ', '', '', 0, 1),
(1015, 63, 'As Suways', 'SWY', '', '', 0, 1),
(1016, 63, 'Aswan', 'ASW', '', '', 0, 1),
(1017, 63, 'Asyut', 'ASY', '', '', 0, 1),
(1018, 63, 'Bani Suwayf', 'BSW', '', '', 0, 1),
(1019, 63, 'Bur Sa''id', 'BSD', '', '', 0, 1),
(1020, 63, 'Dumyat', 'DMY', '', '', 0, 1),
(1021, 63, 'Janub Sina''', 'JNS', '', '', 0, 1),
(1022, 63, 'Kafr ash Shaykh', 'KSH', '', '', 0, 1),
(1023, 63, 'Matruh', 'MAT', '', '', 0, 1),
(1024, 63, 'Qina', 'QIN', '', '', 0, 1),
(1025, 63, 'Shamal Sina''', 'SHS', '', '', 0, 1),
(1026, 63, 'Suhaj', 'SUH', '', '', 0, 1),
(1027, 64, 'Ahuachapan', 'AH', '', '', 0, 1),
(1028, 64, 'Cabanas', 'CA', '', '', 0, 1),
(1029, 64, 'Chalatenango', 'CH', '', '', 0, 1),
(1030, 64, 'Cuscatlan', 'CU', '', '', 0, 1),
(1031, 64, 'La Libertad', 'LB', '', '', 0, 1),
(1032, 64, 'La Paz', 'PZ', '', '', 0, 1),
(1033, 64, 'La Union', 'UN', '', '', 0, 1),
(1034, 64, 'Morazan', 'MO', '', '', 0, 1),
(1035, 64, 'San Miguel', 'SM', '', '', 0, 1),
(1036, 64, 'San Salvador', 'SS', '', '', 0, 1),
(1037, 64, 'San Vicente', 'SV', '', '', 0, 1),
(1038, 64, 'Santa Ana', 'SA', '', '', 0, 1),
(1039, 64, 'Sonsonate', 'SO', '', '', 0, 1),
(1040, 64, 'Usulutan', 'US', '', '', 0, 1),
(1041, 65, 'Provincia Annobon', 'AN', '', '', 0, 1),
(1042, 65, 'Provincia Bioko Norte', 'BN', '', '', 0, 1),
(1043, 65, 'Provincia Bioko Sur', 'BS', '', '', 0, 1),
(1044, 65, 'Provincia Centro Sur', 'CS', '', '', 0, 1),
(1045, 65, 'Provincia Kie-Ntem', 'KN', '', '', 0, 1),
(1046, 65, 'Provincia Litoral', 'LI', '', '', 0, 1),
(1047, 65, 'Provincia Wele-Nzas', 'WN', '', '', 0, 1),
(1048, 66, 'Central (Maekel)', 'MA', '', '', 0, 1),
(1049, 66, 'Anseba (Keren)', 'KE', '', '', 0, 1),
(1050, 66, 'Southern Red Sea (Debub-Keih-Bahri)', 'DK', '', '', 0, 1),
(1051, 66, 'Northern Red Sea (Semien-Keih-Bahri)', 'SK', '', '', 0, 1),
(1052, 66, 'Southern (Debub)', 'DE', '', '', 0, 1),
(1053, 66, 'Gash-Barka (Barentu)', 'BR', '', '', 0, 1),
(1054, 67, 'Harjumaa (Tallinn)', 'HA', '', '', 0, 1),
(1055, 67, 'Hiiumaa (Kardla)', 'HI', '', '', 0, 1),
(1056, 67, 'Ida-Virumaa (Johvi)', 'IV', '', '', 0, 1),
(1057, 67, 'Jarvamaa (Paide)', 'JA', '', '', 0, 1),
(1058, 67, 'Jogevamaa (Jogeva)', 'JO', '', '', 0, 1),
(1059, 67, 'Laane-Virumaa (Rakvere)', 'LV', '', '', 0, 1),
(1060, 67, 'Laanemaa (Haapsalu)', 'LA', '', '', 0, 1),
(1061, 67, 'Parnumaa (Parnu)', 'PA', '', '', 0, 1),
(1062, 67, 'Polvamaa (Polva)', 'PO', '', '', 0, 1),
(1063, 67, 'Raplamaa (Rapla)', 'RA', '', '', 0, 1),
(1064, 67, 'Saaremaa (Kuessaare)', 'SA', '', '', 0, 1),
(1065, 67, 'Tartumaa (Tartu)', 'TA', '', '', 0, 1),
(1066, 67, 'Valgamaa (Valga)', 'VA', '', '', 0, 1),
(1067, 67, 'Viljandimaa (Viljandi)', 'VI', '', '', 0, 1),
(1068, 67, 'Vorumaa (Voru)', 'VO', '', '', 0, 1),
(1069, 68, 'Afar', 'AF', '', '', 0, 1),
(1070, 68, 'Amhara', 'AH', '', '', 0, 1),
(1071, 68, 'Benishangul-Gumaz', 'BG', '', '', 0, 1),
(1072, 68, 'Gambela', 'GB', '', '', 0, 1),
(1073, 68, 'Hariai', 'HR', '', '', 0, 1),
(1074, 68, 'Oromia', 'OR', '', '', 0, 1),
(1075, 68, 'Somali', 'SM', '', '', 0, 1),
(1076, 68, 'Southern Nations - Nationalities and Peoples Region', 'SN', '', '', 0, 1),
(1077, 68, 'Tigray', 'TG', '', '', 0, 1),
(1078, 68, 'Addis Ababa', 'AA', '', '', 0, 1),
(1079, 68, 'Dire Dawa', 'DD', '', '', 0, 1),
(1080, 71, 'Central Division', 'C', '', '', 0, 1),
(1081, 71, 'Northern Division', 'N', '', '', 0, 1),
(1082, 71, 'Eastern Division', 'E', '', '', 0, 1),
(1083, 71, 'Western Division', 'W', '', '', 0, 1),
(1084, 71, 'Rotuma', 'R', '', '', 0, 1),
(1085, 72, 'Ahvenanmaan lääni', 'AL', '', '', 0, 1),
(1086, 72, 'Etelä-Suomen lääni', 'ES', '', '', 0, 1),
(1087, 72, 'Itä-Suomen lääni', 'IS', '', '', 0, 1),
(1088, 72, 'Länsi-Suomen lääni', 'LS', '', '', 0, 1),
(1089, 72, 'Lapin lääni', 'LA', '', '', 0, 1),
(1090, 72, 'Oulun lääni', 'OU', '', '', 0, 1),
(1114, 74, 'Ain', '01', '', '', 0, 1),
(1115, 74, 'Aisne', '02', '', '', 0, 1),
(1116, 74, 'Allier', '03', '', '', 0, 1),
(1117, 74, 'Alpes de Haute Provence', '04', '', '', 0, 1),
(1118, 74, 'Hautes-Alpes', '05', '', '', 0, 1),
(1119, 74, 'Alpes Maritimes', '06', '', '', 0, 1),
(1120, 74, 'Ard&egrave;che', '07', '', '', 0, 1),
(1121, 74, 'Ardennes', '08', '', '', 0, 1),
(1122, 74, 'Ari&egrave;ge', '09', '', '', 0, 1),
(1123, 74, 'Aube', '10', '', '', 0, 1),
(1124, 74, 'Aude', '11', '', '', 0, 1),
(1125, 74, 'Aveyron', '12', '', '', 0, 1),
(1126, 74, 'Bouches du Rh&ocirc;ne', '13', '', '', 0, 1),
(1127, 74, 'Calvados', '14', '', '', 0, 1),
(1128, 74, 'Cantal', '15', '', '', 0, 1),
(1129, 74, 'Charente', '16', '', '', 0, 1),
(1130, 74, 'Charente Maritime', '17', '', '', 0, 1),
(1131, 74, 'Cher', '18', '', '', 0, 1),
(1132, 74, 'Corr&egrave;ze', '19', '', '', 0, 1),
(1133, 74, 'Corse du Sud', '2A', '', '', 0, 1),
(1134, 74, 'Haute Corse', '2B', '', '', 0, 1),
(1135, 74, 'C&ocirc;te d&#039;or', '21', '', '', 0, 1),
(1136, 74, 'C&ocirc;tes d&#039;Armor', '22', '', '', 0, 1),
(1137, 74, 'Creuse', '23', '', '', 0, 1),
(1138, 74, 'Dordogne', '24', '', '', 0, 1),
(1139, 74, 'Doubs', '25', '', '', 0, 1),
(1140, 74, 'Dr&ocirc;me', '26', '', '', 0, 1),
(1141, 74, 'Eure', '27', '', '', 0, 1),
(1142, 74, 'Eure et Loir', '28', '', '', 0, 1),
(1143, 74, 'Finist&egrave;re', '29', '', '', 0, 1),
(1144, 74, 'Gard', '30', '', '', 0, 1),
(1145, 74, 'Haute Garonne', '31', '', '', 0, 1),
(1146, 74, 'Gers', '32', '', '', 0, 1),
(1147, 74, 'Gironde', '33', '', '', 0, 1),
(1148, 74, 'H&eacute;rault', '34', '', '', 0, 1),
(1149, 74, 'Ille et Vilaine', '35', '', '', 0, 1),
(1150, 74, 'Indre', '36', '', '', 0, 1),
(1151, 74, 'Indre et Loire', '37', '', '', 0, 1),
(1152, 74, 'Is&eacute;re', '38', '', '', 0, 1),
(1153, 74, 'Jura', '39', '', '', 0, 1),
(1154, 74, 'Landes', '40', '', '', 0, 1),
(1155, 74, 'Loir et Cher', '41', '', '', 0, 1),
(1156, 74, 'Loire', '42', '', '', 0, 1),
(1157, 74, 'Haute Loire', '43', '', '', 0, 1),
(1158, 74, 'Loire Atlantique', '44', '', '', 0, 1),
(1159, 74, 'Loiret', '45', '', '', 0, 1),
(1160, 74, 'Lot', '46', '', '', 0, 1),
(1161, 74, 'Lot et Garonne', '47', '', '', 0, 1),
(1162, 74, 'Loz&egrave;re', '48', '', '', 0, 1),
(1163, 74, 'Maine et Loire', '49', '', '', 0, 1),
(1164, 74, 'Manche', '50', '', '', 0, 1),
(1165, 74, 'Marne', '51', '', '', 0, 1),
(1166, 74, 'Haute Marne', '52', '', '', 0, 1),
(1167, 74, 'Mayenne', '53', '', '', 0, 1),
(1168, 74, 'Meurthe et Moselle', '54', '', '', 0, 1),
(1169, 74, 'Meuse', '55', '', '', 0, 1),
(1170, 74, 'Morbihan', '56', '', '', 0, 1),
(1171, 74, 'Moselle', '57', '', '', 0, 1),
(1172, 74, 'Ni&egrave;vre', '58', '', '', 0, 1),
(1173, 74, 'Nord', '59', '', '', 0, 1),
(1174, 74, 'Oise', '60', '', '', 0, 1),
(1175, 74, 'Orne', '61', '', '', 0, 1),
(1176, 74, 'Pas de Calais', '62', '', '', 0, 1),
(1177, 74, 'Puy de D&ocirc;me', '63', '', '', 0, 1),
(1178, 74, 'Pyr&eacute;n&eacute;es Atlantiques', '64', '', '', 0, 1),
(1179, 74, 'Hautes Pyr&eacute;n&eacute;es', '65', '', '', 0, 1),
(1180, 74, 'Pyr&eacute;n&eacute;es Orientales', '66', '', '', 0, 1),
(1181, 74, 'Bas Rhin', '67', '', '', 0, 1),
(1182, 74, 'Haut Rhin', '68', '', '', 0, 1),
(1183, 74, 'Rh&ocirc;ne', '69', '', '', 0, 1),
(1184, 74, 'Haute Sa&ocirc;ne', '70', '', '', 0, 1),
(1185, 74, 'Sa&ocirc;ne et Loire', '71', '', '', 0, 1);
INSERT INTO `sma_state` (`state_id`, `country_id`, `state_name`, `state_url`, `state_latitude`, `state_longitude`, `default`, `state_status`) VALUES
(1186, 74, 'Sarthe', '72', '', '', 0, 1),
(1187, 74, 'Savoie', '73', '', '', 0, 1),
(1188, 74, 'Haute Savoie', '74', '', '', 0, 1),
(1189, 74, 'Paris', '75', '', '', 0, 1),
(1190, 74, 'Seine Maritime', '76', '', '', 0, 1),
(1191, 74, 'Seine et Marne', '77', '', '', 0, 1),
(1192, 74, 'Yvelines', '78', '', '', 0, 1),
(1193, 74, 'Deux S&egrave;vres', '79', '', '', 0, 1),
(1194, 74, 'Somme', '80', '', '', 0, 1),
(1195, 74, 'Tarn', '81', '', '', 0, 1),
(1196, 74, 'Tarn et Garonne', '82', '', '', 0, 1),
(1197, 74, 'Var', '83', '', '', 0, 1),
(1198, 74, 'Vaucluse', '84', '', '', 0, 1),
(1199, 74, 'Vend&eacute;e', '85', '', '', 0, 1),
(1200, 74, 'Vienne', '86', '', '', 0, 1),
(1201, 74, 'Haute Vienne', '87', '', '', 0, 1),
(1202, 74, 'Vosges', '88', '', '', 0, 1),
(1203, 74, 'Yonne', '89', '', '', 0, 1),
(1204, 74, 'Territoire de Belfort', '90', '', '', 0, 1),
(1205, 74, 'Essonne', '91', '', '', 0, 1),
(1206, 74, 'Hauts de Seine', '92', '', '', 0, 1),
(1207, 74, 'Seine St-Denis', '93', '', '', 0, 1),
(1208, 74, 'Val de Marne', '94', '', '', 0, 1),
(1209, 74, 'Val d''Oise', '95', '', '', 0, 1),
(1210, 76, 'Archipel des Marquises', 'M', '', '', 0, 1),
(1211, 76, 'Archipel des Tuamotu', 'T', '', '', 0, 1),
(1212, 76, 'Archipel des Tubuai', 'I', '', '', 0, 1),
(1213, 76, 'Iles du Vent', 'V', '', '', 0, 1),
(1214, 76, 'Iles Sous-le-Vent', 'S', '', '', 0, 1),
(1215, 77, 'Iles Crozet', 'C', '', '', 0, 1),
(1216, 77, 'Iles Kerguelen', 'K', '', '', 0, 1),
(1217, 77, 'Ile Amsterdam', 'A', '', '', 0, 1),
(1218, 77, 'Ile Saint-Paul', 'P', '', '', 0, 1),
(1219, 77, 'Adelie Land', 'D', '', '', 0, 1),
(1220, 78, 'Estuaire', 'ES', '', '', 0, 1),
(1221, 78, 'Haut-Ogooue', 'HO', '', '', 0, 1),
(1222, 78, 'Moyen-Ogooue', 'MO', '', '', 0, 1),
(1223, 78, 'Ngounie', 'NG', '', '', 0, 1),
(1224, 78, 'Nyanga', 'NY', '', '', 0, 1),
(1225, 78, 'Ogooue-Ivindo', 'OI', '', '', 0, 1),
(1226, 78, 'Ogooue-Lolo', 'OL', '', '', 0, 1),
(1227, 78, 'Ogooue-Maritime', 'OM', '', '', 0, 1),
(1228, 78, 'Woleu-Ntem', 'WN', '', '', 0, 1),
(1229, 79, 'Banjul', 'BJ', '', '', 0, 1),
(1230, 79, 'Basse', 'BS', '', '', 0, 1),
(1231, 79, 'Brikama', 'BR', '', '', 0, 1),
(1232, 79, 'Janjangbure', 'JA', '', '', 0, 1),
(1233, 79, 'Kanifeng', 'KA', '', '', 0, 1),
(1234, 79, 'Kerewan', 'KE', '', '', 0, 1),
(1235, 79, 'Kuntaur', 'KU', '', '', 0, 1),
(1236, 79, 'Mansakonko', 'MA', '', '', 0, 1),
(1237, 79, 'Lower River', 'LR', '', '', 0, 1),
(1238, 79, 'Central River', 'CR', '', '', 0, 1),
(1239, 79, 'North Bank', 'NB', '', '', 0, 1),
(1240, 79, 'Upper River', 'UR', '', '', 0, 1),
(1241, 79, 'Western', 'WE', '', '', 0, 1),
(1242, 80, 'Abkhazia', 'AB', '', '', 0, 1),
(1243, 80, 'Ajaria', 'AJ', '', '', 0, 1),
(1244, 80, 'Tbilisi', 'TB', '', '', 0, 1),
(1245, 80, 'Guria', 'GU', '', '', 0, 1),
(1246, 80, 'Imereti', 'IM', '', '', 0, 1),
(1247, 80, 'Kakheti', 'KA', '', '', 0, 1),
(1248, 80, 'Kvemo Kartli', 'KK', '', '', 0, 1),
(1249, 80, 'Mtskheta-Mtianeti', 'MM', '', '', 0, 1),
(1250, 80, 'Racha Lechkhumi and Kvemo Svanet', 'RL', '', '', 0, 1),
(1251, 80, 'Samegrelo-Zemo Svaneti', 'SZ', '', '', 0, 1),
(1252, 80, 'Samtskhe-Javakheti', 'SJ', '', '', 0, 1),
(1253, 80, 'Shida Kartli', 'SK', '', '', 0, 1),
(1254, 81, 'Baden-W&uuml;rttemberg', 'BAW', '', '', 0, 1),
(1255, 81, 'Bayern', 'BAY', '', '', 0, 1),
(1256, 81, 'Berlin', 'BER', '', '', 0, 1),
(1257, 81, 'Brandenburg', 'BRG', '', '', 0, 1),
(1258, 81, 'Bremen', 'BRE', '', '', 0, 1),
(1259, 81, 'Hamburg', 'HAM', '', '', 0, 1),
(1260, 81, 'Hessen', 'HES', '', '', 0, 1),
(1261, 81, 'Mecklenburg-Vorpommern', 'MEC', '', '', 0, 1),
(1262, 81, 'Niedersachsen', 'NDS', '', '', 0, 1),
(1263, 81, 'Nordrhein-Westfalen', 'NRW', '', '', 0, 1),
(1264, 81, 'Rheinland-Pfalz', 'RHE', '', '', 0, 1),
(1265, 81, 'Saarland', 'SAR', '', '', 0, 1),
(1266, 81, 'Sachsen', 'SAS', '', '', 0, 1),
(1267, 81, 'Sachsen-Anhalt', 'SAC', '', '', 0, 1),
(1268, 81, 'Schleswig-Holstein', 'SCN', '', '', 0, 1),
(1269, 81, 'Th&uuml;ringen', 'THE', '', '', 0, 1),
(1270, 82, 'Ashanti Region', 'AS', '', '', 0, 1),
(1271, 82, 'Brong-Ahafo Region', 'BA', '', '', 0, 1),
(1272, 82, 'Central Region', 'CE', '', '', 0, 1),
(1273, 82, 'Eastern Region', 'EA', '', '', 0, 1),
(1274, 82, 'Greater Accra Region', 'GA', '', '', 0, 1),
(1275, 82, 'Northern Region', 'NO', '', '', 0, 1),
(1276, 82, 'Upper East Region', 'UE', '', '', 0, 1),
(1277, 82, 'Upper West Region', 'UW', '', '', 0, 1),
(1278, 82, 'Volta Region', 'VO', '', '', 0, 1),
(1279, 82, 'Western Region', 'WE', '', '', 0, 1),
(1280, 84, 'Attica', 'AT', '', '', 0, 1),
(1281, 84, 'Central Greece', 'CN', '', '', 0, 1),
(1282, 84, 'Central Macedonia', 'CM', '', '', 0, 1),
(1283, 84, 'Crete', 'CR', '', '', 0, 1),
(1284, 84, 'East Macedonia and Thrace', 'EM', '', '', 0, 1),
(1285, 84, 'Epirus', 'EP', '', '', 0, 1),
(1286, 84, 'Ionian Islands', 'II', '', '', 0, 1),
(1287, 84, 'North Aegean', 'NA', '', '', 0, 1),
(1288, 84, 'Peloponnesos', 'PP', '', '', 0, 1),
(1289, 84, 'South Aegean', 'SA', '', '', 0, 1),
(1290, 84, 'Thessaly', 'TH', '', '', 0, 1),
(1291, 84, 'West Greece', 'WG', '', '', 0, 1),
(1292, 84, 'West Macedonia', 'WM', '', '', 0, 1),
(1293, 85, 'Avannaa', 'A', '', '', 0, 1),
(1294, 85, 'Tunu', 'T', '', '', 0, 1),
(1295, 85, 'Kitaa', 'K', '', '', 0, 1),
(1296, 86, 'Saint Andrew', 'A', '', '', 0, 1),
(1297, 86, 'Saint David', 'D', '', '', 0, 1),
(1298, 86, 'Saint George', 'G', '', '', 0, 1),
(1299, 86, 'Saint John', 'J', '', '', 0, 1),
(1300, 86, 'Saint Mark', 'M', '', '', 0, 1),
(1301, 86, 'Saint Patrick', 'P', '', '', 0, 1),
(1302, 86, 'Carriacou', 'C', '', '', 0, 1),
(1303, 86, 'Petit Martinique', 'Q', '', '', 0, 1),
(1304, 89, 'Alta Verapaz', 'AV', '', '', 0, 1),
(1305, 89, 'Baja Verapaz', 'BV', '', '', 0, 1),
(1306, 89, 'Chimaltenango', 'CM', '', '', 0, 1),
(1307, 89, 'Chiquimula', 'CQ', '', '', 0, 1),
(1308, 89, 'El Peten', 'PE', '', '', 0, 1),
(1309, 89, 'El Progreso', 'PR', '', '', 0, 1),
(1310, 89, 'El Quiche', 'QC', '', '', 0, 1),
(1311, 89, 'Escuintla', 'ES', '', '', 0, 1),
(1312, 89, 'Guatemala', 'GU', '', '', 0, 1),
(1313, 89, 'Huehuetenango', 'HU', '', '', 0, 1),
(1314, 89, 'Izabal', 'IZ', '', '', 0, 1),
(1315, 89, 'Jalapa', 'JA', '', '', 0, 1),
(1316, 89, 'Jutiapa', 'JU', '', '', 0, 1),
(1317, 89, 'Quetzaltenango', 'QZ', '', '', 0, 1),
(1318, 89, 'Retalhuleu', 'RE', '', '', 0, 1),
(1319, 89, 'Sacatepequez', 'ST', '', '', 0, 1),
(1320, 89, 'San Marcos', 'SM', '', '', 0, 1),
(1321, 89, 'Santa Rosa', 'SR', '', '', 0, 1),
(1322, 89, 'Solola', 'SO', '', '', 0, 1),
(1323, 89, 'Suchitepequez', 'SU', '', '', 0, 1),
(1324, 89, 'Totonicapan', 'TO', '', '', 0, 1),
(1325, 89, 'Zacapa', 'ZA', '', '', 0, 1),
(1326, 90, 'Conakry', 'CNK', '', '', 0, 1),
(1327, 90, 'Beyla', 'BYL', '', '', 0, 1),
(1328, 90, 'Boffa', 'BFA', '', '', 0, 1),
(1329, 90, 'Boke', 'BOK', '', '', 0, 1),
(1330, 90, 'Coyah', 'COY', '', '', 0, 1),
(1331, 90, 'Dabola', 'DBL', '', '', 0, 1),
(1332, 90, 'Dalaba', 'DLB', '', '', 0, 1),
(1333, 90, 'Dinguiraye', 'DGR', '', '', 0, 1),
(1334, 90, 'Dubreka', 'DBR', '', '', 0, 1),
(1335, 90, 'Faranah', 'FRN', '', '', 0, 1),
(1336, 90, 'Forecariah', 'FRC', '', '', 0, 1),
(1337, 90, 'Fria', 'FRI', '', '', 0, 1),
(1338, 90, 'Gaoual', 'GAO', '', '', 0, 1),
(1339, 90, 'Gueckedou', 'GCD', '', '', 0, 1),
(1340, 90, 'Kankan', 'KNK', '', '', 0, 1),
(1341, 90, 'Kerouane', 'KRN', '', '', 0, 1),
(1342, 90, 'Kindia', 'KND', '', '', 0, 1),
(1343, 90, 'Kissidougou', 'KSD', '', '', 0, 1),
(1344, 90, 'Koubia', 'KBA', '', '', 0, 1),
(1345, 90, 'Koundara', 'KDA', '', '', 0, 1),
(1346, 90, 'Kouroussa', 'KRA', '', '', 0, 1),
(1347, 90, 'Labe', 'LAB', '', '', 0, 1),
(1348, 90, 'Lelouma', 'LLM', '', '', 0, 1),
(1349, 90, 'Lola', 'LOL', '', '', 0, 1),
(1350, 90, 'Macenta', 'MCT', '', '', 0, 1),
(1351, 90, 'Mali', 'MAL', '', '', 0, 1),
(1352, 90, 'Mamou', 'MAM', '', '', 0, 1),
(1353, 90, 'Mandiana', 'MAN', '', '', 0, 1),
(1354, 90, 'Nzerekore', 'NZR', '', '', 0, 1),
(1355, 90, 'Pita', 'PIT', '', '', 0, 1),
(1356, 90, 'Siguiri', 'SIG', '', '', 0, 1),
(1357, 90, 'Telimele', 'TLM', '', '', 0, 1),
(1358, 90, 'Tougue', 'TOG', '', '', 0, 1),
(1359, 90, 'Yomou', 'YOM', '', '', 0, 1),
(1360, 91, 'Bafata Region', 'BF', '', '', 0, 1),
(1361, 91, 'Biombo Region', 'BB', '', '', 0, 1),
(1362, 91, 'Bissau Region', 'BS', '', '', 0, 1),
(1363, 91, 'Bolama Region', 'BL', '', '', 0, 1),
(1364, 91, 'Cacheu Region', 'CA', '', '', 0, 1),
(1365, 91, 'Gabu Region', 'GA', '', '', 0, 1),
(1366, 91, 'Oio Region', 'OI', '', '', 0, 1),
(1367, 91, 'Quinara Region', 'QU', '', '', 0, 1),
(1368, 91, 'Tombali Region', 'TO', '', '', 0, 1),
(1369, 92, 'Barima-Waini', 'BW', '', '', 0, 1),
(1370, 92, 'Cuyuni-Mazaruni', 'CM', '', '', 0, 1),
(1371, 92, 'Demerara-Mahaica', 'DM', '', '', 0, 1),
(1372, 92, 'East Berbice-Corentyne', 'EC', '', '', 0, 1),
(1373, 92, 'Essequibo Islands-West Demerara', 'EW', '', '', 0, 1),
(1374, 92, 'Mahaica-Berbice', 'MB', '', '', 0, 1),
(1375, 92, 'Pomeroon-Supenaam', 'PM', '', '', 0, 1),
(1376, 92, 'Potaro-Siparuni', 'PI', '', '', 0, 1),
(1377, 92, 'Upper Demerara-Berbice', 'UD', '', '', 0, 1),
(1378, 92, 'Upper Takutu-Upper Essequibo', 'UT', '', '', 0, 1),
(1379, 93, 'Artibonite', 'AR', '', '', 0, 1),
(1380, 93, 'Centre', 'CE', '', '', 0, 1),
(1381, 93, 'Grand''Anse', 'GA', '', '', 0, 1),
(1382, 93, 'Nord', 'ND', '', '', 0, 1),
(1383, 93, 'Nord-Est', 'NE', '', '', 0, 1),
(1384, 93, 'Nord-Ouest', 'NO', '', '', 0, 1),
(1385, 93, 'Ouest', 'OU', '', '', 0, 1),
(1386, 93, 'Sud', 'SD', '', '', 0, 1),
(1387, 93, 'Sud-Est', 'SE', '', '', 0, 1),
(1388, 94, 'Flat Island', 'F', '', '', 0, 1),
(1389, 94, 'McDonald Island', 'M', '', '', 0, 1),
(1390, 94, 'Shag Island', 'S', '', '', 0, 1),
(1391, 94, 'Heard Island', 'H', '', '', 0, 1),
(1392, 95, 'Atlantida', 'AT', '', '', 0, 1),
(1393, 95, 'Choluteca', 'CH', '', '', 0, 1),
(1394, 95, 'Colon', 'CL', '', '', 0, 1),
(1395, 95, 'Comayagua', 'CM', '', '', 0, 1),
(1396, 95, 'Copan', 'CP', '', '', 0, 1),
(1397, 95, 'Cortes', 'CR', '', '', 0, 1),
(1398, 95, 'El Paraiso', 'PA', '', '', 0, 1),
(1399, 95, 'Francisco Morazan', 'FM', '', '', 0, 1),
(1400, 95, 'Gracias a Dios', 'GD', '', '', 0, 1),
(1401, 95, 'Intibuca', 'IN', '', '', 0, 1),
(1402, 95, 'Islas de la Bahia (Bay Islands)', 'IB', '', '', 0, 1),
(1403, 95, 'La Paz', 'PZ', '', '', 0, 1),
(1404, 95, 'Lempira', 'LE', '', '', 0, 1),
(1405, 95, 'Ocotepeque', 'OC', '', '', 0, 1),
(1406, 95, 'Olancho', 'OL', '', '', 0, 1),
(1407, 95, 'Santa Barbara', 'SB', '', '', 0, 1),
(1408, 95, 'Valle', 'VA', '', '', 0, 1),
(1409, 95, 'Yoro', 'YO', '', '', 0, 1),
(1410, 96, 'Central and Western Hong Kong Island', 'HCW', '', '', 0, 1),
(1411, 96, 'Eastern Hong Kong Island', 'HEA', '', '', 0, 1),
(1412, 96, 'Southern Hong Kong Island', 'HSO', '', '', 0, 1),
(1413, 96, 'Wan Chai Hong Kong Island', 'HWC', '', '', 0, 1),
(1414, 96, 'Kowloon City Kowloon', 'KKC', '', '', 0, 1),
(1415, 96, 'Kwun Tong Kowloon', 'KKT', '', '', 0, 1),
(1416, 96, 'Sham Shui Po Kowloon', 'KSS', '', '', 0, 1),
(1417, 96, 'Wong Tai Sin Kowloon', 'KWT', '', '', 0, 1),
(1418, 96, 'Yau Tsim Mong Kowloon', 'KYT', '', '', 0, 1),
(1419, 96, 'Islands New Territories', 'NIS', '', '', 0, 1),
(1420, 96, 'Kwai Tsing New Territories', 'NKT', '', '', 0, 1),
(1421, 96, 'North New Territories', 'NNO', '', '', 0, 1),
(1422, 96, 'Sai Kung New Territories', 'NSK', '', '', 0, 1),
(1423, 96, 'Sha Tin New Territories', 'NST', '', '', 0, 1),
(1424, 96, 'Tai Po New Territories', 'NTP', '', '', 0, 1),
(1425, 96, 'Tsuen Wan New Territories', 'NTW', '', '', 0, 1),
(1426, 96, 'Tuen Mun New Territories', 'NTM', '', '', 0, 1),
(1427, 96, 'Yuen Long New Territories', 'NYL', '', '', 0, 1),
(1467, 98, 'Austurland', 'AL', '', '', 0, 1),
(1468, 98, 'Hofuoborgarsvaeoi', 'HF', '', '', 0, 1),
(1469, 98, 'Norourland eystra', 'NE', '', '', 0, 1),
(1470, 98, 'Norourland vestra', 'NV', '', '', 0, 1),
(1471, 98, 'Suourland', 'SL', '', '', 0, 1),
(1472, 98, 'Suournes', 'SN', '', '', 0, 1),
(1473, 98, 'Vestfiroir', 'VF', '', '', 0, 1),
(1474, 98, 'Vesturland', 'VL', '', '', 0, 1),
(1475, 99, 'Andaman and Nicobar Islands', 'AN', '', '', 0, 1),
(1476, 99, 'Andhra Pradesh', 'AP', '', '', 0, 1),
(1477, 99, 'Arunachal Pradesh', 'AR', '', '', 0, 1),
(1478, 99, 'Assam', 'AS', '', '', 0, 1),
(1479, 99, 'Bihar', 'BI', '', '', 0, 1),
(1480, 99, 'Chandigarh', 'CH', '', '', 0, 1),
(1481, 99, 'Dadra and Nagar Haveli', 'DA', '', '', 0, 1),
(1482, 99, 'Daman and Diu', 'DM', '', '', 0, 1),
(1483, 99, 'Delhi', 'DE', '', '', 1, 1),
(1484, 99, 'Goa', 'GO', '', '', 0, 1),
(1485, 99, 'Gujarat', 'GU', '', '', 0, 1),
(1486, 99, 'Haryana', 'HA', '', '', 0, 1),
(1487, 99, 'Himachal Pradesh', 'HP', '', '', 0, 1),
(1488, 99, 'Jammu and Kashmir', 'JA', '', '', 0, 1),
(1489, 99, 'Karnataka', 'KA', '', '', 0, 1),
(1490, 99, 'Kerala', 'KE', '', '', 0, 1),
(1491, 99, 'Lakshadweep Islands', 'LI', '', '', 0, 1),
(1492, 99, 'Madhya Pradesh', 'MP', '', '', 0, 1),
(1493, 99, 'Maharashtra', 'MA', '', '', 0, 1),
(1494, 99, 'Manipur', 'MN', '', '', 0, 1),
(1495, 99, 'Meghalaya', 'ME', '', '', 0, 1),
(1496, 99, 'Mizoram', 'MI', '', '', 0, 1),
(1497, 99, 'Nagaland', 'NA', '', '', 0, 1),
(1498, 99, 'Orissa', 'OR', '', '', 0, 1),
(1499, 99, 'Pondicherry', 'PO', '', '', 0, 1),
(1500, 99, 'Punjab', 'PU', '', '', 0, 1),
(1501, 99, 'Rajasthan', 'RA', '', '', 0, 1),
(1502, 99, 'Sikkim', 'SI', '', '', 0, 1),
(1503, 99, 'Tamil Nadu', 'TN', '', '', 0, 1),
(1504, 99, 'Tripura', 'TR', '', '', 0, 1),
(1505, 99, 'Uttar Pradesh', 'UP', '', '', 0, 1),
(1506, 99, 'West Bengal', 'WB', '', '', 0, 1),
(1507, 100, 'Aceh', 'AC', '', '', 0, 1),
(1508, 100, 'Bali', 'BA', '', '', 0, 1),
(1509, 100, 'Banten', 'BT', '', '', 0, 1),
(1510, 100, 'Bengkulu', 'BE', '', '', 0, 1),
(1511, 100, 'BoDeTaBek', 'BD', '', '', 0, 1),
(1512, 100, 'Gorontalo', 'GO', '', '', 0, 1),
(1513, 100, 'Jakarta Raya', 'JK', '', '', 0, 1),
(1514, 100, 'Jambi', 'JA', '', '', 0, 1),
(1515, 100, 'Jawa Barat', 'JB', '', '', 0, 1),
(1516, 100, 'Jawa Tengah', 'JT', '', '', 0, 1),
(1517, 100, 'Jawa Timur', 'JI', '', '', 0, 1),
(1518, 100, 'Kalimantan Barat', 'KB', '', '', 0, 1),
(1519, 100, 'Kalimantan Selatan', 'KS', '', '', 0, 1),
(1520, 100, 'Kalimantan Tengah', 'KT', '', '', 0, 1),
(1521, 100, 'Kalimantan Timur', 'KI', '', '', 0, 1),
(1522, 100, 'Kepulauan Bangka Belitung', 'BB', '', '', 0, 1),
(1523, 100, 'Lampung', 'LA', '', '', 0, 1),
(1524, 100, 'Maluku', 'MA', '', '', 0, 1),
(1525, 100, 'Maluku Utara', 'MU', '', '', 0, 1),
(1526, 100, 'Nusa Tenggara Barat', 'NB', '', '', 0, 1),
(1527, 100, 'Nusa Tenggara Timur', 'NT', '', '', 0, 1),
(1528, 100, 'Papua', 'PA', '', '', 0, 1),
(1529, 100, 'Riau', 'RI', '', '', 0, 1),
(1530, 100, 'Sulawesi Selatan', 'SN', '', '', 0, 1),
(1531, 100, 'Sulawesi Tengah', 'ST', '', '', 0, 1),
(1532, 100, 'Sulawesi Tenggara', 'SG', '', '', 0, 1),
(1533, 100, 'Sulawesi Utara', 'SA', '', '', 0, 1),
(1534, 100, 'Sumatera Barat', 'SB', '', '', 0, 1),
(1535, 100, 'Sumatera Selatan', 'SS', '', '', 0, 1),
(1536, 100, 'Sumatera Utara', 'SU', '', '', 0, 1),
(1537, 100, 'Yogyakarta', 'YO', '', '', 0, 1),
(1538, 101, 'Tehran', 'TEH', '', '', 0, 1),
(1539, 101, 'Qom', 'QOM', '', '', 0, 1),
(1540, 101, 'Markazi', 'MKZ', '', '', 0, 1),
(1541, 101, 'Qazvin', 'QAZ', '', '', 0, 1),
(1542, 101, 'Gilan', 'GIL', '', '', 0, 1),
(1543, 101, 'Ardabil', 'ARD', '', '', 0, 1),
(1544, 101, 'Zanjan', 'ZAN', '', '', 0, 1),
(1545, 101, 'East Azarbaijan', 'EAZ', '', '', 0, 1),
(1546, 101, 'West Azarbaijan', 'WEZ', '', '', 0, 1),
(1547, 101, 'Kurdistan', 'KRD', '', '', 0, 1),
(1548, 101, 'Hamadan', 'HMD', '', '', 0, 1),
(1549, 101, 'Kermanshah', 'KRM', '', '', 0, 1),
(1550, 101, 'Ilam', 'ILM', '', '', 0, 1),
(1551, 101, 'Lorestan', 'LRS', '', '', 0, 1),
(1552, 101, 'Khuzestan', 'KZT', '', '', 0, 1),
(1553, 101, 'Chahar Mahaal and Bakhtiari', 'CMB', '', '', 0, 1),
(1554, 101, 'Kohkiluyeh and Buyer Ahmad', 'KBA', '', '', 0, 1),
(1555, 101, 'Bushehr', 'BSH', '', '', 0, 1),
(1556, 101, 'Fars', 'FAR', '', '', 0, 1),
(1557, 101, 'Hormozgan', 'HRM', '', '', 0, 1),
(1558, 101, 'Sistan and Baluchistan', 'SBL', '', '', 0, 1),
(1559, 101, 'Kerman', 'KRB', '', '', 0, 1),
(1560, 101, 'Yazd', 'YZD', '', '', 0, 1),
(1561, 101, 'Esfahan', 'EFH', '', '', 0, 1),
(1562, 101, 'Semnan', 'SMN', '', '', 0, 1),
(1563, 101, 'Mazandaran', 'MZD', '', '', 0, 1),
(1564, 101, 'Golestan', 'GLS', '', '', 0, 1),
(1565, 101, 'North Khorasan', 'NKH', '', '', 0, 1),
(1566, 101, 'Razavi Khorasan', 'RKH', '', '', 0, 1),
(1567, 101, 'South Khorasan', 'SKH', '', '', 0, 1),
(1568, 102, 'Baghdad', 'BD', '', '', 0, 1),
(1569, 102, 'Salah ad Din', 'SD', '', '', 0, 1),
(1570, 102, 'Diyala', 'DY', '', '', 0, 1),
(1571, 102, 'Wasit', 'WS', '', '', 0, 1),
(1572, 102, 'Maysan', 'MY', '', '', 0, 1),
(1573, 102, 'Al Basrah', 'BA', '', '', 0, 1),
(1574, 102, 'Dhi Qar', 'DQ', '', '', 0, 1),
(1575, 102, 'Al Muthanna', 'MU', '', '', 0, 1),
(1576, 102, 'Al Qadisyah', 'QA', '', '', 0, 1),
(1577, 102, 'Babil', 'BB', '', '', 0, 1),
(1578, 102, 'Al Karbala', 'KB', '', '', 0, 1),
(1579, 102, 'An Najaf', 'NJ', '', '', 0, 1),
(1580, 102, 'Al Anbar', 'AB', '', '', 0, 1),
(1581, 102, 'Ninawa', 'NN', '', '', 0, 1),
(1582, 102, 'Dahuk', 'DH', '', '', 0, 1),
(1583, 102, 'Arbil', 'AL', '', '', 0, 1),
(1584, 102, 'At Ta''mim', 'TM', '', '', 0, 1),
(1585, 102, 'As Sulaymaniyah', 'SL', '', '', 0, 1),
(1586, 103, 'Carlow', 'CA', '', '', 0, 1),
(1587, 103, 'Cavan', 'CV', '', '', 0, 1),
(1588, 103, 'Clare', 'CL', '', '', 0, 1),
(1589, 103, 'Cork', 'CO', '', '', 0, 1),
(1590, 103, 'Donegal', 'DO', '', '', 0, 1),
(1591, 103, 'Dublin', 'DU', '', '', 0, 1),
(1592, 103, 'Galway', 'GA', '', '', 0, 1),
(1593, 103, 'Kerry', 'KE', '', '', 0, 1),
(1594, 103, 'Kildare', 'KI', '', '', 0, 1),
(1595, 103, 'Kilkenny', 'KL', '', '', 0, 1),
(1596, 103, 'Laois', 'LA', '', '', 0, 1),
(1597, 103, 'Leitrim', 'LE', '', '', 0, 1),
(1598, 103, 'Limerick', 'LI', '', '', 0, 1),
(1599, 103, 'Longford', 'LO', '', '', 0, 1),
(1600, 103, 'Louth', 'LU', '', '', 0, 1),
(1601, 103, 'Mayo', 'MA', '', '', 0, 1),
(1602, 103, 'Meath', 'ME', '', '', 0, 1),
(1603, 103, 'Monaghan', 'MO', '', '', 0, 1),
(1604, 103, 'Offaly', 'OF', '', '', 0, 1),
(1605, 103, 'Roscommon', 'RO', '', '', 0, 1),
(1606, 103, 'Sligo', 'SL', '', '', 0, 1),
(1607, 103, 'Tipperary', 'TI', '', '', 0, 1),
(1608, 103, 'Waterford', 'WA', '', '', 0, 1),
(1609, 103, 'Westmeath', 'WE', '', '', 0, 1),
(1610, 103, 'Wexford', 'WX', '', '', 0, 1),
(1611, 103, 'Wicklow', 'WI', '', '', 0, 1),
(1612, 104, 'Be''er Sheva', 'BS', '', '', 0, 1),
(1613, 104, 'Bika''at Hayarden', 'BH', '', '', 0, 1),
(1614, 104, 'Eilat and Arava', 'EA', '', '', 0, 1),
(1615, 104, 'Galil', 'GA', '', '', 0, 1),
(1616, 104, 'Haifa', 'HA', '', '', 0, 1),
(1617, 104, 'Jehuda Mountains', 'JM', '', '', 0, 1),
(1618, 104, 'Jerusalem', 'JE', '', '', 0, 1),
(1619, 104, 'Negev', 'NE', '', '', 0, 1),
(1620, 104, 'Semaria', 'SE', '', '', 0, 1),
(1621, 104, 'Sharon', 'SH', '', '', 0, 1),
(1622, 104, 'Tel Aviv (Gosh Dan)', 'TA', '', '', 0, 1),
(3860, 105, 'Caltanissetta', 'CL', '', '', 0, 1),
(3842, 105, 'Agrigento', 'AG', '', '', 0, 1),
(3843, 105, 'Alessandria', 'AL', '', '', 0, 1),
(3844, 105, 'Ancona', 'AN', '', '', 0, 1),
(3845, 105, 'Aosta', 'AO', '', '', 0, 1),
(3846, 105, 'Arezzo', 'AR', '', '', 0, 1),
(3847, 105, 'Ascoli Piceno', 'AP', '', '', 0, 1),
(3848, 105, 'Asti', 'AT', '', '', 0, 1),
(3849, 105, 'Avellino', 'AV', '', '', 0, 1),
(3850, 105, 'Bari', 'BA', '', '', 0, 1),
(3851, 105, 'Belluno', 'BL', '', '', 0, 1),
(3852, 105, 'Benevento', 'BN', '', '', 0, 1),
(3853, 105, 'Bergamo', 'BG', '', '', 0, 1),
(3854, 105, 'Biella', 'BI', '', '', 0, 1),
(3855, 105, 'Bologna', 'BO', '', '', 0, 1),
(3856, 105, 'Bolzano', 'BZ', '', '', 0, 1),
(3857, 105, 'Brescia', 'BS', '', '', 0, 1),
(3858, 105, 'Brindisi', 'BR', '', '', 0, 1),
(3859, 105, 'Cagliari', 'CA', '', '', 0, 1),
(1643, 106, 'Clarendon Parish', 'CLA', '', '', 0, 1),
(1644, 106, 'Hanover Parish', 'HAN', '', '', 0, 1),
(1645, 106, 'Kingston Parish', 'KIN', '', '', 0, 1),
(1646, 106, 'Manchester Parish', 'MAN', '', '', 0, 1),
(1647, 106, 'Portland Parish', 'POR', '', '', 0, 1),
(1648, 106, 'Saint Andrew Parish', 'AND', '', '', 0, 1),
(1649, 106, 'Saint Ann Parish', 'ANN', '', '', 0, 1),
(1650, 106, 'Saint Catherine Parish', 'CAT', '', '', 0, 1),
(1651, 106, 'Saint Elizabeth Parish', 'ELI', '', '', 0, 1),
(1652, 106, 'Saint James Parish', 'JAM', '', '', 0, 1),
(1653, 106, 'Saint Mary Parish', 'MAR', '', '', 0, 1),
(1654, 106, 'Saint Thomas Parish', 'THO', '', '', 0, 1),
(1655, 106, 'Trelawny Parish', 'TRL', '', '', 0, 1),
(1656, 106, 'Westmoreland Parish', 'WML', '', '', 0, 1),
(1657, 107, 'Aichi', 'AI', '', '', 0, 1),
(1658, 107, 'Akita', 'AK', '', '', 0, 1),
(1659, 107, 'Aomori', 'AO', '', '', 0, 1),
(1660, 107, 'Chiba', 'CH', '', '', 0, 1),
(1661, 107, 'Ehime', 'EH', '', '', 0, 1),
(1662, 107, 'Fukui', 'FK', '', '', 0, 1),
(1663, 107, 'Fukuoka', 'FU', '', '', 0, 1),
(1664, 107, 'Fukushima', 'FS', '', '', 0, 1),
(1665, 107, 'Gifu', 'GI', '', '', 0, 1),
(1666, 107, 'Gumma', 'GU', '', '', 0, 1),
(1667, 107, 'Hiroshima', 'HI', '', '', 0, 1),
(1668, 107, 'Hokkaido', 'HO', '', '', 0, 1),
(1669, 107, 'Hyogo', 'HY', '', '', 0, 1),
(1670, 107, 'Ibaraki', 'IB', '', '', 0, 1),
(1671, 107, 'Ishikawa', 'IS', '', '', 0, 1),
(1672, 107, 'Iwate', 'IW', '', '', 0, 1),
(1673, 107, 'Kagawa', 'KA', '', '', 0, 1),
(1674, 107, 'Kagoshima', 'KG', '', '', 0, 1),
(1675, 107, 'Kanagawa', 'KN', '', '', 0, 1),
(1676, 107, 'Kochi', 'KO', '', '', 0, 1),
(1677, 107, 'Kumamoto', 'KU', '', '', 0, 1),
(1678, 107, 'Kyoto', 'KY', '', '', 0, 1),
(1679, 107, 'Mie', 'MI', '', '', 0, 1),
(1680, 107, 'Miyagi', 'MY', '', '', 0, 1),
(1681, 107, 'Miyazaki', 'MZ', '', '', 0, 1),
(1682, 107, 'Nagano', 'NA', '', '', 0, 1),
(1683, 107, 'Nagasaki', 'NG', '', '', 0, 1),
(1684, 107, 'Nara', 'NR', '', '', 0, 1),
(1685, 107, 'Niigata', 'NI', '', '', 0, 1),
(1686, 107, 'Oita', 'OI', '', '', 0, 1),
(1687, 107, 'Okayama', 'OK', '', '', 0, 1),
(1688, 107, 'Okinawa', 'ON', '', '', 0, 1),
(1689, 107, 'Osaka', 'OS', '', '', 0, 1),
(1690, 107, 'Saga', 'SA', '', '', 0, 1),
(1691, 107, 'Saitama', 'SI', '', '', 0, 1),
(1692, 107, 'Shiga', 'SH', '', '', 0, 1),
(1693, 107, 'Shimane', 'SM', '', '', 0, 1),
(1694, 107, 'Shizuoka', 'SZ', '', '', 0, 1),
(1695, 107, 'Tochigi', 'TO', '', '', 0, 1),
(1696, 107, 'Tokushima', 'TS', '', '', 0, 1),
(1697, 107, 'Tokyo', 'TK', '', '', 0, 1),
(1698, 107, 'Tottori', 'TT', '', '', 0, 1),
(1699, 107, 'Toyama', 'TY', '', '', 0, 1),
(1700, 107, 'Wakayama', 'WA', '', '', 0, 1),
(1701, 107, 'Yamagata', 'YA', '', '', 0, 1),
(1702, 107, 'Yamaguchi', 'YM', '', '', 0, 1),
(1703, 107, 'Yamanashi', 'YN', '', '', 0, 1),
(1704, 108, 'Amman', 'AM', '', '', 0, 1),
(1705, 108, 'Ajlun', 'AJ', '', '', 0, 1),
(1706, 108, 'Al ''Aqabah', 'AA', '', '', 0, 1),
(1707, 108, 'Al Balqa''', 'AB', '', '', 0, 1),
(1708, 108, 'Al Karak', 'AK', '', '', 0, 1),
(1709, 108, 'Al Mafraq', 'AL', '', '', 0, 1),
(1710, 108, 'At Tafilah', 'AT', '', '', 0, 1),
(1711, 108, 'Az Zarqa''', 'AZ', '', '', 0, 1),
(1712, 108, 'Irbid', 'IR', '', '', 0, 1),
(1713, 108, 'Jarash', 'JA', '', '', 0, 1),
(1714, 108, 'Ma''an', 'MA', '', '', 0, 1),
(1715, 108, 'Madaba', 'MD', '', '', 0, 1),
(1716, 109, 'Almaty', 'AL', '', '', 0, 1),
(1717, 109, 'Almaty City', 'AC', '', '', 0, 1),
(1718, 109, 'Aqmola', 'AM', '', '', 0, 1),
(1719, 109, 'Aqtobe', 'AQ', '', '', 0, 1),
(1720, 109, 'Astana City', 'AS', '', '', 0, 1),
(1721, 109, 'Atyrau', 'AT', '', '', 0, 1),
(1722, 109, 'Batys Qazaqstan', 'BA', '', '', 0, 1),
(1723, 109, 'Bayqongyr City', 'BY', '', '', 0, 1),
(1724, 109, 'Mangghystau', 'MA', '', '', 0, 1),
(1725, 109, 'Ongtustik Qazaqstan', 'ON', '', '', 0, 1),
(1726, 109, 'Pavlodar', 'PA', '', '', 0, 1),
(1727, 109, 'Qaraghandy', 'QA', '', '', 0, 1),
(1728, 109, 'Qostanay', 'QO', '', '', 0, 1),
(1729, 109, 'Qyzylorda', 'QY', '', '', 0, 1),
(1730, 109, 'Shyghys Qazaqstan', 'SH', '', '', 0, 1),
(1731, 109, 'Soltustik Qazaqstan', 'SO', '', '', 0, 1),
(1732, 109, 'Zhambyl', 'ZH', '', '', 0, 1),
(1733, 110, 'Central', 'CE', '', '', 0, 1),
(1734, 110, 'Coast', 'CO', '', '', 0, 1),
(1735, 110, 'Eastern', 'EA', '', '', 0, 1),
(1736, 110, 'Nairobi Area', 'NA', '', '', 0, 1),
(1737, 110, 'North Eastern', 'NE', '', '', 0, 1),
(1738, 110, 'Nyanza', 'NY', '', '', 0, 1),
(1739, 110, 'Rift Valley', 'RV', '', '', 0, 1),
(1740, 110, 'Western', 'WE', '', '', 0, 1),
(1741, 111, 'Abaiang', 'AG', '', '', 0, 1),
(1742, 111, 'Abemama', 'AM', '', '', 0, 1),
(1743, 111, 'Aranuka', 'AK', '', '', 0, 1),
(1744, 111, 'Arorae', 'AO', '', '', 0, 1),
(1745, 111, 'Banaba', 'BA', '', '', 0, 1),
(1746, 111, 'Beru', 'BE', '', '', 0, 1),
(1747, 111, 'Butaritari', 'bT', '', '', 0, 1),
(1748, 111, 'Kanton', 'KA', '', '', 0, 1),
(1749, 111, 'Kiritimati', 'KR', '', '', 0, 1),
(1750, 111, 'Kuria', 'KU', '', '', 0, 1),
(1751, 111, 'Maiana', 'MI', '', '', 0, 1),
(1752, 111, 'Makin', 'MN', '', '', 0, 1),
(1753, 111, 'Marakei', 'ME', '', '', 0, 1),
(1754, 111, 'Nikunau', 'NI', '', '', 0, 1),
(1755, 111, 'Nonouti', 'NO', '', '', 0, 1),
(1756, 111, 'Onotoa', 'ON', '', '', 0, 1),
(1757, 111, 'Tabiteuea', 'TT', '', '', 0, 1),
(1758, 111, 'Tabuaeran', 'TR', '', '', 0, 1),
(1759, 111, 'Tamana', 'TM', '', '', 0, 1),
(1760, 111, 'Tarawa', 'TW', '', '', 0, 1),
(1761, 111, 'Teraina', 'TE', '', '', 0, 1),
(1762, 112, 'Chagang-do', 'CHA', '', '', 0, 1),
(1763, 112, 'Hamgyong-bukto', 'HAB', '', '', 0, 1),
(1764, 112, 'Hamgyong-namdo', 'HAN', '', '', 0, 1),
(1765, 112, 'Hwanghae-bukto', 'HWB', '', '', 0, 1),
(1766, 112, 'Hwanghae-namdo', 'HWN', '', '', 0, 1),
(1767, 112, 'Kangwon-do', 'KAN', '', '', 0, 1),
(1768, 112, 'P''yongan-bukto', 'PYB', '', '', 0, 1),
(1769, 112, 'P''yongan-namdo', 'PYN', '', '', 0, 1),
(1770, 112, 'Ryanggang-do (Yanggang-do)', 'YAN', '', '', 0, 1),
(1771, 112, 'Rason Directly Governed City', 'NAJ', '', '', 0, 1),
(1772, 112, 'P''yongyang Special City', 'PYO', '', '', 0, 1),
(1773, 113, 'Ch''ungch''ong-bukto', 'CO', '', '', 0, 1),
(1774, 113, 'Ch''ungch''ong-namdo', 'CH', '', '', 0, 1),
(1775, 113, 'Cheju-do', 'CD', '', '', 0, 1),
(1776, 113, 'Cholla-bukto', 'CB', '', '', 0, 1),
(1777, 113, 'Cholla-namdo', 'CN', '', '', 0, 1),
(1778, 113, 'Inch''on-gwangyoksi', 'IG', '', '', 0, 1),
(1779, 113, 'Kangwon-do', 'KA', '', '', 0, 1),
(1780, 113, 'Kwangju-gwangyoksi', 'KG', '', '', 0, 1),
(1781, 113, 'Kyonggi-do', 'KD', '', '', 0, 1),
(1782, 113, 'Kyongsang-bukto', 'KB', '', '', 0, 1),
(1783, 113, 'Kyongsang-namdo', 'KN', '', '', 0, 1),
(1784, 113, 'Pusan-gwangyoksi', 'PG', '', '', 0, 1),
(1785, 113, 'Soul-t''ukpyolsi', 'SO', '', '', 0, 1),
(1786, 113, 'Taegu-gwangyoksi', 'TA', '', '', 0, 1),
(1787, 113, 'Taejon-gwangyoksi', 'TG', '', '', 0, 1),
(1788, 114, 'Al ''Asimah', 'AL', '', '', 0, 1),
(1789, 114, 'Al Ahmadi', 'AA', '', '', 0, 1),
(1790, 114, 'Al Farwaniyah', 'AF', '', '', 0, 1),
(1791, 114, 'Al Jahra''', 'AJ', '', '', 0, 1),
(1792, 114, 'Hawalli', 'HA', '', '', 0, 1),
(1793, 115, 'Bishkek', 'GB', '', '', 0, 1),
(1794, 115, 'Batken', 'B', '', '', 0, 1),
(1795, 115, 'Chu', 'C', '', '', 0, 1),
(1796, 115, 'Jalal-Abad', 'J', '', '', 0, 1),
(1797, 115, 'Naryn', 'N', '', '', 0, 1),
(1798, 115, 'Osh', 'O', '', '', 0, 1),
(1799, 115, 'Talas', 'T', '', '', 0, 1),
(1800, 115, 'Ysyk-Kol', 'Y', '', '', 0, 1),
(1801, 116, 'Vientiane', 'VT', '', '', 0, 1),
(1802, 116, 'Attapu', 'AT', '', '', 0, 1),
(1803, 116, 'Bokeo', 'BK', '', '', 0, 1),
(1804, 116, 'Bolikhamxai', 'BL', '', '', 0, 1),
(1805, 116, 'Champasak', 'CH', '', '', 0, 1),
(1806, 116, 'Houaphan', 'HO', '', '', 0, 1),
(1807, 116, 'Khammouan', 'KH', '', '', 0, 1),
(1808, 116, 'Louang Namtha', 'LM', '', '', 0, 1),
(1809, 116, 'Louangphabang', 'LP', '', '', 0, 1),
(1810, 116, 'Oudomxai', 'OU', '', '', 0, 1),
(1811, 116, 'Phongsali', 'PH', '', '', 0, 1),
(1812, 116, 'Salavan', 'SL', '', '', 0, 1),
(1813, 116, 'Savannakhet', 'SV', '', '', 0, 1),
(1814, 116, 'Vientiane', 'VI', '', '', 0, 1),
(1815, 116, 'Xaignabouli', 'XA', '', '', 0, 1),
(1816, 116, 'Xekong', 'XE', '', '', 0, 1),
(1817, 116, 'Xiangkhoang', 'XI', '', '', 0, 1),
(1818, 116, 'Xaisomboun', 'XN', '', '', 0, 1),
(1852, 119, 'Berea', 'BE', '', '', 0, 1),
(1853, 119, 'Butha-Buthe', 'BB', '', '', 0, 1),
(1854, 119, 'Leribe', 'LE', '', '', 0, 1),
(1855, 119, 'Mafeteng', 'MF', '', '', 0, 1),
(1856, 119, 'Maseru', 'MS', '', '', 0, 1),
(1857, 119, 'Mohale''s Hoek', 'MH', '', '', 0, 1),
(1858, 119, 'Mokhotlong', 'MK', '', '', 0, 1),
(1859, 119, 'Qacha''s Nek', 'QN', '', '', 0, 1),
(1860, 119, 'Quthing', 'QT', '', '', 0, 1),
(1861, 119, 'Thaba-Tseka', 'TT', '', '', 0, 1),
(1862, 120, 'Bomi', 'BI', '', '', 0, 1),
(1863, 120, 'Bong', 'BG', '', '', 0, 1),
(1864, 120, 'Grand Bassa', 'GB', '', '', 0, 1),
(1865, 120, 'Grand Cape Mount', 'CM', '', '', 0, 1),
(1866, 120, 'Grand Gedeh', 'GG', '', '', 0, 1),
(1867, 120, 'Grand Kru', 'GK', '', '', 0, 1),
(1868, 120, 'Lofa', 'LO', '', '', 0, 1),
(1869, 120, 'Margibi', 'MG', '', '', 0, 1),
(1870, 120, 'Maryland', 'ML', '', '', 0, 1),
(1871, 120, 'Montserrado', 'MS', '', '', 0, 1),
(1872, 120, 'Nimba', 'NB', '', '', 0, 1),
(1873, 120, 'River Cess', 'RC', '', '', 0, 1),
(1874, 120, 'Sinoe', 'SN', '', '', 0, 1),
(1875, 121, 'Ajdabiya', 'AJ', '', '', 0, 1),
(1876, 121, 'Al ''Aziziyah', 'AZ', '', '', 0, 1),
(1877, 121, 'Al Fatih', 'FA', '', '', 0, 1),
(1878, 121, 'Al Jabal al Akhdar', 'JA', '', '', 0, 1),
(1879, 121, 'Al Jufrah', 'JU', '', '', 0, 1),
(1880, 121, 'Al Khums', 'KH', '', '', 0, 1),
(1881, 121, 'Al Kufrah', 'KU', '', '', 0, 1),
(1882, 121, 'An Nuqat al Khams', 'NK', '', '', 0, 1),
(1883, 121, 'Ash Shati''', 'AS', '', '', 0, 1),
(1884, 121, 'Awbari', 'AW', '', '', 0, 1),
(1885, 121, 'Az Zawiyah', 'ZA', '', '', 0, 1),
(1886, 121, 'Banghazi', 'BA', '', '', 0, 1),
(1887, 121, 'Darnah', 'DA', '', '', 0, 1),
(1888, 121, 'Ghadamis', 'GD', '', '', 0, 1),
(1889, 121, 'Gharyan', 'GY', '', '', 0, 1),
(1890, 121, 'Misratah', 'MI', '', '', 0, 1),
(1891, 121, 'Murzuq', 'MZ', '', '', 0, 1),
(1892, 121, 'Sabha', 'SB', '', '', 0, 1),
(1893, 121, 'Sawfajjin', 'SW', '', '', 0, 1),
(1894, 121, 'Surt', 'SU', '', '', 0, 1),
(1895, 121, 'Tarabulus (Tripoli)', 'TL', '', '', 0, 1),
(1896, 121, 'Tarhunah', 'TH', '', '', 0, 1),
(1897, 121, 'Tubruq', 'TU', '', '', 0, 1),
(1898, 121, 'Yafran', 'YA', '', '', 0, 1),
(1899, 121, 'Zlitan', 'ZL', '', '', 0, 1),
(1900, 122, 'Vaduz', 'V', '', '', 0, 1),
(1901, 122, 'Schaan', 'A', '', '', 0, 1),
(1902, 122, 'Balzers', 'B', '', '', 0, 1),
(1903, 122, 'Triesen', 'N', '', '', 0, 1),
(1904, 122, 'Eschen', 'E', '', '', 0, 1),
(1905, 122, 'Mauren', 'M', '', '', 0, 1),
(1906, 122, 'Triesenberg', 'T', '', '', 0, 1),
(1907, 122, 'Ruggell', 'R', '', '', 0, 1),
(1908, 122, 'Gamprin', 'G', '', '', 0, 1),
(1909, 122, 'Schellenberg', 'L', '', '', 0, 1),
(1910, 122, 'Planken', 'P', '', '', 0, 1),
(1911, 123, 'Alytus', 'AL', '', '', 0, 1),
(1912, 123, 'Kaunas', 'KA', '', '', 0, 1),
(1913, 123, 'Klaipeda', 'KL', '', '', 0, 1),
(1914, 123, 'Marijampole', 'MA', '', '', 0, 1),
(1915, 123, 'Panevezys', 'PA', '', '', 0, 1),
(1916, 123, 'Siauliai', 'SI', '', '', 0, 1),
(1917, 123, 'Taurage', 'TA', '', '', 0, 1),
(1918, 123, 'Telsiai', 'TE', '', '', 0, 1),
(1919, 123, 'Utena', 'UT', '', '', 0, 1),
(1920, 123, 'Vilnius', 'VI', '', '', 0, 1),
(1921, 124, 'Diekirch', 'DD', '', '', 0, 1),
(1922, 124, 'Clervaux', 'DC', '', '', 0, 1),
(1923, 124, 'Redange', 'DR', '', '', 0, 1),
(1924, 124, 'Vianden', 'DV', '', '', 0, 1),
(1925, 124, 'Wiltz', 'DW', '', '', 0, 1),
(1926, 124, 'Grevenmacher', 'GG', '', '', 0, 1),
(1927, 124, 'Echternach', 'GE', '', '', 0, 1),
(1928, 124, 'Remich', 'GR', '', '', 0, 1),
(1929, 124, 'Luxembourg', 'LL', '', '', 0, 1),
(1930, 124, 'Capellen', 'LC', '', '', 0, 1),
(1931, 124, 'Esch-sur-Alzette', 'LE', '', '', 0, 1),
(1932, 124, 'Mersch', 'LM', '', '', 0, 1),
(1933, 125, 'Our Lady Fatima Parish', 'OLF', '', '', 0, 1),
(1934, 125, 'St. Anthony Parish', 'ANT', '', '', 0, 1),
(1935, 125, 'St. Lazarus Parish', 'LAZ', '', '', 0, 1),
(1936, 125, 'Cathedral Parish', 'CAT', '', '', 0, 1),
(1937, 125, 'St. Lawrence Parish', 'LAW', '', '', 0, 1),
(1938, 127, 'Antananarivo', 'AN', '', '', 0, 1),
(1939, 127, 'Antsiranana', 'AS', '', '', 0, 1),
(1940, 127, 'Fianarantsoa', 'FN', '', '', 0, 1),
(1941, 127, 'Mahajanga', 'MJ', '', '', 0, 1),
(1942, 127, 'Toamasina', 'TM', '', '', 0, 1),
(1943, 127, 'Toliara', 'TL', '', '', 0, 1),
(1944, 128, 'Balaka', 'BLK', '', '', 0, 1),
(1945, 128, 'Blantyre', 'BLT', '', '', 0, 1),
(1946, 128, 'Chikwawa', 'CKW', '', '', 0, 1),
(1947, 128, 'Chiradzulu', 'CRD', '', '', 0, 1),
(1948, 128, 'Chitipa', 'CTP', '', '', 0, 1),
(1949, 128, 'Dedza', 'DDZ', '', '', 0, 1),
(1950, 128, 'Dowa', 'DWA', '', '', 0, 1),
(1951, 128, 'Karonga', 'KRG', '', '', 0, 1),
(1952, 128, 'Kasungu', 'KSG', '', '', 0, 1),
(1953, 128, 'Likoma', 'LKM', '', '', 0, 1),
(1954, 128, 'Lilongwe', 'LLG', '', '', 0, 1),
(1955, 128, 'Machinga', 'MCG', '', '', 0, 1),
(1956, 128, 'Mangochi', 'MGC', '', '', 0, 1),
(1957, 128, 'Mchinji', 'MCH', '', '', 0, 1),
(1958, 128, 'Mulanje', 'MLJ', '', '', 0, 1),
(1959, 128, 'Mwanza', 'MWZ', '', '', 0, 1),
(1960, 128, 'Mzimba', 'MZM', '', '', 0, 1),
(1961, 128, 'Ntcheu', 'NTU', '', '', 0, 1),
(1962, 128, 'Nkhata Bay', 'NKB', '', '', 0, 1),
(1963, 128, 'Nkhotakota', 'NKH', '', '', 0, 1),
(1964, 128, 'Nsanje', 'NSJ', '', '', 0, 1),
(1965, 128, 'Ntchisi', 'NTI', '', '', 0, 1),
(1966, 128, 'Phalombe', 'PHL', '', '', 0, 1),
(1967, 128, 'Rumphi', 'RMP', '', '', 0, 1),
(1968, 128, 'Salima', 'SLM', '', '', 0, 1),
(1969, 128, 'Thyolo', 'THY', '', '', 0, 1),
(1970, 128, 'Zomba', 'ZBA', '', '', 0, 1),
(1971, 129, 'Johor', 'MY-01', '', '', 0, 1),
(1972, 129, 'Kedah', 'MY-02', '', '', 0, 1),
(1973, 129, 'Kelantan', 'MY-03', '', '', 0, 1),
(1974, 129, 'Labuan', 'MY-15', '', '', 0, 1),
(1975, 129, 'Melaka', 'MY-04', '', '', 0, 1),
(1976, 129, 'Negeri Sembilan', 'MY-05', '', '', 0, 1),
(1977, 129, 'Pahang', 'MY-06', '', '', 0, 1),
(1978, 129, 'Perak', 'MY-08', '', '', 0, 1),
(1979, 129, 'Perlis', 'MY-09', '', '', 0, 1),
(1980, 129, 'Pulau Pinang', 'MY-07', '', '', 0, 1),
(1981, 129, 'Sabah', 'MY-12', '', '', 0, 1),
(1982, 129, 'Sarawak', 'MY-13', '', '', 0, 1),
(1983, 129, 'Selangor', 'MY-10', '', '', 0, 1),
(1984, 129, 'Terengganu', 'MY-11', '', '', 0, 1),
(1985, 129, 'Kuala Lumpur', 'MY-14', '', '', 0, 1),
(4035, 129, 'Putrajaya', 'MY-16', '', '', 0, 1),
(1986, 130, 'Thiladhunmathi Uthuru', 'THU', '', '', 0, 1),
(1987, 130, 'Thiladhunmathi Dhekunu', 'THD', '', '', 0, 1),
(1988, 130, 'Miladhunmadulu Uthuru', 'MLU', '', '', 0, 1),
(1989, 130, 'Miladhunmadulu Dhekunu', 'MLD', '', '', 0, 1),
(1990, 130, 'Maalhosmadulu Uthuru', 'MAU', '', '', 0, 1),
(1991, 130, 'Maalhosmadulu Dhekunu', 'MAD', '', '', 0, 1),
(1992, 130, 'Faadhippolhu', 'FAA', '', '', 0, 1),
(1993, 130, 'Male Atoll', 'MAA', '', '', 0, 1),
(1994, 130, 'Ari Atoll Uthuru', 'AAU', '', '', 0, 1),
(1995, 130, 'Ari Atoll Dheknu', 'AAD', '', '', 0, 1),
(1996, 130, 'Felidhe Atoll', 'FEA', '', '', 0, 1),
(1997, 130, 'Mulaku Atoll', 'MUA', '', '', 0, 1),
(1998, 130, 'Nilandhe Atoll Uthuru', 'NAU', '', '', 0, 1),
(1999, 130, 'Nilandhe Atoll Dhekunu', 'NAD', '', '', 0, 1),
(2000, 130, 'Kolhumadulu', 'KLH', '', '', 0, 1),
(2001, 130, 'Hadhdhunmathi', 'HDH', '', '', 0, 1),
(2002, 130, 'Huvadhu Atoll Uthuru', 'HAU', '', '', 0, 1),
(2003, 130, 'Huvadhu Atoll Dhekunu', 'HAD', '', '', 0, 1),
(2004, 130, 'Fua Mulaku', 'FMU', '', '', 0, 1),
(2005, 130, 'Addu', 'ADD', '', '', 0, 1),
(2006, 131, 'Gao', 'GA', '', '', 0, 1),
(2007, 131, 'Kayes', 'KY', '', '', 0, 1),
(2008, 131, 'Kidal', 'KD', '', '', 0, 1),
(2009, 131, 'Koulikoro', 'KL', '', '', 0, 1),
(2010, 131, 'Mopti', 'MP', '', '', 0, 1),
(2011, 131, 'Segou', 'SG', '', '', 0, 1),
(2012, 131, 'Sikasso', 'SK', '', '', 0, 1),
(2013, 131, 'Tombouctou', 'TB', '', '', 0, 1),
(2014, 131, 'Bamako Capital District', 'CD', '', '', 0, 1),
(2015, 132, 'Attard', 'ATT', '', '', 0, 1),
(2016, 132, 'Balzan', 'BAL', '', '', 0, 1),
(2017, 132, 'Birgu', 'BGU', '', '', 0, 1),
(2018, 132, 'Birkirkara', 'BKK', '', '', 0, 1),
(2019, 132, 'Birzebbuga', 'BRZ', '', '', 0, 1),
(2020, 132, 'Bormla', 'BOR', '', '', 0, 1),
(2021, 132, 'Dingli', 'DIN', '', '', 0, 1),
(2022, 132, 'Fgura', 'FGU', '', '', 0, 1),
(2023, 132, 'Floriana', 'FLO', '', '', 0, 1),
(2024, 132, 'Gudja', 'GDJ', '', '', 0, 1),
(2025, 132, 'Gzira', 'GZR', '', '', 0, 1),
(2026, 132, 'Gargur', 'GRG', '', '', 0, 1),
(2027, 132, 'Gaxaq', 'GXQ', '', '', 0, 1),
(2028, 132, 'Hamrun', 'HMR', '', '', 0, 1),
(2029, 132, 'Iklin', 'IKL', '', '', 0, 1),
(2030, 132, 'Isla', 'ISL', '', '', 0, 1),
(2031, 132, 'Kalkara', 'KLK', '', '', 0, 1),
(2032, 132, 'Kirkop', 'KRK', '', '', 0, 1),
(2033, 132, 'Lija', 'LIJ', '', '', 0, 1),
(2034, 132, 'Luqa', 'LUQ', '', '', 0, 1),
(2035, 132, 'Marsa', 'MRS', '', '', 0, 1),
(2036, 132, 'Marsaskala', 'MKL', '', '', 0, 1),
(2037, 132, 'Marsaxlokk', 'MXL', '', '', 0, 1),
(2038, 132, 'Mdina', 'MDN', '', '', 0, 1),
(2039, 132, 'Melliea', 'MEL', '', '', 0, 1),
(2040, 132, 'Mgarr', 'MGR', '', '', 0, 1),
(2041, 132, 'Mosta', 'MST', '', '', 0, 1),
(2042, 132, 'Mqabba', 'MQA', '', '', 0, 1),
(2043, 132, 'Msida', 'MSI', '', '', 0, 1),
(2044, 132, 'Mtarfa', 'MTF', '', '', 0, 1),
(2045, 132, 'Naxxar', 'NAX', '', '', 0, 1),
(2046, 132, 'Paola', 'PAO', '', '', 0, 1),
(2047, 132, 'Pembroke', 'PEM', '', '', 0, 1),
(2048, 132, 'Pieta', 'PIE', '', '', 0, 1),
(2049, 132, 'Qormi', 'QOR', '', '', 0, 1),
(2050, 132, 'Qrendi', 'QRE', '', '', 0, 1),
(2051, 132, 'Rabat', 'RAB', '', '', 0, 1),
(2052, 132, 'Safi', 'SAF', '', '', 0, 1),
(2053, 132, 'San Giljan', 'SGI', '', '', 0, 1),
(2054, 132, 'Santa Lucija', 'SLU', '', '', 0, 1),
(2055, 132, 'San Pawl il-Bahar', 'SPB', '', '', 0, 1),
(2056, 132, 'San Gwann', 'SGW', '', '', 0, 1),
(2057, 132, 'Santa Venera', 'SVE', '', '', 0, 1),
(2058, 132, 'Siggiewi', 'SIG', '', '', 0, 1),
(2059, 132, 'Sliema', 'SLM', '', '', 0, 1),
(2060, 132, 'Swieqi', 'SWQ', '', '', 0, 1),
(2061, 132, 'Ta Xbiex', 'TXB', '', '', 0, 1),
(2062, 132, 'Tarxien', 'TRX', '', '', 0, 1),
(2063, 132, 'Valletta', 'VLT', '', '', 0, 1),
(2064, 132, 'Xgajra', 'XGJ', '', '', 0, 1),
(2065, 132, 'Zabbar', 'ZBR', '', '', 0, 1),
(2066, 132, 'Zebbug', 'ZBG', '', '', 0, 1),
(2067, 132, 'Zejtun', 'ZJT', '', '', 0, 1),
(2068, 132, 'Zurrieq', 'ZRQ', '', '', 0, 1),
(2069, 132, 'Fontana', 'FNT', '', '', 0, 1),
(2070, 132, 'Ghajnsielem', 'GHJ', '', '', 0, 1),
(2071, 132, 'Gharb', 'GHR', '', '', 0, 1),
(2072, 132, 'Ghasri', 'GHS', '', '', 0, 1),
(2073, 132, 'Kercem', 'KRC', '', '', 0, 1),
(2074, 132, 'Munxar', 'MUN', '', '', 0, 1),
(2075, 132, 'Nadur', 'NAD', '', '', 0, 1),
(2076, 132, 'Qala', 'QAL', '', '', 0, 1),
(2077, 132, 'Victoria', 'VIC', '', '', 0, 1),
(2078, 132, 'San Lawrenz', 'SLA', '', '', 0, 1),
(2079, 132, 'Sannat', 'SNT', '', '', 0, 1),
(2080, 132, 'Xagra', 'ZAG', '', '', 0, 1),
(2081, 132, 'Xewkija', 'XEW', '', '', 0, 1),
(2082, 132, 'Zebbug', 'ZEB', '', '', 0, 1),
(2083, 133, 'Ailinginae', 'ALG', '', '', 0, 1),
(2084, 133, 'Ailinglaplap', 'ALL', '', '', 0, 1),
(2085, 133, 'Ailuk', 'ALK', '', '', 0, 1),
(2086, 133, 'Arno', 'ARN', '', '', 0, 1),
(2087, 133, 'Aur', 'AUR', '', '', 0, 1),
(2088, 133, 'Bikar', 'BKR', '', '', 0, 1),
(2089, 133, 'Bikini', 'BKN', '', '', 0, 1),
(2090, 133, 'Bokak', 'BKK', '', '', 0, 1),
(2091, 133, 'Ebon', 'EBN', '', '', 0, 1),
(2092, 133, 'Enewetak', 'ENT', '', '', 0, 1),
(2093, 133, 'Erikub', 'EKB', '', '', 0, 1),
(2094, 133, 'Jabat', 'JBT', '', '', 0, 1),
(2095, 133, 'Jaluit', 'JLT', '', '', 0, 1),
(2096, 133, 'Jemo', 'JEM', '', '', 0, 1),
(2097, 133, 'Kili', 'KIL', '', '', 0, 1),
(2098, 133, 'Kwajalein', 'KWJ', '', '', 0, 1),
(2099, 133, 'Lae', 'LAE', '', '', 0, 1),
(2100, 133, 'Lib', 'LIB', '', '', 0, 1),
(2101, 133, 'Likiep', 'LKP', '', '', 0, 1),
(2102, 133, 'Majuro', 'MJR', '', '', 0, 1),
(2103, 133, 'Maloelap', 'MLP', '', '', 0, 1),
(2104, 133, 'Mejit', 'MJT', '', '', 0, 1),
(2105, 133, 'Mili', 'MIL', '', '', 0, 1),
(2106, 133, 'Namorik', 'NMK', '', '', 0, 1),
(2107, 133, 'Namu', 'NAM', '', '', 0, 1),
(2108, 133, 'Rongelap', 'RGL', '', '', 0, 1),
(2109, 133, 'Rongrik', 'RGK', '', '', 0, 1),
(2110, 133, 'Toke', 'TOK', '', '', 0, 1),
(2111, 133, 'Ujae', 'UJA', '', '', 0, 1),
(2112, 133, 'Ujelang', 'UJL', '', '', 0, 1),
(2113, 133, 'Utirik', 'UTK', '', '', 0, 1),
(2114, 133, 'Wotho', 'WTH', '', '', 0, 1),
(2115, 133, 'Wotje', 'WTJ', '', '', 0, 1),
(2116, 135, 'Adrar', 'AD', '', '', 0, 1),
(2117, 135, 'Assaba', 'AS', '', '', 0, 1),
(2118, 135, 'Brakna', 'BR', '', '', 0, 1),
(2119, 135, 'Dakhlet Nouadhibou', 'DN', '', '', 0, 1),
(2120, 135, 'Gorgol', 'GO', '', '', 0, 1),
(2121, 135, 'Guidimaka', 'GM', '', '', 0, 1),
(2122, 135, 'Hodh Ech Chargui', 'HC', '', '', 0, 1),
(2123, 135, 'Hodh El Gharbi', 'HG', '', '', 0, 1),
(2124, 135, 'Inchiri', 'IN', '', '', 0, 1),
(2125, 135, 'Tagant', 'TA', '', '', 0, 1),
(2126, 135, 'Tiris Zemmour', 'TZ', '', '', 0, 1),
(2127, 135, 'Trarza', 'TR', '', '', 0, 1),
(2128, 135, 'Nouakchott', 'NO', '', '', 0, 1),
(2129, 136, 'Beau Bassin-Rose Hill', 'BR', '', '', 0, 1),
(2130, 136, 'Curepipe', 'CU', '', '', 0, 1),
(2131, 136, 'Port Louis', 'PU', '', '', 0, 1),
(2132, 136, 'Quatre Bornes', 'QB', '', '', 0, 1),
(2133, 136, 'Vacoas-Phoenix', 'VP', '', '', 0, 1),
(2134, 136, 'Agalega Islands', 'AG', '', '', 0, 1),
(2135, 136, 'Cargados Carajos Shoals (Saint Brandon Islands)', 'CC', '', '', 0, 1),
(2136, 136, 'Rodrigues', 'RO', '', '', 0, 1),
(2137, 136, 'Black River', 'BL', '', '', 0, 1),
(2138, 136, 'Flacq', 'FL', '', '', 0, 1),
(2139, 136, 'Grand Port', 'GP', '', '', 0, 1),
(2140, 136, 'Moka', 'MO', '', '', 0, 1),
(2141, 136, 'Pamplemousses', 'PA', '', '', 0, 1),
(2142, 136, 'Plaines Wilhems', 'PW', '', '', 0, 1),
(2143, 136, 'Port Louis', 'PL', '', '', 0, 1),
(2144, 136, 'Riviere du Rempart', 'RR', '', '', 0, 1),
(2145, 136, 'Savanne', 'SA', '', '', 0, 1),
(2146, 138, 'Baja California Norte', 'BN', '', '', 0, 1),
(2147, 138, 'Baja California Sur', 'BS', '', '', 0, 1),
(2148, 138, 'Campeche', 'CA', '', '', 0, 1),
(2149, 138, 'Chiapas', 'CI', '', '', 0, 1),
(2150, 138, 'Chihuahua', 'CH', '', '', 0, 1),
(2151, 138, 'Coahuila de Zaragoza', 'CZ', '', '', 0, 1),
(2152, 138, 'Colima', 'CL', '', '', 0, 1),
(2153, 138, 'Distrito Federal', 'DF', '', '', 0, 1),
(2154, 138, 'Durango', 'DU', '', '', 0, 1),
(2155, 138, 'Guanajuato', 'GA', '', '', 0, 1),
(2156, 138, 'Guerrero', 'GE', '', '', 0, 1),
(2157, 138, 'Hidalgo', 'HI', '', '', 0, 1),
(2158, 138, 'Jalisco', 'JA', '', '', 0, 1),
(2159, 138, 'Mexico', 'ME', '', '', 0, 1),
(2160, 138, 'Michoacan de Ocampo', 'MI', '', '', 0, 1),
(2161, 138, 'Morelos', 'MO', '', '', 0, 1),
(2162, 138, 'Nayarit', 'NA', '', '', 0, 1),
(2163, 138, 'Nuevo Leon', 'NL', '', '', 0, 1),
(2164, 138, 'Oaxaca', 'OA', '', '', 0, 1),
(2165, 138, 'Puebla', 'PU', '', '', 0, 1),
(2166, 138, 'Queretaro de Arteaga', 'QA', '', '', 0, 1),
(2167, 138, 'Quintana Roo', 'QR', '', '', 0, 1),
(2168, 138, 'San Luis Potosi', 'SA', '', '', 0, 1),
(2169, 138, 'Sinaloa', 'SI', '', '', 0, 1),
(2170, 138, 'Sonora', 'SO', '', '', 0, 1),
(2171, 138, 'Tabasco', 'TB', '', '', 0, 1),
(2172, 138, 'Tamaulipas', 'TM', '', '', 0, 1),
(2173, 138, 'Tlaxcala', 'TL', '', '', 0, 1),
(2174, 138, 'Veracruz-Llave', 'VE', '', '', 0, 1),
(2175, 138, 'Yucatan', 'YU', '', '', 0, 1),
(2176, 138, 'Zacatecas', 'ZA', '', '', 0, 1),
(2177, 139, 'Chuuk', 'C', '', '', 0, 1),
(2178, 139, 'Kosrae', 'K', '', '', 0, 1),
(2179, 139, 'Pohnpei', 'P', '', '', 0, 1),
(2180, 139, 'Yap', 'Y', '', '', 0, 1),
(2181, 140, 'Gagauzia', 'GA', '', '', 0, 1),
(2182, 140, 'Chisinau', 'CU', '', '', 0, 1),
(2183, 140, 'Balti', 'BA', '', '', 0, 1),
(2184, 140, 'Cahul', 'CA', '', '', 0, 1),
(2185, 140, 'Edinet', 'ED', '', '', 0, 1),
(2186, 140, 'Lapusna', 'LA', '', '', 0, 1),
(2187, 140, 'Orhei', 'OR', '', '', 0, 1),
(2188, 140, 'Soroca', 'SO', '', '', 0, 1),
(2189, 140, 'Tighina', 'TI', '', '', 0, 1),
(2190, 140, 'Ungheni', 'UN', '', '', 0, 1),
(2191, 140, 'St‚nga Nistrului', 'SN', '', '', 0, 1),
(2192, 141, 'Fontvieille', 'FV', '', '', 0, 1),
(2193, 141, 'La Condamine', 'LC', '', '', 0, 1),
(2194, 141, 'Monaco-Ville', 'MV', '', '', 0, 1),
(2195, 141, 'Monte-Carlo', 'MC', '', '', 0, 1),
(2196, 142, 'Ulanbaatar', '1', '', '', 0, 1),
(2197, 142, 'Orhon', '035', '', '', 0, 1),
(2198, 142, 'Darhan uul', '037', '', '', 0, 1),
(2199, 142, 'Hentiy', '039', '', '', 0, 1),
(2200, 142, 'Hovsgol', '041', '', '', 0, 1),
(2201, 142, 'Hovd', '043', '', '', 0, 1),
(2202, 142, 'Uvs', '046', '', '', 0, 1),
(2203, 142, 'Tov', '047', '', '', 0, 1),
(2204, 142, 'Selenge', '049', '', '', 0, 1),
(2205, 142, 'Suhbaatar', '051', '', '', 0, 1),
(2206, 142, 'Omnogovi', '053', '', '', 0, 1),
(2207, 142, 'Ovorhangay', '055', '', '', 0, 1),
(2208, 142, 'Dzavhan', '057', '', '', 0, 1),
(2209, 142, 'DundgovL', '059', '', '', 0, 1),
(2210, 142, 'Dornod', '061', '', '', 0, 1),
(2211, 142, 'Dornogov', '063', '', '', 0, 1),
(2212, 142, 'Govi-Sumber', '064', '', '', 0, 1),
(2213, 142, 'Govi-Altay', '065', '', '', 0, 1),
(2214, 142, 'Bulgan', '067', '', '', 0, 1),
(2215, 142, 'Bayanhongor', '069', '', '', 0, 1),
(2216, 142, 'Bayan-Olgiy', '071', '', '', 0, 1),
(2217, 142, 'Arhangay', '073', '', '', 0, 1),
(2218, 143, 'Saint Anthony', 'A', '', '', 0, 1),
(2219, 143, 'Saint Georges', 'G', '', '', 0, 1),
(2220, 143, 'Saint Peter', 'P', '', '', 0, 1),
(2221, 144, 'Agadir', 'AGD', '', '', 0, 1),
(2222, 144, 'Al Hoceima', 'HOC', '', '', 0, 1),
(2223, 144, 'Azilal', 'AZI', '', '', 0, 1),
(2224, 144, 'Beni Mellal', 'BME', '', '', 0, 1),
(2225, 144, 'Ben Slimane', 'BSL', '', '', 0, 1),
(2226, 144, 'Boulemane', 'BLM', '', '', 0, 1),
(2227, 144, 'Casablanca', 'CBL', '', '', 0, 1),
(2228, 144, 'Chaouen', 'CHA', '', '', 0, 1),
(2229, 144, 'El Jadida', 'EJA', '', '', 0, 1),
(2230, 144, 'El Kelaa des Sraghna', 'EKS', '', '', 0, 1),
(2231, 144, 'Er Rachidia', 'ERA', '', '', 0, 1),
(2232, 144, 'Essaouira', 'ESS', '', '', 0, 1),
(2233, 144, 'Fes', 'FES', '', '', 0, 1),
(2234, 144, 'Figuig', 'FIG', '', '', 0, 1),
(2235, 144, 'Guelmim', 'GLM', '', '', 0, 1),
(2236, 144, 'Ifrane', 'IFR', '', '', 0, 1),
(2237, 144, 'Kenitra', 'KEN', '', '', 0, 1),
(2238, 144, 'Khemisset', 'KHM', '', '', 0, 1),
(2239, 144, 'Khenifra', 'KHN', '', '', 0, 1),
(2240, 144, 'Khouribga', 'KHO', '', '', 0, 1),
(2241, 144, 'Laayoune', 'LYN', '', '', 0, 1),
(2242, 144, 'Larache', 'LAR', '', '', 0, 1),
(2243, 144, 'Marrakech', 'MRK', '', '', 0, 1),
(2244, 144, 'Meknes', 'MKN', '', '', 0, 1),
(2245, 144, 'Nador', 'NAD', '', '', 0, 1),
(2246, 144, 'Ouarzazate', 'ORZ', '', '', 0, 1),
(2247, 144, 'Oujda', 'OUJ', '', '', 0, 1),
(2248, 144, 'Rabat-Sale', 'RSA', '', '', 0, 1),
(2249, 144, 'Safi', 'SAF', '', '', 0, 1),
(2250, 144, 'Settat', 'SET', '', '', 0, 1),
(2251, 144, 'Sidi Kacem', 'SKA', '', '', 0, 1),
(2252, 144, 'Tangier', 'TGR', '', '', 0, 1),
(2253, 144, 'Tan-Tan', 'TAN', '', '', 0, 1),
(2254, 144, 'Taounate', 'TAO', '', '', 0, 1),
(2255, 144, 'Taroudannt', 'TRD', '', '', 0, 1),
(2256, 144, 'Tata', 'TAT', '', '', 0, 1),
(2257, 144, 'Taza', 'TAZ', '', '', 0, 1),
(2258, 144, 'Tetouan', 'TET', '', '', 0, 1),
(2259, 144, 'Tiznit', 'TIZ', '', '', 0, 1),
(2260, 144, 'Ad Dakhla', 'ADK', '', '', 0, 1),
(2261, 144, 'Boujdour', 'BJD', '', '', 0, 1),
(2262, 144, 'Es Smara', 'ESM', '', '', 0, 1),
(2263, 145, 'Cabo Delgado', 'CD', '', '', 0, 1),
(2264, 145, 'Gaza', 'GZ', '', '', 0, 1),
(2265, 145, 'Inhambane', 'IN', '', '', 0, 1),
(2266, 145, 'Manica', 'MN', '', '', 0, 1),
(2267, 145, 'Maputo (city)', 'MC', '', '', 0, 1),
(2268, 145, 'Maputo', 'MP', '', '', 0, 1),
(2269, 145, 'Nampula', 'NA', '', '', 0, 1),
(2270, 145, 'Niassa', 'NI', '', '', 0, 1),
(2271, 145, 'Sofala', 'SO', '', '', 0, 1),
(2272, 145, 'Tete', 'TE', '', '', 0, 1),
(2273, 145, 'Zambezia', 'ZA', '', '', 0, 1),
(2274, 146, 'Ayeyarwady', 'AY', '', '', 0, 1),
(2275, 146, 'Bago', 'BG', '', '', 0, 1),
(2276, 146, 'Magway', 'MG', '', '', 0, 1),
(2277, 146, 'Mandalay', 'MD', '', '', 0, 1),
(2278, 146, 'Sagaing', 'SG', '', '', 0, 1),
(2279, 146, 'Tanintharyi', 'TN', '', '', 0, 1),
(2280, 146, 'Yangon', 'YG', '', '', 0, 1),
(2281, 146, 'Chin State', 'CH', '', '', 0, 1),
(2282, 146, 'Kachin State', 'KC', '', '', 0, 1),
(2283, 146, 'Kayah State', 'KH', '', '', 0, 1),
(2284, 146, 'Kayin State', 'KN', '', '', 0, 1),
(2285, 146, 'Mon State', 'MN', '', '', 0, 1),
(2286, 146, 'Rakhine State', 'RK', '', '', 0, 1),
(2287, 146, 'Shan State', 'SH', '', '', 0, 1),
(2288, 147, 'Caprivi', 'CA', '', '', 0, 1),
(2289, 147, 'Erongo', 'ER', '', '', 0, 1),
(2290, 147, 'Hardap', 'HA', '', '', 0, 1),
(2291, 147, 'Karas', 'KR', '', '', 0, 1),
(2292, 147, 'Kavango', 'KV', '', '', 0, 1),
(2293, 147, 'Khomas', 'KH', '', '', 0, 1),
(2294, 147, 'Kunene', 'KU', '', '', 0, 1),
(2295, 147, 'Ohangwena', 'OW', '', '', 0, 1),
(2296, 147, 'Omaheke', 'OK', '', '', 0, 1),
(2297, 147, 'Omusati', 'OT', '', '', 0, 1),
(2298, 147, 'Oshana', 'ON', '', '', 0, 1),
(2299, 147, 'Oshikoto', 'OO', '', '', 0, 1),
(2300, 147, 'Otjozondjupa', 'OJ', '', '', 0, 1),
(2301, 148, 'Aiwo', 'AO', '', '', 0, 1),
(2302, 148, 'Anabar', 'AA', '', '', 0, 1),
(2303, 148, 'Anetan', 'AT', '', '', 0, 1),
(2304, 148, 'Anibare', 'AI', '', '', 0, 1),
(2305, 148, 'Baiti', 'BA', '', '', 0, 1),
(2306, 148, 'Boe', 'BO', '', '', 0, 1),
(2307, 148, 'Buada', 'BU', '', '', 0, 1),
(2308, 148, 'Denigomodu', 'DE', '', '', 0, 1),
(2309, 148, 'Ewa', 'EW', '', '', 0, 1),
(2310, 148, 'Ijuw', 'IJ', '', '', 0, 1),
(2311, 148, 'Meneng', 'ME', '', '', 0, 1),
(2312, 148, 'Nibok', 'NI', '', '', 0, 1),
(2313, 148, 'Uaboe', 'UA', '', '', 0, 1),
(2314, 148, 'Yaren', 'YA', '', '', 0, 1),
(2315, 149, 'Bagmati', 'BA', '', '', 0, 1),
(2316, 149, 'Bheri', 'BH', '', '', 0, 1),
(2317, 149, 'Dhawalagiri', 'DH', '', '', 0, 1),
(2318, 149, 'Gandaki', 'GA', '', '', 0, 1),
(2319, 149, 'Janakpur', 'JA', '', '', 0, 1),
(2320, 149, 'Karnali', 'KA', '', '', 0, 1),
(2321, 149, 'Kosi', 'KO', '', '', 0, 1),
(2322, 149, 'Lumbini', 'LU', '', '', 0, 1),
(2323, 149, 'Mahakali', 'MA', '', '', 0, 1),
(2324, 149, 'Mechi', 'ME', '', '', 0, 1),
(2325, 149, 'Narayani', 'NA', '', '', 0, 1),
(2326, 149, 'Rapti', 'RA', '', '', 0, 1),
(2327, 149, 'Sagarmatha', 'SA', '', '', 0, 1),
(2328, 149, 'Seti', 'SE', '', '', 0, 1),
(2329, 150, 'Drenthe', 'DR', '', '', 0, 1),
(2330, 150, 'Flevoland', 'FL', '', '', 0, 1),
(2331, 150, 'Friesland', 'FR', '', '', 0, 1),
(2332, 150, 'Gelderland', 'GE', '', '', 0, 1),
(2333, 150, 'Groningen', 'GR', '', '', 0, 1),
(2334, 150, 'Limburg', 'LI', '', '', 0, 1),
(2335, 150, 'Noord Brabant', 'NB', '', '', 0, 1),
(2336, 150, 'Noord Holland', 'NH', '', '', 0, 1),
(2337, 150, 'Overijssel', 'OV', '', '', 0, 1),
(2338, 150, 'Utrecht', 'UT', '', '', 0, 1),
(2339, 150, 'Zeeland', 'ZE', '', '', 0, 1),
(2340, 150, 'Zuid Holland', 'ZH', '', '', 0, 1),
(2341, 152, 'Iles Loyaute', 'L', '', '', 0, 1),
(2342, 152, 'Nord', 'N', '', '', 0, 1),
(2343, 152, 'Sud', 'S', '', '', 0, 1),
(2344, 153, 'Auckland', 'AUK', '', '', 0, 1),
(2345, 153, 'Bay of Plenty', 'BOP', '', '', 0, 1),
(2346, 153, 'Canterbury', 'CAN', '', '', 0, 1),
(2347, 153, 'Coromandel', 'COR', '', '', 0, 1),
(2348, 153, 'Gisborne', 'GIS', '', '', 0, 1),
(2349, 153, 'Fiordland', 'FIO', '', '', 0, 1),
(2350, 153, 'Hawke''s Bay', 'HKB', '', '', 0, 1),
(2351, 153, 'Marlborough', 'MBH', '', '', 0, 1),
(2352, 153, 'Manawatu-Wanganui', 'MWT', '', '', 0, 1),
(2353, 153, 'Mt Cook-Mackenzie', 'MCM', '', '', 0, 1),
(2354, 153, 'Nelson', 'NSN', '', '', 0, 1),
(2355, 153, 'Northland', 'NTL', '', '', 0, 1),
(2356, 153, 'Otago', 'OTA', '', '', 0, 1),
(2357, 153, 'Southland', 'STL', '', '', 0, 1),
(2358, 153, 'Taranaki', 'TKI', '', '', 0, 1),
(2359, 153, 'Wellington', 'WGN', '', '', 0, 1),
(2360, 153, 'Waikato', 'WKO', '', '', 0, 1),
(2361, 153, 'Wairarapa', 'WAI', '', '', 0, 1),
(2362, 153, 'West Coast', 'WTC', '', '', 0, 1),
(2363, 154, 'Atlantico Norte', 'AN', '', '', 0, 1),
(2364, 154, 'Atlantico Sur', 'AS', '', '', 0, 1),
(2365, 154, 'Boaco', 'BO', '', '', 0, 1),
(2366, 154, 'Carazo', 'CA', '', '', 0, 1),
(2367, 154, 'Chinandega', 'CI', '', '', 0, 1),
(2368, 154, 'Chontales', 'CO', '', '', 0, 1),
(2369, 154, 'Esteli', 'ES', '', '', 0, 1),
(2370, 154, 'Granada', 'GR', '', '', 0, 1),
(2371, 154, 'Jinotega', 'JI', '', '', 0, 1),
(2372, 154, 'Leon', 'LE', '', '', 0, 1),
(2373, 154, 'Madriz', 'MD', '', '', 0, 1),
(2374, 154, 'Managua', 'MN', '', '', 0, 1),
(2375, 154, 'Masaya', 'MS', '', '', 0, 1);
INSERT INTO `sma_state` (`state_id`, `country_id`, `state_name`, `state_url`, `state_latitude`, `state_longitude`, `default`, `state_status`) VALUES
(2376, 154, 'Matagalpa', 'MT', '', '', 0, 1),
(2377, 154, 'Nuevo Segovia', 'NS', '', '', 0, 1),
(2378, 154, 'Rio San Juan', 'RS', '', '', 0, 1),
(2379, 154, 'Rivas', 'RI', '', '', 0, 1),
(2380, 155, 'Agadez', 'AG', '', '', 0, 1),
(2381, 155, 'Diffa', 'DF', '', '', 0, 1),
(2382, 155, 'Dosso', 'DS', '', '', 0, 1),
(2383, 155, 'Maradi', 'MA', '', '', 0, 1),
(2384, 155, 'Niamey', 'NM', '', '', 0, 1),
(2385, 155, 'Tahoua', 'TH', '', '', 0, 1),
(2386, 155, 'Tillaberi', 'TL', '', '', 0, 1),
(2387, 155, 'Zinder', 'ZD', '', '', 0, 1),
(2388, 156, 'Abia', 'AB', '', '', 0, 1),
(2389, 156, 'Abuja Federal Capital Territory', 'CT', '', '', 0, 1),
(2390, 156, 'Adamawa', 'AD', '', '', 0, 1),
(2391, 156, 'Akwa Ibom', 'AK', '', '', 0, 1),
(2392, 156, 'Anambra', 'AN', '', '', 0, 1),
(2393, 156, 'Bauchi', 'BC', '', '', 0, 1),
(2394, 156, 'Bayelsa', 'BY', '', '', 0, 1),
(2395, 156, 'Benue', 'BN', '', '', 0, 1),
(2396, 156, 'Borno', 'BO', '', '', 0, 1),
(2397, 156, 'Cross River', 'CR', '', '', 0, 1),
(2398, 156, 'Delta', 'DE', '', '', 0, 1),
(2399, 156, 'Ebonyi', 'EB', '', '', 0, 1),
(2400, 156, 'Edo', 'ED', '', '', 0, 1),
(2401, 156, 'Ekiti', 'EK', '', '', 0, 1),
(2402, 156, 'Enugu', 'EN', '', '', 0, 1),
(2403, 156, 'Gombe', 'GO', '', '', 0, 1),
(2404, 156, 'Imo', 'IM', '', '', 0, 1),
(2405, 156, 'Jigawa', 'JI', '', '', 0, 1),
(2406, 156, 'Kaduna', 'KD', '', '', 0, 1),
(2407, 156, 'Kano', 'KN', '', '', 0, 1),
(2408, 156, 'Katsina', 'KT', '', '', 0, 1),
(2409, 156, 'Kebbi', 'KE', '', '', 0, 1),
(2410, 156, 'Kogi', 'KO', '', '', 0, 1),
(2411, 156, 'Kwara', 'KW', '', '', 0, 1),
(2412, 156, 'Lagos', 'LA', '', '', 0, 1),
(2413, 156, 'Nassarawa', 'NA', '', '', 0, 1),
(2414, 156, 'Niger', 'NI', '', '', 0, 1),
(2415, 156, 'Ogun', 'OG', '', '', 0, 1),
(2416, 156, 'Ondo', 'ONG', '', '', 0, 1),
(2417, 156, 'Osun', 'OS', '', '', 0, 1),
(2418, 156, 'Oyo', 'OY', '', '', 0, 1),
(2419, 156, 'Plateau', 'PL', '', '', 0, 1),
(2420, 156, 'Rivers', 'RI', '', '', 0, 1),
(2421, 156, 'Sokoto', 'SO', '', '', 0, 1),
(2422, 156, 'Taraba', 'TA', '', '', 0, 1),
(2423, 156, 'Yobe', 'YO', '', '', 0, 1),
(2424, 156, 'Zamfara', 'ZA', '', '', 0, 1),
(2425, 159, 'Northern Islands', 'N', '', '', 0, 1),
(2426, 159, 'Rota', 'R', '', '', 0, 1),
(2427, 159, 'Saipan', 'S', '', '', 0, 1),
(2428, 159, 'Tinian', 'T', '', '', 0, 1),
(2429, 160, 'Akershus', 'AK', '', '', 0, 1),
(2430, 160, 'Aust-Agder', 'AA', '', '', 0, 1),
(2431, 160, 'Buskerud', 'BU', '', '', 0, 1),
(2432, 160, 'Finnmark', 'FM', '', '', 0, 1),
(2433, 160, 'Hedmark', 'HM', '', '', 0, 1),
(2434, 160, 'Hordaland', 'HL', '', '', 0, 1),
(2435, 160, 'More og Romdal', 'MR', '', '', 0, 1),
(2436, 160, 'Nord-Trondelag', 'NT', '', '', 0, 1),
(2437, 160, 'Nordland', 'NL', '', '', 0, 1),
(2438, 160, 'Ostfold', 'OF', '', '', 0, 1),
(2439, 160, 'Oppland', 'OP', '', '', 0, 1),
(2440, 160, 'Oslo', 'OL', '', '', 0, 1),
(2441, 160, 'Rogaland', 'RL', '', '', 0, 1),
(2442, 160, 'Sor-Trondelag', 'ST', '', '', 0, 1),
(2443, 160, 'Sogn og Fjordane', 'SJ', '', '', 0, 1),
(2444, 160, 'Svalbard', 'SV', '', '', 0, 1),
(2445, 160, 'Telemark', 'TM', '', '', 0, 1),
(2446, 160, 'Troms', 'TR', '', '', 0, 1),
(2447, 160, 'Vest-Agder', 'VA', '', '', 0, 1),
(2448, 160, 'Vestfold', 'VF', '', '', 0, 1),
(2449, 161, 'Ad Dakhiliyah', 'DA', '', '', 0, 1),
(2450, 161, 'Al Batinah', 'BA', '', '', 0, 1),
(2451, 161, 'Al Wusta', 'WU', '', '', 0, 1),
(2452, 161, 'Ash Sharqiyah', 'SH', '', '', 0, 1),
(2453, 161, 'Az Zahirah', 'ZA', '', '', 0, 1),
(2454, 161, 'Masqat', 'MA', '', '', 0, 1),
(2455, 161, 'Musandam', 'MU', '', '', 0, 1),
(2456, 161, 'Zufar', 'ZU', '', '', 0, 1),
(2457, 162, 'Balochistan', 'B', '', '', 0, 1),
(2458, 162, 'Federally Administered Tribal Areas', 'T', '', '', 0, 1),
(2459, 162, 'Islamabad Capital Territory', 'I', '', '', 0, 1),
(2460, 162, 'North-West Frontier', 'N', '', '', 0, 1),
(2461, 162, 'Punjab', 'P', '', '', 0, 1),
(2462, 162, 'Sindh', 'S', '', '', 0, 1),
(2463, 163, 'Aimeliik', 'AM', '', '', 0, 1),
(2464, 163, 'Airai', 'AR', '', '', 0, 1),
(2465, 163, 'Angaur', 'AN', '', '', 0, 1),
(2466, 163, 'Hatohobei', 'HA', '', '', 0, 1),
(2467, 163, 'Kayangel', 'KA', '', '', 0, 1),
(2468, 163, 'Koror', 'KO', '', '', 0, 1),
(2469, 163, 'Melekeok', 'ME', '', '', 0, 1),
(2470, 163, 'Ngaraard', 'NA', '', '', 0, 1),
(2471, 163, 'Ngarchelong', 'NG', '', '', 0, 1),
(2472, 163, 'Ngardmau', 'ND', '', '', 0, 1),
(2473, 163, 'Ngatpang', 'NT', '', '', 0, 1),
(2474, 163, 'Ngchesar', 'NC', '', '', 0, 1),
(2475, 163, 'Ngeremlengui', 'NR', '', '', 0, 1),
(2476, 163, 'Ngiwal', 'NW', '', '', 0, 1),
(2477, 163, 'Peleliu', 'PE', '', '', 0, 1),
(2478, 163, 'Sonsorol', 'SO', '', '', 0, 1),
(2479, 164, 'Bocas del Toro', 'BT', '', '', 0, 1),
(2480, 164, 'Chiriqui', 'CH', '', '', 0, 1),
(2481, 164, 'Cocle', 'CC', '', '', 0, 1),
(2482, 164, 'Colon', 'CL', '', '', 0, 1),
(2483, 164, 'Darien', 'DA', '', '', 0, 1),
(2484, 164, 'Herrera', 'HE', '', '', 0, 1),
(2485, 164, 'Los Santos', 'LS', '', '', 0, 1),
(2486, 164, 'Panama', 'PA', '', '', 0, 1),
(2487, 164, 'San Blas', 'SB', '', '', 0, 1),
(2488, 164, 'Veraguas', 'VG', '', '', 0, 1),
(2489, 165, 'Bougainville', 'BV', '', '', 0, 1),
(2490, 165, 'Central', 'CE', '', '', 0, 1),
(2491, 165, 'Chimbu', 'CH', '', '', 0, 1),
(2492, 165, 'Eastern Highlands', 'EH', '', '', 0, 1),
(2493, 165, 'East New Britain', 'EB', '', '', 0, 1),
(2494, 165, 'East Sepik', 'ES', '', '', 0, 1),
(2495, 165, 'Enga', 'EN', '', '', 0, 1),
(2496, 165, 'Gulf', 'GU', '', '', 0, 1),
(2497, 165, 'Madang', 'MD', '', '', 0, 1),
(2498, 165, 'Manus', 'MN', '', '', 0, 1),
(2499, 165, 'Milne Bay', 'MB', '', '', 0, 1),
(2500, 165, 'Morobe', 'MR', '', '', 0, 1),
(2501, 165, 'National Capital', 'NC', '', '', 0, 1),
(2502, 165, 'New Ireland', 'NI', '', '', 0, 1),
(2503, 165, 'Northern', 'NO', '', '', 0, 1),
(2504, 165, 'Sandaun', 'SA', '', '', 0, 1),
(2505, 165, 'Southern Highlands', 'SH', '', '', 0, 1),
(2506, 165, 'Western', 'WE', '', '', 0, 1),
(2507, 165, 'Western Highlands', 'WH', '', '', 0, 1),
(2508, 165, 'West New Britain', 'WB', '', '', 0, 1),
(2509, 166, 'Alto Paraguay', 'AG', '', '', 0, 1),
(2510, 166, 'Alto Parana', 'AN', '', '', 0, 1),
(2511, 166, 'Amambay', 'AM', '', '', 0, 1),
(2512, 166, 'Asuncion', 'AS', '', '', 0, 1),
(2513, 166, 'Boqueron', 'BO', '', '', 0, 1),
(2514, 166, 'Caaguazu', 'CG', '', '', 0, 1),
(2515, 166, 'Caazapa', 'CZ', '', '', 0, 1),
(2516, 166, 'Canindeyu', 'CN', '', '', 0, 1),
(2517, 166, 'Central', 'CE', '', '', 0, 1),
(2518, 166, 'Concepcion', 'CC', '', '', 0, 1),
(2519, 166, 'Cordillera', 'CD', '', '', 0, 1),
(2520, 166, 'Guaira', 'GU', '', '', 0, 1),
(2521, 166, 'Itapua', 'IT', '', '', 0, 1),
(2522, 166, 'Misiones', 'MI', '', '', 0, 1),
(2523, 166, 'Neembucu', 'NE', '', '', 0, 1),
(2524, 166, 'Paraguari', 'PA', '', '', 0, 1),
(2525, 166, 'Presidente Hayes', 'PH', '', '', 0, 1),
(2526, 166, 'San Pedro', 'SP', '', '', 0, 1),
(2527, 167, 'Amazonas', 'AM', '', '', 0, 1),
(2528, 167, 'Ancash', 'AN', '', '', 0, 1),
(2529, 167, 'Apurimac', 'AP', '', '', 0, 1),
(2530, 167, 'Arequipa', 'AR', '', '', 0, 1),
(2531, 167, 'Ayacucho', 'AY', '', '', 0, 1),
(2532, 167, 'Cajamarca', 'CJ', '', '', 0, 1),
(2533, 167, 'Callao', 'CL', '', '', 0, 1),
(2534, 167, 'Cusco', 'CU', '', '', 0, 1),
(2535, 167, 'Huancavelica', 'HV', '', '', 0, 1),
(2536, 167, 'Huanuco', 'HO', '', '', 0, 1),
(2537, 167, 'Ica', 'IC', '', '', 0, 1),
(2538, 167, 'Junin', 'JU', '', '', 0, 1),
(2539, 167, 'La Libertad', 'LD', '', '', 0, 1),
(2540, 167, 'Lambayeque', 'LY', '', '', 0, 1),
(2541, 167, 'Lima', 'LI', '', '', 0, 1),
(2542, 167, 'Loreto', 'LO', '', '', 0, 1),
(2543, 167, 'Madre de Dios', 'MD', '', '', 0, 1),
(2544, 167, 'Moquegua', 'MO', '', '', 0, 1),
(2545, 167, 'Pasco', 'PA', '', '', 0, 1),
(2546, 167, 'Piura', 'PI', '', '', 0, 1),
(2547, 167, 'Puno', 'PU', '', '', 0, 1),
(2548, 167, 'San Martin', 'SM', '', '', 0, 1),
(2549, 167, 'Tacna', 'TA', '', '', 0, 1),
(2550, 167, 'Tumbes', 'TU', '', '', 0, 1),
(2551, 167, 'Ucayali', 'UC', '', '', 0, 1),
(2552, 168, 'Abra', 'ABR', '', '', 0, 1),
(2553, 168, 'Agusan del Norte', 'ANO', '', '', 0, 1),
(2554, 168, 'Agusan del Sur', 'ASU', '', '', 0, 1),
(2555, 168, 'Aklan', 'AKL', '', '', 0, 1),
(2556, 168, 'Albay', 'ALB', '', '', 0, 1),
(2557, 168, 'Antique', 'ANT', '', '', 0, 1),
(2558, 168, 'Apayao', 'APY', '', '', 0, 1),
(2559, 168, 'Aurora', 'AUR', '', '', 0, 1),
(2560, 168, 'Basilan', 'BAS', '', '', 0, 1),
(2561, 168, 'Bataan', 'BTA', '', '', 0, 1),
(2562, 168, 'Batanes', 'BTE', '', '', 0, 1),
(2563, 168, 'Batangas', 'BTG', '', '', 0, 1),
(2564, 168, 'Biliran', 'BLR', '', '', 0, 1),
(2565, 168, 'Benguet', 'BEN', '', '', 0, 1),
(2566, 168, 'Bohol', 'BOL', '', '', 0, 1),
(2567, 168, 'Bukidnon', 'BUK', '', '', 0, 1),
(2568, 168, 'Bulacan', 'BUL', '', '', 0, 1),
(2569, 168, 'Cagayan', 'CAG', '', '', 0, 1),
(2570, 168, 'Camarines Norte', 'CNO', '', '', 0, 1),
(2571, 168, 'Camarines Sur', 'CSU', '', '', 0, 1),
(2572, 168, 'Camiguin', 'CAM', '', '', 0, 1),
(2573, 168, 'Capiz', 'CAP', '', '', 0, 1),
(2574, 168, 'Catanduanes', 'CAT', '', '', 0, 1),
(2575, 168, 'Cavite', 'CAV', '', '', 0, 1),
(2576, 168, 'Cebu', 'CEB', '', '', 0, 1),
(2577, 168, 'Compostela', 'CMP', '', '', 0, 1),
(2578, 168, 'Davao del Norte', 'DNO', '', '', 0, 1),
(2579, 168, 'Davao del Sur', 'DSU', '', '', 0, 1),
(2580, 168, 'Davao Oriental', 'DOR', '', '', 0, 1),
(2581, 168, 'Eastern Samar', 'ESA', '', '', 0, 1),
(2582, 168, 'Guimaras', 'GUI', '', '', 0, 1),
(2583, 168, 'Ifugao', 'IFU', '', '', 0, 1),
(2584, 168, 'Ilocos Norte', 'INO', '', '', 0, 1),
(2585, 168, 'Ilocos Sur', 'ISU', '', '', 0, 1),
(2586, 168, 'Iloilo', 'ILO', '', '', 0, 1),
(2587, 168, 'Isabela', 'ISA', '', '', 0, 1),
(2588, 168, 'Kalinga', 'KAL', '', '', 0, 1),
(2589, 168, 'Laguna', 'LAG', '', '', 0, 1),
(2590, 168, 'Lanao del Norte', 'LNO', '', '', 0, 1),
(2591, 168, 'Lanao del Sur', 'LSU', '', '', 0, 1),
(2592, 168, 'La Union', 'UNI', '', '', 0, 1),
(2593, 168, 'Leyte', 'LEY', '', '', 0, 1),
(2594, 168, 'Maguindanao', 'MAG', '', '', 0, 1),
(2595, 168, 'Marinduque', 'MRN', '', '', 0, 1),
(2596, 168, 'Masbate', 'MSB', '', '', 0, 1),
(2597, 168, 'Mindoro Occidental', 'MIC', '', '', 0, 1),
(2598, 168, 'Mindoro Oriental', 'MIR', '', '', 0, 1),
(2599, 168, 'Misamis Occidental', 'MSC', '', '', 0, 1),
(2600, 168, 'Misamis Oriental', 'MOR', '', '', 0, 1),
(2601, 168, 'Mountain', 'MOP', '', '', 0, 1),
(2602, 168, 'Negros Occidental', 'NOC', '', '', 0, 1),
(2603, 168, 'Negros Oriental', 'NOR', '', '', 0, 1),
(2604, 168, 'North Cotabato', 'NCT', '', '', 0, 1),
(2605, 168, 'Northern Samar', 'NSM', '', '', 0, 1),
(2606, 168, 'Nueva Ecija', 'NEC', '', '', 0, 1),
(2607, 168, 'Nueva Vizcaya', 'NVZ', '', '', 0, 1),
(2608, 168, 'Palawan', 'PLW', '', '', 0, 1),
(2609, 168, 'Pampanga', 'PMP', '', '', 0, 1),
(2610, 168, 'Pangasinan', 'PNG', '', '', 0, 1),
(2611, 168, 'Quezon', 'QZN', '', '', 0, 1),
(2612, 168, 'Quirino', 'QRN', '', '', 0, 1),
(2613, 168, 'Rizal', 'RIZ', '', '', 0, 1),
(2614, 168, 'Romblon', 'ROM', '', '', 0, 1),
(2615, 168, 'Samar', 'SMR', '', '', 0, 1),
(2616, 168, 'Sarangani', 'SRG', '', '', 0, 1),
(2617, 168, 'Siquijor', 'SQJ', '', '', 0, 1),
(2618, 168, 'Sorsogon', 'SRS', '', '', 0, 1),
(2619, 168, 'South Cotabato', 'SCO', '', '', 0, 1),
(2620, 168, 'Southern Leyte', 'SLE', '', '', 0, 1),
(2621, 168, 'Sultan Kudarat', 'SKU', '', '', 0, 1),
(2622, 168, 'Sulu', 'SLU', '', '', 0, 1),
(2623, 168, 'Surigao del Norte', 'SNO', '', '', 0, 1),
(2624, 168, 'Surigao del Sur', 'SSU', '', '', 0, 1),
(2625, 168, 'Tarlac', 'TAR', '', '', 0, 1),
(2626, 168, 'Tawi-Tawi', 'TAW', '', '', 0, 1),
(2627, 168, 'Zambales', 'ZBL', '', '', 0, 1),
(2628, 168, 'Zamboanga del Norte', 'ZNO', '', '', 0, 1),
(2629, 168, 'Zamboanga del Sur', 'ZSU', '', '', 0, 1),
(2630, 168, 'Zamboanga Sibugay', 'ZSI', '', '', 0, 1),
(2631, 170, 'Dolnoslaskie', 'DO', '', '', 0, 1),
(2632, 170, 'Kujawsko-Pomorskie', 'KP', '', '', 0, 1),
(2633, 170, 'Lodzkie', 'LO', '', '', 0, 1),
(2634, 170, 'Lubelskie', 'LL', '', '', 0, 1),
(2635, 170, 'Lubuskie', 'LU', '', '', 0, 1),
(2636, 170, 'Malopolskie', 'ML', '', '', 0, 1),
(2637, 170, 'Mazowieckie', 'MZ', '', '', 0, 1),
(2638, 170, 'Opolskie', 'OP', '', '', 0, 1),
(2639, 170, 'Podkarpackie', 'PP', '', '', 0, 1),
(2640, 170, 'Podlaskie', 'PL', '', '', 0, 1),
(2641, 170, 'Pomorskie', 'PM', '', '', 0, 1),
(2642, 170, 'Slaskie', 'SL', '', '', 0, 1),
(2643, 170, 'Swietokrzyskie', 'SW', '', '', 0, 1),
(2644, 170, 'Warminsko-Mazurskie', 'WM', '', '', 0, 1),
(2645, 170, 'Wielkopolskie', 'WP', '', '', 0, 1),
(2646, 170, 'Zachodniopomorskie', 'ZA', '', '', 0, 1),
(2647, 198, 'Saint Pierre', 'P', '', '', 0, 1),
(2648, 198, 'Miquelon', 'M', '', '', 0, 1),
(2649, 171, 'Accedilores', 'AC', '', '', 0, 1),
(2650, 171, 'Aveiro', 'AV', '', '', 0, 1),
(2651, 171, 'Beja', 'BE', '', '', 0, 1),
(2652, 171, 'Braga', 'BR', '', '', 0, 1),
(2653, 171, 'Bragan&ccedil;a', 'BA', '', '', 0, 1),
(2654, 171, 'Castelo Branco', 'CB', '', '', 0, 1),
(2655, 171, 'Coimbra', 'CO', '', '', 0, 1),
(2656, 171, 'Eacutevora', 'EV', '', '', 0, 1),
(2657, 171, 'Faro', 'FA', '', '', 0, 1),
(2658, 171, 'Guarda', 'GU', '', '', 0, 1),
(2659, 171, 'Leiria', 'LE', '', '', 0, 1),
(2660, 171, 'Lisboa', 'LI', '', '', 0, 1),
(2661, 171, 'Madeira', 'ME', '', '', 0, 1),
(2662, 171, 'Portalegre', 'PO', '', '', 0, 1),
(2663, 171, 'Porto', 'PR', '', '', 0, 1),
(2664, 171, 'Santar&eacute;m', 'SA', '', '', 0, 1),
(2665, 171, 'Set&uacute;bal', 'SE', '', '', 0, 1),
(2666, 171, 'Viana do Castelo', 'VC', '', '', 0, 1),
(2667, 171, 'Vila Real', 'VR', '', '', 0, 1),
(2668, 171, 'Viseu', 'VI', '', '', 0, 1),
(2669, 173, 'Ad Dawhah', 'DW', '', '', 0, 1),
(2670, 173, 'Al Ghuwayriyah', 'GW', '', '', 0, 1),
(2671, 173, 'Al Jumayliyah', 'JM', '', '', 0, 1),
(2672, 173, 'Al Khawr', 'KR', '', '', 0, 1),
(2673, 173, 'Al Wakrah', 'WK', '', '', 0, 1),
(2674, 173, 'Ar Rayyan', 'RN', '', '', 0, 1),
(2675, 173, 'Jarayan al Batinah', 'JB', '', '', 0, 1),
(2676, 173, 'Madinat ash Shamal', 'MS', '', '', 0, 1),
(2677, 173, 'Umm Sa''id', 'UD', '', '', 0, 1),
(2678, 173, 'Umm Salal', 'UL', '', '', 0, 1),
(2679, 175, 'Alba', 'AB', '', '', 0, 1),
(2680, 175, 'Arad', 'AR', '', '', 0, 1),
(2681, 175, 'Arges', 'AG', '', '', 0, 1),
(2682, 175, 'Bacau', 'BC', '', '', 0, 1),
(2683, 175, 'Bihor', 'BH', '', '', 0, 1),
(2684, 175, 'Bistrita-Nasaud', 'BN', '', '', 0, 1),
(2685, 175, 'Botosani', 'BT', '', '', 0, 1),
(2686, 175, 'Brasov', 'BV', '', '', 0, 1),
(2687, 175, 'Braila', 'BR', '', '', 0, 1),
(2688, 175, 'Bucuresti', 'B', '', '', 0, 1),
(2689, 175, 'Buzau', 'BZ', '', '', 0, 1),
(2690, 175, 'Caras-Severin', 'CS', '', '', 0, 1),
(2691, 175, 'Calarasi', 'CL', '', '', 0, 1),
(2692, 175, 'Cluj', 'CJ', '', '', 0, 1),
(2693, 175, 'Constanta', 'CT', '', '', 0, 1),
(2694, 175, 'Covasna', 'CV', '', '', 0, 1),
(2695, 175, 'Dimbovita', 'DB', '', '', 0, 1),
(2696, 175, 'Dolj', 'DJ', '', '', 0, 1),
(2697, 175, 'Galati', 'GL', '', '', 0, 1),
(2698, 175, 'Giurgiu', 'GR', '', '', 0, 1),
(2699, 175, 'Gorj', 'GJ', '', '', 0, 1),
(2700, 175, 'Harghita', 'HR', '', '', 0, 1),
(2701, 175, 'Hunedoara', 'HD', '', '', 0, 1),
(2702, 175, 'Ialomita', 'IL', '', '', 0, 1),
(2703, 175, 'Iasi', 'IS', '', '', 0, 1),
(2704, 175, 'Ilfov', 'IF', '', '', 0, 1),
(2705, 175, 'Maramures', 'MM', '', '', 0, 1),
(2706, 175, 'Mehedinti', 'MH', '', '', 0, 1),
(2707, 175, 'Mures', 'MS', '', '', 0, 1),
(2708, 175, 'Neamt', 'NT', '', '', 0, 1),
(2709, 175, 'Olt', 'OT', '', '', 0, 1),
(2710, 175, 'Prahova', 'PH', '', '', 0, 1),
(2711, 175, 'Satu-Mare', 'SM', '', '', 0, 1),
(2712, 175, 'Salaj', 'SJ', '', '', 0, 1),
(2713, 175, 'Sibiu', 'SB', '', '', 0, 1),
(2714, 175, 'Suceava', 'SV', '', '', 0, 1),
(2715, 175, 'Teleorman', 'TR', '', '', 0, 1),
(2716, 175, 'Timis', 'TM', '', '', 0, 1),
(2717, 175, 'Tulcea', 'TL', '', '', 0, 1),
(2718, 175, 'Vaslui', 'VS', '', '', 0, 1),
(2719, 175, 'Valcea', 'VL', '', '', 0, 1),
(2720, 175, 'Vrancea', 'VN', '', '', 0, 1),
(2721, 176, 'Abakan', 'AB', '', '', 0, 1),
(2722, 176, 'Aginskoye', 'AG', '', '', 0, 1),
(2723, 176, 'Anadyr', 'AN', '', '', 0, 1),
(2724, 176, 'Arkahangelsk', 'AR', '', '', 0, 1),
(2725, 176, 'Astrakhan', 'AS', '', '', 0, 1),
(2726, 176, 'Barnaul', 'BA', '', '', 0, 1),
(2727, 176, 'Belgorod', 'BE', '', '', 0, 1),
(2728, 176, 'Birobidzhan', 'BI', '', '', 0, 1),
(2729, 176, 'Blagoveshchensk', 'BL', '', '', 0, 1),
(2730, 176, 'Bryansk', 'BR', '', '', 0, 1),
(2731, 176, 'Cheboksary', 'CH', '', '', 0, 1),
(2732, 176, 'Chelyabinsk', 'CL', '', '', 0, 1),
(2733, 176, 'Cherkessk', 'CR', '', '', 0, 1),
(2734, 176, 'Chita', 'CI', '', '', 0, 1),
(2735, 176, 'Dudinka', 'DU', '', '', 0, 1),
(2736, 176, 'Elista', 'EL', '', '', 0, 1),
(2737, 176, 'Gomo-Altaysk', 'GO', '', '', 0, 1),
(2738, 176, 'Gorno-Altaysk', 'GA', '', '', 0, 1),
(2739, 176, 'Groznyy', 'GR', '', '', 0, 1),
(2740, 176, 'Irkutsk', 'IR', '', '', 0, 1),
(2741, 176, 'Ivanovo', 'IV', '', '', 0, 1),
(2742, 176, 'Izhevsk', 'IZ', '', '', 0, 1),
(2743, 176, 'Kalinigrad', 'KA', '', '', 0, 1),
(2744, 176, 'Kaluga', 'KL', '', '', 0, 1),
(2745, 176, 'Kasnodar', 'KS', '', '', 0, 1),
(2746, 176, 'Kazan', 'KZ', '', '', 0, 1),
(2747, 176, 'Kemerovo', 'KE', '', '', 0, 1),
(2748, 176, 'Khabarovsk', 'KH', '', '', 0, 1),
(2749, 176, 'Khanty-Mansiysk', 'KM', '', '', 0, 1),
(2750, 176, 'Kostroma', 'KO', '', '', 0, 1),
(2751, 176, 'Krasnodar', 'KR', '', '', 0, 1),
(2752, 176, 'Krasnoyarsk', 'KN', '', '', 0, 1),
(2753, 176, 'Kudymkar', 'KU', '', '', 0, 1),
(2754, 176, 'Kurgan', 'KG', '', '', 0, 1),
(2755, 176, 'Kursk', 'KK', '', '', 0, 1),
(2756, 176, 'Kyzyl', 'KY', '', '', 0, 1),
(2757, 176, 'Lipetsk', 'LI', '', '', 0, 1),
(2758, 176, 'Magadan', 'MA', '', '', 0, 1),
(2759, 176, 'Makhachkala', 'MK', '', '', 0, 1),
(2760, 176, 'Maykop', 'MY', '', '', 0, 1),
(2761, 176, 'Moscow', 'MO', '', '', 0, 1),
(2762, 176, 'Murmansk', 'MU', '', '', 0, 1),
(2763, 176, 'Nalchik', 'NA', '', '', 0, 1),
(2764, 176, 'Naryan Mar', 'NR', '', '', 0, 1),
(2765, 176, 'Nazran', 'NZ', '', '', 0, 1),
(2766, 176, 'Nizhniy Novgorod', 'NI', '', '', 0, 1),
(2767, 176, 'Novgorod', 'NO', '', '', 0, 1),
(2768, 176, 'Novosibirsk', 'NV', '', '', 0, 1),
(2769, 176, 'Omsk', 'OM', '', '', 0, 1),
(2770, 176, 'Orel', 'OR', '', '', 0, 1),
(2771, 176, 'Orenburg', 'OE', '', '', 0, 1),
(2772, 176, 'Palana', 'PA', '', '', 0, 1),
(2773, 176, 'Penza', 'PE', '', '', 0, 1),
(2774, 176, 'Perm', 'PR', '', '', 0, 1),
(2775, 176, 'Petropavlovsk-Kamchatskiy', 'PK', '', '', 0, 1),
(2776, 176, 'Petrozavodsk', 'PT', '', '', 0, 1),
(2777, 176, 'Pskov', 'PS', '', '', 0, 1),
(2778, 176, 'Rostov-na-Donu', 'RO', '', '', 0, 1),
(2779, 176, 'Ryazan', 'RY', '', '', 0, 1),
(2780, 176, 'Salekhard', 'SL', '', '', 0, 1),
(2781, 176, 'Samara', 'SA', '', '', 0, 1),
(2782, 176, 'Saransk', 'SR', '', '', 0, 1),
(2783, 176, 'Saratov', 'SV', '', '', 0, 1),
(2784, 176, 'Smolensk', 'SM', '', '', 0, 1),
(2785, 176, 'St. Petersburg', 'SP', '', '', 0, 1),
(2786, 176, 'Stavropol', 'ST', '', '', 0, 1),
(2787, 176, 'Syktyvkar', 'SY', '', '', 0, 1),
(2788, 176, 'Tambov', 'TA', '', '', 0, 1),
(2789, 176, 'Tomsk', 'TO', '', '', 0, 1),
(2790, 176, 'Tula', 'TU', '', '', 0, 1),
(2791, 176, 'Tura', 'TR', '', '', 0, 1),
(2792, 176, 'Tver', 'TV', '', '', 0, 1),
(2793, 176, 'Tyumen', 'TY', '', '', 0, 1),
(2794, 176, 'Ufa', 'UF', '', '', 0, 1),
(2795, 176, 'Ul''yanovsk', 'UL', '', '', 0, 1),
(2796, 176, 'Ulan-Ude', 'UU', '', '', 0, 1),
(2797, 176, 'Ust''-Ordynskiy', 'US', '', '', 0, 1),
(2798, 176, 'Vladikavkaz', 'VL', '', '', 0, 1),
(2799, 176, 'Vladimir', 'VA', '', '', 0, 1),
(2800, 176, 'Vladivostok', 'VV', '', '', 0, 1),
(2801, 176, 'Volgograd', 'VG', '', '', 0, 1),
(2802, 176, 'Vologda', 'VD', '', '', 0, 1),
(2803, 176, 'Voronezh', 'VO', '', '', 0, 1),
(2804, 176, 'Vyatka', 'VY', '', '', 0, 1),
(2805, 176, 'Yakutsk', 'YA', '', '', 0, 1),
(2806, 176, 'Yaroslavl', 'YR', '', '', 0, 1),
(2807, 176, 'Yekaterinburg', 'YE', '', '', 0, 1),
(2808, 176, 'Yoshkar-Ola', 'YO', '', '', 0, 1),
(2809, 177, 'Butare', 'BU', '', '', 0, 1),
(2810, 177, 'Byumba', 'BY', '', '', 0, 1),
(2811, 177, 'Cyangugu', 'CY', '', '', 0, 1),
(2812, 177, 'Gikongoro', 'GK', '', '', 0, 1),
(2813, 177, 'Gisenyi', 'GS', '', '', 0, 1),
(2814, 177, 'Gitarama', 'GT', '', '', 0, 1),
(2815, 177, 'Kibungo', 'KG', '', '', 0, 1),
(2816, 177, 'Kibuye', 'KY', '', '', 0, 1),
(2817, 177, 'Kigali Rurale', 'KR', '', '', 0, 1),
(2818, 177, 'Kigali-ville', 'KV', '', '', 0, 1),
(2819, 177, 'Ruhengeri', 'RU', '', '', 0, 1),
(2820, 177, 'Umutara', 'UM', '', '', 0, 1),
(2821, 178, 'Christ Church Nichola Town', 'CCN', '', '', 0, 1),
(2822, 178, 'Saint Anne Sandy Point', 'SAS', '', '', 0, 1),
(2823, 178, 'Saint George Basseterre', 'SGB', '', '', 0, 1),
(2824, 178, 'Saint George Gingerland', 'SGG', '', '', 0, 1),
(2825, 178, 'Saint James Windward', 'SJW', '', '', 0, 1),
(2826, 178, 'Saint John Capesterre', 'SJC', '', '', 0, 1),
(2827, 178, 'Saint John Figtree', 'SJF', '', '', 0, 1),
(2828, 178, 'Saint Mary Cayon', 'SMC', '', '', 0, 1),
(2829, 178, 'Saint Paul Capesterre', 'CAP', '', '', 0, 1),
(2830, 178, 'Saint Paul Charlestown', 'CHA', '', '', 0, 1),
(2831, 178, 'Saint Peter Basseterre', 'SPB', '', '', 0, 1),
(2832, 178, 'Saint Thomas Lowland', 'STL', '', '', 0, 1),
(2833, 178, 'Saint Thomas Middle Island', 'STM', '', '', 0, 1),
(2834, 178, 'Trinity Palmetto Point', 'TPP', '', '', 0, 1),
(2835, 179, 'Anse-la-Raye', 'AR', '', '', 0, 1),
(2836, 179, 'Castries', 'CA', '', '', 0, 1),
(2837, 179, 'Choiseul', 'CH', '', '', 0, 1),
(2838, 179, 'Dauphin', 'DA', '', '', 0, 1),
(2839, 179, 'Dennery', 'DE', '', '', 0, 1),
(2840, 179, 'Gros-Islet', 'GI', '', '', 0, 1),
(2841, 179, 'Laborie', 'LA', '', '', 0, 1),
(2842, 179, 'Micoud', 'MI', '', '', 0, 1),
(2843, 179, 'Praslin', 'PR', '', '', 0, 1),
(2844, 179, 'Soufriere', 'SO', '', '', 0, 1),
(2845, 179, 'Vieux-Fort', 'VF', '', '', 0, 1),
(2846, 180, 'Charlotte', 'C', '', '', 0, 1),
(2847, 180, 'Grenadines', 'R', '', '', 0, 1),
(2848, 180, 'Saint Andrew', 'A', '', '', 0, 1),
(2849, 180, 'Saint David', 'D', '', '', 0, 1),
(2850, 180, 'Saint George', 'G', '', '', 0, 1),
(2851, 180, 'Saint Patrick', 'P', '', '', 0, 1),
(2852, 181, 'Aana', 'AN', '', '', 0, 1),
(2853, 181, 'Aiga-i-le-Tai', 'AI', '', '', 0, 1),
(2854, 181, 'Atua', 'AT', '', '', 0, 1),
(2855, 181, 'Fa''asaleleaga', 'FA', '', '', 0, 1),
(2856, 181, 'Gaga''emauga', 'GE', '', '', 0, 1),
(2857, 181, 'Gagaifomauga', 'GF', '', '', 0, 1),
(2858, 181, 'Palauli', 'PA', '', '', 0, 1),
(2859, 181, 'Satupa''itea', 'SA', '', '', 0, 1),
(2860, 181, 'Tuamasaga', 'TU', '', '', 0, 1),
(2861, 181, 'Va''a-o-Fonoti', 'VF', '', '', 0, 1),
(2862, 181, 'Vaisigano', 'VS', '', '', 0, 1),
(2863, 182, 'Acquaviva', 'AC', '', '', 0, 1),
(2864, 182, 'Borgo Maggiore', 'BM', '', '', 0, 1),
(2865, 182, 'Chiesanuova', 'CH', '', '', 0, 1),
(2866, 182, 'Domagnano', 'DO', '', '', 0, 1),
(2867, 182, 'Faetano', 'FA', '', '', 0, 1),
(2868, 182, 'Fiorentino', 'FI', '', '', 0, 1),
(2869, 182, 'Montegiardino', 'MO', '', '', 0, 1),
(2870, 182, 'Citta di San Marino', 'SM', '', '', 0, 1),
(2871, 182, 'Serravalle', 'SE', '', '', 0, 1),
(2872, 183, 'Sao Tome', 'S', '', '', 0, 1),
(2873, 183, 'Principe', 'P', '', '', 0, 1),
(2874, 184, 'Al Bahah', 'BH', '', '', 0, 1),
(2875, 184, 'Al Hudud ash Shamaliyah', 'HS', '', '', 0, 1),
(2876, 184, 'Al Jawf', 'JF', '', '', 0, 1),
(2877, 184, 'Al Madinah', 'MD', '', '', 0, 1),
(2878, 184, 'Al Qasim', 'QS', '', '', 0, 1),
(2879, 184, 'Ar Riyad', 'RD', '', '', 0, 1),
(2880, 184, 'Ash Sharqiyah (Eastern)', 'AQ', '', '', 0, 1),
(2881, 184, 'Asir', 'AS', '', '', 0, 1),
(2882, 184, 'Ha''il', 'HL', '', '', 0, 1),
(2883, 184, 'Jizan', 'JZ', '', '', 0, 1),
(2884, 184, 'Makkah', 'ML', '', '', 0, 1),
(2885, 184, 'Najran', 'NR', '', '', 0, 1),
(2886, 184, 'Tabuk', 'TB', '', '', 0, 1),
(2887, 185, 'Dakar', 'DA', '', '', 0, 1),
(2888, 185, 'Diourbel', 'DI', '', '', 0, 1),
(2889, 185, 'Fatick', 'FA', '', '', 0, 1),
(2890, 185, 'Kaolack', 'KA', '', '', 0, 1),
(2891, 185, 'Kolda', 'KO', '', '', 0, 1),
(2892, 185, 'Louga', 'LO', '', '', 0, 1),
(2893, 185, 'Matam', 'MA', '', '', 0, 1),
(2894, 185, 'Saint-Louis', 'SL', '', '', 0, 1),
(2895, 185, 'Tambacounda', 'TA', '', '', 0, 1),
(2896, 185, 'Thies', 'TH', '', '', 0, 1),
(2897, 185, 'Ziguinchor', 'ZI', '', '', 0, 1),
(2898, 186, 'Anse aux Pins', 'AP', '', '', 0, 1),
(2899, 186, 'Anse Boileau', 'AB', '', '', 0, 1),
(2900, 186, 'Anse Etoile', 'AE', '', '', 0, 1),
(2901, 186, 'Anse Louis', 'AL', '', '', 0, 1),
(2902, 186, 'Anse Royale', 'AR', '', '', 0, 1),
(2903, 186, 'Baie Lazare', 'BL', '', '', 0, 1),
(2904, 186, 'Baie Sainte Anne', 'BS', '', '', 0, 1),
(2905, 186, 'Beau Vallon', 'BV', '', '', 0, 1),
(2906, 186, 'Bel Air', 'BA', '', '', 0, 1),
(2907, 186, 'Bel Ombre', 'BO', '', '', 0, 1),
(2908, 186, 'Cascade', 'CA', '', '', 0, 1),
(2909, 186, 'Glacis', 'GL', '', '', 0, 1),
(2910, 186, 'Grand'' Anse (on Mahe)', 'GM', '', '', 0, 1),
(2911, 186, 'Grand'' Anse (on Praslin)', 'GP', '', '', 0, 1),
(2912, 186, 'La Digue', 'DG', '', '', 0, 1),
(2913, 186, 'La Riviere Anglaise', 'RA', '', '', 0, 1),
(2914, 186, 'Mont Buxton', 'MB', '', '', 0, 1),
(2915, 186, 'Mont Fleuri', 'MF', '', '', 0, 1),
(2916, 186, 'Plaisance', 'PL', '', '', 0, 1),
(2917, 186, 'Pointe La Rue', 'PR', '', '', 0, 1),
(2918, 186, 'Port Glaud', 'PG', '', '', 0, 1),
(2919, 186, 'Saint Louis', 'SL', '', '', 0, 1),
(2920, 186, 'Takamaka', 'TA', '', '', 0, 1),
(2921, 187, 'Eastern', 'E', '', '', 0, 1),
(2922, 187, 'Northern', 'N', '', '', 0, 1),
(2923, 187, 'Southern', 'S', '', '', 0, 1),
(2924, 187, 'Western', 'W', '', '', 0, 1),
(2925, 189, 'Banskobystrický', 'BA', '', '', 0, 1),
(2926, 189, 'Bratislavský', 'BR', '', '', 0, 1),
(2927, 189, 'Košický', 'KO', '', '', 0, 1),
(2928, 189, 'Nitriansky', 'NI', '', '', 0, 1),
(2929, 189, 'Prešovský', 'PR', '', '', 0, 1),
(2930, 189, 'Trenčiansky', 'TC', '', '', 0, 1),
(2931, 189, 'Trnavský', 'TV', '', '', 0, 1),
(2932, 189, 'Žilinský', 'ZI', '', '', 0, 1),
(2933, 191, 'Central', 'CE', '', '', 0, 1),
(2934, 191, 'Choiseul', 'CH', '', '', 0, 1),
(2935, 191, 'Guadalcanal', 'GC', '', '', 0, 1),
(2936, 191, 'Honiara', 'HO', '', '', 0, 1),
(2937, 191, 'Isabel', 'IS', '', '', 0, 1),
(2938, 191, 'Makira', 'MK', '', '', 0, 1),
(2939, 191, 'Malaita', 'ML', '', '', 0, 1),
(2940, 191, 'Rennell and Bellona', 'RB', '', '', 0, 1),
(2941, 191, 'Temotu', 'TM', '', '', 0, 1),
(2942, 191, 'Western', 'WE', '', '', 0, 1),
(2943, 192, 'Awdal', 'AW', '', '', 0, 1),
(2944, 192, 'Bakool', 'BK', '', '', 0, 1),
(2945, 192, 'Banaadir', 'BN', '', '', 0, 1),
(2946, 192, 'Bari', 'BR', '', '', 0, 1),
(2947, 192, 'Bay', 'BY', '', '', 0, 1),
(2948, 192, 'Galguduud', 'GA', '', '', 0, 1),
(2949, 192, 'Gedo', 'GE', '', '', 0, 1),
(2950, 192, 'Hiiraan', 'HI', '', '', 0, 1),
(2951, 192, 'Jubbada Dhexe', 'JD', '', '', 0, 1),
(2952, 192, 'Jubbada Hoose', 'JH', '', '', 0, 1),
(2953, 192, 'Mudug', 'MU', '', '', 0, 1),
(2954, 192, 'Nugaal', 'NU', '', '', 0, 1),
(2955, 192, 'Sanaag', 'SA', '', '', 0, 1),
(2956, 192, 'Shabeellaha Dhexe', 'SD', '', '', 0, 1),
(2957, 192, 'Shabeellaha Hoose', 'SH', '', '', 0, 1),
(2958, 192, 'Sool', 'SL', '', '', 0, 1),
(2959, 192, 'Togdheer', 'TO', '', '', 0, 1),
(2960, 192, 'Woqooyi Galbeed', 'WG', '', '', 0, 1),
(2961, 193, 'Eastern Cape', 'EC', '', '', 0, 1),
(2962, 193, 'Free State', 'FS', '', '', 0, 1),
(2963, 193, 'Gauteng', 'GT', '', '', 0, 1),
(2964, 193, 'KwaZulu-Natal', 'KN', '', '', 0, 1),
(2965, 193, 'Limpopo', 'LP', '', '', 0, 1),
(2966, 193, 'Mpumalanga', 'MP', '', '', 0, 1),
(2967, 193, 'North West', 'NW', '', '', 0, 1),
(2968, 193, 'Northern Cape', 'NC', '', '', 0, 1),
(2969, 193, 'Western Cape', 'WC', '', '', 0, 1),
(2970, 195, 'La Coru&ntilde;a', 'CA', '', '', 0, 1),
(2971, 195, 'Aacutelava', 'AL', '', '', 0, 1),
(2972, 195, 'Albacete', 'AB', '', '', 0, 1),
(2973, 195, 'Alicante', 'AC', '', '', 0, 1),
(2974, 195, 'Almeria', 'AM', '', '', 0, 1),
(2975, 195, 'Asturias', 'AS', '', '', 0, 1),
(2976, 195, 'Aacutevila', 'AV', '', '', 0, 1),
(2977, 195, 'Badajoz', 'BJ', '', '', 0, 1),
(2978, 195, 'Baleares', 'IB', '', '', 0, 1),
(2979, 195, 'Barcelona', 'BA', '', '', 0, 1),
(2980, 195, 'Burgos', 'BU', '', '', 0, 1),
(2981, 195, 'C&aacute;ceres', 'CC', '', '', 0, 1),
(2982, 195, 'C&aacute;diz', 'CZ', '', '', 0, 1),
(2983, 195, 'Cantabria', 'CT', '', '', 0, 1),
(2984, 195, 'Castell&oacute;n', 'CL', '', '', 0, 1),
(2985, 195, 'Ceuta', 'CE', '', '', 0, 1),
(2986, 195, 'Ciudad Real', 'CR', '', '', 0, 1),
(2987, 195, 'C&oacute;rdoba', 'CD', '', '', 0, 1),
(2988, 195, 'Cuenca', 'CU', '', '', 0, 1),
(2989, 195, 'Girona', 'GI', '', '', 0, 1),
(2990, 195, 'Granada', 'GD', '', '', 0, 1),
(2991, 195, 'Guadalajara', 'GJ', '', '', 0, 1),
(2992, 195, 'Guip&uacute;zcoa', 'GP', '', '', 0, 1),
(2993, 195, 'Huelva', 'HL', '', '', 0, 1),
(2994, 195, 'Huesca', 'HS', '', '', 0, 1),
(2995, 195, 'Ja&eacute;n', 'JN', '', '', 0, 1),
(2996, 195, 'La Rioja', 'RJ', '', '', 0, 1),
(2997, 195, 'Las Palmas', 'PM', '', '', 0, 1),
(2998, 195, 'Leon', 'LE', '', '', 0, 1),
(2999, 195, 'Lleida', 'LL', '', '', 0, 1),
(3000, 195, 'Lugo', 'LG', '', '', 0, 1),
(3001, 195, 'Madrid', 'MD', '', '', 0, 1),
(3002, 195, 'Malaga', 'MA', '', '', 0, 1),
(3003, 195, 'Melilla', 'ML', '', '', 0, 1),
(3004, 195, 'Murcia', 'MU', '', '', 0, 1),
(3005, 195, 'Navarra', 'NV', '', '', 0, 1),
(3006, 195, 'Ourense', 'OU', '', '', 0, 1),
(3007, 195, 'Palencia', 'PL', '', '', 0, 1),
(3008, 195, 'Pontevedra', 'PO', '', '', 0, 1),
(3009, 195, 'Salamanca', 'SL', '', '', 0, 1),
(3010, 195, 'Santa Cruz de Tenerife', 'SC', '', '', 0, 1),
(3011, 195, 'Segovia', 'SG', '', '', 0, 1),
(3012, 195, 'Sevilla', 'SV', '', '', 0, 1),
(3013, 195, 'Soria', 'SO', '', '', 0, 1),
(3014, 195, 'Tarragona', 'TA', '', '', 0, 1),
(3015, 195, 'Teruel', 'TE', '', '', 0, 1),
(3016, 195, 'Toledo', 'TO', '', '', 0, 1),
(3017, 195, 'Valencia', 'VC', '', '', 0, 1),
(3018, 195, 'Valladolid', 'VD', '', '', 0, 1),
(3019, 195, 'Vizcaya', 'VZ', '', '', 0, 1),
(3020, 195, 'Zamora', 'ZM', '', '', 0, 1),
(3021, 195, 'Zaragoza', 'ZR', '', '', 0, 1),
(3022, 196, 'Central', 'CE', '', '', 0, 1),
(3023, 196, 'Eastern', 'EA', '', '', 0, 1),
(3024, 196, 'North Central', 'NC', '', '', 0, 1),
(3025, 196, 'Northern', 'NO', '', '', 0, 1),
(3026, 196, 'North Western', 'NW', '', '', 0, 1),
(3027, 196, 'Sabaragamuwa', 'SA', '', '', 0, 1),
(3028, 196, 'Southern', 'SO', '', '', 0, 1),
(3029, 196, 'Uva', 'UV', '', '', 0, 1),
(3030, 196, 'Western', 'WE', '', '', 0, 1),
(3032, 197, 'Saint Helena', 'S', '', '', 0, 1),
(3034, 199, 'Aali an Nil', 'ANL', '', '', 0, 1),
(3035, 199, 'Al Bahr al Ahmar', 'BAM', '', '', 0, 1),
(3036, 199, 'Al Buhayrat', 'BRT', '', '', 0, 1),
(3037, 199, 'Al Jazirah', 'JZR', '', '', 0, 1),
(3038, 199, 'Al Khartum', 'KRT', '', '', 0, 1),
(3039, 199, 'Al Qadarif', 'QDR', '', '', 0, 1),
(3040, 199, 'Al Wahdah', 'WDH', '', '', 0, 1),
(3041, 199, 'An Nil al Abyad', 'ANB', '', '', 0, 1),
(3042, 199, 'An Nil al Azraq', 'ANZ', '', '', 0, 1),
(3043, 199, 'Ash Shamaliyah', 'ASH', '', '', 0, 1),
(3044, 199, 'Bahr al Jabal', 'BJA', '', '', 0, 1),
(3045, 199, 'Gharb al Istiwa''iyah', 'GIS', '', '', 0, 1),
(3046, 199, 'Gharb Bahr al Ghazal', 'GBG', '', '', 0, 1),
(3047, 199, 'Gharb Darfur', 'GDA', '', '', 0, 1),
(3048, 199, 'Gharb Kurdufan', 'GKU', '', '', 0, 1),
(3049, 199, 'Janub Darfur', 'JDA', '', '', 0, 1),
(3050, 199, 'Janub Kurdufan', 'JKU', '', '', 0, 1),
(3051, 199, 'Junqali', 'JQL', '', '', 0, 1),
(3052, 199, 'Kassala', 'KSL', '', '', 0, 1),
(3053, 199, 'Nahr an Nil', 'NNL', '', '', 0, 1),
(3054, 199, 'Shamal Bahr al Ghazal', 'SBG', '', '', 0, 1),
(3055, 199, 'Shamal Darfur', 'SDA', '', '', 0, 1),
(3056, 199, 'Shamal Kurdufan', 'SKU', '', '', 0, 1),
(3057, 199, 'Sharq al Istiwa''iyah', 'SIS', '', '', 0, 1),
(3058, 199, 'Sinnar', 'SNR', '', '', 0, 1),
(3059, 199, 'Warab', 'WRB', '', '', 0, 1),
(3060, 200, 'Brokopondo', 'BR', '', '', 0, 1),
(3061, 200, 'Commewijne', 'CM', '', '', 0, 1),
(3062, 200, 'Coronie', 'CR', '', '', 0, 1),
(3063, 200, 'Marowijne', 'MA', '', '', 0, 1),
(3064, 200, 'Nickerie', 'NI', '', '', 0, 1),
(3065, 200, 'Para', 'PA', '', '', 0, 1),
(3066, 200, 'Paramaribo', 'PM', '', '', 0, 1),
(3067, 200, 'Saramacca', 'SA', '', '', 0, 1),
(3068, 200, 'Sipaliwini', 'SI', '', '', 0, 1),
(3069, 200, 'Wanica', 'WA', '', '', 0, 1),
(3070, 202, 'Hhohho', 'H', '', '', 0, 1),
(3071, 202, 'Lubombo', 'L', '', '', 0, 1),
(3072, 202, 'Manzini', 'M', '', '', 0, 1),
(3073, 202, 'Shishelweni', 'S', '', '', 0, 1),
(3074, 203, 'Blekinge', 'K', '', '', 0, 1),
(3075, 203, 'Dalarna', 'W', '', '', 0, 1),
(3076, 203, 'G&auml;vleborg', 'X', '', '', 0, 1),
(3077, 203, 'Gotland', 'I', '', '', 0, 1),
(3078, 203, 'Halland', 'N', '', '', 0, 1),
(3079, 203, 'J&auml;mtland', 'Z', '', '', 0, 1),
(3080, 203, 'J&ouml;nk&ouml;ping', 'F', '', '', 0, 1),
(3081, 203, 'Kalmar', 'H', '', '', 0, 1),
(3082, 203, 'Kronoberg', 'G', '', '', 0, 1),
(3083, 203, 'Norrbotten', 'BD', '', '', 0, 1),
(3084, 203, 'Oumlrebro', 'T', '', '', 0, 1),
(3085, 203, 'Oumlstergoumltland', 'E', '', '', 0, 1),
(3086, 203, 'Sk&aring;ne', 'M', '', '', 0, 1),
(3087, 203, 'S&ouml;dermanland', 'D', '', '', 0, 1),
(3088, 203, 'Stockholm', 'AB', '', '', 0, 1),
(3089, 203, 'Uppsala', 'C', '', '', 0, 1),
(3090, 203, 'V&auml;rmland', 'S', '', '', 0, 1),
(3091, 203, 'V&auml;sterbotten', 'AC', '', '', 0, 1),
(3092, 203, 'V&auml;sternorrland', 'Y', '', '', 0, 1),
(3093, 203, 'V&auml;stmanland', 'U', '', '', 0, 1),
(3094, 203, 'V&auml;stra G&ouml;taland', 'O', '', '', 0, 1),
(3095, 204, 'Aargau', 'AG', '', '', 0, 1),
(3096, 204, 'Appenzell Ausserrhoden', 'AR', '', '', 0, 1),
(3097, 204, 'Appenzell Innerrhoden', 'AI', '', '', 0, 1),
(3098, 204, 'Basel-Stadt', 'BS', '', '', 0, 1),
(3099, 204, 'Basel-Landschaft', 'BL', '', '', 0, 1),
(3100, 204, 'Bern', 'BE', '', '', 0, 1),
(3101, 204, 'Fribourg', 'FR', '', '', 0, 1),
(3102, 204, 'Gen&egrave;ve', 'GE', '', '', 0, 1),
(3103, 204, 'Glarus', 'GL', '', '', 0, 1),
(3104, 204, 'Graub&uuml;nden', 'GR', '', '', 0, 1),
(3105, 204, 'Jura', 'JU', '', '', 0, 1),
(3106, 204, 'Luzern', 'LU', '', '', 0, 1),
(3107, 204, 'Neuch&acirc;tel', 'NE', '', '', 0, 1),
(3108, 204, 'Nidwald', 'NW', '', '', 0, 1),
(3109, 204, 'Obwald', 'OW', '', '', 0, 1),
(3110, 204, 'St. Gallen', 'SG', '', '', 0, 1),
(3111, 204, 'Schaffhausen', 'SH', '', '', 0, 1),
(3112, 204, 'Schwyz', 'SZ', '', '', 0, 1),
(3113, 204, 'Solothurn', 'SO', '', '', 0, 1),
(3114, 204, 'Thurgau', 'TG', '', '', 0, 1),
(3115, 204, 'Ticino', 'TI', '', '', 0, 1),
(3116, 204, 'Uri', 'UR', '', '', 0, 1),
(3117, 204, 'Valais', 'VS', '', '', 0, 1),
(3118, 204, 'Vaud', 'VD', '', '', 0, 1),
(3119, 204, 'Zug', 'ZG', '', '', 0, 1),
(3120, 204, 'Z&uuml;rich', 'ZH', '', '', 0, 1),
(3121, 205, 'Al Hasakah', 'HA', '', '', 0, 1),
(3122, 205, 'Al Ladhiqiyah', 'LA', '', '', 0, 1),
(3123, 205, 'Al Qunaytirah', 'QU', '', '', 0, 1),
(3124, 205, 'Ar Raqqah', 'RQ', '', '', 0, 1),
(3125, 205, 'As Suwayda', 'SU', '', '', 0, 1),
(3126, 205, 'Dara', 'DA', '', '', 0, 1),
(3127, 205, 'Dayr az Zawr', 'DZ', '', '', 0, 1),
(3128, 205, 'Dimashq', 'DI', '', '', 0, 1),
(3129, 205, 'Halab', 'HL', '', '', 0, 1),
(3130, 205, 'Hamah', 'HM', '', '', 0, 1),
(3131, 205, 'Hims', 'HI', '', '', 0, 1),
(3132, 205, 'Idlib', 'ID', '', '', 0, 1),
(3133, 205, 'Rif Dimashq', 'RD', '', '', 0, 1),
(3134, 205, 'Tartus', 'TA', '', '', 0, 1),
(3135, 206, 'Chang-hua', 'CH', '', '', 0, 1),
(3136, 206, 'Chia-i', 'CI', '', '', 0, 1),
(3137, 206, 'Hsin-chu', 'HS', '', '', 0, 1),
(3138, 206, 'Hua-lien', 'HL', '', '', 0, 1),
(3139, 206, 'I-lan', 'IL', '', '', 0, 1),
(3140, 206, 'Kao-hsiung county', 'KH', '', '', 0, 1),
(3141, 206, 'Kin-men', 'KM', '', '', 0, 1),
(3142, 206, 'Lien-chiang', 'LC', '', '', 0, 1),
(3143, 206, 'Miao-li', 'ML', '', '', 0, 1),
(3144, 206, 'Nan-t''ou', 'NT', '', '', 0, 1),
(3145, 206, 'P''eng-hu', 'PH', '', '', 0, 1),
(3146, 206, 'P''ing-tung', 'PT', '', '', 0, 1),
(3147, 206, 'T''ai-chung', 'TG', '', '', 0, 1),
(3148, 206, 'T''ai-nan', 'TA', '', '', 0, 1),
(3149, 206, 'T''ai-pei county', 'TP', '', '', 0, 1),
(3150, 206, 'T''ai-tung', 'TT', '', '', 0, 1),
(3151, 206, 'T''ao-yuan', 'TY', '', '', 0, 1),
(3152, 206, 'Yun-lin', 'YL', '', '', 0, 1),
(3153, 206, 'Chia-i city', 'CC', '', '', 0, 1),
(3154, 206, 'Chi-lung', 'CL', '', '', 0, 1),
(3155, 206, 'Hsin-chu', 'HC', '', '', 0, 1),
(3156, 206, 'T''ai-chung', 'TH', '', '', 0, 1),
(3157, 206, 'T''ai-nan', 'TN', '', '', 0, 1),
(3158, 206, 'Kao-hsiung city', 'KC', '', '', 0, 1),
(3159, 206, 'T''ai-pei city', 'TC', '', '', 0, 1),
(3160, 207, 'Gorno-Badakhstan', 'GB', '', '', 0, 1),
(3161, 207, 'Khatlon', 'KT', '', '', 0, 1),
(3162, 207, 'Sughd', 'SU', '', '', 0, 1),
(3163, 208, 'Arusha', 'AR', '', '', 0, 1),
(3164, 208, 'Dar es Salaam', 'DS', '', '', 0, 1),
(3165, 208, 'Dodoma', 'DO', '', '', 0, 1),
(3166, 208, 'Iringa', 'IR', '', '', 0, 1),
(3167, 208, 'Kagera', 'KA', '', '', 0, 1),
(3168, 208, 'Kigoma', 'KI', '', '', 0, 1),
(3169, 208, 'Kilimanjaro', 'KJ', '', '', 0, 1),
(3170, 208, 'Lindi', 'LN', '', '', 0, 1),
(3171, 208, 'Manyara', 'MY', '', '', 0, 1),
(3172, 208, 'Mara', 'MR', '', '', 0, 1),
(3173, 208, 'Mbeya', 'MB', '', '', 0, 1),
(3174, 208, 'Morogoro', 'MO', '', '', 0, 1),
(3175, 208, 'Mtwara', 'MT', '', '', 0, 1),
(3176, 208, 'Mwanza', 'MW', '', '', 0, 1),
(3177, 208, 'Pemba North', 'PN', '', '', 0, 1),
(3178, 208, 'Pemba South', 'PS', '', '', 0, 1),
(3179, 208, 'Pwani', 'PW', '', '', 0, 1),
(3180, 208, 'Rukwa', 'RK', '', '', 0, 1),
(3181, 208, 'Ruvuma', 'RV', '', '', 0, 1),
(3182, 208, 'Shinyanga', 'SH', '', '', 0, 1),
(3183, 208, 'Singida', 'SI', '', '', 0, 1),
(3184, 208, 'Tabora', 'TB', '', '', 0, 1),
(3185, 208, 'Tanga', 'TN', '', '', 0, 1),
(3186, 208, 'Zanzibar Central/South', 'ZC', '', '', 0, 1),
(3187, 208, 'Zanzibar North', 'ZN', '', '', 0, 1),
(3188, 208, 'Zanzibar Urban/West', 'ZU', '', '', 0, 1),
(3189, 209, 'Amnat Charoen', 'Amnat Charoen', '', '', 0, 1),
(3190, 209, 'Ang Thong', 'Ang Thong', '', '', 0, 1),
(3191, 209, 'Ayutthaya', 'Ayutthaya', '', '', 0, 1),
(3192, 209, 'Bangkok', 'Bangkok', '', '', 0, 1),
(3193, 209, 'Buriram', 'Buriram', '', '', 0, 1),
(3194, 209, 'Chachoengsao', 'Chachoengsao', '', '', 0, 1),
(3195, 209, 'Chai Nat', 'Chai Nat', '', '', 0, 1),
(3196, 209, 'Chaiyaphum', 'Chaiyaphum', '', '', 0, 1),
(3197, 209, 'Chanthaburi', 'Chanthaburi', '', '', 0, 1),
(3198, 209, 'Chiang Mai', 'Chiang Mai', '', '', 0, 1),
(3199, 209, 'Chiang Rai', 'Chiang Rai', '', '', 0, 1),
(3200, 209, 'Chon Buri', 'Chon Buri', '', '', 0, 1),
(3201, 209, 'Chumphon', 'Chumphon', '', '', 0, 1),
(3202, 209, 'Kalasin', 'Kalasin', '', '', 0, 1),
(3203, 209, 'Kamphaeng Phet', 'Kamphaeng Phet', '', '', 0, 1),
(3204, 209, 'Kanchanaburi', 'Kanchanaburi', '', '', 0, 1),
(3205, 209, 'Khon Kaen', 'Khon Kaen', '', '', 0, 1),
(3206, 209, 'Krabi', 'Krabi', '', '', 0, 1),
(3207, 209, 'Lampang', 'Lampang', '', '', 0, 1),
(3208, 209, 'Lamphun', 'Lamphun', '', '', 0, 1),
(3209, 209, 'Loei', 'Loei', '', '', 0, 1),
(3210, 209, 'Lop Buri', 'Lop Buri', '', '', 0, 1),
(3211, 209, 'Mae Hong Son', 'Mae Hong Son', '', '', 0, 1),
(3212, 209, 'Maha Sarakham', 'Maha Sarakham', '', '', 0, 1),
(3213, 209, 'Mukdahan', 'Mukdahan', '', '', 0, 1),
(3214, 209, 'Nakhon Nayok', 'Nakhon Nayok', '', '', 0, 1),
(3215, 209, 'Nakhon Pathom', 'Nakhon Pathom', '', '', 0, 1),
(3216, 209, 'Nakhon Phanom', 'Nakhon Phanom', '', '', 0, 1),
(3217, 209, 'Nakhon Ratchasima', 'Nakhon Ratchasima', '', '', 0, 1),
(3218, 209, 'Nakhon Sawan', 'Nakhon Sawan', '', '', 0, 1),
(3219, 209, 'Nakhon Si Thammarat', 'Nakhon Si Thammarat', '', '', 0, 1),
(3220, 209, 'Nan', 'Nan', '', '', 0, 1),
(3221, 209, 'Narathiwat', 'Narathiwat', '', '', 0, 1),
(3222, 209, 'Nong Bua Lamphu', 'Nong Bua Lamphu', '', '', 0, 1),
(3223, 209, 'Nong Khai', 'Nong Khai', '', '', 0, 1),
(3224, 209, 'Nonthaburi', 'Nonthaburi', '', '', 0, 1),
(3225, 209, 'Pathum Thani', 'Pathum Thani', '', '', 0, 1),
(3226, 209, 'Pattani', 'Pattani', '', '', 0, 1),
(3227, 209, 'Phangnga', 'Phangnga', '', '', 0, 1),
(3228, 209, 'Phatthalung', 'Phatthalung', '', '', 0, 1),
(3229, 209, 'Phayao', 'Phayao', '', '', 0, 1),
(3230, 209, 'Phetchabun', 'Phetchabun', '', '', 0, 1),
(3231, 209, 'Phetchaburi', 'Phetchaburi', '', '', 0, 1),
(3232, 209, 'Phichit', 'Phichit', '', '', 0, 1),
(3233, 209, 'Phitsanulok', 'Phitsanulok', '', '', 0, 1),
(3234, 209, 'Phrae', 'Phrae', '', '', 0, 1),
(3235, 209, 'Phuket', 'Phuket', '', '', 0, 1),
(3236, 209, 'Prachin Buri', 'Prachin Buri', '', '', 0, 1),
(3237, 209, 'Prachuap Khiri Khan', 'Prachuap Khiri Khan', '', '', 0, 1),
(3238, 209, 'Ranong', 'Ranong', '', '', 0, 1),
(3239, 209, 'Ratchaburi', 'Ratchaburi', '', '', 0, 1),
(3240, 209, 'Rayong', 'Rayong', '', '', 0, 1),
(3241, 209, 'Roi Et', 'Roi Et', '', '', 0, 1),
(3242, 209, 'Sa Kaeo', 'Sa Kaeo', '', '', 0, 1),
(3243, 209, 'Sakon Nakhon', 'Sakon Nakhon', '', '', 0, 1),
(3244, 209, 'Samut Prakan', 'Samut Prakan', '', '', 0, 1),
(3245, 209, 'Samut Sakhon', 'Samut Sakhon', '', '', 0, 1),
(3246, 209, 'Samut Songkhram', 'Samut Songkhram', '', '', 0, 1),
(3247, 209, 'Sara Buri', 'Sara Buri', '', '', 0, 1),
(3248, 209, 'Satun', 'Satun', '', '', 0, 1),
(3249, 209, 'Sing Buri', 'Sing Buri', '', '', 0, 1),
(3250, 209, 'Sisaket', 'Sisaket', '', '', 0, 1),
(3251, 209, 'Songkhla', 'Songkhla', '', '', 0, 1),
(3252, 209, 'Sukhothai', 'Sukhothai', '', '', 0, 1),
(3253, 209, 'Suphan Buri', 'Suphan Buri', '', '', 0, 1),
(3254, 209, 'Surat Thani', 'Surat Thani', '', '', 0, 1),
(3255, 209, 'Surin', 'Surin', '', '', 0, 1),
(3256, 209, 'Tak', 'Tak', '', '', 0, 1),
(3257, 209, 'Trang', 'Trang', '', '', 0, 1),
(3258, 209, 'Trat', 'Trat', '', '', 0, 1),
(3259, 209, 'Ubon Ratchathani', 'Ubon Ratchathani', '', '', 0, 1),
(3260, 209, 'Udon Thani', 'Udon Thani', '', '', 0, 1),
(3261, 209, 'Uthai Thani', 'Uthai Thani', '', '', 0, 1),
(3262, 209, 'Uttaradit', 'Uttaradit', '', '', 0, 1),
(3263, 209, 'Yala', 'Yala', '', '', 0, 1),
(3264, 209, 'Yasothon', 'Yasothon', '', '', 0, 1),
(3265, 210, 'Kara', 'K', '', '', 0, 1),
(3266, 210, 'Plateaux', 'P', '', '', 0, 1),
(3267, 210, 'Savanes', 'S', '', '', 0, 1),
(3268, 210, 'Centrale', 'C', '', '', 0, 1),
(3269, 210, 'Maritime', 'M', '', '', 0, 1),
(3270, 211, 'Atafu', 'A', '', '', 0, 1),
(3271, 211, 'Fakaofo', 'F', '', '', 0, 1),
(3272, 211, 'Nukunonu', 'N', '', '', 0, 1),
(3273, 212, 'Ha''apai', 'H', '', '', 0, 1),
(3274, 212, 'Tongatapu', 'T', '', '', 0, 1),
(3275, 212, 'Vava''u', 'V', '', '', 0, 1),
(3276, 213, 'Couva/Tabaquite/Talparo', 'CT', '', '', 0, 1),
(3277, 213, 'Diego Martin', 'DM', '', '', 0, 1),
(3278, 213, 'Mayaro/Rio Claro', 'MR', '', '', 0, 1),
(3279, 213, 'Penal/Debe', 'PD', '', '', 0, 1),
(3280, 213, 'Princes Town', 'PT', '', '', 0, 1),
(3281, 213, 'Sangre Grande', 'SG', '', '', 0, 1),
(3282, 213, 'San Juan/Laventille', 'SL', '', '', 0, 1),
(3283, 213, 'Siparia', 'SI', '', '', 0, 1),
(3284, 213, 'Tunapuna/Piarco', 'TP', '', '', 0, 1),
(3285, 213, 'Port of Spain', 'PS', '', '', 0, 1),
(3286, 213, 'San Fernando', 'SF', '', '', 0, 1),
(3287, 213, 'Arima', 'AR', '', '', 0, 1),
(3288, 213, 'Point Fortin', 'PF', '', '', 0, 1),
(3289, 213, 'Chaguanas', 'CH', '', '', 0, 1),
(3290, 213, 'Tobago', 'TO', '', '', 0, 1),
(3291, 214, 'Ariana', 'AR', '', '', 0, 1),
(3292, 214, 'Beja', 'BJ', '', '', 0, 1),
(3293, 214, 'Ben Arous', 'BA', '', '', 0, 1),
(3294, 214, 'Bizerte', 'BI', '', '', 0, 1),
(3295, 214, 'Gabes', 'GB', '', '', 0, 1),
(3296, 214, 'Gafsa', 'GF', '', '', 0, 1),
(3297, 214, 'Jendouba', 'JE', '', '', 0, 1),
(3298, 214, 'Kairouan', 'KR', '', '', 0, 1),
(3299, 214, 'Kasserine', 'KS', '', '', 0, 1),
(3300, 214, 'Kebili', 'KB', '', '', 0, 1),
(3301, 214, 'Kef', 'KF', '', '', 0, 1),
(3302, 214, 'Mahdia', 'MH', '', '', 0, 1),
(3303, 214, 'Manouba', 'MN', '', '', 0, 1),
(3304, 214, 'Medenine', 'ME', '', '', 0, 1),
(3305, 214, 'Monastir', 'MO', '', '', 0, 1),
(3306, 214, 'Nabeul', 'NA', '', '', 0, 1),
(3307, 214, 'Sfax', 'SF', '', '', 0, 1),
(3308, 214, 'Sidi', 'SD', '', '', 0, 1),
(3309, 214, 'Siliana', 'SL', '', '', 0, 1),
(3310, 214, 'Sousse', 'SO', '', '', 0, 1),
(3311, 214, 'Tataouine', 'TA', '', '', 0, 1),
(3312, 214, 'Tozeur', 'TO', '', '', 0, 1),
(3313, 214, 'Tunis', 'TU', '', '', 0, 1),
(3314, 214, 'Zaghouan', 'ZA', '', '', 0, 1),
(3315, 215, 'Adana', 'ADA', '', '', 0, 1),
(3316, 215, 'Adıyaman', 'ADI', '', '', 0, 1),
(3317, 215, 'Afyonkarahisar', 'AFY', '', '', 0, 1),
(3318, 215, 'Ağrı', 'AGR', '', '', 0, 1),
(3319, 215, 'Aksaray', 'AKS', '', '', 0, 1),
(3320, 215, 'Amasya', 'AMA', '', '', 0, 1),
(3321, 215, 'Ankara', 'ANK', '', '', 0, 1),
(3322, 215, 'Antalya', 'ANT', '', '', 0, 1),
(3323, 215, 'Ardahan', 'ARD', '', '', 0, 1),
(3324, 215, 'Artvin', 'ART', '', '', 0, 1),
(3325, 215, 'Aydın', 'AYI', '', '', 0, 1),
(3326, 215, 'Balıkesir', 'BAL', '', '', 0, 1),
(3327, 215, 'Bartın', 'BAR', '', '', 0, 1),
(3328, 215, 'Batman', 'BAT', '', '', 0, 1),
(3329, 215, 'Bayburt', 'BAY', '', '', 0, 1),
(3330, 215, 'Bilecik', 'BIL', '', '', 0, 1),
(3331, 215, 'Bingöl', 'BIN', '', '', 0, 1),
(3332, 215, 'Bitlis', 'BIT', '', '', 0, 1),
(3333, 215, 'Bolu', 'BOL', '', '', 0, 1),
(3334, 215, 'Burdur', 'BRD', '', '', 0, 1),
(3335, 215, 'Bursa', 'BRS', '', '', 0, 1),
(3336, 215, 'Çanakkale', 'CKL', '', '', 0, 1),
(3337, 215, 'Çankırı', 'CKR', '', '', 0, 1),
(3338, 215, 'Çorum', 'COR', '', '', 0, 1),
(3339, 215, 'Denizli', 'DEN', '', '', 0, 1),
(3340, 215, 'Diyarbakır', 'DIY', '', '', 0, 1),
(3341, 215, 'Düzce', 'DUZ', '', '', 0, 1),
(3342, 215, 'Edirne', 'EDI', '', '', 0, 1),
(3343, 215, 'Elazığ', 'ELA', '', '', 0, 1),
(3344, 215, 'Erzincan', 'EZC', '', '', 0, 1),
(3345, 215, 'Erzurum', 'EZR', '', '', 0, 1),
(3346, 215, 'Eskişehir', 'ESK', '', '', 0, 1),
(3347, 215, 'Gaziantep', 'GAZ', '', '', 0, 1),
(3348, 215, 'Giresun', 'GIR', '', '', 0, 1),
(3349, 215, 'Gümüşhane', 'GMS', '', '', 0, 1),
(3350, 215, 'Hakkari', 'HKR', '', '', 0, 1),
(3351, 215, 'Hatay', 'HTY', '', '', 0, 1),
(3352, 215, 'Iğdır', 'IGD', '', '', 0, 1),
(3353, 215, 'Isparta', 'ISP', '', '', 0, 1),
(3354, 215, 'İstanbul', 'IST', '', '', 0, 1),
(3355, 215, 'İzmir', 'IZM', '', '', 0, 1),
(3356, 215, 'Kahramanmaraş', 'KAH', '', '', 0, 1),
(3357, 215, 'Karabük', 'KRB', '', '', 0, 1),
(3358, 215, 'Karaman', 'KRM', '', '', 0, 1),
(3359, 215, 'Kars', 'KRS', '', '', 0, 1),
(3360, 215, 'Kastamonu', 'KAS', '', '', 0, 1),
(3361, 215, 'Kayseri', 'KAY', '', '', 0, 1),
(3362, 215, 'Kilis', 'KLS', '', '', 0, 1),
(3363, 215, 'Kırıkkale', 'KRK', '', '', 0, 1),
(3364, 215, 'Kırklareli', 'KLR', '', '', 0, 1),
(3365, 215, 'Kırşehir', 'KRH', '', '', 0, 1),
(3366, 215, 'Kocaeli', 'KOC', '', '', 0, 1),
(3367, 215, 'Konya', 'KON', '', '', 0, 1),
(3368, 215, 'Kütahya', 'KUT', '', '', 0, 1),
(3369, 215, 'Malatya', 'MAL', '', '', 0, 1),
(3370, 215, 'Manisa', 'MAN', '', '', 0, 1),
(3371, 215, 'Mardin', 'MAR', '', '', 0, 1),
(3372, 215, 'Mersin', 'MER', '', '', 0, 1),
(3373, 215, 'Muğla', 'MUG', '', '', 0, 1),
(3374, 215, 'Muş', 'MUS', '', '', 0, 1),
(3375, 215, 'Nevşehir', 'NEV', '', '', 0, 1),
(3376, 215, 'Niğde', 'NIG', '', '', 0, 1),
(3377, 215, 'Ordu', 'ORD', '', '', 0, 1),
(3378, 215, 'Osmaniye', 'OSM', '', '', 0, 1),
(3379, 215, 'Rize', 'RIZ', '', '', 0, 1),
(3380, 215, 'Sakarya', 'SAK', '', '', 0, 1),
(3381, 215, 'Samsun', 'SAM', '', '', 0, 1),
(3382, 215, 'Şanlıurfa', 'SAN', '', '', 0, 1),
(3383, 215, 'Siirt', 'SII', '', '', 0, 1),
(3384, 215, 'Sinop', 'SIN', '', '', 0, 1),
(3385, 215, 'Şırnak', 'SIR', '', '', 0, 1),
(3386, 215, 'Sivas', 'SIV', '', '', 0, 1),
(3387, 215, 'Tekirdağ', 'TEL', '', '', 0, 1),
(3388, 215, 'Tokat', 'TOK', '', '', 0, 1),
(3389, 215, 'Trabzon', 'TRA', '', '', 0, 1),
(3390, 215, 'Tunceli', 'TUN', '', '', 0, 1),
(3391, 215, 'Uşak', 'USK', '', '', 0, 1),
(3392, 215, 'Van', 'VAN', '', '', 0, 1),
(3393, 215, 'Yalova', 'YAL', '', '', 0, 1),
(3394, 215, 'Yozgat', 'YOZ', '', '', 0, 1),
(3395, 215, 'Zonguldak', 'ZON', '', '', 0, 1),
(3396, 216, 'Ahal Welayaty', 'A', '', '', 0, 1),
(3397, 216, 'Balkan Welayaty', 'B', '', '', 0, 1),
(3398, 216, 'Dashhowuz Welayaty', 'D', '', '', 0, 1),
(3399, 216, 'Lebap Welayaty', 'L', '', '', 0, 1),
(3400, 216, 'Mary Welayaty', 'M', '', '', 0, 1),
(3401, 217, 'Ambergris Cays', 'AC', '', '', 0, 1),
(3402, 217, 'Dellis Cay', 'DC', '', '', 0, 1),
(3403, 217, 'French Cay', 'FC', '', '', 0, 1),
(3404, 217, 'Little Water Cay', 'LW', '', '', 0, 1),
(3405, 217, 'Parrot Cay', 'RC', '', '', 0, 1),
(3406, 217, 'Pine Cay', 'PN', '', '', 0, 1),
(3407, 217, 'Salt Cay', 'SL', '', '', 0, 1),
(3408, 217, 'Grand Turk', 'GT', '', '', 0, 1),
(3409, 217, 'South Caicos', 'SC', '', '', 0, 1),
(3410, 217, 'East Caicos', 'EC', '', '', 0, 1),
(3411, 217, 'Middle Caicos', 'MC', '', '', 0, 1),
(3412, 217, 'North Caicos', 'NC', '', '', 0, 1),
(3413, 217, 'Providenciales', 'PR', '', '', 0, 1),
(3414, 217, 'West Caicos', 'WC', '', '', 0, 1),
(3415, 218, 'Nanumanga', 'NMG', '', '', 0, 1),
(3416, 218, 'Niulakita', 'NLK', '', '', 0, 1),
(3417, 218, 'Niutao', 'NTO', '', '', 0, 1),
(3418, 218, 'Funafuti', 'FUN', '', '', 0, 1),
(3419, 218, 'Nanumea', 'NME', '', '', 0, 1),
(3420, 218, 'Nui', 'NUI', '', '', 0, 1),
(3421, 218, 'Nukufetau', 'NFT', '', '', 0, 1),
(3422, 218, 'Nukulaelae', 'NLL', '', '', 0, 1),
(3423, 218, 'Vaitupu', 'VAI', '', '', 0, 1),
(3424, 219, 'Kalangala', 'KAL', '', '', 0, 1),
(3425, 219, 'Kampala', 'KMP', '', '', 0, 1),
(3426, 219, 'Kayunga', 'KAY', '', '', 0, 1),
(3427, 219, 'Kiboga', 'KIB', '', '', 0, 1),
(3428, 219, 'Luwero', 'LUW', '', '', 0, 1),
(3429, 219, 'Masaka', 'MAS', '', '', 0, 1),
(3430, 219, 'Mpigi', 'MPI', '', '', 0, 1),
(3431, 219, 'Mubende', 'MUB', '', '', 0, 1),
(3432, 219, 'Mukono', 'MUK', '', '', 0, 1),
(3433, 219, 'Nakasongola', 'NKS', '', '', 0, 1),
(3434, 219, 'Rakai', 'RAK', '', '', 0, 1),
(3435, 219, 'Sembabule', 'SEM', '', '', 0, 1),
(3436, 219, 'Wakiso', 'WAK', '', '', 0, 1),
(3437, 219, 'Bugiri', 'BUG', '', '', 0, 1),
(3438, 219, 'Busia', 'BUS', '', '', 0, 1),
(3439, 219, 'Iganga', 'IGA', '', '', 0, 1),
(3440, 219, 'Jinja', 'JIN', '', '', 0, 1),
(3441, 219, 'Kaberamaido', 'KAB', '', '', 0, 1),
(3442, 219, 'Kamuli', 'KML', '', '', 0, 1),
(3443, 219, 'Kapchorwa', 'KPC', '', '', 0, 1),
(3444, 219, 'Katakwi', 'KTK', '', '', 0, 1),
(3445, 219, 'Kumi', 'KUM', '', '', 0, 1),
(3446, 219, 'Mayuge', 'MAY', '', '', 0, 1),
(3447, 219, 'Mbale', 'MBA', '', '', 0, 1),
(3448, 219, 'Pallisa', 'PAL', '', '', 0, 1),
(3449, 219, 'Sironko', 'SIR', '', '', 0, 1),
(3450, 219, 'Soroti', 'SOR', '', '', 0, 1),
(3451, 219, 'Tororo', 'TOR', '', '', 0, 1),
(3452, 219, 'Adjumani', 'ADJ', '', '', 0, 1),
(3453, 219, 'Apac', 'APC', '', '', 0, 1),
(3454, 219, 'Arua', 'ARU', '', '', 0, 1),
(3455, 219, 'Gulu', 'GUL', '', '', 0, 1),
(3456, 219, 'Kitgum', 'KIT', '', '', 0, 1),
(3457, 219, 'Kotido', 'KOT', '', '', 0, 1),
(3458, 219, 'Lira', 'LIR', '', '', 0, 1),
(3459, 219, 'Moroto', 'MRT', '', '', 0, 1),
(3460, 219, 'Moyo', 'MOY', '', '', 0, 1),
(3461, 219, 'Nakapiripirit', 'NAK', '', '', 0, 1),
(3462, 219, 'Nebbi', 'NEB', '', '', 0, 1),
(3463, 219, 'Pader', 'PAD', '', '', 0, 1),
(3464, 219, 'Yumbe', 'YUM', '', '', 0, 1),
(3465, 219, 'Bundibugyo', 'BUN', '', '', 0, 1),
(3466, 219, 'Bushenyi', 'BSH', '', '', 0, 1),
(3467, 219, 'Hoima', 'HOI', '', '', 0, 1),
(3468, 219, 'Kabale', 'KBL', '', '', 0, 1),
(3469, 219, 'Kabarole', 'KAR', '', '', 0, 1),
(3470, 219, 'Kamwenge', 'KAM', '', '', 0, 1),
(3471, 219, 'Kanungu', 'KAN', '', '', 0, 1),
(3472, 219, 'Kasese', 'KAS', '', '', 0, 1),
(3473, 219, 'Kibaale', 'KBA', '', '', 0, 1),
(3474, 219, 'Kisoro', 'KIS', '', '', 0, 1),
(3475, 219, 'Kyenjojo', 'KYE', '', '', 0, 1),
(3476, 219, 'Masindi', 'MSN', '', '', 0, 1),
(3477, 219, 'Mbarara', 'MBR', '', '', 0, 1),
(3478, 219, 'Ntungamo', 'NTU', '', '', 0, 1),
(3479, 219, 'Rukungiri', 'RUK', '', '', 0, 1),
(3480, 220, 'Cherkas''ka Oblast''', '71', '', '', 0, 1),
(3481, 220, 'Chernihivs''ka Oblast''', '74', '', '', 0, 1),
(3482, 220, 'Chernivets''ka Oblast''', '77', '', '', 0, 1),
(3483, 220, 'Crimea', '43', '', '', 0, 1),
(3484, 220, 'Dnipropetrovs''ka Oblast''', '12', '', '', 0, 1),
(3485, 220, 'Donets''ka Oblast''', '14', '', '', 0, 1),
(3486, 220, 'Ivano-Frankivs''ka Oblast''', '26', '', '', 0, 1),
(3487, 220, 'Khersons''ka Oblast''', '65', '', '', 0, 1),
(3488, 220, 'Khmel''nyts''ka Oblast''', '68', '', '', 0, 1),
(3489, 220, 'Kirovohrads''ka Oblast''', '35', '', '', 0, 1),
(3490, 220, 'Kyiv', '30', '', '', 0, 1);
INSERT INTO `sma_state` (`state_id`, `country_id`, `state_name`, `state_url`, `state_latitude`, `state_longitude`, `default`, `state_status`) VALUES
(3491, 220, 'Kyivs''ka Oblast''', '32', '', '', 0, 1),
(3492, 220, 'Luhans''ka Oblast''', '09', '', '', 0, 1),
(3493, 220, 'L''vivs''ka Oblast''', '46', '', '', 0, 1),
(3494, 220, 'Mykolayivs''ka Oblast''', '48', '', '', 0, 1),
(3495, 220, 'Odes''ka Oblast''', '51', '', '', 0, 1),
(3496, 220, 'Poltavs''ka Oblast''', '53', '', '', 0, 1),
(3497, 220, 'Rivnens''ka Oblast''', '56', '', '', 0, 1),
(3498, 220, 'Sevastopol''', '40', '', '', 0, 1),
(3499, 220, 'Sums''ka Oblast''', '59', '', '', 0, 1),
(3500, 220, 'Ternopil''s''ka Oblast''', '61', '', '', 0, 1),
(3501, 220, 'Vinnyts''ka Oblast''', '05', '', '', 0, 1),
(3502, 220, 'Volyns''ka Oblast''', '07', '', '', 0, 1),
(3503, 220, 'Zakarpats''ka Oblast''', '21', '', '', 0, 1),
(3504, 220, 'Zaporiz''ka Oblast''', '23', '', '', 0, 1),
(3505, 220, 'Zhytomyrs''ka oblast''', '18', '', '', 0, 1),
(3506, 221, 'Abu Zaby', 'AZ', '', '', 0, 1),
(3507, 221, 'Ajman', 'AJ', '', '', 0, 1),
(3508, 221, 'Al Fujayrah', 'FU', '', '', 0, 1),
(3509, 221, 'Ash Shariqah', 'SH', '', '', 0, 1),
(3510, 221, 'Dubayy', 'DU', '', '', 0, 1),
(3511, 221, 'R''as al Khaymah', 'RK', '', '', 0, 1),
(3512, 221, 'Umm al Qaywayn', 'UQ', '', '', 0, 1),
(3513, 222, 'Aberdeen', 'ABN', '', '', 0, 1),
(3514, 222, 'Aberdeenshire', 'ABNS', '', '', 0, 1),
(3515, 222, 'Anglesey', 'ANG', '', '', 0, 1),
(3516, 222, 'Angus', 'AGS', '', '', 0, 1),
(3517, 222, 'Argyll and Bute', 'ARY', '', '', 0, 1),
(3518, 222, 'Bedfordshire', 'BEDS', '', '', 0, 1),
(3519, 222, 'Berkshire', 'BERKS', '', '', 0, 1),
(3520, 222, 'Blaenau Gwent', 'BLA', '', '', 0, 1),
(3521, 222, 'Bridgend', 'BRI', '', '', 0, 1),
(3522, 222, 'Bristol', 'BSTL', '', '', 0, 1),
(3523, 222, 'Buckinghamshire', 'BUCKS', '', '', 0, 1),
(3524, 222, 'Caerphilly', 'CAE', '', '', 0, 1),
(3525, 222, 'Cambridgeshire', 'CAMBS', '', '', 0, 1),
(3526, 222, 'Cardiff', 'CDF', '', '', 0, 1),
(3527, 222, 'Carmarthenshire', 'CARM', '', '', 0, 1),
(3528, 222, 'Ceredigion', 'CDGN', '', '', 0, 1),
(3529, 222, 'Cheshire', 'CHES', '', '', 0, 1),
(3530, 222, 'Clackmannanshire', 'CLACK', '', '', 0, 1),
(3531, 222, 'Conwy', 'CON', '', '', 0, 1),
(3532, 222, 'Cornwall', 'CORN', '', '', 0, 1),
(3533, 222, 'Denbighshire', 'DNBG', '', '', 0, 1),
(3534, 222, 'Derbyshire', 'DERBY', '', '', 0, 1),
(3535, 222, 'Devon', 'DVN', '', '', 0, 1),
(3536, 222, 'Dorset', 'DOR', '', '', 0, 1),
(3537, 222, 'Dumfries and Galloway', 'DGL', '', '', 0, 1),
(3538, 222, 'Dundee', 'DUND', '', '', 0, 1),
(3539, 222, 'Durham', 'DHM', '', '', 0, 1),
(3540, 222, 'East Ayrshire', 'ARYE', '', '', 0, 1),
(3541, 222, 'East Dunbartonshire', 'DUNBE', '', '', 0, 1),
(3542, 222, 'East Lothian', 'LOTE', '', '', 0, 1),
(3543, 222, 'East Renfrewshire', 'RENE', '', '', 0, 1),
(3544, 222, 'East Riding of Yorkshire', 'ERYS', '', '', 0, 1),
(3545, 222, 'East Sussex', 'SXE', '', '', 0, 1),
(3546, 222, 'Edinburgh', 'EDIN', '', '', 0, 1),
(3547, 222, 'Essex', 'ESX', '', '', 0, 1),
(3548, 222, 'Falkirk', 'FALK', '', '', 0, 1),
(3549, 222, 'Fife', 'FFE', '', '', 0, 1),
(3550, 222, 'Flintshire', 'FLINT', '', '', 0, 1),
(3551, 222, 'Glasgow', 'GLAS', '', '', 0, 1),
(3552, 222, 'Gloucestershire', 'GLOS', '', '', 0, 1),
(3553, 222, 'Greater London', 'LDN', '', '', 0, 1),
(3554, 222, 'Greater Manchester', 'MCH', '', '', 0, 1),
(3555, 222, 'Gwynedd', 'GDD', '', '', 0, 1),
(3556, 222, 'Hampshire', 'HANTS', '', '', 0, 1),
(3557, 222, 'Herefordshire', 'HWR', '', '', 0, 1),
(3558, 222, 'Hertfordshire', 'HERTS', '', '', 0, 1),
(3559, 222, 'Highlands', 'HLD', '', '', 0, 1),
(3560, 222, 'Inverclyde', 'IVER', '', '', 0, 1),
(3561, 222, 'Isle of Wight', 'IOW', '', '', 0, 1),
(3562, 222, 'Kent', 'KNT', '', '', 0, 1),
(3563, 222, 'Lancashire', 'LANCS', '', '', 0, 1),
(3564, 222, 'Leicestershire', 'LEICS', '', '', 0, 1),
(3565, 222, 'Lincolnshire', 'LINCS', '', '', 0, 1),
(3566, 222, 'Merseyside', 'MSY', '', '', 0, 1),
(3567, 222, 'Merthyr Tydfil', 'MERT', '', '', 0, 1),
(3568, 222, 'Midlothian', 'MLOT', '', '', 0, 1),
(3569, 222, 'Monmouthshire', 'MMOUTH', '', '', 0, 1),
(3570, 222, 'Moray', 'MORAY', '', '', 0, 1),
(3571, 222, 'Neath Port Talbot', 'NPRTAL', '', '', 0, 1),
(3572, 222, 'Newport', 'NEWPT', '', '', 0, 1),
(3573, 222, 'Norfolk', 'NOR', '', '', 0, 1),
(3574, 222, 'North Ayrshire', 'ARYN', '', '', 0, 1),
(3575, 222, 'North Lanarkshire', 'LANN', '', '', 0, 1),
(3576, 222, 'North Yorkshire', 'YSN', '', '', 0, 1),
(3577, 222, 'Northamptonshire', 'NHM', '', '', 0, 1),
(3578, 222, 'Northumberland', 'NLD', '', '', 0, 1),
(3579, 222, 'Nottinghamshire', 'NOT', '', '', 0, 1),
(3580, 222, 'Orkney Islands', 'ORK', '', '', 0, 1),
(3581, 222, 'Oxfordshire', 'OFE', '', '', 0, 1),
(3582, 222, 'Pembrokeshire', 'PEM', '', '', 0, 1),
(3583, 222, 'Perth and Kinross', 'PERTH', '', '', 0, 1),
(3584, 222, 'Powys', 'PWS', '', '', 0, 1),
(3585, 222, 'Renfrewshire', 'REN', '', '', 0, 1),
(3586, 222, 'Rhondda Cynon Taff', 'RHON', '', '', 0, 1),
(3587, 222, 'Rutland', 'RUT', '', '', 0, 1),
(3588, 222, 'Scottish Borders', 'BOR', '', '', 0, 1),
(3589, 222, 'Shetland Islands', 'SHET', '', '', 0, 1),
(3590, 222, 'Shropshire', 'SPE', '', '', 0, 1),
(3591, 222, 'Somerset', 'SOM', '', '', 0, 1),
(3592, 222, 'South Ayrshire', 'ARYS', '', '', 0, 1),
(3593, 222, 'South Lanarkshire', 'LANS', '', '', 0, 1),
(3594, 222, 'South Yorkshire', 'YSS', '', '', 0, 1),
(3595, 222, 'Staffordshire', 'SFD', '', '', 0, 1),
(3596, 222, 'Stirling', 'STIR', '', '', 0, 1),
(3597, 222, 'Suffolk', 'SFK', '', '', 0, 1),
(3598, 222, 'Surrey', 'SRY', '', '', 0, 1),
(3599, 222, 'Swansea', 'SWAN', '', '', 0, 1),
(3600, 222, 'Torfaen', 'TORF', '', '', 0, 1),
(3601, 222, 'Tyne and Wear', 'TWR', '', '', 0, 1),
(3602, 222, 'Vale of Glamorgan', 'VGLAM', '', '', 0, 1),
(3603, 222, 'Warwickshire', 'WARKS', '', '', 0, 1),
(3604, 222, 'West Dunbartonshire', 'WDUN', '', '', 0, 1),
(3605, 222, 'West Lothian', 'WLOT', '', '', 0, 1),
(3606, 222, 'West Midlands', 'WMD', '', '', 0, 1),
(3607, 222, 'West Sussex', 'SXW', '', '', 0, 1),
(3608, 222, 'West Yorkshire', 'YSW', '', '', 0, 1),
(3609, 222, 'Western Isles', 'WIL', '', '', 0, 1),
(3610, 222, 'Wiltshire', 'WLT', '', '', 0, 1),
(3611, 222, 'Worcestershire', 'WORCS', '', '', 0, 1),
(3612, 222, 'Wrexham', 'WRX', '', '', 0, 1),
(3613, 223, 'Alabama', 'AL', '', '', 0, 1),
(3614, 223, 'Alaska', 'AK', '', '', 0, 1),
(3615, 223, 'American Samoa', 'AS', '', '', 0, 1),
(3616, 223, 'Arizona', 'AZ', '', '', 0, 1),
(3617, 223, 'Arkansas', 'AR', '', '', 0, 1),
(3624, 223, 'California', 'CA', '', '', 0, 1),
(3625, 223, 'Colorado', 'CO', '', '', 0, 1),
(3626, 223, 'Connecticut', 'CT', '', '', 0, 1),
(3627, 223, 'Delaware', 'DE', '', '', 0, 1),
(3628, 223, 'District of Columbia', 'DC', '', '', 0, 1),
(3630, 223, 'Florida', 'FL', '', '', 0, 1),
(3631, 223, 'Georgia', 'GA', '', '', 0, 1),
(3632, 223, 'Guam', 'GU', '', '', 0, 1),
(3633, 223, 'Hawaii', 'HI', '', '', 0, 1),
(3634, 223, 'Idaho', 'ID', '', '', 0, 1),
(3635, 223, 'Illinois', 'IL', '', '', 0, 1),
(3636, 223, 'Indiana', 'IN', '', '', 0, 1),
(3637, 223, 'Iowa', 'IA', '', '', 0, 1),
(3638, 223, 'Kansas', 'KS', '', '', 0, 1),
(3639, 223, 'Kentucky', 'KY', '', '', 0, 1),
(3640, 223, 'Louisiana', 'LA', '', '', 0, 1),
(3641, 223, 'Maine', 'ME', '', '', 0, 1),
(3642, 223, 'Marshall Islands', 'MH', '', '', 0, 1),
(3643, 223, 'Maryland', 'MD', '', '', 0, 1),
(3644, 223, 'Massachusetts', 'MA', '', '', 0, 1),
(3645, 223, 'Michigan', 'MI', '', '', 0, 1),
(3646, 223, 'Minnesota', 'MN', '', '', 0, 1),
(3647, 223, 'Mississippi', 'MS', '', '', 0, 1),
(3648, 223, 'Missouri', 'MO', '', '', 0, 1),
(3649, 223, 'Montana', 'MT', '', '', 0, 1),
(3650, 223, 'Nebraska', 'NE', '', '', 0, 1),
(3651, 223, 'Nevada', 'NV', '', '', 0, 1),
(3652, 223, 'New Hampshire', 'NH', '', '', 0, 1),
(3653, 223, 'New Jersey', 'NJ', '', '', 0, 1),
(3654, 223, 'New Mexico', 'NM', '', '', 0, 1),
(3655, 223, 'New York', 'NY', '', '', 0, 1),
(3656, 223, 'North Carolina', 'NC', '', '', 0, 1),
(3657, 223, 'North Dakota', 'ND', '', '', 0, 1),
(3658, 223, 'Northern Mariana Islands', 'MP', '', '', 0, 1),
(3659, 223, 'Ohio', 'OH', '', '', 0, 1),
(3660, 223, 'Oklahoma', 'OK', '', '', 0, 1),
(3661, 223, 'Oregon', 'OR', '', '', 0, 1),
(3662, 223, 'Palau', 'PW', '', '', 0, 1),
(3663, 223, 'Pennsylvania', 'PA', '', '', 0, 1),
(3664, 223, 'Puerto Rico', 'PR', '', '', 0, 1),
(3665, 223, 'Rhode Island', 'RI', '', '', 0, 1),
(3666, 223, 'South Carolina', 'SC', '', '', 0, 1),
(3667, 223, 'South Dakota', 'SD', '', '', 0, 1),
(3668, 223, 'Tennessee', 'TN', '', '', 0, 1),
(3669, 223, 'Texas', 'TX', '', '', 0, 1),
(3670, 223, 'Utah', 'UT', '', '', 0, 1),
(3671, 223, 'Vermont', 'VT', '', '', 0, 1),
(3672, 223, 'Virgin Islands', 'VI', '', '', 0, 1),
(3673, 223, 'Virginia', 'VA', '', '', 0, 1),
(3674, 223, 'Washington', 'WA', '', '', 0, 1),
(3675, 223, 'West Virginia', 'WV', '', '', 0, 1),
(3676, 223, 'Wisconsin', 'WI', '', '', 0, 1),
(3677, 223, 'Wyoming', 'WY', '', '', 0, 1),
(3678, 224, 'Baker Island', 'BI', '', '', 0, 1),
(3679, 224, 'Howland Island', 'HI', '', '', 0, 1),
(3680, 224, 'Jarvis Island', 'JI', '', '', 0, 1),
(3681, 224, 'Johnston Atoll', 'JA', '', '', 0, 1),
(3682, 224, 'Kingman Reef', 'KR', '', '', 0, 1),
(3683, 224, 'Midway Atoll', 'MA', '', '', 0, 1),
(3684, 224, 'Navassa Island', 'NI', '', '', 0, 1),
(3685, 224, 'Palmyra Atoll', 'PA', '', '', 0, 1),
(3686, 224, 'Wake Island', 'WI', '', '', 0, 1),
(3687, 225, 'Artigas', 'AR', '', '', 0, 1),
(3688, 225, 'Canelones', 'CA', '', '', 0, 1),
(3689, 225, 'Cerro Largo', 'CL', '', '', 0, 1),
(3690, 225, 'Colonia', 'CO', '', '', 0, 1),
(3691, 225, 'Durazno', 'DU', '', '', 0, 1),
(3692, 225, 'Flores', 'FS', '', '', 0, 1),
(3693, 225, 'Florida', 'FA', '', '', 0, 1),
(3694, 225, 'Lavalleja', 'LA', '', '', 0, 1),
(3695, 225, 'Maldonado', 'MA', '', '', 0, 1),
(3696, 225, 'Montevideo', 'MO', '', '', 0, 1),
(3697, 225, 'Paysandu', 'PA', '', '', 0, 1),
(3698, 225, 'Rio Negro', 'RN', '', '', 0, 1),
(3699, 225, 'Rivera', 'RV', '', '', 0, 1),
(3700, 225, 'Rocha', 'RO', '', '', 0, 1),
(3701, 225, 'Salto', 'SL', '', '', 0, 1),
(3702, 225, 'San Jose', 'SJ', '', '', 0, 1),
(3703, 225, 'Soriano', 'SO', '', '', 0, 1),
(3704, 225, 'Tacuarembo', 'TA', '', '', 0, 1),
(3705, 225, 'Treinta y Tres', 'TT', '', '', 0, 1),
(3706, 226, 'Andijon', 'AN', '', '', 0, 1),
(3707, 226, 'Buxoro', 'BU', '', '', 0, 1),
(3708, 226, 'Farg''ona', 'FA', '', '', 0, 1),
(3709, 226, 'Jizzax', 'JI', '', '', 0, 1),
(3710, 226, 'Namangan', 'NG', '', '', 0, 1),
(3711, 226, 'Navoiy', 'NW', '', '', 0, 1),
(3712, 226, 'Qashqadaryo', 'QA', '', '', 0, 1),
(3713, 226, 'Qoraqalpog''iston Republikasi', 'QR', '', '', 0, 1),
(3714, 226, 'Samarqand', 'SA', '', '', 0, 1),
(3715, 226, 'Sirdaryo', 'SI', '', '', 0, 1),
(3716, 226, 'Surxondaryo', 'SU', '', '', 0, 1),
(3717, 226, 'Toshkent City', 'TK', '', '', 0, 1),
(3718, 226, 'Toshkent Region', 'TO', '', '', 0, 1),
(3719, 226, 'Xorazm', 'XO', '', '', 0, 1),
(3720, 227, 'Malampa', 'MA', '', '', 0, 1),
(3721, 227, 'Penama', 'PE', '', '', 0, 1),
(3722, 227, 'Sanma', 'SA', '', '', 0, 1),
(3723, 227, 'Shefa', 'SH', '', '', 0, 1),
(3724, 227, 'Tafea', 'TA', '', '', 0, 1),
(3725, 227, 'Torba', 'TO', '', '', 0, 1),
(3726, 229, 'Amazonas', 'AM', '', '', 0, 1),
(3727, 229, 'Anzoategui', 'AN', '', '', 0, 1),
(3728, 229, 'Apure', 'AP', '', '', 0, 1),
(3729, 229, 'Aragua', 'AR', '', '', 0, 1),
(3730, 229, 'Barinas', 'BA', '', '', 0, 1),
(3731, 229, 'Bolivar', 'BO', '', '', 0, 1),
(3732, 229, 'Carabobo', 'CA', '', '', 0, 1),
(3733, 229, 'Cojedes', 'CO', '', '', 0, 1),
(3734, 229, 'Delta Amacuro', 'DA', '', '', 0, 1),
(3735, 229, 'Dependencias Federales', 'DF', '', '', 0, 1),
(3736, 229, 'Distrito Federal', 'DI', '', '', 0, 1),
(3737, 229, 'Falcon', 'FA', '', '', 0, 1),
(3738, 229, 'Guarico', 'GU', '', '', 0, 1),
(3739, 229, 'Lara', 'LA', '', '', 0, 1),
(3740, 229, 'Merida', 'ME', '', '', 0, 1),
(3741, 229, 'Miranda', 'MI', '', '', 0, 1),
(3742, 229, 'Monagas', 'MO', '', '', 0, 1),
(3743, 229, 'Nueva Esparta', 'NE', '', '', 0, 1),
(3744, 229, 'Portuguesa', 'PO', '', '', 0, 1),
(3745, 229, 'Sucre', 'SU', '', '', 0, 1),
(3746, 229, 'Tachira', 'TA', '', '', 0, 1),
(3747, 229, 'Trujillo', 'TR', '', '', 0, 1),
(3748, 229, 'Vargas', 'VA', '', '', 0, 1),
(3749, 229, 'Yaracuy', 'YA', '', '', 0, 1),
(3750, 229, 'Zulia', 'ZU', '', '', 0, 1),
(3751, 230, 'An Giang', 'AG', '', '', 0, 1),
(3752, 230, 'Bac Giang', 'BG', '', '', 0, 1),
(3753, 230, 'Bac Kan', 'BK', '', '', 0, 1),
(3754, 230, 'Bac Lieu', 'BL', '', '', 0, 1),
(3755, 230, 'Bac Ninh', 'BC', '', '', 0, 1),
(3756, 230, 'Ba Ria-Vung Tau', 'BR', '', '', 0, 1),
(3757, 230, 'Ben Tre', 'BN', '', '', 0, 1),
(3758, 230, 'Binh Dinh', 'BH', '', '', 0, 1),
(3759, 230, 'Binh Duong', 'BU', '', '', 0, 1),
(3760, 230, 'Binh Phuoc', 'BP', '', '', 0, 1),
(3761, 230, 'Binh Thuan', 'BT', '', '', 0, 1),
(3762, 230, 'Ca Mau', 'CM', '', '', 0, 1),
(3763, 230, 'Can Tho', 'CT', '', '', 0, 1),
(3764, 230, 'Cao Bang', 'CB', '', '', 0, 1),
(3765, 230, 'Dak Lak', 'DL', '', '', 0, 1),
(3766, 230, 'Dak Nong', 'DG', '', '', 0, 1),
(3767, 230, 'Da Nang', 'DN', '', '', 0, 1),
(3768, 230, 'Dien Bien', 'DB', '', '', 0, 1),
(3769, 230, 'Dong Nai', 'DI', '', '', 0, 1),
(3770, 230, 'Dong Thap', 'DT', '', '', 0, 1),
(3771, 230, 'Gia Lai', 'GL', '', '', 0, 1),
(3772, 230, 'Ha Giang', 'HG', '', '', 0, 1),
(3773, 230, 'Hai Duong', 'HD', '', '', 0, 1),
(3774, 230, 'Hai Phong', 'HP', '', '', 0, 1),
(3775, 230, 'Ha Nam', 'HM', '', '', 0, 1),
(3776, 230, 'Ha Noi', 'HI', '', '', 0, 1),
(3777, 230, 'Ha Tay', 'HT', '', '', 0, 1),
(3778, 230, 'Ha Tinh', 'HH', '', '', 0, 1),
(3779, 230, 'Hoa Binh', 'HB', '', '', 0, 1),
(3780, 230, 'Ho Chi Minh City', 'HC', '', '', 0, 1),
(3781, 230, 'Hau Giang', 'HU', '', '', 0, 1),
(3782, 230, 'Hung Yen', 'HY', '', '', 0, 1),
(3783, 232, 'Saint Croix', 'C', '', '', 0, 1),
(3784, 232, 'Saint John', 'J', '', '', 0, 1),
(3785, 232, 'Saint Thomas', 'T', '', '', 0, 1),
(3786, 233, 'Alo', 'A', '', '', 0, 1),
(3787, 233, 'Sigave', 'S', '', '', 0, 1),
(3788, 233, 'Wallis', 'W', '', '', 0, 1),
(3789, 235, 'Abyan', 'AB', '', '', 0, 1),
(3790, 235, 'Adan', 'AD', '', '', 0, 1),
(3791, 235, 'Amran', 'AM', '', '', 0, 1),
(3792, 235, 'Al Bayda', 'BA', '', '', 0, 1),
(3793, 235, 'Ad Dali', 'DA', '', '', 0, 1),
(3794, 235, 'Dhamar', 'DH', '', '', 0, 1),
(3795, 235, 'Hadramawt', 'HD', '', '', 0, 1),
(3796, 235, 'Hajjah', 'HJ', '', '', 0, 1),
(3797, 235, 'Al Hudaydah', 'HU', '', '', 0, 1),
(3798, 235, 'Ibb', 'IB', '', '', 0, 1),
(3799, 235, 'Al Jawf', 'JA', '', '', 0, 1),
(3800, 235, 'Lahij', 'LA', '', '', 0, 1),
(3801, 235, 'Ma''rib', 'MA', '', '', 0, 1),
(3802, 235, 'Al Mahrah', 'MR', '', '', 0, 1),
(3803, 235, 'Al Mahwit', 'MW', '', '', 0, 1),
(3804, 235, 'Sa''dah', 'SD', '', '', 0, 1),
(3805, 235, 'San''a', 'SN', '', '', 0, 1),
(3806, 235, 'Shabwah', 'SH', '', '', 0, 1),
(3807, 235, 'Ta''izz', 'TA', '', '', 0, 1),
(3812, 237, 'Bas-Congo', 'BC', '', '', 0, 1),
(3813, 237, 'Bandundu', 'BN', '', '', 0, 1),
(3814, 237, 'Equateur', 'EQ', '', '', 0, 1),
(3815, 237, 'Katanga', 'KA', '', '', 0, 1),
(3816, 237, 'Kasai-Oriental', 'KE', '', '', 0, 1),
(3817, 237, 'Kinshasa', 'KN', '', '', 0, 1),
(3818, 237, 'Kasai-Occidental', 'KW', '', '', 0, 1),
(3819, 237, 'Maniema', 'MA', '', '', 0, 1),
(3820, 237, 'Nord-Kivu', 'NK', '', '', 0, 1),
(3821, 237, 'Orientale', 'OR', '', '', 0, 1),
(3822, 237, 'Sud-Kivu', 'SK', '', '', 0, 1),
(3823, 238, 'Central', 'CE', '', '', 0, 1),
(3824, 238, 'Copperbelt', 'CB', '', '', 0, 1),
(3825, 238, 'Eastern', 'EA', '', '', 0, 1),
(3826, 238, 'Luapula', 'LP', '', '', 0, 1),
(3827, 238, 'Lusaka', 'LK', '', '', 0, 1),
(3828, 238, 'Northern', 'NO', '', '', 0, 1),
(3829, 238, 'North-Western', 'NW', '', '', 0, 1),
(3830, 238, 'Southern', 'SO', '', '', 0, 1),
(3831, 238, 'Western', 'WE', '', '', 0, 1),
(3832, 239, 'Bulawayo', 'BU', '', '', 0, 1),
(3833, 239, 'Harare', 'HA', '', '', 0, 1),
(3834, 239, 'Manicaland', 'ML', '', '', 0, 1),
(3835, 239, 'Mashonaland Central', 'MC', '', '', 0, 1),
(3836, 239, 'Mashonaland East', 'ME', '', '', 0, 1),
(3837, 239, 'Mashonaland West', 'MW', '', '', 0, 1),
(3838, 239, 'Masvingo', 'MV', '', '', 0, 1),
(3839, 239, 'Matabeleland North', 'MN', '', '', 0, 1),
(3840, 239, 'Matabeleland South', 'MS', '', '', 0, 1),
(3841, 239, 'Midlands', 'MD', '', '', 0, 1),
(3861, 105, 'Campobasso', 'CB', '', '', 0, 1),
(3862, 105, 'Carbonia-Iglesias', 'CI', '', '', 0, 1),
(3863, 105, 'Caserta', 'CE', '', '', 0, 1),
(3864, 105, 'Catania', 'CT', '', '', 0, 1),
(3865, 105, 'Catanzaro', 'CZ', '', '', 0, 1),
(3866, 105, 'Chieti', 'CH', '', '', 0, 1),
(3867, 105, 'Como', 'CO', '', '', 0, 1),
(3868, 105, 'Cosenza', 'CS', '', '', 0, 1),
(3869, 105, 'Cremona', 'CR', '', '', 0, 1),
(3870, 105, 'Crotone', 'KR', '', '', 0, 1),
(3871, 105, 'Cuneo', 'CN', '', '', 0, 1),
(3872, 105, 'Enna', 'EN', '', '', 0, 1),
(3873, 105, 'Ferrara', 'FE', '', '', 0, 1),
(3874, 105, 'Firenze', 'FI', '', '', 0, 1),
(3875, 105, 'Foggia', 'FG', '', '', 0, 1),
(3876, 105, 'Forli-Cesena', 'FC', '', '', 0, 1),
(3877, 105, 'Frosinone', 'FR', '', '', 0, 1),
(3878, 105, 'Genova', 'GE', '', '', 0, 1),
(3879, 105, 'Gorizia', 'GO', '', '', 0, 1),
(3880, 105, 'Grosseto', 'GR', '', '', 0, 1),
(3881, 105, 'Imperia', 'IM', '', '', 0, 1),
(3882, 105, 'Isernia', 'IS', '', '', 0, 1),
(3883, 105, 'L&#39;Aquila', 'AQ', '', '', 0, 1),
(3884, 105, 'La Spezia', 'SP', '', '', 0, 1),
(3885, 105, 'Latina', 'LT', '', '', 0, 1),
(3886, 105, 'Lecce', 'LE', '', '', 0, 1),
(3887, 105, 'Lecco', 'LC', '', '', 0, 1),
(3888, 105, 'Livorno', 'LI', '', '', 0, 1),
(3889, 105, 'Lodi', 'LO', '', '', 0, 1),
(3890, 105, 'Lucca', 'LU', '', '', 0, 1),
(3891, 105, 'Macerata', 'MC', '', '', 0, 1),
(3892, 105, 'Mantova', 'MN', '', '', 0, 1),
(3893, 105, 'Massa-Carrara', 'MS', '', '', 0, 1),
(3894, 105, 'Matera', 'MT', '', '', 0, 1),
(3895, 105, 'Medio Campidano', 'VS', '', '', 0, 1),
(3896, 105, 'Messina', 'ME', '', '', 0, 1),
(3897, 105, 'Milano', 'MI', '', '', 0, 1),
(3898, 105, 'Modena', 'MO', '', '', 0, 1),
(3899, 105, 'Napoli', 'NA', '', '', 0, 1),
(3900, 105, 'Novara', 'NO', '', '', 0, 1),
(3901, 105, 'Nuoro', 'NU', '', '', 0, 1),
(3902, 105, 'Ogliastra', 'OG', '', '', 0, 1),
(3903, 105, 'Olbia-Tempio', 'OT', '', '', 0, 1),
(3904, 105, 'Oristano', 'OR', '', '', 0, 1),
(3905, 105, 'Padova', 'PD', '', '', 0, 1),
(3906, 105, 'Palermo', 'PA', '', '', 0, 1),
(3907, 105, 'Parma', 'PR', '', '', 0, 1),
(3908, 105, 'Pavia', 'PV', '', '', 0, 1),
(3909, 105, 'Perugia', 'PG', '', '', 0, 1),
(3910, 105, 'Pesaro e Urbino', 'PU', '', '', 0, 1),
(3911, 105, 'Pescara', 'PE', '', '', 0, 1),
(3912, 105, 'Piacenza', 'PC', '', '', 0, 1),
(3913, 105, 'Pisa', 'PI', '', '', 0, 1),
(3914, 105, 'Pistoia', 'PT', '', '', 0, 1),
(3915, 105, 'Pordenone', 'PN', '', '', 0, 1),
(3916, 105, 'Potenza', 'PZ', '', '', 0, 1),
(3917, 105, 'Prato', 'PO', '', '', 0, 1),
(3918, 105, 'Ragusa', 'RG', '', '', 0, 1),
(3919, 105, 'Ravenna', 'RA', '', '', 0, 1),
(3920, 105, 'Reggio Calabria', 'RC', '', '', 0, 1),
(3921, 105, 'Reggio Emilia', 'RE', '', '', 0, 1),
(3922, 105, 'Rieti', 'RI', '', '', 0, 1),
(3923, 105, 'Rimini', 'RN', '', '', 0, 1),
(3924, 105, 'Roma', 'RM', '', '', 0, 1),
(3925, 105, 'Rovigo', 'RO', '', '', 0, 1),
(3926, 105, 'Salerno', 'SA', '', '', 0, 1),
(3927, 105, 'Sassari', 'SS', '', '', 0, 1),
(3928, 105, 'Savona', 'SV', '', '', 0, 1),
(3929, 105, 'Siena', 'SI', '', '', 0, 1),
(3930, 105, 'Siracusa', 'SR', '', '', 0, 1),
(3931, 105, 'Sondrio', 'SO', '', '', 0, 1),
(3932, 105, 'Taranto', 'TA', '', '', 0, 1),
(3933, 105, 'Teramo', 'TE', '', '', 0, 1),
(3934, 105, 'Terni', 'TR', '', '', 0, 1),
(3935, 105, 'Torino', 'TO', '', '', 0, 1),
(3936, 105, 'Trapani', 'TP', '', '', 0, 1),
(3937, 105, 'Trento', 'TN', '', '', 0, 1),
(3938, 105, 'Treviso', 'TV', '', '', 0, 1),
(3939, 105, 'Trieste', 'TS', '', '', 0, 1),
(3940, 105, 'Udine', 'UD', '', '', 0, 1),
(3941, 105, 'Varese', 'VA', '', '', 0, 1),
(3942, 105, 'Venezia', 'VE', '', '', 0, 1),
(3943, 105, 'Verbano-Cusio-Ossola', 'VB', '', '', 0, 1),
(3944, 105, 'Vercelli', 'VC', '', '', 0, 1),
(3945, 105, 'Verona', 'VR', '', '', 0, 1),
(3946, 105, 'Vibo Valentia', 'VV', '', '', 0, 1),
(3947, 105, 'Vicenza', 'VI', '', '', 0, 1),
(3948, 105, 'Viterbo', 'VT', '', '', 0, 1),
(3949, 222, 'County Antrim', 'ANT', '', '', 0, 1),
(3950, 222, 'County Armagh', 'ARM', '', '', 0, 1),
(3951, 222, 'County Down', 'DOW', '', '', 0, 1),
(3952, 222, 'County Fermanagh', 'FER', '', '', 0, 1),
(3953, 222, 'County Londonderry', 'LDY', '', '', 0, 1),
(3954, 222, 'County Tyrone', 'TYR', '', '', 0, 1),
(3955, 222, 'Cumbria', 'CMA', '', '', 0, 1),
(3956, 190, 'Pomurska', '1', '', '', 0, 1),
(3957, 190, 'Podravska', '2', '', '', 0, 1),
(3958, 190, 'Koroška', '3', '', '', 0, 1),
(3959, 190, 'Savinjska', '4', '', '', 0, 1),
(3960, 190, 'Zasavska', '5', '', '', 0, 1),
(3961, 190, 'Spodnjeposavska', '6', '', '', 0, 1),
(3962, 190, 'Jugovzhodna Slovenija', '7', '', '', 0, 1),
(3963, 190, 'Osrednjeslovenska', '8', '', '', 0, 1),
(3964, 190, 'Gorenjska', '9', '', '', 0, 1),
(3965, 190, 'Notranjsko-kraška', '10', '', '', 0, 1),
(3966, 190, 'Goriška', '11', '', '', 0, 1),
(3967, 190, 'Obalno-kraška', '12', '', '', 0, 1),
(3968, 33, 'Ruse', '', '', '', 0, 1),
(3969, 101, 'Alborz', 'ALB', '', '', 0, 1),
(3970, 21, 'Brussels-Capital Region', 'BRU', '', '', 0, 1),
(3971, 138, 'Aguascalientes', 'AG', '', '', 0, 1),
(3973, 242, 'Andrijevica', '01', '', '', 0, 1),
(3974, 242, 'Bar', '02', '', '', 0, 1),
(3975, 242, 'Berane', '03', '', '', 0, 1),
(3976, 242, 'Bijelo Polje', '04', '', '', 0, 1),
(3977, 242, 'Budva', '05', '', '', 0, 1),
(3978, 242, 'Cetinje', '06', '', '', 0, 1),
(3979, 242, 'Danilovgrad', '07', '', '', 0, 1),
(3980, 242, 'Herceg-Novi', '08', '', '', 0, 1),
(3981, 242, 'Kolašin', '09', '', '', 0, 1),
(3982, 242, 'Kotor', '10', '', '', 0, 1),
(3983, 242, 'Mojkovac', '11', '', '', 0, 1),
(3984, 242, 'Nikšić', '12', '', '', 0, 1),
(3985, 242, 'Plav', '13', '', '', 0, 1),
(3986, 242, 'Pljevlja', '14', '', '', 0, 1),
(3987, 242, 'Plužine', '15', '', '', 0, 1),
(3988, 242, 'Podgorica', '16', '', '', 0, 1),
(3989, 242, 'Rožaje', '17', '', '', 0, 1),
(3990, 242, 'Šavnik', '18', '', '', 0, 1),
(3991, 242, 'Tivat', '19', '', '', 0, 1),
(3992, 242, 'Ulcinj', '20', '', '', 0, 1),
(3993, 242, 'Žabljak', '21', '', '', 0, 1),
(3994, 243, 'Belgrade', '00', '', '', 0, 1),
(3995, 243, 'North Bačka', '01', '', '', 0, 1),
(3996, 243, 'Central Banat', '02', '', '', 0, 1),
(3997, 243, 'North Banat', '03', '', '', 0, 1),
(3998, 243, 'South Banat', '04', '', '', 0, 1),
(3999, 243, 'West Bačka', '05', '', '', 0, 1),
(4000, 243, 'South Bačka', '06', '', '', 0, 1),
(4001, 243, 'Srem', '07', '', '', 0, 1),
(4002, 243, 'Mačva', '08', '', '', 0, 1),
(4003, 243, 'Kolubara', '09', '', '', 0, 1),
(4004, 243, 'Podunavlje', '10', '', '', 0, 1),
(4005, 243, 'Braničevo', '11', '', '', 0, 1),
(4006, 243, 'Šumadija', '12', '', '', 0, 1),
(4007, 243, 'Pomoravlje', '13', '', '', 0, 1),
(4008, 243, 'Bor', '14', '', '', 0, 1),
(4009, 243, 'Zaječar', '15', '', '', 0, 1),
(4010, 243, 'Zlatibor', '16', '', '', 0, 1),
(4011, 243, 'Moravica', '17', '', '', 0, 1),
(4012, 243, 'Raška', '18', '', '', 0, 1),
(4013, 243, 'Rasina', '19', '', '', 0, 1),
(4014, 243, 'Nišava', '20', '', '', 0, 1),
(4015, 243, 'Toplica', '21', '', '', 0, 1),
(4016, 243, 'Pirot', '22', '', '', 0, 1),
(4017, 243, 'Jablanica', '23', '', '', 0, 1),
(4018, 243, 'Pčinja', '24', '', '', 0, 1),
(4020, 245, 'Bonaire', 'BO', '', '', 0, 1),
(4021, 245, 'Saba', 'SA', '', '', 0, 1),
(4022, 245, 'Sint Eustatius', 'SE', '', '', 0, 1),
(4023, 248, 'Central Equatoria', 'EC', '', '', 0, 1),
(4024, 248, 'Eastern Equatoria', 'EE', '', '', 0, 1),
(4025, 248, 'Jonglei', 'JG', '', '', 0, 1),
(4026, 248, 'Lakes', 'LK', '', '', 0, 1),
(4027, 248, 'Northern Bahr el-Ghazal', 'BN', '', '', 0, 1),
(4028, 248, 'Unity', 'UY', '', '', 0, 1),
(4029, 248, 'Upper Nile', 'NU', '', '', 0, 1),
(4030, 248, 'Warrap', 'WR', '', '', 0, 1),
(4031, 248, 'Western Bahr el-Ghazal', 'BW', '', '', 0, 1),
(4032, 248, 'Western Equatoria', 'EW', '', '', 0, 1),
(4036, 117, 'Ainaži, Salacgrīvas novads', '0661405', '', '', 0, 1),
(4037, 117, 'Aizkraukle, Aizkraukles novads', '0320201', '', '', 0, 1),
(4038, 117, 'Aizkraukles novads', '0320200', '', '', 0, 1),
(4039, 117, 'Aizpute, Aizputes novads', '0640605', '', '', 0, 1),
(4040, 117, 'Aizputes novads', '0640600', '', '', 0, 1),
(4041, 117, 'Aknīste, Aknīstes novads', '0560805', '', '', 0, 1),
(4042, 117, 'Aknīstes novads', '0560800', '', '', 0, 1),
(4043, 117, 'Aloja, Alojas novads', '0661007', '', '', 0, 1),
(4044, 117, 'Alojas novads', '0661000', '', '', 0, 1),
(4045, 117, 'Alsungas novads', '0624200', '', '', 0, 1),
(4046, 117, 'Alūksne, Alūksnes novads', '0360201', '', '', 0, 1),
(4047, 117, 'Alūksnes novads', '0360200', '', '', 0, 1),
(4048, 117, 'Amatas novads', '0424701', '', '', 0, 1),
(4049, 117, 'Ape, Apes novads', '0360805', '', '', 0, 1),
(4050, 117, 'Apes novads', '0360800', '', '', 0, 1),
(4051, 117, 'Auce, Auces novads', '0460805', '', '', 0, 1),
(4052, 117, 'Auces novads', '0460800', '', '', 0, 1),
(4053, 117, 'Ādažu novads', '0804400', '', '', 0, 1),
(4054, 117, 'Babītes novads', '0804900', '', '', 0, 1),
(4055, 117, 'Baldone, Baldones novads', '0800605', '', '', 0, 1),
(4056, 117, 'Baldones novads', '0800600', '', '', 0, 1),
(4057, 117, 'Baloži, Ķekavas novads', '0800807', '', '', 0, 1),
(4058, 117, 'Baltinavas novads', '0384400', '', '', 0, 1),
(4059, 117, 'Balvi, Balvu novads', '0380201', '', '', 0, 1),
(4060, 117, 'Balvu novads', '0380200', '', '', 0, 1),
(4061, 117, 'Bauska, Bauskas novads', '0400201', '', '', 0, 1),
(4062, 117, 'Bauskas novads', '0400200', '', '', 0, 1),
(4063, 117, 'Beverīnas novads', '0964700', '', '', 0, 1),
(4064, 117, 'Brocēni, Brocēnu novads', '0840605', '', '', 0, 1),
(4065, 117, 'Brocēnu novads', '0840601', '', '', 0, 1),
(4066, 117, 'Burtnieku novads', '0967101', '', '', 0, 1),
(4067, 117, 'Carnikavas novads', '0805200', '', '', 0, 1),
(4068, 117, 'Cesvaine, Cesvaines novads', '0700807', '', '', 0, 1),
(4069, 117, 'Cesvaines novads', '0700800', '', '', 0, 1),
(4070, 117, 'Cēsis, Cēsu novads', '0420201', '', '', 0, 1),
(4071, 117, 'Cēsu novads', '0420200', '', '', 0, 1),
(4072, 117, 'Ciblas novads', '0684901', '', '', 0, 1),
(4073, 117, 'Dagda, Dagdas novads', '0601009', '', '', 0, 1),
(4074, 117, 'Dagdas novads', '0601000', '', '', 0, 1),
(4075, 117, 'Daugavpils', '0050000', '', '', 0, 1),
(4076, 117, 'Daugavpils novads', '0440200', '', '', 0, 1),
(4077, 117, 'Dobele, Dobeles novads', '0460201', '', '', 0, 1),
(4078, 117, 'Dobeles novads', '0460200', '', '', 0, 1),
(4079, 117, 'Dundagas novads', '0885100', '', '', 0, 1),
(4080, 117, 'Durbe, Durbes novads', '0640807', '', '', 0, 1),
(4081, 117, 'Durbes novads', '0640801', '', '', 0, 1),
(4082, 117, 'Engures novads', '0905100', '', '', 0, 1),
(4083, 117, 'Ērgļu novads', '0705500', '', '', 0, 1),
(4084, 117, 'Garkalnes novads', '0806000', '', '', 0, 1),
(4085, 117, 'Grobiņa, Grobiņas novads', '0641009', '', '', 0, 1),
(4086, 117, 'Grobiņas novads', '0641000', '', '', 0, 1),
(4087, 117, 'Gulbene, Gulbenes novads', '0500201', '', '', 0, 1),
(4088, 117, 'Gulbenes novads', '0500200', '', '', 0, 1),
(4089, 117, 'Iecavas novads', '0406400', '', '', 0, 1),
(4090, 117, 'Ikšķile, Ikšķiles novads', '0740605', '', '', 0, 1),
(4091, 117, 'Ikšķiles novads', '0740600', '', '', 0, 1),
(4092, 117, 'Ilūkste, Ilūkstes novads', '0440807', '', '', 0, 1),
(4093, 117, 'Ilūkstes novads', '0440801', '', '', 0, 1),
(4094, 117, 'Inčukalna novads', '0801800', '', '', 0, 1),
(4095, 117, 'Jaunjelgava, Jaunjelgavas novads', '0321007', '', '', 0, 1),
(4096, 117, 'Jaunjelgavas novads', '0321000', '', '', 0, 1),
(4097, 117, 'Jaunpiebalgas novads', '0425700', '', '', 0, 1),
(4098, 117, 'Jaunpils novads', '0905700', '', '', 0, 1),
(4099, 117, 'Jelgava', '0090000', '', '', 0, 1),
(4100, 117, 'Jelgavas novads', '0540200', '', '', 0, 1),
(4101, 117, 'Jēkabpils', '0110000', '', '', 0, 1),
(4102, 117, 'Jēkabpils novads', '0560200', '', '', 0, 1),
(4103, 117, 'Jūrmala', '0130000', '', '', 0, 1),
(4104, 117, 'Kalnciems, Jelgavas novads', '0540211', '', '', 0, 1),
(4105, 117, 'Kandava, Kandavas novads', '0901211', '', '', 0, 1),
(4106, 117, 'Kandavas novads', '0901201', '', '', 0, 1),
(4107, 117, 'Kārsava, Kārsavas novads', '0681009', '', '', 0, 1),
(4108, 117, 'Kārsavas novads', '0681000', '', '', 0, 1),
(4109, 117, 'Kocēnu novads ,bij. Valmieras)', '0960200', '', '', 0, 1),
(4110, 117, 'Kokneses novads', '0326100', '', '', 0, 1),
(4111, 117, 'Krāslava, Krāslavas novads', '0600201', '', '', 0, 1),
(4112, 117, 'Krāslavas novads', '0600202', '', '', 0, 1),
(4113, 117, 'Krimuldas novads', '0806900', '', '', 0, 1),
(4114, 117, 'Krustpils novads', '0566900', '', '', 0, 1),
(4115, 117, 'Kuldīga, Kuldīgas novads', '0620201', '', '', 0, 1),
(4116, 117, 'Kuldīgas novads', '0620200', '', '', 0, 1),
(4117, 117, 'Ķeguma novads', '0741001', '', '', 0, 1),
(4118, 117, 'Ķegums, Ķeguma novads', '0741009', '', '', 0, 1),
(4119, 117, 'Ķekavas novads', '0800800', '', '', 0, 1),
(4120, 117, 'Lielvārde, Lielvārdes novads', '0741413', '', '', 0, 1),
(4121, 117, 'Lielvārdes novads', '0741401', '', '', 0, 1),
(4122, 117, 'Liepāja', '0170000', '', '', 0, 1),
(4123, 117, 'Limbaži, Limbažu novads', '0660201', '', '', 0, 1),
(4124, 117, 'Limbažu novads', '0660200', '', '', 0, 1),
(4125, 117, 'Līgatne, Līgatnes novads', '0421211', '', '', 0, 1),
(4126, 117, 'Līgatnes novads', '0421200', '', '', 0, 1),
(4127, 117, 'Līvāni, Līvānu novads', '0761211', '', '', 0, 1),
(4128, 117, 'Līvānu novads', '0761201', '', '', 0, 1),
(4129, 117, 'Lubāna, Lubānas novads', '0701413', '', '', 0, 1),
(4130, 117, 'Lubānas novads', '0701400', '', '', 0, 1),
(4131, 117, 'Ludza, Ludzas novads', '0680201', '', '', 0, 1),
(4132, 117, 'Ludzas novads', '0680200', '', '', 0, 1),
(4133, 117, 'Madona, Madonas novads', '0700201', '', '', 0, 1),
(4134, 117, 'Madonas novads', '0700200', '', '', 0, 1),
(4135, 117, 'Mazsalaca, Mazsalacas novads', '0961011', '', '', 0, 1),
(4136, 117, 'Mazsalacas novads', '0961000', '', '', 0, 1),
(4137, 117, 'Mālpils novads', '0807400', '', '', 0, 1),
(4138, 117, 'Mārupes novads', '0807600', '', '', 0, 1),
(4139, 117, 'Mērsraga novads', '0887600', '', '', 0, 1),
(4140, 117, 'Naukšēnu novads', '0967300', '', '', 0, 1),
(4141, 117, 'Neretas novads', '0327100', '', '', 0, 1),
(4142, 117, 'Nīcas novads', '0647900', '', '', 0, 1),
(4143, 117, 'Ogre, Ogres novads', '0740201', '', '', 0, 1),
(4144, 117, 'Ogres novads', '0740202', '', '', 0, 1),
(4145, 117, 'Olaine, Olaines novads', '0801009', '', '', 0, 1),
(4146, 117, 'Olaines novads', '0801000', '', '', 0, 1),
(4147, 117, 'Ozolnieku novads', '0546701', '', '', 0, 1),
(4148, 117, 'Pārgaujas novads', '0427500', '', '', 0, 1),
(4149, 117, 'Pāvilosta, Pāvilostas novads', '0641413', '', '', 0, 1),
(4150, 117, 'Pāvilostas novads', '0641401', '', '', 0, 1),
(4151, 117, 'Piltene, Ventspils novads', '0980213', '', '', 0, 1),
(4152, 117, 'Pļaviņas, Pļaviņu novads', '0321413', '', '', 0, 1),
(4153, 117, 'Pļaviņu novads', '0321400', '', '', 0, 1),
(4154, 117, 'Preiļi, Preiļu novads', '0760201', '', '', 0, 1),
(4155, 117, 'Preiļu novads', '0760202', '', '', 0, 1),
(4156, 117, 'Priekule, Priekules novads', '0641615', '', '', 0, 1),
(4157, 117, 'Priekules novads', '0641600', '', '', 0, 1),
(4158, 117, 'Priekuļu novads', '0427300', '', '', 0, 1),
(4159, 117, 'Raunas novads', '0427700', '', '', 0, 1),
(4160, 117, 'Rēzekne', '0210000', '', '', 0, 1),
(4161, 117, 'Rēzeknes novads', '0780200', '', '', 0, 1),
(4162, 117, 'Riebiņu novads', '0766300', '', '', 0, 1),
(4163, 117, 'Rīga', '0010000', '', '', 0, 1),
(4164, 117, 'Rojas novads', '0888300', '', '', 0, 1),
(4165, 117, 'Ropažu novads', '0808400', '', '', 0, 1),
(4166, 117, 'Rucavas novads', '0648500', '', '', 0, 1),
(4167, 117, 'Rugāju novads', '0387500', '', '', 0, 1),
(4168, 117, 'Rundāles novads', '0407700', '', '', 0, 1),
(4169, 117, 'Rūjiena, Rūjienas novads', '0961615', '', '', 0, 1),
(4170, 117, 'Rūjienas novads', '0961600', '', '', 0, 1),
(4171, 117, 'Sabile, Talsu novads', '0880213', '', '', 0, 1),
(4172, 117, 'Salacgrīva, Salacgrīvas novads', '0661415', '', '', 0, 1),
(4173, 117, 'Salacgrīvas novads', '0661400', '', '', 0, 1),
(4174, 117, 'Salas novads', '0568700', '', '', 0, 1),
(4175, 117, 'Salaspils novads', '0801200', '', '', 0, 1),
(4176, 117, 'Salaspils, Salaspils novads', '0801211', '', '', 0, 1),
(4177, 117, 'Saldus novads', '0840200', '', '', 0, 1),
(4178, 117, 'Saldus, Saldus novads', '0840201', '', '', 0, 1),
(4179, 117, 'Saulkrasti, Saulkrastu novads', '0801413', '', '', 0, 1),
(4180, 117, 'Saulkrastu novads', '0801400', '', '', 0, 1),
(4181, 117, 'Seda, Strenču novads', '0941813', '', '', 0, 1),
(4182, 117, 'Sējas novads', '0809200', '', '', 0, 1),
(4183, 117, 'Sigulda, Siguldas novads', '0801615', '', '', 0, 1),
(4184, 117, 'Siguldas novads', '0801601', '', '', 0, 1),
(4185, 117, 'Skrīveru novads', '0328200', '', '', 0, 1),
(4186, 117, 'Skrunda, Skrundas novads', '0621209', '', '', 0, 1),
(4187, 117, 'Skrundas novads', '0621200', '', '', 0, 1),
(4188, 117, 'Smiltene, Smiltenes novads', '0941615', '', '', 0, 1),
(4189, 117, 'Smiltenes novads', '0941600', '', '', 0, 1),
(4190, 117, 'Staicele, Alojas novads', '0661017', '', '', 0, 1),
(4191, 117, 'Stende, Talsu novads', '0880215', '', '', 0, 1),
(4192, 117, 'Stopiņu novads', '0809600', '', '', 0, 1),
(4193, 117, 'Strenči, Strenču novads', '0941817', '', '', 0, 1),
(4194, 117, 'Strenču novads', '0941800', '', '', 0, 1),
(4195, 117, 'Subate, Ilūkstes novads', '0440815', '', '', 0, 1),
(4196, 117, 'Talsi, Talsu novads', '0880201', '', '', 0, 1),
(4197, 117, 'Talsu novads', '0880200', '', '', 0, 1),
(4198, 117, 'Tērvetes novads', '0468900', '', '', 0, 1),
(4199, 117, 'Tukuma novads', '0900200', '', '', 0, 1),
(4200, 117, 'Tukums, Tukuma novads', '0900201', '', '', 0, 1),
(4201, 117, 'Vaiņodes novads', '0649300', '', '', 0, 1),
(4202, 117, 'Valdemārpils, Talsu novads', '0880217', '', '', 0, 1),
(4203, 117, 'Valka, Valkas novads', '0940201', '', '', 0, 1),
(4204, 117, 'Valkas novads', '0940200', '', '', 0, 1),
(4205, 117, 'Valmiera', '0250000', '', '', 0, 1),
(4206, 117, 'Vangaži, Inčukalna novads', '0801817', '', '', 0, 1),
(4207, 117, 'Varakļāni, Varakļānu novads', '0701817', '', '', 0, 1),
(4208, 117, 'Varakļānu novads', '0701800', '', '', 0, 1),
(4209, 117, 'Vārkavas novads', '0769101', '', '', 0, 1),
(4210, 117, 'Vecpiebalgas novads', '0429300', '', '', 0, 1),
(4211, 117, 'Vecumnieku novads', '0409500', '', '', 0, 1),
(4212, 117, 'Ventspils', '0270000', '', '', 0, 1),
(4213, 117, 'Ventspils novads', '0980200', '', '', 0, 1),
(4214, 117, 'Viesīte, Viesītes novads', '0561815', '', '', 0, 1),
(4215, 117, 'Viesītes novads', '0561800', '', '', 0, 1),
(4216, 117, 'Viļaka, Viļakas novads', '0381615', '', '', 0, 1),
(4217, 117, 'Viļakas novads', '0381600', '', '', 0, 1),
(4218, 117, 'Viļāni, Viļānu novads', '0781817', '', '', 0, 1),
(4219, 117, 'Viļānu novads', '0781800', '', '', 0, 1),
(4220, 117, 'Zilupe, Zilupes novads', '0681817', '', '', 0, 1),
(4221, 117, 'Zilupes novads', '0681801', '', '', 0, 1),
(4222, 43, 'Arica y Parinacota', 'AP', '', '', 0, 1),
(4223, 43, 'Los Rios', 'LR', '', '', 0, 1),
(4224, 220, 'Kharkivs''ka Oblast''', '63', '', '', 0, 1),
(4225, 99, 'Telangana', 'TL', '', '', 1, 1);





--
-- Table structure for table `sma_stock_bin`
--

DROP TABLE IF EXISTS `sma_stock_bin`;
CREATE TABLE `sma_stock_bin` (
  `id` bigint(20) NOT NULL,
  `org_id` varchar(20) NOT NULL,
  `wh_id` varchar(20) NOT NULL,
  `itm_id` varchar(50) NOT NULL,
  `lot_id` varchar(20) NOT NULL,
  `bin_id` varchar(20) NOT NULL,
  `itm_uom_bs` varchar(20) NOT NULL,
  `tot_stk` decimal(26,6) NOT NULL,
  `usr_id_mod` int(11) NOT NULL,
  `usr_id_mod_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `rej_stk` decimal(26,6) DEFAULT NULL,
  `rwk_stk` decimal(26,6) DEFAULT NULL,
  `rej_cnt_qty` decimal(26,6) DEFAULT NULL,
  `rwk_cnt_qty` decimal(26,6) DEFAULT NULL,
  `cnt_qty` decimal(26,6) DEFAULT NULL,
  `code` varchar(50) NOT NULL,
  `upd_flg` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_stock_item`
--

DROP TABLE IF EXISTS `sma_stock_item`;
CREATE TABLE `sma_stock_item` (
  `id` bigint(20) NOT NULL,
  `org_id` varchar(20) NOT NULL,
  `wh_id` varchar(20) NOT NULL,
  `fy_id` int(5) NOT NULL,
  `itm_id` varchar(50) NOT NULL,
  `itm_uom_bs` varchar(20) DEFAULT NULL,
  `op_stk` decimal(26,6) DEFAULT NULL,
  `op_avl_stk` decimal(26,6) DEFAULT NULL,
  `op_req_stk` decimal(26,6) DEFAULT NULL,
  `op_ord_stk` decimal(26,6) DEFAULT NULL,
  `op_rej_stk` decimal(26,6) DEFAULT NULL,
  `tot_stk` decimal(26,6) DEFAULT NULL,
  `avl_stk` decimal(26,6) DEFAULT NULL,
  `req_stk` decimal(26,6) DEFAULT NULL,
  `ord_stk` decimal(26,6) DEFAULT NULL,
  `rej_stk` int(10) DEFAULT NULL,
  `usr_id_mod_dt` timestamp NULL DEFAULT NULL,
  `op_scrp_stk` decimal(26,6) DEFAULT NULL,
  `op_rwk_stk` decimal(26,6) DEFAULT NULL,
  `scrp_stk` decimal(26,6) DEFAULT NULL,
  `rwk_stk` decimal(26,6) DEFAULT NULL,
  `map_price` decimal(26,6) DEFAULT NULL,
  `wap_price` decimal(26,6) DEFAULT NULL,
  `std_val` decimal(26,6) DEFAULT NULL,
  `map_val` decimal(26,6) DEFAULT NULL,
  `wap_val` decimal(26,6) DEFAULT NULL,
  `lifo_val` decimal(26,6) DEFAULT NULL,
  `fifo_val` decimal(26,6) DEFAULT NULL,
  `cum_tot_rej_qty` decimal(26,6) DEFAULT NULL,
  `cum_tot_rwk_qty` decimal(26,6) DEFAULT NULL,
  `cum_tot_scrp_qty` decimal(26,6) DEFAULT NULL,
  `cum_tot_rcpt_qty` decimal(26,6) DEFAULT NULL,
  `cum_tot_rcpt_lnd_val` decimal(26,6) DEFAULT NULL,
  `cum_tot_issu_qty` decimal(26,6) DEFAULT NULL,
  `cum_tot_issu_lnd_val` decimal(26,6) DEFAULT NULL,
  `lifo_price` decimal(26,6) DEFAULT NULL,
  `fifo_price` decimal(26,6) DEFAULT NULL,
  `git_stk` decimal(26,6) DEFAULT NULL,
  `rej_cnt_qty` decimal(26,6) DEFAULT NULL,
  `rwk_cnt_qty` decimal(26,6) DEFAULT NULL,
  `cnt_qty` decimal(26,6) DEFAULT NULL,
  `code` varchar(50) NOT NULL,
  `upd_flg` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_stock_lot`
--

DROP TABLE IF EXISTS `sma_stock_lot`;
CREATE TABLE `sma_stock_lot` (
  `id` bigint(20) NOT NULL,
  `org_id` varchar(20) NOT NULL,
  `wh_id` varchar(20) NOT NULL,
  `itm_id` varchar(50) NOT NULL,
  `lot_id` varchar(20) NOT NULL,
  `itm_uom_bs` varchar(20) DEFAULT NULL,
  `op_stk` decimal(26,6) DEFAULT NULL,
  `tot_stk` decimal(26,6) DEFAULT NULL,
  `basic_price` decimal(26,6) DEFAULT NULL,
  `lnd_price` decimal(26,6) DEFAULT NULL,
  `lnd_val` decimal(26,6) DEFAULT NULL,
  `warranty_date` date DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `lot_date` timestamp NULL DEFAULT NULL,
  `scrp_stk` decimal(26,6) NOT NULL,
  `rej_stk` decimal(26,6) NOT NULL,
  `rwk_stk` decimal(26,6) NOT NULL,
  `lot_price_op` decimal(26,6) NOT NULL,
  `lot_price_crnt` decimal(26,6) NOT NULL,
  `mfg_date` date NOT NULL,
  `batch_no` varchar(20) NOT NULL,
  `rej_cnt_qty` decimal(26,6) NOT NULL,
  `rwk_cnt_qty` decimal(26,6) NOT NULL,
  `cnt_qty` decimal(26,6) NOT NULL,
  `remarks` varchar(4000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_subcategories`
--

DROP TABLE IF EXISTS `sma_subcategories`;
CREATE TABLE `sma_subcategories` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `code` varchar(55) NOT NULL,
  `name` varchar(55) NOT NULL,
  `image` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_suspended_bills`
--

DROP TABLE IF EXISTS `sma_suspended_bills`;
CREATE TABLE `sma_suspended_bills` (
  `id` int(11) UNSIGNED NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) DEFAULT NULL,
  `count` int(11) NOT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `biller_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `suspend_note` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_suspended_items`
--

DROP TABLE IF EXISTS `sma_suspended_items`;
CREATE TABLE `sma_suspended_items` (
  `id` int(11) NOT NULL,
  `suspend_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `expiry` date DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL,
  `bin_id` int(11) DEFAULT NULL,
  `lot_no` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_sync_table`
--

DROP TABLE IF EXISTS `sma_sync_table`;
CREATE TABLE `sma_sync_table` (
  `id` int(11) NOT NULL,
  `table_name` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_tax_rates`
--

DROP TABLE IF EXISTS `sma_tax_rates`;
CREATE TABLE `sma_tax_rates` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `code` varchar(10) DEFAULT NULL,
  `rate` decimal(12,4) NOT NULL,
  `type` varchar(50) NOT NULL,
  `org_id` varchar(20) DEFAULT NULL,
  `wh_id` varchar(20) DEFAULT NULL,
  `grp_id` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_till`
--

DROP TABLE IF EXISTS `sma_till`;
CREATE TABLE `sma_till` (
  `id` int(5) NOT NULL,
  `till_name` varchar(50) NOT NULL,
  `till_ip` varchar(50) NOT NULL,
  `store_id` int(5) NOT NULL,
  `user_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_transfers`
--

DROP TABLE IF EXISTS `sma_transfers`;
CREATE TABLE `sma_transfers` (
  `id` int(11) NOT NULL,
  `transfer_no` varchar(55) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `from_warehouse_id` int(11) NOT NULL,
  `from_warehouse_code` varchar(55) NOT NULL,
  `from_warehouse_name` varchar(55) NOT NULL,
  `to_warehouse_id` int(11) NOT NULL,
  `to_warehouse_code` varchar(55) NOT NULL,
  `to_warehouse_name` varchar(55) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT NULL,
  `grand_total` decimal(25,4) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `status` varchar(55) NOT NULL DEFAULT 'pending',
  `shipping` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `attachment` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_transfer_items`
--

DROP TABLE IF EXISTS `sma_transfer_items`;
CREATE TABLE `sma_transfer_items` (
  `id` int(11) NOT NULL,
  `transfer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `net_unit_cost` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) DEFAULT NULL,
  `quantity_balance` decimal(15,4) NOT NULL,
  `unit_cost` decimal(25,4) DEFAULT NULL,
  `real_unit_cost` decimal(25,4) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_uom_cls`
--

DROP TABLE IF EXISTS `sma_uom_cls`;
CREATE TABLE `sma_uom_cls` (
  `id` bigint(20) NOT NULL,
  `uom_class_id` bigint(20) NOT NULL,
  `uom_class_name` varchar(50) NOT NULL,
  `usr_id_create` int(4) NOT NULL,
  `usr_id_create_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_uom_conv_std`
--

DROP TABLE IF EXISTS `sma_uom_conv_std`;
CREATE TABLE `sma_uom_conv_std` (
  `id` bigint(20) NOT NULL,
  `uom_id` varchar(20) NOT NULL,
  `uom_name` varchar(2) NOT NULL,
  `uom_desc` varchar(50) NOT NULL,
  `uom_class` bigint(20) NOT NULL,
  `base_uom` varchar(1) NOT NULL,
  `conv_fctr` decimal(26,6) NOT NULL,
  `actv` varchar(1) NOT NULL,
  `inactv_resn` varchar(200) NOT NULL,
  `inactv_date` date NOT NULL,
  `uom_std_ent_id` bigint(20) NOT NULL,
  `usr_id_create` int(4) NOT NULL,
  `usr_id_create_dt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_users`
--

DROP TABLE IF EXISTS `sma_users`;
CREATE TABLE `sma_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `last_ip_address` varbinary(45) DEFAULT NULL,
  `ip_address` varbinary(45) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `avatar` varchar(55) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `warehouse_id` varchar(255) DEFAULT NULL,
  `segment_id` int(11) DEFAULT NULL,
  `biller_id` int(10) UNSIGNED DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `show_cost` tinyint(1) DEFAULT '0',
  `show_price` tinyint(1) DEFAULT '0',
  `award_points` int(11) DEFAULT '0',
  `show_discount` tinyint(1) DEFAULT NULL,
  `upd_flg` tinyint(1) DEFAULT NULL,
  `usr_id` int(11) DEFAULT NULL,
  `pwid` varchar(255) DEFAULT NULL,
  `org_id` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `sma_users`
--
DROP TRIGGER IF EXISTS `delete_sma_users`;
DELIMITER $$
CREATE TRIGGER `delete_sma_users` BEFORE DELETE ON `sma_users` FOR EACH ROW BEGIN IF OLD.id = 1 OR OLD.id = 2 THEN

SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot delete locked record'; 

END IF; 

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sma_user_logins`
--

DROP TABLE IF EXISTS `sma_user_logins`;
CREATE TABLE `sma_user_logins` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_variants`
--

DROP TABLE IF EXISTS `sma_variants`;
CREATE TABLE `sma_variants` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_warehouses`
--

DROP TABLE IF EXISTS `sma_warehouses`;
CREATE TABLE `sma_warehouses` (
  `id` bigint(20) NOT NULL,
  `org_id` varchar(20) NOT NULL,
  `wh_id` varchar(20) NOT NULL,
  `wh_type` varchar(20) DEFAULT NULL,
  `usr_id_create` int(4) DEFAULT NULL,
  `usr_id_create_dt` date DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `wh_onrshp_type` int(5) DEFAULT NULL,
  `wh_strg_type` int(5) DEFAULT NULL,
  `adds_id` varchar(20) DEFAULT NULL,
  `wh_desc` varchar(1000) DEFAULT NULL,
  `actv` varchar(1) NOT NULL,
  `inactv_resn` varchar(200) DEFAULT NULL,
  `inactv_dt` date DEFAULT NULL,
  `wh_ent_id` int(20) NOT NULL,
  `prj_id` varchar(40) DEFAULT NULL,
  `segment_id` int(11) DEFAULT NULL,
  `biller_id` int(5) NOT NULL,
  `address` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_warehouses_old`
--

DROP TABLE IF EXISTS `sma_warehouses_old`;
CREATE TABLE `sma_warehouses_old` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `map` varchar(255) DEFAULT NULL,
  `phone` varchar(55) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL,
  `org_id` varchar(100) DEFAULT NULL,
  `biller_id` int(5) DEFAULT NULL,
  `segment_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_warehouses_products`
--

DROP TABLE IF EXISTS `sma_warehouses_products`;
CREATE TABLE `sma_warehouses_products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `lot_no` varchar(20) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `org_id` varchar(20) DEFAULT NULL,
  `wh_id` varchar(20) DEFAULT NULL,
  `grp_id` varchar(20) DEFAULT NULL,
  `rack` varchar(55) DEFAULT NULL,
  `psale` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_warehouses_products_variants`
--

DROP TABLE IF EXISTS `sma_warehouses_products_variants`;
CREATE TABLE `sma_warehouses_products_variants` (
  `id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `rack` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_warehouse_type`
--

DROP TABLE IF EXISTS `sma_warehouse_type`;
CREATE TABLE `sma_warehouse_type` (
  `id` int(11) NOT NULL,
  `wh_id` varchar(20) DEFAULT NULL,
  `wh_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `sma_warehouse_type` (`id`, `wh_id`, `wh_name`) VALUES
(1, '1', 'POS WAREHOUSE'),
(2, '2', 'CONSIGNMENT-STOCK'),
(3, '3', 'CONSIGNMENT-SALE'),
(4, '4', 'EXHIBITION');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sma_adjustments`
--
ALTER TABLE `sma_adjustments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_bin`
--
ALTER TABLE `sma_bin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_calendar`
--
ALTER TABLE `sma_calendar`
  ADD PRIMARY KEY (`date`);

--
-- Indexes for table `sma_captcha`
--
ALTER TABLE `sma_captcha`
  ADD PRIMARY KEY (`captcha_id`),
  ADD KEY `word` (`word`);

--
-- Indexes for table `sma_categories`
--
ALTER TABLE `sma_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_categories_new`
--
ALTER TABLE `sma_categories_new`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_combo_items`
--
ALTER TABLE `sma_combo_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_companies`
--
ALTER TABLE `sma_companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `group_id_2` (`group_id`);

--
-- Indexes for table `sma_cons_item_stk_lot`
--
ALTER TABLE `sma_cons_item_stk_lot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_cons_item_stk_lot_bin`
--
ALTER TABLE `sma_cons_item_stk_lot_bin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_cons_item_stk_profile`
--
ALTER TABLE `sma_cons_item_stk_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_costing`
--
ALTER TABLE `sma_costing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_country`
--
ALTER TABLE `sma_country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `sma_credit_voucher`
--
ALTER TABLE `sma_credit_voucher`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `card_no` (`card_no`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `sma_currencies`
--
ALTER TABLE `sma_currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_customer_groups`
--
ALTER TABLE `sma_customer_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_date_format`
--
ALTER TABLE `sma_date_format`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_deliveries`
--
ALTER TABLE `sma_deliveries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_drft_po`
--
ALTER TABLE `sma_drft_po`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_drft_po_dlv_schdl`
--
ALTER TABLE `sma_drft_po_dlv_schdl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_ds_att`
--
ALTER TABLE `sma_ds_att`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_ds_att_reln`
--
ALTER TABLE `sma_ds_att_reln`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_ds_att_type`
--
ALTER TABLE `sma_ds_att_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_emrs`
--
ALTER TABLE `sma_emrs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_emrs_doc_src`
--
ALTER TABLE `sma_emrs_doc_src`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_emrs_item`
--
ALTER TABLE `sma_emrs_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_expenses`
--
ALTER TABLE `sma_expenses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `register_id` (`register_id`);

--
-- Indexes for table `sma_gatepass_types`
--
ALTER TABLE `sma_gatepass_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_gift_cards`
--
ALTER TABLE `sma_gift_cards`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `card_no` (`card_no`);

--
-- Indexes for table `sma_gp`
--
ALTER TABLE `sma_gp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_gp_item`
--
ALTER TABLE `sma_gp_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_gp_item_lot`
--
ALTER TABLE `sma_gp_item_lot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_gp_lot_bin`
--
ALTER TABLE `sma_gp_lot_bin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_gp_src`
--
ALTER TABLE `sma_gp_src`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_gp_srl`
--
ALTER TABLE `sma_gp_srl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_groups`
--
ALTER TABLE `sma_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_item_profile`
--
ALTER TABLE `sma_item_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_item_stk_lot`
--
ALTER TABLE `sma_item_stk_lot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_item_stk_lot_bin`
--
ALTER TABLE `sma_item_stk_lot_bin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_item_stk_profile`
--
ALTER TABLE `sma_item_stk_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_item_stk_srl`
--
ALTER TABLE `sma_item_stk_srl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_item_warehouse_details`
--
ALTER TABLE `sma_item_warehouse_details`
  ADD PRIMARY KEY (`id`);



--
-- Indexes for table `sma_login_attempts`
--
ALTER TABLE `sma_login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_payments`
--
ALTER TABLE `sma_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_pos_cash_drawer`
--
ALTER TABLE `sma_pos_cash_drawer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_pos_register`
--
ALTER TABLE `sma_pos_register`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_products`
--
ALTER TABLE `sma_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_products_old`
--
ALTER TABLE `sma_products_old`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_purchase_items`
--
ALTER TABLE `sma_purchase_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_return_items`
--
ALTER TABLE `sma_return_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_return_sales`
--
ALTER TABLE `sma_return_sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_sales`
--
ALTER TABLE `sma_sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_sale_items`
--
ALTER TABLE `sma_sale_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_segment`
--
ALTER TABLE `sma_segment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_segment_ent`
--
ALTER TABLE `sma_segment_ent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_sessions`
--
ALTER TABLE `sma_sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_state`
--
ALTER TABLE `sma_state`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `sma_stock_bin`
--
ALTER TABLE `sma_stock_bin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_stock_item`
--
ALTER TABLE `sma_stock_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_stock_lot`
--
ALTER TABLE `sma_stock_lot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_suspended_bills`
--
ALTER TABLE `sma_suspended_bills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_suspended_items`
--
ALTER TABLE `sma_suspended_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_sync_table`
--
ALTER TABLE `sma_sync_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_tax_rates`
--
ALTER TABLE `sma_tax_rates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_uom_cls`
--
ALTER TABLE `sma_uom_cls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_uom_conv_std`
--
ALTER TABLE `sma_uom_conv_std`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_users`
--
ALTER TABLE `sma_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_user_logins`
--
ALTER TABLE `sma_user_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_variants`
--
ALTER TABLE `sma_variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_warehouses`
--
ALTER TABLE `sma_warehouses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_warehouses_products`
--
ALTER TABLE `sma_warehouses_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_warehouses_products_variants`
--
ALTER TABLE `sma_warehouses_products_variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_warehouse_type`
--
ALTER TABLE `sma_warehouse_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sma_adjustments`
--
ALTER TABLE `sma_adjustments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_bin`
--
ALTER TABLE `sma_bin`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `sma_categories`
--
ALTER TABLE `sma_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT for table `sma_combo_items`
--
ALTER TABLE `sma_combo_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_companies`
--
ALTER TABLE `sma_companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=556;
--
-- AUTO_INCREMENT for table `sma_cons_item_stk_lot`
--
ALTER TABLE `sma_cons_item_stk_lot`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_cons_item_stk_lot_bin`
--
ALTER TABLE `sma_cons_item_stk_lot_bin`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_cons_item_stk_profile`
--
ALTER TABLE `sma_cons_item_stk_profile`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_costing`
--
ALTER TABLE `sma_costing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sma_country`
--
ALTER TABLE `sma_country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;
--
-- AUTO_INCREMENT for table `sma_credit_voucher`
--
ALTER TABLE `sma_credit_voucher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_currencies`
--
ALTER TABLE `sma_currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sma_customer_groups`
--
ALTER TABLE `sma_customer_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sma_drft_po`
--
ALTER TABLE `sma_drft_po`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_drft_po_dlv_schdl`
--
ALTER TABLE `sma_drft_po_dlv_schdl`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_ds_att`
--
ALTER TABLE `sma_ds_att`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_ds_att_reln`
--
ALTER TABLE `sma_ds_att_reln`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_ds_att_type`
--
ALTER TABLE `sma_ds_att_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_emrs`
--
ALTER TABLE `sma_emrs`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_emrs_doc_src`
--
ALTER TABLE `sma_emrs_doc_src`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_emrs_item`
--
ALTER TABLE `sma_emrs_item`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_expenses`
--
ALTER TABLE `sma_expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_gatepass_types`
--
ALTER TABLE `sma_gatepass_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sma_gp`
--
ALTER TABLE `sma_gp`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_gp_item`
--
ALTER TABLE `sma_gp_item`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_gp_item_lot`
--
ALTER TABLE `sma_gp_item_lot`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_gp_lot_bin`
--
ALTER TABLE `sma_gp_lot_bin`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_gp_src`
--
ALTER TABLE `sma_gp_src`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_gp_srl`
--
ALTER TABLE `sma_gp_srl`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_groups`
--
ALTER TABLE `sma_groups`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sma_item_profile`
--
ALTER TABLE `sma_item_profile`
  MODIFY `id` bigint(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_item_stk_lot`
--
ALTER TABLE `sma_item_stk_lot`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3375;
--
-- AUTO_INCREMENT for table `sma_item_stk_lot_bin`
--
ALTER TABLE `sma_item_stk_lot_bin`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13597;
--
-- AUTO_INCREMENT for table `sma_item_stk_profile`
--
ALTER TABLE `sma_item_stk_profile`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2263;
--
-- AUTO_INCREMENT for table `sma_item_stk_srl`
--
ALTER TABLE `sma_item_stk_srl`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_item_warehouse_details`
--
ALTER TABLE `sma_item_warehouse_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_login_attempts`
--
ALTER TABLE `sma_login_attempts`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sma_mrn_rcpt_item`
--
ALTER TABLE `sma_mrn_rcpt_item`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_mrn_rcpt_item_lot`
--
ALTER TABLE `sma_mrn_rcpt_item_lot`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_mrn_rcpt_lot_bin`
--
ALTER TABLE `sma_mrn_rcpt_lot_bin`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_mrn_rcpt_src`
--
ALTER TABLE `sma_mrn_rcpt_src`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_mrn_receipt`
--
ALTER TABLE `sma_mrn_receipt`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_payments`
--
ALTER TABLE `sma_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sma_pos_cash_drawer`
--
ALTER TABLE `sma_pos_cash_drawer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_pos_register`
--
ALTER TABLE `sma_pos_register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sma_products`
--
ALTER TABLE `sma_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80558;
--
-- AUTO_INCREMENT for table `sma_products_old`
--
ALTER TABLE `sma_products_old`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80901;
--
-- AUTO_INCREMENT for table `sma_purchase_items`
--
ALTER TABLE `sma_purchase_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80558;
--
-- AUTO_INCREMENT for table `sma_return_items`
--
ALTER TABLE `sma_return_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_return_sales`
--
ALTER TABLE `sma_return_sales`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_sales`
--
ALTER TABLE `sma_sales`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sma_sale_items`
--
ALTER TABLE `sma_sale_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sma_segment`
--
ALTER TABLE `sma_segment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `sma_segment_ent`
--
ALTER TABLE `sma_segment_ent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_state`
--
ALTER TABLE `sma_state`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4226;
--
-- AUTO_INCREMENT for table `sma_stock_bin`
--
ALTER TABLE `sma_stock_bin`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_stock_item`
--
ALTER TABLE `sma_stock_item`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_stock_lot`
--
ALTER TABLE `sma_stock_lot`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_suspended_bills`
--
ALTER TABLE `sma_suspended_bills`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_suspended_items`
--
ALTER TABLE `sma_suspended_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_sync_table`
--
ALTER TABLE `sma_sync_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `sma_tax_rates`
--
ALTER TABLE `sma_tax_rates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13103;
--
-- AUTO_INCREMENT for table `sma_uom_cls`
--
ALTER TABLE `sma_uom_cls`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_uom_conv_std`
--
ALTER TABLE `sma_uom_conv_std`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_users`
--
ALTER TABLE `sma_users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `sma_user_logins`
--
ALTER TABLE `sma_user_logins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sma_variants`
--
ALTER TABLE `sma_variants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_warehouses`
--
ALTER TABLE `sma_warehouses`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT for table `sma_warehouses_products`
--
ALTER TABLE `sma_warehouses_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80558;
--
-- AUTO_INCREMENT for table `sma_warehouses_products_variants`
--
ALTER TABLE `sma_warehouses_products_variants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_warehouse_type`
--
ALTER TABLE `sma_warehouse_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


DROP TABLE IF EXISTS `sma_location`;
CREATE TABLE IF NOT EXISTS `sma_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `org_id` varchar(20) NOT NULL,
  `prj_doc_id` varchar(40) NOT NULL,
  `parent_proj_id` varchar(40) NOT NULL,
  `proj_name` varchar(100) NOT NULL,
  `prj_actv` varchar(1) NOT NULL,
  `pos_create_flg` varchar(2) DEFAULT NULL,
  `pos_upd_flg` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=256 ;

--
-- Dumping data for table `sma_segment`
--

INSERT INTO `sma_location` (`id`, `org_id`, `prj_doc_id`, `parent_proj_id`, `proj_name`, `prj_actv`, `pos_create_flg`, `pos_upd_flg`) VALUES
(1, '04', '0000.01.04.0001.00XR.00.1UNW6AkXP1', '0', 'VARANASI CO', 'Y', '1', NULL),
(2, '03', '0000.01.03.0001.00XR.00.1UNW6Arvxi', '0', 'HOSHANGABAD CO', 'Y', '1', NULL),
(3, '0K', 'PROJ0000', '0', 'Default Project', 'Y', '1', NULL),
(4, '0G', 'PROJ0000', '0', 'Default Project', 'Y', '1', NULL),
(5, '04', '0000.01.04.0001.00XR.00.1UNWOfvClN', '0000.01.04.0001.00XR.00.1UNWOftpA4', 'JATPURA POS', 'Y', '1', NULL),
(6, '0F', '0000.01.0F.0001.00XR.00.1UNW7CCsUh', '0', 'SAMBALPUR CO', 'Y', '1', NULL),
(7, '04', '0000.01.04.0001.00XR.00.1UKxc4hs1H', '0', 'FAIZABAD CO', 'Y', '1', NULL),
(8, '04', '0000.01.04.0001.00XR.00.1UNWT4jbAb', '0000.01.04.0001.00XR.00.1UNW6AkXP1', 'MARDAH POS', 'Y', '1', NULL),
(9, '04', '0000.01.04.0001.00XR.00.1UKxyxy6xk', '0000.01.04.0001.00XR.00.1UKxyxwQdc', 'sAHARANPUR POS', 'Y', '1', NULL),
(10, '07', 'PROJ0000', '0', 'Default Project', 'Y', '1', NULL),
(11, '08', '0000.01.08.0001.00XR.00.1UKzaL6wSe', '0', 'UDAIPUR CO', 'Y', '1', NULL),
(12, '03', 'PROJ0000', '0', 'Default Project', 'Y', '1', NULL),
(13, '03', '0000.01.03.0001.00XR.00.1UNW6BEZQl', '0000.01.03.0001.00XR.00.1UNW6AwdfD', 'SATNA POS', 'Y', '1', NULL),
(14, '03', '0000.01.03.0001.00XR.00.1UNW6AtBiP', '0000.01.03.0001.00XR.00.1UNW6Arvxi', 'VIDISHA POS', 'Y', '1', NULL),
(15, '0C', '0000.01.0C.0001.00XR.00.1UKzbQCOhX', '0', 'KOLHAPUR CO', 'Y', '1', NULL),
(16, '04', '0000.01.04.0001.00XR.00.1UNW6Al0Iw', '0000.01.04.0001.00XR.00.1UNW6AkXP1', 'PINDRA POS', 'Y', '1', NULL),
(17, '05', '0000.01.05.0001.00XR.00.1UNW6AcZW9', '0000.01.05.0001.00XR.00.1UKzaKSFNX', 'BAIJNATHPUR POS', 'Y', '1', NULL),
(18, '07', '0000.01.07.0001.00XR.00.1UNW7CFRJs', '0', 'ABOHAR CO', 'Y', '1', NULL),
(19, '0D', '0000.01.0D.0001.00XR.00.1UNWOfwnEg', '0000.01.0D.0001.00XR.00.1UNWOfwOLa', 'MADANPUR POS', 'Y', '1', NULL),
(20, '0D', '0000.01.0D.0001.00XR.00.1UKzaLDYP6', '0', 'BERHAMPUR CO', 'Y', '1', NULL),
(21, '03', '0000.01.03.0001.00XR.00.1UNW6Aprmi', '0000.01.03.0001.00XR.00.1UNW6Ap9ex', 'GOHAD POS', 'Y', '1', NULL),
(22, '05', '0000.01.05.0001.00XR.00.1UKzaKMGfS', '0000.01.05.0001.00XR.00.1UKzaKLLM4', 'HARSIDHI POS', 'Y', '1', NULL),
(23, '04', '0000.01.04.0001.00XR.00.1UNWT4jGjF', '0000.01.04.0001.00XR.00.1UNW6AkXP1', 'PINDRA POS', 'Y', '1', NULL),
(24, '0D', '0000.01.0D.0001.00XR.00.1UKzaLChl1', '0', 'BURDWAN CO', 'Y', '1', NULL),
(25, '03', '0000.01.03.0001.00XR.00.1UNW6AtNoR', '0000.01.03.0001.00XR.00.1UNW6Arvxi', 'GANJ BASODA POS', 'Y', '1', NULL),
(26, '0E', 'PROJ0000', '0', 'Default Project', 'Y', '1', NULL),
(27, '05', '0000.01.05.0001.00XR.00.1UNW6Af0mn', '0000.01.05.0001.00XR.00.1UKzaKJGod', 'DEEPNAGAR POS', 'Y', '1', NULL),
(28, '08', '0000.01.08.0001.00XR.00.1UKzaL30e0', '0', 'SRI GANGA NAGAR CO', 'Y', '1', NULL),
(29, '04', '0000.01.04.0001.00XR.00.1UNWOfuNeR', '0000.01.04.0001.00XR.00.1UNWOftpA4', 'CHHAJLET POS', 'Y', '1', NULL),
(30, '0D', '0000.01.0D.0001.00XR.00.1UNW6AoJAZ', '0000.01.0D.0001.00XR.00.1UKzaLElgI', 'BETAI POS', 'Y', '1', NULL),
(31, '04', '0000.01.04.0001.00XR.00.1UKxc4jxdZ', '0000.01.04.0001.00XR.00.1UKxc4iCfs', 'MATHURA MANDI POS', 'Y', '1', NULL),
(32, '04', '0000.01.04.0001.00XR.00.1UKxyxwQdc', '0', 'Saharanpur CO', 'Y', '1', NULL),
(33, '0H', '0000.01.0H.0001.00XR.00.1UNWOfsDdA', '0000.01.0H.0001.00XR.00.1UNWOfs5Or', 'AVNIGADA POS', 'Y', '1', NULL),
(34, '05', '0000.01.05.0001.00XR.00.1UKzaKKHfv', '0000.01.05.0001.00XR.00.1UKzaKJGod', 'NAYA BHOJPUR POS', 'Y', '1', NULL),
(35, '08', '0000.01.08.0001.00XR.00.1UKzaL5fZC', '0000.01.08.0001.00XR.00.1UKzaL5BUB', 'DHAULPUR POS', 'Y', '1', NULL),
(36, '05', '0000.01.05.0001.00XR.00.1UKzaKGTMQ', '0', 'Bhagalpur CO', 'Y', '1', NULL),
(37, '08', '0000.01.08.0001.00XR.00.1UKzaL4f7p', '0000.01.08.0001.00XR.00.1UKzaL3xXl', 'KAPREN POS', 'Y', '1', NULL),
(38, '0H', '0000.01.0H.0001.00XR.00.1UKzbQcO7U', '0', 'KAKINADA CO', 'Y', '1', NULL),
(39, '0I', '0000.01.0I.0001.00XR.00.1UNW7CHF5D', '0000.01.0I.0001.00XR.00.1UNW7CGn6V', 'GARREPALLY POS', 'Y', '1', NULL),
(40, '04', '0000.01.04.0001.00XR.00.1UNW6AlLdq', '0000.01.04.0001.00XR.00.1UNW6AkXP1', 'MARDAH POS', 'Y', '1', NULL),
(41, '02', '0000.01.02.0001.00XR.00.1UKzbQHhO1', '0000.01.02.0001.00XR.00.1UKxc4FOiL', 'SHAHZADPUR POS', 'Y', '1', NULL),
(42, '03', '0000.01.03.0001.00XR.00.1UNW6BF7gY', '0000.01.03.0001.00XR.00.1UNW6AwdfD', 'MANGAWAN POS', 'Y', '1', NULL),
(43, '05', '0000.01.05.0001.00XR.00.1UKzaKSFNX', '0', 'PURNIA CO', 'Y', '1', NULL),
(44, '0H', 'PROJ0000', '0', 'Default Project', 'Y', '1', NULL),
(45, '05', '0000.01.05.0001.00XR.00.1UKzaKUc1S', '0000.01.05.0001.00XR.00.1UKzaKLLM4', 'JAGDISHPUR POS', 'Y', '1', NULL),
(46, '07', '0000.01.07.0001.00XR.00.1UNW7CFtkJ', '0000.01.07.0001.00XR.00.1UNW7CF4rI', 'NAWANSHAHR POS', 'Y', '1', NULL),
(47, '03', '0000.01.03.0001.00XR.00.1UNW6BEklT', '0000.01.03.0001.00XR.00.1UNW6AwdfD', 'SEMARIYA POS', 'Y', '1', NULL),
(48, '03', '0000.01.03.0001.00XR.00.1UNW6AxBk2', '0000.01.03.0001.00XR.00.1UNW6AwdfD', 'CHATTARPUR POS', 'Y', '1', NULL),
(49, '02', '0000.01.02.0001.00XR.00.1UKxc4GDO1', '0000.01.02.0001.00XR.00.1UKxc4FOiL', 'NIGHDU POS', 'Y', '1', NULL),
(50, '08', '0000.01.08.0001.00XR.00.1UKzaL687s', '0000.01.08.0001.00XR.00.1UKzaL5BUB', 'TONK POS', 'Y', '1', NULL),
(51, '0A', 'PROJ0000', '0', 'Default Project', 'Y', '1', NULL),
(52, '04', '0000.01.04.0001.00XR.00.1UNW6AnQ7q', '0000.01.04.0001.00XR.00.1UNW6Am8IZ', 'BHEERPUR POS', 'Y', '1', NULL),
(53, '07', '0000.01.07.0001.00XR.00.1UNW7CGDtf', '0000.01.07.0001.00XR.00.1UNW7CFRJs', 'MUKTSAR FSC POS', 'Y', '1', NULL),
(54, '02', '0000.01.02.0001.00XR.00.1UKxc4DuWc', '0000.01.02.0001.00XR.00.1UKxc4CS5i', 'BAWANIKHERA POS', 'Y', '1', NULL),
(55, '05', 'PROJ0000', '0', 'Default Project', 'Y', '1', NULL),
(56, '04', '0000.01.04.0001.00XR.00.1UKxc4hShD', '0000.01.04.0001.00XR.00.1UKxc43tG9', 'SISOLI POS', 'Y', '1', NULL),
(57, '05', '0000.01.05.0001.00XR.00.1UNW6AeFyl', '0000.01.05.0001.00XR.00.1UKzaKJGod', 'KAMIRAON POS', 'Y', '1', NULL),
(58, '0J', 'PROJ0000', '0', 'Default Project', 'Y', '1', NULL),
(59, '0D', '0000.01.0D.0001.00XR.00.1UNWOfwOLa', '0', 'SILLIGURI CO', 'Y', '1', NULL),
(60, '04', '0000.01.04.0001.00XR.00.1UKxyxqSeY', '0', 'MEERUT CO', 'Y', '1', NULL),
(61, '0H', '0000.01.0H.0001.00XR.00.1UKzbQd0NP', '0000.01.0H.0001.00XR.00.1UKzbQcO7U', 'SANKHAVARAM POS', 'Y', '1', NULL),
(62, '02', '0000.01.02.0001.00XR.00.1UKxc4FALg', '0000.01.02.0001.00XR.00.1UKxc4E6RA', 'KULLAN POS', 'Y', '1', NULL),
(63, '0D', '0000.01.0D.0001.00XR.00.1UKzaLG4WA', '0000.01.0D.0001.00XR.00.1UKzaLDYP6', 'JADUPUR POS', 'Y', '1', NULL),
(64, '0C', 'PROJ0000', '0', 'Default Project', 'Y', '1', NULL),
(65, '05', '0000.01.05.0001.00XR.00.1UKzaKTvLz', '0000.01.05.0001.00XR.00.1UKzaKGTMQ', 'BALIA POS', 'Y', '1', NULL),
(66, '04', '0000.01.04.0001.00XR.00.1UNWOfupQJ', '0000.01.04.0001.00XR.00.1UNWOftpA4', 'GAJRAULA POS', 'Y', '1', NULL),
(67, '05', '0000.01.05.0001.00XR.00.1UKzaKVq5K', '0000.01.05.0001.00XR.00.1UKzaKSFNX', 'RANIGANJ POS', 'Y', '1', NULL),
(68, '02', '0000.01.02.0001.00XR.00.1UKxc4FOiL', '0', 'KARNAL CO', 'Y', '1', NULL),
(69, '04', '0000.01.04.0001.00XR.00.1UNW6AjWcE', '0000.01.04.0001.00XR.00.1UKxc43tG9', 'BUALNDSHAHR POS', 'Y', '1', NULL),
(70, '03', '0000.01.03.0001.00XR.00.1UNW6AtpP2', '0', 'JABALPUR CO', 'Y', '1', NULL),
(71, '03', '0000.01.03.0001.00XR.00.1UNWOfxY7W', '0', 'BINJAL CO', 'Y', '1', NULL),
(72, '0F', '0000.01.0F.0001.00XR.00.1UNW7CDklp', '0000.01.0F.0001.00XR.00.1UNW7CCsUh', 'RENGALI CAMP POS', 'Y', '1', NULL),
(73, '0D', '0000.01.0D.0001.00XR.00.1UKzaLFa7I', '0000.01.0D.0001.00XR.00.1UKzaLElgI', 'CHANDNESWAR POS', 'Y', '1', NULL),
(74, '04', '0000.01.04.0001.00XR.00.1UNW6AmezK', '0000.01.04.0001.00XR.00.1UNW6Am8IZ', 'HOLAGARH POS', 'Y', '1', NULL),
(75, '04', '0000.01.04.0001.00XR.00.1UNW6Am8IZ', '0', 'ALLAHABAD CO', 'Y', '1', NULL),
(76, '0D', '0000.01.0D.0001.00XR.00.1UKzaLGTtt', '0000.01.0D.0001.00XR.00.1UKzaLElgI', 'GANRAPOTA POS', 'Y', '1', NULL),
(77, '03', '0000.01.03.0001.00XR.00.1UNW6Avk6Y', '0000.01.03.0001.00XR.00.1UNW6AtpP2', 'BELKHEDA POS', 'Y', '1', NULL),
(78, '04', '0000.01.04.0001.00XR.00.1UNWRxNC8R', '0000.01.04.0001.00XR.00.1UKxc43tG9', 'DUMMY POS', 'Y', '1', NULL),
(79, '0D', 'PROJ0000', '0', 'Default Project', 'Y', '1', NULL),
(80, '04', '0000.01.04.0001.00XR.00.1UKxc44uJN', '0000.01.04.0001.00XR.00.1UKxc43tG9', 'Govindpuram POS', 'Y', '1', NULL),
(81, '0H', '0000.01.0H.0001.00XR.00.1UNWOfsdN9', '0000.01.0H.0001.00XR.00.1UNWOfs5Or', 'AMRAVATI POS', 'Y', '1', NULL),
(82, '0F', '0000.01.0F.0001.00XR.00.1UNW7CCRhe', '0', 'CUTTAK CO', 'Y', '1', NULL),
(83, '0D', '0000.01.0D.0001.00XR.00.1UKzaLE5pm', '0000.01.0D.0001.00XR.00.1UKzaLDYP6', 'BHARSALA MORE POS', 'Y', '1', NULL),
(84, '0B', 'PROJ0000', '0', 'Default Project', 'Y', '1', NULL),
(85, '08', '0000.01.08.0001.00XR.00.1UNW6AiNLG', '0000.01.08.0001.00XR.00.1UKzaL3xXl', 'SULTANPUR POS', 'Y', '1', NULL),
(86, '06', '0000.01.06.0001.00XR.00.1UNWOfyU3H', '0', 'SHIMLA CO', 'Y', '1', NULL),
(87, '02', '0000.01.02.0001.00XR.00.1UKzbQHDey', '0000.01.02.0001.00XR.00.1UKxc4FOiL', 'SANAULI POS', 'Y', '1', NULL),
(88, '0C', '0000.01.0C.0001.00XR.00.1UNW6AgnxZ', '0000.01.0C.0001.00XR.00.1UKzbQCOhX', 'KOLHAPUR POS', 'Y', '1', NULL),
(89, '03', '0000.01.03.0001.00XR.00.1UNW6AwdfD', '0', 'REWA CO', 'Y', '1', NULL),
(90, '02', '0000.01.02.0001.00XR.00.1UKxyxuHua', '0', 'GURGAON CO', 'Y', '1', NULL),
(91, '04', '0000.01.04.0001.00XR.00.1UKxc4l8uW', '0000.01.04.0001.00XR.00.1UKxc4iCfs', 'NAUJHIL POS', 'Y', '1', NULL),
(92, '02', '0000.01.02.0001.00XR.00.1UKxc4CS5i', '0', 'ROHTAK CO', 'Y', '1', NULL),
(93, '02', '0000.01.02.0001.00XR.00.1UKxc4Gd0w', '0', 'REWARI CO ', 'Y', '1', NULL),
(94, '02', '0000.01.02.0001.00XR.00.1UKxc4YZ3L', '0000.01.02.0001.00XR.00.1UKxc4Gd0w', 'HODAL POS', 'Y', '1', NULL),
(95, '04', '0000.01.04.0001.00XR.00.1UNWOfvY7P', '0000.01.04.0001.00XR.00.1UKxc4iCfs', 'MADHOGANJ POS', 'Y', '1', NULL),
(96, '0C', '0000.01.0C.0001.00XR.00.1UKzbQE3qw', '0000.01.0C.0001.00XR.00.1UKzbQCOhX', 'HAMIDWADA POS', 'Y', '1', NULL),
(97, '04', '0000.01.04.0001.00XR.00.1UNW6An3ue', '0000.01.04.0001.00XR.00.1UNW6Am8IZ', 'KORAON POS', 'Y', '1', NULL),
(98, '04', '0000.01.04.0001.00XR.00.1UKxc4iCfs', '0', 'AGRA CO', 'Y', '1', NULL),
(99, '0C', '0000.01.0C.0001.00XR.00.1UKzbQAFMz', '0000.01.0C.0001.00XR.00.1UKzbQ9gpj', 'JALNA POS', 'Y', '1', NULL),
(100, '0C', '0000.01.0C.0001.00XR.00.1UNW6AhBjr', '0', 'NASIK CO', 'Y', '1', NULL),
(101, '04', '0000.01.04.0001.00XR.00.1UKxc43tG9', '0', 'Meerut CO', 'Y', '1', NULL),
(102, '03', '0000.01.03.0001.00XR.00.1UNWOfy1iZ', '0000.01.03.0001.00XR.00.1UNW6Ar572', 'PIIPLIYANATH POS', 'Y', '1', NULL),
(103, '08', 'PROJ0000', '0', 'Default Project', 'Y', '1', NULL),
(104, '02', '0000.01.02.0001.00XR.00.1UNW6AgAS4', '0000.01.02.0001.00XR.00.1UKxc4E6RA', 'NARNAUND POS', 'Y', '1', NULL),
(105, '02', '0000.01.02.0001.00XR.00.1UKxc4Yye5', '0000.01.02.0001.00XR.00.1UKxc4Gd0w', 'BERI POS', 'Y', '1', NULL),
(106, '07', '0000.01.07.0001.00XR.00.1UNW7CF4rI', '0', 'JALANDHAR CO', 'Y', '1', NULL),
(107, '05', '0000.01.05.0001.00XR.00.1UKzaKV2W4', '0000.01.05.0001.00XR.00.1UKzaKSFNX', 'SINGHESHWAR POS', 'Y', '1', NULL),
(108, '0F', 'PROJ0000', '0', 'Default Project', 'Y', '1', NULL),
(109, '08', '0000.01.08.0001.00XR.00.1UKzaL5BUB', '0', 'JAIPUR CO', 'Y', '1', NULL),
(110, '04', '0000.01.04.0001.00XR.00.1UNWOftpA4', '0', 'MORADABAD CO', 'Y', '1', NULL),
(111, '05', '0000.01.05.0001.00XR.00.1UKzaKIqln', '0000.01.05.0001.00XR.00.1UKzaKGTMQ', 'PIRPAITI POS', 'Y', '1', NULL),
(112, '08', '0000.01.08.0001.00XR.00.1UKzaL3X2h', '0000.01.08.0001.00XR.00.1UKzaL30e0', 'KANCHIYA POS', 'Y', '1', NULL),
(113, '0I', '0000.01.0I.0001.00XR.00.1UNW7CGn6V', '0', 'KARIMNAGAR CO', 'Y', '1', NULL),
(114, '04', '0000.01.04.0001.00XR.00.1UNWT4iqIc', '0', 'VARANASI CO', 'Y', '1', NULL),
(115, '08', '0000.01.08.0001.00XR.00.1UKzaL3xXl', '0', 'KOTA CO', 'Y', '1', NULL),
(116, '03', '0000.01.03.0001.00XR.00.1UNW6ArUvc', '0000.01.03.0001.00XR.00.1UNW6Ar572', 'MANDSAUR POS', 'Y', '1', NULL),
(117, '01', 'PROJ0000', '0', 'Default Project', 'Y', '1', NULL),
(118, '0L', 'PROJ0000', '0', 'Default Project', 'Y', '1', NULL),
(119, '01', '0000.01.01.0001.00XR.00.1UKxWZnKOw', '0000.01.01.0001.00XR.00.1UKxWZlIxY', 'GOVINDPURAM POS', 'N', '1', NULL),
(120, '06', '0000.01.06.0001.00XR.00.1UNWOfypO0', '0000.01.06.0001.00XR.00.1UNWOfyU3H', 'UNA POS', 'Y', '1', NULL),
(121, '02', '0000.01.02.0001.00XR.00.1UKxc4ZOpX', '0000.01.02.0001.00XR.00.1UKxc4FOiL', 'KHIZRABAD POS', 'Y', '1', NULL),
(122, '0H', '0000.01.0H.0001.00XR.00.1UNWOfs5Or', '0', 'VIJAYWADA CO', 'Y', '1', NULL),
(123, '03', '0000.01.03.0001.00XR.00.1UNW6Ar572', '0', 'UJJAIN CO', 'Y', '1', NULL),
(124, '04', '0000.01.04.0001.00XR.00.1UKxc4ge73', '0000.01.04.0001.00XR.00.1UKxc43tG9', 'TITARI POS', 'Y', '1', NULL),
(125, '02', 'PROJ0000', '0', 'Default Project', 'Y', '1', NULL),
(126, '04', 'PROJ0000', '0', 'Default Project', 'Y', '1', NULL),
(127, '03', '0000.01.03.0001.00XR.00.1UNW6AuSm1', '0000.01.03.0001.00XR.00.1UNW6AtpP2', 'PANAGAR POS', 'Y', '1', NULL),
(128, '08', '0000.01.08.0001.00XR.00.1UKzaL6XHY', '0000.01.08.0001.00XR.00.1UKzaL5BUB', 'SIKANDRA POS', 'Y', '1', NULL),
(129, '03', '0000.01.03.0001.00XR.00.1UNW6Avva8', '0000.01.03.0001.00XR.00.1UNW6AtpP2', 'SHAHPURA POS', 'Y', '1', NULL),
(130, '08', '0000.01.08.0001.00XR.00.1UKzaL7d3z', '0000.01.08.0001.00XR.00.1UKzaL6wSe', 'PRATAPGARH POS', 'Y', '1', NULL),
(131, '05', '0000.01.05.0001.00XR.00.1UKzaKJqQT', '0000.01.05.0001.00XR.00.1UKzaKJGod', 'TRIKALPUR POS', 'Y', '1', NULL),
(132, '0H', '0000.01.0H.0001.00XR.00.1UKzbQdSAz', '0', 'NELLORE CO', 'Y', '1', NULL),
(133, '02', '0000.01.02.0001.00XR.00.1UNW6AfoH1', '0000.01.02.0001.00XR.00.1UKxc4E6RA', 'UKLANA RURAL POS', 'Y', '1', NULL),
(134, '04', '0000.01.04.0001.00XR.00.1UKxc4h4GW', '0000.01.04.0001.00XR.00.1UKxc43tG9', 'AMINAGAR POS', 'Y', '1', NULL),
(135, '03', '0000.01.03.0001.00XR.00.1UNW6AqjKy', '0000.01.03.0001.00XR.00.1UNW6AqJ8m', 'ANARAD POS', 'Y', '1', NULL),
(136, '02', '0000.01.02.0001.00XR.00.1UKxc4E6RA', '0', 'FATEHBAD CO', 'Y', '1', NULL),
(137, '0F', '0000.01.0F.0001.00XR.00.1UNW7CDPMF', '0000.01.0F.0001.00XR.00.1UNW7CCRhe', 'DOLASAHI POS', 'Y', '1', NULL),
(138, '05', '0000.01.05.0001.00XR.00.1UKzaKT4wd', '0000.01.05.0001.00XR.00.1UKzaKSFNX', 'GERABADI POS', 'Y', '1', NULL),
(139, '0C', '0000.01.0C.0001.00XR.00.1UKzbQB1Tr', '0000.01.0C.0001.00XR.00.1UKzbQ9gpj', 'AURANGABAD MANDI POS', 'Y', '1', NULL),
(140, '04', '0000.01.04.0001.00XR.00.1UKxc4jZRJ', '0000.01.04.0001.00XR.00.1UKxc4hs1H', 'RAMPUR HALWARA POS', 'Y', '1', NULL),
(141, '0D', '0000.01.0D.0001.00XR.00.1UKzaLElgI', '0', 'KALYANI CO', 'Y', '1', NULL),
(142, '0D', '0000.01.0D.0001.00XR.00.1UNW6AnvSy', '0000.01.0D.0001.00XR.00.1UKzaLDYP6', 'HIYATNAGAR POS', 'Y', '1', NULL),
(143, '09', 'PROJ0000', '0', 'Default Project', 'Y', '1', NULL),
(144, '03', '0000.01.03.0001.00XR.00.1UNWOfxhuD', '0000.01.03.0001.00XR.00.1UNW6Ar572', 'NANDED POS', 'Y', '1', NULL),
(145, '04', '0000.01.04.0001.00XR.00.1UNW6Aj9Lb', '0000.01.04.0001.00XR.00.1UKxc43tG9', 'MEERUT POS', 'Y', '1', NULL),
(146, '03', '0000.01.03.0001.00XR.00.1UNW6Ap9ex', '0', 'GWALIOR CO', 'Y', '1', NULL),
(147, '06', 'PROJ0000', '0', 'Default Project', 'Y', '1', NULL),
(148, '0I', 'PROJ0000', '0', 'Default Project', 'Y', '1', NULL),
(149, '05', '0000.01.05.0001.00XR.00.1UNW6AdMrI', '0', 'DUMRAON NAYA BHOJPUR POS', 'Y', '1', NULL),
(150, '03', '0000.01.03.0001.00XR.00.1UNW6ApY1G', '0000.01.03.0001.00XR.00.1UNW6Ap9ex', 'BHIND POS', 'Y', '1', NULL),
(151, '0C', '0000.01.0C.0001.00XR.00.1UNW6AhdBP', '0000.01.0C.0001.00XR.00.1UNW6AhBjr', 'NASIK POS', 'Y', '1', NULL),
(152, '05', '0000.01.05.0001.00XR.00.1UKzaKJGod', '0', 'PATNA CO', 'Y', '1', NULL),
(153, '0H', '0000.01.0H.0001.00XR.00.1UKzbQdwYs', '0000.01.0H.0001.00XR.00.1UKzbQdSAz', 'NORTH RAJUPALEM POS', 'Y', '1', NULL),
(154, '02', '0000.01.02.0001.00XR.00.1UKxc4YDGv', '0000.01.02.0001.00XR.00.1UKxc4Gd0w', 'HATHIN POS', 'Y', '1', NULL),
(155, '05', '0000.01.05.0001.00XR.00.1UKzaKLLM4', '0', 'MUZAFFARPUR CO', 'Y', '1', NULL),
(156, '04', '0000.01.04.0001.00XR.00.1UKxc4jP3m', '0000.01.04.0001.00XR.00.1UKxc4hs1H', 'PARASPUR GONDA POS', 'Y', '1', NULL),
(157, '0C', '0000.01.0C.0001.00XR.00.1UKzbQ9gpj', '0', 'AURANGABAD CO', 'Y', '1', NULL),
(158, '0D', '0000.01.0D.0001.00XR.00.1UKzaLD36T', '0000.01.0D.0001.00XR.00.1UKzaLChl1', 'MUTHADANGA POS', 'Y', '1', NULL),
(159, '03', '0000.01.03.0001.00XR.00.1UNW6AqJ8m', '0', 'INDORE CO', 'Y', '1', NULL),
(160, '0A', '0000.01.0A.0001.00XR.00.1UNXo3PGxv', '0000.01.0A.0001.00XR.00.1UNXo3Oote', 'DHAMDHA POS', 'Y', '1', NULL),
(161, '0G', '0000.01.0G.0001.00XR.00.1UNXwk0prE', '0000.01.0G.0001.00XR.00.1UNXwk0IRy', 'BELGAUM POS', 'Y', '1', NULL),
(162, '0I', '0000.01.0I.0001.00XR.00.1UNXxrC8ub', '0000.01.0I.0001.00XR.00.1UNXp7TG6S', 'GUDEBALLUR POS', 'Y', '1', NULL),
(163, '0A', '0000.01.0A.0001.00XR.00.1UNXo3Oote', '0', 'RAIPUR CO', 'Y', '1', NULL),
(164, '0I', '0000.01.0I.0001.00XR.00.1UNXwlTPqn', '0000.01.0I.0001.00XR.00.1UNXp7TG6S', 'MAHABUBNAGAR POS', 'N', '1', NULL),
(165, '0I', '0000.01.0I.0001.00XR.00.1UNXwk1fCA', '0000.01.0I.0001.00XR.00.1UNXp7TG6S', 'MAGANOOR POS', 'N', '1', NULL),
(166, '0G', '0000.01.0G.0001.00XR.00.1UNXwk0IRy', '0', 'HUBLI CO', 'Y', '1', NULL),
(167, '0I', '0000.01.0I.0001.00XR.00.1UNXp7TT6h', '0', 'KODAR POS', 'Y', '1', NULL),
(168, '0I', '0000.01.0I.0001.00XR.00.1UNXp7TG6S', '0', 'HYDERABAD CO', 'Y', '1', NULL),
(169, '0D', '0000.01.0D.0001.00XR.00.1UNZaEWEoq', '0000.01.0D.0001.00XR.00.1UKzaLChl1', 'MUTHADANGA WHOLESALE POS', 'Y', '1', NULL),
(170, '02', '0000.01.02.0001.00XR.00.1UNZj2UkLp', '0000.01.02.0001.00XR.00.1UKxc4CS5i', 'MEHAM POS', 'Y', '1', NULL),
(171, '0D', '0000.01.0D.0001.00XR.00.1UNZk701PH', '0000.01.0D.0001.00XR.00.1UKzaLElgI', 'BERAKAMGACHI POS', 'Y', '1', NULL),
(172, '0D', '0000.01.0D.0001.00XR.00.1UNZk7U9On', '0000.01.0D.0001.00XR.00.1UKzaLDYP6', 'BHARSALAMORE WHOLESALE POS', 'Y', '1', NULL),
(173, '0D', '0000.01.0D.0001.00XR.00.1UNZk7VJDD', '0000.01.0D.0001.00XR.00.1UKzaLElgI', 'CHANDNESWAR WHOLESALE POS', 'Y', '1', NULL),
(174, '0D', '0000.01.0D.0001.00XR.00.1UNZk7kVQg', '0000.01.0D.0001.00XR.00.1UKzaLElgI', 'BETAI WHOLESALE POS', 'Y', '1', NULL),
(175, '0D', '0000.01.0D.0001.00XR.00.1UNZk7i7DL', '0000.01.0D.0001.00XR.00.1UKzaLElgI', 'GANRAPOTA WHOLESALE POS', 'Y', '1', NULL),
(176, '0D', '0000.01.0D.0001.00XR.00.1UNZk7io47', '0000.01.0D.0001.00XR.00.1UKzaLDYP6', 'HIYATNAGAR WHOLESALE POS', 'Y', '1', NULL),
(177, '0D', '0000.01.0D.0001.00XR.00.1UNZk7hVmT', '0000.01.0D.0001.00XR.00.1UKzaLDYP6', 'JADUPUR WHOLESALE POS', 'Y', '1', NULL),
(178, '0D', '0000.01.0D.0001.00XR.00.1UNZk7kuzv', '0000.01.0D.0001.00XR.00.1UNWOfwOLa', 'MADANPUR WHOLESALE POS', 'Y', '1', NULL),
(179, '0D', '0000.01.0D.0001.00XR.00.1UNZk7lThK', '0000.01.0D.0001.00XR.00.1UKzaLElgI', 'BERAKAMGACHI WHOLESALE POS', 'Y', '1', NULL),
(180, '0D', '0000.01.0D.0001.00XR.00.1UNZnNK5Nv', '0', 'HASSAN CO', 'Y', '1', NULL),
(181, '0D', '0000.01.0D.0001.00XR.00.1UNZnNKqZp', '0000.01.0D.0001.00XR.00.1UNZnNK5Nv', 'TARIKERE POS', 'Y', '1', NULL),
(182, '06', '0000.01.06.0001.00XR.00.1UNZnNLbXj', '0000.01.06.0001.00XR.00.1UNWOfyU3H', 'BHUNTAR POS', 'Y', '1', NULL),
(183, '0G', '0000.01.0G.0001.00XR.00.1UNZnNmqqA', '0', 'HASSAN CO', 'Y', '1', NULL),
(184, '0G', '0000.01.0G.0001.00XR.00.1UNZnNnJZS', '0000.01.0G.0001.00XR.00.1UNZnNmqqA', 'TARIKERE POS', 'Y', '1', NULL),
(185, '05', '0000.01.05.0001.00XR.00.1UNZqbXWd2', '0000.01.05.0001.00XR.00.1UKzaKLLM4', 'PARU POS', 'Y', '1', NULL),
(186, '0C', '0000.01.0C.0001.00XR.00.1UNZqd2kHU', '0000.01.0C.0001.00XR.00.1UKzbQ9gpj', 'JALNA WHOLESALE POS', 'Y', '1', NULL),
(187, '0C', '0000.01.0C.0001.00XR.00.1UNZqd39Ha', '0000.01.0C.0001.00XR.00.1UKzbQCOhX', 'KOLHAPUR WHOLESALE POS', 'Y', '1', NULL),
(188, '0C', '0000.01.0C.0001.00XR.00.1UNZqd1oJz', '0000.01.0C.0001.00XR.00.1UKzbQ9gpj', 'AURANGABAD WHOLESALE POS', 'Y', '1', NULL),
(189, '0C', '0000.01.0C.0001.00XR.00.1UNZqd3XVW', '0000.01.0C.0001.00XR.00.1UNW6AhBjr', 'NASIK WHOLESALE POS', 'N', '1', NULL),
(256, '0L', '0000.01.0L.0001.00XR.00.1UNZw5TCjI', '0', 'DEOGHAR CO', 'Y', '1', NULL),
(257, '0L', '0000.01.0L.0001.00XR.00.1UNZw6YiQC', '0000.01.0L.0001.00XR.00.1UNZw5TCjI', 'SARWAN POS', 'Y', '1', NULL),
(259, '0H', '0000.01.0H.0001.00XR.00.1UNZx9WFbG', '0000.01.0H.0001.00XR.00.1UNWOfs5Or', 'PARCHUR POS', 'Y', '1', NULL),
(260, '05', '0000.01.05.0001.00XR.00.1UNZx9fZbJ', '0000.01.05.0001.00XR.00.1UKzaKLLM4', 'SAHEBGANJ POS', 'Y', '1', NULL),
(261, '05', '0000.01.05.0001.00XR.00.1UNZxADfuO', '0000.01.05.0001.00XR.00.1UKzaKLLM4', 'KALYANPUR POS', 'Y', '1', NULL),
(262, '05', '0000.01.05.0001.00XR.00.1UNZxAMqmD', '0', 'WEST CHAMPARAN CO', 'Y', '1', NULL),
(263, '05', '0000.01.05.0001.00XR.00.1UNZxANhej', '0000.01.05.0001.00XR.00.1UNZxAMqmD', 'YOGAPATTI POS', 'Y', '1', NULL),
(264, '05', '0000.01.05.0001.00XR.00.1UNZxAXJlA', '0000.01.05.0001.00XR.00.1UKzaKJGod', 'GAMHARIYA POS', 'Y', '1', NULL),
(265, '0A', '0000.01.0A.0001.00XR.00.1UNZxAz3Gg', '0', 'BILASPUR CO', 'Y', '1', NULL),
(266, '0A', '0000.01.0A.0001.00XR.00.1UNZxB0Ecx', '0000.01.0A.0001.00XR.00.1UNZxAz3Gg', 'AMBIKAPUR POS', 'Y', '1', NULL),
(267, '03', '0000.01.03.0001.00XR.00.1UNZxB8INX', '0000.01.03.0001.00XR.00.1UNW6Arvxi', 'RAISEN POS', 'Y', '1', NULL),
(268, '03', '0000.01.03.0001.00XR.00.1UNZxB7L3u', '0000.01.03.0001.00XR.00.1UNW6Arvxi', 'REHTI POS', 'Y', '1', NULL),
(269, '03', '0000.01.03.0001.00XR.00.1UNZxB85gG', '0000.01.03.0001.00XR.00.1UNW6Arvxi', 'SEHORE POS', 'Y', '1', NULL),
(270, '03', '0000.01.03.0001.00XR.00.1UNZxBB6ju', '0000.01.03.0001.00XR.00.1UNW6Arvxi', 'SIRALI POS', 'Y', '1', NULL),
(271, '03', '0000.01.03.0001.00XR.00.1UNZxBCI6i', '0000.01.03.0001.00XR.00.1UNW6Arvxi', 'HOSHANGABAD POS', 'Y', '1', NULL),
(272, '03', '0000.01.03.0001.00XR.00.1UNZxBAN8m', '0000.01.03.0001.00XR.00.1UNW6Arvxi', 'SEMARI HARCHAND POS', 'Y', '1', NULL),
(273, '03', '0000.01.03.0001.00XR.00.1UNZxB9ZfL', '0000.01.03.0001.00XR.00.1UNW6Arvxi', 'BABAI POS', 'Y', '1', NULL),
(274, '03', '0000.01.03.0001.00XR.00.1UNZxB92af', '0000.01.03.0001.00XR.00.1UNW6Arvxi', 'KHEDA ITARSI POS', 'Y', '1', NULL),
(275, '03', '0000.01.03.0001.00XR.00.1UNZxBBuPI', '0000.01.03.0001.00XR.00.1UNW6Arvxi', 'HARDA POS', 'Y', '1', NULL),
(276, '03', '0000.01.03.0001.00XR.00.1UNZxBklCc', '0000.01.03.0001.00XR.00.1UNW6Arvxi', 'ICCHAWAR POS', 'N', '1', NULL),
(277, '03', '0000.01.03.0001.00XR.00.1UNZxBndBn', '0000.01.03.0001.00XR.00.1UNW6Arvxi', 'BARELI POS', 'Y', '1', NULL),
(278, '03', '0000.01.03.0001.00XR.00.1UNZxBlfpT', '0000.01.03.0001.00XR.00.1UNW6Arvxi', 'BODA POS', 'Y', '1', NULL),
(279, '03', '0000.01.03.0001.00XR.00.1UNZxBoTqe', '0000.01.03.0001.00XR.00.1UNW6Arvxi', 'GADAGHAT POS', 'Y', '1', NULL),
(280, '03', '0000.01.03.0001.00XR.00.1UNZxBozhI', '0000.01.03.0001.00XR.00.1UNW6Arvxi', 'PACHOR POS', 'Y', '1', NULL),
(281, '03', '0000.01.03.0001.00XR.00.1UNZxBmVsW', '0000.01.03.0001.00XR.00.1UNW6Arvxi', 'UDAIPURA POS', 'Y', '1', NULL),
(282, '03', '0000.01.03.0001.00XR.00.1UNZxBo6Gl', '0000.01.03.0001.00XR.00.1UNW6Arvxi', 'GAIRATGANJ POS', 'Y', '1', NULL),
(283, '03', '0000.01.03.0001.00XR.00.1UNZxBnE70', '0000.01.03.0001.00XR.00.1UNW6Arvxi', 'SARANGPUR POS', 'Y', '1', NULL),
(284, '03', '0000.01.03.0001.00XR.00.1UNZxBpoqE', '0000.01.03.0001.00XR.00.1UNW6AtpP2', 'CHINDWADA POS', 'Y', '1', NULL),
(285, '03', '0000.01.03.0001.00XR.00.1UNZxBpNho', '0000.01.03.0001.00XR.00.1UNW6Arvxi', 'BIORA POS', 'Y', '1', NULL);
--
-- Database: `iffco_final`
--

-- --------------------------------------------------------

--
-- Table structure for table `sma_sync_table`
--

DROP TABLE IF EXISTS `sma_sync_table`;
CREATE TABLE `sma_sync_table` (
  `id` int(11) NOT NULL,
  `table_name` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sma_sync_table`
--

INSERT INTO `sma_sync_table` (`id`, `table_name`, `status`) VALUES
(1, 'sma_segment', 1),
(2, 'sma_warehouses', 1),
(3, 'sma_users', 1),
(4, 'sma_products', 1),
(5, 'sma_purchase_items', 1),
(6, 'sma_warehouses_products', 1),
(7, 'sma_item_stk_profile', 1),
(8, 'sma_item_stk_lot', 1),
(9, 'sma_item_stk_lot_bin', 1),
(10, 'sma_tax_rates', 1),
(11, 'sma_emrs', 1),
(12, 'sma_emrs_doc_src', 1),
(13, 'sma_emrs_item', 1),
(14, 'sma_bin', 1),
(15, 'sma_categories', 1),
(16, 'sma_companies', 1),
(17, 'sma_currencies', 1),
(18, 'sma_drft_po_dlv_schdl', 1),
(19, 'sma_drft_po', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sma_sync_table`
--
ALTER TABLE `sma_sync_table`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sma_sync_table`
--
ALTER TABLE `sma_sync_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

ALTER TABLE `sma_pos_register` ADD `sync_flg` VARCHAR(2) NULL DEFAULT NULL AFTER `closed_by`;
ALTER TABLE `sma_return_sales` ADD `sync_flg` VARCHAR(2) NULL DEFAULT NULL AFTER `updated_at`;
ALTER TABLE `sma_return_items` ADD `sync_flg` VARCHAR(2) NULL DEFAULT NULL AFTER `serial_no`;
ALTER TABLE `sma_gp` ADD `sync_flg` VARCHAR(2) NULL DEFAULT NULL AFTER `json_lotdata`;
ALTER TABLE `sma_gp_item` ADD `sync_flg` VARCHAR(2) NULL DEFAULT NULL AFTER `item_amt`;
ALTER TABLE `sma_gp_item_lot` ADD `sync_flg` VARCHAR(2) NULL DEFAULT NULL AFTER `pending_qty`;
ALTER TABLE `sma_gp_lot_bin` ADD `sync_flg` VARCHAR(2) NULL DEFAULT NULL AFTER `pending_qty`;
ALTER TABLE `sma_gp_src` ADD `sync_flg` VARCHAR(2) NULL DEFAULT NULL AFTER `doc_date`;
ALTER TABLE `sma_costing` ADD `sync_flg` VARCHAR(2) NULL DEFAULT NULL AFTER `option_id`, ADD `upd_flg` VARCHAR(2) NULL DEFAULT NULL AFTER `sync_flg`;


ALTER TABLE `sma_drft_po_dlv_schdl` ADD `upd_flg` VARCHAR(2) NULL DEFAULT NULL AFTER `use_rented_wh`;



DROP TABLE IF EXISTS `sma_syncout_table`;
CREATE TABLE `sma_syncout_table` (
  `id` int(11) NOT NULL,
  `table_name` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sma_syncout_table`
--

INSERT INTO `sma_syncout_table` (`id`, `table_name`, `status`) VALUES
(1, 'sma_sales', 1),
(2, 'sma_sale_items', 1),
(3, 'sma_return_sales', 1),
(4, 'sma_return_items', 1),
(5, 'sma_payments', 1),
(6, 'sma_item_stk_lot', 1),
(7, 'sma_item_stk_lot_bin', 1),
(8, 'sma_item_stk_profile', 1),
(9, 'sma_drft_po', 1),
(10, 'sma_drft_po_dlv_schdl', 1),
(11, 'sma_cons_item_stk_lot', 1),
(12, 'sma_cons_item_stk_lot_bin', 1),
(13, 'sma_cons_item_stk_profile', 1),
(14, 'sma_gp', 1),
(15, 'sma_gp_item', 1),
(16, 'sma_gp_item_lot', 1),
(17, 'sma_gp_lot_bin', 1),
(18, 'sma_gp_src', 1),
(19, 'sma_mrn_receipt', 1),
(20, 'sma_mrn_rcpt_src', 1),
(21, 'sma_mrn_rcpt_item', 1),
(22, 'sma_mrn_rcpt_item_lot', 1),
(23, 'sma_mrn_rcpt_lot_bin', 1),
(24, 'sma_emrs_item', 1),
(25, 'sma_companies', 1),
(26, 'sma_costing', 1),
(27, 'sma_pos_register', 1);

--
-- Indexes for dumped tables
--

ALTER TABLE `sma_products` ADD `bat_item_id` VARCHAR(2) NULL AFTER `psale`;
ALTER TABLE `sma_purchase_items` ADD `bat_item_id` VARCHAR(2) NULL AFTER `psale`;


DROP TABLE IF EXISTS `sma_sale_item_bin`;
CREATE TABLE `sma_sale_item_bin` (
 `id` bigint(20) NOT NULL AUTO_INCREMENT,
 `org_id` varchar(255) DEFAULT NULL,
 `wh_id` varchar(20) DEFAULT NULL,
 `warehouse_id` int(20) DEFAULT NULL,
 `code` varchar(255) DEFAULT NULL,
 `sale_id` bigint(20) NOT NULL,
 `sale_item_id` bigint(20) NOT NULL,
 `lot_id` bigint(20) DEFAULT NULL,
 `bin_id` bigint(20) DEFAULT NULL,
 `bin_name` varchar(255) NOT NULL,
 `quantity` decimal(26,6) NOT NULL,
 `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `sma_sale_item_lot`;


CREATE TABLE `sma_sale_item_lot` (
 `id` bigint(20) NOT NULL AUTO_INCREMENT,
 `org_id` varchar(255) DEFAULT NULL,
 `wh_id` varchar(20) DEFAULT NULL,
 `warehouse_id` bigint(20) NOT NULL,
 `code` varchar(255) DEFAULT NULL,
 `sale_id` bigint(20) NOT NULL,
 `sale_item_id` bigint(20) NOT NULL,
 `lot_id` bigint(20) DEFAULT NULL,
 `lot_no` varchar(255) DEFAULT NULL,
 `quantity` decimal(26,6) NOT NULL,
 `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

INSERT INTO `iffco_final`.`sma_syncout_table` (`id`, `table_name`, `status`) VALUES (28, 'sma_sale_item_lot', '1');
INSERT INTO `iffco_final`.`sma_syncout_table` (`id`, `table_name`, `status`) VALUES (29, 'sma_sale_item_bin', '1');

ALTER TABLE `sma_mrn_rcpt_item` CHANGE `item_uom_id` `item_uom_id` VARCHAR(255) NULL DEFAULT NULL;
ALTER TABLE `sma_mrn_rcpt_item_lot` CHANGE `itm_uom_id` `itm_uom_id` VARCHAR(255) NULL DEFAULT NULL;
ALTER TABLE `sma_mrn_rcpt_lot_bin` CHANGE `itm_uom_id` `itm_uom_id` VARCHAR(255) NULL DEFAULT NULL;
ALTER TABLE `sma_sale_item_lot` ADD `sync_flg` VARCHAR(2) NULL DEFAULT NULL AFTER `created_date`;
ALTER TABLE `sma_sale_item_bin` ADD `sync_flg` VARCHAR(2) NULL DEFAULT NULL AFTER `created_date`;
ALTER TABLE `sma_companies` ADD `adhar_card_no` VARCHAR(255) NULL AFTER `pos_upd_flg`;
ALTER TABLE `sma_sales` CHANGE `customer_id` `customer_id` BIGINT(20) NOT NULL;

INSERT INTO `sma_syncout_table` (`id`, `table_name`, `status`) VALUES ('31', 'sma_stk_adjt', '1'),('32',  'sma_stk_adjt_itm', '1'),('33', 'sma_stk_adjt_lot', '1'),('34', 'sma_stk_adjt_bin', '1');

DROP TABLE IF EXISTS sma_gp;
DROP TABLE IF EXISTS sma_gp_src;
DROP TABLE IF EXISTS sma_gp_item;
DROP TABLE IF EXISTS sma_gp_item_lot;
DROP TABLE IF EXISTS sma_gp_lot_bin;
DROP TABLE IF EXISTS sma_cons_item_stk_profile;
DROP TABLE IF EXISTS sma_cons_item_stk_lot;
DROP TABLE IF EXISTS sma_cons_item_stk_lot_bin;

CREATE TABLE `sma_gp` (
 `id` bigint(11) NOT NULL AUTO_INCREMENT,
 `ho_id` int(11) NOT NULL,
 `org_id` varchar(20) NOT NULL,
 `wh_id` varchar(20) DEFAULT NULL,
 `segment_id` int(11) DEFAULT NULL,
 `warehouse_id` int(11) NOT NULL,
 `fy_id` int(5) DEFAULT NULL,
 `gp_type` int(5) NOT NULL,
 `supplier_id` int(5) DEFAULT NULL,
 `doc_no` varchar(20) DEFAULT NULL,
 `doc_dt` date DEFAULT NULL,
 `gp_no` varchar(20) NOT NULL,
 `gp_date` date NOT NULL,
 `tp_id` int(5) NOT NULL,
 `tpt_lr_no` varchar(30) NOT NULL,
 `tpt_lr_dt` date DEFAULT NULL,
 `vehicle_no` varchar(30) NOT NULL,
 `status` int(5) NOT NULL,
 `rcpt_date` date DEFAULT NULL,
 `addl_amt` decimal(26,6) NOT NULL,
 `cust_name` varchar(20) NOT NULL,
 `remarks` varchar(255) NOT NULL,
 `curr_id` int(10) DEFAULT NULL,
 `user_id` int(10) DEFAULT NULL,
 `created_date` date DEFAULT NULL,
 `mod_date` datetime DEFAULT NULL,
 `json_lotdata` varchar(300) DEFAULT NULL,
 `sync_flg` varchar(2) DEFAULT NULL,
 `pos_create_flg` varchar(2) DEFAULT NULL,
 `pos_upd_flg` varchar(2) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=latin1;



CREATE TABLE `sma_gp_item` (
 `id` bigint(20) NOT NULL AUTO_INCREMENT,
 `org_id` varchar(20) NOT NULL,
 `wh_id` varchar(20) DEFAULT NULL,
 `segment_id` bigint(20) DEFAULT NULL,
 `warehouse_id` bigint(21) NOT NULL,
 `gp_src_id` varchar(40) NOT NULL,
 `item_id` bigint(20) NOT NULL,
 `code` varchar(50) DEFAULT NULL,
 `item_uom_id` varchar(100) DEFAULT NULL,
 `req_qty` decimal(26,6) DEFAULT NULL,
 `pending_qty` decimal(26,6) NOT NULL DEFAULT '0.000000',
 `dlv_note_qty` decimal(26,6) NOT NULL,
 `act_rcpt_qty` decimal(26,6) NOT NULL,
 `item_price` decimal(26,6) DEFAULT NULL,
 `item_amt` decimal(26,6) NOT NULL,
 `sync_flg` varchar(2) DEFAULT NULL,
 `gp_no` varchar(20) DEFAULT NULL,
 `pos_create_flg` varchar(2) DEFAULT NULL,
 `pos_upd_flg` varchar(2) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;


CREATE TABLE `sma_gp_item_lot` (
 `id` bigint(20) NOT NULL AUTO_INCREMENT,
 `org_id` varchar(20) NOT NULL,
 `wh_id` varchar(20) DEFAULT NULL,
 `segment_id` bigint(21) DEFAULT NULL,
 `warehouse_id` bigint(21) NOT NULL,
 `mrn_gp_item_id` bigint(40) NOT NULL,
 `lot_no` varchar(255) DEFAULT NULL,
 `item_id` bigint(50) NOT NULL,
 `code` varchar(50) DEFAULT NULL,
 `item_uom_id` varchar(100) DEFAULT NULL,
 `lot_qty` decimal(26,6) NOT NULL,
 `mfg_dt` date DEFAULT NULL,
 `expiry_dt` date DEFAULT NULL,
 `batch_no` varchar(255) DEFAULT NULL,
 `itm_uom_bs` varchar(20) DEFAULT NULL,
 `pending_qty` decimal(26,6) DEFAULT NULL,
 `sync_flg` varchar(2) DEFAULT NULL,
 `gp_no` varchar(20) DEFAULT NULL,
 `pos_create_flg` varchar(2) DEFAULT NULL,
 `pos_upd_flg` varchar(2) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=latin1;

CREATE TABLE `sma_stk_adjt` (
 `id` bigint(21) NOT NULL AUTO_INCREMENT,
 `org_id` varchar(2) DEFAULT NULL,
 `wh_id` varchar(20) DEFAULT NULL,
 `doc_id` varchar(50) DEFAULT NULL,
 `doc_dt` date DEFAULT NULL,
 `prj_id` varchar(40) DEFAULT NULL,
 `note` text,
 `sync_flg` varchar(2) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;


CREATE TABLE `sma_stk_adjt_itm` (
 `id` bigint(21) NOT NULL AUTO_INCREMENT,
 `org_id` varchar(10) DEFAULT NULL,
 `wh_id` varchar(20) DEFAULT NULL,
 `doc_id` varchar(40) DEFAULT NULL,
 `itm_id` varchar(50) DEFAULT NULL,
 `itm_uom` varchar(20) DEFAULT NULL,
 `qty` decimal(26,6) DEFAULT NULL,
 `type` varchar(20) DEFAULT NULL,
 `sync_flg` varchar(2) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;


CREATE TABLE `sma_stk_adjt_lot` (
 `id` bigint(21) NOT NULL AUTO_INCREMENT,
 `org_id` varchar(10) DEFAULT NULL,
 `wh_id` varchar(20) DEFAULT NULL,
 `doc_id` varchar(40) DEFAULT NULL,
 `lot_no` varchar(20) DEFAULT NULL,
 `itm_id` varchar(50) DEFAULT NULL,
 `itm_uom` varchar(20) DEFAULT NULL,
 `qty` decimal(26,6) DEFAULT NULL,
 `type` varchar(20) DEFAULT NULL,
 `sync_flg` varchar(2) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;


CREATE TABLE `sma_stk_adjt_bin` (
 `id` bigint(21) NOT NULL AUTO_INCREMENT,
 `org_id` varchar(10) DEFAULT NULL,
 `wh_id` varchar(20) DEFAULT NULL,
 `doc_id` varchar(40) DEFAULT NULL,
 `lot_no` varchar(20) DEFAULT NULL,
 `bin_id` varchar(20) DEFAULT NULL,
 `itm_id` varchar(50) DEFAULT NULL,
 `itm_uom` varchar(20) DEFAULT NULL,
 `qty` decimal(26,6) DEFAULT NULL,
 `type` varchar(20) DEFAULT NULL,
 `sync_flg` varchar(2) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;