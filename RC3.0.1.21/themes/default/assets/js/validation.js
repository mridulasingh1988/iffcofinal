$(document).ready(function(){
    $('#open-register-form').validate({
        rules:{
            cash_in_hand:{
                required:true,
                //positiveNumber:true
            }
        },
        messages:{
            cash_in_hand:{
                required:"Cash in hand can't be left empty" //,
               // positiveNumber:"Cash in hand amount can't be negative"
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
    
    // $.validator.addMethod('positiveNumber',
    // function (value) { 
    //     return Number(value) >=0;
    // }, 'Enter a positive number.');
   //$('#open_register').attr('disabled','disabled');
});