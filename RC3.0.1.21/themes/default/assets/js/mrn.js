jQuery(document).ready(function() {
    var div_count = $("div.po_ro_tab").length;
    if(div_count>0)
    $("#supplier_id").prop( "disabled", true );  
    $( document ).on( 'click', '.delete-mrn', function() {
        if(confirm("Are you sure you want to delete this record?")){
            var split_ids = $(this).attr('id').split("_");
            var ro_id = split_ids[1]; 
            var src_id = split_ids[0];
            var obj = $(this);
            if(src_id){
                $.ajax({
                    url: site.base_url+"mrn/deletemrn",
                    async: false,
                    type: "POST",
                    data: {"src_id":src_id,"ro_id":ro_id},
                    dataType:"json",
                    success: function(data) {
                        if(data.response==1){
                            obj.closest(".po_ro_tab").remove();  
                        }else{
                            alert("failed to delete Mrn. Please try again!!");
                            return false;
                        }
                    }
                })
            }
        }
    });

 $( document ).on( 'change', '#supplier_id', function() {
     var supplierid = $(this).val();
    if(supplierid!=''){

     $.ajax({   
         url: site.base_url+"mrn/getPoList", 
         async: false,
         type: "POST", 
         data: {"supplier_id":supplierid},
         dataType: "json", 
         success: function(data) {
            $("#po_no").empty(); 
                
               $.each(data,function(key,val){ 
                    var value_d = key.split("~");
                    var opt = $('<option />'); 
                    opt.val(value_d[0]);
                   // opt.attr('attri',key);
                    opt.text(val);
                    $('#po_no').append(opt);
                 //   $('#ro_no option[value="0"]').attr("selected",true);
               });   
            }
        });
 }
    });

     /*$( document ).on( 'change', '#supplier_id', function() {
     var supplierid = $(this).val();
     $.ajax({   
         url: site.base_url+"mrn/getPoList", 
         async: false,
         type: "POST", 
         data: {"supplier_id":supplierid},
         dataType: "json", 
         success: function(data) {
            $("#po_no").empty(); 
                
               $.each(data,function(key,val){ 
                    var value_d = key.split("~");
                    var opt = $('<option />'); 
                    opt.val(value_d[0]);
                   // opt.attr('attri',key);
                    opt.text(val);
                    $('#po_no').append(opt);
                 //   $('#ro_no option[value="0"]').attr("selected",true);
               });   
            }
        });
    });*/

    $('#save-sale').on('click', function(event){
        $("#submit-type").val(1);
        var $frm =  $("#mrn-form");
        event.preventDefault();
        $frm.validate()
        /*if($frm.valid()){
            submitHandler();     
        }*/
    });
    $('#hold-sale').on('click', function(){
        $("#submit-type").val(2);
        var $frm =  $("#mrn-form");
        $frm.validate()
        // if($frm.valid()){
        //     submitHandler();     
        // }
    });
    $( document ).on( 'click', '.delete', function() {
        var obj = $(this);
        var lot_no_detail = obj.attr('data-del'); 
        var lot_id = lot_no_detail.split("_");
        var del_ajax = obj.attr("data-exist");
        var table_length = $('table[data-id="lot_bin_'+lot_no_detail+'"] tr').length;       
        if(table_length==0 || table_length==1){
            if(confirm("Are you sure you want to delete this record?")){
                if(del_ajax==1){
                    $.ajax({
                        url: site.base_url+"mrn/deletedata",
                        async: false,
                        type: "POST",
                        data: {"lot_id":lot_id[0]},
                        dataType:"json",
                        success: function(data) {
                            if(data.response==1){
                                obj.closest("tr").remove();
                                if(table_length==1){
                                    $('table[data-id="lot_bin_'+lot_no_detail+'"]').remove();
                                }     
                            }else{
                                alert("failed to delete bin. Please try again!!");
                                return false;
                            }
                        }
                    })
                }else{
                    obj.closest("tr").remove();
                    if(table_length==1){
                        $('table[data-id="lot_bin_'+lot_no_detail+'"]').remove();
                    }
                }
            }
        }else{
            alert("Please delete all bins to which this lot is allocated");
            return false;
        }
    });
    $( document ).on( 'click', '.deletebin', function() {
        var delete_id = $(this).attr("id");
        var obj = $(this);
        if(confirm("Are you sure you want to delete this record?")){
            if(delete_id){ // if delete id exists
                $.ajax({
                        url: site.base_url+"mrn/deletedata",
                        async: false,
                        type: "POST",
                        data: {"bin_id":delete_id},
                        dataType:"json",
                        success: function(data) {
                            if(data.response==1){
                                obj.closest("tr").remove();      
                            }else{
                                alert("failed to delete bin. Please try again!!");
                                return false;
                            }
                        }
                    })
            }else{
                 obj.closest("tr").remove();      
             } 
        }      
    });
     $( document ).on( 'click', '.accordian-div', function() {
        $('.po_ro_tab').find('.box-content-div').slideUp('slow');
        $(this).closest('.po_ro_tab').find('.box-content-div').toggle('slow');  
    });
   
    $( document ).on( 'click', '#cancelButton', function() {
        var parentId = $(this).closest('.modal').attr('id');
        $("#"+parentId).modal('hide');
    });
    $( document ).on( 'change', '.show-decimal', function() {

        var input_val = $(this).val();

        if(!isNaN(input_val)){
            $(this).val(parseFloat(input_val).toFixed(2));
        }
        var parent_obj = $(this).closest('tr').attr('id');

        var bill_qty = parseFloat($("#"+parent_obj).find('.bill_qty').val());
        var rec_qty = parseFloat($("#"+parent_obj).find('.rec_qty').val());
        var rej_qty = parseFloat($("#"+parent_obj).find('.rej_qty').val());
        //alert(bill_qty+"rec qty"+rec_qty+"rej qty"+rej_qty);
        var maxQty = parseFloat($(this).attr('data-maxqty'));
        // && !NaN(maxQty)
        if(maxQty!=''){
            if(rec_qty>maxQty){
                alert("Maximum receiving quantity cannot be greater than "+maxQty);
                $("#"+parent_obj).find('.rec_qty').val(maxQty);
                return false;
            }
        }

        //var bill_qty = $("#"+parent_obj).find('.bill_qty').val();
        if((!isNaN(bill_qty)) && (!isNaN(rec_qty)) && (!isNaN(rej_qty))){
            var short_qty = ((rec_qty+rej_qty)-bill_qty).toFixed(2);
        }
        $("#"+parent_obj).find('.short_qty').val(short_qty);

     });    
       
        var today = new Date();
        var lastDate = new Date(today.getFullYear(), today.getMonth(0)-1, 31);
        var startDate = new Date(today.getFullYear(), 0, 1);

      // alert(startDate+"enddate"+today)
        jQuery("#lr_date").datetimepicker({

           // startDate: startDate,

            endDate: today,
            changeMonth: true,
            autoclose:true,
            showSecond: false,
            minView: 2,
            format: 'yyyy-mm-dd',
        });

        jQuery("#rcpt_dt").datetimepicker({

           // startDate: startDate,

            endDate: today,
            changeMonth: true,
            autoclose:true,
            showSecond: false,
            minView: 2,
            format: 'yyyy-mm-dd',
        });
       /* jQuery("#lr_date").datetimepicker({
            endDate: lastDate               
        });*/
        jQuery("#del_note_date").datetimepicker({
                changeMonth: true,
                autoclose:true,
                showSecond: false,
                minView: 2,
                format: 'yyyy-mm-dd',
                //startDate: startDate,

                endDate: today
        });
       /* jQuery("#ro_date").datetimepicker({
                changeMonth: true,
                autoclose:true,
                showSecond: false,
                minView: 2,
                format: 'yyyy-mm-dd',
                startDate: startDate,
                endDate: today       
        });*/
        jQuery("#edit_mfg_date").datetimepicker({
            changeMonth: true,
            autoclose:true,
            showSecond: false,
            minView: 2,
            format: 'yyyy-mm-dd',
            onSelect: function(selected) {

                $("#edit_exp_date").datetimepicker("option","minDate", selected)

            }
            //startDate: startDate,

           // endDate: today
        });
        jQuery("#edit_exp_date").datetimepicker({
                changeMonth: true,
                autoclose:true,
                showSecond: false,
                minView: 2,
                format: 'yyyy-mm-dd',
                onSelect: function(selected) {
                     $("#edit_mfg_date").datetimepicker("option","maxDate", selected)

                  }

               // startDate: startDate,

              //  endDate: today
        });
        jQuery("#gatepass_date").datetimepicker({
                changeMonth: true,
                autoclose:true,
                showSecond: false,
                minView: 2,
                format: 'yyyy-mm-dd',
                //startDate: startDate,

                endDate: today
        });
    
    
    $('#po_ro_tab_div').on('click','.remove',function() {

       
        $(this).closest('div.po_ro_tab').remove();
        var div_count = $("div.po_ro_tab").length;
        if(div_count==0)
         $("#supplier_id").prop( "disabled", false );  
       
    });
    
    $( document ).on( 'click', '#genlot', function() {
        var no = generateLotNo(6);
        $("#lot_no").val(no);
        return false;
    });
    
    $( document ).on( 'click', '.editLot', function() {
        var mylist = [];
        var obj = $(this).closest("div#lotModal");
           var flg = 0;           
            if($("#lot_no").val()==''){
                alert("Please enter Lot No.");
                flg++;
                return false;
            }
            var batch_val = $("#is_batch").val();
            if((batch_val=='Y') && ($("#edit_batch_no").val()=='')){
                alert("Please enter batch no.");
               // flg++;
                return false;
            }
            var item_id = $("#item_id").val();
            var check_type = $("#item_id").attr("class");
            var ro_div = $("#ro_val").val();
           // alert("item"+item_id);
           var lot_qty = parseFloat($("#edit_lot_qty").val());
           var remain_qty = parseFloat($("#lot_remaining_qty").val());           
                    
           if(lot_qty>remain_qty){             
                alert("Item quantity cannot exceed remaining item quantity");
                $("#edit_lot_qty").val(remain_qty);
                return false;
           }

          var date_received = new Date($("#edit_mfg_date").data("datetimepicker").getDate());
        //  new Date($("#edit_mfg_date").datetimepicker('getDate'));
          var date_completed = new Date($("#edit_exp_date").data("datetimepicker").getDate());

          var diff = date_completed - date_received;
          var days = diff / 1000 / 60 / 60 / 24;
          if(days<0){
            alert("Expiry Date cannot be less than Manufacturing Date");
            return false;
          }
            if(flg==0){
                var lot_name = '';
                if(check_type=='mrn_type')
                    lot_name = 'mrn_lot_data';
                else
                    lot_name = 'lot_data';
                //alert(item_id)
                if (check_type === undefined || check_type === null) 
                    check_type = "";    
                var row = {};
                row["lot_no"] = $("#lot_no").val();
                row["lot_qty"] = $("#edit_lot_qty").val();
                row["batch_no"] = $("#edit_batch_no").val();
                row['mfg_date'] = $("#edit_mfg_date").val();
                row['exp_date'] = $("#edit_exp_date").val();
                mylist.push(row);

                var lot_obj = decodeURIComponent(JSON.stringify(mylist));       
                var input = document.createElement("input");
                input.type = "hidden";
                input.name = ''+lot_name+'['+item_id+'][]';
                input.value = lot_obj;
                $("#lotModal").modal('hide');
                
                var table_id = 'lot_'+item_id;
                //     alert(table_id)
                $("#"+ro_div).find(".lot_bin_div_"+item_id).find("#lot_"+item_id+" tbody").children("tr").removeClass("background");
                $("#"+ro_div).find(".lot_bin_div_"+item_id).find("#lot_"+item_id+" tbody").children("tr").addClass("white-background");
                var lot_no = row['lot_no'].replace(/[!"#$%&'()*+,.\/:;<=>?@[\\\]^`{|}~]/g,"");
                if ( $("#"+ro_div).find( "#"+table_id ).length ) {
                   // alert(1)
                    var tr = $('<tr class="background" data-id="lot_'+lot_no+'_'+item_id+'"></tr>');

                    var div_td_html= '<td><input type="hidden" class="lot-class" name="lot_qty['+item_id+'][]" value="'+row['lot_qty']+'" />'+row["lot_no"];
                    tr.append(input);   
                    div_td_html+='</td>';
                    div_td_html+= '<td>'+row['lot_qty']+'</td>';
                    div_td_html+= '<td><a href="javascript:void(0)" id="bin['+item_id+'][]" data-id="'+lot_no+'_'+item_id+'_'+row['lot_qty']+'" class="allocate_bin" data-check="'+check_type+'">Allocate Bin</a>&nbsp;<a href="javascript:void(0)" data-del="'+row['lot_no']+'_'+item_id+'" class="delete">Delete</a></td>';

                    tr.append(div_td_html);
                    //alert("table id"+table_id)
                    // table.append(div_html);
                   // $('.po_ro_tab').find(".lot_bin_table").hide();
                    $("#"+ro_div).find(".top_lot_div").hide();
                    $('#binModal').modal('hide');
                    $("#"+ro_div).find("#"+table_id ).append(tr);
                    $("#"+ro_div).find(".bin_table").hide();
                    $('table[data-id="lot_bin_'+row['lot_no']+'_'+item_id+'"]').show();
                    $("#"+ro_div).find(".lot_bin_div_"+item_id).show();
                    $("#"+ro_div).find( "#"+table_id ).show();
                   // $('.po_ro_tab').find('.box-content-div').slideUp('slow');
                  //  $( "#"+table_id ).closest('.po_ro_tab').find('.box-content-div').slideDown('slow');

                }else{
                  //  alert(2)
                    var pTag = $("");
                    //alert($(this).index())
                    var table = $('<table class="table table-bordered table-condensed lot_bin_table" id="lot_'+item_id+'" style="margin-bottom:0;"></table>');
                    div_html+= '<thead>';
                    div_html+= '<tr>';
                    div_html+= '<th>'+lang.lot_name+'</th>';
                    div_html+= '<th>'+lang.qty+'</th>';
                    div_html+= '<th>'+lang.action+'</th>';
                    div_html+= '</tr>';
                    div_html+= '</thead>';
                    //div_html+= '<tbody>';
                    var tr = $('<tr class="background" data-id="lot_'+lot_no+'_'+item_id+'"></tr>');
                    var div_td_html= '<td><input type="text" name="lot_qty['+item_id+'][]" value="'+row['lot_qty']+'" />'+$("#bin_lot_no").val();
                    tr.append(input);     
                    div_td_html+='</td>';
                    div_td_html+= '<td>'+row['lot_qty']+'</td>';
                    div_td_html+= '<td><a href="javascript:void(0)" data-check="'+check_type+'" data-id="'+lot_no+'_'+item_id+'_'+row['lot_qty']+'" class="allocate_bin">Allocate Bin</a></td>';
                   // console.log("tr=============>",tr);
                    tr.append(div_td_html);
                    table.append(div_html);
                    table.append(tr);
                    //div_html+= '</tbody></table>';
                    //pTag.append(div_html);   
                    //console.log(pTag)
                    $("#"+ro_div).find(".lot_"+item_id).append(table);
                }
                obj.find('input:text').val(''); 

            }


           // console.log("------------------->",JSON.parse($("#lot_data_"+item_id).val()));
          //  $(".lot_"+item_id).hide()
          //  $(".bin_"+item_id).show()   
            

    });
     $( document ).on( 'click', '.bin_background', function() {
        var slot_id = $(this).attr("id").split("_"); 
        /*alert(slot_id)*/
        $(".bin_background").addClass('white-background');
        $(this).removeClass('white-background');        
        $(this).addClass('background');
        var table_length = $("#lot_"+slot_id[1]+" > tbody").children().length;
       // alert("=====================$("").hide();==>"+table_length);
        $(this).closest('#stock_details').find(".top_lot_div").hide();
        if(table_length>0){
        //    alert($(".lot_bin_div_"+slot_id[1]).find("#lot_"+slot_id[1]+" tbody").children("tr:first").attr("data-id"))
           // $(this).closest('#stock_details').find(".top_lot_div").hide(); 
            $(".lot_bin_div_"+slot_id[1]).show();
            $(".lot_bin_div_"+slot_id[1]).find("#lot_"+slot_id[1]+" tbody").children("tr").removeClass("background");
            $(".lot_bin_div_"+slot_id[1]).find("#lot_"+slot_id[1]+" tbody").children("tr").addClass("white-background");
            $(".lot_bin_div_"+slot_id[1]).find("#lot_"+slot_id[1]+" tbody").children("tr:first").addClass("background");
            $(".lot_bin_div_"+slot_id[1]).find("#lot_"+slot_id[1]+" tbody").children("tr:first").removeClass("white-background");
            $(".lot_bin_div_"+slot_id[1]).find(".bin_div").hide();
            $(".lot_bin_div_"+slot_id[1]).find(".bin_table").hide();
            $(".lot_bin_div_"+slot_id[1]).find(".bin_"+slot_id[1]).show();
            $(".lot_bin_div_"+slot_id[1]).find(".bin_div").find("table:first").show();
        }
     });
    $( document ).on( 'click', '.lot_bin_table tr', function() {
        var obj = $(this).closest('div.po_ro_tab');
        var lot_detail = $(this).attr('data-id').split("_");
        $(this).siblings().removeClass('background');
        $(this).siblings().addClass('white-background');
        $(this).addClass('background');
        $(this).removeClass('white-background');
        $(this).siblings().removeClass('white-background');
        // alert("'lot_bin_"+lot_detail[1]+"_"+lot_detail[2]+"'")
        //$(".bin_"+item_id).find;
        var table_id = 'lot_bin_'+lot_detail[1]+"_"+lot_detail[2];
        obj.find(".bin_table").hide();
        obj.find(".bin_"+lot_detail[2]).show();
       // $('.bin_table').hide();
        //$('table[data-id='+table_id+']').siblings().hide();
        $('table[data-id='+table_id+']').show()
        //$("table[data-id='lot_bin_"+lot_detail[1]+"_"+lot_detail[2]+"']").show();        
        
    });
     
    $( document ).on( 'click', '.editBin', function() { 
        var check_type = $("#bin_item_id").attr('class');
        var is_new_data = $("#bin_item_id").attr('data-is-new');

        var bin_list = [];
        var obj = $(this).closest("div#binModal");
        var flg = 0;   
        var item_id = $("#bin_item_id").val();
        var bin_qty = parseFloat($("#bin_qty").val());
        var remain_qty = parseFloat($("#bin_remaining_qty").val());           
        if(bin_qty==0){
            alert("Please enter quantity for bin!");
            return false;
        }
        if(bin_qty>remain_qty){             
            alert("Item quantity cannot exceed remaining item quantity");
            $("#bin_qty").val(remain_qty);
            return false;
        }
        var row = {};
        row['bin_lot_no'] = $("#bin_lot_no").val();
        row['bin_qty'] = $("#bin_qty").val();
        row['bin_id'] = $("#bin_id").val();
        //alert("bin_lot"+row['bin_lot_no']);
        bin_list.push(row);
        var bin_obj = decodeURIComponent(JSON.stringify(bin_list)); 
        var is_data_saved = 0;
        if(is_new_data){ // saving bin data to database if lot is not deleted
            //alert(1)
            
             $.ajax({
                url: site.base_url+"mrn/saveBin",
                async: false,
                type: "POST",
                data: {"bin_data":bin_obj},
                dataType:"json",
                success: function(data) {
                   if(data.response){                   
                        is_data_saved = 1;
                   }
                }
            })

        }
//        alert("is nore"+is_new_data+"save"+is_data_saved);

        if((is_new_data==1) && (is_data_saved==0)){
            alert("Failed to save record. Please try again");
            return false;
        }

        var bin_names = '';
        if(check_type=='mrn_type')
            bin_names = 'mrn_bin_data';
        else
            bin_names = 'bin_data';
        if (check_type === undefined || check_type === null) 
        check_type = ""; 
        var bin_name = $("#bin_id option:selected").text();
              
        var input = document.createElement("input");
        input.type = "hidden";
        input.name = ''+bin_names+'['+row['bin_lot_no']+'][]';
       // input.className = "css-class-name";
        input.value = bin_obj;
        
        var lot_no = row['bin_lot_no'].replace(/[!"#$%&'()*+,.\/:;<=>?@[\\\]^`{|}~]/g, "");
        var table_id = 'lot_bin_'+lot_no+'_'+item_id;
        //alert(table_id)
        if ( $('table[data-id='+table_id+']').length ) {                
          //  alert("1")
            $('table[data-id='+table_id+']').find("tr").removeClass("background");
            var tr = $('<tr class=""></tr>');

            //div_html+= '<tr class="background">';
            var div_td_html= '<td><input type="hidden" class="" name="bin_qty['+row['bin_lot_no']+'][]" value="'+row['bin_qty']+'" />'+bin_name;
            tr.append(input);   
          //  pTag.append(input);
            div_td_html+='</td>';
            div_td_html+= '<td>'+row['bin_qty']+'</td>';
            /*div_td_html+= '<td><a href="javascript:void(0)" data-id="'+row['bin_lot_no']+'_'+item_id+'_'+row['bin_qty']+'" class="allocate_bin">Edit</a></td>';*/
            div_td_html+= '<td><a href="javascript:void(0)" class="deletebin">Delete</a></td>';
            //div_html+= '</tr>';
            tr.append(div_td_html);
            //table.append(div_html);
            $('#binModal').modal('hide');
            $('table[data-id='+table_id+']').show();
            $('table[data-id='+table_id+']').append(tr);
        }else{
           // alert(2)
            var pTag = $("");
            //alert($(this).index())
            var tables = $('<table class="table table-bordered table-condensed bin_table" data-id="lot_bin_'+row['bin_lot_no']+'_'+item_id+'" style="margin-bottom:0;"></table>');
            var div_html= '<thead>';
            div_html+= '<tr>';
            div_html+= '<th>'+lang.bin_name+'</th>';
            div_html+= '<th>'+lang.qty+'</th>';
            div_html+= '<th>'+lang.action+'</th>';
            div_html+= '</tr>';
            div_html+= '</thead>';
            //div_html+= '<tbody>';
            var tr = $('<tr class=""></tr>');
            //div_html+= '<tr class="background">';
            var div_td_html= '<td><input type="hidden" name="bin_qty['+row['bin_lot_no']+'][]" value="'+row['bin_qty']+'" />'+bin_name;
            tr.append(input);   
          //  pTag.append(input);
            div_td_html+='</td>';
            div_td_html+= '<td>'+row['bin_qty']+'</td>';
           /* div_td_html+= '<td><a href="javascript:void(0)" data-id="'+row['bin_lot_no']+'_'+item_id+'_'+row['bin_qty']+'" class="allocate_bin">Edit</a></td>';*/
            div_td_html+= '<td><a href="javascript:void(0)" class="deletebin">Delete</a></td>';
            //div_html+= '</tr>';
            tr.append(div_td_html);
            console.log("tacblw------>",tables)
            tables.append(div_html);
            tables.append(tr);
            //div_html+= '</tbody></table>';
            //pTag.append(div_html);   
            //console.log(pTag)
            $(".bin_"+item_id).append(tables);

        }
        $('#binModal').modal('hide');
        $('table[data-id='+table_id+']').siblings().hide();
        $("bin_"+item_id).show();
        $('table[data-id='+table_id+']').show()
        //$(".stock_data_div").find(".bin_table").show();
        obj.find('input:text').val('');

    });

    $( document ).on( 'click', '.allocate_lot', function() { 
        var obj = $(this).closest('.po_ro_tab').attr('id');
        var is_batch = $("#"+obj).find("#is_batch_item").val();

       // alert(obj)
        //var obj_id = obj.attr('id');        
        var check_type = $(this).attr("data-check");
        $("#"+obj).find("table tr").addClass('white-background');
        /*$(this).closest("tr").css("background-color","#dddddd");
        $(this).closest("tr").css("background-color","#dddddd");*/   
        $("#lotModalLabel").html("Add/Edit Lot");
        var id = $(this).attr('data-id').split("_");
        var item_qty = $("#"+obj).find(".rec_qty_"+id[1]).text();
       // alert("id"+id+"item qry"+item_qty+"obk"+obj);

        $("#item_id").val(id[1]);
        $("#ro_val").val(obj);
        if(check_type=="mrn_type")
            $("#item_id").addClass(check_type);
        else
            $("#item_id").removeClass(check_type);
        
        var total_qty = 0;
        $("#"+obj).find('input[name="lot_qty['+id[1]+'][]"]').each(function(){
            total_qty = parseFloat(total_qty)+parseFloat($(this).val());
        });
        var rest_qty = parseFloat(item_qty) - total_qty;
        if(parseInt(rest_qty)>0){
            $('#lotModal').modal({backdrop: 'static', keyboard: false});
            $("#edit_lot_qty").val(rest_qty);
            $("#lot_remaining_qty").val(rest_qty);
            $("#is_batch").val(is_batch);
            $('#lotModal').css('zIndex', '1050');
            //$(this).removeClass("modal-backdrop in");
        }
    });
    
    $( document ).on( 'click', '.allocate_bin', function() {

        var check_type = $(this).attr("data-check"); 
        var is_new_data = $(this).attr("data-is-new");        
        $("#binModalLabel").html("Add/Edit Bin");
        var id = $(this).attr('data-id').split("_");
        $("#bin_item_id").val(id[1]);
        if(is_new_data)
            $("#bin_item_id").attr('data-is-new',is_new_data);     
        else
            $("#bin_item_id").removeAttr('data-is-new');
        $("#bin_item_id").addClass(check_type);
        $("#binModalLabel").html("Add/Edit Bin");
        //$("#bin_item_id").val(item_id);
        $("#bin_lot_no").val(id[0]);
        var total_qty = 0;
        var lot_qty = id[2];
        $('input[name="bin_qty['+id[0]+'][]"]').each(function(){
            total_qty = parseFloat(total_qty)+parseFloat($(this).val());
        });
        //    alert("total"+total_qty+"||||lotqty"+lot_qty)
        var rest_qty = parseFloat(lot_qty) - total_qty;
        $("#edit_lot_qty").val(rest_qty)   
        $("#bin_remaining_qty").val(rest_qty) 
        $("#bin_qty").val(rest_qty);
        $('#binModal').modal({backdrop: 'static', keyboard: false});
        $('#binModal').modal({backdrop: 'static', keyboard: false});
      //  $( ".modal-backdrop" ).remove();
        //$(this).removeClass("modal-backdrop in");
    });

     $('#po_no').change(function () {
            var po_no = $(this).val(); // <-- change this line
            if(po_no!=0){
               // $('#ro_no').empty();
                $('#ro_no').prop('selectedIndex',0);
                 $.ajax({
                    url: site.base_url+"mrn/getRoDetails",
                    async: false,
                    type: "POST",
                    data: {"po_id":po_no},
                    dataType:"json",
                    success: function(data) {
                        //$("#ro_no").empty(); 
                        $("#ro_no").empty();
                        $("#s2id_ro_no").find(".select2-chosen").text("Select Ro No.");
                        $("#ro_no option:eq(0)").prop("selected", true);
                        $("#ro_date").val('');
                        $("#ro_date").empty();
                        /*var opt = $('<option />'); 
                            opt.val('Select Ro No.');
                            opt.attr('attri','');
                            opt.text(0);
                            $('#ro_no').html(opt);*/
                       $.each(data,function(key,val){ 
                            var opt = $('<option />'); 
                            opt.val(val);
                            opt.attr('attri',key);
                            opt.text(val);
                            $('#ro_no').append(opt);
                         //   $('#ro_no option[value="0"]').attr("selected",true);
                       });
                    }
                })
            }
        });

        $('#warehouse_id').change(function () {
            var wh_id = $(this).val(); // <-- change this line
            if(wh_id!=0){
                 $.ajax({
                    url: site.base_url+"mrn/getPoDetails",
                    async: false,
                    type: "POST",
                    data: {"wh_id":wh_id},
                    dataType:"json",
                    success: function(data) {
                        $("#ro_no").empty(); 
                        /*var opt = $('<option />'); 
                            opt.val('Select Ro No.');
                            opt.attr('attri','');
                            opt.text(0);
                            $('#ro_no').html(opt);*/
                       $.each(data,function(key,val){ 
                            var opt = $('<option />'); 
                            opt.val(val);
                            opt.attr('attri',key);
                            opt.text(val);
                            $('#ro_no').append(opt);
                         //   $('#ro_no option[value="0"]').attr("selected",true);
                       });
                    }
                })
            }
        });

        $('#ro_no').change(function () {
            var ro_no = $(this).val(); // <-- change this line
            if(ro_no!=0){
                var ro_date =  $('option:selected', this).attr('attri').split("~")[0];                
                //alert(ro_date)
                $("#ro_date").val(ro_date);    
            }
        });


        $('#add_po_details').click(function () {  
            var po_length = $("input[name='po_list[]']").length;
            var ro_length = $("input[name='ro_list[]']").length;
            var po_no = $("#po_no").val();
            var ro_no = $("#ro_no").val();
            var ro_date = $("#ro_date").val();
            var sup_id = $("#supplier_id").val();
            var ro_flag = 0;
           if(sup_id==''){
                alert("Please Select Supplier");
                return false;
           }
           if(po_length>=1){
                $.each($('.ro_list'), function(index, attr) {
                    var po = $(this).attr('vals');
                    if((attr.value===ro_no) && (po===po_no)){
                        ro_flag++;
                    }
                });
           }
           //return false;
           if(ro_flag==0){

                $.ajax({
                    url: site.base_url+"mrn/getRoProducts",
                    async: false,
                    type: "POST",
                    data: {"po_no":po_no,"ro_no":ro_no,"sup_id":sup_id},
                    dataType:"json",
                    success: function(data) {                                            
                       if(data && data.length>0){
                        add_product_tab(data,po_no,ro_no,ro_date);  
                        $("#supplier_id").prop( "disabled", true ); 
                       }else{
                        $("#supplier_id").prop( "disabled", false );
                        alert("Please Select RO No.")
                         return false;
                       }                                          
                    }
                })

        }else{
            alert("This Ro No. is already selected.")
            return false;

        }
    });         

    $("#mrn-form").validate({
        ignore: [],
        rules: {
             other_charges: {
                required : true,
                digits : true,
                checkChargeVal : true 
            },
            supplier_id:{
                required : true               
            },
            lr_name:{
                required : true               
            },
            rcpt_dt:{
                required : true
            }

            
        },
        messages: {
            other_charges : {
                required : "Please enter unloading charges",
                checkChargeVal : "Please enter unloading charges"
            },
             supplier_id:{
                required : "Please select a supplier"
            },
            lr_name:{
                required : "Please select a transporter name"
            },
            rcpt_dt:{
                required : "Please enter the receipt date"
            }
           /* name: "Please enter your name",
            father_name: "Please enter your father name",

            contact_person: {
                required: "Please enter contact person",
            },*/
        },
        errorElement: "em", 
        submitHandler:function(form){            
            var error = [];
            var rec_count = 0;
            $('.rec_qty').each(function(i,obj){
                if(parseInt($(this).val())!=0){
                    rec_count++;
                }               
            });
            $('.rej_qty').each(function(i,obj){
                if(parseInt($(this).val())!=0){
                    rec_count++;
                }               
            });
            if(rec_count==0){
                alert("Please enter details of atleast of one item");
                return false;
            }

            $('.rec_qty').each(function(i, obj) {
                var obj = $(this).closest(".po_ro_tab").attr("id");
                var ro_id = $(this).attr('id').split("_");
                var receive_qty = parseInt($(this).val());
                //alert(receive_qty);
                if(receive_qty!=0){
                    var total_qty = 0;  
                    $("#"+obj).find('input[name="lot_qty['+ro_id[2]+'][]"]').each(function(){
                        total_qty = parseFloat(total_qty)+parseFloat($(this).val());
                    });
                    if(total_qty!=receive_qty){
                       // alert("less quantity")
                        error.push(ro_id[2]);
                        $("#"+obj).find(".bin_background").addClass('white-background');
                        $("#"+obj).find("#slot_"+ro_id[2]).removeClass('white-background');        
                        $("#"+obj).find("#slot_"+ro_id[2]).addClass('background');
                        $("#"+obj).find("#slot_"+ro_id[2]).addClass('add-border');
                        $("#"+obj).find("#slot_"+ro_id[2]).closest('div.po_ro_tab').find(".top_lot_div").hide();
                        
                        $("#"+obj).find(".lot_bin_div_"+ro_id[2]).show();
                        $("#"+obj).find("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('.box-content-div').show();
                        $("#"+obj).find("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('#item_details').hide();
                        $("#"+obj).find("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('#stock_details').show();
                        $("#"+obj).find("#slot_"+ro_id[2]).tooltip({'show': true,
                            'placement': 'bottom',
                            'title': "Please allot Lot to the received items"
                        });
                        $("#"+obj).find("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('.stock_details_tab').addClass('active');
                        $("#"+obj).find("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('.item_details_tab').removeClass('active');
                       // return false;
                    }else{
                       jQuery.grep(error, function(value) {
                        return value != ro_id[2];
                      });
                       $("#"+obj).find("#slot_"+ro_id[2]).tooltip({'show': true,
                            'placement': 'bottom',
                            'title': "Lot assigned successfully"
                        });
                        
                        $("#"+obj).find("#slot_"+ro_id[2]).closest('#stock_details').find(".top_lot_div").hide();
                        $("#"+obj).find("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('.box-content-div').hide();
                        $("#"+obj).find("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('#item_details').show();
                        $("#"+obj).find("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('#stock_details').hide();
                        $("#"+obj).find("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('.stock_details_tab').removeClass('active');
                        $("#"+obj).find("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('.item_details_tab').addClass('active');
                        // error.remove(ro_id[2]);
                        $("#"+obj).find("#slot_"+ro_id[2]).removeClass('add-border');
                    }
                }
            });
            if(error.length>0){
                return false;
            }
            var bin_error = [];
            $('.lot-class').each(function(i, obj) {
                
                var lot_qty = parseInt($(this).val());
                var lot_id = $(this).closest('tr').attr('data-id');
                var lot_no = $(this).closest('tr').attr('data-id').split("_");                
             
                if(lot_qty!=0){
                    var total_bin_qty = 0;
                    $('input[name="bin_qty['+lot_no[1]+'][]"]').each(function(){
                        total_bin_qty = parseFloat(total_bin_qty)+parseFloat($(this).val());
                    });
             
                    if(total_bin_qty!=lot_qty){
                       // alert("less")
                        //alert(lot_id)
                        bin_error.push(lot_no[1]);
                        $(this).closest('.stock_data_div').find(".top_lot_div").hide();
                        $(".bin_background").addClass('white-background');
                        $("#slot_"+lot_no[2]).removeClass('white-background');        
                        $("#slot_"+lot_no[2]).addClass('background');
                        
                      
                        $(".lot_bin_div_"+lot_no[2]).show();
                        $('tr[data-id='+lot_id+']').addClass('add-border');
                        
                       // $('lot_'+lot_id).children('tr').removeClass('white-background');
                        //$('tr[data-id='+lot_id+']').addClass('background');
                        $('tr[data-id='+lot_id+']').closest('div.po_ro_tab').find('.box-content-div').show();
                        $('tr[data-id='+lot_id+']').closest('div.po_ro_tab').find('#item_details').hide();
                        $('tr[data-id='+lot_id+']').closest('div.po_ro_tab').find('#stock_details').show();
                        $('tr[data-id='+lot_id+']').tooltip({'show': true,
                            'placement': 'bottom',
                            'title': "Please allot Bin to the received items"
                        });
                        $('tr[data-id='+lot_id+']').closest('div.po_ro_tab').find('.stock_details_tab').addClass('active');
                        $('tr[data-id='+lot_id+']').closest('div.po_ro_tab').find('.item_details_tab').removeClass('active');
                        return false;
                    }else{                       
                       jQuery.grep(bin_error, function(value) {
                        return value !=lot_no[1] ;
                      });
                       
                       $('tr[data-id='+lot_id+']').tooltip({'show': true,
                            'placement': 'bottom',
                            'title': "Bin assigned successfully"
                        });
                        $('tr[data-id='+lot_id+']').removeClass('add-border');
                        $(".lot_bin_"+lot_no[1]+"_"+lot_no[2]).hide();
                        
                        $(".lot_bin_div_"+lot_no[2]).hide();
                    }
                }
                //console.log("value");
            });
            if(bin_error.length>0){
                return false;
            }else{
                
                var obj = $(".item_detail");
                // alert(obj.length);
                // return false;
                var flg1 = 0;
                var flg2 = 0;
                obj.find("tr").each(function () {
                       // alert(parseInt($(this).find('.rec_qty').val()) +"&&"+ parseInt($(this).find('.rej_qty').val()) +"&&"+ parseInt($(this).find('.short_qty').val()));
                      if($(this).hasClass('rec_detail') && ((parseInt($(this).find('.rec_qty').val())==0) && (parseInt($(this).find('.rej_qty').val())==0) && (parseInt($(this).find('.short_qty').val())<=0))){
                      //  alert(parseInt($(this).find('.short_qty').val()))
                       // alert(1)
                        $(this).find('.item-list').prop( "disabled", true );
                        $(this).find('.rec_qty').prop( "disabled", true ); 
                        $(this).find('.rej_qty').prop( "disabled", true ); 
                        $(this).find('.short_qty').prop( "disabled", true );  
                        $(this).find('.bill_qty').prop( "disabled", true );  
                      }else{
                      //  alert(2)
                       // alert("else")
                       // alert(parseInt($(this).find('.short_qty').val()))
                        $(this).find('.item-list').prop( "disabled", false ); 
                        $(this).find('.rec_qty').prop( "disabled", false ); 
                        $(this).find('.rej_qty').prop( "disabled", false ); 
                        $(this).find('.short_qty').prop( "disabled", false );
                        $(this).find('.bill_qty').prop( "disabled", false );  
                      }
                        
                });
                //return false;




                $("#supplier_id").prop( "disabled", false ); 
                var submit_type = $("#submit-type").val();
                $(".show-decimal").attr("readonly", false); 
                if(submit_type==1){
                    if(confirm("Are you sure you want to save the details? Once you click OK, all the details will be saved and you will not be able to modify details")){
                        form.submit();
                    }
                }else{
                    form.submit();
                    
                }
            }
        },
        invalidHandler: function() {
            // re-enable the button here as validation has failed
        }         
    });
    $( document ).on( 'click', '.item_details_tab', function() {
        var obj = $(this).closest("div.po_ro_tab");        
        $(this).addClass('active');
        obj.find('.stock_details_tab').removeClass('active');
        obj.find("#item_details").show()
        obj.find("#stock_details").hide();

    });
    
    $( document ).on( 'change', '.rec_qty', function() {        
        var obj = $(this).closest("div.po_ro_tab");
        var flg1 = 0;
        var flg2 = 0;
        var from_gp = 0;
        
        obj.find("input").each(function () {
            var gp_ele = $(this).closest('tr').find("#from_gp");

            if($(this).attr('at')=='rec_qty'){
                if(gp_ele.length>0){
                   // alert(1)
                    var recv_val = parseFloat(this.value);
                    var recv_qty = parseFloat(gp_ele.val());
                   // alert(recv_val+"rec qty"+recv_qty);
                    if(recv_qty==0){
                        alert("The remaining item is not yet out from consignment warehouse!!");
                        this.value = recv_qty.toFixed(2);
                    }
                    else if(recv_val>recv_qty){
                        alert("Quantity cannot be greater than mentioned");
                        this.value = recv_qty.toFixed(2);                            
                    }
                } 
                var id = this.id;                                           
                obj.find("."+id).text(this.value);

            }
        });    

        var input_val = $(this).val();

        if(!isNaN(input_val)){
            $(this).val(parseFloat(input_val).toFixed(2));
        }
        var parent_obj = $(this).closest('tr').attr('id');

        var bill_qty = parseFloat($("#"+parent_obj).find('.bill_qty').val());
        var rec_qty = parseFloat($("#"+parent_obj).find('.rec_qty').val());
        var rej_qty = parseFloat($("#"+parent_obj).find('.rej_qty').val());
        //alert(bill_qty+"rec qty"+rec_qty+"rej qty"+rej_qty);


        //var bill_qty = $("#"+parent_obj).find('.bill_qty').val();
        if((!isNaN(bill_qty)) && (!isNaN(rec_qty)) && (!isNaN(rej_qty))){
            var short_qty = ((rec_qty+rej_qty)-bill_qty).toFixed(2);
        }
        $("#"+parent_obj).find('.short_qty').val(short_qty);

    });

    $( document ).on( 'click', '.stock_details_tab', function() {        
        var obj = $(this).closest("div.po_ro_tab");
        var flg1 = 0;
        var flg2 = 0;
        obj.find("input").each(function () {
            //alert("value"+this.value+"id"+this.id);
            /*else if(isNaN(this.value)){
                flg2++;
                alert("Please enter valid value(s)")
                return false;
            }*/
            if($(this).attr('at')=='rec_qty'){
                if(this.value==''){
                    alert("Please fill all fields");
                    flg1++;
                    return false;
                 }
                var id = this.id;                                           
                obj.find("."+id).text(this.value);  
                //alert(obj.find("#"+id).attr("class"))                  
            }
        });

        if(flg1==0 && flg2==0){           
            //$("#dbTab").children('li').removeClass('active');
            $(this).addClass('active');
            obj.find('.item_details_tab').removeClass('active');
            obj.find("#item_details").hide()
            obj.find("#stock_details").show();
            var table_first_row = obj.find(".item_table tbody").children("tr:first").attr("id").split("_")[1]; // first row id
           // var table_first_row = table_first_row.trim().replace(/["~!@#$%^&*\(\)_+=`{}\[\]\|\\:;'<>,.\/?"\- \t\r\n]+/g, '');
            
            obj.find(".top_lot_div").hide();                        
            $(".lot_bin_div_"+table_first_row).show();

            obj.find(".bin_background").addClass("white-background");
          //  alert(obj.find(".item_table tbody").children("tr:first").attr("id"))
            obj.find(".item_table tbody").children("tr:first").removeClass("white-background");
            obj.find(".item_table tbody").children("tr:first").addClass("background");
            obj.find("#lot_"+table_first_row+" tbody").children("tr").addClass("white-background");
            obj.find("#lot_"+table_first_row+" tbody").children("tr:first").removeClass("white-background");
            obj.find("#lot_"+table_first_row+" tbody").children("tr").addClass("background");
           // alert("#lot_"+table_first_row);
           var bin_id = '';
           if(obj.find("#lot_"+table_first_row+" tbody").children("tr:first").attr("data-id")!=undefined){
            bin_id = obj.find("#lot_"+table_first_row+" tbody").children("tr:first").attr("data-id").split("lot_")[1];
           }
            
           
            obj.find(".bin_div").hide();
            obj.find(".bin_"+table_first_row).show();
            if(bin_id!=undefined){
                var id = 'lot_bin_'+bin_id;
                // alert('table[data-id='+id+']')
                obj.find('.bin_table').hide();
                obj.find('table[data-id='+id+']').show();
            }
            //alert()
         //   obj.find("")

        }
    });

    $.validator.addMethod("checkChargeVal",
        function(value, element) {

            if(parseInt(value)>=0)
                return true;
            else 
                return false;
    }); 

//        $( document ).on( 'click', '#add_mrn', function() {


            
            //if  
            /*var submit_flg = 0;
            var obj = $("div.po_ro_tab");
            obj.find("input").each(function () {
                if(this.value==''){
                    submit_flg++;
                }    

            });

            if(submit_flg==0){
                alert(1)
                $("form#mrn-form").submit();
            }else{
                alert(2)
                return false;
            }*/
            


  //      });
});
 



function isNumberKey(e, t) {

    try {
        if (window.event) {

            var charCode = window.event.keyCode;
        }
        else if (e) {

            var charCode = e.which;

        }
        else { return true; }

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {

            return false;
        }
        return true;
    }
    catch (err) {
        alert(err.Description);
    }
} 

function add_product_tab(product_array,po_no,ro_no,ro_date){

  //  alert(po_no);
    var div_html="";
    var ro_nos = ro_no;
    var ro_no = ro_no.trim().replace(/["~!@#$%^&*\(\)_+=`{}\[\]\|\\:;'<>,.\/?"\ \t\r\n]+/g, '');
    var auth_po_no = product_array[0].auth_po_no;
    var po_id = product_array[0].po_id;
    div_html+= '<div class="col-sm-12 po_ro_tab" id="po_'+ro_no+'_'+po_id+'">';
    div_html+= '<hr></hr>';
    div_html+= '<div class="accordian-div"><div class="col-sm-12 remove text-right"><i class="fa fa-trash"></i></div>';
    div_html+= '<div class="col-sm-6 text-left">PO NO: '+auth_po_no+'</div>';
    div_html+= '<div class="col-sm-5 text-left">RO No: '+ro_nos+'</div></div>';
    div_html+= '<div class="row" style="margin-bottom: 15px;">';
    div_html+= '<div class="col-md-12" >';
    div_html+= '<div class="box-content  box-content-div" style="border:1px solid #DDDDDD;border-radius:2px;">';
    div_html+= '<div class="row inner-content">';
    div_html+= '<div class="col-md-12">';
    div_html+= '<ul id="dbTab" class="nav nav-tabs">';
    div_html+= '<li class="item_details_tab active"><a href="javascript:void(0)">'+lang.item_details+'</a></li>';
    div_html+= '<li class="stock_details_tab"><a href="javascript:void(0)">'+lang.stock_details+'</a></li>';
    div_html+= '</ul>';
    div_html+= '<div class="tab-content">';
    div_html+= '<div id="item_details" class="tab-pane fade in active">';
    div_html+= '<div class="row">';
    div_html+= '<div class="col-sm-12">';
    div_html+= '<div class="table-responsive">';
    div_html+= '<table class="table items table-striped table-bordered table-condensed table-hover totals item_detail" style="margin-bottom:0;">';
    div_html+= '<thead>';
    div_html+= '<tr>';
    div_html+= '<th>'+lang.item_name+'</th>';
    div_html+= '<th>'+lang.unit+'</th>';
    div_html+= '<th>'+lang.bill_qty+'</th>';
    div_html+= '<th>'+lang.recv_qty+'</th>';
    div_html+= '<th>'+lang.rej_qty+'</th>';
    div_html+= '<th>'+lang.short_qty+'</th>';
    div_html+= '</tr>';
    div_html+= '</thead>';
    div_html+= '<tbody>';
    div_html+= '<input type="hidden" name="po_list[]" value="'+po_no+'" />';
    div_html+= '<input type="hidden" name="ro_list['+po_no+'][]" class="form-control ro_list" vals="'+po_no+'" value="'+ro_nos+'" />';
    div_html+= '<input type="hidden" name="ro_date_list['+po_no+'][]" class="form-control" value="'+ro_date+'" />';

    if(product_array.length>0){
        for(var i in product_array){
            product_array[i].ro_no = product_array[i].ro_no.trim().replace(/["~!@#$%^&*\(\)_+=`{}\[\]\|\\:;'<>,.\/?"\- \t\r\n]+/g, '');
           // console.log(product_array[i].ro_no);

            var rcv_qty = '0.00';
            var short_qty = '0.00';
            var dlv_qty = parseFloat(product_array[i].dlv_qty).toFixed(2);
            if(product_array[i].recv_qty!=undefined){
               //alert("rec"+product_array[i].recv_qty)
                rcv_qty = product_array[i].recv_qty.toFixed(2); 
               
                short_qty = rcv_qty - dlv_qty;
                short_qty = short_qty.toFixed(2);
            }
            var from_gp = '';
           if(product_array[i].from_gp==1){
                from_gp = '<input type="hidden" name="gp_list['+po_id+'-'+ro_no+']" id="from_gp" value="'+rcv_qty+'" />';
           }
           // console.log("from gp",product_array[i].from_gp);
                     
            div_html+= '<tr id="prod_'+product_array[i].id+'" class="rec_detail">';
            div_html+= '<td>'+product_array[i].itm_name+'';
            div_html+= '<td>'+product_array[i].unit+'';
            div_html+='<input type="hidden" name="item_list['+po_id+'-'+ro_no+'][]" class="form-control item-list" value="'+product_array[i].itm_id+'" />';
            div_html+='<input type="hidden" name="is_batch_item" id="is_batch_item" class="form-control" value="'+product_array[i].bat_item_id+'" />';         
            div_html+= from_gp; 
           /* div_html+='<input type="hidden" name="lot_data['+product_array[i].ro_no+']['+product_array[i].itm_id+'][]" value="0" id="lot_data_'+product_array[i].id+'" onkeypress="return isNumberKey(event,this)" class="lot_data" />';*/
            div_html+='</td>';
            div_html+= '<td><input type="text" name="bill_qty['+po_id+'-'+ro_no+']['+product_array[i].id+']"  class="form-control bill_qty show-decimal" value="'+dlv_qty+'" onkeypress="return isNumberKey(event,this)" required="required" readonly="readonly" /></td>';
            div_html+= '<td><input type="text" name="rec_qty['+po_id+'-'+ro_no+']['+product_array[i].id+']" id="rec_qty_'+product_array[i].id+'" value="'+rcv_qty+'" at="rec_qty" class="form-control show-decimal rec_qty" onkeypress="return isNumberKey(event,this)" required="required"/></td>';
            div_html+= '<td><input type="text" name="rej_qty['+po_id+'-'+ro_no+']['+product_array[i].id+']" class="form-control rej_qty show-decimal" value="0.00" required="required" onkeypress="return isNumberKey(event,this)" /></td>';
            div_html+= '<td data-original-title="Please enter lot for all the items" data-container="body" data-toggle="tooltip" data-placement="bottom"><input type="text" name="short_qty['+po_id+'-'+ro_no+']['+product_array[i].id+']" value="'+short_qty+'" class="form-control short_qty show-decimal" value="0.00" required="required" onkeypress="return isNumberKey(event,this)" readonly="readonly" /></td>';
            div_html+= '</tr>';
        }
    }
    div_html+= '</tbody>';
    div_html+= '</table>';  
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '<div id="stock_details" class="tab-pane fade in">';
    div_html+= '<div class="row">';
    div_html+= '<div class="col-sm-12">';
    div_html+= '<div class="table-responsive">';
    div_html+= '<div class="col-sm-3">';
    div_html+= '<table class="table table-bordered table-condensed totals item_table " style="margin-bottom:0;">';
    div_html+= '<thead>';
    div_html+= '<tr>';
    div_html+= '<th>'+lang.item_name+'</th>';
    div_html+= '<th>'+lang.qty+'</th>';
    div_html+= '<th>'+lang.action+'</th>';
    div_html+= '</tr>';
    div_html+= '</thead>';
    div_html+= '<tbody>';
    for(var i in product_array){ 
        var rcv_qty = '0.00';
        if(product_array[i].recv_qty!=undefined){
            rcv_qty = product_array[i].recv_qty.toFixed(2);
        }
        var ro_nos = product_array[i].ro_no.trim().replace(/["~!@#$%^&*\(\)_+=`{}\[\]\|\\:;'<>,.\/?"\- \t\r\n]+/g, '');
        div_html+= '<tr class="bin_background" id="slot_'+product_array[i].id+'" data-original-title="" data-container="body" data-toggle="tooltip" data-placement="bottom">';
        div_html+= '<td>'+product_array[i].itm_name+'</td>';
        div_html+= '<td class="rec_qty_'+product_array[i].id+'">'+rcv_qty+'</td>';
        div_html+= '<td><a href="javascript:void(0)" class="allocate_lot lot_'+product_array[i].id+'" data-id="lot_'+product_array[i].id+'_'+ro_nos+'">Allocate Lot</a>';
        div_html+= '</tr>';
    }
    div_html+= '</tbody>';
    div_html+= '</table>';
    div_html+= '</div>';
    div_html+= '<div class="col-sm-9 stock_data_div">';
    for(var i in product_array){
        div_html+= '<div class="lot_bin_div_'+product_array[i].id+' top_lot_div"><div class="lot_div lot_'+product_array[i].id+'"><table class="table table-bordered table-condensed lot_bin_table" id="lot_'+product_array[i].id+'" style="margin-bottom:0;display:none;">';
        div_html+= '<thead>';
        div_html+= '<tr>';
        div_html+= '<th>'+lang.lot_name+'</th>';
        /*div_html+= '<th><?= lang("bin_name"); ?></th>';*/
        div_html+= '<th>'+lang.qty+'</th>';
        div_html+= '<th>'+lang.action +'</th>';
        div_html+= '</tr>';
        div_html+= '</thead>';
        div_html+= '<tbody>';
        div_html+= '</tbody>';
        div_html+= '</table></div><div class="bin_div bin_'+product_array[i].id+'"></div>';
        div_html+= '</div>';
    }

    
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    $(".po_ro_tab").find('.box-content-div').slideUp('slow');
    $('#po_ro_tab_div').append('<div>'+div_html+'</div>');

}
