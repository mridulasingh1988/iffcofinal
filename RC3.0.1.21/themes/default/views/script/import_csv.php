<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('Import PO And PO Schedule By CSV File'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <?php
                //$attrib = array('class' => 'form-horizontal', 'data-toggle' => 'validator', 'role' => 'form');
                //echo form_open_multipart("script/import_csv", $attrib)
                echo form_open_multipart("script/import_csv")
                ?>
                <div class="row">
                    <div class="col-md-12">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="csv_file"><?= lang("Uploard PO File"); ?></label>
                                <input type="file" name="userfile[]" class="form-control file" data-show-upload="false"
                                       data-show-preview="false" id="csv_file" required="required"/>
                            </div>

                            <div class="form-group">
                                <label for="csv_file"><?= lang("Uploard PO Schedule File"); ?></label>
                                <input type="file" name="userfile[]" class="form-control file" data-show-upload="false"
                                       data-show-preview="false" id="csv_file1" required="required"/>
                            </div>

                            <div class="form-group">
                                <?php echo form_submit('import', $this->lang->line("import"), 'class="btn btn-primary"'); ?>
                            </div>
                        </div>
                    </div>
                    <?= form_close(); ?>

                </div>

            </div>
        </div>
    </div>
</div>