<?php
if (!empty($variants)) {
    foreach ($variants as $variant) {
        $vars[] = addslashes($variant->name);
    }
} else {
    $vars = array();
}
?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i>Edit Laboratory</h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?php echo lang('enter_info'); ?></p>

                <?php
                $attrib = array('data-toggle' => 'validator', 'role' => 'form');
                echo form_open_multipart("products/edit_laboratory/". $product->id, $attrib)
                ?>

                <div class="col-md-5">
                    <div class="form-group all">
                        <?= lang("laboratory_name", "name") ?>
                        <?= form_input('laboratory_name', (isset($_POST['name']) ? $_POST['
                        name'] : ($product ? $product->lab_name : '')), 'class="form-control" id="name" required="required" placeholder="Enter Laboratory Name"'); ?>
                    </div>

                    <div class="form-group all">
                        <?= lang("concerned_person", "concerned_person") ?>
                        <?= form_input('concerned_person', (isset($_POST['concerned_person']) ? $_POST['concerned_person'] : ($product ? $product->lab_person : '')), 'class="form-control" id="concerned_person" placeholder="Enter name of the concerned person" required="required"'); ?>
                    </div>

                    <div class="form-group all">
                        <?= lang("laboratory_location", "laboratory_location") ?>
                        <?= form_input('laboratory_location', (isset($_POST['laboratory_location']) ? $_POST['laboratory_location'] : ($product ? $product->lab_location : '')), 'class="form-control" id="laboratory_location" required="required" placeholder="Enter Laboratory Location"'); ?>
                    </div>

                    <div class="form-group all">
                        <?= lang("lab_center", "lab_center") ?>
                        <?php
                        $cat[''] = "";

                        foreach ($warehouses as $warehouse) {
                            $cat[$warehouse->id] = $warehouse->name;
                        }
                       
                        echo form_dropdown('lab_center', $cat, (isset($_POST['lab_center']) ? $_POST['lab_center'] : ($product ? $product->warehouse_id : '')), 'class="form-control select" id="lab_center" placeholder="Select Warehouse" required="required" style="width:100%"')
                        ?>

                    </div>
                    
                    <div class="form-group">
                        <?php echo form_submit('edit_laboratory/'.$product->id, $this->lang->line("submit"), 'class="btn btn-primary"'); ?>
                    </div> 
                </div>
            <?=form_close();?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var audio_success = new Audio('<?= $assets ?>sounds/sound2.mp3');
        var audio_error = new Audio('<?= $assets ?>sounds/sound3.mp3');
        var items = {};

        $(document).on('click', '.del', function () {
            var id = $(this).attr('id');
            delete items[id];
            $(this).closest('#row_' + id).remove();
        });
        var su = 2;
        $(document).on('click', '.delAttr', function () {
            $(this).closest("tr").remove();
        });

        $(document).on('click', '.attr-remove-all', function () {
            $('#attrTable tbody').empty();
            $('#attrTable').hide();
        });

        var row, warehouses = <?= json_encode($warehouses); ?>;
        $(document).on('click', '.attr td:not(:last-child)', function () {
            row = $(this).closest("tr");
            $('#aModalLabel').text(row.children().eq(0).find('span').text());
            $('#awarehouse').select2("val", (row.children().eq(1).find('input').val()));
            $('#aquantity').val(row.children().eq(2).find('input').val());
            $('#acost').val(row.children().eq(3).find('span').text());
            $('#aprice').val(row.children().eq(4).find('span').text());
            $('#aModal').appendTo('body').modal('show');
        });

        $(document).on('click', '#updateAttr', function () {
            var wh = $('#awarehouse').val(), wh_name;
            $.each(warehouses, function () {
                if (this.id == wh) {
                    wh_name = this.name;
                }
            });
            row.children().eq(1).html('<input type="hidden" name="attr_warehouse[]" value="' + wh + '"><input type="hidden" name="attr_wh_name[]" value="' + wh_name + '"><span>' + wh_name + '</span>');
            row.children().eq(2).html('<input type="hidden" name="attr_quantity[]" value="' + $('#aquantity').val() + '"><span>' + decimalFormat($('#aquantity').val()) + '</span>');
            row.children().eq(3).html('<input type="hidden" name="attr_cost[]" value="' + $('#acost').val() + '"><span>' + currencyFormat($('#acost').val()) + '</span>');
            row.children().eq(4).html('<input type="hidden" name="attr_price[]" value="' + $('#aprice').val() + '"><span>' + currencyFormat($('#aprice').val()) + '</span>');
            $('#aModal').modal('hide');
        });
    });

   
</script>