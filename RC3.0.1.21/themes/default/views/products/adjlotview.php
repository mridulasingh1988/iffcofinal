<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header no-print">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('Adjustments Details'); ?></h4>
        </div>
        <div class="modal-body">
            <div class="row" >
                <div class="col-sm-12">
                    
                    
                    
                    

                    <div class="col-md-12">
                        <h5 class="text-center">Adjustments Stock Details</h5>

                        <table cellpadding="0" cellspacing="0" border="0"
                           class="table table-bordered table-condensed table-hover table-striped" style="font-size:12px;">

                           <thead>
                               <tr>
                                   <th><?=lang('s_no');?></th>
                                   <th><?=lang('DOC ID');?></th>
                                   <th><?=lang('product_name');?></th>
                                   <th><?=lang('Unit');?></th>
                                   <th><?=lang('Adj Qty.');?></th>
                                   <th><?=lang('lot_no');?></th>
                                   <th><?=lang('Bin ID');?></th>
                                   <th><?=lang('type');?></th>
                               </tr>
                           </thead>

                           <tbody>
                            <?php //echo "hi.."; print_r($lotview); die;
                            $i=1;
                            foreach($lotview as $lt){ ?>
                               <tr>
                                   <td><?=$i;?></td>
                                   <td><?=$lt->doc_id;?></td>
                                   <td><?=$lt->name;?></td>
                                   <td><?=$lt->itm_uom;?></td>
                                   <td><?=$this->sma->formatDecimal($lt->qty);?></td>
                                   <td><?=$lt->lot_no;?></td>
                                   <td><?=$lt->bin_id;?></td>
                                   <td><?=$lt->type;?></td>
                              
                                   
                               </tr>
                             <?php 
                             $i++;
                             }
                             ?>
                           </tbody>

                        </table>
                    </div>

                </div>
            </div>
        </div>
        
        <div class="row"></div>
    </div>
    </div>
</div>
