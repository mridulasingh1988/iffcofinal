<script type="text/javascript">

$('#adjust_quantity').click(function(event){
            
            
            var stk = $("#stock").val();
            var qty = $("#quantity").val(); 
            var tp = $("#type").val();
            //alert("stk=>"+stk+" qty=>"+qty+" type=>"+tp);
            if(tp == 'subtraction' && stk == '0.00')
           {
                  alert("Stock Not Available..");
                  $("#quantity").val('0.00');
                  $("[data-dismiss=modal]").trigger({ type: "click" });
                  return false;
           }

           if(tp == 'addition')
           {

               if(qty == '0.00' || qty == '')
                {
                    if(qty == '')
                     {
                        alert("Please Fill Qty..")
                        $("#quantity").val('');
                        $("#quantity").focus();
                        return false;
                  }
                        alert("Quantity should be Greater Than 0..")
                        $("#quantity").val('');
                        $("#quantity").focus();
                        return false;
                 }

           }
            if(qty == '0.00' || qty == '')
            {
                if(qty == '')
                {
                alert("Please Fill Qty..")
                $("#quantity").val('');
                $("#quantity").focus();
                return false;
                }
                alert("Quantity should be Greater Than 0..")
                $("#quantity").val('');
                $("#quantity").focus();
                return false;
            }

           //alert("stk=>"+stk+" qty=>"+qty+" type=>"+tp);
           if(tp == 'subtraction')
           {
             //if (parseFloat(qty.toFixed(2)) > parseFloat(stk.toFixed(2)))
             if (Math.round(qty * 100) > Math.round(stk * 100))
              {
                   alert("Stock Out Qty Never Greater Than Available Stock..")
                   $("#quantity").val('');
                   $("#quantity").focus();
                   return false;
              }
           }
           
            return true;
    });

$( document ).on( 'change', '.show-decimal', function() { 
      var input_val = $(this).val();
        if(!isNaN(input_val)){
            $(this).val(parseFloat(input_val).toFixed(2));
        }
        if(isNaN(input_val))
         {
            input_val = $(this).val('');
         }

});

jQuery(document).ready(function() { 

      var t = $("#avl").val(); //alert(t);
      if(t == 'NOT_IN_STOCK')
      {
        alert("Stock Not Available In Warehouse ?");
        $("[data-dismiss=modal]").trigger({ type: "click" });
        return true;
      }
      var input_val = $("#stock").val(); //alert(input_val);
        if(!isNaN(input_val)){
            $("#stock").val(parseFloat(input_val).toFixed(2));
        }
        if ($("#stock").val() == 'NaN')
        {
            //alert("Stock Qty Not Available, Please Select Available Qty product ?")
            //$("[data-dismiss=modal]").trigger({ type: "click" });
           // return true;
           $("#stock").val('0.00');
        }
        if(isNaN(input_val))
         {
            $("#stock").val();
         }
});

jQuery('#quantity').keyup(function () { 
    this.value = this.value.replace(/[^0-9\.]/g,'');
});

</script>




<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('adjust_quantity').' - '. $product->name .'('.$product->code.')'; ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("products/add_adjustment/" . $product_id, $attrib);
        ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <p style="font-weight: bold;"><?= lang("Item ID") . ": " . $product->code . "<br/> " . lang("Item Name") . ": " . $product->name ?></p>
            <?php if ($Owner || $Admin) { ?>
                <div class="form-group">
                    <?php echo lang('date', 'date'); ?>
                    <div class="controls">
                        <?php echo form_input('date', '', 'class="form-control datetime" id="date" required="required"'); ?>
                    </div>
                </div>
            <?php } ?>
            <?= form_hidden('code', $product->code) ?>
            <?= form_hidden('name', $product->name) ?>
            <div class="form-group">
                <?= lang('type', 'type'); ?>
                <?php $opts = array('addition' => lang('Stock In'), 'subtraction' => lang('Stock Out')); ?>
                <?= form_dropdown('type', $opts, set_value('type', 'addition'), 'class="form-control tip" id="type"  required="required"'); ?>
            </div>
           
            <div class="form-group">
                <label for="stock"><?php echo $this->lang->line("Available Stock"); ?></label>

                <div
                    class="controls"> <?php echo form_input('stock', (isset($stock) ? $stock : ""), 'class="form-control input-tip " id="stock" required="required" readonly= "true"'); ?> </div>
            </div>

            <div class="form-group">
                <label for="quantity"><?php echo $this->lang->line("quantity"); ?></label>

                <div
                    class="controls"> <?php echo form_input('quantity', (isset($_POST['quantity']) ? $_POST['quantity'] : ""), 'class="form-control input-tip show-decimal" id="quantity" required="required"'); ?> </div>

                   <input type="hidden" name="org_id"  value="<?php echo $org_id; ?>" />
                   <input type="hidden" name="wh_id"  value="<?php echo $wh_id; ?>" />
                   <input type="hidden" name="prj_id"  value="<?php echo $prj_id; ?>" /> 
                   <input type="hidden" name="segment_id"  value="<?php echo $segment_id; ?>" />
                   <input type="hidden" name="unit"  value="<?php echo $unit; ?>" /> 
                   <input type="hidden" name="avl" id="avl"  value="<?php echo $avl; ?>" /> 
            </div>
            <?php if (!empty($options)) { ?>
                <div class="form-group">
                    <label for="option"><?php echo $this->lang->line("product_variant"); ?></label>

                    <div class="controls">  <?php
                        $op[''] = '';
                        foreach ($options as $option) {
                            $op[$option->id] = $option->name;
                        }
                        echo form_dropdown('option', $op, (isset($_POST['option']) ? $_POST['option'] : ''), 'id="option" class="form-control input-pop" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("option") . '" required="required"');
                        ?> </div>
                </div>
            <?php } else {
                echo form_hidden('option', 0);
            } ?>
            <div class="form-group">
                <label for="warehouse"><?php echo $this->lang->line("warehouse"); ?></label>

                <div class="controls">  <?php
                    $wh[''] = '';
                    foreach ($warehouses as $warehouse) {
                        $wh[$warehouse->id] = $warehouse->name;
                    }
                    echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $warehouse_id ? $warehouse_id : $Settings->default_warehouse), 'id="warehouse" class="form-control input-pop" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("warehouse") . '" required="required"');
                    ?> </div>
            </div>
            <div class="form-group">
                <label for="note"><?php echo $this->lang->line("note"); ?></label>

                <div
                    class="controls"> <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="note" required="required" style="margin-top: 10px; height: 100px;"'); ?> </div>
            </div>

        </div>
        <div class="modal-footer">
            <?php echo form_submit('adjust_quantity', lang('adjust_quantity'), 'class="btn btn-primary" id="adjust_quantity"'); ?>
            <!-- <button class="btn btn-success" name="adjust_quantity" value="1" id="adjust_quantity"><?= lang('submit'); ?></button> -->
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
    });
</script>

