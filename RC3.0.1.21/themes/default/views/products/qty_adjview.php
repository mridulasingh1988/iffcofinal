<script>
    $(document).ready(function () {
        var oTable = $('#adjData').dataTable({
            "aaSorting": [[1, "desc"]],
            "aLengthMenu": [[10, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': false, 'bDestroy': true,
            'sAjaxSource': '<?= site_url('products/qty_adj') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [
            {
                "bSortable": false,
                "mRender": checkbox
        }, 
       null,null, null, null, null, null,null,{"bSortable": false}]
    });

 });
</script>
<?php if ($Owner) {
    echo form_open('products/qty_adjview', 'id="action-form"');
} ?>
<div class="box">
     <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-users"></i><?= lang('Adjustments Listing'); ?></h2>

         <?php if ($Owner || $Admin) { ?>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right" class="tasks-menus" role="menu" aria-labelledby="dLabel">
                      
                            <li><a href="#" id="excel" data-action="export_excel"><i
                                        class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?></a></li>
                            <li><a href="#" id="pdf" data-action="export_pdf"><i
                                        class="fa fa-file-pdf-o"></i> <?= lang('export_to_pdf') ?></a></li>
                        
                    </ul>
                </li>
            </ul>
        </div>
        <?php } ?>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
            <?php if($this->Owner || $this->Admin){ ?>
                <p class="introtext"><?= lang('list_results'); ?></p>
            <?php } ?>    
                
                <div class="row" style="margin-bottom: 15px;">
                  <div class="col-md-12">
                     <div class="box">
                        <div class="box-header">
                           <h2 class="blue"><i class="fa-fw fa fa-tasks"></i> <?= lang('Adjustments Details') ?></h2>
                        </div>
               <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <ul id="dbTab" class="nav nav-tabs">
                            <li class="active"><a href="#qty_view"><?= lang('Quantity Adjustments') ?></a></li>
                        </ul>
                        <div class="tab-content">
                        
                        <div id="qty_view" class="tab-pane in active">
                           <div class="row">
                               <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <table id="adjData" cellpadding="0" cellspacing="0" border="0"
                                               class="table table-bordered table-condensed table-hover table-striped">
                                            <thead>
                                            <tr class="primary">
                                                <th style="min-width:30px; width: 30px; text-align: center;">
                                                    <input class="checkbox checkth" type="checkbox" name="check"/>
                                                </th>
                                                <th><?= lang("DATE"); ?></th>
                                                <th><?= lang("DOC_ID"); ?></th>
                                                <th><?= lang("NAME"); ?></th>
                                                <th><?= lang("ITM_ID"); ?></th>
                                                <th><?= lang("UNIT"); ?></th>
                                                <th><?= lang("QTY"); ?></th>
                                                <th><?= lang("TYPE"); ?></th>
                                                <th style="width:85px;"><?= lang("actions"); ?></th>
                                                
                                                <!-- <th style="width:85px;"><?= lang("actions"); ?></th> -->
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td colspan="9" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                            </tr>
                                            </tbody>
                                            <!-- Footer Added By Anil Start-->
                                            <tfoot class="dtFilter">
                                                <tr class="active">
                                                    <th style="min-width:30px; width: 30px; text-align: center;">
                                                        <input class="checkbox checkft" type="checkbox" name="check"/>
                                                    </th>  
                                                    <th><?= lang("DATE"); ?></th>                      
                                                    <th><?= lang("DOC_ID"); ?></th>
                                                    <th><?= lang("NAME"); ?></th>
                                                    <th><?= lang("ITM_ID"); ?></th>
                                                    <th><?= lang("UNIT"); ?></th>
                                                    <th><?= lang("QTY"); ?></th>
                                                    <th><?= lang("TYPE"); ?></th>
                                                    <th style="width:85px;"><?= lang("actions"); ?></th>
                                                    
                                                    <!-- <th style="min-width: 100px; width:85px;"><?= lang("actions"); ?></th> -->
                                                </tr>
                                             </tfoot>
                                            <!-- Footer Added By Anil End-->
                                        </table>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        
                            
                            
                                                    
                    </div>
                        
                       </div>
                    </div>
                </div>
              </div>
            </div>
         </div>
       </div>
     </div>
    </div>
</div>
<?php if ($Owner) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>
<?php if ($action && $action == 'add') {
    echo '<script>$(document).ready(function(){$("#add").trigger("click");});</script>';
}
?>