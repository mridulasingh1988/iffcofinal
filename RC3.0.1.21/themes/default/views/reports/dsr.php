<?php //print_r($dsr); die; ?>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-users"></i><?= lang('Daily Sales Reports'); ?></h2>

         <?php if (!$Owner || !$Admin) { ?>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <!--<a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right" class="tasks-menus" role="menu" aria-labelledby="dLabel">
                      
                            <li><a href="#" id="excel" data-action="export_excel"><i
                                        class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?></a></li>
                            <li><a href="#" id="pdf" data-action="export_pdf"><i
                                        class="fa fa-file-pdf-o"></i> <?= lang('export_to_pdf') ?></a></li>
                        
                    </ul>
                </li> -->
            </ul>
        </div>
        <a href="../../../../../../Server 231/iffcov9/RC3.0.1.21/themes/default/views/reports/dsr.php"></a>
        <?php } ?>
    </div>&nbsp;&nbsp;
          
<div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                              <?php if (!empty($seg)) {
                                echo "<strong> Shop Name: </strong>".$seg[0]->Shop_Name."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                echo "<strong>Date: </strong>".date('d-m-Y')."<br/><br/>";

                                } ?>
                            </div>                                                    
                            
                        <ul id="dbTab" class="nav nav-tabs">
                            <li class="active"><a href="#dsr_reports"><?= lang('Stock Details') ?></a></li>
                            <li class=""><a href="#dsr_rstr"><?= lang('Register Details') ?></a></li>

                        </ul>


<div class="tab-content">
                              <div id="dsr_reports" class="tab-pane in active">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="table-responsive">
                                            <table id="dsr_reports1" cellpadding="0" cellspacing="0" border="0"
                                                   class="table table-bordered table-hover table-striped"
                                                   style="margin-bottom: 0;">
                                                <thead>
                                                <tr>
                                                    <!--<th style="width:30px !important;">#</th> -->
                                                    <th><?= $this->lang->line("Product"); ?></th>
                                                    <!--<th><?= $this->lang->line("Date"); ?></th> -->
                                                    <th><?= $this->lang->line("Unit"); ?></th>
                                                    <th><?= $this->lang->line("Opening Stock"); ?></th>
                                                    <th><?= $this->lang->line("Receipt(In)"); ?></th>
                                                    <th><?= $this->lang->line("Sale"); ?></th>
                                                    <th><?= $this->lang->line("Other(Out)"); ?></th>
                                                    <th><?= $this->lang->line("Closing Stock"); ?></th>
                                                </tr>
                                                </thead>
                                                <tbody id = 'msr_reports'>
                                                <?php  if (!empty($dsr)) {
                                                    $r = 0;
                                                    foreach ($dsr as $order) {
                                                        echo '<tr>  
                                                           <td>' . $order->Product . '</td>
                                                            <td>' . $order->Unit . '</td>
                                                            <td>' . $order->Opening_Stk . '</td>
                                                            <td>' . $order->Receipt_In . '</td>
                                                            <td>' . $order->Sale_Out . '</td>
                                                            <td>' . $order->Sale_Other . '</td>
                                                            <td>' . $order->Closing_stk . '</td>
                                                            
                                                        </tr>';
                                                        $r++;
                                                    }
                                                } else { ?>
                                                    <tr>
                                                        <td colspan="7"
                                                            class="dataTables_empty"><?= lang('no_data_available') ?></td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

 </div>

               <!-- ############################################################ -->  
               
               <div id="dsr_rstr" class="tab-pane fade in">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="table-responsive">
                                            <table id="dsr_rstr1" cellpadding="0" cellspacing="0" border="0"
                                                   class="table table-bordered table-hover table-striped"
                                                   style="margin-bottom: 0;">
                                                <thead>
                                                <tr>
                                                    <!--<th style="width:30px !important;">#</th> -->
                                                    <th><?= $this->lang->line("Cash Opening"); ?></th>
                                                    <th><?= $this->lang->line("Cash Sale"); ?></th>
                                                    <th><?= $this->lang->line("Bank Sale"); ?></th>
                                                    <th><?= $this->lang->line("Deposited"); ?></th>
                                                    <th><?= $this->lang->line("Closing"); ?></th>
                                                    <th><?= $this->lang->line("Closing Time"); ?></th>
                                                    
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php  
                                                if(!empty($creg))
                                                {
                                                    if($creg!= 'open')

                                                    {
                                                if (!empty($rdtl)) {
                                                    //echo "<pre/>";print_r($rdtl); die;
                                                    $r = 0;
                                                    foreach ($rdtl as $order) {

                                                        echo '<tr>  
                                                           <td>' . $order->cash_in_hand . '</td>
                                                            <td>' . $order->csh . '</td>
                                                            <td>' . $order->bnk . '</td>
                                                            <td>' . $order->exp . '</td>
                                                            <td>' . $order->total_cash_submitted . '</td>
                                                            <td>' . $order->closed_at . '</td>
                                                            
                                                            
                                                        </tr>';
                                                        $r++;
                                                    }
                                                } else { ?>
                                                    <tr>
                                                        <td colspan="6"
                                                            class="dataTables_empty"><?= lang('no_data_available') ?></td>
                                                    </tr>
                                                <?php }


                                            }
                                        
                                        else{ ?>

                                        <tr>
                                                        <td colspan="6"
                                                            class="dataTables_empty"><?= lang('Please Close Register First..') ?></td>
                                                    </tr>

                                       <?php }  }





                                                 ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>               
                            

         </div>
        
         </div>


   </div>   
   </div>                      