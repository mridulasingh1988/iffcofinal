<script>
    $(document).ready(function () {
        var oTable = $('#stockData').dataTable({
            "aaSorting": [[1, "asc"], [2, "desc"]],           
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': false, 'bDestroy': true,
            'sAjaxSource': '<?= site_url('reports/msr1') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
               aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            }, 
            "aoColumns": [
            {
                "bSortable": false,
                "mRender": checkbox
        }, 
 null,null, {"mRender": currencyFormat},{"mRender": currencyFormat}, {"mRender": currencyFormat},{"mRender": currencyFormat},{"mRender": currencyFormat}]
    });


    
 });
</script>

<?php echo form_open('reports/msr_actions', 'id="action-form"'); ?>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-users"></i><?= lang('Monthly Sales Reports'); ?></h2>
        <div class="box-icon">          
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right" class="tasks-menus" role="menu" aria-labelledby="dLabel">
                      
                            <li><a href="#" id="excel" data-action="export_excel"><i
                                        class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?></a></li>
                            <li><a href="#" id="pdf" data-action="export_pdf"><i
                                        class="fa fa-file-pdf-o"></i> <?= lang('export_to_pdf') ?></a></li>
                        
                    </ul>
                </li> 
            </ul>    
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="row" style="margin-bottom: 15px;">
                  <div class="col-md-12">
                     <div class="box">
                        <div class="box-header">
                           <h2 class="blue"><i class="fa-fw fa fa-tasks"></i> <?= lang('Stock Details') ?></h2>
                        </div>
                <div class="box-content">
                <div class="row">
                <?php if ($Owner || $Admin) {                      
                    ?>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?php 
                            
                             $seg[''] = '';                               
                                foreach ($get_segments as $val) { 
                                    $seg[$val->id] = ucwords($val->proj_name);
                                }                               
                                    echo form_dropdown('segments', $seg, (isset($data['proj_name']) ? $data['proj_name'] : $_POST['segment_id']), 'id="seg_id" class = "form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("Segment") .'" required="required" style="width:100%;" ');
                                ?>                        

                           
                        </div>
                    </div>    
                <?php } ?>       
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" name="mdate1" class="form-control input-tip" id="mdate1" value="<?php echo date("d-m-Y");?>" readonly="readonly" placeholder="Select Date">
                          <div class="input-group-addon">to</div>
                          <input type="text" name="mdate2" class="form-control input-tip" id="mdate2" value="<?php echo date("d-m-Y");?>" readonly="readonly" placeholder="Select Date">
                        </div>                      
                    </div>
                    <div class="col-md-2">                     
                        <button type="button" id="get-records" class="btn btn-success" name="get_records"><?= lang('Get Records'); ?></button> 
                      
                    </div>                    
                    <div class="col-md-12 clearfix"></div>
                    <div class="col-md-12">                        
                        <div class="tab-content">
                        
                        <div id="gatepass_view" class="tab-pane in active">
                           <div class="row">
                               <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <table id="stockData" cellpadding="0" cellspacing="0" border="0"
                                               class="table table-bordered table-condensed table-hover table-striped">
                                            <thead>
                                                <tr>
                                                    <th style="min-width:30px; width: 30px; text-align: center;">
                                                        <input class="checkbox checkft" type="checkbox" name="check"/>
                                                    </th>
                                                   <!--<th><?= $this->lang->line("ID"); ?></th>-->
<!--                                                    <th><?= $this->lang->line("Date"); ?></th>-->
                                                    <th style="width:200px !important;"><?= $this->lang->line("Product"); ?></th>
                                                    <th><?= $this->lang->line("Unit"); ?></th>
                                                    <th><?= $this->lang->line("Opening Stock"); ?></th>
                                                    <th><?= $this->lang->line("Receipt(In)"); ?></th>
                                                    <th><?= $this->lang->line("Sale"); ?></th>
                                                    <th><?= $this->lang->line("Other(Out)"); ?></th>
                                                    <th><?= $this->lang->line("Closing Stock"); ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td colspan="8" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                            </tr>
                                            </tbody>
                                            <!-- Footer Added By Anil Start-->
                                            <tfoot class="dtFilter">                                                
                                                <tr class="active">
                                                    <th style="min-width:30px; width: 30px; text-align: center;">
                                                        <input class="checkbox checkft" type="checkbox" name="check"/>
                                                    </th>
                                                   <!--<th><?= $this->lang->line("ID"); ?></th>-->
<!--                                                    <th><?= $this->lang->line("Date"); ?></th>-->
                                                    <th style="width:200px !important;"><?= $this->lang->line("Product"); ?></th>
                                                    <th><?= $this->lang->line("Unit"); ?></th>
                                                    <th><?= $this->lang->line("Opening Stock"); ?></th>
                                                    <th><?= $this->lang->line("Receipt(In)"); ?></th>
                                                    <th><?= $this->lang->line("Sale"); ?></th>
                                                    <th><?= $this->lang->line("Other(Out)"); ?></th>
                                                    <th><?= $this->lang->line("Closing Stock"); ?></th>
                                                </tr>
                                             </tfoot>
                                            <!-- Footer Added By Anil End-->
                                        </table>
                                    </div>
                                  </div>
                                </div>
                            </div>                     
            
                        </div>
                        
                       </div>
                    </div>
                </div>
              </div>
            </div>
         </div>
       </div>
     </div>
    </div>
</div>
<script type="text/javascript">
        var today = new Date();
        var lastDate = new Date(today.getFullYear(), today.getMonth(0), 31);
        var startDate = new Date(today.getFullYear(), 0, 1);    
        
        $('document').ready(function(){
            $("#mdate1").datetimepicker({    
                autoclose:true,
                showSecond: false,
                minView: 2,
                format: 'dd-mm-yyyy',
                startDate: startDate,
                endDate: today

            })
            $("#mdate2").datetimepicker({
                autoclose:true,
                showSecond: false,
                minView: 2,
                format: 'dd-mm-yyyy',
                startDate: startDate,
                endDate: today
            });
            
            $('#get-records').on('click', function(){
                
                var seg_id = $('#seg_id').val(); 
                //For validations start********
                    if(seg_id === ''){
                        alert('Please Select Segment');
                    }
                var startDate = $('#mdate1').val().replace('-','/');
                var endDate = $('#mdate2').val().replace('-','/');
                    if(startDate > endDate){
                        alert('Please select valid date range');
                    }
                //For validations end********
                
                var date1 = $('#mdate1').val();
                var date2 = $('#mdate2').val();
                
                var oTable = $('#stockData').dataTable({
                    "aaSorting": [[1, "asc"], [2, "desc"]],
                    //"aLengthMenu": [[10, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
                    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
                    "iDisplayLength": <?= $Settings->rows_per_page ?>,
                    'bProcessing': true, 'bServerSide': false, 'bDestroy': true,
                    'sAjaxSource': '<?= site_url('reports/msr1') ?>',
                        'fnServerData': function (sSource, aoData, fnCallback) {
                            aoData.push({
                                "name": "<?= $this->security->get_csrf_token_name() ?>",
                                "value": "<?= $this->security->get_csrf_hash() ?>",
                                "name": "pdate",
                                "value": date1+'|'+date2+'|'+seg_id
                            });
                            $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                        }, 
                    "aoColumns": [
            {
                "bSortable": false,
                "mRender": checkbox
        }, 
             null,null, {"mRender": currencyFormat},{"mRender": currencyFormat}, {"mRender": currencyFormat},{"mRender": currencyFormat},{"mRender": currencyFormat}]
    });
                    
            });
         
        });                   
        
    </script>      
<?php //if (!$Owner || !$Admin) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php  //} ?>
<?php if ($action && $action == 'add') {
    echo '<script>$(document).ready(function(){$("#add").trigger("click");});</script>';
}
?>