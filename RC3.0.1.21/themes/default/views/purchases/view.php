
<div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
        <h4 class="modal-title" id="myModalLabel">PO ID: <?php echo $poDetail[0]->auth_po_no; ?></h4>
    </div>
        <div class="modal-body">
            <div class="row">                
                <div class="col-xs-12">
                    <div class="table-responsive">
                         <table class="table table-bordered table-condensed table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Item Name</th>
                                    <th>Total Quantity</th>
                                    <th>Bill Quantity</th>
                                    <th>Balance Quantity</th>
                                    <th>RO No.</th>
                                    <th>RO Date</th>
                                </tr>
                            </thead>
                            <?php foreach($poDetail as $row){
                                ?>
                            <tbody>                             
                                <tr>
                                    <td style="width:50%" ><?php echo $row->itm_name?></td>
                                    <td ><?php echo $row->tot_qty?></td>
                                    <td ><?php echo $row->dlv_qty?></td>
                                    <td ><?php echo $row->bal_qty?></td>
                                    <td ><?php echo $row->ro_no?></td>
                                    <td ><?php echo date('Y-m-d',strtotime($row->ro_dt))?></td>                                    
                                </tr>                                
                                   
                            </tbody>
                            <?php 
                                } ?>
                        </table>
                            
                        </div>
                    </div>
                    <div class="clearfix"></div>   
            </div>
        </div>
    </div>

</div>

</div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $('.change_img').click(function(event) {
        event.preventDefault();
        var img_src = $(this).attr('href');
        $('#pr-image').attr('src', img_src);
        return false;
    });
});
</script>
