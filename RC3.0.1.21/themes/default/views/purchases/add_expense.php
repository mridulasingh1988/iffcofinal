<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('Bank Deposit'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form','id' => 'add-expense-form');
        echo form_open_multipart("purchases/add_expense", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <?php if ($Owner || $Admin) { ?>

                <div class="form-group">
                    <?= lang("date", "date"); ?>
                    <?= form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control datetime" id="date" required="required"'); ?>
                </div>
            <?php } ?>
             <!--added by vikas singh for manual bank Deposit -->
                        <script type="text/javascript">
                            $(function () {
                                $("#manual2").click(function () {
                                  
                                    if ($(this).is(":checked")) {
                                        $("#dvmanual2").show();
                                    } else {
                                        $("#dvmanual2").hide();
                                    }
                                });
                            });
                        </script>
                       <div class="form-group">
                               <label for="manual">
                                   
                                    Do you want Back Date Deposit?</label> 
                                     <input type="checkbox" name="manual2" value="1" id="manual2" />
                               
                      </div>
                     <div class="form-group" id="dvmanual2" style="display: none">
                      <label for="manual2">Deposit Date*</label> 
                      <input type="text" id="deposit_date" name="deposit_date"  required="required" value="" class="form-control col-sm-12 kb-pad"/>
                       
                    </div>
            <div class="form-group">
                <?= lang("reference", "reference"); ?>
                <?= form_input('reference', (isset($_POST['reference']) ? $_POST['reference'] : $exnumber), 'class="form-control tip" id="reference"'); ?>
            </div>

            <div class="form-group">
                <?= lang("Bank Name", "Bank Name"); ?>*
          
                            <?php
                               $fbankdetails = array_diff($bankdetails, array(""));
                               $fbankdetails['']=array('' => 'Select Bank Name' );
                            echo form_dropdown('bank_name', $fbankdetails,'','class="form-control tip" id="bank_name" style="width:100%;" required="required"'); ?>
                    
            </div>


            <div class="form-group">
                <?= lang("amount", "amount"); ?>*
                <input name="amount" type="text" id="amount" value="" class="pa form-control kb-pad amount"
                       required="required"/>
            </div>

            <div class="form-group">
                <?= lang("attachment", "attachment") ?>
                <input id="attachment" type="file" name="userfile" data-show-upload="false" data-show-preview="false"
                       class="form-control file">
            </div>

            <div class="form-group">
                <?= lang("note", "note"); ?>
                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="note"'); ?>
            </div>

        </div>
        <div class="modal-footer">
            <?php echo form_submit('add_expense', lang('Add Bank Deposit'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
</script>
<!-- <?= $modal_js ?> -->

<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
        $("#date").datetimepicker({
            format: site.dateFormats.js_ldate,
            fontAwesome: true,
            language: 'sma',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0
        }).datetimepicker('update', new Date());
    });

    $("#add-expense-form").validate({

        rules:{
               'bank_name':{
                            required:true
                           
                        },

               'amount':{
                            required:true,
                             amt: true
                           
                        }        
                 
           },
           messages:{
               
               'bank_name':{
                           required:'Please select bank name',
                          
                   
               },

               'amount':{
                           required:'Please enter amount',
                           amt:'Enter valid amount'
               }
                  
           },
           submitHandler:function(form){
             form.submit();
           }
           
       });

    $.validator.addMethod("amt",
                 function(value, element) {
                    return /^[1-9]\d*(\.\d+)?$/.test(value);
    }); 

</script>

<script type="text/javascript"> var today = new Date();
    var lastDate = new Date(today.getFullYear(), today.getMonth(0), 31);
    var startDate = new Date(today.getFullYear(), 0, 1);

    //var lastDate = new Date(today.getFullYear(), today.getMonth(0), 31);
    jQuery("#deposit_date").datetimepicker({
        autoclose:true,
        showSecond: false,
        // minView: 2,
        format: 'yyyy-mm-dd H:i:s',
        startDate: startDate,
        endDate: today
    });

    </script>

