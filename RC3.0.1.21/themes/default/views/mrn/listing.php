<script>
    $(document).ready(function () { 
        $( document ).on( 'click', '.delete-icon', function() {
            if(confirm("Are you sure you want to delete this record?")){
                var obj = $(this);
                var rcpt_id = $(this).attr('id');
                $.ajax({
                    url: site.base_url+"mrn/deletemrn",
                    async: false,
                    type: "POST",
                    data: {"rcpt_id":rcpt_id},
                    dataType:"json",
                    success: function(data) {                       
                        if(data.response==1){
                            obj.closest("tr").remove();
                        }else{
                            alert("failed to delete Mrn. Please try again!!");
                            return false;
                        }
                    }
                })
            }
        });      
        var oTable = $('#mrnData').dataTable({
            "order": [[0, 'desc']],
            "aaSorting": [[0, "asc"]],
            "aLengthMenu": [[10, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('mrn/getMrn') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                  
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax( {
                        'dataType': 'json',
                        'type': 'POST',
                        'url': sSource,
                        'dataFilter': function(data){
                            var json = jQuery.parseJSON( data );
                            json.recordsTotal = json.total;
                            json.recordsFiltered = json.total;
                            json.data = json.list;
                           
                            return JSON.stringify( json ); // return JSON string
                        },
                        'data': aoData,
                        'success': fnCallback,
                       

                });
            },

             "fnCreatedRow" : function( nRow, aData, iDataIndex) {
                var obj = $(nRow);
                if(obj.children('td').eq(4).text()=='Pending'){
                    obj.find(".edit-icon").show();
                    obj.find(".delete-icon").show();
                }else{
                    obj.find(".edit-icon").hide();
                    obj.find(".delete-icon").hide();
                }


                },
            "aoColumns": [{
                "bSortable": true,
                "mRender": checkbox
            //}, null, null, null, null, null, null, null, null, {"bSortable": false}]
        }, null, null, /*null,*/  null,null, {"bSortable": true}]
        }).dtFilter([
           // {column_number: 1, filter_default_label: "[<?=lang('company');?>]", filter_type: "text", data: []},
            {column_number: 1, filter_default_label: "[<?=lang('rcpt_no');?>]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('rcpt_dt');?>]", filter_type: "text", data: []},
            //{column_number: 3, filter_default_label: "[<?=lang('auth_po_no');?>]", filter_type: "text", data: []},
            //{column_number: 4, filter_default_label: "[<?=lang('ro_no');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('supplier');?>]", filter_type: "text", data: []},
            
            {column_number: 4, filter_default_label: "[<?=lang('status');?>]", filter_type: "text", data: []},            
           // {column_number: 5, filter_default_label: "[<?=lang('vat_no');?>]", filter_type: "text", data: []},
           // {column_number: 8, filter_default_label: "[<?=lang('award_points');?>]", filter_type: "text", data: []},
        ], "footer");
    });
</script>
<?php if ($Owner) {
    echo form_open('customers/customer_actions', 'id="action-form"');
} ?>
<div class="box">
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <?php if($this->Owner || $this->Admin){ ?>
                     <p class="introtext"><?= lang('list_results'); ?></p>
                <?php } ?> 

                <div class="table-responsive">
                    <table id="mrnData" cellpadding="0" cellspacing="0" border="0"
                           class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                        <tr class="primary">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkth" type="checkbox" name="check"/>
                            </th>
                            <!-- <th><?= lang("company"); ?></th> -->
                            <th><?= lang("rcpt_no"); ?></th>
                            <th><?= lang("rcpt_dt"); ?></th>
                            <!-- <th><?= lang("auth_po_no"); ?></th>
                            <th><?= lang("ro_no"); ?></th> -->
                            <th><?= lang("supplier"); ?></th>
                            <th><?= lang("status"); ?></th>
                             <!-- <th><?= lang("award_points"); ?></th> -->
                            <th style="width:85px;"><?= lang("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="5" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft" type="checkbox" name="check"/>
                            </th>                            
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                   
                            <th style="width:85px;" class="text-center"><?= lang("actions"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>
<?php if ($action && $action == 'add') {
    echo '<script>$(document).ready(function(){$("#add").trigger("click");});</script>';
}
?>
