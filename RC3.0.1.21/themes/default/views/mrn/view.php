<?php
/*echo "<pre>";
print_r($mrnDetail);
exit;*/
?>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
        <h4 class="modal-title" id="myModalLabel">MRN RECIEPT- <?= $mrnDetail[0]->rcpt_no; ?></h4>
    </div>
        <div class="modal-body">
            <div class="row">                
                <div class="col-xs-10">
                    <div class="table-responsive">
                         <?php foreach($mrnDetail as $row){
                        ?>
                        <div>PO NO: <?php echo $row[0]->auth_po_no?></div>
                        <div>PO Date: <?php echo date('Y-m-d',strtotime($row[0]->po_date))?></div>
                        <table class="table table-bordered table-condensed table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Item Name</th>
                                    <th>Total Quantity</th>
                                    <th>Bill Quantity</th>
                                    <th>Received Quantity</th>
                                    <th>Short/Excess Quantity</th>
                                    <th>Rejected Quantity</th>
                                </tr>
                            </thead>
                            <?php foreach($row as $val){                              
                            ?>                            
                                <tbody>                             
                                    <tr>
                                        <td style="width:60%" ><?php echo $val->name?></td>
                                        <td ><?php echo $val->tot_rcpt_qty?></td>
                                        
                                        <td ><?php echo $val->tot_rcpt_qty?></td>
                                        <td ><?php echo $val->rcpt_qty?></td>
                                        <td ><?php echo $val->pending_qty?></td>
                                        <td ><?php echo $val->rej_qty?></td>                                    
                                    </tr>                                
                                       
                                    </tbody>
                                
                            <?php }
                            ?>
                            </table>
                            <?php 
                                } ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>   
            </div>
        </div>
    </div>

    <div class="col-xs-12">

        <?= $product->details ? '<div class="panel panel-success"><div class="panel-heading">' . lang('product_details_for_invoice') . '</div><div class="panel-body">' . $product->details . '</div></div>' : ''; ?>
        <?= $product->product_details ? '<div class="panel panel-primary"><div class="panel-heading">' . lang('product_details') . '</div><div class="panel-body">' . $product->product_details . '</div></div>' : ''; ?>

    </div>
</div>

</div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $('.change_img').click(function(event) {
        event.preventDefault();
        var img_src = $(this).attr('href');
        $('#pr-image').attr('src', img_src);
        return false;
    });
});
</script>

