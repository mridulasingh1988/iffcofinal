<style>
.form-group label {
    float: left;
}
.form-group{
    margin-top: 15px;
}
</style>
<script type="text/javascript" src="<?= $assets ?>js/mrn.js"></script>
<?php
$attrib = array('data-toggle' => 'validator', 'role' => 'form','id'=>'mrn-form');
echo form_open_multipart("mrn/add/".$mrn_id."", $attrib)
?>
<input type="hidden" name="submit-type" id="submit-type" value="" /> 
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('add_mrn'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?php echo lang('enter_info'); ?></p>                
                <div class="row">
                    <div class="col-lg-12">
                         <div class="panel panel-warning">
                            <div class="panel-heading"><?= lang('basic_details') ?></div>

                                <div class="panel-body" style="padding: 5px;">
                                    <div class="col-md-6">

                                        <div class="form-group required">
                                            <?= lang("warehouse", "warehouse"); ?> &nbsp;                             
                                            <?php
                                           // $wh[''] = 'Please Select Warehouse';
                                            foreach ($warehouse_name as $warehouse) {
                                                $wh[$warehouse['id']] = $warehouse['name'];
                                            }
                                            echo form_dropdown('warehouse_id', $wh, (isset($data['wh_id']) ? $data['wh_id'] : $wh['']), 'id="warehouse_id" class="form-control input-tip required" readonly="readonly" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("warehouse") . '"   style="width:50%;" ');
                                            ?>
                                        </div>
                                    </div>
                               
                       <!--  <div class="col-md-3">
                            <div class="form-group">
                                <?= lang("source_type", "source_type"); ?>
                                <br>
                                <?php
                                echo "Supplier";
                                /*$sup[0] = 'Please Select Supplier';
                                foreach ($supplier_details as $sups) {
                                    $sup[$sups->id] = $sups->name;
                                }
                                echo form_dropdown('source_type', $sup, $sup[0], 'id="source_type" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("source_type") . '" required="required" style="width:100%;" ');*/
                                ?>
                            </div>
                        </div> 
                        <div class="col-md-3">
                            <div class="form-group">
                                <?= lang("source_doc_type", "source_doc_type"); ?>
                                <br>
                                <?php
                                echo "Purchase Order";
                               /* $wh[''] = '';
                                foreach ($warehouses as $warehouse) {
                                    $wh[$warehouse->id] = $warehouse->name;
                                }
                                echo form_dropdown('source_doc_type', $wh, (isset($_POST['source_doc_type']) ? $_POST['source_doc_type'] : $Settings->default_warehouse), 'id="source_doc_type" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("source_doc_type") . '" required="required" style="width:100%;" ');*/
                                ?>
                            </div>
                        </div>-->
                        <div class="col-md-6">
                            <div class="form-group required">
                                <?= lang("SUPPLIER", "SUPPLIER"); ?> &nbsp;
                                
                                <?php
                               // echo "Iffco";
                                $sup[''] = 'Please Select Supplier';
                                foreach ($supplier_details as $sups) {
                                    $sup[$sups->eo_id] = $sups->name;
                                }                                
                                echo form_dropdown('supplier_id', $sup, (isset($data['sel_sup_id']) ? $data['sel_sup_id'] : $sup[0]), 'id="supplier_id" class="form-control input-tip required" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("source_type") . '"  style="width:50%;" ');                  
                                ?>
                               <!--  <div class="input-group-addon no-print" style="padding: 2px 5px;">
                                    <a href="<?= site_url('suppliers/add'); ?>" id="add-supplier" class="external" data-toggle="modal" data-target="#myModal">
                                        <i class="fa fa-2x fa-plus-circle" id="addIcon"></i>
                                    </a>
                                </div> -->
                            </div>
                        </div>
                     </div>
                
                        <!-- <div class="col-md-3">
                            <div class="form-group">
                                <?= lang("currency", "currency"); ?>
                                <br>
                                <?php                                   
                                    echo $currency;                          
                                ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <?= lang("conversion_fac", "currency"); ?>
                                <br>
                                <?php                                   
                                    echo $conv_factor;                              
                                ?>
                            </div>
                        </div> -->
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-warning">
                        <div class="panel-heading"><?= lang('invoice_details') ?></div>
                        <div class="panel-body" style="padding: 5px;">                                              
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= lang("del_note_no", "del_note_no"); ?>&nbsp;&nbsp;&nbsp;
                                    <?php echo form_input('del_note_no', (isset($data['dn_no']) ? $data['dn_no'] : ''), 'class="form-control input-tip" style="width:50%;float:left" id="del_note_no"'); ?>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= lang("del_note_date", "del_note_date"); ?>
                                    <?php echo form_input('del_note_date', (isset($data['dn_dt']) && $data['dn_dt']!='1970-01-01' ? $data['dn_dt'] : ""), 'class="form-control input-tip" id="del_note_date" readonly="readonly" placeholder="Delivery Note Date" style="width:50%;float:left"'); ?>
                                </div>
                            </div>
                        </div>    
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-warning">
                        <div class="panel-heading"><?= lang('transpoter_details') ?></div>
                        <div class="panel-body" style="padding: 5px;">                                              
                            <div class="col-md-4">
                                <div class="form-group required">
                                    <?= lang("Name", "Name"); ?>
                                    <?php 
                                    $tp[''] = 'Select Transporter';
                                     //$po[0] = "Select PO No.";

                                    foreach ($tp_list as $val) {
                                        $tp[$val->eo_id] = $val->name;
                                    }
                                    echo form_dropdown('lr_name', $tp, (isset($data['tp_id']) ? $data['tp_id'] : $tp['']), 'id="lr_name" class="form-control input-tip required" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("name") . '" style="width:100%;" ');
                                    ?>
                                </div>
                            <!-- <div class="form-group">
                                <?= lang("name", "lr_name"); ?>
                                <?php echo form_input('lr_name', (isset($_POST['lr_name']) ? $_POST['lr_name'] : $lr_name), 'class="form-control input-tip" id="lr_name"'); ?>
                            </div> -->
                            </div>                        
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("lr_no", "lr_no"); ?>
                                    <?php echo form_input('lr_no', (isset($data['tpt_lr_no']) ? $data['tpt_lr_no'] : ""), 'class="form-control" id="lr_no"'); ?>
                                </div>
                            </div>                        
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("lr_date", "lr_date"); ?>
                                    <?php echo form_input('lr_date', (isset($data['tpt_lr_dt']) && $data['dn_dt']!='1970-01-01' ? $data['tpt_lr_dt'] : $lr_date), 'class="form-control input-tip" id="lr_date" readonly="readonly" placeholder="LR Date"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("rcpt_dt", "rcpt_dt"); ?>
                                    <?php echo form_input('rcpt_dt', (isset($data['rcpt_dt']) && $data['rcpt_dt']!='1970-01-01' ? $data['rcpt_dt'] : $rcpt_dt), 'class="form-control input-tip" id="rcpt_dt" readonly="readonly" placeholder="'.lang('rcpt_dt').'"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">                                
                                    <div style="float:left"><?= lang("vehicle_no", "vehicle_no"); ?></div>
                                    <div>
                                    <?php echo form_input('vehicle_no', (isset($data['vehicle_no']) ? $data['vehicle_no'] : ""), 'class="form-control" id="vehicle_no"'); ?>
                                    </div>
                                </div>
                            </div>                        
                            <div class="col-md-4">
                                <div class="form-group required">
                                    <?= lang("other_charges", "other_charges"); ?>
                                    <?php echo form_input('other_charges', (isset($data['addl_amt']) ? round($data['addl_amt'],2) : ""), 'class="form-control input-tip" id="other_charges"'); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>                      
                            <div class="col-md-9">
                                <div class="form-group">
                                    <?= lang("remarks", "remarks"); ?>
                                    <?php echo form_textarea('remarks', (isset($data['remarks']) ? $data['remarks'] : $data['remarks']), 'rows="4" cols="10" class="form-control input-tip" id="remarks" '); ?>
                                </div>
                            </div>
                        </div>    
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-warning">
                        <div class="panel-heading"><?= lang('po_details') ?></div>
                        <div class="panel-body" style="padding: 5px;">                                              
                            <div class="col-md-4">                            
                                <div class="form-group">
                                    <?= lang("po_no", "po_no"); ?>
                                    <?php                         

                                    $po[0] = 'Select';
                                     //$po[0] = "Select PO No.";
                                    if(count($data['po_list'])>0){
                                        foreach ($data['po_list'] as $val) {
                                            $po[$val->id."~".$val->auth_po_no] = $val->auth_po_no;
                                        }
                                    }
                                    echo form_dropdown('po_no', $po, $po[0], 'id="po_no" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("po_no") . '" required="required" style="width:100%;" ');
                                    ?>
                                </div>
                            </div>                        
                            <div class="col-md-4">                            
                                <div class="form-group">
                                    <?= lang("ro_no", "ro_no"); ?>
                                    <?php
                                    $ro[0] = "Select Ro No.";                                
                                    echo form_dropdown('ro_no', $ro, $ro[0], 'id="ro_no" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("ro_no") . '" required="required" style="width:100%;" ');
                                    ?>
                                </div>                            
                            </div>                        
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("ro_date", "ro_date"); ?>
                                    <?php echo form_input('ro_date', (isset($_POST['ro_date']) ? $_POST['ro_date'] : $ro_date), 'class="form-control input-tip" id="ro_date" readonly="readonly" placeholder="RO Date"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="from-group">
                                    <?php echo form_button('add_po_details', $this->lang->line("add_reciept"), 'id="add_po_details" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                                </div>
                            </div>
                            <div id="po_ro_tab_div">
                            <?php if(isset($ro_data)){ 
                                $i=0;
                                    foreach($ro_data as $key=>$row){ 

                                        foreach($row['ro_val'] as $val1){ 
                                            // fetching lot details
                                            $lot_data = array();
                                            $lot_datas = array();
                                            $bin_data = array();
                                            $bin_datas = array();
                                            $temp_ar = array();
                                            foreach($row[$val1] as $val2){
                                                if(!in_array($val2->pro_lot_no,$lot_data[$val1][$val2->item_id])){
                                                    $lot_data[$val1][$val2->item_id][] = $val2->pro_lot_no;
                                                    $lot_datas[$val1][$val2->item_id][] = $val2;
                                                    $bin_data[$val1][$val2->item_id][] = $val2->pro_lot_no;
                                                    $bin_datas[$val1][$val2->item_id][$val2->pro_lot_no] = array();
                                                }
                                                $bin_datas[$val1][$val2->item_id][$val2->pro_lot_no][] = $val2;
                                            }   
                                            //->lot_qty."~".$val2->pro_lot_no."~".$val2->bin_qty
                                                 /* echo "<pre>";
                                                  print_r($bin_datas);
                                                  exit; */                            
                                       

                                ?>
                            
                                <div class="col-sm-12 po_ro_tab" id="po_<?php echo $val1 ?>_<?php echo $val2->po_id ?>" style="">
                                <hr></hr>
                                <div class="accordian-div">
                                    <div  id="<?php echo $val2->mrn_src_id ?>_<?php echo $val1 ?>" class="col-sm-12 delete-mrn text-right" style="cursor:pointer;float:right;width:20px;line-height:23px;"><i class="fa fa-trash"></i></div>
                                    <div class="col-sm-6 text-left">PO NO: <?php echo $val2->auth_po_no ?></div>
                                    <div class="col-sm-5 text-left">RO No: <?php echo $val1 ?></div></div>
                                    <div class="row" style="margin-bottom: 15px;" >
                                    <div class="col-md-12" >
                                    <div class="box-content  box-content-div" style="border:1px solid #DDDDDD;border-radius:2px;<?php echo $i==0?"display: block":"display: none"?>">
                                    <div class="row inner-content">
                                    <div class="col-md-12">
                                    <ul id="dbTab" class="nav nav-tabs">
                                    <li class="item_details_tab active"><a href="javascript:void(0)"><?= lang('item_details') ?></a></li>
                                    <li class="stock_details_tab"><a href="javascript:void(0)"><?= lang('stock_details') ?></a></li>
                                    </ul>
                                    <div class="tab-content">
                                    <div id="item_details" class="tab-pane fade in active">
                                    <div class="row">
                                    <div class="col-sm-12">
                                    <div class="table-responsive">
                                    <table class="table table-bordered table-condensed totals " style="margin-bottom:0;">
                                    <thead>
                                    <tr>
                                    <th><?= lang("item_name"); ?></th>
                                    <th><?= lang("Unit"); ?></th>
                                    <th><?= lang("bill_qty"); ?></th>
                                    <th><?= lang("recv_qty"); ?></th>
                                    <th><?= lang("rej_qty"); ?></th>
                                    <th><?= lang("short_qty"); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <input type="hidden" name="mrn_list[]" class="form-control mrn_list" value="<?php echo $val2->po_id?>" />
                                     <input type="hidden" name="ro_list[<?php echo $val2->po_id?>][]" class="form-control ro_list" value="<?php echo $val1?>" />
                                   <!--  <input type="hidden" name="ro_date_list['+po_no+'][]" class="form-control" value="'+ro_date+'" />  -->
                                    <?php                                 
                                        if(count($row[$val1])>0){
                                            $tmp_item_array = array();
                                            foreach($row[$val1] as $item_value){                                             
                                                if(!in_array($item_value->item_id, $tmp_item_array)){
                                                   
                                                    array_push($tmp_item_array,$item_value->item_id);
                                                                              
                                        ?>                   
                                                <!--var dlv_qty = parseFloat(product_array[i].dlv_qty).toFixed(2); -->  
                                            <tr id="prod_<?php echo $item_value->mrn_itm_id?>">
                                                <td><?php echo $item_value->name ?>
                                                <td><?php echo $item_value->unit ?>         
                                                <input type="hidden" name="item_list[<?php echo $val2->po_id."-".$val1?>][]" class="form-control" value="<?php echo $item_value->item?>" />
                                                </td>
                                                <td>
                                                <input type="text" name="mrn_bill_qty[<?php echo $val2->po_id."-".$val1?>][<?php echo $item_value->mrn_itm_id?>]" class="form-control bill_qty show-decimal" value="<?php echo number_format($item_value->dlv_note_qty,2)?>" оnkeypress="return isNumberKey(event,this)" required="required" readonly="readonly" />
                                                </td>
                                                <td>
                                                <input type="text" name="mrn_rec_qty[<?php echo $val2->po_id."-".$val1?>][<?php echo $item_value->mrn_itm_id?>]" id="rec_qty_<?php echo $item_value->mrn_itm_id?>" data-maxQty="<?php echo $item_value->maxQty?>" value="<?php echo number_format($item_value->rcpt_qty,2)?>" at="rec_qty" class="form-control show-decimal rec_qty" оnkeypress="return isNumberKey(event,this)" required="required"/>
                                                </td>
                                                <td>
                                                    <input type="text" name="mrn_rej_qty[<?php echo $val2->po_id."-".$val1?>][<?php echo $item_value->mrn_itm_id?>]" class="form-control rej_qty show-decimal" value="<?php echo $item_value->rej_qty==''?"0.00":number_format($item_value->rej_qty,2)?>" required="required" оnkeypress="return isNumberKey(event,this)" /></td>
                                                <td data-original-title="Please enter lot for all the items" data-container="body" data-toggle="tooltip" data-placement="bottom">
                                                    <input type="text" name="mrn_short_qty[<?php echo $val2->po_id."-".$val1?>][<?php echo $item_value->mrn_itm_id?>]" class="form-control short_qty show-decimal" value="<?php echo number_format($item_value->pending_qty,2)?>" required="required" оnkeypress="return isNumberKey(event,this)" readonly="readonly" />
                                                </td>
                                            </tr>
                                    <?php
                                            }

                                        } 
                                    }
                                    ?>
                                    </tbody>
                                    </table>
                                    </div>
                                    </div>
                                    </div>
                                </div>
                                <div id="stock_details" class="tab-pane fade in">
                                <div class="row">
                                <div class="col-sm-12">
                                <div class="table-responsive">
                                <div class="col-sm-3">
                                <table class="table table-bordered table-condensed totals item_table" style="margin-bottom:0;">
                                <thead>
                                <tr>
                                <th><?= lang("item_name"); ?></th>
                                <th><?= lang("qty"); ?></th>
                                <th><?= lang("action"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php                                      
                                    if(count($row[$val1])>0){
                                        $tmp_item_arrays = array();                                      
                                        foreach($row[$val1] as $item_values){
                                            
                                            if(!in_array($item_values->item_id, $tmp_item_arrays)){
                                             
                                                array_push($tmp_item_arrays,$item_values->item_id);
                                                                          
                                ?>
                                    <tr class="bin_background" id="slot_<?php echo $item_values->mrn_itm_id?>" data-original-title="" data-container="body" data-toggle="tooltip" data-placement="bottom">                
                                        <td><?php echo $item_values->name?></td>
                                        <td class="rec_qty_<?php echo $item_values->mrn_itm_id?>"><?php echo number_format($item_values->rcpt_qty,2)?></td>
                                        <td><a href="javascript:void(0)" class="allocate_lot lot_<?php echo $item_value->mrn_itm_id?>" data-check="mrn_type" data-id="lot_<?php echo $item_values->mrn_itm_id?>_<?php echo $val1?>">Allocate Lot</a></td>
                                    </tr>
                                <?php                                            
                                        } 
                                    }    
                                  //  exit;                              
                                }
                                ?>
                                </tbody>
                                </table>
                                </div>
                                <div class="col-sm-9 stock_data_div">
                                <!--for(var i in product_array){-->
                                    <?php                                   
                                    if(count($lot_datas)>0){
                                        //if(count($row[$val1])>0){//
                                        $tmp_item_arrays = array();                                      
                                        foreach($row[$val1] as $itm_key=>$item_values){
                                            if(!in_array($item_values->item_id, $tmp_item_arrays)){
                                               
                                                array_push($tmp_item_arrays,$item_values->item_id);
                                                if($itm_key==0)
                                                    $display = "";
                                                else
                                                    $display = "display:none";
                                            
                                    ?>
                                    <div class="lot_bin_div_<?php echo $item_values->mrn_itm_id?> top_lot_div" style="<?php echo $display?>">
                                        <div class="lot_div lot_<?php echo $item_values->mrn_itm_id?>">
                                            <table class="table table-bordered table-condensed lot_bin_table" id="lot_<?php echo $item_values->mrn_itm_id?>" style="margin-bottom:0;">
                                                    <thead>
                                                        <tr>
                                                            <th><?= lang("lot_name"); ?></th>
                                                            <th><?= lang("qty"); ?></th>
                                                            <th><?= lang("action"); ?></th>
                                                        </tr>
                                                        </thead>
                                                    <tbody>
                                                    <?php                                          
                                                    foreach($lot_datas[$val1][$item_values->item_id] as $lot_key=>$lot_vals){
                                                        $show = $lot_key==0?"background":"";
                                                        if($lot_vals->lot_qty){                       
                                                    ?> 
                                                       <tr class="<?php echo $show?>" data-id="lot_<?php echo $lot_vals->pro_lot_no?>_<?php echo $item_values->mrn_itm_id?>"> 
                                                            <td>
                                                           <!--  <input type="text" name="lot_data[<?php echo $item_values->mrn_itm_id?>][]" value="<?php echo json_encode($lot_vals)?>"> -->
                                                           <!--  <input type="text" class="lot-class" name="lot_qty[<?php echo $item_values->mrn_itm_id?>][]" value="<?php echo $lot_vals->lot_qty?>" /> -->
                                                                <input type="hidden" class="lot-class" name="lot_qty[<?php echo $item_values->mrn_itm_id?>][]" value="<?php echo $lot_vals->lot_qty?>" />
                                                            <?php echo $lot_vals->pro_lot_no?>                                       
                                                            </td>
                                                            <td><?php echo number_format($lot_vals->lot_qty,2)?></td>
                                                            <td><a href="javascript:void(0)" id="bin[<?php echo $item_values->mrn_itm_id?>][]" data-id="<?php echo $lot_vals->pro_lot_no?>_<?php echo $item_values->mrn_itm_id?>_<?php echo number_format($lot_vals->lot_qty,2)?>" class="allocate_bin" data-check="mrn_type" data-is-new="1">Allocate Bin</a>&nbsp;<a href="javascript:void(0)" data-exist="1" data-del="<?php echo $lot_vals->pro_lot_no?>_<?php echo $item_values->mrn_itm_id?>" class="delete">Delete</a></td>
                                                        </tr>
                                                    <?php
                                                        }
                                                    }
                                                    ?> 
                                                    </tbody>
                                            </table>
                                        </div>
                                        <div class="bin_div bin_<?php echo $item_values->mrn_itm_id?>">
                                            <?php
                                            foreach($lot_data[$val1][$item_values->item_id] as $lot_keys=>$lot_value){
                                                $bin_show = $lot_keys!=0?"display:none;":"";
                                                if($lot_value!=''){
                                            ?>
                                               <table class="table table-bordered table-condensed bin_table" data-id="lot_bin_<?php echo $lot_value?>_<?php echo $item_values->mrn_itm_id?>" style="margin-bottom:0;<?php echo $bin_show ?>">
                                                <thead>
                                                    <tr>
                                                        <th><?= lang("bin_name"); ?></th>
                                                        <th><?= lang("qty"); ?></th>
                                                        <th><?= lang("action"); ?></th>
                                                    </tr>
                                                </thead>
                                                 <?php
                                                foreach($bin_datas[$val1][$item_values->item_id][$lot_value] as $bin_key=>$bin_val){                                                   
                                                    if($bin_val->bin_mstr_id){                 
                                                ?>
                                                    <tr>            
                                                        <td><input type="hidden" class="" name="bin_qty[<?php echo $lot_value?>][]" value="<?php echo $bin_val->bin_qty?>" /><?php echo $bin_val->bin_nm
                                                        ?>           
                                                        </td>
                                                        <td><?php echo number_format($bin_val->bin_qty,2)?></td>
                                                        <td><a href="javascript:void(0)" id="<?php echo $bin_val->bin_mstr_id?>" class="deletebin">Delete</a></td>
                                                    </tr>

                                                <?php
                                                        }
                                                    }
                                                    ?>
                                            </table>   

                                            <?php
                                            }
                                        }
                                        ?>
                                        </div>
                                    </div>
                                    <?php 
                                    }
                                    ?>
                                   
                                <?php
                                }
                            }                                                                              
                            ?>  
                                                              
                               
                                <!--} -->                                
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                </div></div>
                            <?php
                                        }
                                    $i++;
                                    }

                                }
                            ?>
                            </div>
                        </div>    
                    </div>
                </div>
               
                  
                
                         
             
<!--                <div class="row">
                    <div class="col-lg-12"><hr></hr><?=lang("invoice_details");?></div>
                </div>                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("del_note_no", "del_note_no"); ?>
                                <?php echo form_input('del_note_no', (isset($_POST['del_note_no']) ? $_POST['del_note_no'] : $del_note_no), 'class="form-control input-tip" id="del_note_no"'); ?>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("del_note_date", "del_note_date"); ?>
                                <?php echo form_input('del_note_date', (isset($_POST['del_note_date']) ? $_POST['del_note_date'] : $del_note_date), 'class="form-control input-tip" id="del_note_date" readonly="readonly" placeholder="Delivery Note Date"'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-lg-12"><hr></hr><?=lang("transpoter_details");?></div>
                </div>
                
                <div class="row">
                    <div class="col-lg-12">
                        
                        <div class="col-md-4">
                        <div class="form-group">
                                <?= lang("Name", "Name"); ?>
                                <?php                         

                                $tp[0] = 'Select Transporter';
                                 //$po[0] = "Select PO No.";

                                foreach ($tp_list as $val) {
                                    $tp[$val->id] = $val->name;
                                }
                                echo form_dropdown('lr_name', $tp, $tp[0], 'id="lr_name" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("name") . '" required="required" style="width:100%;" ');
                                ?>
                            </div>
                             <div class="form-group">
                                <?= lang("name", "lr_name"); ?>
                                <?php echo form_input('lr_name', (isset($_POST['lr_name']) ? $_POST['lr_name'] : $lr_name), 'class="form-control input-tip" id="lr_name"'); ?>
                            </div> 
                        </div>
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("lr_no", "lr_no"); ?>
                                <?php echo form_input('lr_no', (isset($_POST['lr_no']) ? $_POST['lr_no'] : $lr_no), 'class="form-control" id="lr_no"'); ?>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("lr_date", "lr_date"); ?>
                                <?php echo form_input('lr_date', (isset($_POST['lr_date']) ? $_POST['lr_date'] : $lr_date), 'class="form-control input-tip" id="lr_date" readonly="readonly" placeholder="LR Date"'); ?>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("vehicle_no", "vehicle_no"); ?>
                                <?php echo form_input('vehicle_no', (isset($_POST['vehicle_no']) ? $_POST['vehicle_no'] : $vehicle_no), 'class="form-control" id="vehicle_no"'); ?>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("other_charges", "other_charges"); ?>
                                <?php echo form_input('other_charges', (isset($_POST['other_charges']) ? $_POST['other_charges'] : $other_charges), 'class="form-control input-tip" id="other_charges"'); ?>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("remarks", "remarks"); ?>
                                <?php echo form_input('remarks', (isset($_POST['remarks']) ? $_POST['remarks'] : $remarks), 'class="form-control input-tip" id="remarks"'); ?>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-lg-12"><hr></hr><?=lang("po_details");?></div>
                </div>
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-md-4">
                            
                            <div class="form-group">
                                <?= lang("po_no", "po_no"); ?>
                                <?php                         

                                $po[0] = 'Select';
                                 //$po[0] = "Select PO No.";

                                foreach ($po_list as $val) {
                                    $po[$val->id] = $val->auth_po_no;
                                }
                                echo form_dropdown('po_no', $po, $po[0], 'id="po_no" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("po_no") . '" required="required" style="width:100%;" ');
                                ?>
                            </div>

                        </div>
                        
                        <div class="col-md-4">
                            
                            <div class="form-group">
                                <?= lang("ro_no", "ro_no"); ?>
                                <?php
                                $ro[0] = "Select Ro No.";                                
                                echo form_dropdown('ro_no', $ro, $ro[0], 'id="ro_no" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("ro_no") . '" required="required" style="width:100%;" ');
                                ?>
                            </div>
                            
                        </div>
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("ro_date", "ro_date"); ?>
                                <?php echo form_input('ro_date', (isset($_POST['ro_date']) ? $_POST['ro_date'] : $ro_date), 'class="form-control input-tip" id="ro_date" readonly="readonly" placeholder="RO Date"'); ?>
                            </div>
                        </div>
                        
                         <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("gatepass_no", "gatepass_no"); ?>
                                <?php echo form_input('gatepass_no', (isset($_POST['gatepass_no']) ? $_POST['gatepass_no'] : $gatepass_no), 'class="form-control input-tip" id="gatepass_no"'); ?>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("gatepass_date", "gatepass_date"); ?>
                                <?php echo form_input('gatepass_date', (isset($_POST['gatepass_date']) ? $_POST['gatepass_date'] : $gatepass_date), 'class="form-control input-tip" id="gatepass_date" readonly="readonly" placeholder="Gatepass Date"'); ?>
                            </div>
                        </div> 
                        
                        <div class="col-md-4">
                            <div class="from-group">
                                <?php echo form_button('add_po_details', $this->lang->line("add_reciept"), 'id="add_po_details" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                            </div>
                        </div>

                        <div id="po_ro_tab_div"></div>
                        
                    </div>
                </div>-->
                
            </div>           

        <div class="col-md-12">
            <div class="from-group">
                <button type="submit" class="btn btn-primary" id="save-sale"><?= lang('save'); ?></button>
               <!--  <button type="submit" class="btn btn-primary" style="background-color: green" id="hold-sale"><?= lang('hold'); ?></button> -->
                <?php //echo form_submit('add_mrn', $this->lang->line("submit"), 'id="add_mrn" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                <!-- <button type="button" class="btn" id="hold">
                    <?= lang('hold') ?></button> -->
                    <button type="button" class="btn" onclick="location.href = '<?php echo base_url()?>';">
                    <?= lang('Cancel') ?></button>
            </div>
        </div>
    </div>
       
            
    </div>

        </div>
    </div>
</div>
<?php echo form_close(); ?>
