<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= $title ?></title>
    <script type="text/javascript">if (parent.frames.length !== 0) {
            top.location = '<?=site_url('pos')?>';
        }</script>
<style>
    .form-group label {
        float: left;
        margin-right: 10px;
    }
    .form-group{
        margin-top: 15px;
    }
    </style>
    <script>
    $(document).ready(function () {

    $( "#save-sale" ).click(function() {
        $('#modal-loading').show();
        $('.blackbg').css('zIndex', '1041');
        $('.loader').css('zIndex', '1042');
    });


    });

</script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= $assets ?>images/icon.png"/>
    <link href="<?= $assets ?>styles/theme.css" rel="stylesheet"/>
    <link href="<?= $assets ?>styles/style.css" rel="stylesheet"/>
    <link href="<?= $assets ?>styles/helpers/login.css" rel="stylesheet"/>
    <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
    <!--[if lt IE 9]>
    <script src="<?= $assets ?>js/respond.min.js"></script>
    <![endif]-->

</head>

<body class="login-page">

<?php
$attrib = array('data-toggle' => 'validator', 'role' => 'form','id'=>'mrn-form');
echo form_open_multipart("syncin/syncin", $attrib)
?>
<input type="hidden" name="submit-type" id="submit-type" value="" /> 
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('Sync Data from Central Server'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?php echo lang('enter_info'); ?></p>                
                <div class="row">
                    <div class="col-lg-12">
                         <div class="panel panel-warning">
                            <div class="panel-heading"><?= lang('basic_details') ?></div>

                                <div class="panel-body" style="padding: 5px;">
                                    <div class="col-md-6" style="">

                                        <div class="form-group required">
                                            <?= lang("Segment", "Segment"); ?>                             
                                            <?php 
                                                                               
                                           // $wh[''] = 'Please Select Warehouse';
                                            foreach ($segmentList as $value) {
                                                $wh[$value->id] = $value->proj_name;
                                            }
                                                
                                            echo form_dropdown('segment_id', $wh, (isset($data['segment_id']) ? $data['segment_id'] : $wh['']), 'id="segment_id" class="form-control input-tip required" readonly="readonly" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("segment") . '"   style="width:50%;" ');
                                            ?>
                                        </div>
                                    </div> 
                        </div>
                
                        
                    </div>
                </div>

            </div>           

        <div class="col-md-12">
            <div class="from-group">
                <button type="submit" class="btn btn-primary" id="save-sale"><?= lang('submit'); ?></button>
               <button type="button" class="btn" onclick="location.href = '<?php echo base_url()?>';">
                    <?= lang('Cancel') ?></button>
            </div>
        </div>
    </div>
       
            
    </div>

        </div>
    </div>
</div>
<?php echo form_close(); ?>
</body>
</html>
