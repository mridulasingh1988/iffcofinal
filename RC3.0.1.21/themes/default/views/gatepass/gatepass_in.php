<script type="text/javascript">
 jQuery(document).ready(function() {
    $('#sto_no').prop('disabled', true);
    function getSupplierSto(sup_name, wh_name, gp_type){

            if(gp_type != 0 && wh_name != 0){
                 $.ajax({
                    url: site.base_url+"gatepass/getGPType",
                    async: false,
                    type: "GET",
                    data: {"gp_type_id":gp_type,"wh_name":wh_name,"sup_name":sup_name},
                    dataType:"json",
                    success: function(data) {
                        if(data != 0){
                            var opt = '';
                            $('#sto_no').prop('disabled', false);
                            
                            $.each(data,function(key,val){
                                opt = $('<option />'); 
                                opt.val(val['doc_id']+'-'+val['doc_no']);
                                opt.attr('attri',key);
                                opt.text(val['doc_no']);
                                $('#sto_no').append(opt);
                            });
                        }
                        else{
                            $('#sto_no').html('');
                            $('#sto_no').prop('disabled', true);
                            bootbox.alert("No Invoice available for this gatepass!");
                            return false;
                        }
                    }
                });
            }
            else{

                if(gp_type == ''){
                    $('#sto_no').prop('disabled', true);
                    alert("Please select gatepass type");
                    $("#gatepass_type").focus();
                    return false;
                }

                else if(wh_name == ""){
                    $('#sto_no').prop('disabled', true);
                    alert("Please select warehouse");
                    $("#warehouselist").focus();
                    return false;
                }

                else if(wh_name == "" || gp_type == ''){
                    $('#sto_no').prop('disabled', true);
                    alert("Please select gatepass type and warehouse!");
                    $("#gatepass_type").focus();
                    $("#warehouselist").focus();
                    return false;
                }

                return false;
            }
    }

    function getSupplierWH(gp_type){
        $("#warehouselist").select2('val', 'All');
        $("#sto_no").select2('val', 'All');
        $("#sto_date").val('');
        $.ajax({   
             url: site.base_url+"gatepass/ajax_call_warehousesbysegment", 
             async: false,
             type: "POST", 
             data: {"gp_type_id":gp_type},
             dataType: "json", 
             success: function(data) {
                var $option = $("<option selected></option>").val('').text("Select warehouse"); 
                $('#warehouselist').append($option).trigger('change'); 

                $('#warehouselist').html(data);                
            }
        });
    }

    if($('#supplier_details').val() != ''){
        var sup_name = $('#supplier_details').val();
        var wh_name = $("#warehouselist").val();
        var gp_type = $("#gatepass_type").val();
        getSupplierSto(sup_name, wh_name, gp_type);
    }

    if($('#gatepass_type').val() != ''){
        var gp_type = $('#gatepass_type').val();
        getSupplierWH(gp_type);
    }

    $('#gatepass_type').change(function () {
        var gp_type = $(this).val();
        getSupplierWH(gp_type);
    });

    $('#supplier_details').change(function () {
        var sup_name = $(this).val();
        var wh_name = $("#warehouselist").val();
        var gp_type = $("#gatepass_type").val();

        getSupplierSto(sup_name, wh_name, gp_type);
    });
     
     //added by vikas singh for NaN validation 
     /*$( document ).on( 'blur', '.assgin_qty', function() {
         
         var parent_obj = $(this).closest('tr').attr('id');
         var actual_qty1= parseFloat($("#"+parent_obj).find('.actual_qty').val());
         if(isNaN(actual_qty1) || actual_qty1 == '0' )
         {
            input_val = $(this).val('');
            parseFloat($("#"+parent_obj).find('.short_qty').val(''));
            var req_qty1 = parseFloat($("#"+parent_obj).find('.bill_qty').val());
            var rate1 = parseFloat($("#"+parent_obj).find('.rate').val());
            var amount1 = (rate1*req_qty1).toFixed(2);
             $("#"+parent_obj).find('.amount').val(amount1);
         }
     });*/


    $( document ).on( 'change', '.show-decimal', function() {
        var input_val = $(this).val();
        if(!isNaN(input_val)){
            $(this).val(parseFloat(input_val).toFixed(2));
        }       

        var parent_obj = $(this).closest('tr').attr('id');
        var bill_qty = parseFloat($("#"+parent_obj).find('.bill_qty').val());
        var actual_qty = parseFloat($("#"+parent_obj).find('.actual_qty').val());
        var short_qty = parseFloat($("#"+parent_obj).find('.short_qty').val());
        var rate = parseFloat($("#"+parent_obj).find('.rate').val());
        //var bill_qty = $("#"+parent_obj).find('.bill_qty').val();
        if((bill_qty != '') && (actual_qty!='') && (!isNaN(bill_qty))  && (!isNaN(actual_qty))){
            var short_qty = ((actual_qty)-bill_qty).toFixed(2);
            var amount = (rate*actual_qty).toFixed(2);
        }
        $("#"+parent_obj).find('.short_qty').val(short_qty);
        $("#"+parent_obj).find('.amount').val(amount);
    });

    $('#save-sale').on('click', function(event){

        if($('#rcpt_dt').val() < $('#sto_date').val() ){
            alert('Receipt date should be greater than or equal from Invoice date');
            $('#rcpt_dt').focus();   
            return false;          
        } else {
        $("#submit-type").val(1);
        var $frm =  $("#gatepass-form");
        event.preventDefault();
        $frm.validate();
        }
    });
    

    $('#hold-sale').on('click', function(event){
        $("#submit-type").val(2);
        var $frm =  $("#gatepass-form");
        event.preventDefault();
        $frm.validate();
    });

    $( document ).on( 'click', '.delete', function() {
        var lot_no_detail = $(this).attr('data-del');
        var obj = $(this);
        var del_ajax = obj.attr("data-exist");
        
        var table_length = $('table[data-id="lot_bin_'+lot_no_detail+'"] tr').length;

        if(table_length <= 1){
            var lot_id = $(this).attr('at').split("_");
            var x = confirm("Are you sure you want to delete this lot?");
            if(x){
                //if(del_ajax == 1){
                    $.ajax({
                        url: site.base_url+"gatepass/getLotProductsById",
                        async: false,
                        type: "POST",
                        data: {"lot_id":lot_id[2]},
                        dataType:"json",
                        success: function(data) {
                            //console.log(data);
                            if(data.response==1){
                                obj.closest("tr").remove();
                                if(table_length==1){
                                    $('table[data-id="lot_bin_'+lot_no_detail+'"]').remove();
                                }     
                            }else{
                                //alert("Failed to delete lot. Please try again!!");
                                return false;
                            }
                        }
                    });
                /*}else{
                    obj.closest("tr").remove();
                    if(table_length==1){
                        $('table[data-id="lot_bin_'+lot_no_detail+'"]').remove();
                    }
                }*/
            }
            else{
                return false;
            }
            $(this).closest("tr").remove();
        }
        else{
            alert("Please delete all bins to which this lot is allocated");
            return false;
        }
    });

    $( document ).on( 'click', '.deletebin', function() {
        //console.log($(this).attr('id'));return false;
        var binid = $(this).attr('id');
        var obj = $(this);
        //console.log(bin_id[2]); return false;
            var x = confirm("Are you sure you want to delete this bin?");
            if(x)
            {
                if(binid)
                {
                    var bin_id = binid.split("_");
                    $.ajax({
                        url: site.base_url+"gatepass/getBinProductsById",
                        async: false,
                        type: "POST",
                        data: {"bin_id":bin_id[2]},
                        dataType:"json",
                        success: function(data) {
                            if(data.response==1){
                                obj.closest("tr").remove();
                            }
                            else{
                                alert("Incorrect Format!!!");
                            }
                           
                        }
                    });
                }
                else{
                    obj.closest("tr").remove();
                }
            }
            else{
                alert("No record deleted!!");
                return false;
            }
        });

    $( document ).on( 'click', '.delete-sto', function() {
        if(confirm("Are you sure you want to delete this record?")){
            var split_ids = $(this).attr('id').split("_");
            var sto_no = split_ids[0];
            var item_id = split_ids[1]; 
            var gp_id = split_ids[2]; 
            var obj = $(this);
            if(sto_id){
                $.ajax({
                    url: site.base_url+"gatepass/deletesto",
                    async: false,
                    type: "POST",
                    data: {"sto_no":sto_no,"item_id":item_id,"gp_id":gp_id},
                    dataType:"json",
                    success: function(data) {
                        if(data.response==1){
                            obj.closest(".po_ro_tab").remove();  
                        }else{
                            alert("failed to delete gatepass. Please try again!!");
                            return false;
                        }
                    }
                })
            }
        }
    });

    $( document ).on( 'click', '.accordian-div', function() {
        $('.po_ro_tab').find('.box-content-div').slideUp('slow');
        $(this).closest('.po_ro_tab').find('.box-content-div').toggle('slow');  
    });

    var today = new Date();
    var lastDate = new Date(today.getFullYear(), today.getMonth(0), 31);
    var startDate = new Date(today.getFullYear(), 0, 1);

    //var lastDate = new Date(today.getFullYear(), today.getMonth(0), 31);
    jQuery("#lr_date").datetimepicker({
        autoclose:true,
        showSecond: false,
        minView: 2,
        format: 'yyyy-mm-dd',
        startDate: startDate,
        endDate: today
    });

    jQuery("#rcpt_dt").datetimepicker({
        endDate: today,
        changeMonth: true,
        autoclose:true,
        showSecond: false,
        minView: 2,
        format: 'yyyy-mm-dd',
        });

    jQuery("#edit_mfg_date").datetimepicker({
        changeMonth: true,
        autoclose:true,
        showSecond: false,
        minView: 2,
        format: 'yyyy-mm-dd',
        startDate: 'today',
        endDate: new Date()
    });

    jQuery("#edit_exp_date").datetimepicker({
        changeMonth: true,
        autoclose:true,
        showSecond: false,
        minView: 2,
        format: 'yyyy-mm-dd',
        startDate: 'today'
        //endDate: new Date()
    });
        
    $( document ).on( 'change', '#edit_exp_date', function() {
        var mfg = $('#edit_mfg_date').val();
        var exp = $("#edit_exp_date").val();
        //console.log(mfg);
        //console.log(exp);

        if(mfg != '' && exp != ''){
            if(mfg > exp){
                alert("Please enter valid date");
                $(this).focus();
                $(this).val('');
                //$("#edit_mfg_date").focus();
                return false;
            }
            else{
                return true;
            }
        }
    });
    
    $('#po_ro_tab_div').on('click','.remove',function() {
        $(this).closest('div.po_ro_tab').remove();
        var div_count = $("div.po_ro_tab").length;
        if(div_count==0)
         $("#supplier_details").prop( "disabled", false );
         $("#warehouselist").prop( "disabled", false ); 
         $("#gatepass_type").prop( "disabled", false );  
    });

    
    $( document ).on( 'click', '#genlot', function() {
        var no = generateLotNo(6);
        $("#lot_no").val(no);
        return false;
    });
    $( document ).on( 'click', '#cancelButton', function() {
        var parentId = $(this).closest('.modal').attr('id');
        $("#"+parentId).modal('hide');
    }); 
    
    $( document ).on( 'click', '.editLot', function() {
        var mylist = [];
        var obj = $(this).closest("div#lotModal");
           var flg = 0;

            if($("#lot_no").val()==''){
                alert("Please enter Lot No.");
                flg++;
                return false;
            }            

            if($('#edit_mfg_date').val() > $("#edit_exp_date").val()){
                alert('Expiry date should be greater than or equal from Manufacturing date');
                $(this).focus();
                $(this).val('');
                return false;
            }
            if(($('#edit_mfg_date').val() === '') && ($("#edit_exp_date").val() != '')){
                    alert("Please enter Manufacturing date");
                     $(this).focus();
                     $(this).val('');
                     return false;
            }

            var edit_lot_qty = parseFloat($("#edit_lot_qty").val());
            var lot_remaining_qty = parseFloat($("#lot_remaining_qty").val());

            if(edit_lot_qty == ''){
                alert("Please enter lot qty");
                flg++;
                return false;
            }

            if(edit_lot_qty > lot_remaining_qty){
                alert("Lot qty should be less than or equal to actual qty");
                flg++;
                return false;
            }

            if(edit_lot_qty == 0){
                flg++;
                $("#lotModal").modal('hide');
                return false;
            }

            var item_id = $("#gp_item_id").val();
            //console.log("Item Id ------- "+item_id);
            
            var item_qty = $("#lot_total_qty").val();
            var lot_qty = $("#edit_lot_qty").val();
            var rem_qty = item_qty-lot_qty;
            $("#lot_remaining_qty").val(rem_qty);//return false;
            
            
            if(flg==0){
                var row = {};
                row["lot_no"] = $("#lot_no").val();
                //console.log("Lot no. on edit---- "+row["lot_no"]);
                row["lot_qty"] = $("#edit_lot_qty").val();
                row["batch_no"] = $("#edit_batch_no").val();
                row['mfg_date'] = $("#edit_mfg_date").val();
                row['exp_date'] = $("#edit_exp_date").val();
                //row['total_lot_qty'] = $("#lot_remaining_qty").val();
                mylist.push(row);

                var lot_obj = decodeURIComponent(JSON.stringify(mylist));       
                var input = document.createElement("input");
                input.type = "hidden";
                input.name = 'lot_data['+item_id+'][]';
                input.value = lot_obj;
                $("#lotModal").modal('hide');
                
                var table_id = 'lot_'+item_id;

                if ( $( "#"+table_id ).length ) {
                   
                    var tr = $('<tr class="background" data-id="lot_'+row['lot_no']+'_'+item_id+'"></tr>');

                    var div_td_html= '<td><input type="hidden" class="lot-class" name="lot_qty['+item_id+'][]" value="'+row['lot_qty']+'" />'+row["lot_no"];
                    tr.append(input);   
                    div_td_html+='</td>';
                    div_td_html+= '<td>'+row['lot_qty']+'</td>';
                    div_td_html+= '<td><a href="javascript:void(0)" id="bin['+item_id+'][]" data-id="'+row['lot_no']+'_'+item_id+'_'+row['lot_qty']+'" class="allocate_bin">Allocate Bin</a> <a href="javascript:void(0)" id="bin['+item_id+'][]" data-del="'+row['lot_no']+'_'+item_id+'" at="lot_id_'+item_id+'" class="delete">Delete</a></td>';
                    tr.append(div_td_html);

                    $(".top_lot_div").hide();
                    $('#binModal').modal('hide');
                    $("#"+table_id ).append(tr);
                    $(".bin_table").hide();
                    $('table[data-id="lot_bin_'+row['lot_no']+'_'+item_id+'"]').show();
                    $(".lot_bin_div_"+item_id).show();
                    $( "#"+table_id ).show();

                }
                else
                {
                  
                    var div_html = '';
                  var tr = '';
                    var pTag = $("");
                    
                    var table = $('<table class="table table-bordered table-condensed  lot_bin_table" id="lot_'+item_id+'" style="margin-bottom:0;"></table>');
                    div_html+= '<thead>';
                    div_html+= '<tr style="cursor: pointer;">';
                    div_html+= '<th><?= lang("lot_name"); ?></th>';
                    div_html+= '<th><?= lang("qty"); ?></th>';
                    div_html+= '<th><?= lang("action"); ?></th>';
                    div_html+= '</tr>';
                    div_html+= '</thead>';
                    //div_html+= '<tbody>';
                    tr = $('<tr class="background" data-id="lot_'+row['lot_no']+'_'+item_id+'"></tr>');
                    var div_td_html= '<td><input type="text" name="lot_qty['+item_id+'][]" value="'+row['lot_qty']+'" />'+$("#bin_lot_no").val();
                    tr.append(input);     
                    div_td_html+='</td>';
                    div_td_html+= '<td>'+row['lot_qty']+'</td>';
                    div_td_html+= '<td><a href="javascript:void(0)" data-id="'+row['lot_no']+'_'+item_id+'_'+row['lot_qty']+'" class="allocate_bin">Allocate Bin</a> <a href="javascript:void(0)" id="bin['+item_id+'][]" data-del="'+row['lot_no']+'_'+item_id+'" at="lot_id_'+item_id+'" class="delete">Delete</a></td>';
                    //console.log(tr);
                    tr.append(div_td_html);
                    table.append(div_html);
                    table.append(tr);
                    //console.log(table);
                    //console.log("id----------->",item_id);
                    $(".lot_"+item_id).append(table);
                }
                obj.find('input:text').val(''); 

            }
    });

    $( document ).on( 'click', '.bin_background', function() {
        var slot_id = $(this).attr("id").split("_"); 
        //console.log(slot_id[1]);
        $(".bin_background").css("background-color","#FFFFFF");
        $(this).css("background-color","#DDDDDD");
        var table_length = $("#lot_"+slot_id[1]+" > tbody").children().length;
        //console.log(table_length);//return false;
        $(this).closest('#stock_details').find(".top_lot_div").hide();
        if(table_length>0){
            console.log(slot_id[1]);
            //$(".lot_bin_table").hide();
            $(".bin_"+slot_id[1]+" > table").show();
            $(".lot_bin_div_"+slot_id[1]).show();
            $(".lot_bin_div_"+slot_id[1]).find("#lot_"+slot_id[1]+" tbody").children("tr").removeClass("background");
            $(".lot_bin_div_"+slot_id[1]).find("#lot_"+slot_id[1]+" tbody").children("tr").addClass("white-background");
            $(".lot_bin_div_"+slot_id[1]).find("#lot_"+slot_id[1]+" tbody").children("tr:first").addClass("background");
            $(".lot_bin_div_"+slot_id[1]).find("#lot_"+slot_id[1]+" tbody").children("tr:first").removeClass("white-background");
            $(".lot_bin_div_"+slot_id[1]).find(".bin_div").hide();
            $(".lot_bin_div_"+slot_id[1]).find(".bin_table").hide();
            $(".lot_bin_div_"+slot_id[1]).find(".bin_"+slot_id[1]).show();
            $(".lot_bin_div_"+slot_id[1]).find(".bin_div").find("table:first").show();
            //$(".lot_"+slot_id[1]).show();

            $("#lot_bin_"+slot_id[1]).show();
        }

    });

    $( document ).on( 'click', '.lot_bin_table tr', function() {
        var obj = $(this).closest('div.po_ro_tab');
        var lot_detail = $(this).attr('data-id').split("_");
        //console.log(lot_detail);
        $(this).siblings().removeClass('background');
        $(this).siblings().addClass('white-background');
        $(this).addClass('background');
        $(this).removeClass('white-background');
        $(this).siblings().removeClass('white-background');
        var table_id = 'lot_bin_'+lot_detail[1]+"_"+lot_detail[2];
        obj.find(".bin_table").hide();
        obj.find(".bin_"+lot_detail[2]).show();
        $('table[data-id='+table_id+']').show();
    });
    
    $( document ).on( 'click', '.editBin', function() {
        var bin_list = [];
        var obj = $(this).closest("div#binModal");
        var flg = 0;   
        var item_id = $("#bin_item_id").val();

        var bin_qty = parseFloat($("#bin_qty").val());
        var remain_qty = parseFloat($("#bin_remaining_qty").val());           
        if(bin_qty>remain_qty){             
            alert("Item quantity cannot exceed remaining item quantity");
            $("#bin_qty").val(remain_qty);
            return false;
        }
        if(bin_qty == 0){
            $('#binModal').modal('hide');
            return false;
        }
        var row = {};
        row['bin_lot_no'] = $("#bin_lot_no").val();
        row['bin_qty'] = $("#bin_qty").val();
        row['bin_id'] = $("#bin_id").val();
        bin_list.push(row);

        var bin_name = $("#bin_id option:selected").text();
        //console.log("bin_name -------- "+bin_name);

        var bin_obj = decodeURIComponent(JSON.stringify(bin_list));       
        var input = document.createElement("input");
        input.type = "hidden";
        input.name = 'bin_data['+row['bin_lot_no']+'][]';
        input.value = bin_obj;

        var table_id = 'lot_bin_'+row['bin_lot_no']+'_'+item_id;
        if ( $('table[data-id='+table_id+']').length ) {                
            var tr = $('<tr class="background" style="cursor: pointer;"></tr>');

            var div_td_html= '<td><input type="hidden" class="" name="bin_qty['+row['bin_lot_no']+'][]" value="'+row['bin_qty']+'" />'+bin_name;
            tr.append(input);   

            div_td_html+='</td>';
            div_td_html+= '<td>'+row['bin_qty']+'</td>';
            div_td_html+= '<td><a href="javascript:void(0)" class="deletebin">Delete</a></td>';
            //div_td_html+= '<td><a href="javascript:void(0)" data-id="'+row['bin_lot_no']+'_'+item_id+'_'+row['bin_qty']+'" class="allocate_bin">Edit</a></td>';
            //div_td_html+= '<td><a href="javascript:void(0)" data-id="'+row['bin_lot_no']+'_'+item_id+'_'+row['bin_qty']+'" class="allocate_bin">Delete</a></td>';
            
            tr.append(div_td_html);
            
            $('#binModal').modal('hide');
            $('table[data-id='+table_id+']').show();
            $('table[data-id='+table_id+']').append(tr);
        }else{
            var pTag = $("");
            
            var tables = $('<table class="table table-bordered table-condensed bin_table" data-id="lot_bin_'+row['bin_lot_no']+'_'+item_id+'" style="margin-bottom:0;"></table>');
            var div_html= '<thead>';
            div_html+= '<tr>';
            div_html+= '<th class="col-sm-6"><?= lang("bin_name"); ?></th>';
            div_html+= '<th class="col-sm-6"><?= lang("qty"); ?></th>';
            div_html+= '<th colspan="2"><?= lang("action"); ?></th>';
            div_html+= '</tr>';
            div_html+= '</thead>';
            
            var tr = $('<tr class="background" style="cursor: pointer;"></tr>');

            var div_td_html= '<td><input type="hidden" name="bin_qty['+row['bin_lot_no']+'][]" value="'+row['bin_qty']+'" />'+bin_name;
            tr.append(input);   

            div_td_html+='</td>';
            div_td_html+= '<td>'+row['bin_qty']+'</td>';
            div_td_html+= '<td><a href="javascript:void(0)" class="deletebin">Delete</a></td>';
            //div_td_html+= '<td><a href="javascript:void(0)" data-id="'+row['bin_lot_no']+'_'+item_id+'_'+row['bin_qty']+'" class="allocate_bin">Edit</a></td>';
            //div_td_html+= '<td><a href="javascript:void(0)" data-id="'+row['bin_lot_no']+'_'+item_id+'_'+row['bin_qty']+'" class="allocate_bin">Delete</a></td>';

            tr.append(div_td_html);
            
            tables.append(div_html);
            tables.append(tr);

            $(".bin_"+item_id).append(tables);

        }
        $('#binModal').modal('hide');
        $('table[data-id='+table_id+']').siblings().hide();
        $('table[data-id='+table_id+']').show()

        obj.find('input:text').val('');
    });

    $( document ).on( 'click', '.allocate_lot', function() { 
        
        $(this).closest("div.po_ro_tab").find("table tr").css("background-color","#FFFFFF");
        $(this).closest("tr").css("background-color","#dddddd");
        var obj = $(this).closest('.po_ro_tab').attr('id');

        $("#lotModalLabel").html("Add/Edit Lot");
        var id = $(this).attr('data-id').split("_");
        //console.log(id);
        
        var item_qty = $("#actual_qty_"+id[1]).val();
        var total_qty = 0;

        
        if(parseInt(item_qty)>0){
            var item_qty = $("#actual_qty_"+id[1]).val();
            var actual_qty = $("#actual_qty_"+id[1]).val();
            $("#gp_item_id").val(id[1]);
                
            $('input[name="lot_qty['+id[1]+'][]"]').each(function(){
                total_qty = parseFloat(total_qty)+parseFloat($(this).val());
            });

            $('#lotModal').modal({backdrop: 'static', keyboard: false});

            var rest_qty = parseFloat(item_qty) - total_qty;
            //console.log("rest_qty **** ---- *** "+rest_qty);
            $("#edit_lot_qty").val(rest_qty);
            $("#lot_remaining_qty").val(rest_qty);

            $('#lotModal').css('zIndex', '1050');
        }
        else{
            $('#lotModal').hide();
           // alert("Item Qty is less than 1.00 !!");
            return false;
        }

    });
    
    $( document ).on( 'click', '.allocate_bin', function() { 

        $("#binModalLabel").html("Add/Edit Bin");
        var id = $(this).attr('data-id').split("_");
        
        $("#bin_item_id").val(id[1]);
        $("#binModalLabel").html("Add/Edit Bin");
        
        $("#bin_lot_no").val(id[0]);
        var total_qty = 0;
        var lot_qty = id[2];
       // console.log(id);
        $('input[name="bin_qty['+id[0]+'][]"]').each(function(){
            total_qty = parseFloat(total_qty)+parseFloat($(this).val());
        });
        
        var rest_qty = parseFloat(lot_qty) - total_qty;
        
        $("#edit_lot_qty").val(rest_qty)   
        $("#bin_remaining_qty").val(rest_qty) 
        $("#bin_qty").val(rest_qty);

        $('#binModal').modal({backdrop: 'static', keyboard: false});
        //$('#binModal').modal({backdrop: 'static', keyboard: false});
    });

    $('#sto_no').change(function () {
        $("#sto_date").val('');
        
        var sto_val = $(this).val();
        var sto_n = sto_val.split('-');
        var sto_no = sto_n[0];
       
        if(sto_no!=0){
             $.ajax({
                url: site.base_url+"gatepass/getStoDetails",
                async: false,
                type: "POST",
                data: {"sto_id":sto_no},
                dataType:"json",
                success: function(data) {
                    //console.log(data);
                    $.each(data,function(key,val){ 
                       $("#sto_date").val(val.doc_date);
                    });
                    
                }
            })
        }
    });

    $('#add_sto_details').click(function () { 

            //console.log("here"); 
            //add_product_tab('',111,'2016-11-08');
            var sto_length = $("#po_ro_tab_div").length;
            var sto_no = $("#sto_no").val();
            //alert($("#sto_no").val());
            //alert( $('#sto_no').find('option:selected').text());
            var sto_text = $("#sto_no option:selected").text();
                      
            var sto_date = $("#sto_date").val();
            var sup_id = $("#supplier_details").val();
            //alert(sup_id);
            //alert(sto_length);//return false;
            var sto_flag = 0;
            if(sup_id==''){
                alert("Please Select Supplier");
                return false;
           }

           if($('#rcpt_dt').val() < $('#sto_date').val()){
                alert('Receipt date should be greater than or equal from Invoice date');
                $('#rcpt_dt').focus();   
                return false;          
            }
            //alert(sto_no);
            if(sto_length>=1){
                $.each($('.sto_list'), function(index, attr) {
                    var sto = $(this).attr('vals');   
                    
                    if(sto===sto_no){
                        sto_flag++;
                    }                   
                });
            }

        if(sto_flag==0){
                $.ajax({
                    url: site.base_url+"gatepass/getStoProductsList",
                    async: false,
                    type: "POST",
                    data: {"sto_no":sto_no,"sto_date":sto_date,"sto_text":sto_text},
                    dataType:"json",
                    success: function(data) {

                        if(data && data.length>0){
                        add_product_tab(data,sto_no,sto_date);  
                        $("#supplier_details").prop( "disabled", true );
                        $("#warehouselist").prop("disabled", true);
                        $("#gatepass_type").prop( "disabled", true );  
                       }else{
                        $("#supplier_details").prop( "disabled", false );
                        $("#warehouselist").prop("disabled", false);
                        $("#gatepass_type").prop( "disabled", false ); 
                        alert("Please Select Invoice No.")
                         return false;
                       }                                      
                    }
                })
        }else{
            alert("This Invoice No. is already selected.")
            return false;

        }            

    });
         

    $("#gatepass-form").validate({
        ignore: [],
        rules: {
            gatepass_type : {
                required : true
            },
            warehouse_name : {
                required : true
            },
            other_charges: {
                required : true,
                decimalchecktest:true,
                checkChargeVal : true 
            },
            supplier_id:{
                required : true               
            },
            lr_name:{
                required : true               
            },
            rcpt_date:{
                required : true
            },
        },
        messages: {
            gatepass_type : {
                required : "Please select a Gatepass type"
            },
            warehouse_name:{
                required : "Please select a Warehouse"
            },
            other_charges : {
                required : "Please enter other charges",
                decimalchecktest: 'Please enter valid format',
                checkChargeVal : "Please enter other charges"
            },
            supplier_id:{
                required : "Please select a Supplier"
            },
            lr_name:{
                required : "Please select a Transporter name"
            },
            // sto_no:{
            //     required : "Please select a Invoice no."
            // },
            rcpt_date:{
                required : "Please enter the receipt date"
            }

        },

        errorElement: "em", 
        submitHandler:function(form){
            var error = [];
            var rec_count = 0;
            $('.actual_qty').each(function(i,obj){
                if($(this).val()!=0){
                    rec_count++;
                }
            });

            if(rec_count==0){
                alert("Please enter details of atleast of one item");
                return false;
            }
            var error = [];
            //$('.po_ro_tab').each(function(x, obj1) {
                var act = $(this).find('.actual_qty');
                $('.actual_qty').each(function(i, obj) {
                    var ro_id = $(this).attr('id').split("_");
                    var receive_qty = parseInt($(this).val()); 
                    
                    if(receive_qty!=0){
                        var total_qty = 0;  
                        $('input[name="lot_qty['+ro_id[2]+'][]"]').each(function(){
                            total_qty = parseFloat(total_qty)+parseFloat($(this).val());
                        });
                        if(total_qty!=receive_qty){
                            error.push(ro_id[2]);
                            alert("Please enter correct lot details to all items received! ");

                            $(".bin_background").addClass('white-background');
                            $("#slot_"+ro_id[2]).removeClass('white-background');        
                            $("#slot_"+ro_id[2]).addClass('background');
                            $("#slot_"+ro_id[2]).addClass('add-border');
                            $("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('.box-content-div').show();
                            $("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('#item_details').hide();
                            $("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('#stock_details').show();
                            $('.actual_qty_'+ro_id[2]).text(receive_qty);

                            $("#slot_"+ro_id[2]).tooltip({'show': true,
                                'placement': 'bottom',
                                'title': "Please allot Lot to the received items"
                            });
                            $("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('.stock_details_tab').addClass('active');
                            $("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('.item_details_tab').removeClass('active');
                            return false;
                        }
                        else
                        {
                            jQuery.grep(error, function(value) {
                                return value != ro_id[2];
                            });

                            $("#slot_"+ro_id[2]).tooltip({
                                'show': true,
                                'placement': 'bottom',
                                'title': "Lot assigned successfully"
                            });

                            $("#slot_"+ro_id[2]).closest('#stock_details').find(".top_lot_div").hide();
                            $("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('.box-content-div').hide();
                            $("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('#item_details').show();
                            $("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('#stock_details').hide();
                            $("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('.stock_details_tab').removeClass('active');
                            $("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('.item_details_tab').addClass('active');
                            $("#slot_"+ro_id[2]).removeClass('add-border');
                        }
                    }
                    //i++;
                });
            //});
            
            if(error.length>0){
                //alert("Err len ------------ "+error.length);
                return false;
            }

            var bin_error = [];
            $('.lot-class').each(function(i, obj) {
                var lot_qty = parseInt($(this).val());
                //console.log("Lot ----- "+lot_qty);
                var lot_id = $(this).closest('tr').attr('data-id');
                var lot_no = $(this).closest('tr').attr('data-id').split("_");
                //console.log(lot_no);
                
                if(lot_qty!=0){

                    var total_bin_qty = 0;
                    $('input[name="bin_qty['+lot_no[1]+'][]"]').each(function(){
                        //console.log("bin_qty -------- "+$(this).value);
                        total_bin_qty = parseFloat(total_bin_qty)+parseFloat($(this).val());
                    });
                    //console.log("Total ---- "+total_bin_qty);
                    if(total_bin_qty<lot_qty){
                        bin_error.push(lot_no[1]);
                        $(this).closest('.stock_data_div').find(".top_lot_div").hide();
                        $(".bin_background").addClass('white-background');
                        $("#slot_"+lot_no[2]).removeClass('white-background');        
                        $("#slot_"+lot_no[2]).addClass('background');

                        $(".lot_bin_div_"+lot_no[2]).show();
                        $('tr[data-id='+lot_id+']').addClass('add-border');
                        $('tr[data-id='+lot_id+']').closest('div.po_ro_tab').find('.box-content-div').show();
                        $('tr[data-id='+lot_id+']').closest('div.po_ro_tab').find('#item_details').hide();
                        $('tr[data-id='+lot_id+']').closest('div.po_ro_tab').find('#stock_details').show();
                        $('tr[data-id='+lot_id+']').tooltip({'show': true,
                            'placement': 'bottom',
                            'title': "Please allot Bin to the received items"
                        });
                        $('tr[data-id='+lot_id+']').closest('div.po_ro_tab').find('.stock_details_tab').addClass('active');
                        $('tr[data-id='+lot_id+']').closest('div.po_ro_tab').find('.item_details_tab').removeClass('active');
                        alert("Please enter bin details to all lots!");
                        return false;
                    }else{
                       jQuery.grep(bin_error, function(value) {
                        return value !=lot_no[1] ;
                      });
                       
                       $('tr[data-id='+lot_id+']').tooltip({'show': true,
                            'placement': 'bottom',
                            'title': "Bin assigned successfully"
                        });
                       
                        $('tr[data-id='+lot_id+']').removeClass('add-border');
                        $(".lot_bin_"+lot_no[1]+"_"+lot_no[2]).hide();
                        
                        $(".lot_bin_div_"+lot_no[2]).hide();
                    }
                }
                else{
                    alert("Please enter bin details!");
                    return false;
                }
               
            });
            console.log(bin_error.length);
       
            if(bin_error.length>0){
                //alert("Please enter all bin details");
                return false;
            }else{

                $("#supplier_details").prop( "disabled", false ); 
                $("#warehouselist").prop("disabled", false);
                $("#gatepass_type").prop( "disabled", false ); 
                var submit_type = $("#submit-type").val();
                $(".show-decimal").attr("readonly", false); 

                if(submit_type==1){
                    if(confirm("Are you sure you want to save the details? Once you click OK, all the details will be saved and you will not be able to modify details")){
                        form.submit();
                    }
                    else{
                        return false;
                    }
                }else{
                    form.submit();
                    
                }
            }
        }

    });
    
    $.validator.addMethod("decimalchecktest",
        function(value, element) {
        return /^[0-9]\d*(\.\d+)?$/.test(value);
    }); 

    $( document ).on( 'click', '.item_details_tab', function() {
        var obj = $(this).closest("div.po_ro_tab");
        $("#dbTab").children('li').removeClass('active');
        $(this).addClass('active');
        obj.find("#item_details").show()
        obj.find("#stock_details").hide();

    });
    
    $( document ).on( 'click', '.stock_details_tab', function() {
   
        var obj = $(this).closest("div.po_ro_tab");
        var flg1 = 0;
        var flg2 = 0;
        obj.find("input").each(function () {
           
            /*if(this.value==''){
                alert("Please fill all fields");
                flg1++;
                return false;
            }*/
            if($(this).attr('at')=='actual_qty'){
                var id = this.id;
                //console.log("Stock id ------------ "+id);
                                            
                obj.find("."+id).text(this.value);               
            }
        });

        if(flg1==0 && flg2==0){
            
            var obj = $(this).closest("div.po_ro_tab");
            $("#dbTab").children('li').removeClass('active');
            $(this).addClass('active');
            obj.find("#item_details").hide()
            obj.find("#stock_details").show();

        }
    });

    $.validator.addMethod("checkChargeVal",
        function(value, element) {

            if(parseInt(value)>=0)
                return true;
            else 
                return false;
    });

});

function isNumberKey(e, t) {
    try {
        if (window.event) {

            var charCode = window.event.keyCode;
        }
        else if (e) {

            var charCode = e.which;

        }
        else { return true; }

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {

            return false;
        }
        return true;
    }
    catch (err) {
        alert(err.Description);
    }
} 

function add_product_tab(product_array, sto_no, sto_date){
   // console.log(product_array);
   var j =0;
    var div_html="";
    div_html+= '<div class="col-sm-12 po_ro_tab" id="popopo">';
    div_html+= '<hr></hr>';
    div_html+= '<div class="accordian-div1">';
    div_html+= '<div class="col-sm-12 text-right heading remove" style="cursor: pointer; float: right; width: 20px; line-height: 23px;">X</div>';  
    div_html+= '<div class="col-sm-6 text-left"><?= lang('item_details')?><labal> ('+product_array[j].emrs_no+'-'+product_array[0].doc_no_src_id+')</div>';
   /* div_html+= '<div class="col-sm-5 text-right">'+sto_date+'</div>';*/
    div_html+= '<div class="table-responsive"><input type="hidden" name="date[]" id="date" class="form-control" value="'+sto_date+'" />';
    div_html+= '<div class="table-responsive"><input type="hidden" name="sto_no_new[]" id="sto_no_new" class="form-control" value="'+product_array[0].doc_no_src_id+'" />';
    div_html+= '<div class="table-responsive"><input type="hidden" name="doc_id[]" id="docid" class="form-control" value="'+product_array[0].doc_id+'" />';

   /* div_html+= '<input type="hidden" name="sto_no_new['+sto_no+']"  value="'+product_array[0].doc_no_src_id+'"/>';*/

    div_html+= '</div>'; 
    
    div_html+= '<div class="row" style="margin-bottom: 15px;">';
    div_html+= '<div class="col-md-12" >';
    div_html+= '<div class="box-content" style="border:1px solid #DDDDDD;border-radius:2px;">';
    div_html+= '<div class="row">';
    div_html+= '<div class="col-md-12">';
    div_html+= '<ul id="dbTab" class="nav nav-tabs">';
    div_html+= '<li class="item_details_tab active"><a href="javascript:void(0)"><?= lang('item_details') ?></a></li>';
    div_html+= '<li class="stock_details_tab"><a href="javascript:void(0)"><?= lang('stock_details') ?></a></li>';
    div_html+= '</ul>';
    div_html+= '<div class="tab-content">';
    div_html+= '<div id="item_details" class="tab-pane fade in active">';
    div_html+= '<div class="row">';
    div_html+= '<div class="col-sm-12">';
    div_html+= '<div class="table-responsive">';
    div_html+= '<table class="table items table-striped table-bordered table-condensed table-hover">';
    div_html+= '</thead>';
    div_html+= '<thead>';
    div_html+= '<tr>';
    div_html+= '<th class="col-md-2"><?= lang("product_name"); ?></th>';
    div_html+= '<th class="col-md-1"><?= lang("Unit"); ?></th>';
    div_html+= '<th class="col-md-1"><?= lang("del_note_qty"); ?></th>'
    div_html+= '<th class="col-md-1"><?= lang("avail_qty"); ?></th>';
    div_html+= '<th class="col-md-1"><?= lang("actual_qty"); ?></th>';
    div_html+= '<th class="col-md-2"><?= lang("short_qty"); ?></th>';
    div_html+= '<th class="col-md-2"><?= lang("rate"); ?></th>';
    div_html+= '<th class="col-md-2"><?= lang("amount"); ?></th>';
    div_html+= '</tr>';
    div_html+= '</thead>';

    if(product_array.length>0){
        for(var i in product_array){
        //console.log(product_array);                   
            var dlv_qty = parseFloat(product_array[i].dlv_qty).toFixed(2);  
          //  alert(parseFloat(dlv_qty).toFixed(2))
            div_html+= '<tr id="prod_'+product_array[i].id+'">';
            div_html+= '<td width="15%">'+product_array[i].pr_name+'';
            //div_html+= '<td width="10%">'+product_array[i].itm_uom_bs+'';         
            div_html+= '<td width="10%">'+product_array[i].unit+'';
            div_html+= '<input type="hidden" name="item_list['+sto_no+'][]" class="form-control" value="'+product_array[i].id+'" />';
            div_html+= '<input type="hidden" name="wh_id['+sto_no+']"  value="'+product_array[0].wh_id+'"/>';
            /*div_html+= '<input type="hidden" name="sto_no_new['+sto_no+']"  value="'+product_array[0].doc_no_src_id+'"/>';*/

            div_html+= '<input type="hidden" name="org_id" class="form-control" value="'+product_array[i].org_id+'" />';           
            div_html+= '<input type="hidden" name="warehouse_id" class="form-control" value="'+product_array[i].warehouse_id+'" />'; 
            div_html+= '<input type="hidden" name="segment_id" class="form-control" value="'+product_array[i].segment_id+'" />'; 
            
            /*div_html+= '<input type="hidden" name="sto_list['+sto_no+'][]" class="form-control sto_list"  vals="'+sto_no+'"value="'+product_array[i].doc_no_src_id+'" />';*/


            div_html+= '<input type="hidden" name="sto_list['+product_array[i].sto_no+'][]" class="form-control sto_list" vals = "'+product_array[i].doc_id+'-'+product_array[i].doc_no_src_id+'" value="'+product_array[i].doc_id+'" />';

            div_html+= '<input type="hidden" name="sto_list123['+product_array[i].sto_no+']['+product_array[i].id+']" class="form-control sto_list123" vals = "'+product_array[i].doc_id+'" value="'+product_array[i].doc_no_src_id+'" />';

            div_html+= '<input type="hidden" name="sto_id['+product_array[i].sto_no+'][]" class="form-control sto_id_list" value="'+product_array[i].id+'" />';

            div_html+= '<input type="hidden" name="sto_date_list['+product_array[i].sto_no+'][]" class="form-control" value="'+sto_date+'" />';
            
            div_html+= '</td><td width="10%"><input type="text" name="req_qty['+product_array[i].sto_no+']['+product_array[i].id+']" id="req_qty_'+product_array[i].id+'" at="req_qty" class="form-control" value="'+parseFloat(product_array[i].req_qty).toFixed(2)+'" readonly="readonly" /></td>';

            div_html+= '</td><td width="10%"><input type="text" name="bill_qty['+product_array[i].sto_no+']['+product_array[i].id+']" id="bill_qty_'+product_array[i].id+'" at="bill_qty"  class="form-control bill_qty" value="'+parseFloat(product_array[i].avail_qty).toFixed(2)+'" readonly="readonly" /></td>';
            
            div_html+= '<td width="10%"><input type="text" name="actual_qty['+product_array[i].sto_no+']['+product_array[i].id+']" id="actual_qty_'+product_array[i].id+'" at="actual_qty" value="0.00" class="form-control show-decimal actual_qty assgin_qty" onkeypress="return isNumberKey(event,this)"/></td>';
            
            div_html+= '<td width="10%"><input type="text" name="short_qty['+product_array[i].sto_no+']['+product_array[i].id+']" id="short_qty_'+product_array[i].id+'" at="short_qty" value="0.00" class="form-control short_qty" readonly="readonly"/></td>';
            
            //div_html+= '<td width="10%"><input type="text" name="rate['+sto_no+']['+product_array[i].id+']" id="rate_'+product_array[i].id+'" class="form-control rate " readonly="readonly" value="'+parseFloat(product_array[i].itm_price_bs).toFixed(2)+'" /></td>';
            div_html+= '<td width="10%"><input type="text" name="rate['+product_array[i].sto_no+']['+product_array[i].id+']" id="rate_'+product_array[i].id+'" class="form-control rate " readonly="readonly" value="'+parseFloat(product_array[i].price).toFixed(2)+'" /></td>';
            
            // div_html+= '<td width="10%"><input type="text" name="amount['+sto_no+']['+product_array[i].id+']" class="form-control amount" readonly="readonly" value="'+parseFloat(product_array[i].itm_amt_bs).toFixed(2)+'"/></td>';
            div_html+= '<td width="10%"><input type="text" name="amount['+product_array[i].sto_no+']['+product_array[i].id+']" class="form-control amount" readonly="readonly" value="0.00" /></td>';
            //value="'+parseFloat((product_array[i].price * product_array[i].avail_qty)).toFixed(2)+'"

            //div_html+= '<input type="hidden" name="item_uom['+sto_no+']['+product_array[i].id+']"  value="'+product_array[i].itm_uom_bs+'"/>';
            div_html+= '<input type="hidden" name="item_uom['+product_array[i].sto_no+']['+product_array[i].id+']"  value="'+product_array[i].unit+'"/>';

            div_html+= '<input type="hidden" name="item_id['+product_array[i].sto_no+']['+product_array[i].id+']" value="'+product_array[i].item_id+'"/>';

            div_html+= '<input type="hidden" name="item_code['+product_array[i].sto_no+']['+product_array[i].id+']"  value="'+product_array[i].code+'"/>';
            
            div_html+= '<input type="hidden" name="code['+product_array[i].sto_no+']['+product_array[i].id+']"  value="'+product_array[i].code+'"/>';


            div_html+= '<input type="hidden" name="item_emrs['+sto_no+']['+product_array[i].id+']"  value="'+product_array[i].emrs+'"/>';
            div_html+= '</tr>';
        }
    } j++;
    div_html+= '</tbody>';
    div_html+= '</table>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    
    div_html+= '<div id="stock_details" class="tab-pane fade in">';
    div_html+= '<div class="row">';
    div_html+= '<div class="col-sm-12">';
    div_html+= '<div class="table-responsive">';
    div_html+= '<div class="col-sm-4">';
    div_html+= '<table class="table items table-striped table-bordered table-condensed table-hover">';
    div_html+= '<thead>';
    div_html+= '<tr style="cursor: pointer;">';
    div_html+= '<th class="col-md-4"><?= lang("item_name"); ?></th>';
    div_html+= '<th class="col-md-4"><?= lang("qty"); ?></th>';
    div_html+= '<th class="col-md-4"><?= lang("action"); ?></th>';
    div_html+= '</tr>';
    div_html+= '</thead>';
        div_html+= '<tbody>';
    for(var i in product_array){
        div_html+= '<tr class="bin_background" id="slot_'+product_array[i].id+'">';
        div_html+= '<td>'+product_array[i].pr_name+'</td>';
        div_html+= '<td class="actual_qty_'+product_array[i].id+'"></td>';
        div_html+= '<td><a href="javascript:void(0)" class="allocate_lot lot_'+product_array[i].id+'" data-check="gatepass_type" data-id="lot_'+product_array[i].id+'_'+product_array[i].item_id+'">Allocate Lot</a>';
        div_html+= '</tr>';
    }
    div_html+= '</tbody>';
    div_html+= '</table>';
    div_html+= '</div>';
    div_html+= '<div class="col-sm-8 stock_data_div">';
    /*for(var i in product_array){
        div_html+= '<table class="table table-bordered table-condensed lot_bin_table" id="lot_bin_'+product_array[i].id+'" style="margin-bottom:0;display:none;">';
        div_html+= '<thead>';
        div_html+= '<tr>';
        div_html+= '<th><?= lang("lot_name"); ?></th>';
        div_html+= '<th><?= lang("bin_name"); ?></th>';
        div_html+= '<th><?= lang("qty"); ?></th>';
        div_html+= '</tr>';
        div_html+= '</thead>';
        div_html+= '<tbody>';
    }*/

    for(var i in product_array){
        div_html+= '<div class="col-sm-12 lot_bin_div_'+product_array[i].id+' top_lot_div" id="lot_bin_'+product_array[i].id+'">';
            div_html+= '<div class="col-sm-6 lot_div lot_'+product_array[i].id+'">';
            div_html+= '<table class="table table-bordered table-condensed lot_bin_table" data-id="lot_bin_'+product_array[i].lot_no+'_'+product_array[i].item_id+'" id="lot_'+product_array[i].id+'" style="margin-bottom:0;display:none;">';
            div_html+= '<thead>';
            div_html+= '<tr>';
            div_html+= '<th class="col-sm-4"><?= lang("lot_name"); ?></th>'
            div_html+= '<th col-sm-4><?= lang("qty"); ?></th>';
            div_html+= '<th col-sm-4><?= lang("action"); ?></th>';
            div_html+= '</tr>';
            div_html+= '</thead>';
            div_html+= '<tbody>';
            div_html+= '</tbody>';
            div_html+= '</table>';
            div_html+= '</div>';
        //div_html+= '</div>';

        div_html+= '<div class="col-sm-6 bin_div bin_'+product_array[i].id+'"></div>';
    div_html+= '</div>';
    }
    div_html+= '</tbody>';
    div_html+= '</table>';
    div_html+= '</div>';


    /*div_html+= '<div class="col-sm-4">';
    div_html+= '<table class="table table-bordered table-condensed totals" style="margin-bottom:0;">';
    div_html+= '<thead>';
    div_html+= '<tr>';
    div_html+= '<th colspan="2"><?= lang("batch_no"); ?></th>';
    div_html+= '</tr>';
    div_html+= '</thead>';
                
    div_html+= '<tbody>';
    for(var i in product_array){ 
        div_html+= '<tr class="bin_background" id="slot_'+product_array[i].id+'">';
        div_html+= '<td>'+product_array[i].item_id+'</td>';
        div_html+= '<td><a href="javascript:void(0)" class="allocate_lot lot_'+product_array[i].id+'" data-id="lot_'+product_array[i].id+'_'+sto_no+'">Allocate Lot</a><a href="javascript:void(0)" style="display:none" class="allocate_bin bin_'+product_array[i].id+'" data-ids="bin_'+product_array[i].id+'_'+sto_no+'">Add</a></td>';
        div_html+= '</tr>';
    }
    div_html+= '</tbody>';
    div_html+= '</table>';
    div_html+= '</div>';*/
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';

    //$(".po_ro_tab").find('.box-content-div').slideUp('slow');
    $('#po_ro_tab_div').append('<div>'+div_html+'</div>');
    //$('#po_ro_tab_div').accordion("add");
}

</script>

<?php
if($this->uri->segment(3) != null){
    $gpId = $this->uri->segment(3);
}
else{
    $gpId = '';
}

$attrib = array('data-toggle' => 'validator', 'method'=>'post', 'role' => 'form','id'=>'gatepass-form');
echo form_open_multipart("gatepass/in/".$gpItemId, $attrib);
?>

<input type="hidden" name="submit-type" id="submit-type" value="" /> 
<input type="hidden" name="item-id" id="item-id" value="<?=$gpId;?>" /> 
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('gatepass_in'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?php echo lang('enter_info'); ?></p>
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-md-4">
                            <div class="form-group required">
                                <?= lang("gatepass_type", "gt"); ?>
                            
                                <?php
                                //echo "<pre>";print_r($data);
                                $gp[''] = '';
                                $gp1[''] = '';
                                $gp1[1080] = 'External Material In';
                                foreach ($gatepass_type as $val) { 
                                        $gp[$val->type_id] = ucwords($val->name);
                                    }
                                    echo form_dropdown('gatepass_type', $gp1, (isset($data['gp_type']) ? $data['gp_type'] : $_POST['gatepass_type']), 'id="gatepass_type" class = "form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("gatepass type") . '" required="required" style="width:100%;" ');
                                ?>                        

                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group required">
                                <?= lang("warehouse", "warehouse"); ?>
                                <?php
                                $gp_id = $this->uri->segment(3);
                                
                                $arrWarehouses = array();
                                $wh_name = array();

                                $arrWarehouses['']= '';
                                foreach($warehouse_name as $wh_key=>$wh_val){
                                    $warehouse_name[$wh_val['id']]= $wh_val['name'];
                                }

                                echo form_dropdown('warehouse_name', (isset($gp_id) ? $warehouse_name : $arrWarehouses), (isset($gp_id) ? $warehouse_name[0]['id'] : ''), 'id="warehouselist" class="form-control input-tip" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" style="width:100%;" ');
                                ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group required">
                                <?= lang("SUPPLIER", "SUPPLIER"); ?>
                                <?php                              
                                $sup[''] = '';
                                foreach ($supplier_details as $sups) {
                                    $sup[$sups->eo_id] = $sups->name;
                                }
                                echo form_dropdown('supplier_id', $sup, (isset($_POST['supplier_id']) ? $_POST['supplier_id'] : $data['supplier_id']), 'id="supplier_details" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("supplier_details") . '" required="required" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
                <div class="col-md-12">
                    <div class="panel panel-warning">
                        <div class="panel-heading"><?= lang('transpoter_details') ?></div>
                        <div class="panel-body" style="padding: 5px;">

                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group required">
                                    <?= lang("Name", "Name"); ?>
                                    <?php
                                    $tp[''] = '';
                                    foreach ($tp_list as $val) {
                                        $tp[$val->eo_id] = $val->name;
                                    }
                                    echo form_dropdown('lr_name', $tp, (isset($_POST['lr_name']) ? $_POST['supplier_id'] : $data['tp_id']), 'id="lr_name" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("name") . '" required="required" style="width:100%;" ');
                                    ?>
                                </div>
                            </div>                        
                            
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("lr_no", "lr_no"); ?>
                                    <?php 
                                    echo form_input('lr_no', (isset($_POST['lr_no']) ? $_POST['lr_no'] : $data['tpt_lr_no']), 'class="form-control" id="lr_no"'); ?>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("lr_date", "lr_date"); ?>
                                    <?php 
                                    echo form_input('lr_date', (isset($_POST['lr_date']) ? $_POST['lr_date'] : $data['tpt_lr_dt']), 'class="form-control input-tip" id="lr_date" readonly="readonly" placeholder="LR Date"'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("vehicle_no", "vehicle_no"); ?>
                                    <?php echo form_input('vehicle_no', (isset($_POST['vehicle_no']) ? $_POST['vehicle_no'] : $data['vehicle_no']), 'class="form-control" id="vehicle_no"'); ?>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group required">
                                    <?= lang("other_charge", "other_charge"); ?>
                                    <?php 
                                    echo form_input('other_charges', (isset($_POST['other_charges']) ? $_POST['other_charges'] : $this->sma->formatDecimal($data['addl_amt'])), 'class="form-control input-tip show-decimal" id="other_charges"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group required">
                                    <?= lang("rcpt_dt", "rcpt_dt"); ?>
                                    <?php echo form_input('rcpt_date', (isset($data['rcpt_date']) && $data['rcpt_date']!='1970-01-01' ? $data['rcpt_date'] : $rcpt_date), 'class="form-control input-tip" id="rcpt_dt" readonly="readonly" placeholder="'.lang('rcpt_dt').'"'); ?>
                                </div>
                            </div>

                            <!--<div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("remarks", "remarks"); ?>
                                    <?php echo form_input('remarks', (isset($_POST['remarks']) ? $_POST['remarks'] : $gp[0]->remarks), 'class="form-control input-tip" id="remarks"'); ?>
                                </div>
                            </div>-->


                            <div class="clearfix"></div>

                         <div class="col-md-12">
                             <div class="form-group">
                                 <?= lang("remarks", "remarks"); ?>
                                 <?php echo form_textarea('remarks', (isset($_POST['remarks']) ? $_POST['remarks'] : $data['remarks']), 'class="form-control input-tip" id="remarks"'); ?>
                             </div>
                         </div> 
                        </div>                            
                        </div>    
                    </div>
                </div>
                
                <div class="clearfix"></div>
                
                <div class="col-md-12">
                    <div class="panel panel-warning">
                        <div class="panel-heading"><?= lang('sto_details') ?></div>
                        <div class="panel-body" style="padding: 5px;">  
                            <div class="col-md-4">                            
                                <div class="form-group">
                                    <?= lang("sto_no", "sto_no"); ?>
                                    <br>
                                    <?php
                                    //echo "<pre>";print_r($gp);
                                    $sto[''] = '';                                    
                                    if(count($sto_data)>0){
                                        foreach ($sto_data as $sto_no) {
                                            $sto[$sto_no->doc_id] = $sto_no->doc_no;                                          

                                        }
                                    }                                
                                    echo form_dropdown('sto_no', $sto, (isset($_POST['sto_no']) ? $_POST['sto_no'] : ''), 'id="sto_no" class="form-control" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("sto_no") . '"  style="width:100%;" ');                  
                                    ?>
                                </div>

                            </div>                        
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("sto_date", "sto_date"); ?>
                                    <?php echo form_input('sto_date', (isset($_POST['sto_date']) ? $_POST['sto_date'] : ''), 'class="form-control input-tip" id="sto_date" readonly="readonly" placeholder="STO Date"'); ?>
                                </div>
                            </div>                        
                            <div class="col-md-4">
                                <div class="from-group">                                   
                                    <?php echo form_button('add_sto_details', $this->lang->line("add_sto_details"), 'id="add_sto_details" class="btn btn-primary" style="padding: 6px 15px; margin:29px 0 15px;"'); ?>
                                </div>
                            </div>  
    
    <div id="po_ro_tab_div">
    <div>
    
    <?php
    //echo "<pre>";print_r($sto_list_data);exit;
    if(isset($sto_list_data)){
        foreach($sto_list_data as $key=>$row){
            foreach ($row['sto_data'] as $res) {
                $lot_data = array();
                $lot_datas = array();
                $bin_data = array();
                $bin_datas = array();
                $temp_ar = array();
                
                if(!in_array($res->lot_no, $bin_data[$res->item_id])){
                
                }
                if(!in_array($res->lot_no, $lot_data[$res->item_id])){
                        $lot_data['lot_data'][$res->item_id][] = $res->lot_no;
                        $lot_datas['lot_data'][$res->item_id][] = $res;
                        $bin_data['bin_data'][$res->item_id][] = $res->lot_no;
                        $bin_datas['bin_data'][$res->item_id][$res->lot_no] = array();
                }
                $bin_datas['bin_data'][$res->item_id][$res->lot_no] = $res;
            }
    ?>
    <div class="col-sm-12 po_ro_tab" id="popopo">
        <hr></hr>
        <div class="accordian-div">
            <!--<div id="<?=$row['sto_no'] ?>_<?=$res->item_id ?>_<?=$res->id;?>" class="col-sm-12 delete-sto text-right" style="cursor: pointer; float: right; width: 20px; line-height: 23px;">
                <i class="fa fa-trash"></i>
            </div>-->

            <div class="col-sm-6 text-left"> Invoice NO: <?=$row['sto_no']; ?> </div>
            <div class="col-sm-5 text-left">Date: <?=$row['sto_date'];?></div>
        </div>

        <div class="row" style="margin-bottom: 15px;">
            <div class="col-md-12" >
                <div class="box-content box-content-div" style="border:1px solid #DDDDDD;border-radius:2px;">
                        <div class="row inner-content">
                            <div class="col-md-12">
                                <ul id="dbTab" class="nav nav-tabs">
                                    <li class="item_details_tab active"><a href="javascript:void(0)"><?= lang('item_details') ?></a></li>
                                    <li class="stock_details_tab"><a href="javascript:void(0)"><?= lang('stock_details') ?></a></li>
                                </ul>

                                <div class="tab-content">
                                    <div id="item_details" class="tab-pane fade in active">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="table-responsive">
                                                    <table class="table items table-striped table-bordered table-condensed table-hover" style="margin-bottom:0;">
                                                        <thead>
                                                            <tr>
                                                            <th class="col-md-2"><?= lang("product_name"); ?></th>
                                                            <th class="col-md-1"><?= lang("unit"); ?></th>
                                                            <th class="col-md-1"><?= lang("del_note_qty"); ?></th>
                                                            <th class="col-md-1"><?= lang("avail_qty"); ?></th>
                                                            <th class="col-md-1"><?= lang("actual_qty"); ?></th>
                                                            <th class="col-md-2"><?= lang("short_qty"); ?></th>
                                                            <th class="col-md-2"><?= lang("rate"); ?></th>
                                                            <th class="col-md-2"><?= lang("amount"); ?></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        foreach($row['sto_data'] as $sto_data){
                                                        ?>
                                                        <tr id="prod_<?=$sto_data->gpItemId?>">
                                                        <input class="form-control" name="org_id" value="<?=$sto_data->org_id?>" type="hidden">
                                                        <input class="form-control" name="warehouse_id" value="<?=$sto_data->warehouse_id?>" type="hidden">
                                                        <input class="form-control" name="segment_id" value="<?=$sto_data->segment_id?>" type="hidden">
                                                        <input class="form-control" name="gp_item_list[<?=$sto_data->doc_no?>][]" value="<?=$sto_data->gpItemId?>" type="hidden">
                                                        <input class="form-control" name="gp_id_list[<?=$sto_data->doc_no?>][]" value="<?=$sto_data->emrs_id?>" type="hidden">

                                                        <td><?=$sto_data->product_name;?></td>
                                                            
                                                        <td><?=$sto_data->item_uom_id;?></td>
                                                            
                                                        <td><input type='text' id="bill_qty_<?=$sto_data->gpItemId;?>" name="gp_bill_qty[<?=$sto_data->doc_no?>][<?=$sto_data->id?>]" class="form-control bill_qty show-decimal" value="<?=$this->sma->formatMoney($sto_data->dlv_note_qty)?>" readonly="readonly" />
                                                        </td>
                                                            
                                                        <td><input type='text' id="actual_qty_<?=$sto_data->gpItemId?>" name="gp_actual_qty[<?=$sto_data->doc_no?>][<?=$sto_data->id?>]" class="form-control actual_qty assign_qty show-decimal" value="<?=$this->sma->formatMoney($sto_data->act_rcpt_qty)?>" at="actual_qty" оnkeypress="return isNumberKey(event,this)" <?php if($sto_data->gp_type == '1084') { ?> readonly="readonly" <?php } ?> />
                                                        </td>
                                                            
                                                        <td><input type='text' id="short_qty_<?=$sto_data->gpItemId?>" name="gp_short_qty[<?=$sto_data->doc_no?>][<?=$sto_data->id?>]" at="short_qty" class="form-control short_qty show-decimal" value="<?=$this->sma->formatMoney($sto_data->p_qty)?>" оnkeypress="return isNumberKey(event,this)" readonly="readonly" />
                                                        </td>
                                                            
                                                        <td><input type='text' id="rate_<?=$sto_data->gpItemId?>" name="gp_rate[<?=$sto_data->doc_no?>][<?=$sto_data->id?>]" at="rate" class = "form-control rate show-decimal" value="<?=$this->sma->formatMoney($sto_data->item_price)?>" оnkeypress="return isNumberKey(event,this)" readonly="readonly" />
                                                        </td>
                                                            
                                                        <td><input type='text' id="amount_<?=$sto_data->gpItemId?>" name="gp_amount[<?=$sto_data->doc_no?>][<?=$sto_data->id?>]" class="form-control amount show-decimal" value="<?=(float)$sto_data->item_amt?>" оnkeypress="return isNumberKey(event,this)" readonly="readonly" />
                                                        </td>
                                                        <input name="gp_item_uom[<?=$sto_data->doc_no?>][<?=$sto_data->id?>]" value="<?=$sto_data->item_uom_id?>" type="hidden">
                                                        <input name="gp_item_id[<?=$sto_data->doc_no?>][<?=$sto_data->id?>]" value="<?=$sto_data->item_id?>" type="hidden">
                                                        <input name="gp_item_code[<?=$sto_data->doc_no?>][<?=$sto_data->id?>]" value="<?=$sto_data->product_code?>" type="hidden">
                                                        </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="stock_details" class="tab-pane fade in">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="table-responsive">
                                                    <div class="col-sm-4">
                                                        <table class="table items table-striped table-bordered table-condensed table-hover" >
                                                        <thead>
                                                            <tr>
                                                            <th class="col-sm-2"><?= lang("item_name"); ?></th>
                                                            <th class="col-sm-2"><?= lang("qty"); ?></th>
                                                            <th class="col-sm-2"><?= lang("action"); ?></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                <?php
                                if(count($row['sto_data'])>0){
                                    $tmp_item_arrays = array();
                                    foreach($row['sto_data'] as $item_values){
                                        if(!in_array($item_values->gpItemId, $tmp_item_arrays)){
                                            array_push($tmp_item_arrays,$item_values->id);
                                    ?>
                                    <tr class="bin_background" id="slot_<?=$item_values->gpItemId?>" data-original-title="" data-container="body" data-toggle="tooltip" data-placement="bottom" style="cursor: pointer;">
                                        <td><?php echo $item_values->product_name?></td>
                                        <td class="actual_qty_<?php echo $item_values->gpItemId?>"><?=number_format($item_values->act_rcpt_qty,2);?></td>
                                        <td><a href="javascript:void(0)" class="allocate_lot lot_<?=$item_values->id?>_<?=$item_values->gpItemId?>" data-check="gatepass_type" data-id="lot_<?=$item_values->id?>_<?=$item_values->gpItemId;?>">Allocate Lot</a> </td>
                                    </tr>
                                    <?php
                                        }
                                    }
                                }
                                                                          
                                ?>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                <div class="col-sm-8 stock_data_div">
                                    
                                    <?php
                                    if(count($lot_datas['lot_data'])>0){
                                        $tmp_item_arrays = array();
                                        foreach($row['sto_data'] as $itm_key=>$item_val){
                                            if(!in_array($item_val->item_id, $tmp_item_arrays)){
                                                array_push($tmp_item_arrays,$item_val->item_id);
                                                if($itm_key==0)
                                                    $display = "";
                                                else
                                                    $display = "display:none";
                                                ?>

                                    <div class="col-sm-12 lot_bin_div_<?=$item_val->gpItemId?> top_lot_div" id="lot_bin_<?=$item_val->gpItemId?>" style="<?=$display;?>">

                                        <div class="col-sm-6 lot_div lot_<?=$item_val->gpItemId?>" >
                                            <table class="table table-bordered table-condensed lot_bin_table" id="lot_<?=$item_val->gpItemId?>" data-id="lot_<?=$item_val->gpItemId?>" style="margin-bottom:0;">
                                                <thead>
                                                    <tr>
                                                        <th class="col-sm-2"><?= lang("lot_name"); ?></th>
                                                        <th class="col-sm-2"><?= lang("qty"); ?></th>
                                                        <th class="col-sm-6"><?= lang("action"); ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach($lot_list[$item_val->gpItemId]['lot_data'] as $lot_key=>$lot_val)
                                                    {
                                                        //echo "<pre>";print_r($lot_val);
                                                        $show = $lot_key==0?"background":"";                    
                                                    ?> 
                                                       <tr class="<?php echo $show?>" data-id="lot_<?=$lot_val->lot_no?>_<?=$lot_val->gpItemId?>" style="cursor: pointer;"> 
                                                            <td>
                                                            <input type="hidden" class="lot-class" name="lot_qty[<?=$lot_val->gpItemId?>][]" value="<?=$lot_val->lot_qty?>" />
                                                            <?=$lot_val->lot_no?>                                       
                                                            </td>
                                                            <td><?=number_format($lot_val->lot_qty,2)?></td>
                                                            <td><a href="javascript:void(0)" id="bin[<?=$lot_val->id;?>][]" data-check="gatepass_type" at="lot_id_<?=$lot_val->id?>" class="allocate_bin" data-id="<?=$lot_val->lot_no;?>_<?=$lot_val->gpItemId;?>_<?=$lot_val->lot_qty;?>" >Allocate Bin</a> <a href="javascript:void(0)" id="bin[<?=$lot_val->id;?>][]" data-del="<?=$lot_val->lot_no?>_<?=$lot_val->gpItemId?>" at="lot_id_<?=$lot_val->id?>" class="delete" style="color: green;">Delete</a></td>
                                                        </tr>
                                                    <?php
                                                    }
                                                    ?> 
                                                </tbody>
                                            </table>
                                        </div>
                
                <div class="col-sm-6 bin_div bin_<?=$item_val->gpItemId?>">
                <?php
                foreach($lot_list[$item_val->gpItemId]['lot_data'] as $lotdatakey=>$lotData)
                {
                    $bin_show = $lotdatakey!=0?"display:none;":"";
                    if($lotData != '')
                    {
                    ?>
                        <table class="table table-bordered table-condensed bin_table" data-id="lot_bin_<?=$lotData->lot_no?>_<?=$item_val->gpItemId?>" style="margin-bottom:0;<?php echo $bin_show ?>">
                            <thead>
                                <tr>
                                    <th class="col-sm-2"><?= lang("bin_name"); ?></th>
                                    <th class="col-sm-2"><?= lang("qty"); ?></th>
                                    <th class="col-sm-2"><?= lang("action"); ?></th>
                                </tr>
                            </thead>
                        <?php
                        foreach($bin_list[$lotData->id]['bin_data'] as $bin_keys=>$binvalue)
                        {
                            //echo "Bin val --- ".$bin_val['lot_id']."<br>";
                            foreach($binvalue as $bin_val){
                                if($bin_val->lot_no == $lotData->lot_no)
                                {
                        ?>
                            <tbody>
                                <tr>
                                    <td><input type="hidden" class="" name="bin_qty[<?=$bin_val->lot_no?>][]" value="<?=number_format($bin_val->bin_qty,2)?>" />
                                    <?=$bin_val->bin_nm?>           
                                    </td>
                                    <td><?=number_format($bin_val->bin_qty,2)?></td>
                                    <td><a href="javascript:void(0)" id="bin_id_<?=$bin_val->id?>" class="deletebin">Delete</a></td>
                                </tr>
                            </tbody>
                        <?php

                                }
                            }
                        }
                        ?>
                        </table>
                <?php
                    }
                }
                ?>
                </div>
                                    </div>
                                    <div class="clearfix"></div>
                                        <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <!--end popopo div -->
    <?php
        }
    }
    ?>
    </div>
    </div>
    <!-- end po_ro_tab div -->
</div>
</div>
</div>

<div class="clearfix"></div> 

</div>           

        <div class="col-md-12">
            <div class="from-group">
                <button type="submit" class="btn btn-success" name="save-sale" id="save-sale"><?= lang('save'); ?></button>
                <?php //if($this->uri->segment(3) == ""){ ?>                
             <!--    <button type="submit" class="btn btn-primary" style="background-color: green" id="hold-sale"><?= lang('hold'); ?></button> -->
                <?php //} ?>
                <button type="button" class="btn btn-danger" onclick="location.href = '<?php echo base_url()?>gatepass/listing';">
                    <?= lang('Cancel') ?></button>
            </div>
        </div>
    </div>
       
            
    </div>

        </div>
    </div>
</div>
<?php echo form_close(); ?>