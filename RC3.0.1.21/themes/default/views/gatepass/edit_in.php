<script type="text/javascript">
 jQuery(document).ready(function() {
     $( document ).on( 'change', '.show-decimal', function() {
        var input_val = $(this).val();
        if(!isNaN(input_val)){
            $(this).val(parseFloat(input_val).toFixed(2));
        }
        var parent_obj = $(this).closest('tr').attr('id');
        var bill_qty = parseFloat($("#"+parent_obj).find('.bill_qty').val());
        var actual_qty = parseFloat($("#"+parent_obj).find('.actual_qty').val());
        var short_qty = parseFloat($("#"+parent_obj).find('.short_qty').val());
        var rate = parseFloat($("#"+parent_obj).find('.rate').val());
        //var bill_qty = $("#"+parent_obj).find('.bill_qty').val();
        if((bill_qty != '') && (actual_qty!='') && (!isNaN(bill_qty)) && (!isNaN(actual_qty))){
            var short_qty = ((actual_qty)-bill_qty).toFixed(2);
            var amount = (rate*actual_qty).toFixed(2);
        }
        $("#"+parent_obj).find('.short_qty').val(short_qty);
        $("#"+parent_obj).find('.amount').val(amount);

     });

        /*jQuery("#lr_date").datetimepicker({
                changeMonth: true,
                changeYear: false,
                autoclose:true,
                showSecond: false,
                minView: 2,
                format: 'yyyy-mm-dd',
                startDate: 'today',
                endDate: new Date()
        });*/

        var today = new Date();
        var lastDate = new Date(today.getFullYear(), today.getMonth(0), 31);
        var startDate = new Date(today.getFullYear(), 0, 1);

        //var lastDate = new Date(today.getFullYear(), today.getMonth(0), 31);
        jQuery("#lr_date").datetimepicker({
            autoclose:true,
            showSecond: false,
            minView: 2,
            format: 'yyyy-mm-dd',
            startDate: startDate,
            endDate: lastDate
        });

        jQuery("#sto_date").datetimepicker({
                changeMonth: true,
                autoclose:true,
                showSecond: false,
                minView: 2,
                format: 'yyyy-mm-dd',
                startDate: 'today',
                endDate: new Date()
        });

        jQuery("#edit_mfg_date").datetimepicker({
                changeMonth: true,
                autoclose:true,
                showSecond: false,
                minView: 2,
                format: 'yyyy-mm-dd',
                startDate: 'today',
                endDate: new Date()
        });
        jQuery("#edit_exp_date").datetimepicker({
                changeMonth: true,
                autoclose:true,
                showSecond: false,
                minView: 2,
                format: 'yyyy-mm-dd',
                startDate: 'today'
                //endDate: new Date()
        });    
    
    $('#po_ro_tab_div').on('click','.remove',function() {
        $(this).closest('div.po_ro_tab').remove();
    });
    
    $( document ).on( 'click', '#genlot', function() {
        var no = generateLotNo(6);
        $("#lot_no").val(no);
        return false;
    });
    $( document ).on( 'click', '#cancelButton', function() {
        var parentId = $(this).closest('.modal').attr('id');
        $("#"+parentId).modal('hide');
    }); 
    
    $( document ).on( 'click', '.editLot', function() {
        var mylist = [];
        var obj = $(this).closest("div#lotModal");
           var flg = 0;

            if($("#lot_no").val()==''){
                    alert("Please enter Lot No.");
                    flg++;
                    return false;
                }
            var item_id = $("#item_id").val();
            
            
            if(flg==0){
                
                var row = {};
                row["lot_no"] = $("#lot_no").val();
                row["lot_qty"] = $("#edit_lot_qty").val();
                row["batch_no"] = $("#edit_batch_no").val();
                row['mfg_date'] = $("#edit_mfg_date").val();
                row['exp_date'] = $("#edit_exp_date").val();
                mylist.push(row);

                var lot_obj = decodeURIComponent(JSON.stringify(mylist));       
                var input = document.createElement("input");
                input.type = "hidden";
                input.name = 'lot_data['+item_id+'][]';
                input.value = lot_obj;
                $("#lotModal").modal('hide');
                
                var table_id = 'lot_'+item_id;

                if ( $( "#"+table_id ).length ) {
                   
                    var tr = $('<tr class="background" data-id="lot_'+row['lot_no']+'_'+item_id+'"></tr>');

                    var div_td_html= '<td><input type="hidden" class="lot-class" name="lot_qty['+item_id+'][]" value="'+row['lot_qty']+'" />'+row["lot_no"];
                    tr.append(input);   
                    div_td_html+='</td>';
                    div_td_html+= '<td>'+row['lot_qty']+'</td>';
                    div_td_html+= '<td><a href="javascript:void(0)" id="bin['+item_id+'][]" data-id="'+row['lot_no']+'_'+item_id+'_'+row['lot_qty']+'" class="allocate_bin">Allocate Bin</a></td>';

                    tr.append(div_td_html);

                    $(".top_lot_div").hide();
                    $('#binModal').modal('hide');
                    $("#"+table_id ).append(tr);
                    $(".bin_table").hide();
                    $('table[data-id="lot_bin_'+row['lot_no']+'_'+item_id+'"]').show();
                    $(".lot_bin_div_"+item_id).show();
                    $( "#"+table_id ).show();
                }else{
                  
                    var pTag = $("");
                    
                    var table = $('<table class="table table-bordered table-condensed lot_bin_table" id="lot_'+item_id+'" style="margin-bottom:0;"></table>');
                    div_html+= '<thead>';
                    div_html+= '<tr>';
                    div_html+= '<th><?= lang("lot_name"); ?></th>';
                    div_html+= '<th><?= lang("qty"); ?></th>';
                    div_html+= '<th><?= lang("action"); ?></th>';
                    div_html+= '</tr>';
                    div_html+= '</thead>';
                    //div_html+= '<tbody>';
                    var tr = $('<tr class="background" data-id="lot_'+row['lot_no']+'_'+item_id+'"></tr>');
                    var div_td_html= '<td><input type="text" name="lot_qty['+item_id+'][]" value="'+row['lot_qty']+'" />'+$("#bin_lot_no").val();
                    tr.append(input);     
                    div_td_html+='</td>';
                    div_td_html+= '<td>'+row['lot_qty']+'</td>';
                    div_td_html+= '<td><a href="javascript:void(0)" data-id="'+row['lot_no']+'_'+item_id+'_'+row['lot_qty']+'" class="allocate_bin">Allocate Bin</a></td>';
                    console.log("tr=============>",tr);
                    tr.append(div_td_html);
                    table.append(div_html);
                    table.append(tr);
                    $(".lot_"+item_id).append(table);
                }
                obj.find('input:text').val(''); 

            }



    });

     $( document ).on( 'click', '.bin_background', function() {
        var slot_id = $(this).attr("id"); 
        $(".bin_background").css("background-color","#FFFFFF");
        $(this).css("background-color","#DDDDDD");
        var table_length = $("#lot_bin_"+slot_id+" > tbody").children().length;
        if(table_length>0){
            $(".lot_bin_table").hide();
            $("#lot_bin_"+slot_id).show();
        }

     });
    
    $( document ).on( 'click', '.editBin', function() {


        var bin_list = [];
        var obj = $(this).closest("div#binModal");
        var flg = 0;   
        var item_id = $("#bin_item_id").val();
        var row = {};
        row['bin_lot_no'] = $("#bin_lot_no").val();
        row['bin_qty'] = $("#bin_qty").val();
        row['bin_id'] = $("#bin_id").val();
        bin_list.push(row);

        var bin_obj = decodeURIComponent(JSON.stringify(bin_list));       
        var input = document.createElement("input");
        input.type = "hidden";
        input.name = 'bin_data['+row['bin_lot_no']+'][]';
        input.value = bin_obj;

        var table_id = 'lot_bin_'+row['bin_lot_no']+'_'+item_id;
        if ( $('table[data-id='+table_id+']').length ) {                
            var tr = $('<tr class="background"></tr>');

            var div_td_html= '<td><input type="hidden" class="" name="bin_qty['+row['bin_lot_no']+'][]" value="'+row['bin_qty']+'" />'+$("#bin_lot_no").val();
            tr.append(input);   

            div_td_html+='</td>';
            div_td_html+= '<td>'+row['bin_qty']+'</td>';
            div_td_html+= '<td><a href="javascript:void(0)" data-id="'+row['bin_lot_no']+'_'+item_id+'_'+row['bin_qty']+'" class="allocate_bin">Edit</a></td>';
            div_td_html+= '<td><a href="javascript:void(0)" data-id="'+row['bin_lot_no']+'_'+item_id+'_'+row['bin_qty']+'" class="allocate_bin">Delete</a></td>';
            
            tr.append(div_td_html);
            
            $('#binModal').modal('hide');
            $('table[data-id='+table_id+']').show();
            $('table[data-id='+table_id+']').append(tr);
        }else{
            var pTag = $("");
            
            var tables = $('<table class="table table-bordered table-condensed bin_table" data-id="lot_bin_'+row['bin_lot_no']+'_'+item_id+'" style="margin-bottom:0;"></table>');
            var div_html= '<thead>';
            div_html+= '<tr>';
            div_html+= '<th><?= lang("bin_name"); ?></th>';
            div_html+= '<th><?= lang("qty"); ?></th>';
            div_html+= '<th colspan="2"><?= lang("action"); ?></th>';
            div_html+= '</tr>';
            div_html+= '</thead>';
            
            var tr = $('<tr class="background"></tr>');

            var div_td_html= '<td><input type="hidden" name="bin_qty['+row['bin_lot_no']+'][]" value="'+row['bin_qty']+'" />'+$("#bin_lot_no").val();
            tr.append(input);   

            div_td_html+='</td>';
            div_td_html+= '<td>'+row['bin_qty']+'</td>';
            div_td_html+= '<td><a href="javascript:void(0)" data-id="'+row['bin_lot_no']+'_'+item_id+'_'+row['bin_qty']+'" class="allocate_bin">Edit</a></td>';
            div_td_html+= '<td><a href="javascript:void(0)" data-id="'+row['bin_lot_no']+'_'+item_id+'_'+row['bin_qty']+'" class="allocate_bin">Delete</a></td>';

            tr.append(div_td_html);
            
            tables.append(div_html);
            tables.append(tr);

            $(".bin_"+item_id).append(tables);

        }
        $('#binModal').modal('hide');
        $('table[data-id='+table_id+']').siblings().hide();
        $('table[data-id='+table_id+']').show()

        obj.find('input:text').val('');



    });

    $( document ).on( 'click', '.allocate_lot', function() { 
        $(this).closest("div.po_ro_tab").find("table tr").css("background-color","#FFFFFF");
        $(this).closest("tr").css("background-color","#dddddd");
        $(this).closest("tr").css("background-color","#dddddd");   
        $("#lotModalLabel").html("Add/Edit Lot");
        var id = $(this).attr('data-id').split("_");
        var item_qty = $(".actual_qty_"+id[1]).text();
        $("#item_id").val(id[1]);
        //alert(item_qty)
        $("#edit_lot_qty").val(item_qty)

        $('#lotModal').modal({backdrop: 'static', keyboard: false});
        $('#lotModal').css('zIndex', '1050');
    });
    
    $( document ).on( 'click', '.allocate_bin', function() { 

        $("#binModalLabel").html("Add/Edit Bin");
        var id = $(this).attr('data-id').split("_");
        
        $("#bin_item_id").val(id[1]);
        $("#binModalLabel").html("Add/Edit Bin");
        
        $("#bin_lot_no").val(id[0]);
        var total_qty = 0;
        var lot_qty = id[2];
       // console.log(id);
        $('input[name="bin_qty['+id[0]+'][]"]').each(function(){
            total_qty = parseFloat(total_qty)+parseFloat($(this).val());
        });
        
        var rest_qty = parseFloat(lot_qty) - total_qty;
       

        $("#bin_qty").val(rest_qty);
        $('#binModal').modal({backdrop: 'static', keyboard: false});
        $('#binModal').modal({backdrop: 'static', keyboard: false});
    });

    $('#gatepass_type').change(function () {
            var gp_type = $(this).val();
            //console.log(gp_type);
            if(gp_type!=0){
                 $.ajax({
                    url: site.base_url+"gatepass/getGPType",
                    async: false,
                    type: "GET",
                    data: {"gp_type_id":gp_type},
                    dataType:"json",
                    success: function(data) {
                        //console.log(data);
                        if(data != 0){
                            $.each(data,function(key,val){
                            console.log(val);
                           var opt = $('<option />'); 
                            opt.val(val);
                            opt.attr('attri',key);
                            opt.text(val);
                            //console.log(opt);
                            $('#sto_no').append(opt);
                            });
                        }
                        else{
                            bootbox.alert("No Invoice available for this gatepass!");
                            return false;
                        }
                        
                        
                    }
                })
            }
    });

    $('#sto_no').change(function () {
            var sto_no = $(this).val(); // <-- change this line
            if(sto_no!=0){
                 $.ajax({
                    url: site.base_url+"gatepass/getStoDetails",
                    async: false,
                    type: "POST",
                    data: {"sto_id":sto_no},
                    dataType:"json",
                    success: function(data) {
                        $.each(data,function(key,val){ 
                           $("#sto_date").val(val.doc_date);
                        });
                        
                    }
                })
            }
    });


        $('#add_sto_details').click(function () { 
            //console.log("here"); 
            //add_product_tab('',111,'2016-11-08');
            var sto_length = $("#po_ro_tab_div").length;
            var sto_no = $("#sto_no").val();
            var sto_date = $("#sto_date").val();
            //alert(sto_length);return false;
            var sto_flag = 0;
           if(sto_length>=1){
                $.each($('.sto_list'), function(index, attr) {
                    if(attr.value===sto_no){
                        sto_flag++;
                    }
                });
           }
           //alert(sto_flag);
           if(sto_flag==0){
                $.ajax({
                    url: site.base_url+"gatepass/getStoProducts",
                    async: false,
                    type: "POST",
                    data: {"sto_no":sto_no,"sto_date":sto_date},
                    dataType:"json",
                    success: function(data) {
                       // console.log(data);
                       if(data && data.length>0){
                        add_product_tab(data,sto_no,sto_date);   
                       }else{
                        alert("Please Select Invoice No.")
                         return false;
                       }                                          
                    }
                })
        }else{
            alert("This Invoice No. is already selected.")
            return false;

        }
    });
         

    $("#gatepass-form").validate({
        ignore: [],
        rules: {
             other_charges: {
                required : true,
                digits : true,
                checkChargeVal : true 
            },
            supplier_id:{
                required : true               
            },
            lr_name:{
                required : true               
            },
        },
        messages: {
            other_charges : {
                required : "Please enter unloading charges",
                checkChargeVal : "Please enter unloading charges"
            },
            supplier_id:{
                required : "Please select a Supplier"
            },
            lr_name:{
                required : "Please select a Transporter name"
            },
            sto_no:{
                required : "Please select a Invoice no."
            },
            warehouse:{
                required : "Please select a Warehouse"
            }

        },

        errorElement: "em", 
        submitHandler:function(form){ 
            var error = [];
            $('.actual_qty').
            $('.actual_qty').each(function(i, obj) {
                var ro_id = $(this).attr('id').split("_");
                var receive_qty = parseInt($(this).val());
                //alert(receive_qty);
                if(receive_qty!=0){
                    var total_qty = 0;  
                    $('input[name="lot_qty['+ro_id[2]+'][]"]').each(function(){
                        total_qty = parseFloat(total_qty)+parseFloat($(this).val());
                    });
                    if(total_qty<receive_qty){
                       // alert("less quantity")
                        error.push(ro_id[2]);
                        $(".bin_background").addClass('white-background');
                        $("#slot_"+ro_id[2]).removeClass('white-background');        
                        $("#slot_"+ro_id[2]).addClass('background');
                        $("#slot_"+ro_id[2]).addClass('add-border');
                        $("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('.box-content-div').show();
                        $("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('#item_details').hide();
                        $("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('#stock_details').show();
                        $("#slot_"+ro_id[2]).tooltip({'show': true,
                            'placement': 'bottom',
                            'title': "Please allot Lot to the received items"
                        });
                        $("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('.stock_details_tab').addClass('active');
                        $("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('.item_details_tab').removeClass('active');
                       // return false;
                    }else{
                       jQuery.grep(error, function(value) {
                        return value != ro_id[2];
                      });
                       $("#slot_"+ro_id[2]).tooltip({'show': true,
                            'placement': 'bottom',
                            'title': "Lot assigned successfully"
                        });
                        //$(".bin_table")
                        $("#slot_"+ro_id[2]).closest('#stock_details').find(".top_lot_div").hide();
                        $("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('.box-content-div').hide();
                        $("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('#item_details').show();
                        $("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('#stock_details').hide();
                        $("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('.stock_details_tab').removeClass('active');
                        $("#slot_"+ro_id[2]).closest('div.po_ro_tab').find('.item_details_tab').addClass('active');
                        // error.remove(ro_id[2]);
                        $("#slot_"+ro_id[2]).removeClass('add-border');
                    }
                }
                
            });
            if(error.length>0){
                return false;
            }
            var bin_error = [];
            $('.lot-class').each(function(i, obj) {
                var lot_qty = parseInt($(this).val());
                var lot_id = $(this).closest('tr').attr('data-id');
                var lot_no = $(this).closest('tr').attr('data-id').split("_");

                if(lot_qty!=0){
                    var total_bin_qty = 0;
                    $('input[name="bin_qty['+lot_no[1]+'][]"]').each(function(){
                        total_bin_qty = parseFloat(total_bin_qty)+parseFloat($(this).val());
                    });

                    if(total_bin_qty<lot_qty){
                       
                        bin_error.push(lot_no[1]);
                        $(this).closest('.stock_data_div').find(".top_lot_div").hide();
                        $(".bin_background").addClass('white-background');
                        $("#slot_"+lot_no[2]).removeClass('white-background');        
                        $("#slot_"+lot_no[2]).addClass('background');
                        
                      
                        $(".lot_bin_div_"+lot_no[2]).show();
                        $('tr[data-id='+lot_id+']').addClass('add-border');
                        $('tr[data-id='+lot_id+']').closest('div.po_ro_tab').find('.box-content-div').show();
                        $('tr[data-id='+lot_id+']').closest('div.po_ro_tab').find('#item_details').hide();
                        $('tr[data-id='+lot_id+']').closest('div.po_ro_tab').find('#stock_details').show();
                        $('tr[data-id='+lot_id+']').tooltip({'show': true,
                            'placement': 'bottom',
                            'title': "Please allot Bin to the received items"
                        });
                        $('tr[data-id='+lot_id+']').closest('div.po_ro_tab').find('.stock_details_tab').addClass('active');
                        $('tr[data-id='+lot_id+']').closest('div.po_ro_tab').find('.item_details_tab').removeClass('active');
                        return false;
                    }else{                       
                       jQuery.grep(bin_error, function(value) {
                        return value !=lot_no[1] ;
                      });
                       
                       $('tr[data-id='+lot_id+']').tooltip({'show': true,
                            'placement': 'bottom',
                            'title': "Bin assigned successfully"
                        });
                       
                        $('tr[data-id='+lot_id+']').removeClass('add-border');
                        $(".lot_bin_"+lot_no[1]+"_"+lot_no[2]).hide();
                        
                        $(".lot_bin_div_"+lot_no[2]).hide();
                    }
                }
               
            });
       
            if(bin_error.length>0){
                return false;
            }else{
                form.submit();    
            }
        }



    });


    $( document ).on( 'click', '.item_details_tab', function() {
        var obj = $(this).closest("div.po_ro_tab");
        $("#dbTab").children('li').removeClass('active');
        $(this).addClass('active');
        obj.find("#item_details").show()
        obj.find("#stock_details").hide();

    });
    
    $( document ).on( 'click', '.stock_details_tab', function() {
   
        var obj = $(this).closest("div.po_ro_tab");
        var flg1 = 0;
        var flg2 = 0;
        obj.find("input").each(function () {
           
            if(this.value==''){
                alert("Please fill all fields");
                flg1++;
                return false;
            }
            if($(this).attr('at')=='actual_qty'){
                var id = this.id;
                                            
                obj.find("."+id).text(this.value);               
            }
        });

        if(flg1==0 && flg2==0){
            
            var obj = $(this).closest("div.po_ro_tab");
            $("#dbTab").children('li').removeClass('active');
            $(this).addClass('active');
            obj.find("#item_details").hide()
            obj.find("#stock_details").show();

        }
    });

    $.validator.addMethod("checkChargeVal",
        function(value, element) {

            if(parseInt(value)>=0)
                return true;
            else 
                return false;
    });

});
 



function isNumberKey(e, t) {

    try {
        if (window.event) {

            var charCode = window.event.keyCode;
        }
        else if (e) {

            var charCode = e.which;

        }
        else { return true; }

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {

            return false;
        }
        return true;
    }
    catch (err) {
        alert(err.Description);
    }
} 

function add_product_tab(product_array, sto_no, sto_date){
    //console.log(product_array);
    var po_no = 111;
    var div_html="";
    div_html+= '<div class="col-sm-12 po_ro_tab id="popopo">';
    //div_html+= '<hr></hr>';
    div_html+= '<div style="background-color:#faebcc;float:left;height:45px;width:100%;border-radius:5px;line-height:45px;"><div class="col-sm-12 remove text-right" style="cursor:pointer;color:#000000;float:right;width:20px;line-height:23px;">X</div>';  
    div_html+= '<div class="col-sm-6 text-left" style="color:#000000"></div></div>';    
    
    div_html+= '<div class="row" style="margin-bottom: 15px;">';
    div_html+= '<div class="col-md-12" >';
    div_html+= '<div class="box-content" style="border:1px solid #DDDDDD;border-radius:2px;">';
    div_html+= '<div class="row">';
    div_html+= '<div class="col-md-12">';
    div_html+= '<ul id="dbTab" class="nav nav-tabs">';
    div_html+= '<li class="item_details_tab active"><a href="javascript:void(0)"><?= lang('item_details') ?></a></li>';
    div_html+= '<li class="stock_details_tab"><a href="javascript:void(0)"><?= lang('stock_details') ?></a></li>';
    div_html+= '</ul>';
    div_html+= '<div class="tab-content">';
    div_html+= '<div id="item_details" class="tab-pane fade in active">';
    div_html+= '<div class="row">';
    div_html+= '<div class="col-sm-12">';
    div_html+= '<div class="table-responsive">';
    div_html+= '<table class="table items table-striped table-bordered table-condensed table-hover">';
    div_html+= '</thead>';
    div_html+= '<thead>';
    div_html+= '<tr>';
    div_html+= '<th class="col-md-2"><?= lang("product_name"); ?></th>';
    div_html+= '<th class="col-md-1"><?= lang("Unit"); ?></th>';
    div_html+= '<th class="col-md-2"><?= lang("del_note_qty"); ?></th>';
    div_html+= '<th class="col-md-2"><?= lang("actual_qty"); ?></th>';
    div_html+= '<th class="col-md-2"><?= lang("short_qty"); ?></th>';
    div_html+= '<th class="col-md-2"><?= lang("rate"); ?></th>';
    div_html+= '<th class="col-md-2"><?= lang("amount"); ?></th>';
    div_html+= '</tr>';
    div_html+= '</thead>';

    if(product_array.length>0){
        for(var i in product_array){
        //console.log(product_array);                   
            var dlv_qty = parseFloat(product_array[i].dlv_qty).toFixed(2);  
          //  alert(parseFloat(dlv_qty).toFixed(2))
            div_html+= '<tr id="prod_'+product_array[i].id+'">';
            div_html+= '<td>'+product_array[i].code+'';
            div_html+= '<td>'+product_array[i].itm_uom_bs+'';         
            
            div_html+= '<input type="hidden" name="item_list['+sto_no+'][]" class="form-control" value="'+product_array[i].id+'" />';

            div_html+= '<input type="hidden" name="org_id" class="form-control" value="'+product_array[i].org_id+'" />'; 
            div_html+= '<input type="hidden" name="warehouse_id" class="form-control" value="'+product_array[i].warehouse_id+'" />'; 
            div_html+= '<input type="hidden" name="segment_id" class="form-control" value="'+product_array[i].segment_id+'" />'; 
            
            div_html+= '<input type="hidden" name="sto_list['+sto_no+'][]" class="form-control sto_list" value="'+product_array[i].doc_no_src_id+'" />';

            div_html+= '<input type="hidden" name="sto_date_list['+sto_no+'][]" class="form-control" value="'+sto_date+'" />';
            
            div_html+= '</td><td><input type="text" name="bill_qty['+sto_no+']['+product_array[i].id+']"  class="form-control bill_qty show-decimal" value="'+parseFloat(product_array[i].bal_qty).toFixed(2)+'" onkeypress="return isNumberKey(event,this)" required="required" /></td>';
            
            div_html+= '<td><input type="text" name="actual_qty['+sto_no+']['+product_array[i].id+']" id="actual_qty_'+product_array[i].id+'" at="actual_qty" class="form-control show-decimal actual_qty" onkeypress="return isNumberKey(event,this)" required="required"/></td>';
            
            div_html+= '<td><input type="text" name="short_qty['+sto_no+']['+product_array[i].id+']" id="short_qty_'+product_array[i].id+'" at="short_qty" class="form-control show-decimal short_qty" onkeypress="return isNumberKey(event,this)" required="required"/></td>';
            
            div_html+= '<td><input type="text" name="rate['+sto_no+']['+product_array[i].id+']" id="rate_'+product_array[i].id+'" class="form-control rate show-decimal" readonly="readonly" onkeypress="return isNumberKey(event,this)" value="'+product_array[i].itm_price_bs+'" /></td>';
            
            div_html+= '<td><input type="text" name="amount['+sto_no+']['+product_array[i].id+']" class="form-control amount show-decimal" readonly="readonly" onkeypress="return isNumberKey(event,this)" value="'+product_array[i].itm_amt_bs+'"/></td>';

            div_html+= '<input type="hidden" name="item_uom['+sto_no+']['+product_array[i].id+']"  value="'+product_array[i].itm_uom_bs+'"/>';

            div_html+= '<input type="hidden" name="item_id['+sto_no+']['+product_array[i].id+']"  value="'+product_array[i].item_id+'"/>';

            div_html+= '<input type="hidden" name="item_code['+sto_no+']['+product_array[i].id+']"  value="'+product_array[i].code+'"/>';
            div_html+= '</tr>';
        }
    }
    div_html+= '</tbody>';
    div_html+= '</table>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    
    div_html+= '<div id="stock_details" class="tab-pane fade in">';
    div_html+= '<div class="row">';
    div_html+= '<div class="col-sm-12">';
    div_html+= '<div class="table-responsive">';
    div_html+= '<div class="col-sm-4">';
    div_html+= '<table class="table items table-striped table-bordered table-condensed table-hover">';
    div_html+= '<thead>';
    div_html+= '<tr>';
    div_html+= '<th class="col-md-2"><?= lang("item_name"); ?></th>';
    div_html+= '<th class="col-md-2"><?= lang("qty"); ?></th>';
    div_html+= '<th class="col-md-2"><?= lang("action"); ?></th>';
    div_html+= '</tr>';
    div_html+= '</thead>';    
        div_html+= '<tbody>';
    for(var i in product_array){ 
        div_html+= '<tr class="bin_background" id="slot_'+product_array[i].id+'">';
        div_html+= '<td>'+product_array[i].code+'</td>';
        div_html+= '<td class="actual_qty_'+product_array[i].id+'"></td>';
        div_html+= '<td><a href="javascript:void(0)" class="allocate_lot lot_'+product_array[i].id+'" data-id="lot_'+product_array[i].id+'_'+sto_no+'">Allocate Lot</a>';
        div_html+= '</tr>';
    }
    div_html+= '</tbody>';
    div_html+= '</table>';
    div_html+= '</div>';
    div_html+= '<div class="col-sm-8 stock_data_div">';
    /*for(var i in product_array){
        div_html+= '<table class="table table-bordered table-condensed lot_bin_table" id="lot_bin_'+product_array[i].id+'" style="margin-bottom:0;display:none;">';
        div_html+= '<thead>';
        div_html+= '<tr>';
        div_html+= '<th><?= lang("lot_name"); ?></th>';
        div_html+= '<th><?= lang("bin_name"); ?></th>';
        div_html+= '<th><?= lang("qty"); ?></th>';
        div_html+= '</tr>';
        div_html+= '</thead>';
        div_html+= '<tbody>';
    }*/

    for(var i in product_array){
        div_html+= '<div class="col-sm-6 lot_bin_div_'+product_array[i].id+' top_lot_div">';
            div_html+= '<div class="lot_div lot_'+product_array[i].id+'">';
            div_html+= '<table class="table table-bordered table-condensed lot_bin_table" id="lot_'+product_array[i].id+'" style="margin-bottom:0;display:none;">';
            div_html+= '<thead>';
            div_html+= '<tr>';
            div_html+= '<th><?= lang("lot_name"); ?></th>'
            div_html+= '<th><?= lang("qty"); ?></th>';
            div_html+= '<th><?= lang("action"); ?></th>';
            div_html+= '</tr>';
            div_html+= '</thead>';
            div_html+= '<tbody>';
            div_html+= '</tbody>';
            div_html+= '</table>';
            div_html+= '</div>';
        div_html+= '</div>';

        div_html+= '<div class="col-sm-6 bin_div bin_'+product_array[i].id+'"></div>';
    div_html+= '</div>';
    }
    div_html+= '</tbody>';
    div_html+= '</table>';
    div_html+= '</div>';


    /*div_html+= '<div class="col-sm-4">';
    div_html+= '<table class="table table-bordered table-condensed totals" style="margin-bottom:0;">';
    div_html+= '<thead>';
    div_html+= '<tr>';
    div_html+= '<th colspan="2"><?= lang("batch_no"); ?></th>';
    div_html+= '</tr>';
    div_html+= '</thead>';
                
    div_html+= '<tbody>';
    for(var i in product_array){ 
        div_html+= '<tr class="bin_background" id="slot_'+product_array[i].id+'">';
        div_html+= '<td>'+product_array[i].item_id+'</td>';
        div_html+= '<td><a href="javascript:void(0)" class="allocate_lot lot_'+product_array[i].id+'" data-id="lot_'+product_array[i].id+'_'+sto_no+'">Allocate Lot</a><a href="javascript:void(0)" style="display:none" class="allocate_bin bin_'+product_array[i].id+'" data-ids="bin_'+product_array[i].id+'_'+sto_no+'">Add</a></td>';
        div_html+= '</tr>';
    }
    div_html+= '</tbody>';
    div_html+= '</table>';
    div_html+= '</div>';*/
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';

    $('#po_ro_tab_div').append('<div>'+div_html+'</div>');
    //$('#po_ro_tab_div').accordion("add");
}
</script>

<?php
$attrib = array('data-toggle' => 'validator', 'method'=>'post', 'role' => 'form','id'=>'gatepass-form');
echo form_open_multipart("gatepass/in", $attrib)
?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('gatepass_in'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?php echo lang('enter_info'); ?></p>
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("gatepass_type", "gatepass_type"); ?>
                                <br>
                                <?php
                                $gp[''] = '';
                                    foreach ($gatepass_type as $val) {
                                        $gp[$val->type_id] = $val->name;
                                    }
                                    echo form_dropdown('gatepass_type', $gp, (isset($_POST['gatepass_type']) ? $_POST['gatepass_type'] : $val), 'id="gatepass_type" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("gatepass type") . '" required="required" style="width:100%;" ');                                
                                ?>                        
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("warehouse", "warehouse"); ?> *
                                <br>
                                <?php
                                $wh[''] ='';
                                foreach ($warehouse_name as $wh_name){
                                    $wh[$wh_name->id] = $wh_name->name;
                                }  
                                echo form_dropdown('warehouse_name', $wh, (isset($_POST['warehouse_name']) ? $_POST['warehouse_name'] : $wh_name), 'id="warehouse_name" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("warehouse_name") . '" required="required" style="width:100%;" ');                           
                                ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("entity", "entity"); ?>
                                <br>
                                <?php                              
                                $sups[''] = '';
                                foreach ($supplier_details as $sups) {
                                    $sup[$sups->id] = $sups->name;
                                }
                                echo form_dropdown('supplier_id', $sup, $sup[0], 'id="supplier_id" class="form-control required" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("supplier") . '"  style="width:100%;" ');                  
                                ?>
                               
                            </div>
                        </div>
                        
                    </div>
                </div>
                
                <div class="col-md-12">
                    <div class="panel panel-warning">
                        <div class="panel-heading"><?= lang('transpoter_details') ?></div>
                        <div class="panel-body" style="padding: 5px;">

                        <div class="col-md-12">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("Name", "Name"); ?>
                                    <?php
                                    $tp[''] = '';
                                    foreach ($tp_list as $val) {
                                        $tp[$val->id] = $val->name;
                                    }
                                    echo form_dropdown('lr_name', $tp, $tp[0], 'id="lr_name" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("transporter") . '" required="required" style="width:100%;" ');
                                    ?>
                                </div>
                            </div>                        
                            
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("lr_no", "lr_no"); ?>
                                    <?php echo form_input('lr_no', (isset($_POST['lr_no']) ? $_POST['lr_no'] : $lr_no), 'class="form-control" id="lr_no"'); ?>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("lr_date", "lr_date"); ?>
                                    <?php echo form_input('lr_date', (isset($_POST['lr_date']) ? $_POST['lr_date'] : $lr_date), 'class="form-control input-tip" id="lr_date" readonly="readonly" placeholder="LR Date"'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("vehicle_no", "vehicle_no"); ?>
                                    <?php echo form_input('vehicle_no', (isset($_POST['vehicle_no']) ? $_POST['vehicle_no'] : $vehicle_no), 'class="form-control" id="vehicle_no"'); ?>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("other_charge", "other_charge"); ?>
                                    <?php echo form_input('other_charges', (isset($_POST['other_charges']) ? $_POST['other_charges'] : $other_charges), 'class="form-control input-tip" id="other_charges"'); ?>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("remarks", "remarks"); ?>
                                    <?php echo form_input('remarks', (isset($_POST['remarks']) ? $_POST['remarks'] : $remarks), 'class="form-control input-tip" id="remarks"'); ?>
                                </div>
                            </div>
                        </div>                            
                        </div>    
                    </div>
                </div>
                
                <div class="clearfix"></div>             
                
                <div class="col-md-12">
                    <div class="panel panel-warning">
                        <div class="panel-heading"><?= lang('sto_details') ?></div>
                        <div class="panel-body" style="padding: 5px;">  
                            <div class="col-md-4">                            
                                <div class="form-group">
                                    <?= lang("sto_no", "sto_no"); ?>
                                    <br>
                                    <?php                              
                                    $sto[''] = '';
                                    foreach ($sto_no as $sto_no) {
                                        $sto[$sto_no->doc_no] = $sto_no->doc_no;
                                    }
                                    echo form_dropdown('sto_no[]', $sto, $sto[0], 'id="sto_no" class="form-control required" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("sto_no") . '"  style="width:100%;" required="required" ');                  
                                    ?>
                                </div>
                            </div>                        
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("sto_date", "sto_date"); ?>
                                    <?php echo form_input('sto_date', (isset($_POST['sto_date']) ? $_POST['sto_date'] : $sto_date), 'class="form-control input-tip" id="sto_date" readonly="readonly" placeholder="STO Date"'); ?>
                                </div>
                            </div>                        
                            <div class="col-md-4">
                                <div class="from-group">                                   
                                    <?php echo form_button('add_sto_details', $this->lang->line("add_sto_details"), 'id="add_sto_details" class="btn btn-primary" style="padding: 6px 15px; margin:29px 0 15px;"'); ?>
                                </div>
                            </div>  
                             <div id="po_ro_tab_div"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>   
               
            </div>           

        <div class="col-md-12">
            <div class="from-group">
                <button class="btn btn-primary" id="submit-sale"><?= lang('submit'); ?></button>
                <?php //echo form_submit('add_mrn', $this->lang->line("submit"), 'id="add_mrn" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                <!-- <button type="button" class="btn" id="hold">
                    <?= lang('hold') ?></button> -->
                    <button type="button" class="btn" onclick="location.href = '<?php echo base_url()?>';">
                    <?= lang('Cancel') ?></button>
            </div>
        </div>
    </div>
       
            
    </div>

        </div>
    </div>
</div>
<?php echo form_close(); ?>