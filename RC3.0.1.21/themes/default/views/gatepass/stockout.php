<script type="text/javascript">

jQuery(document).ready(function() {
                if($("#stock_val").val()<=0)   
                     {
                        alert("Release Qty Not Empty, Please Fill Release Qty First ?")
                        $("[data-dismiss=modal]").trigger({ type: "click" });
                        return true;
                     }
                var sum = 0; var sum1 = 0;
                $( document ).on( 'blur', '.pqty', function() { 
                var obj =  $(this).closest("tr").find("#tstk").text();  
                var input_val1 = parseFloat($(this).val()).toFixed(2);
                    var input_val = $(this).val();
                    if(isNaN(input_val))
                     {
                        input_val = $(this).val('');
                     }
                     if(input_val == '0.00')
                     {
                        input_val = $(this).val('');
                     }
                     if(Math.round(input_val1 * 100)> Math.round(obj * 100))
                     {
                        alert("Input Qty Not Greater Than Lot Available Qty?")
                        $(this).val('');
                        return false;
                     } 

                });

                $( document ).on( 'change', '.pqty', function() {
                    var input_val = $(this).val();
                    if(!isNaN(input_val)){
                          $(this).val(parseFloat(input_val).toFixed(2));
                        }
                    });

                $('button[name=add]').on('click',function(event){
                    var fdata = $("#stockout").serializeArray();
                    var sto_no = $("#sto_no1").val();
                    var item_id = $("#itemid").val();
                    var stock = $("#stock_val").val();
                    $('.pqty').each(function(){
                        var obj1 =  $(this).closest('tr').find('.pqty').val();
                        if(obj1!='')
                        {
                           // obj1= 0;
                            sum = parseInt(sum)+parseInt(obj1);
                        }
                    });
                    var sum1 = parseFloat(sum).toFixed(2)
                  // alert("stock:=>"+stock);
                  // alert("sum:=>"+sum1);
                    if(stock!=sum1)
                    {
                        alert("Total Fill Qty Not Equal To Release Qty? ");
                        sum = 0;
                        return false;
                    }
                    //alert("stono:=>"+sto_no+"  item_id:=> "+item_id);
                     //console.log(JSON.stringify(fdata));
                     $("input[name='lotdata["+sto_no+"]["+item_id+"]']").val(JSON.stringify(fdata));
                    $("[data-dismiss=modal]").trigger({ type: "click" });
                    return true;
                });

    });
    </script> 

<div class="modal-dialog modal-md" id="aa">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('stock_out_info'); ?></h4>
        </div>

        <div class="clearfix"></div>

        <?php $attrib = array('role' => 'form','id'=>'stockout');
        echo form_open_multipart("", $attrib); ?>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="table-responsive">
                       <table class="table items table-striped table-bordered table-condensed table-hover" id="tab">
                            <thead>
                                <tr>
                                    <th class="col-md-2"><?= lang("lot No"); ?></th>
                                    <th class="col-md-2"><?= lang("Serial No"); ?></th>
                                    <th class="col-md-2"><?= lang("Available QTY"); ?></th>
                                     <th class="col-md-2"><?= lang("rej_qty"); ?></th> 
                                    <th class="col-md-2"><?= lang("Release QTY"); ?></th>                                    
                                </tr>
                            </thead>

                             <?php if(!empty($products)) {
                             ?>
                             
                             <input type="hidden" name="itemid" id="itemid" class="form-control" value="<?php echo $id; ?>" />
                             <input type="hidden" name="sto_no1" id="sto_no1" class="form-control" value="<?php echo $sto_no; ?>" />
                             <input type="hidden" name="stock_val" id="stock_val" class="form-control" value="<?php echo $stock; ?>" />  
                             <?php
                                      $i=0;
                                      foreach ($products as $ar) {
                               ?>
                            

                                         <tbody>
                                             
                                             <tr align="center">
                                                  <td><?php echo $ar->lot_no; ?>
                                                  <input type="hidden" name="lot_<?php echo $i; ?>" id="lot_<?php echo $i; ?>" class="form-control" value="<?php echo $ar->lot_no; ?>" />
                                                  </td>

                                                   <td><?php if(!empty($ar->serial_number)) {echo $ar->serial_number;} else { echo "Not Serialized";} ?></td>

                                                   <td><div id="tstk"><?php echo number_format($ar->total_stk, 2, '.', ''); ?>
                                                   </div> </td>

                                                    <td><?php echo number_format($ar->rejected_stk, 2, '.', ''); ?> </td>

                                                   <td id="td_<?php echo $i; ?>" ><input type="text" name="pqty_<?php echo $i; ?>" id="pqty_<?php echo $i; ?>" class="form-control pqty" value="" id="pqty" onkeypress="return isNumberKey(event,this)"  />
                                                   </td>
                                                   
                                                   
                                             </tr>
                                         </tbody>
                            <?php $i++; }  } ?>
                        </table>
                    </div>
                </div>                
            </div>
        </div>

<div class="clearfix"></div>

        <div class="modal-footer">
            <?php echo form_button('add', lang('add'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript">
    function isNumberKey(e, t) {
        try {
            if (window.event) {
                var charCode = window.event.keyCode;
            }
            else if (e) {
                var charCode = e.which;
            }
            else { return true; }

            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        catch (err) {
            alert(err.Description);
        }
}
</script>
 