<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header no-print">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('pass_view'); ?></h4>
        </div>
        <div class="modal-body">
            <div class="row" >
                <div class="col-sm-12">
                    
                    <div class="col-sm-12">
                        <div class="col-md-6 text-left" style="font-size:12px;padding:0px !important;">
                            <p><strong><?= lang("supplier_name"); ?></strong> : <?php echo $supplier;?></p>
                            <p><strong><?= lang("warehouse"); ?></strong> : <?php 
                            echo $warehouse;?></p>
                        </div>
                        
                        <div class="col-md-6 text-right" style="font-size:12px;padding:0px !important;">
                             
                            <p><strong><?= lang("GatePass No."); ?></strong> : <?php 
                            echo $gp->gp_no;?></p>
                             
                            <p><strong><?= lang("reciept_date"); ?></strong> : <?php 
                            echo $gp->rcpt_date;?></p>
                            
                        </div>
                    </div>
                    
                    <div class="col-md-12"><hr></hr></div>

                    <div class="col-md-12">
                        <h5 class="text-center">Stock Details</h5>

                        <table cellpadding="0" cellspacing="0" border="0"
                           class="table table-bordered table-condensed table-hover table-striped" style="font-size:12px;">

                           <thead>
                               <tr>
                                   <th><?=lang('s_no');?></th>
                                   <th><?=lang('doc_no');?></th>
                                   <th><?=lang('product_name');?></th>
                                   <!-- <th><?=lang('product_uom');?></th> -->
                                   <th><?=lang('Unit');?></th>
                                   <th><?=lang('Request Qty.');?></th>
                                  <!--  <th><?=lang('total_quant');?></th> -->
                                   <th><?=lang('rcpt_qty');?></th>
                                   <th><?=lang('pending_qty');?></th>
                                   <th><?=lang('item_price');?></th>
                                   <th><?=lang('amount');?></th>
                               </tr>
                           </thead>

                           <tbody>
                            <?php 
                            $i=1;
                            foreach($emrs_detail as $emrs){ ?>
                               <tr>
                                   <td><?=$i;?></td>
                                   <td><?=$emrs->doc_no;?></td>
                                   <td><?=$emrs->name;?></td>
                                   <td><?=$emrs->item_uom_id;?></td>
                                   <td><?=$this->sma->formatDecimal($emrs->req_qty);?></td>
                              <!--      <td><?=$emrs->req_qty;?></td> -->
                                   <td><?=$this->sma->formatDecimal($emrs->act_rcpt_qty);?></td>
                              <!--    <td><?=str_replace('-', '', $this->sma->formatDecimal($emrs->pending_qty));?></td>  -->
                                    <td><?=$this->sma->formatDecimal($emrs->pending_qty);?></td>
                                   <td><?=$this->sma->formatDecimal($emrs->item_price);?></td>
                                   <td><?=$this->sma->formatDecimal($emrs->item_amt);?></td>
                                   
                               </tr>
                             <?php 
                             $i++;
                             }
                             ?>
                           </tbody>

                        </table>
                    </div>

                </div>
            </div>
        </div>
        
        <div class="row"></div>
    </div>
    </div>
</div>
