<script type="text/javascript">
 jQuery(document).ready(function() {

     $('#sto_no').prop('disabled',true); 
     $('#linkTo').click(function(e)
     {
        e.preventDefault();
     });
    // ---------------Ankit-HM Start-------------
     $( document ).on( 'blur', '.show-decimal', function() {
         
         var parent_obj = $(this).closest('tr').attr('id');
         var actual_qty1= parseFloat($("#"+parent_obj).find('.actual_qty').val());
         if(isNaN(actual_qty1) || actual_qty1 == '0' )
         {
            input_val = $(this).val('');
            parseFloat($("#"+parent_obj).find('.short_qty').val(''));
            var actual_qty_new12 = parseFloat($("#"+parent_obj).find('.actual_qty_new').val(''));
            var req_qty1 = parseFloat($("#"+parent_obj).find('.req_qty').val());
            var rate1 = parseFloat($("#"+parent_obj).find('.rate').val());
            var amount1 = (rate1*req_qty1).toFixed(2);
             $("#"+parent_obj).find('.amount').val(amount1);
         }
     });
     
     $('input[type="text"]').keyup(function(evt){
    var txt = $(this).val();
    $(this).val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
     });
     // ---------------Ankit-HM End-------------
     $( document ).on( 'change', '.show-decimal', function() {
        var parent_obj = $(this).closest('tr').attr('id');
        var actual_qty_new = parseFloat($("#"+parent_obj).find('.actual_qty_new').val());
        var input_val = $(this).val();
        if(!isNaN(input_val)){
            $(this).val(parseFloat(input_val).toFixed(2));
        }
        if(isNaN(input_val))
         {
            input_val = $(this).val('');
         }
         if(isNaN(actual_qty_new))
         {
            actual_qty_new = parseFloat($("#"+parent_obj).find('.actual_qty_new').val(''));
         }
       
        var req_qty = parseFloat($("#"+parent_obj).find('.req_qty').val());
        var avail_qty = parseFloat($("#"+parent_obj).find('.avail_qty').val());
        var actual_qty = parseFloat($("#"+parent_obj).find('.actual_qty').val());
        var short_qty = parseFloat($("#"+parent_obj).find('.short_qty').val());
        var rec_qty = parseFloat($("#"+parent_obj).find('.rec_qty').val());
        var rate = parseFloat($("#"+parent_obj).find('.rate').val());
        var obj = $(this).closest("tr").find("#linkTo");
        var current_url = obj.attr("href");

        if((req_qty != '') && (actual_qty!='') && (!isNaN(req_qty)) && (!isNaN(actual_qty))){
            var short_qty = (req_qty - (actual_qty)).toFixed(2);
            var amount = (rate*actual_qty).toFixed(2);
        }
        if(actual_qty > avail_qty)
        {
                alert("Release Qty Should Not Greater Then Available Qty");
                $("#"+parent_obj).find('.actual_qty').val('');
                $("#"+parent_obj).find('.actual_qty_new').val('');
                return false;
        }       
        if(actual_qty > req_qty)
            {
                alert("Release Qty Should Not Greater Then Request Qty");
                $("#"+parent_obj).find('.actual_qty').val('');
                $("#"+parent_obj).find('.actual_qty_new').val('');
                return false;
             }
       
       var new_url = current_url+"&stock="+parseFloat(actual_qty).toFixed(2);
       obj.attr("href",new_url);
       $("#"+parent_obj).find('.short_qty').val(short_qty);
        $("#"+parent_obj).find('.amount').val(amount);
        $("#"+parent_obj).find('.actual_qty_new').val(actual_qty);
        
     });
        var today = new Date();
        var lastDate = new Date(today.getFullYear(), today.getMonth(0), 31);
        var startDate = new Date(today.getFullYear(), 0, 1);

        jQuery("#lr_date").datetimepicker({
            autoclose:true,
            showSecond: false,
            minView: 2,
            format: 'yyyy-mm-dd',
            startDate: startDate,
            endDate: lastDate
        });

        jQuery("#rcpt_dt").datetimepicker({
          endDate: today,
          changeMonth: true,
          autoclose:true,
          showSecond: false,
          minView: 2,
          format: 'yyyy-mm-dd',
        });

    $('#po_ro_tab_div').on('click','.remove',function(){
        $(this).closest('div.po_ro_tab').remove();
        var div_count = $("div.po_ro_tab").length;
        if(div_count==0)
         $("#supplier_id").prop( "disabled", false );
         $("#warehouselist").prop("disabled", false);
         $("#gatepass_type").prop( "disabled", false ); 
    });
    
    $('#sto_no').change(function () {        
            var sto_val = $(this).val(); // <-- change this line          
            var sto_n = sto_val.split('-');
            var sto_no = sto_n[0];
            
            if(sto_no!=0){
                 $.ajax({
                    url: site.base_url+"gatepass/getStoDetails",
                    async: false,
                    type: "POST",
                    data: {"sto_id":sto_no},
                    dataType:"json",
                    success: function(data) {
                        
                        $.each(data,function(key,val){ 
                           $("#sto_date").val(val.doc_date);
                        });
                        
                    }
                })
            }
        });
    $('#gatepass_type').change(function(){
        // added by vikas singh to refresh dropdowns
        
        $("#warehouselist").select2('val', 'All');
        $("#sto_no").select2('val', 'All');
        $("#sto_date").val('');
        //end
        var s = $(this).val();
           
        //console.log(selWarehouse);
        $.ajax({   
            url: "gatepass/ajax_call_warehousesbysegment", 
            async: false,
            type: "POST", 
            data: "gp_type_id="+s, 
            dataType: "json", 
            success: function(data) {
        //console.log(data);
               // added by vikas singh for default selection
                var $option = $("<option selected></option>").val('1').text("Select warehouse"); $('#warehouselist').append($option).trigger('change');
                var $option = $("<option selected></option>").val('1').text("Select STO/RO No."); $('#sto_no').append($option).trigger('change');
                $('#sto_no').html("");
                //end
                $('#warehouselist').html(data);
            }
        });
    });

     $('#supplier_id').change(function () {
        var sup_name = $(this).val();
        var wh_name = $("#warehouselist").val();
        var gp_type = $("#gatepass_type").val();

    
        //alert($("#warehouselist :selected").text());
            if(gp_type != 0 && wh_name != 0){
                 $.ajax({
                    url: site.base_url+"gatepass/getGPTypeOut",
                    async: false,
                    type: "GET",
                    data: {"gp_type_id":gp_type,"wh_name":wh_name,"sup_name":sup_name},
                    dataType:"json",
                    success: function(data) {
                         $('#sto_no').prop('disabled', false);

                      var res= "<option value='' selected='selected'><\/option>";
                      if(data != res){

                         var $option = $("<option selected></option>").val('1').text("Select STO/RO No."); $('#sto_no').append($option).trigger('change');
                           $('#sto_no').html(data);                          
                        }
                        else{
                            $('#sto_no').html('');
                            $('#sto_no').prop('disabled', true);
                            bootbox.alert("No Invoice available for this gatepass!");
                            return false;
                        }
                    }
                });
            }
            else{

                if(gp_type == ''){
                    //$('#sto_no').prop('disabled', true);
                    alert("Please select gatepass type");
                    $("#gatepass_type").focus();
                    return false;
                }

                else if(wh_name == ""){
                   // $('#sto_no').prop('disabled', true);
                    alert("Please select warehouse");
                    $("#warehouselist").focus();
                    return false;
                }

                else if(wh_name == "" || gp_type == ''){
                    //$('#sto_no').prop('disabled', true);
                    alert("Please select gatepass type and warehouse!");
                    $("#gatepass_type").focus();
                    $("#warehouselist").focus();
                    return false;
                }

                return false;
            }
    });



    $('#other_charges').keyup(function () {     
          this.value = this.value.replace(/[^0-9\.]/g,'');
       });

    // $('#sec_cust_name').keypress(function(event) {     
    //       var inputValue = event.which;
    //       if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0) && inputValue != 8) { 
    //         event.preventDefault();
    //        }
    //    });
        
    $('#get_invoice_details').click(function () { 
            var sto_length = $("#po_ro_tab_div").length;  
            var sto_no = $("#sto_no").val();
            var sto_date = $("#sto_date").val();
            var sup_id = $("#supplier_id").val();

            if(sup_id==''){
                alert("Please Select Supplier");
                return false;
            }
    
// Added by Anil start 
    $('#submit-sale').click(function(event){
            if($('#rcpt_dt').val() < $('#sto_date').val() ){
                alert('Receipt date should be greater than or equal from Invoice date');
                $('#rcpt_dt').focus();   
                return false;          
            } 
    });

            if($('#rcpt_dt').val() < $('#sto_date').val()){
                alert('Receipt date should be greater than or equal from Invoice date');
                $('#rcpt_dt').focus();   
                return false;          
            }
// Added by Anil end
            var sto_flag = 0;
            if(sto_length>=1){
                $.each($('.sto_list'), function(index, attr) {                   
                    var sto = $(this).attr('vals');

                    if(sto===sto_no){
                        sto_flag++;
                    }                   
                });
            }
           
          if(sto_flag==0){
                $.ajax({
                    url: site.base_url+"gatepass/getStoProducts",
                    async: false,
                    type: "POST",
                    data: {"sto_no":sto_no,"sto_date":sto_date},
                    dataType:"json",
                    success: function(data) {      

                      if(data && data.length>0){
                        add_product_tab(data,sto_no,sto_date);  
                        $("#supplier_id").prop( "disabled", true );
                        $("#warehouselist").prop("disabled", true);
                        $("#gatepass_type").prop( "disabled", true );  
                      } else {
                        $("#supplier_id").prop( "disabled", false );
                        $("#warehouselist").prop("disabled", false);
                        $("#gatepass_type").prop( "disabled", false ); 
                        alert("Please Select STO/RO No.")
                          return false;
                      } 
                        //console.log(data);
                        /*if(data == '1')
                        {
                            alert("Product Not Found against Selected STO/RO No.")
                           return false;
                        }
                         if(data && data.length>0){
                          add_product_tab(data,sto_no,sto_date);
                          //$("#supplier_id").prop( "disabled", true ); 
                         // $("#gatepass_type").prop( "disabled", true ); 
                         // $("#warehouselist").prop( "disabled", true ); 
                        }else{ 
                          $("#supplier_id").prop( "disabled", false );    
                          $("#gatepass_type").prop( "disabled", false );    
                          $("#warehouse_name").prop( "disabled", false );                         
                          alert("Please Select STO/RO No.")
                           return false;
                         }   */                                       
                    }
                })
        }else{
            alert("This STO/RO No. is already selected.")
            return false;

        }
    }); 
        
    $("#gatepass-form_out").validate({
         ignore: [],
          rules: {
             other_charges: {
                required : true,
                 decimalchecktest:true,
                checkChargeVal : true 
            },
            actual_qty: {
                required : true,
                decimalchecktest: 'Please enter valid format',
                checkChargeVal : true           
                
            },

             gatepass_type: {
                required : true
                 
            },
            warehouse_name: {
                required : true
                 
            },
            sec_cust_name: {
                required : true
                 
            },
            rcpt_date:{
                required : true
            },

             other_charges: {
                required : true,
                decimalchecktest:true,
                checkChargeVal : true 
            },

            supplier_id:{
                required : true               
            },

            lr_name:{
                required : true               
            }
            
        },
        messages: {
             gatepass_type: {
                required : "Please select a Gatepass type"                 
            },
            actual_qty : {
                required : "Please enter actual quantity"
            },
            warehouse_name: {
                required : "Please select a Warehouse"
                 
            },
            sec_cust_name: {
                required : "Please enter customer name",                 
            },
            rcpt_date:{
                required : "Please enter the receipt date"
            },
            other_charges : {
                required : "Please enter other charges",
                decimalchecktest: 'Please enter valid format',
                checkChargeVal : "Please enter other charges"
            },
            supplier_id:{
                required : "Please select a Supplier"
            },
            lr_name:{
                required : "Please select a Transporter name"
            }           
        },


        errorElement: "em", 
        submitHandler:function(form){  
            if($('#po_ro_tab_div').children().length < 1){
                alert("Please fill STO/RO details");
                return false;
            }
            var flag=0; var flag1=0;            
             $('.item_id').each(function(){                 
                  var parent_obj = $(this).closest('tr').attr('id');
                  var id= parseFloat($("#"+parent_obj).find('.item_id').val());
                  var avlqty= parseFloat($("#"+parent_obj).find('.avail_qty').val());
                  var qtys = 0;
                   $('.actual_qty').each(function(){                       

                        var parent_obj = $(this).closest('tr').attr('id');
                        var qty= parseFloat($("#"+parent_obj).find('.actual_qty').val());
                        var itemid= parseFloat($("#"+parent_obj).find('.item_id').val());
                          
                        if(id == itemid)
                        {
                            qtys = qtys + qty;
                
                        }
                       // alert('Total==>'+qtys+ 'Actual Qty==>'+qty);
                    
                        if(qtys > avlqty)
                        {
                           alert("Filled QTY Not Available. It May Be Used In Another Invoice Already");
                           parseFloat($("#"+parent_obj).find('.actual_qty').val(''));
                           parseFloat($("#"+parent_obj).find('.actual_qty_new').val(''));
                           flag1=1;
                           return false;
                       }

                     });
                    

                   });

                   $('.actual_qty').each(function(){
                        var parent_obj = $(this).closest('tr').attr('id');
                        var actual_qty1= parseFloat($("#"+parent_obj).find('.actual_qty').val());
                        if((actual_qty1!='') && (!isNaN(actual_qty1)) && (actual_qty1!='0.00'))
                        {
                           flag++;
                        }

                    });
                  $("#supplier_id").prop( "disabled", false );
                  $("#warehouselist").prop("disabled", false);
                  $("#gatepass_type").prop( "disabled", false );   
                  if((flag >= 1) && (flag1 != 1))
                        {
                            form.submit();
                        }
                     else {
                         if(flag1 == 0){
                             alert("Please enter details for at least one item");
                              return false; 
                          }
                          return false; 
                     }   
                    
           
           // form.submit();
        }         
    });
    
      $.validator.addMethod("checkChargeVal",
        function(value, element) {

            if(parseInt(value)>=0)
                return true;
            else 
                return false;
    });
    
    
$.validator.addMethod("decimalchecktest",
        function(value, element) {
        return /^[0-9]\d*(\.\d+)?$/.test(value);
    }); 
});

function add_product_tab(product_array, ro_no, ro_date){
    //console.log(product_array);
    var j = 0;
    var div_html="";
    div_html+= '<div class="col-sm-12 po_ro_tab " id="popopo">';
    div_html+= '<div style="background-color:#faebcc;float:left;height:45px;width:100%;border-radius:5px;line-height:45px;"><div class="col-sm-12 remove text-right" style="cursor:pointer;color:#000000;float:right;width:20px;line-height:23px;">X</div>';  
    div_html+= '<div class="col-sm-6 text-left" style="color:#000000"><?= lang('item_details')?><labal> ('+product_array[j].emrs_no+'-'+product_array[0].doc_no_src_id+')</labal></div></div>';
    div_html+= '<div class="row" style="margin-bottom: 15px;">';
    div_html+= '<div class="col-md-12">';
    div_html+= '<div class="box-content" style="border:1px solid #DDDDDD;border-radius:2px;">';
    div_html+= '<div class="row">';
        div_html+= '<div class="col-sm-12">';
            div_html+= '<div class="table-responsive"><input type="hidden" name="date[]" id="date" class="form-control" value="'+ro_date+'" />';  
            div_html+= '<div class="table-responsive"><input type="hidden" name="rono[]" id="rono" class="form-control" value="'+product_array[0].doc_no_src_id+'" />';
            div_html+= '<div class="table-responsive"><input type="hidden" name="doc_id[]" id="docid" class="form-control" value="'+product_array[0].doc_id+'" />';               
                div_html+= '<table class="table items table-striped table-bordered table-condensed table-hover">';
                    div_html+= '<thead>';
                        div_html+= '<tr>';
                        div_html+= '<th class="col-md-2"><?= lang("item_name"); ?></th>';                       
                        div_html+= '<th class="col-md-2"><?= lang("Unit"); ?></th>';
                        div_html+= '<th class="col-md-1"><?= lang("Request QTY"); ?></th>';
                        div_html+= '<th class="col-md-1"><?= lang("Remains QTY"); ?></th>';
                        div_html+= '<th class="col-md-1"><?= lang("Available QTY"); ?></th>';
                        div_html+= '<th class="col-md-1"><?= lang("Release QTY"); ?></th>';
                        div_html+= '<th class="col-md-1"><?= lang("Short QTY"); ?></th>';
                        div_html+= '<th class="col-md-1"><?= lang("rate"); ?></th>';
                        div_html+= '<th class="col-md-1"><?= lang("amount"); ?></th>';
                       // div_html+= '<th class="col-md-1"><?= lang("actions"); ?></th>';
                        div_html+= '</tr>';
                    div_html+= '</thead>';
                    div_html+= '<tbody>';
                    if(product_array.length>0){
                        for(var i in product_array){
                          if(product_array[i].available_stk > 0)
                          {
                        //console.log(product_array[i]);
                        var dlv_qty = parseFloat(product_array[i].dlv_qty).toFixed(2);  
                          //  alert(parseFloat(dlv_qty).toFixed(2))
                            div_html+= '<tr id="prod_'+product_array[i].id+'">';
                            div_html+= '<td>'+product_array[i].name+'';
                            //div_html+= '<td>'+product_array[i].itm_uom_bs+'';         
                            div_html+= '<td>'+product_array[i].unit+'';
                            div_html+= '<input type="hidden" name="item_list['+sto_no+'][]" class="form-control" value="'+product_array[i].id+'" />'; 
                            div_html+= '<input type="hidden" name="org_id['+sto_no+'][]" class="form-control" value="'+product_array[i].org_id+'" />'; 

                            //div_html+= '<input type="hidden" name="wh_id['+sto_no+'][]" class="form-control" value="'+product_array[i].wh_id+'" />'; 
                            div_html+= '<input type="hidden" name="wh_id['+sto_no+']"  value="'+product_array[0].wh_id+'"/>';
                            div_html+= '<input type="hidden" name="segment_id['+sto_no+'][]" class="form-control" value="'+product_array[i].segment_id+'" />'; 

                            div_html+= '<input type="hidden" name="sto_list['+product_array[i].sto_no+'][]" class="form-control sto_list" vals = "'+product_array[i].doc_id+'-'+product_array[i].doc_no_src_id+'" value="'+product_array[i].doc_id+'" />';

                            div_html+= '<input type="hidden" name="sto_list123['+product_array[i].sto_no+'][]" class="form-control sto_list123" vals = "'+product_array[i].doc_id+'" value="'+product_array[i].doc_no_src_id+'" />';

                            div_html+= '<input type="hidden" name="code['+product_array[i].sto_no+']['+product_array[i].id+']"  value="'+product_array[i].code+'"/>';


                          div_html+= '<input type="hidden" name="sto_date_list['+sto_no+'][]" class="form-control" value="'+sto_date+'" />'; 

                          div_html+= '<td><input type="text" name="sreq_qty['+product_array[i].sto_no+']['+product_array[i].id+']"  class="form-control r_req_qty" value="'+parseFloat(product_array[i].Rqty).toFixed(2)+'" onkeypress="return isNumberKey(event,this)" readonly="readonly" required="required" /></td>';
                          
                          div_html+= '<td><input type="text" name="req_qty['+product_array[i].sto_no+']['+product_array[i].id+']"  class="form-control req_qty" value="'+parseFloat(product_array[i].bal_qty).toFixed(2)+'" onkeypress="return isNumberKey(event,this)" readonly="readonly" required="required" /></td>';

                            div_html+= '<td><input type="text" name="avail_qty['+product_array[i].sto_no+']['+product_array[i].id+']"  class="form-control avail_qty" value="'+parseFloat(product_array[i].available_stk).toFixed(2)+'" onkeypress="return isNumberKey(event,this)" readonly="readonly" required="required" /></td>';

                            div_html+= '<td><input type="hidden" name="actual_qty_new['+product_array[i].doc_no_src_id+']['+product_array[i].id+']" id="actual_qty_new'+product_array[i].id+'" class="form-control show-decimal actual_qty_new" />';

                            div_html+= '<input type="text" name="actual_qty['+product_array[i].sto_no+']['+product_array[i].id+']" id="actual_qty_'+product_array[i].id+'" at="actual_qty" class="form-control show-decimal actual_qty" onkeypress="return isNumberKey(event,this)" /></td>';
                            
                             div_html+= '<td><input type="text" name="short_qty['+product_array[i].sto_no+']['+product_array[i].id+']" id="short_qty_'+product_array[i].id+'" at="short_qty" class="form-control  short_qty" onkeypress="return isNumberKey(event,this)" readonly="readonly"/></td>';
                            
                            // div_html+= '<td><input type="text" name="rate['+product_array[i].sto_no+']['+product_array[i].id+']" id="rate_'+product_array[i].id+'" class="form-control rate " readonly="readonly" onkeypress="return isNumberKey(event,this)" value="'+parseFloat(product_array[i].itm_price_bs).toFixed(2)+'" /></td>';   

                            div_html+= '<td><input type="text" name="rate['+product_array[i].sto_no+']['+product_array[i].id+']" id="rate_'+product_array[i].id+'" class="form-control rate " readonly="readonly" onkeypress="return isNumberKey(event,this)" value="'+parseFloat(product_array[i].price).toFixed(2)+'" /></td>';                        
                            
                            // div_html+= '<td><input type="text" name="amount['+product_array[i].sto_no+']['+product_array[i].id+']" class="form-control amount " readonly="readonly" onkeypress="return isNumberKey(event,this)" value="'+parseFloat(product_array[i].itm_amt_bs).toFixed(2)+'"/></td>';
                            div_html+= '<td><input type="text" name="amount['+product_array[i].sto_no+']['+product_array[i].id+']" class="form-control amount " readonly="readonly" onkeypress="return isNumberKey(event,this)" value="'+parseFloat((product_array[i].price * product_array[i].available_stk)).toFixed(2)+'"/></td>';
                            
                            // div_html+= '<input type="hidden" name="item_uom['+sto_no+']['+product_array[i].id+']"  value="'+product_array[i].itm_uom_bs+'"/>';
                            div_html+= '<input type="hidden" name="item_uom['+product_array[i].sto_no+']['+product_array[i].id+']"  value="'+product_array[i].unit+'"/>';
                            
                            div_html+= '<input type="hidden" class="item_id" name="item_id['+product_array[i].sto_no+']['+product_array[i].id+']"  value="'+product_array[i].item_id+'"/></td>';


                            //div_html+= '<td><a id="linkTo" href="<?php echo site_url();?>/gatepass/stockout/dashboard?id='+product_array[i].item_id+'&qty='+product_array[i].available_stk+'&sto_no='+product_array[i].doc_no_src_id+'" data-toggle="modal" data-target="#myModal" >Stock Out</a>';

                             //div_html+= '<input type="hidden" name="lotdata['+product_array[i].doc_no_src_id+']['+product_array[i].item_id+']" id="lotdata['+item_id+']"  value="" class="lotdata"/></td>';

                            div_html+= '</tr>';
                        }
                      }
                    }   j++;
                    
                    div_html+= '</tbody>';
                div_html+= '</table>';
            div_html+= '</div>';
        div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';
    div_html+= '</div>';

    $('#po_ro_tab_div').append('<div>'+div_html+'</div>');
}

function isNumberKey(e, t) {
        try {
            if (window.event) {
                var charCode = window.event.keyCode;
            }
            else if (e) {
                var charCode = e.which;
            }
            else { return true; }

            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        catch (err) {
            alert(err.Description);
        }
}
   
</script>
<?php
$attrib = array('data-toggle' => 'validator', 'role' => 'form','id'=>'gatepass-form_out');
echo form_open_multipart("gatepass/out", $attrib)
?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('gatepass_out'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?php echo lang('enter_info'); ?></p>
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-md-4">
                            <div class="form-group required">
                                <?= lang("gatepass_type", "gt"); ?>
                                <br>                                
                                <?php
                                $gp[''] ='';
                                $gp1[''] = '';
                                $gp1[1081] = 'External Material Out';
                                foreach ($gatepass_type as $gatepass_type){
                                    $gp[$gatepass_type->type_id] = $gatepass_type->name;
                                }  
                                echo form_dropdown('gatepass_type', $gp1, (isset($_POST['gatepass_type']) ? $_POST['gatepass_type'] : $gatepass_type), 'id="gatepass_type" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("gatepass_type") . '" required="required" style="width:100%;" ');                                
                                ?>                        
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group required">
                              <?= lang("warehouse", "warehouse"); ?>
                              <?php
                              $arrWarehouses = array();
                              $arrWarehouses['']= 'Select warehouse';
                              echo form_dropdown('warehouse_name', $arrWarehouses, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ''), 'id="warehouselist" class="form-control input-tip" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" style="width:100%;" ');
                              ?>
                            </div>
                        </div>        
                        <div class="col-md-4">
                            <div class="form-group required">
                                <?= lang("supplier", "supplier"); ?>
                                <br>
                               <?php                              
                                $sup[''] = '';
                                //print_r($supplier_details);
                                foreach ($supplier_details as $sups) {
                                    $sup[$sups->eo_id] = $sups->name;
                                }
                                echo form_dropdown('supplier_id', $sup, $sup[0], 'id="supplier_id" class="form-control required" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("supplier") . '"  style="width:100%;" ');                  
                                ?>
                               
                            </div>
                        </div>                        
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">
                    <div class="panel panel-warning">
                        <div class="panel-heading"><?= lang('transpoter_details') ?></div>
                        <div class="panel-body" style="padding: 5px;">                                              
                            <div class="col-md-4">
                                <div class="form-group required">
                                    <?= lang("Name", "Name"); ?>
                                    <?php                         

                                    $tp[''] = '';
                                  
                                    foreach ($tp_list as $val) {
                                        $tp[$val->eo_id] = $val->name;
                                    }
                                    echo form_dropdown('lr_name', $tp, $tp[0], 'id="lr_name" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("Transporter") . '" required="required" style="width:100%;" ');
                                    ?>
                                </div>
                                
                            </div>                        
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("lr_no", "lr_no"); ?>
                                    <?php echo form_input('lr_no', (isset($_POST['lr_no']) ? $_POST['lr_no'] : $lr_no), 'class="form-control" id="lr_no"'); ?>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("lr_date", "lr_date"); ?>
                                    <?php echo form_input('lr_date', (isset($_POST['lr_date']) ? $_POST['lr_date'] : $lr_date), 'class="form-control input-tip" id="lr_date" readonly="readonly" placeholder="LR Date"'); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>                            

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("vehicle_no", "vehicle_no"); ?>
                                    <?php echo form_input('vehicle_no', (isset($_POST['vehicle_no']) ? $_POST['vehicle_no'] : $vehicle_no), 'class="form-control" id="vehicle_no"'); ?>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group required">
                                    <?= lang("other_charge", "other_charge"); ?>
                                    <?php echo form_input('other_charges', (isset($_POST['other_charges']) ? $_POST['other_charges'] : $other_charges), 'class="form-control input-tip" id="other_charges"'); ?>
                                </div>
                            </div>                            

                            <div class="col-md-4">
                              <div class="form-group required">
                                  <?= lang("rcpt_dt", "rcpt_dt"); ?>
                                  <?php echo form_input('rcpt_date', (isset($data['rcpt_date']) && $data['rcpt_date']!='1970-01-01' ? $data['rcpt_date'] : $rcpt_date), 'class="form-control input-tip" id="rcpt_dt" readonly="readonly" placeholder="'.lang('rcpt_dt').'"'); ?>
                              </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <?= lang("remarks", "remarks"); ?>
                                    <?php echo form_textarea('remarks', (isset($_POST['remarks']) ? $_POST['remarks'] : $remarks), 'class="form-control input-tip" id="remarks"'); ?>
                                </div>
                            </div>                            
                        </div>    
                    </div>
                </div>
                <div class="clearfix"></div>             
                <div class="col-md-12">
                    <div class="panel panel-warning">
                        <div class="panel-heading"><?= lang('STO/RO DETAILS') ?></div>
                        <div class="panel-body" style="padding: 5px;">  
                            <div class="col-md-4">                            
                                <div class="form-group">
                                    <?= lang("STO/RO Details", "STO/RO Details"); ?> 
                                        <select name="sto_no" id="sto_no" class="form-control" style="width:100%">
                                            <option value="" selected="selected">Select STO/RO No.</option>
                                        </select>
                                </div>
                            </div>                        
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("STO/RO Date", "STO/RO Date"); ?>
                                    <?php echo form_input('sto_date', (isset($_POST['sto_date']) ? $_POST['sto_date'] : $sto_date), 'class="form-control input-tip" id="sto_date" readonly="readonly" placeholder="STO/RO Date"');                                 
                                    ?>                                    
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group required">
                                    <?= lang("sec_cust_name", "sec_cust_name"); ?>
                                    <?php echo form_input('sec_cust_name', (isset($_POST['sec_cust_name']) ? $_POST['sec_cust_name'] : $sec_cust_name), 'class="form-control input-tip"  id="sec_cust_name"placeholder="Secondary Customer Name"'); ?>
                                </div>
                            </div>                        
                        </div>
                    </div>
                </div> 
                <div class="clearfix"></div>   
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-md-4">
                            <div class="from-group">
                                <?php echo form_button('get_invoice_details', $this->lang->line("get_invoice_details"), 'id="get_invoice_details" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                            </div>
                        </div>
                        <div id="po_ro_tab_div"></div>
                    </div>
                </div>                
            </div>
            <div class="col-md-12">
                <div class="from-group">
                    <button class="btn btn-success" name="submit-sale" value="1" id="submit-sale"><?= lang('submit'); ?></button>   
                   <!--  <button class="btn btn-primary" id="submit-hold" name="submit-sale" value="2"><?= lang('Hold'); ?></button>    -->             
                    <button type="button" class="btn btn-danger" onclick="location.href = '<?php echo base_url()?>';"><?= lang('Cancel') ?></button>                    
                </div>
            </div>
        </div>   
    </div>

        </div>
    </div>
</div>
<?php echo form_close(); ?>





 
