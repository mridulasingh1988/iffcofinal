  <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <link rel="shortcut icon" href="<?= $assets ?>images/icon.png"/>
    <link rel="stylesheet" href="<?= $assets ?>styles/theme.css" type="text/css"/>
    <link rel="stylesheet" href="<?= $assets ?>styles/style.css" type="text/css"/>
    <link rel="stylesheet" href="<?= $assets ?>pos/css/posajax.css" type="text/css"/>
    <link href="<?= $assets ?>styles/bootstrap-glyphicons.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= $assets ?>pos/css/print.css" type="text/css" media="print"/>
<script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="<?= $assets ?>js/jquery-migrate-1.2.1.min.js"></script>
    
    <script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/perfect-scrollbar.min.js"></script>

<script type="text/javascript" src="<?= $assets ?>js/select2.min.js"></script> 

<script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/jquery.calculator.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/additional-methods.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>pos/js/plugins.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>pos/js/parse-track-data.js"></script>

<style>
.form-group label {
    float: left;
}
.form-group{
    margin-top: 15px;
}
</style>

<?php
$attrib = array('data-toggle' => 'validator', 'role' => 'form','id'=>'provision-form');
echo form_open_multipart("pos/edit_prov_list/".$sale_id."", $attrib)
?>
<input type="hidden" name="submit-type" id="submit-type" value="" /> 
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('provisional_sales'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?php echo lang('enter_info'); ?></p>                
                <div class="row">
                    <div class="col-lg-12">
                         <div class="panel panel-warning">
                            <div class="panel-heading"><?= lang('basic_details') ?></div>

                                <div class="panel-body" style="padding: 5px;">
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <span>
                                                <?= lang("reference_no", "reference_no"); ?> &nbsp;
                                            </span>
                                            <span>
                                             <?php echo $saleData[0]->reference_no?>
                                            </span>
                                        </div>                                       
                                    </div>
                               
                     
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <?= lang("trans_date"); ?> &nbsp;
                                            <?php echo $saleData[0]->date?>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-warning">
                            <div class="panel-heading"><?= lang('item_details') ?></div>
                            <table width="100%" cellpadding="0" cellspacing="0" class="table table-horizontal product-table">
                                <thead>
                                    <tr>
                                        <td valign="center">Product Code</td>
                                        <td valign="center">Product Name</td>
                                        <td valign="center">Quantity</td>
                                        <td valign="center">Unit Price</td>
                                        <td valign="center">Tax</td>
                                        <td valign="center">Total</td>
                                        <?php if($cheq_stat==1){
                                        ?> 
                                        <td valign="center">Action</td>
                                        <?php
                                        }
                                        ?>
                                    </tr>
                                </thead>
                                <?php foreach($itemData as $key=>$row){                               
                                ?>
                                <tr id="row_<?php echo $row->product_id?>_<?php echo $row->id?>" class="prod_row">
                                    <td valign="center"><span class="product_code"><?php echo $row->product_code?></span></td>
                                    <td valign="center"><?php echo $row->product_name?></td>
                                     <td valign="center"><input type="text" class="form-control text-center allownumericwithoutdecimal quantity" readonly id="quantity_<?php echo $row->product_id?>_<?php echo $row->id?>" value="<?php echo $row->quantity?>" name="quantity[<?php echo $row->id?>]" /></td>
                                    <td valign="center"><span class="unit_price"><?php echo $row->net_unit_price?></span></td>
                                    <td valign="center"><span class="tax"><?php echo $row->item_tax?></span></td>
                                    <td valign="center"><span class="subtotal"><?php echo $row->subtotal?></span>
                                        <!-- <input type="text" id="amount_<?php echo $key?>" value="<?php echo $row->subtotal?>" /> -->
                                        <input type="text" name="subtotal[<?php echo $row->id?>]" id="subtotal_<?php echo $row->product_id?>_<?php echo $row->id?>" value="<?php echo $row->subtotal?>" class="subtotalamount" />
                                        <input type="text" name="sale_item_id[]" value="<?php echo $row->id?>" />
                                    </td>
                                    <?php if($cheq_stat==1){

                                    ?>                                   
                                    <td valign="center">
                                        <?php if($row->quantity>0){                                            
                                        ?>
                                        <a href="javascript:void(0)" class="edit">Edit</a>&nbsp;&nbsp;
                                        <?php
                                        }
                                         if(($key!=0) && ((int)$row->quantity>0)){
                                        ?>                                        
                                        <a href="javascript:void(0)" id="<?php echo $row->product_id?>_<?php echo $row->id?>" class="deleteprod">Delete</a>
                                        <?php
                                        }else{
                                            ?>
                                         <a href="javascript:void(0)" id="<?php echo $row->product_id?>_<?php echo $row->id?>" class="deleteprod">Delete</a>
                                        <?php
                                        }
                                        ?>
                                    </td>
                                    <?php
                                    }
                                    ?>
                                </tr>
                                <?php 
                                }     
                                ?>                           
                            </table>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="panel panel-warning">
                            <div class="panel-heading"><?= lang('cheque_details') ?></div>
                            <table width="100%" cellpadding="0" cellspacing="0" class="table table-horizontal">
                                <thead>
                                    <tr>
                                        <td valign="center">Cheque no.</td>
                                        <td valign="center">Cheque Date</td>
                                        <td valign="center">Customer Name</td>
                                        <td valign="center">Bank Name</td>
                                        <td valign="center">Amount</td>
                                        <td valign="center">Status</td>
                                        <td valign="center">Action</td>
                                    </tr>
                                </thead>
                                <?php foreach($chequeData as $row){
                                    if($row->status==1)
                                        $cheq_status = 'Cleared';
                                    else if($row->status==2)
                                        $cheq_status = 'Bounced';
                                    else
                                        $cheq_status = 'Pending';
                                ?>
                                <tr>
                                    <td valign="center"><?php echo $row->cheque_no?></td>
                                    <td valign="center"><?php echo $row->cheque_date?></td>
                                    <td valign="center"><?php echo $row->customer_name?></td>
                                    <td valign="center"><?php echo $row->bank_name?></td>
                                    <td valign="center"><?php echo $row->amount?></td>
                                    <td valign="center"><?php echo $row->status==1?'Cleared':'Pending'?></td>
                                    <td valign="right"><?php echo $cheq_status?>
                                    <input type="checkbox" <?php echo $row->status==1?'checked':'' ?> name="cheque_name[<?php echo $row->id?>]" /></td>
                                </tr>
                                <?php 
                                }     
                                ?>                           
                            </table>
                        </div>
                    </div>
                </div>           

                <div class="col-md-12">
                    <div class="from-group">
                        <button type="button" class="btn btn-primary" id="update-stock"><?= lang('Update Stock'); ?></button>
                                   
                        <button type="button"  class="btn btn-primary" onclick="location.href = '<?php echo base_url()?>';"><?= lang('Cancel') ?></button>
                    </div>
                </div>
            </div>   
            
        </div>

    </div>
</div>
<div id="payment-con">
    <?php for ($i = 1; $i <= 5; $i++) { ?>
        <input type="hidden" name="amount[]" id="amount_val_<?= $i ?>" value=""/>
        <input type="hidden" name="balance_amount[]" id="balance_amount_<?= $i ?>" value=""/>
        <input type="hidden" name="paying_amount[]" id="paying_amount_<?= $i ?>" value=""/>
        <input type="hidden" name="paid_by[]" id="paid_by_val_<?= $i ?>" value="cash"/>
        <input type="hidden" name="cc_no[]" id="cc_no_val_<?= $i ?>" value=""/>
        <input type="hidden" name="paying_gift_card_no[]" id="paying_gift_card_no_val_<?= $i ?>" value=""/>
        
         <input type="hidden" name="paying_credit_voucher_no[]" id="paying_credit_voucher_no_val_<?= $i ?>" value=""/>
         
        <input type="hidden" name="cc_holder[]" id="cc_holder_val_<?= $i ?>" value=""/>
        <input type="hidden" name="cheque_no[]" id="cheque_no_val_<?= $i ?>" value=""/>
        <input type="hidden" name="cheque_date[]" id="cheque_date_val_<?= $i ?>" value=""/>
        <input type="hidden" name="rtgs_no[]" id="rtgs_no_val_<?= $i ?>" value=""/>
        <input type="hidden" name="cc_month[]" id="cc_month_val_<?= $i ?>" value=""/>
        <input type="hidden" name="cc_year[]" id="cc_year_val_<?= $i ?>" value=""/>
        <input type="hidden" name="cc_type[]" id="cc_type_val_<?= $i ?>" value=""/>
        <input type="hidden" name="cc_cvv2[]" id="cc_cvv2_val_<?= $i ?>" value=""/>
        <input type="hidden" name="payment_note[]" id="payment_note_val_<?= $i ?>"
         value=""/>
        <input type="hidden" name="customer_name[]" id="customer_name_<?= $i ?>" value=""/>
        <input type="hidden" name="bank_name[]" id="bank_name_<?= $i ?>" value=""/> 
        <input type="hidden" name="pcheque_no[]" id="pcheque_no_val_<?= $i ?>" value=""/>
        <input type="hidden" name="pcheque_date[]" id="pcheque_date_val_<?= $i ?>" value=""/>                               
    <?php } ?>
</div>
</div>



<?php echo form_close(); ?>



<div class="modal fade in" id="paymentModals" tabindex="-1" role="dialog" aria-labelledby="payModalLabel"
     aria-hidden="true">

  <!--added by vikas singh 12-10-2016 -->
     <?php $attribs = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'pos-payment-form');
                    echo form_open("", $attribs);
                   ?>
  
   <!--end added by vikas singh 12-10-2016 -->
   
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="payModalLabel"><?= lang('finalize_sale'); ?></h4>
            </div>
            <div class="modal-body" id="payment_content">
                <div class="row">
                    <div class="col-md-10 col-sm-9">
                        <?php if ($Owner || $Admin) { ?>
                            <div class="form-group">
                                <?= lang("biller", "biller"); ?>
                                <?php
                                foreach ($billers as $biller) {
                                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                }
                                echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $pos_settings->default_biller), 'class="form-control" id="posbiller" required="required"');
                                ?>
                            </div>
                        <?php } else {
                            $biller_input = array(
                                'type' => 'hidden',
                                'name' => 'biller',
                                'id' => 'posbiller',
                                'value' => $this->session->userdata('biller_id'),
                            );

                            echo form_input($biller_input);
                        }
                        ?>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <?= form_textarea('sale_note', '', 'id="sale_note" class="form-control kb-text skip" style="height: 100px;" placeholder="' . lang('sale_note') . '" maxlength="250"'); ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= form_textarea('staffnote', '', 'id="staffnote" class="form-control kb-text skip" style="height: 100px;" placeholder="' . lang('staff_note') . '" maxlength="250"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="clearfir"></div>
                        <div id="payments">
                            <div class="well well-sm well_1">
                                <div class="payment">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <?= lang("amount", "amount_1"); ?>

                                                <input name="amountc[0]" type="text" id="amount_1"

                                                       class="pa form-control amount fillamount"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 col-sm-offset-1">
                                            <div class="form-group">
                                                <?= lang("paying_by", "paid_by_1"); ?>
                                                <select name="paid_by1[0]" id="paid_by_1" class="form-control paid_by">
                                                  <option value="cash" selected="selected"><?= lang("cash"); ?></option>
                                                  <option value="dd"><?= lang("dd"); ?></option>
                                                  <option value="rtgs"><?= lang("RTGS"); ?></option>
                                                  <option value="cc"><?= lang("cc"); ?></option>
                                                  <option value="pos"><?= lang("pos_machine"); ?></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-11">
                                            <div class="form-group gc_1" style="display: none;">
                                                <?= lang("gift_card_no", "gift_card_no_1"); ?>

                                                <input name="cpaying_gift_card_no[0]" type="text" id="gift_card_no_1"
                                                       class="pa form-control kb-pad gift_card_no" required="required"/>


                                                <div id="gc_details_1"></div>
                                            </div>
                                            

                                            <div class="form-group cv_1" style="display: none;">
                                                <?= lang("credit_voucher_no", "credit_voucher_no_1"); ?>
                                                <input name="cpaying_credit_voucher_no[0]" type="text" id="credit_voucher_no_1"
                                                       class="pa form-control kb-pad credit_voucher_no" required="required"/>


                                                <div id="cv_details_1"></div>
                                            </div>
                                            
                                            <div class="pcc_1" style="display:none;">
                                               <!--  <div class="form-group">
                                                    <input type="text" name="swipe[0]" id="swipe_1" class="form-control swipe"
                                                           placeholder="<?= lang('swipe') ?>"/>
                                                </div> -->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">

                                                      <input name="ccc_no[0]" type="text" id="pcc_no_1"
                                                             class="form-control cardno" required="required" maxlength="20"

                                                                   placeholder="<?= lang('cc_no') ?>"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">


                                                            <input name="ccc_holer[0]" type="text" id="pcc_holder_1"
                                                                   class="form-control cc_holder" required="required"

                                                                   placeholder="<?= lang('cc_holder') ?>"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">

                                                            <select name="ccc_type[0]" id="pcc_type_1"
                                                                    class="form-control pcc_type" required="required"

                                                                    placeholder="<?= lang('card_type') ?>">
                                                                <option value="Visa"><?= lang("Visa"); ?></option>
                                                                <option
                                                                    value="MasterCard"><?= lang("MasterCard"); ?></option>
                                                                <option value="Amex"><?= lang("Amex"); ?></option>
                                                                <option
                                                                    value="Discover"><?= lang("Discover"); ?></option>
                                                            </select>
                                                            <!-- <input type="text" id="pcc_type_1" class="form-control" placeholder="<?= lang('card_type') ?>" />-->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">

                                                            <?php 
                                                                 $months = array('' => 'Month',
                                                                 '01'  => '01',
                                                                 '02'  => '02',
                                                                 '03'  => '03',
                                                                 '04'  => '04',
                                                                 '05'  => '05',
                                                                 '06'  => '06',
                                                                 '07'  => '07',
                                                                 '08'  => '08',
                                                                 '09'  => '09',
                                                                 '10' => '10',
                                                                 '11' => '11',
                                                                 '12' => '12'
                                                                );
                                                                 
                                                               echo form_dropdown('ccc_month[0]',$months,$current_month, 'id="pcc_month_1"','class="form-control monthList" required="required"','style="width:100%;"');
                                                                ?>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <?php

                                                             $days[''] = 'Day';         
                                                             for($i=1;$i<=31;$i++){
                                                                $days[$i] = $i;
                                                             }

                                                            $start_year = date("Y");

                                                             $years[''] = 'Year';

                                                             for ($i=$start_year;$i<=date("Y")+20;++$i) {
                                                                $years[$i] = $i;
                                                             } 

                                                             echo form_dropdown('ccc_year[0]',$years,'','id="pcc_year_1" class="form-control yearList"','required="required"');
                                                                ?>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">

                                                            <input name="ccc_cvv2[]" type="text" id="pcc_cvv2_1"
                                                                   class="form-control approval" required="required"
                                                                   placeholder="<?= lang('cvv2') ?>"/>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pcheque_1" style="display:none;">
                                                <div class="form-group"><?= lang("dd_no","dd_no_1"); ?>

                                                    <input name="ccheque_no[0]" type="text" id="cheque_no_1"
                                                           class="form-control cheque_no" maxlength="6"/>

                                                </div>
                                                <div class="form-group required">

                                                    <?= lang("date", "date"); ?>
                                                    <input name="ccheque_date[0]" type="text" id="cheque_date_1"
                                                           class="form-control ccheque_date"/>                                                    
                                                </div>
                                            </div>

                                            <div class="pcheques_1" style="display:none;">
                                                <div class="form-group"><?= lang("cheque_no", "cheque_no_1"); ?>
                                                    <input name="pcheque_no[0]" type="text" id="pcheque_no_1"
                                                           class="form-control cheque_nos" maxlength="6"/>

                                                </div>
                                                <div class="form-group required">
                                                    <?= lang("date", "date"); ?>
                                                    <input name="pcheque_date[0]" type="text" id="pcheque_date_1"
                                                           class="form-control date"/>                                      
                                                </div>

                                                <div class="form-group"><?= lang("customer_name", "customer_name_1"); ?>
                                                    <input name="customer_name[0]" type="text" id="customer_name_1"
                                                           class="form-control customer_name" />
                                                </div>

                                                <div class="form-group"><?= lang("bank_name", "bank_name_1"); ?>
                                                    <input name="bank_name[0]" type="text" id="bank_name_1"
                                                           class="form-control bank_name" />
                                                </div>
                                            </div>
                                            <div class="prtgs_1" style="display:none;">
                                                <div class="form-group"><?= lang("approval_no", "approval_no_1"); ?>
                                                    <input name="rtgs_no[0]" type="text" id="rtgs_no_1"
                                                           class="form-control rtgs_no" maxlength="6"/>
                                                </div>
                                             
                                            </div>
                                            <div class="form-group">
                                                <?= lang('payment_note', 'payment_note'); ?>
                                                <textarea name="payment_note[0]" id="payment_note_1"
                                                          class="pa form-control kb-text payment_note"></textarea>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="multi-payment"></div>
                        <button type="button" class="btn btn-primary col-md-12 addButton"><i
                                class="fa fa-plus"></i> <?= lang('add_more_payments') ?></button>
                        <div style="clear:both; height:15px;"></div>
                        <div class="font16">
                            <table class="table table-bordered table-condensed table-striped" style="margin-bottom: 0;">
                                <tbody>
                                <tr>
                                    <td width="25%"><?= lang("total_items"); ?></td>
                                    <td width="25%" class="text-right"><span id="item_count">0.00</span></td>
                                    <td width="25%"><?= lang("total_payable"); ?></td>
                                    <td width="25%" class="text-right"><span id="twt">0.00</span></td>
                                </tr>
                                <tr>
                                    <td><?= lang("total_paying"); ?></td>
                                    <td class="text-right"><span id="total_paying">0.00</span></td>
                                    <td><?= lang("balance"); ?></td>
                                    <td class="text-right"><span id="balance">0.00</span></td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-3 text-center">
                        <span style="font-size: 1.2em; font-weight: bold;"><?= lang('quick_cash'); ?></span>

                        <div class="btn-group btn-group-vertical">
                            <button type="button" class="btn btn-lg btn-info quick-cash" id="quick-payable">0.00
                            </button>
                            <?php
                            foreach (lang('quick_cash_notes') as $cash_note_amount) {
                                echo '<button type="button" class="btn btn-lg btn-warning quick-cash">' . $cash_note_amount . '</button>';
                            }
                            ?>
                            <button type="button" class="btn btn-lg btn-danger"
                                    id="clear-cash-notes"><?= lang('clear'); ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-block btn-lg btn-primary" id="submit-sale"><?= lang('submit'); ?></button>
            </div>
        </div>
    </div>

    <!--added by vikas singh 12-10-2016 -->
     <?php echo form_close(); ?>
     <!--end added by vikas singh 12-10-2016 -->

</div>

<script>
var total = 0,gtotal=0,invoice_tax=0,order_discount=0,count=0,total_paid=0,grand_total=0;
var cleared_cheque_amount = <?php echo $cleared_cheque_amount?>; 
 var pi = 'amount_1', pa = 2;
$(document).ready(function () {
    <?php for($i=1; $i<=5; $i++) { ?>
        $('#paymentModals').on('change', '#amount_<?=$i?>', function (e) {
            $('#amount_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModals').on('blur', '#amount_<?=$i?>', function (e) {
            $('#amount_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModals').on('select2-close', '#paid_by_<?=$i?>', function (e) {
            $('#paid_by_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModals').on('change', '#pcc_no_<?=$i?>', function (e) {
            $('#cc_no_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModals').on('change', '#pcc_holder_<?=$i?>', function (e) {
            $('#cc_holder_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModals').on('change', '#gift_card_no_<?=$i?>', function (e) {
            $('#paying_gift_card_no_val_<?=$i?>').val($(this).val());
        });
        
        $('#paymentModals').on('change', '#credit_voucher_no_<?=$i?>', function (e) {
            $('#paying_credit_voucher_no_val_<?=$i?>').val($(this).val());
        });
        
        $('#paymentModals').on('change', '#pcc_month_<?=$i?>', function (e) {
            $('#cc_month_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModals').on('change', '#pcc_year_<?=$i?>', function (e) {
            $('#cc_year_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModals').on('change', '#pcc_type_<?=$i?>', function (e) {
            $('#cc_type_val_<?=$i?>').val($(this).val());
        });
        $('#cc_type_val_<?=$i?>').val($("#pcc_type_<?=$i?>").val());
        $('#paymentModals').on('change', '#pcc_cvv2_<?=$i?>', function (e) {
            $('#cc_cvv2_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModals').on('change', '#cheque_no_<?=$i?>', function (e) {
            $('#cheque_no_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModals').on('change', '#payment_note_<?=$i?>', function (e) {
            $('#payment_note_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModals').on('change', '#cheque_date_<?=$i?>', function (e) {
            $('#cheque_date_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModals').on('change', '#rtgs_no_<?=$i?>', function (e) {
            $('#rtgs_no_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModals').on('change', '#pcheque_no_<?=$i?>', function (e) {
            $('#pcheque_no_val_<?=$i?>').val($(this).val());
        });          
        $('#paymentModals').on('change', '#pcheque_date_<?=$i?>', function (e) {
            $('#pcheque_date_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModals').on('change', '#customer_name_<?=$i?>', function (e) {
            $('#customer_name_<?=$i?>').val($(this).val());
        });          
        $('#paymentModals').on('change', '#bank_name_<?=$i?>', function (e) {
            $('#bank_name_<?=$i?>').val($(this).val());
        });


       /* $('input[name="ccc_month[<?php echo $i?>]"]').rules("add", {
            required:true,            
            cc_month:true ,
            cc_valid_month:true,
            messages:{
                 required: "expiration month can't be left empty",
                 cc_month:"CC expiry month should be between 01 and 12"
            }   
        });
      
         $('input[name="ccc_year[<?php echo $i?>]"]').rules("add", {
            required:true,             
            checkYear:true,
            cc_valid_year:true,
            messages:{
                 required: "expiration year can't be left empty",
                 checkYear:"CC expiry year >= current year "
            }
        });*/
    <?php } ?>
    $('#payment').click(function () {    
        
        var twt = formatDecimal((total + invoice_tax) - order_discount);
        if (count == 1) {
            bootbox.alert('<?= lang('x_total'); ?>');
            return false;
        }
        gtotal = formatDecimal(twt);
        <?php if($pos_settings->rounding) { ?>
        round_total = roundNumber(gtotal, <?php echo $pos_settings->rounding?>);
        var rounding = formatDecimal(0 - (gtotal - round_total));
        $('#twt').text(formatMoney(round_total) + ' (' + formatMoney(rounding) + ')');
        $('#quick-payable').text(round_total);
        <?php } else { ?>
        $('#twt').text(formatMoney(gtotal));
        $('#quick-payable').text(gtotal);
        <?php } ?>
        $('#item_count').text(count - 1);
        $('#paymentModals').appendTo("body").modal('show');
        $('#amount_1').focus();
    });

    $('.quantity').change(function () {
        var selected_id = $(this).attr('id');
        var parentObj = $(this).closest(".prod_row").attr("id");
        var ids = parentObj.split("_"); 
        var unit_price = $("#"+parentObj).find(".unit_price").text();        
        var product_code = $("#"+parentObj).find(".product_code").text();
        var unit_price = $("#"+parentObj).find(".unit_price").text();
        var item_tax = $("#"+parentObj).find(".tax").text();
        var product_price = parseFloat($(this).val()*(parseFloat(unit_price) + parseFloat(item_tax)));
        var subtotal = 0;
        var cleared_cheque_amount = <?php echo $cleared_cheque_amount?>;
       // var cleared_cheque_amount = 200000;
        $('.subtotalamount').each(function(i, obj) {        
            if($(this).attr('id')!='subtotal_'+ids[1]+'_'+ids[2]+''){
                subtotal += parseFloat($(this).val());
            }
        });
        subtotal += product_price;

        if(subtotal<cleared_cheque_amount){
            bootbox.alert('Quantity of Product should be worth atleast Rs. '+cleared_cheque_amount+'');
            return false;
        }else{
            $("#"+parentObj).find(".subtotal").text(product_price);
           
            $("#"+parentObj).find("#subtotal_"+ids[1]+"_"+ids[2]).val(product_price);
        }
        
    });

    $('.edit').click(function () {
        var parentObj = $(this).closest(".prod_row").attr("id");
        var text = $("#"+parentObj).find('input.quantity');
        if ( text.is('[readonly]') ) { 
            text.attr("readonly", false); 
        }else{
             text.attr("readonly", true); 
        }

    });
    $('.deleteprod').click(function () {
        //var cleared_cheque_amount = <?php echo $cleared_cheque_amount?>;
        var parentObj = $(this).closest(".prod_row").attr("id");
        var text = $("#"+parentObj).find('input.quantity');
        var selected_row = $(this).attr('id');
        var subtotal = calculate_subtotal(selected_row,0);      
       // alert(subtotal+"")  
        if(subtotal<cleared_cheque_amount){
            bootbox.alert('Quantity of Product should be worth atleast Rs. '+cleared_cheque_amount+'');
        }else{
            $("#"+parentObj).remove();
        }         

    }); 

    $('#update-stock').click(function () {
       var subtotal = calculate_subtotal('',1); 
      
       if(subtotal<cleared_cheque_amount){
            bootbox.alert('Quantity of Product should be worth atleast Rs. '+cleared_cheque_amount+'');
        }else if(subtotal>cleared_cheque_amount){
            if(confirm('Total product amount exceeds the cheque clearance amount. Either add another payment mode to proceed or reduce the quantity of Product')){
                subtotal = subtotal - cleared_cheque_amount;
                var twt = formatDecimal((subtotal) - order_discount);
                
                gtotal = formatDecimal(twt);
                <?php if($pos_settings->rounding) { ?>
                round_total = roundNumber(gtotal, <?=$pos_settings->rounding?>);
                var rounding = formatDecimal(0 - (gtotal - round_total));
                $('#twt').text(formatMoney(round_total) + ' (' + formatMoney(rounding) + ')');
                $('#quick-payable').text(round_total);
                <?php } else { ?>
                $('#twt').text(formatMoney(gtotal));
                $('#quick-payable').text(gtotal);
                <?php } ?>
                var rowCount = $('.product-table >tbody >tr').length;
                
                $('#item_count').text(rowCount);
                $('#paymentModals').appendTo("body").modal('show');  
            }
                     
            return false;
        }else{
            alert("success");
        }     
      
    });

    
        $(document).on('click', '.quick-cash', function () {
            var $quick_cash = $(this);            
            var amt = $quick_cash.contents().filter(function () {
                return this.nodeType == 3;
            }).text();           
            var th = site.settings.thousands_sep == 0 ? '' : site.settings.thousands_sep;
            var $pi = $('#' + pi);
            var abc = $('div.btn-group-vertical').children().first();
           
            if($(this).is(abc) == true)
            {     
                amt = formatDecimal(amt.split(th).join(""));
               
            }
            else{
                amt = formatDecimal(amt.split(th).join("")) * 1 + $pi.val() * 1;
            }
            $pi.val(formatDecimal(amt)).focus();
            var note_count = $quick_cash.find('span');
            if (note_count.length == 0) {
                //commented by vikas singh 25-10-2016 to remove count of currency
                 /*$quick_cash.append('<span class="badge">1</span>'); */
            } else {
                /* note_count.text(parseInt(note_count.text()) + 1); */
            }
       //     var pi = $pi;
           calculateTotals();
        });


        var maxField = 5; //Input fields increment limitation
    var addButton = $(".addButton"); //Add button selector
    var wrapper = $('#payments'); //Input field wrapper
   
    var x = 1, fieldCount = 1; //Initial field counter is 1
     //Initial field counter is 1
    //$("form").on("click", ".addButton", function (e) {
    $(addButton).click(function(){ //Once add button is clicked
        var total_amount=0;
        $('.amount').each(function () {
            var amount_val = parseFloat($(this).val());           
            if(!isNaN(amount_val)){                
                total_amount = total_amount + amount_val;
            }
        });        
        var amount_payable = parseFloat($("#quick-payable").text());        
        if(total_amount>=amount_payable){
            return false;
        }          

        if(fieldCount < maxField){ //Check maximum number of input fields
            var months = ['01','02','03','04','05','06','07','08','09','10','11','12'];

            var fieldHTML ="";
           // 
             //Increment field counter
            //alert("x"+x+"max"+maxField)
            x++;
            fieldCount++; //Increment field counter
            fieldHTML += '<div id="payments_'+x+'">';
            fieldHTML += '<div class="well well-sm well_1">';
            fieldHTML += '<a href="javascript:void(0);" class="remove_button" id="cl_row_' + x + '"  title="Remove field" style="position: absolute; margin-left: 90%;"><i class="fa fa-2x">×</i></a>';
            fieldHTML += '<div class="payment">';
            fieldHTML += '<div class="row">';
            fieldHTML += '<div class="col-sm-5">';
            fieldHTML += '<div class="form-group">';
            fieldHTML += '<?= lang("amount", "amount_'+x+'"); ?>';
            fieldHTML += '<input name="amountc['+x+']" type="text" id="amount_'+x+'" required="required" class="pa form-control amount addmorePay fillamount"/>';
            fieldHTML += '</div>';   
            fieldHTML += '</div>';                         
            fieldHTML += '<div class="col-sm-5 col-sm-offset-1">';                                
            fieldHTML += '<div class="form-group">';                                     
            fieldHTML += '<?= lang("paying_by", "paid_by_'+x+'"); ?>';                                        
            fieldHTML += '<select name="paid_by_['+x+']" id="paid_by_'+x+'" class="form-control paid_by addmorePay">';                                             
            fieldHTML += '<option value="ss">Select an option</option>';                                                
            fieldHTML += '<option value="cash"><?= lang("cash"); ?></option>';
            fieldHTML += '<option value="dd"><?= lang("dd"); ?></option>';    
            fieldHTML += '<option value="rtgs"><?= lang("rtgs"); ?></option>';         
            fieldHTML += '<option value="cc"><?= lang("CC"); ?></option>';   
            fieldHTML += '<option value="pos"><?= lang("pos_machine"); ?></option> ';                                                        
           /* fieldHTML += '<option value="cheque"><?= lang("cheque"); ?></option>';*/
            fieldHTML += '</select>'; 
            fieldHTML += '</div>';                                       
            fieldHTML += '</div>';                                         
            fieldHTML += '</div>';                                             
            fieldHTML += '<div class="row">';                                               
            fieldHTML += '<div class="col-sm-11">';                                                 
            fieldHTML += '<div class="form-group gc_'+x+'" style="display:none;">'; 
            fieldHTML += '<?= lang("gift_card_no", "gift_card_no_'+x+'"); ?>';                                                 
            fieldHTML += '<input name="cpaying_gift_card_no['+x+']" type="text" id="gift_card_no_'+x+'"class="pa form-control gift_card_no addmorePay" />';                                            
            fieldHTML += '<div id="gc_details_'+x+'"></div>';
            fieldHTML += '<input type="hidden" name="cn_balance_'+x+'" value="" id="cn_balance_'+x+'">';
            fieldHTML += '<div style="color:red;display:none;" id="cv_error_msg_' + x +'"><p></p></div>';
            //fieldHTML += '<div style="color:red;display:none;" id="cv_error_msg2_' + x +'"><p><?= lang('gift_card_not_for_customer') ?></p></div>';
            fieldHTML += '</div>';                                  
            fieldHTML += '<div class="form-group cv_'+x+'" style="display: none;">';                                   
            fieldHTML += '<?= lang("credit_voucher_no", "credit_voucher_no_'+x+'"); ?>';                                        

            fieldHTML += '<input name="cpaying_credit_voucher_no['+x+']" type="text" id="credit_voucher_no_'+x+'" class="pa form-control kb-pad credit_voucher_no addmorePay"/>';                                           
            fieldHTML += '<div id="cv_details_'+x+'"></div>';                                        
            fieldHTML += '</div>'; 


            fieldHTML += '<div class="pcc_'+x+'" style="display:none;">';
            fieldHTML += '<div class="row"><div class="col-md-6">';
            fieldHTML += '<div class="form-group"><input name="ccc_no['+x+']" type="text" id="pcc_no_'+x+'" class="form-control cardno" required="required" maxlength="20" placeholder="<?= lang('cc_no') ?>"/></div>';
            fieldHTML += '</div><div class="col-md-6"><div class="form-group">';
            fieldHTML += '<input name="ccc_holer['+x+']" type="text" id="pcc_holder_'+x+'" class="form-control cc_holder" required="required" placeholder="<?= lang('cc_holder') ?>"/></div>';
            fieldHTML += '</div><div class="col-md-3"><div class="form-group">';
            fieldHTML += '<select name="ccc_type['+x+']" id="pcc_type_'+x+'" class="form-control pcc_type" required="required" placeholder="<?= lang('card_type') ?>">';
            fieldHTML += '<option value=""><?= lang("select_pay"); ?></option>';
            fieldHTML += '<option value="Visa"><?= lang("Visa"); ?></option>';
            fieldHTML += '<option value="MasterCard"><?= lang("MasterCard"); ?></option>';
            fieldHTML += '<option value="Amex"><?= lang("Amex"); ?></option>';
            fieldHTML += '<option value="Discover"><?= lang("Discover"); ?></option>';                       
            fieldHTML += '<option value="RuPay"><?= lang("RuPay"); ?></option>';
            fieldHTML += '<option value="Contactless"><?= lang("Contactless"); ?></option>';           
                                                                
            fieldHTML += '</select>';
            fieldHTML += '</div></div>';
            fieldHTML += '<div class="col-md-3"><div class="form-group"><select name="ccc_month['+x+']" id="pcc_month_'+x+'" class="form-control monthList" style="width:100%;"  placeholder="<?= lang('card_type') ?>"><option value="">Month</option>';
            //required="required"
            for(var i=0; i < months.length;i++){
               fieldHTML += '<option value="'+months[i]+'">'+months[i]+'</option>'; 
            }

            fieldHTML += '</select>';           
            fieldHTML += '</div></div>';
            fieldHTML += '<div class="col-md-3"><div class="form-group">';
            fieldHTML += '<select name="ccc_year['+x+']" id="pcc_year_'+x+'" class="form-control yearList" style="width:100%;" required="required"><option value="">Year</option>';
            var start = new Date().getFullYear();    
            var end = new Date().getFullYear()+30;            
            for(var year = start ; year <=end; year++){
                fieldHTML += '<option value="'+year+'">'+year+'</option>';  
            }                  
             fieldHTML += '</select></div></div>';
             fieldHTML += '<div class="col-md-3"><div class="form-group">';
             fieldHTML += '<input name="ccc_cvv2[]" type="text" id="pcc_cvv2_'+x+'" class="form-control approval"  placeholder="<?= lang('cvv2') ?>"/>';
             fieldHTML += '</div></div></div></div>';
             
          
            fieldHTML += '<div class="pcheque_'+x+'" style="display:none;">';
            fieldHTML += '<div class="form-group"><?= lang("dd_no", "dd_no'+x+'"); ?>';                                                            
            fieldHTML += '<input name="ccheque_no['+x+']" type="text" id="cheque_no_'+x+'"class="form-control cheque_no addmorePay" maxlength="6"/></div>'; 
            fieldHTML += '<div class="form-group"><?= lang("date", "cheque_date_'+x+'"); ?>';
            fieldHTML += '<input name="ccheque_date['+x+']" type="text" id="cheque_date_'+x+'"class="form-control ccheque_date" /></div>';                                           
            fieldHTML += '</div>';

            fieldHTML += '<div class="pcheques_'+x+'" style="display:none;">';
            fieldHTML += '<div class="form-group"><?= lang("cheque_no", "cheque_no_'+x+'"); ?>';
            fieldHTML += '<input name="pcheque_no['+x+']" type="text" id="pcheque_no_'+x+'" class="form-control cheque_nos" maxlength="6"/></div>';
            fieldHTML += '<div class="form-group required"><?= lang("date", "date"); ?>';
            fieldHTML += '<input name="pcheque_date['+x+']" type="text" id="pcheque_date_'+x+'" class="form-control date"/> </div>';
            fieldHTML += '<div class="form-group"><?= lang("customer_name", "customer_name_'+x+'"); ?>';
            fieldHTML += '<input name="customer_name['+x+']" type="text" id="customer_name_'+x+'" class="form-control customer_name" />';
            fieldHTML += '</div>';
            fieldHTML += '<div class="form-group"><?= lang("bank_name", "bank_name_1"); ?>';
            fieldHTML += '<input name="bank_name['+x+']" type="text" id="bank_name_'+x+'" class="form-control bank_name" /></div></div>';

            fieldHTML += '<div class="prtgs_'+x+'" style="display:none;">';
            fieldHTML += '<div class="form-group"><?= lang("ref_no", "ref_no'+x+'"); ?>';
            fieldHTML += '<input name="rtgs_no['+x+']" type="text" id="rtgs_no_'+x+'"class="form-control rtgs_no addmorePay" />';                                                     
            fieldHTML += '</div>';
            fieldHTML += '</div>';                                                 
            fieldHTML += '<div class="form-group note">';  
            fieldHTML += '<?= lang('payment_note', 'payment_note'); ?>';                                                    
            fieldHTML += '<textarea name="payment_note['+x+']" id="payment_note_'+x+'"class="pa form-control payment_note"></textarea>'; 
            fieldHTML += '</div>';                                                           
            fieldHTML += '</div>';
            fieldHTML += '</div>';                                                           
            fieldHTML += '</div>';                                               
            fieldHTML += '</div>';                                                         
            $(wrapper).append(fieldHTML); // Add field html
            // add year list using jquery
           
           $('#paymentModals').css('overflow-y', 'scroll');
            $('.paid_by').change(function () {
                //alert(1)
            $('#paid_by_val_'+x).val($(this).val());   
            if($(this).val() == "cash")
            {            
                if ($('select option[value="' + $(this).val() + '"]:selected').length > 1)
                {
                   bootbox.alert('Payment method is already selected');                    
                   // self.select2('val','ss').attr('selected',true);
                   $("#paid_by_"+x+" option[value='ss']").prop('selected', true);                
                }
            }
            });


            
          /* $('.addmorePay').each(function () {               
              
              
               
               $('.cardno').bind('keyup blur',function(){ 
                    var node = $(this);
                    node.val(node.val().replace(/[^0-9]+/i, '') ); }
               );

                var validator = $.data(document.forms[1], 'validator');
               
                if ( validator ) {
                    return validator;
                }

               
                    //HERE it is creating a new one using the constructor   
                validator = new $.validator( options, this[0] );
                $.data(this[0], 'validator', validator);
   
               $(this).rules("add", {
                    required: true
               });

               // $('#amount_'+x).rules("add", {
               //       required:true              
              
               //  });
               //  $('#pcc_no_'+x).rules("add", {
               //       required:true              
              
               //  });
               //  $('#pcc_cvv2_'+x).rules("add", {
               //       required:true              
              
               //  });

               //  $('#cheque_no_'+x).rules("add", {
               //       required:true              
              
               //  });

               //  $('#cheque_no_'+x).rules("add", {
               //       required:true              
              
               //  });

               //   $('#pcheque_no'+x).rules("add", {
               //       required:true              
              
               //  });


               
               $('input[name="amountc['+x+']"]').rules("add", {
                    required:true              
              
               });
           
                $('input[name="ccc_no['+x+']"]').rules("add", {
                   required:true,              
                   creditcard: true
               });
           
                $('input[name="ccc_cvv2['+x+']"]').rules("add", {  
                   required:true,
                   cvvnumber:true,
                   messages:{
                      cvvnumber: "Please enter valid cvv number" 
                   }
                });
         
               $('input[name="ccheque_no['+x+']"]').rules("add", {  
                   required:true,
                   alphanumeric :true,
                   maxlength:6,
                   minlength:6
               
               });
          
              $('input[name="pcheque_no['+x+']"]').rules("add", {  
                   required:true,
                   alphanumeric :true,
                   maxlength:6,
                   minlength:6           
               });

               $('input[name="pcheque_date['+x+']"]').rules("add", {  
                    required:true,                 
               });
               $('input[name="customer_name['+x+']"]').rules("add", {  
                    required:true,                 
               });
               $('input[name="bank_name['+x+']"]').rules("add", {  
                    required:true,                 
               });              
           });*/
           
    //    x++;
        }
         else{
              bootbox.alert('<?= lang('max_reached') ?>');
                return false;
        }
        
        var d = new Date(),
        n = d.getMonth(),
        y = d.getFullYear();

         for (i = new Date().getFullYear() ; i < 2036; i++) {
        
            $('.cyear').append("<option val="+i+">"+i+"</option>");
                  
           }
           
     selectValues = { "1": "01", "2": "02", "3": "03", "4": "04", "5": "05", "6": "06", "7": "07", "8": "08", "9": "09", "10": "10", "11": "11", "12": "12"};
     $.each(selectValues, function(key, value) {   
     $('.cmonth').append($("<option></option>").attr("value",key).text(value)); 
     $('.cmonth option:eq('+n+')').prop('selected', true);
     
     });
     
    });

    pi = 'amount_' + pa;
       
    $(document).on('focus', '.amount', function () {
        pi = $(this).attr('id');
        calculateTotals();
    }).on('blur', '.amount', function () {
        calculateTotals();
    });

    $(document).on('click', '.remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        
        total_paying = $('#total_paying').text().replace(',','');
        var id = $(this).attr('id').split('_')[2];
        var focusid = parseInt(id - 1);
        var balance_amount = 
            total_paying -= $('#amount_'+id).val();
            $('#total_paying').text(formatMoney(total_paying));
            $('#balance').text(formatMoney(grand_total - total_paying));
            
            $('#amount_'+id).val('');
            $('#amount_val_'+id).val('');
            $('#paid_by_val_'+id).val('');
            
        $(this).parent('div').remove(); //Remove field html
        fieldCount--; //Decrement field counter
    });
    
     $(document).on('change', '.paid_by', function () {       
    
        var p_val = $(this).val(),
        id = $(this).attr('id'),
        pa_no = id.substr(id.length - 1);

        if((pa_no>1) && (($("#paid_by_1").val()=='cheque')) && (p_val!='cheque')){
            bootbox.alert('Cannot add cheque payment method with other payment options');
            $("#paid_by_"+x+" option[value='ss']").prop('selected', true);         
            return false;

        }            

        $('#rpaidby').val(p_val);
        if (p_val == 'cash' || p_val == 'other') {
            $('.pcheque_' + pa_no).hide();
            $('.pcc_' + pa_no).hide();
            $('.pcash_' + pa_no).show();
            $('.pcheques_' + pa_no).hide();
            $('.prtgs_' + pa_no).hide();
            $('#payment_note_' + pa_no).focus();
        } else if (p_val == 'cc' || p_val == 'stripe' || p_val == 'ppp') {
            $('.pcheque_' + pa_no).hide();
            $('.pcash_' + pa_no).hide();
            $('.pcc_' + pa_no).show();
             $('.prtgs_' + pa_no).hide();
             $('.pcheques_' + pa_no).hide();
            $('#swipe_' + pa_no).focus();
        } else if (p_val == 'dd') {
            $('.pcc_' + pa_no).hide();
            $('.pcash_' + pa_no).hide();
            $('.pcheque_' + pa_no).show();
            $('.prtgs_' + pa_no).hide();
            $('.pcheques_' + pa_no).hide();
            $('#cheque_no_' + pa_no).focus();
        }else if(p_val == 'cheque'){
           // alert(1+"=====>"+pa_no)
            $('.pcc_' + pa_no).hide();
            $('.pcash_' + pa_no).hide();
            $('.pcheque_' + pa_no).hide();
            $('.prtgs_' + pa_no).hide();                
            $('#cheque_no_' + pa_no).focus();   
            $('.pcheques_' + pa_no).show();    

        }  else if (p_val == 'rtgs' || p_val == 'pos') {
            $('.pcc_' + pa_no).hide();
            $('.pcash_' + pa_no).hide();
            $('.pcheque_' + pa_no).hide();
            $('.prtgs_' + pa_no).show();
            $('.pcheques_' + pa_no).hide();
            $('#rtgs_no_' + pa_no).focus();
        } 
        else if (p_val == 'gift_card') {
            $('.gc_' + pa_no).show();
            $('.prtgs_' + pa_no).hide();
            $('.ngc_' + pa_no).hide();
            $('#gift_card_no_' + pa_no).focus();
            $('.pcheques_' + pa_no).hide();
        }          
         else if (p_val == 'credit_voucher') {
            $('.cv_' + pa_no).show();
            $('.ngc_' + pa_no).hide();
            $('.prtgs_' + pa_no).hide();
            $('#credit_voucher_no_' + pa_no).focus();
            $('.pcheques_' + pa_no).hide();
        } else {
            $('.pcc_' + pa_no).hide();
            $('.pcash_' + pa_no).hide();
            $('.pcheque_' + pa_no).hide();
            $('.prtgs_' + pa_no).hide();           
            $('.pcheques_' + pa_no).hide();    
            $('.ngc_' + pa_no).hide();
            $('.cv_' + pa_no).hide();
            $('.prtgs_' + pa_no).hide();
            $('#cv_details_' + pa_no).html('');
            $('.pcheques_' + pa_no).hide();
        }
    });

     $("#pos-payment-form").validate({
        
           rules:{
                'amountc[0]':{
                            required:true,                            
                        },
               'ccc_no[0]':{
                            required:true,
                            creditcard: true
                        },
            
                        
              'ccc_month[0]':{
                            required:true,
                            cc_valid_month:true,
                          
                        },
              'ccc_year[0]':{
                            required:true
                          
                        },
              'ccheque_no[0]':{
                       required:true,
                       alphanumeric:true,
                       maxlength:6,
                       minlength:6
                           
                      },
               'pcheque_no[0]':{
                   required:true,
                   alphanumeric:true,
                   maxlength:6,
                   minlength:6
                   
                },
                'pcheque_date[0]':{
                   required:true,                  
                   
                },
                'customer_name[0]':{
                   required:true,                
                   
                },
                'bank_name[0]':{
                   required:true,            
                   
                },    
           },
           
           messages:{
               
               'ccc_no[0]':{
                           required:'Please enter credit card number',
                           creditcard:'Please enter valid credit card no'
                   
               },
               
               'ccc_month[0]':{
                            required:"choose month of expiry",
                            
                          
                        },
               'ccc_year[0]':{
                          required:"choose year of expiry"

               },
               
               'ccheque_no[0]':{
                           required:"Please enter a valid cheque number",
                           number:"Cheque number must be a number"
                           
                      }   
              
               
           },
           
           submitHandler:function(form){
                
               var rv;
               var appr;
               /*var chequeArray = [];
               $(".cheque_nos").each(function() {
                var c_no = jQuery.trim($(this).val());
                  if($.inArray(c_no,$chequeArray)!=-1){
                    alert("Please enter unique cheque number");
                     alert(2)
                    return false;
                  } else{
                    alert(1)
                  }             

               });
               return false;*/
              
               var approvalArr = [];
               var chequeArray = [];
               var today = new Date();
               var thisYear = today.getFullYear();
               var month = (1 + today.getMonth()).toString();
               var thisMonth = month.length > 1 ? month : '0' + month;
               $('.paid_by').each(function() {
                 
               var paidmethod = $(this).val();
               var parentObj = $(this).closest('div.payment');
               var amountc = parentObj.find('.amount').val();
             
               if(paidmethod == 'cash' && amountc > 200000 )
                 {    
                    bootbox.alert("Cash payment method can not accept more than Rs.200000, kindly choose another payment method");
                    rv = false;
                 }
                 if(paidmethod == 'rtgs' && amountc < 200000 )
                 {    
                    bootbox.alert("RTGS payment amount should be minimum of Rs.200000");
                    rv = false;
                 }
                 if(paidmethod=='cheque'){
                    var cheque_nos = parentObj.find(".cheque_nos").val();
                    if($.inArray(cheque_nos,chequeArray)==-1){
                        chequeArray.push(cheque_nos);
                    }else{
                        if($('.paid_by').length>1){
                           bootbox.alert("Cheque no. should be unique");
                            //rv = false;
                            appr = false;
                            return false;
                        }
                    }
                }

                 if(paidmethod=='cc'){
                        var approval_no = parentObj.find(".approval").val();
                        var yearVal = parentObj.find(".yearList").val();
                        var monthVal = parentObj.find(".monthList").val();
                        
                        if(yearVal==''){
                            var yearVal = parentObj.find("#pcc_year_1").val();
                        }
                        if(monthVal==''){
                            var monthVal = parentObj.find("#pcc_month_1").val();
                        }
                   
                  //  alert(yearVal+"==="+thisYear+"monthVal"+monthVal+"thisMonth"+thisMonth)
                        if(yearVal!='' && monthVal!='')
                        {
                           
                            if(yearVal < thisYear)
                            {
                                bootbox.alert("Invalid Year");
                                appr = false;
                            }
                            else if(((parseInt(yearVal) == parseInt(thisYear))) && (parseInt(thisMonth) >= parseInt(monthVal)))
                            { 
                            //alert(3)                      
                                bootbox.alert("Invalid date of expiration");                       
                                appr = false;
                                return false;
                            }
                        }

                        if($.inArray(approval_no,approvalArr)==-1){
                            approvalArr.push(approval_no);
                        }else{
                            if($('.paid_by').length>1){
                               bootbox.alert("Aprroval no. should be unique");
                                //rv = false;
                                appr = false;
                                return false;
                            }
                        }

                    }

                    });
             
              
                    if(rv != undefined){
                       
                        return false;
                    }
                    if(appr != undefined){
                       
                        return false;
                    }
                    var total_amount = 0;

                    $('.amount').each(function () {
                        var amount_val = parseFloat($(this).val());           
                        if(!isNaN(amount_val)){                
                            total_amount = total_amount + amount_val;
                        }
                    }); 
                    var ii = 1;
                    var remaining = total_amount;
                    $('.fillamount').each(function () {
                        var amount = parseFloat($(this).val());                       
                        remaining = remaining-amount;
                        //alert("remaine"+"#balance_amount_"+ii);         
                        $("#balance_amount_"+ii).val(remaining);
                        //alert($("#balance_amount_"+ii).val())
                        ii++;
                    }); 

                   
                    //added by vikas 20-10-2016
                    var pa_methods = [];

                    $('select.paid_by').each(function(){
                        pa_methods.push($(this).val());
                    });

                    if(pa_methods.indexOf('ss') > -1){
                        bootbox.alert("Please Select an Option");
                        return false;
                    } 
       
                    if (total_paid < grand_total) {
                             bootbox.alert('<?=lang('less_than_total_paying');?>');
                             return false;
                           // bootbox.confirm("Invalid amount", function (res) {
       
                    } else {
                      //  alert(2)                  
                          document.forms[0].submit();
                          $('#submit-sale').attr('disabled','disabled');
                         // $("#update-stock").bind("click");
                          //$('#provision-form').submit();
                        
                        }
                    }
            });
             
             
            $.validator.addMethod("cvvnumber",
                     function(value, element) {
                        return /^[0-9]{3,4}$/.test(value);
            });

});

function calculate_subtotal(selected_row='',flag){  
    //alert("flag"+flag+"==="+selected_row) 
    var subtotal = 0;
    $('.subtotalamount').each(function(i, obj) {  

        if(flag==0){   
         
          if($(this).attr('id')!='subtotal_'+selected_row+'') {
             subtotal += parseFloat($(this).val());
          }            
           
        }else{
            
            subtotal += parseFloat($(this).val());
        }
    });
   return subtotal;
}

function calculateTotals() {
    var total_paying = 0;
    var ia = $(".amount");
    $.each(ia, function (i) {
        total_paying += parseFloat($(this).val());
    });
    $('#total_paying').text(formatMoney(total_paying));
    <?php if($pos_settings->rounding) { ?>
    $('#balance').text(formatMoney(total_paying - round_total));
    $('#balance_amount_' + pi).val(formatDecimal(total_paying - round_total));

    total_paid = total_paying;
    grand_total = round_total;
    <?php } else { ?>
    $('#balance').text(formatMoney(total_paying - gtotal));
    $('#balance_amount_' + pi).val(formatDecimal(total_paying - gtotal));
    total_paid = total_paying;
    grand_total = gtotal;
    <?php } ?>
}
</script>
