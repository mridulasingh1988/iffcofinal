<?php //

function product_name($name)
{
    return character_limiter($name, (isset($pos_settings->char_per_line) ? ($pos_settings->char_per_line-8) : 35));
}

if ($modal) {
    echo '<div class="modal-dialog no-modal-header" style="width:780px">'
            . '<div class="modal-content" style="padding:7px;">'
            . '<div class="modal-body">'
            . '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">'
            . '<i class="fa fa-2x">&times;</i></button>';
} else { ?>
    <!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title><?= $page_title . " " . lang("no") . " " . $inv->reference_no; ?></title>
        <base href="<?= base_url() ?>"/>
        <meta http-equiv="cache-control" content="max-age=0"/>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="expires" content="0"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <link rel="shortcut icon" href="<?= $assets ?>images/icon.png"/>
        <link rel="stylesheet" href="<?= $assets ?>styles/theme.css" type="text/css"/>
        
        <style type="text/css" media="all">
            body {
                color: #000;
                font-size: 10px;
            }

            #wrapper {
                max-width: 980px;
                margin: 0 auto;
                padding-top: 20px;
            }
            table>tr>td{
                vertical-align: top;
            }
            .btn {
                border-radius: 0;
                margin-bottom: 5px;
            }

            h3 {
                margin: 5px 0;
            }

            @media print {
                .no-print {
                    display: none;
                }

                #wrapper {
                    max-width: 680px;
                    width: 100%;
                    min-width: 250px;
                    margin: 0 auto;
                }
            }

            @page { size 8.5in 11in; margin: 2cm }
            div.page { page-break-after: always }
        </style>
    </head>

    <body>

<?php } ?>

<div id="wrapper">
<div style="color:#000000;font-size:25px;height:500px;padding:150px 0px 0px 100px;align:justify">
    Thank you! Your cheque details has been sent for reconciliation. <br>After reconciliation, your order will be dispatched.
</div>
</div>
<?php if ($modal) {
    echo '</div></div></div>';
}else { ?>
<div id="buttons" style="padding-top:10px; text-transform:uppercase;" class="no-print">
   
    <span class="col-xs-12">
        <!--Add logout functionality after payment @ ankit-->
        <?php if($pos_settings->logout_after_payment=='1'){  ?>
        <a class="btn btn-block btn-warning" href="<?= site_url('auth/logout'); ?>"><?= lang("back_to_pos"); ?></a>
        <?php } else { ?>
        <a class="btn btn-block btn-warning" href="<?= site_url('pos'); ?>"><?= lang("back_to_pos"); ?></a>
        <?php } ?>
    </span>
    <?php if (!$pos_settings->java_applet) { ?>
        <div style="clear:both;"></div>
        <div class="col-xs-12 text-center" style="background:#F5F5F5; padding:10px;">
            <p style="font-weight:bold;">Please don't forget to disble the header and footer in browser print
                settings.</p>

            <p style="text-transform: capitalize;"><strong>FF:</strong> File &gt; Print Setup &gt; Margin &amp;
                Header/Footer Make all --blank--</p>

            <p style="text-transform: capitalize;"><strong>chrome:</strong> Menu &gt; Print &gt; Disable Header/Footer
                in Option &amp; Set Margins to None</p></div>
    <?php } ?>
    <div style="clear:both;"></div>

</div>
 </body>
  </html>
    <?php } ?>

