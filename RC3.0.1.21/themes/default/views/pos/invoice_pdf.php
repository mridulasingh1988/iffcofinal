<?php //
function product_name($name)
{
    return character_limiter($name, (isset($pos_settings->char_per_line) ? ($pos_settings->char_per_line-8) : 35));
}

if ($modal) {
    echo '<div class="modal-dialog no-modal-header" style="width:780px">'
            . '<div class="modal-content" style="padding:7px;">'
            . '<div class="modal-body">'
            . '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">'
            . '<i class="fa fa-2x">&times;</i></button>';
} else { ?>
    <!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title><?= $page_title . " " . lang("no") . " " . $inv->id; ?></title>
        <base href="<?= base_url() ?>"/>
        <meta http-equiv="cache-control" content="max-age=0"/>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="expires" content="0"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <link rel="shortcut icon" href="<?= $assets ?>images/icon.png"/>
        <link rel="stylesheet" href="<?= $assets ?>styles/theme.css" type="text/css"/>
        
        <style type="text/css" media="all">
            body {
                color: #000;
                font-size: 10px;
            }

            #wrapper {
                max-width: 980px;
                margin: 0 auto;
                padding-top: 20px;
            }
            table>tr>td{
                vertical-align: top;
            }
            .btn {
                border-radius: 0;
                margin-bottom: 5px;
            }

            h3 {
                margin: 5px 0;
            }

            @media print {
                .no-print {
                    display: none;
                }

                #wrapper {
                    max-width: 680px;
                    width: 100%;
                    min-width: 250px;
                    margin: 0 auto;
                }
            }

            @page { size 8.5in 11in; margin: 2cm }
            div.page { page-break-after: always }
        </style>
    </head>

    <body>

<?php } ?>

<div id="wrapper">

<?php 
$mod = count($rows)%4;
$modloop = count($rows)/4;
$loop = $mod==0?$modloop:ceil($modloop);
for($i=0;$i<$loop;$i++){
?>
    
    <div id="receiptData">
        <div class="no-print">
            <?php if ($message) { ?>
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <?= is_array($message) ? print_r($message, true) : $message; ?>
                </div>
            <?php } ?>
        </div>
        <!--store-copy-->
        <div id="receipt-data">
            <div class="row" style="width:100%;">
                <div class="text-left textleft"><?= lang("store_copy"); ?></div>
                <div class="text-center textcenter"><img src="<?= base_url() . 'assets/uploads/logos/logo.jpg'; ?>" alt="<?= $inv->biller; ?>" height="30px;"> 
                <br><?= $inv->biller; ?>

                </div>
                <div class="text-right textright" style="font-size:9px;">
                    <?php
                    echo "<p>" . lang("bill_time") . ": " . $this->sma->hrld($inv->date) . "<br>";
                    echo lang("tin_no") . ": 06192243686<br>";
                    echo lang("phone") . ": " . $biller->phone."<br>";
                    echo lang("ebazar_pan") . ": AAKCA1714G </p>";
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="text-center">
                    <table class="table table-striped table-condensed no-bg" cellspacing="0" border="1" cellpadding="0" style="font-size:10px;vertical-align:top;padding:2px !important;line-height:10px !important;vertical-align:top !important;">
                        <tbody>
                            
                            <tr>
                                <th align="right" style="padding:2px; vertical-align:top; text-align:right !important;"><?=lang("customer_num"); ?></th>
                                <th align="left" colspan="2" style="padding:2px; vertical-align:top;"><strong><?=$customer->id?></strong></th>
                                <th colspan="2" align="right" style="padding:2px; vertical-align:top;text-align:right !important;"><?=lang("cash_memo"); ?></th>
                                <th colspan="2" align="left" style="padding:2px; vertical-align:top;"><?=$inv->reference_no;?></th>
                                <th colspan="2" align="right" style="padding:2px; vertical-align:top; text-align:right !important;"><?=lang("invoice_date"); ?></th>
                                <th colspan="3" align="left" style="padding:2px; vertical-align:top;"><?=date("d/m/Y", strtotime($inv->date));?></th>
                            </tr>
                            
                            <tr style="background-color: #fff;">
                                <td align="right" style="padding:2px; vertical-align:top;"><?=lang("customer_name"); ?></td>
                                <td colspan="2" align="left" style="padding:2px; vertical-align:top;"><strong><?=$customer->name;?></strong></td>
                                <td colspan="2" align="right" style="padding:2px; vertical-align:top;"><?=lang("father"); ?></td>
                                <td colspan="2" align="left" style="padding:2px; vertical-align:top;"><strong><?=$customer->father_name;?></strong></td>
                                <td colspan="2" align="right" style="padding:2px; vertical-align:top;"><?=lang("tele"); ?></td>
                                <td colspan="3" align="left" style="padding:2px; vertical-align:top;"><strong><?=$customer->phone;?></strong></td>
                            </tr>
                            
                            <tr style="background-color: #fff;">
                                <td align="right" style="padding:2px; vertical-align:top;"><?=lang("address"); ?></td>
                                <td colspan="2" align="left" style="padding:2px; vertical-align:top;"><strong><?=$customer->address;?></strong></td>
                                <td colspan="2" align="right" style="padding:2px; vertical-align:top;"><?=lang("nominee"); ?></td>
                                <td colspan="2" align="left" style="padding:2px; vertical-align:top;"><strong><?=$customer->nominees1;?></strong></td>
                                <td colspan="2" align="right" style="padding:2px; vertical-align:top;"><?=lang("identity"); ?></td>
                                <td colspan="3" align="left" style="padding:2px; vertical-align:top;"><strong><?=$customer->id_proof; ?></strong></td>
                            </tr>
                        
                            <tr class="head-bg">
                                <th width="15%" rowspan="2" style="background-color:#dbdbdb !important;padding:2px; vertical-align:top;"><?= lang("product"); ?></th>
                                <th width="10%" rowspan="2" style="background-color:#dbdbdb !important;padding:2px; vertical-align:top;"><?= lang("alot_no"); ?></th>
                                <th width="10%" rowspan="2" style="background-color:#dbdbdb !important;padding:2px; vertical-align:top;"><?= lang("end_date"); ?></th>
                                <th width="5%" colspan="2" style="background-color:#dbdbdb !important;padding:2px; text-align:center; vertical-align:top;"><?= lang("nug"); ?></th>
                                
                                <th width="25%" colspan="4" align="center" style="background-color:#dbdbdb !important;text-align:center;padding:2px; vertical-align:top; border-bottom:1px solid #333;"><?= lang("every_nug"); ?></th>
                                <th width="25%" colspan="3" style="background-color:#dbdbdb !important;text-align:center;padding:2px; vertical-align:top; border-bottom: 1px solid #333;"><?= lang("total"); ?></th>
                                
                            </tr>
                        
                            <tr class="head-bg">
                                <th width="10%" style="background-color:#dbdbdb !important; padding:2px; vertical-align:top; border-top: 1px solid #333;">Number.</th>
                                <th width="10%" style="background-color:#dbdbdb !important; padding:2px; vertical-align:top; border-top: 1px solid #333;">In words</th>
                                <th width="10%" style="border:left: 1px solid #333;background-color:#dbdbdb !important; padding:2px; vertical-align:top;"><?= lang("bag_type"); ?></th>
                                <th width="10%" style="background-color:#dbdbdb !important; padding:2px; vertical-align:top;"><?= lang("rate"); ?></th>
                                <th width="10%" style="background-color:#dbdbdb !important; padding:2px; vertical-align:top;"><?= lang("tax_vat_serv"); ?></th>
                                <th width="10%" style="background-color:#dbdbdb !important; padding:2px; vertical-align:top;"><?= lang("amount"); ?></th>
                                <th width="10%" style="background-color:#dbdbdb !important; padding:2px; vertical-align:top;"><?= lang("tax_vat"); ?></th>
                                <th width="10%" style="background-color:#dbdbdb !important; padding:2px; vertical-align:top;"><?= lang("discount"); ?></th>
                                <th width="10%" style="background-color:#dbdbdb !important; padding:2px; vertical-align:top;"><?= lang("amount"); ?></th>
                                
                            </tr>
                            
                        <?php
                        $r = 1;
                        $tax_summary = array();
                        $count = count($rows);
                        $limit = 4;
                        if($i == 0){
                                $start = 0;
                            }
                            else{
                                $start = ($i*$limit)+1;
                                $limit = 4+$limit;
                                $r = $start;
                            }
                           $total_net_amt = 0;
                        foreach ($rows as $row) {
                            $total_discounted_amount += ($row->quantity * $row->net_unit_price);
                            $total_net_amt += ($row->quantity * $row->net_unit_price) + $row->item_discount;
                            $total_amt += $row->unit_price;
                            $total_items +=$row->quantity;
                            $total_discount += $row->item_discount;
                            $total_tax += $row->item_tax;

                            if (isset($tax_summary[$row->tax_code])) {
                                $tax_summary[$row->tax_code]['items'] += $row->quantity;
                                $tax_summary[$row->tax_code]['tax'] += $row->item_tax;
                                $tax_summary[$row->tax_code]['amt'] += ($row->quantity * $row->net_unit_price) - $row->item_discount;
                            } else {
                                $tax_summary[$row->tax_code]['items'] = $row->quantity;
                                $tax_summary[$row->tax_code]['tax'] = $row->item_tax;
                                $tax_summary[$row->tax_code]['amt'] = ($row->quantity * $row->net_unit_price) - $row->item_discount;
                                $tax_summary[$row->tax_code]['name'] = $row->tax_name;
                                $tax_summary[$row->tax_code]['code'] = $row->tax_code;
                                $tax_summary[$row->tax_code]['rate'] = $row->tax_rate;
                            }
                            $total_tax = ($row->tax_value*$row->subtotal)/(100+$row->tax_value);
                            $subtotal_item = ($row->real_unit_price) * ($row->quantity);
                            ?>
                            
                            <tr style="background-color: #fff;">
                                <td align="left" style="padding:2px; vertical-align:top;"><?= $r.' '.product_name($row->product_name) . ($row->variant ? ' (' . $row->variant . ')' : ''); ?></td>
                                <td align="left" style="padding:2px; vertical-align:top;"><?=$row->lot_no;?></td>
                                <td align="left" style="padding:2px; vertical-align:top;"><?= date("d/m/Y", strtotime($row->date."+3years")); ?></td>
                                <td align="left" style="padding:2px; vertical-align:top;"><?= $this->sma->formatQuantity($row->quantity); ?> </td>
                                <td align="left" style="padding:2px;"><?=$this->pos_model->convertNumber($row->quantity);?></td>
                                <td align="left" style="padding:2px; vertical-align:top;">Packets</td>
                                <td align="left" style="padding:2px; vertical-align:top;"><?= $this->sma->formatMoney($row->unit_cost); ?></td>
                                <td align="left" style="padding:2px;"><?= $this->sma->formatMoney($row->unit_tax); ?></td>
                                <td align="left" style="padding:2px;">
                                <?= $this->sma->formatMoney($row->real_unit_price) ?>
                                </td>
                                <td align="left" style="padding:2px; vertical-align:top;"><?= $this->sma->formatMoney($total_tax) ?></td>
                                <td align="left" style="padding:2px; vertical-align:top;"><?= $this->sma->formatMoney($row->item_discount) ?></td>
                                <td align="left" style="padding:2px; vertical-align:top;"><?= $this->sma->formatMoney($subtotal_item); ?></td>
                            </tr>
                            <?php
                                    if($r == $limit){
                                        break;
                                    }
                                    if($count == $r){
                                        break;
                                    }

                                $r++;
                                //$start++; 
                                
                                }
                        
                        if ($pos_settings->rounding) { 
                            $round_total = $this->sma->roundNumber($inv->grand_total, $pos_settings->rounding);
                            $rounding = $this->sma->formatMoney($round_total - $inv->grand_total);
                        }
                        ?>
                            <tr class="no-bg">
                                <td colspan="7" style="padding:2px; vertical-align:top;"></td>
                                <td colspan="2" align="left" style="padding:2px; vertical-align:top;"><?=lang("total_amount");?></td>
                                <td colspan="3" align="left" style="padding:2px; vertical-align:top;"><strong><?= $this->sma->formatMoney($total_net_amt1); ?></strong></td>
                            </tr>

                            <tr class="no-bg">
                                <td colspan="7" style="padding:2px; vertical-align:top;"></td>
                                <td colspan="2" align="left" style="padding:2px; vertical-align:top;padding:2px; vertical-align:top;"><?=lang("discount");?></td>
                                <td colspan="3" align="left" style="padding:2px; vertical-align:top;"><strong><?= $this->sma->formatMoney($row->discount_value); ?></strong></td>
                            </tr>

                            <tr class="no-bg">
                                <td align="left" style="padding:2px; vertical-align:top;"><?= lang("in_words"); ?></td>
                                <td align="left" colspan="6" style="padding:2px; vertical-align:top;">
                                    <strong><?=$this->pos_model->convertWords($inv->grand_total + $rounding)?>Only</strong>
                                </td>
                                <td align="left" colspan="2" style="padding:2px; vertical-align:top;"><?=lang("total_pay_amount");?></td>
                                <td colspan="3" align="left" style="padding:2px; vertical-align:top;"><strong><?= $this->sma->formatMoney($inv->grand_total); ?></strong></td>
                            </tr>
                            <tr>
                                <td colspan="12" align="left" style="vertical-align: bottom; font-size: 10px;"><b>Payment Note:</b> &nbsp; &nbsp;
                                <?php 
                                // Update By Ankit
                                if(isset($payments[0]->note))
                                {
                                    echo $payments[0]->note;
                                }
                                
                                ?>

                                 </td>
                            </tr>
                            
                            <tr class="no-bg sign-height" style="line-height:45px !important;">
                                <td colspan="6" align="center" style="vertical-align: bottom;">______________________<br> <?=lang("cus_sign");?></td>
                                <td colspan="6" align="center" style="vertical-align: bottom;">_____________________________ <br> <?=lang("sign_center");?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
        <!--customer-copy-->
        <div class="dash-border">&nbsp;</div>
        
        <div class="row">&nbsp;</div>
        
        <div id="receipt-data">
            <div class="row" style="width:100%;">
                <div class="text-left textleft"><?= lang("customer_copy"); ?></div>
                <div class="text-center textcenter"><img src="<?= base_url() . 'assets/uploads/logos/logo.jpg'; ?>" alt="<?= $inv->biller; ?>" height="30px;"> 
                <br><?= $inv->biller; ?>

                </div>
                <div class="text-right textright" style="font-size:9px;">
                    <?php
                    echo "<p>" . lang("bill_time") . ": " . $this->sma->hrld($inv->date) . "<br>";
                    echo lang("tin_no") . ":  06192243686<br>";
                    echo lang("phone") . ": " . $biller->phone."<br>";
                    echo lang("ebazar_pan") . ": AAKCA1714G </p>";
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="text-center">
                    <table class="table table-striped table-condensed no-bg" cellspacing="0" border="1" cellpadding="0" style="font-size:10px;vertical-align:top;padding:2px !important;line-height:10px !important;vertical-align:top !important;">
                        <tbody>
                            
                            <tr>
                                <th align="right" style="padding:2px; vertical-align:top; text-align:right !important;"><?=lang("customer_num"); ?></th>
                                <th align="left" colspan="2" style="padding:2px; vertical-align:top;"><strong><?=$customer->id?></strong></th>
                                <th colspan="2" align="right" style="padding:2px; vertical-align:top;text-align:right !important;"><?=lang("cash_memo"); ?></th>
                                <th colspan="2" align="left" style="padding:2px; vertical-align:top;"><?=$inv->reference_no;?></th>
                                <th colspan="2" align="right" style="padding:2px; vertical-align:top; text-align:right !important;"><?=lang("invoice_date"); ?></th>
                                <th colspan="3" align="left" style="padding:2px; vertical-align:top;"><?=date("d/m/Y", strtotime($inv->date));?></th>
                            </tr>
                            
                            <tr style="background-color: #fff;">
                                <td align="right" style="padding:2px; vertical-align:top;"><?=lang("customer_name"); ?></td>
                                <td colspan="2" align="left" style="padding:2px; vertical-align:top;"><strong><?=$customer->name;?></strong></td>
                                <td colspan="2" align="right" style="padding:2px; vertical-align:top;"><?=lang("father"); ?></td>
                                <td colspan="2" align="left" style="padding:2px; vertical-align:top;"><strong><?=$customer->father_name;?></strong></td>
                                <td colspan="2" align="right" style="padding:2px; vertical-align:top;"><?=lang("tele"); ?></td>
                                <td colspan="3" align="left" style="padding:2px; vertical-align:top;"><strong><?=$customer->phone;?></strong></td>
                            </tr>
                            
                            <tr style="background-color: #fff;">
                                <td align="right" style="padding:2px; vertical-align:top;"><?=lang("address"); ?></td>
                                <td colspan="2" align="left" style="padding:2px; vertical-align:top;"><strong><?=$customer->address;?></strong></td>
                                <td colspan="2" align="right" style="padding:2px; vertical-align:top;"><?=lang("nominee"); ?></td>
                                <td colspan="2" align="left" style="padding:2px; vertical-align:top;"><strong><?=$customer->nominees1;?></strong></td>
                                <td colspan="2" align="right" style="padding:2px; vertical-align:top;"><?=lang("identity"); ?></td>
                                <td colspan="3" align="left" style="padding:2px; vertical-align:top;"><strong><?=$customer->id_proof; ?></strong></td>
                            </tr>
                        
                            <tr class="head-bg">
                                <th width="15%" rowspan="2" style="background-color:#dbdbdb !important;padding:2px; vertical-align:top;"><?= lang("product"); ?></th>
                                <th width="10%" rowspan="2" style="background-color:#dbdbdb !important;padding:2px; vertical-align:top;"><?= lang("alot_no"); ?></th>
                                <th width="10%" rowspan="2" style="background-color:#dbdbdb !important;padding:2px; vertical-align:top;"><?= lang("end_date"); ?></th>
                                <th width="5%" colspan="2" style="background-color:#dbdbdb !important;padding:2px; text-align:center; vertical-align:top;"><?= lang("nug"); ?></th>
                                
                                <th width="25%" colspan="4" align="center" style="background-color:#dbdbdb !important;text-align:center;padding:2px; vertical-align:top; border-bottom:1px solid #333;"><?= lang("every_nug"); ?></th>
                                <th width="25%" colspan="3" style="background-color:#dbdbdb !important;text-align:center;padding:2px; vertical-align:top; border-bottom: 1px solid #333;"><?= lang("total"); ?></th>
                                
                            </tr>
                        
                            <tr class="head-bg">
                                <th width="10%" style="background-color:#dbdbdb !important; padding:2px; vertical-align:top; border-top: 1px solid #333;">Number</th>
                                <th width="10%" style="background-color:#dbdbdb !important; padding:2px; vertical-align:top; border-top: 1px solid #333;">In words</th>
                                <th width="10%" style="border:left: 1px solid #333;background-color:#dbdbdb !important; padding:2px; vertical-align:top;"><?= lang("bag_type"); ?></th>
                                <th width="10%" style="background-color:#dbdbdb !important; padding:2px; vertical-align:top;"><?= lang("rate"); ?></th>
                                <th width="10%" style="background-color:#dbdbdb !important; padding:2px; vertical-align:top;"><?= lang("tax_vat_serv"); ?></th>
                                <th width="10%" style="background-color:#dbdbdb !important; padding:2px; vertical-align:top;"><?= lang("amount"); ?></th>
                                <th width="10%" style="background-color:#dbdbdb !important; padding:2px; vertical-align:top;"><?= lang("tax_vat"); ?></th>
                                <th width="10%" style="background-color:#dbdbdb !important; padding:2px; vertical-align:top;"><?= lang("discount"); ?></th>
                                <th width="10%" style="background-color:#dbdbdb !important; padding:2px; vertical-align:top;"><?= lang("amount"); ?></th>
                                
                            </tr>
                            
                        <?php
                        $r = 1;
                        $tax_summary = array();
                        $count = count($rows);
                        $limit = 4;
                        if($i == 0){
                                $start = 0;
                            }
                            else{
                                $start = ($i*$limit)+1;
                                $limit = 4+$limit;
                                $r = $start;
                            }
                           $total_net_amt = 0;
                        foreach ($rows as $row) {
                            $total_discounted_amount += ($row->quantity * $row->net_unit_price);
                            $total_net_amt += ($row->quantity * $row->net_unit_price) + $row->item_discount;
                            $total_amt += $row->unit_price;
                            $total_items +=$row->quantity;
                            $total_discount += $row->item_discount;
                            $total_tax += $row->item_tax;

                            if (isset($tax_summary[$row->tax_code])) {
                                $tax_summary[$row->tax_code]['items'] += $row->quantity;
                                $tax_summary[$row->tax_code]['tax'] += $row->item_tax;
                                $tax_summary[$row->tax_code]['amt'] += ($row->quantity * $row->net_unit_price) - $row->item_discount;
                            } else {
                                $tax_summary[$row->tax_code]['items'] = $row->quantity;
                                $tax_summary[$row->tax_code]['tax'] = $row->item_tax;
                                $tax_summary[$row->tax_code]['amt'] = ($row->quantity * $row->net_unit_price) - $row->item_discount;
                                $tax_summary[$row->tax_code]['name'] = $row->tax_name;
                                $tax_summary[$row->tax_code]['code'] = $row->tax_code;
                                $tax_summary[$row->tax_code]['rate'] = $row->tax_rate;
                            }
                            $total_tax = ($row->tax_value*$row->subtotal)/(100+$row->tax_value);
                            $subtotal_item = ($row->real_unit_price) * ($row->quantity);
                            ?>
                            
                            <tr style="background-color: #fff;">
                                <td align="left" style="padding:2px; vertical-align:top;"><?= $r.' '.product_name($row->product_name) . ($row->variant ? ' (' . $row->variant . ')' : ''); ?></td>
                                <td align="left" style="padding:2px; vertical-align:top;"><?=$row->lot_no;?></td>
                                <td align="left" style="padding:2px; vertical-align:top;"><?= date("d/m/Y", strtotime($row->date."+3years")); ?></td>
                                <td align="left" style="padding:2px; vertical-align:top;"><?= $this->sma->formatQuantity($row->quantity); ?> </td>
                                <td align="left" style="padding:2px;"><?=$this->pos_model->convertNumber($row->quantity);?></td>
                                <td align="left" style="padding:2px; vertical-align:top;">Packets</td>
                                <td align="left" style="padding:2px; vertical-align:top;"><?= $this->sma->formatMoney($row->unit_cost); ?></td>
                                <td align="left" style="padding:2px;"><?= $this->sma->formatMoney($row->unit_tax); ?></td>
                                <td align="left" style="padding:2px;">
                                <?= $this->sma->formatMoney($row->real_unit_price) ?>
                                </td>
                                <td align="left" style="padding:2px; vertical-align:top;"><?= $this->sma->formatMoney($total_tax) ?></td>
                                <td align="left" style="padding:2px; vertical-align:top;"><?= $this->sma->formatMoney($row->item_discount) ?></td>
                                <td align="left" style="padding:2px; vertical-align:top;"><?= $this->sma->formatMoney($subtotal_item); ?></td>
                            </tr>
                            <?php
                                    if($r == $limit){
                                        break;
                                    }
                                    if($count == $r){
                                        break;
                                    }

                                $r++;
                                //$start++; 
                                
                                }
                        
                        if ($pos_settings->rounding) { 
                            $round_total = $this->sma->roundNumber($inv->grand_total, $pos_settings->rounding);
                            $rounding = $this->sma->formatMoney($round_total - $inv->grand_total);
                        }
                        ?>
                            <tr class="no-bg">
                                <td colspan="7" style="padding:2px; vertical-align:top;"></td>
                                <td colspan="2" align="left" style="padding:2px; vertical-align:top;"><?=lang("total_amount");?></td>
                                <td colspan="3" align="left" style="padding:2px; vertical-align:top;"><strong><?= $this->sma->formatMoney($total_net_amt1); ?></strong></td>
                            </tr>

                            <tr class="no-bg">
                                <td colspan="7" style="padding:2px; vertical-align:top;"></td>
                                <td colspan="2" align="left" style="padding:2px; vertical-align:top;padding:2px; vertical-align:top;"><?=lang("discount");?></td>
                                <td colspan="3" align="left" style="padding:2px; vertical-align:top;"><strong><?= $this->sma->formatMoney($row->discount_value); ?></strong></td>
                            </tr>

                            <tr class="no-bg">
                                <td align="left" style="padding:2px; vertical-align:top;"><?= lang("in_words"); ?></td>
                                <td align="left" colspan="6" style="padding:2px; vertical-align:top;">
                                    <strong><?=$this->pos_model->convertWords($inv->grand_total + $rounding)?>Only</strong>
                                </td>
                                <td align="left" colspan="2" style="padding:2px; vertical-align:top;"><?=lang("total_pay_amount");?></td>
                                <td colspan="3" align="left" style="padding:2px; vertical-align:top;"><strong><?= $this->sma->formatMoney($inv->grand_total); ?></strong></td>
                            </tr>

                            <tr>
                                <td colspan="12" align="left" style="vertical-align: bottom; font-size: 10px;"><b>Payment Note:</b> &nbsp; &nbsp;
                                <?php 
                                // Update By Ankit
                                if(isset($payments[0]->note))
                                {
                                    echo $payments[0]->note;
                                }
                                
                                ?>

                                 </td>
                            </tr>
                            
                            <tr class="no-bg sign-height" style="line-height:45px !important;">
                                <td colspan="6" align="center" style="vertical-align: bottom;">______________________<br> <?=lang("cus_sign");?></td>
                                <td colspan="6" align="center" style="vertical-align: bottom;">____________________________ <br> <?=lang("sign_center");?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <div class="dash-border">&nbsp;</div>
        
        <div class="row">&nbsp;</div>
        <!--HELPER-copy-->
        <div id="receipt-data">
            
            <div class="row">
                <div class="text-center">
                    <table class="table table-striped table-condensed no-bg" cellspacing="0" border="1" cellpadding="1" style="font-size:10px;padding:2px !important;line-height:10px !important;">
                        <tbody>
                            <tr>
                                <td colspan="2" align="left" style="font-size:10px;padding:2px;vertical-align:top;"><?=lang("bill_run_time"); ?> <?=date("d/m/Y h:i:s", strtotime($inv->date));?></td>
                                <td colspan="4" align="left" style="font-size:10px;padding:2px;vertical-align:top;"><strong><?=lang("sahayak_copy"); ?></strong></td>
                            </tr>
                            
                            <tr class="no-bg">
                                <td colspan="4" align="center" style="padding:2px;vertical-align:top;"><?=lang("cash_memo");?> <?=$inv->reference_no;?></td>
                                <td colspan="2" style="padding:2px;vertical-align:top;">Date: <?=date("d/m/Y", strtotime($inv->date));?></td>
                            </tr>
                            
                            <tr class="head-bg">
                                <th align="left" style="background-color:#dbdbdb !important;padding:2px;vertical-align:top;"><?=lang("prod_qty"); ?></th>
                                <th align="left" style="background-color:#dbdbdb !important;padding:2px;vertical-align:top;"><?=lang("alot_no"); ?></th>
                                <th align="left" style="background-color:#dbdbdb !important;padding:2px;vertical-align:top;"><?=lang("nug");?></th>
                                <th align="left" style="background-color:#dbdbdb !important;padding:2px;vertical-align:top;"><?=lang("prod_qty"); ?></th>
                                <th align="left" style="background-color:#dbdbdb !important;padding:2px;vertical-align:top;"><?=lang("alot_no"); ?></th>
                                <th align="left" style="background-color:#dbdbdb !important;padding:2px;vertical-align:top;"><?=lang("nug");?></th>
                            </tr>
                          <tr style="background-color: #fff;">  
                        <?php
                        $r = 1;
                        $tax_summary = array();
                        $count = count($rows);
                        $limit = 4;
                        if($i == 0){
                                $start = 0;
                            }
                            else{
                                $start = ($i*$limit)+1;
                                $limit = 4+$limit;
                                $r = $start;
                            }
                        foreach ($rows as $row) {
                            if (isset($tax_summary[$row->tax_code])) {
                                $tax_summary[$row->tax_code]['items'] += $row->quantity;
                                $tax_summary[$row->tax_code]['tax'] += $row->item_tax;
                                $tax_summary[$row->tax_code]['amt'] += ($row->quantity * $row->net_unit_price) - $row->item_discount;
                            } else {
                                $tax_summary[$row->tax_code]['items'] = $row->quantity;
                                $tax_summary[$row->tax_code]['tax'] = $row->item_tax;
                                $tax_summary[$row->tax_code]['amt'] = ($row->quantity * $row->net_unit_price) - $row->item_discount;
                                $tax_summary[$row->tax_code]['name'] = $row->tax_name;
                                $tax_summary[$row->tax_code]['code'] = $row->tax_code;
                                $tax_summary[$row->tax_code]['rate'] = $row->tax_rate;
                            }?>
                            
                            
                                <td align="left" style="padding:2px;vertical-align:top;"><?= product_name($row->product_name) . ($row->variant ? ' (' . $row->variant . ')' : ''); ?></td>
                                <td align="left" style="padding:2px;vertical-align:top;"><?=$row->lot_no;?></td>
                                <td align="left" style="padding:2px;vertical-align:top;"><?= $this->sma->formatQuantity($row->quantity); ?> 
                                <?php
                                if($r%2==0)
                                {
                                    echo "</tr>";
                                    echo "<tr style='background-color: #fff;'>";
                                }

                                    if($r == $limit){
                                        break;
                                    }
                                    if($count == $r){
                                        break;
                                    }

                                $r++;
                                //$start++; 
                                
                                }
                        
                         ?>
                        </tr>
                            <tr class="no-bg">
                                <td colspan="6" align="left" style="padding:2px;vertical-align:top;">
                                    <p>Product has received in good condition.</p>
                                </td>
                            </tr>
                            
                            <tr class="no-bg">
                                <td colspan="3" align="center" style="vertical-align: bottom;height:35px !important;">______________________<br> <?=lang("cus_sign");?></td>
                                <td colspan="3" align="center" style="vertical-align: bottom;height:35px !important;">_____________________________ <br> <?=lang("sign_center");?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
    </div>
    <?php
}
?>


</div>
<?php if ($modal) {
    echo '</div></div></div>';
}else { ?>
<div id="buttons" style="padding-top:10px; text-transform:uppercase;" class="no-print">
    <?php if ($pos_settings->java_applet) { ?>
        <span class="col-xs-12"><a class="btn btn-block btn-primary" onClick="printReceipt()"><?= lang("print"); ?></a></span>
        <span class="col-xs-12"><a class="btn btn-block btn-info" type="button" onClick="openCashDrawer()">Open Cash
                Drawer</a></span>
        <div style="clear:both;"></div>
    <?php } else { ?>
        <span class="pull-right col-xs-12">
        <a href="javascript:window.print()" id="web_print" class="btn btn-block btn-primary"
           onClick="window.print();return false;"><?= lang("web_print"); ?></a>
    </span>
    <?php } ?>
    <span class="pull-left col-xs-12"><a class="btn btn-block btn-success" href="<?= site_url('pos/save_invoice_pdf/'.$sid); ?>" ><?= lang("save_as_pdf"); ?></a></span>

    <span class="col-xs-12">
        <!--Add logout functionality after payment @ ankit-->
        <?php if($pos_settings->logout_after_payment=='1'){  ?>
        <a class="btn btn-block btn-warning" href="<?= site_url('auth/logout'); ?>"><?= lang("back_to_pos"); ?></a>
        <?php } else { ?>
        <a class="btn btn-block btn-warning" href="<?= site_url('pos'); ?>"><?= lang("back_to_pos"); ?></a>
        <?php } ?>
    </span>
    <?php if (!$pos_settings->java_applet) { ?>
        <div style="clear:both;"></div>
        <div class="col-xs-12 text-center" style="background:#F5F5F5; padding:10px;">
            <p style="font-weight:bold;">Please don't forget to disble the header and footer in browser print
                settings.</p>

            <p style="text-transform: capitalize;"><strong>FF:</strong> File &gt; Print Setup &gt; Margin &amp;
                Header/Footer Make all --blank--</p>

            <p style="text-transform: capitalize;"><strong>chrome:</strong> Menu &gt; Print &gt; Disable Header/Footer
                in Option &amp; Set Margins to None</p></div>
    <?php } ?>
    <div style="clear:both;"></div>

</div>
    </body>
    </html>

    <?php } ?>