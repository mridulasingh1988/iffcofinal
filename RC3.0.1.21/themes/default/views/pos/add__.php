<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= lang('pos_module') . " | " . $Settings->site_name; ?></title>
    <script type="text/javascript">if(parent.frames.length !== 0){top.location = '<?= site_url('pos') ?>';}</script>
    <base href="<?= base_url() ?>"/>
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <link rel="shortcut icon" href="<?= $assets ?>images/icon.png"/>
    <link rel="stylesheet" href="<?= $assets ?>styles/theme.css" type="text/css"/>
    <link rel="stylesheet" href="<?= $assets ?>styles/style.css" type="text/css"/>
    <link rel="stylesheet" href="<?= $assets ?>pos/css/posajax.css" type="text/css"/>
    <link href="<?= $assets ?>styles/bootstrap-glyphicons.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= $assets ?>pos/css/print.css" type="text/css" media="print"/>
    <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="<?= $assets ?>js/jquery-migrate-1.2.1.min.js"></script>
    <!--[if lt IE 9]>
    <script src="<?= $assets ?>js/jquery.js"></script>
    <![endif]-->
    <?php if ($Settings->rtl) { ?>
        <link href="<?= $assets ?>styles/helpers/bootstrap-rtl.min.css" rel="stylesheet"/>
        <link href="<?= $assets ?>styles/style-rtl.css" rel="stylesheet"/>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.pull-right, .pull-left').addClass('flip');
            });
        </script>
    <?php } ?>
</head>
<body>
<noscript>
    <div class="global-site-notice noscript">
        <div class="notice-inner">
            <p><strong>JavaScript seems to be disabled in your browser.</strong><br>You must have JavaScript enabled in
                your browser to utilize the functionality of this website.</p>
        </div>
    </div>
</noscript>

<div id="wrapper">
    <header id="header" class="navbar">
        <div class="container">
            <a class="navbar-brand" href="<?= site_url() ?>"><span class="logo"><span class="pos-logo-lg"><?= $Settings->site_name ?></span><span class="pos-logo-sm"><?= lang('pos') ?></span></span></a>

            <div class="header-nav">
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown">
                        <a class="btn account dropdown-toggle" data-toggle="dropdown" href="#">
                            <img alt="" src="<?= $this->session->userdata('avatar') ? site_url() . 'assets/uploads/avatars/thumbs/' . $this->session->userdata('avatar') : $assets . 'images/' . $this->session->userdata('gender') . '.png'; ?>" class="mini_avatar img-rounded">

                            <div class="user">
                                <span><?= lang('welcome') ?>! <?= $this->session->userdata('username'); ?></span>
                            </div>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <!-- <li>
                                <a href="<?= site_url('auth/profile/' . $this->session->userdata('user_id')); ?>">
                                    <i class="fa fa-user"></i> <?= lang('profile'); ?>
                                </a>
                            </li> -->
                            <!-- <li>
                                <a href="<?= site_url('auth/profile/' . $this->session->userdata('user_id') . '/#cpassword'); ?>">
                                    <i class="fa fa-key"></i> <?= lang('change_password'); ?>
                                </a>
                            </li> -->
                            <!-- <li class="divider"></li> -->
                            <li>
                                <a href="<?= site_url('auth/logout'); ?>">
                                    <i class="fa fa-sign-out"></i> <?= lang('logout'); ?>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>

                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown">
                        <a class="btn bblue pos-tip" title="<?= lang('dashboard') ?>" data-placement="left" href="<?= site_url('welcome') ?>">
                            <i class="fa fa-dashboard"></i>
                        </a>
                    </li>
                    <?php if ($Owner) { ?>
                        <li class="dropdown hidden-sm">
                            <a class="btn pos-tip" title="<?= lang('settings') ?>" data-placement="left" href="<?= site_url('pos/settings') ?>">
                                <i class="fa fa-cogs"></i>
                            </a>
                        </li>
                    <?php } ?>
                    <li class="dropdown hidden-xs">
                        <a class="btn pos-tip" title="<?= lang('calculator') ?>" data-placement="left" href="#" data-toggle="dropdown">
                            <i class="fa fa-calculator"></i>
                        </a>
                        <ul class="dropdown-menu pull-right calc">
                            <li class="dropdown-content">
                                <span id="inlineCalc"></span>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown hidden-sm">
                        <a class="btn pos-tip" title="<?= lang('shortcuts') ?>" data-placement="left" href="#" data-toggle="modal" data-target="#sckModal">
                            <i class="fa fa-key"></i>
                        </a>
                    </li>
                   <!-- <li class="dropdown">
                        <a class="btn pos-tip" title="<?= lang('view_bill_screen') ?>" data-placement="bottom" href="<?= site_url('pos/view_bill') ?>" target="_blank">
                            <i class="fa fa-laptop"></i>
                        </a> -->
                    </li>
                    <li class="dropdown">
                        <a class="btn blightOrange pos-tip" id="opened_bills" title="<span><?= lang('suspended_sales') ?></span>" data-placement="bottom" data-html="true" href="<?= site_url('pos/opened_bills') ?>" data-toggle="ajax">
                            <i class="fa fa-th"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a class="btn bdarkGreen pos-tip" id="register_details" title="<span><?= lang('register_details') ?></span>" data-placement="bottom" data-html="true" href="<?= site_url('pos/register_details') ?>" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-check-circle"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a class="btn borange pos-tip" id="close_register" title="<span><?= lang('close_register') ?></span>" data-placement="bottom" data-html="true" href="<?= site_url('pos/close_register') ?>" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-times-circle"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a class="btn borange pos-tip" id="add_expense" title="<span><?= lang('add_expense') ?></span>" data-placement="bottom" data-html="true" href="<?= site_url('purchases/add_expense') ?>" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-dollar"></i>
                        </a>
                    </li>
                    <?php if ($Owner) { ?>
                        <li class="dropdown">
                            <a class="btn bdarkGreen pos-tip" id="today_profit" title="<span><?= lang('today_profit') ?></span>" data-placement="bottom" data-html="true" href="<?= site_url('reports/profit') ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-hourglass-half"></i>
                            </a>
                        </li>
                    <?php } ?>
                    <?php if ($Owner || $Admin) { ?>
                        <li class="dropdown">
                            <a class="btn bdarkGreen pos-tip" id="today_sale" title="<span><?= lang('today_sale') ?></span>" data-placement="bottom" data-html="true" href="<?= site_url('pos/today_sale') ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-heart"></i>
                            </a>
                        </li>
                        <li class="dropdown hidden-xs">
                            <a class="btn bblue pos-tip" title="<?= lang('list_open_registers') ?>" data-placement="bottom" href="<?= site_url('pos/registers') ?>">
                                <i class="fa fa-list"></i>
                            </a>
                        </li>
                        <li class="dropdown hidden-xs">
                            <a class="btn bred pos-tip" title="<?= lang('clear_ls') ?>" data-placement="bottom" id="clearLS" href="#">
                                <i class="fa fa-eraser"></i>
                            </a>
                        </li>
                    <?php } ?>
                </ul>

                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown">
                        <a class="btn bblack" style="cursor: default;"><span id="display_time"></span></a>
                    </li>
                </ul>
            </div>
        </div>
    </header>

    <div id="content">
        <div class="c1">
            <div class="pos">
                <?php
                if ($error) {
                    echo "<div class=\"alert alert-danger\"><button type=\"button\" class=\"close fa-2x\" data-dismiss=\"alert\">&times;</button>" . $error . "</div>";
                }
                ?>
                <?php
                if ($message) {
                    echo "<div class=\"alert alert-success\"><button type=\"button\" class=\"close fa-2x\" data-dismiss=\"alert\">&times;</button>" . $message . "</div>";
                }
                ?>
                <div id="pos">
                    <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'pos-sale-form');
                    echo form_open("pos", $attrib); ?>
                    <div id="leftdiv">
                        <div id="printhead">
                            <h4 style="text-transform:uppercase;"><?php echo $Settings->site_name; ?></h4>
                            <?php
                            echo "<h5 style=\"text-transform:uppercase;\">" . $this->lang->line('order_list') . "</h5>";
                            echo $this->lang->line("date") . " " . $this->sma->hrld(date('Y-m-d H:i:s'));
                            ?>
                        </div>
                        <div id="left-top">
           
                            <div
                                style="position: absolute; <?= $Settings->rtl ? 'right:-9999px;' : 'left:-9999px;'; ?>"><?php echo form_input('test', '', 'id="test" class="kb-pad"'); ?></div>
                            <div class="row">
                                <div class="col-xs-8 col-md-8">
                                    <div class="form-group">

                                        <?php if ($Owner || $Admin || $GP['customers-add']) { ?><div class="input-group"><?php } ?>
                                            <script>
                                            localStorage.setItem("poscustomer",'<?php echo (!empty($_GET['customer'])) ? $_GET['customer'] : $pos_settings->default_customer;?>');
                                            </script>
                                            <?php
                                           
                                            echo form_input('customer', (!empty($_GET['customer']) ? $_GET['customer'] : $pos_settings->default_customer), 'id="poscustomer" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("customer") . '" required="required" class="form-control pos-input-tip" style="width:100%;"');
                                            ?>
                                            <?php if ($Owner || $Admin || $GP['customers-add']) { ?>
                                            <div class="input-group-addon no-print" style="padding: 2px 5px;">
                                                <a href="<?= site_url('customers/add/pos'); ?>" id="add-customer" class="external" data-toggle="modal" data-target="#myModal">
                                                    <i class="fa fa-2x fa-plus-circle" id="addIcon"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <div style="clear:both;"></div>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-md-4">
                                   <?php
                                       
                                        $billers = array($_SESSION['biller_id']=>'My Store','all'=>'All Stores');

                                        echo form_dropdown('biller',$billers,$_SESSION['biller_id'],'class="form-control" id="customer_biller"');
                                   ?>
                                </div>
                            </div>
                            <div class="no-print">
                                <?php if ($Owner || $Admin || $Sales_staff) {
                                     ?>
                                    <div class="form-group">
                                      
                                      <!--added by vikas singh to reset warehouse id on refresh -->
                                        <script>
                                            localStorage.setItem("poswarehouse",'<?php echo (!empty($_GET['poswarehouse'])) ? $_GET['poswarehouse'] : $warehouses[0]['id'];?>');
                                        </script>
                                           <?php
                                        $wh = array();
                                      //print_r($warehouses);exit;
                                        foreach ($warehouses as $warehouse) {
                                        //    print_r($warehouse); die;
                                           
                                            $wh[$warehouse['id']] = $warehouse['name']."(".$warehouse['wh_id'].")";
                                        }
               
                                        echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $warehouses[0]['id']), 'id="poswarehouse" class="form-control pos-input-tip" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("warehouse") . '" required="required" style="width:100%;" ');
                                        ?>
                                    </div>
                                <?php } else {

                                    $warehouse_input = array(
                                        'type' => 'hidden',
                                        'name' => 'warehouse',
                                        'id' => 'poswarehouse',
                                        'value' => $this->session->userdata('warehouse_id'),
                                    );

                                    echo form_input($warehouse_input);
                                } ?>
                                <div class="form-group" id="ui">
                                    <?php if ($Owner || $Admin || $GP['products-add']) { ?><div class="input-group"><?php } ?>
                                        <?php echo form_input('add_item', '', 'class="form-control pos-tip" id="add_item" data-placement="top" data-trigger="focus" placeholder="' . $this->lang->line("search_product_by_name_code") . '" title="' . $this->lang->line("au_pr_name_tip") . '"'); ?>
                                        <?php if ($Owner || $Admin || $GP['products-add']) { ?>
                                        <div class="input-group-addon" style="padding: 2px 5px;">
                                            <a href="#" id="addManually">
                                                <i class="fa fa-2x fa-plus-circle" id="addIcon"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <div style="clear:both;"></div>
                                </div>
                            </div>
                        </div>
                        <div id="print">
                            <div id="left-middle">
                                <div id="product-list">
                                    <table class="table items table-striped table-bordered table-condensed table-hover"
                                           id="posTable" style="margin-bottom: 0;">
                                        <thead>
                                        <tr>
                                            <th width="40%"><?= lang("product"); ?></th>
                                            <th width="15%"><?= lang("price"); ?></th>
                                            <?php if($this->pos_settings->enable_discount){ ?>
                                            <th width="15%"><?= lang("discount"); ?></th>
                                            <?php }?>
                                            <th width="15%"><?= lang("qty"); ?></th>
                                            <th width="20%"><?= lang("subtotal"); ?></th>
                                            <th style="width: 5%; text-align: center;"><i class="fa fa-trash-o"
                                                                                          style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <div style="clear:both;"></div>
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                            <div id="left-bottom">
                                <table id="totalTable"
                                       style="width:100%; float:right; padding:5px; color:#000; background: #FFF;">
                                    <tr>
                                        <td style="padding: 5px 10px;"><?= lang('items'); ?></td>
                                        
                                        <td class="text-right" style="padding: 5px 10px;font-size: 14px; font-weight:bold;">
                                            <span id="titems">0</span>
                                        </td>
                                        <td style="padding: 5px 10px;"><?= lang('total'); ?></td>
                                        <td class="text-right" style="padding: 5px 10px;font-size: 14px; font-weight:bold;">
                                            <span id="total">0.00</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 5px 10px;">
                                      <!-- commented by vikas singh to hide order tax -->      
                                      <!-- <?= lang('order_tax'); ?> -->
<!--                                            <a href="#" id="pptax2">
                                                <i class="fa fa-edit"></i>
                                            </a>-->
                                        </td>
                                        <td class="text-right" style="padding: 5px 10px;font-size: 14px; font-weight:bold;">
<!--                                            <span id="ttax2">0.00</span>-->
                                            <!-- end by vikas singh order tax hide -->
                                        </td>
                                        <?php if($this->pos_settings->enable_discount){ ?>
                                        <td style="padding: 5px 10px;"><?= lang('discount'); ?>
                                            <a href="#" id="ppdiscount">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </td>
                                        <td class="text-right" style="padding: 5px 10px;font-weight:bold;">
                                            <span id="tds">0.00</span>
                                        </td>
                                        <?php } ?>
                                        
                                    </tr>
                                    <tr>
                                        <td style="padding: 5px 10px; border-top: 1px solid #666; font-weight:bold; background:#333; color:#FFF;" colspan="2">
                                            <?= lang('total_payable'); ?>
                                        </td>
                                        <td class="text-right" style="padding:5px 10px 5px 10px; font-size: 14px;border-top: 1px solid #666; font-weight:bold; background:#333; color:#FFF;" colspan="2">
                                            <span id="gtotal">0.00</span>
                                        </td>
                                    </tr>
                                </table>

                                <div class="clearfix"></div>
                                <div id="botbuttons" style="text-align:center;">
                                    <input type="hidden" name="biller" id="biller"
                                           value="<?= ($Owner || $Admin) ? $pos_settings->default_biller : $this->session->userdata('biller_id') ?>"/>

                                    <div class="btn-group btn-group-justified">
                                        <div class="btn-group">
                                            <div class="btn-group btn-group-justified">
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-danger"
                                                            id="reset"><?= lang('cancel'); ?></button>
                                                </div>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-warning"
                                                            id="suspend"><?= lang('suspend'); ?></button>
                                                </div>
                                            </div>
                                        </div>
                                       <!-- <div class="btn-group">
                                            <div class="btn-group btn-group-justified">
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary" id="print_order">
                                                        <i class="fa fa-print"></i> <?= lang('order'); ?>
                                                    </button>
                                                </div>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary" id="print_bill">
                                                        <i class="fa fa-print"></i> <?= lang('bill'); ?>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>-->
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-success" id="payment">
                                                <i class="fa fa-money"></i> <?= lang('payment'); ?>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div style="clear:both; height:5px;"></div>
                                <div id="num">
                                    <div id="icon"></div>
                                </div>
                                <span id="hidesuspend"></span>
                                <input type="hidden" name="pos_note" value="" id="pos_note">
                                <input type="hidden" name="staff_note" value="" id="staff_note">
                                <!-- added by vikas singh to get discount status -->
                                <input name="is_discount_enable" value="<?php echo $this->pos_settings->enable_discount;?>" type="hidden" id="is_discount_enable"/>

                                <div id="payment-con">
                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                        <input type="hidden" name="amount[]" id="amount_val_<?= $i ?>" value=""/>
                                        <input type="hidden" name="balance_amount[]" id="balance_amount_<?= $i ?>" value=""/>
                                        <input type="hidden" name="paid_by[]" id="paid_by_val_<?= $i ?>" value="cash"/>
                                        <input type="hidden" name="cc_no[]" id="cc_no_val_<?= $i ?>" value=""/>
                                        <input type="hidden" name="paying_gift_card_no[]" id="paying_gift_card_no_val_<?= $i ?>" value=""/>
										
										 <input type="hidden" name="paying_credit_voucher_no[]" id="paying_credit_voucher_no_val_<?= $i ?>" value=""/>
										 
                                        <input type="hidden" name="cc_holder[]" id="cc_holder_val_<?= $i ?>" value=""/>
                                        <input type="hidden" name="cheque_no[]" id="cheque_no_val_<?= $i ?>" value=""/>
                                        <input type="hidden" name="cc_month[]" id="cc_month_val_<?= $i ?>" value=""/>
                                        <input type="hidden" name="cc_year[]" id="cc_year_val_<?= $i ?>" value=""/>
                                        <input type="hidden" name="cc_type[]" id="cc_type_val_<?= $i ?>" value=""/>
                                        <input type="hidden" name="cc_cvv2[]" id="cc_cvv2_val_<?= $i ?>" value=""/>
                                        <input type="hidden" name="payment_note[]" id="payment_note_val_<?= $i ?>" value=""/>
                                    <?php } ?>
                                </div>
                                <input name="order_tax" type="hidden" value="<?= $suspend_sale ? $suspend_sale->order_tax_id : $Settings->default_tax_rate2; ?>" id="postax2">
                                <input name="discount" type="hidden" value="<?= $suspend_sale ? $suspend_sale->order_discount_id : ''; ?>" id="posdiscount">
                                <input type="hidden" name="rpaidby" id="rpaidby" value="cash" style="display: none;"/>
                                <input type="hidden" name="total_items" id="total_items" value="0" style="display: none;"/>
                                <input type="submit" id="submit_sale" value="Submit Sale" style="display: none;"/>
                            </div>
                        </div>

                    </div>
                    <?php echo form_close(); ?>
                    <div id="cp">
                        <div id="cpinner">
                            <div class="quick-menu">
                                <div id="proContainer">
                                    <div id="ajaxproducts">
                                        <div id="item-list">
                                            <?php echo $products; ?>
                                        </div>

                                       <div class="btn-group btn-group-justified">

                                            <div class="btn-group">
                                                <button style="z-index:10002;" class="btn btn-primary pos-tip" title="<?= lang('previous') ?>" type="button" id="previous">
                                                    <i class="fa fa-chevron-left"></i>
                                                </button>
                                            </div>

                                            <?php //if ($Owner || $Admin || $GP['sales-add_gift_card']) { ?>
											<?php if ($Owner) { ?>

                                            <div class="btn-group">
                                                <button style="z-index:10003;" class="btn btn-primary pos-tip" type="button" id="sellGiftCard" title="<?= lang('sell_gift_card') ?>">
                                                    <i class="fa fa-credit-card" id="addIcon"></i> <?= lang('sell_gift_card') ?>
                                                </button>
                                            </div>
                                            <?php } ?>
                                            <div class="btn-group">
                                                <button style="z-index:10004;" class="btn btn-primary pos-tip" title="<?= lang('next') ?>" type="button" id="next">
                                                    <i class="fa fa-chevron-right"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="clear:both;"></div>
                                </div>
                            </div>
                        </div>
                        <div style="clear:both;"></div>
                    </div>
                    <div style="clear:both;"></div>
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
    </div>
</div>
<div class="rotate btn-cat-con">
    <button type="button" id="open-endcategory" class="btn btn-primary open-endcategory">Endcategories</button>
    <button type="button" id="open-subcategory" class="btn btn-warning open-subcategory">Subcategories</button>
    <button type="button" id="open-category" class="btn btn-primary open-category">Categories</button>
</div>
<div id="category-slider">
    <!--<button type="button" class="close open-category"><i class="fa fa-2x">&times;</i></button>-->
    <div id="category-list">
        <?php
        //for ($i = 1; $i <= 40; $i++) {
        foreach ($Ncategories as $category) {
            echo "<button id=\"category-" . $category->id . "\" type=\"button\" value='" . $category->id . "' class=\"btn-prni category\" ><img src=\"assets/uploads/thumbs/" . ($category->image ? $category->image : 'no_image.png') . "\" style='width:40px;height:40px;' class='img-rounded img-thumbnail' /><span>" . $category->name . "</span></button>";
        }
        //}
        ?>
    </div>
</div>
<div id="subcategory-slider">
    <!--<button type="button" class="close open-category"><i class="fa fa-2x">&times;</i></button>-->
    <div id="subcategory-list">
        <?php
        if (!empty($subcategories)) {
            foreach ($subcategories as $category) {
                echo "<button id=\"subcategory-" . $category->id . "\" type=\"button\" value='" . $category->id . "' class=\"btn-prni subcategory\" ><img src=\"assets/uploads/thumbs/" . ($category->image ? $category->image : 'no_image.png') . "\" style='width:40px;height:40px;' class='img-rounded img-thumbnail' /><span>" . $category->name . "</span></button>";
            }
        }
        ?>
    </div>
</div>
<div id="endcategory-slider">
    <!--<button type="button" class="close open-category"><i class="fa fa-2x">&times;</i></button>-->
    <div id="endcategory-list">
        <?php
        if (!empty($endcategories)) {
            foreach ($endcategories as $category) {
                echo "<button id=\"endcategory-" . $category->id . "\" type=\"button\" value='" . $category->id . "' class=\"btn-prni endcategory\" ><img src=\"assets/uploads/thumbs/" . ($category->image ? $category->image : 'no_image.png') . "\" style='width:width:40px;height:40px;' class='img-rounded img-thumbnail' /><span>" . $category->name . "</span></button>";
            }
        }
        ?>
    </div>
</div>
<div class="modal fade in" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="payModalLabel"
     aria-hidden="true">

  <!--added by vikas singh 12-10-2016 -->
     <?php $attribs = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'pos-payment-form');
                    echo form_open("", $attribs);
                   ?>
  
   <!--end added by vikas singh 12-10-2016 -->
   
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="payModalLabel"><?= lang('finalize_sale'); ?></h4>
            </div>
            <div class="modal-body" id="payment_content">
                <div class="row">
                    <div class="col-md-10 col-sm-9">
                        <?php if ($Owner || $Admin) { ?>
                            <div class="form-group">
                                <?= lang("biller", "biller"); ?>
                                <?php
                                foreach ($billers as $biller) {
                                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                }
                                echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $pos_settings->default_biller), 'class="form-control" id="posbiller" required="required"');
                                ?>
                            </div>
                        <?php } else {
                            $biller_input = array(
                                'type' => 'hidden',
                                'name' => 'biller',
                                'id' => 'posbiller',
                                'value' => $this->session->userdata('biller_id'),
                            );

                            echo form_input($biller_input);
                        }
                        ?>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <?= form_textarea('sale_note', '', 'id="sale_note" class="form-control kb-text skip" style="height: 100px;" placeholder="' . lang('sale_note') . '" maxlength="250"'); ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= form_textarea('staffnote', '', 'id="staffnote" class="form-control kb-text skip" style="height: 100px;" placeholder="' . lang('staff_note') . '" maxlength="250"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="clearfir"></div>
                        <div id="payments">
                            <div class="well well-sm well_1">
                                <div class="payment">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <?= lang("amount", "amount_1"); ?>

                                                <input name="amountc[0]" type="text" id="amount_1" required="required"

                                                       class="pa form-control kb-pad amount"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 col-sm-offset-1">
                                            <div class="form-group">
                                                <?= lang("paying_by", "paid_by_1"); ?>
                                                <select name="paid_by1[0]" id="paid_by_1" class="form-control paid_by">
                                                   <option value="ss">Select an option</option>
                                                   <option value="cash" selected="selected"><?= lang("cash"); ?></option>

                                                    <option value="CC"><?= lang("cc"); ?></option>
                                                    <option value="Cheque"><?= lang("cheque"); ?></option>
<!--                                                    <option value="gift_card"><?= lang("gift_card"); ?></option>-->
<!--						    <option value="credit_voucher"><?= lang("credit_voucher"); ?></option>-->
                                                    <?= $pos_settings->paypal_pro ? '<option value="ppp">' . lang("paypal_pro") . '</option>' : ''; ?>
                                                    <?= $pos_settings->stripe ? '<option value="stripe">' . lang("stripe") . '</option>' : ''; ?>
<!--                                                    <option value="other"><?= lang("other"); ?></option>-->

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-11">
                                            <div class="form-group gc_1" style="display: none;">
                                                <?= lang("gift_card_no", "gift_card_no_1"); ?>

                                                <input name="cpaying_gift_card_no[0]" type="text" id="gift_card_no_1"
                                                       class="pa form-control kb-pad gift_card_no" required="required"/>


                                                <div id="gc_details_1"></div>
                                            </div>
											

						<div class="form-group cv_1" style="display: none;">
                                                <?= lang("credit_voucher_no", "credit_voucher_no_1"); ?>
                                                <input name="cpaying_credit_voucher_no[0]" type="text" id="credit_voucher_no_1"
                                                       class="pa form-control kb-pad credit_voucher_no" required="required"/>


                                                <div id="cv_details_1"></div>
                                            </div>
											
                                            <div class="pcc_1" style="display:none;">
                                                <div class="form-group">
                                                    <input type="text" name="swipe[0]" id="swipe_1" class="form-control swipe"
                                                           placeholder="<?= lang('swipe') ?>"/>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">

                                                      <input name="ccc_no[0]" type="text" id="pcc_no_1"
                                                             class="form-control cardno" required="required" maxlength="20"

                                                                   placeholder="<?= lang('cc_no') ?>"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">


                                                            <input name="ccc_holer[0]" type="text" id="pcc_holder_1"
                                                                   class="form-control cc_holder" required="required"

                                                                   placeholder="<?= lang('cc_holder') ?>"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">

                                                            <select name="ccc_type[0]" id="pcc_type_1"
                                                                    class="form-control pcc_type" required="required"

                                                                    placeholder="<?= lang('card_type') ?>">
                                                                <option value="Visa"><?= lang("Visa"); ?></option>
                                                                <option
                                                                    value="MasterCard"><?= lang("MasterCard"); ?></option>
                                                                <option value="Amex"><?= lang("Amex"); ?></option>
                                                                <option
                                                                    value="Discover"><?= lang("Discover"); ?></option>
                                                            </select>
                                                            <!-- <input type="text" id="pcc_type_1" class="form-control" placeholder="<?= lang('card_type') ?>" />-->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">

                                                            <?php 
                                                                 $months = array('' => 'Month',
                                                                 '01'  => '01',
                                                                 '02'  => '02',
                                                                 '03'  => '03',
                                                                 '04'  => '04',
                                                                 '05'  => '05',
                                                                 '06'  => '06',
                                                                 '07'  => '07',
                                                                 '08'  => '08',
                                                                 '09'  => '09',
                                                                 '10' => '10',
                                                                 '11' => '11',
                                                                 '12' => '12'
                                                                );
                                                                 
                                                               echo form_dropdown('ccc_month[0]',$months,$current_month, 'id="pcc_month_1"','class="form-control"','style="width:100%;"');
                                                                ?>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">


                                                            <?php

                                                             $days[''] = 'Day';         
                                                             for($i=1;$i<=31;$i++){
                                                                $days[$i] = $i;
                                                             }

                                                            $start_year = date("Y");

                                                             $years[''] = 'Year';

                                                             for ($i=$start_year;$i<=date("Y")+20;++$i) {
                                                                $years[$i] = $i;
                                                             } 

                                                             echo form_dropdown('ccc_year[0]',$years,'','id="pcc_year_1"','class="form-control"','required="required"');
                                                                ?>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">


                                                            <input name="ccc_cvv2[]" type="text" id="pcc_cvv2_1"
                                                                   class="form-control" required="required"
                                                                   placeholder="<?= lang('cvv2') ?>"/>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pcheque_1" style="display:none;">
                                                <div class="form-group"><?= lang("cheque_no", "cheque_no_1"); ?>

                                                    <input name="ccheque_no[0]" type="text" id="cheque_no_1"
                                                           class="form-control cheque_no" maxlength="6"/>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <?= lang('payment_note', 'payment_note'); ?>
                                                <textarea name="payment_note[0]" id="payment_note_1"
                                                          class="pa form-control kb-text payment_note"></textarea>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="multi-payment"></div>
                        <button type="button" class="btn btn-primary col-md-12 addButton"><i
                                class="fa fa-plus"></i> <?= lang('add_more_payments') ?></button>
                        <div style="clear:both; height:15px;"></div>
                        <div class="font16">
                            <table class="table table-bordered table-condensed table-striped" style="margin-bottom: 0;">
                                <tbody>
                                <tr>
                                    <td width="25%"><?= lang("total_items"); ?></td>
                                    <td width="25%" class="text-right"><span id="item_count">0.00</span></td>
                                    <td width="25%"><?= lang("total_payable"); ?></td>
                                    <td width="25%" class="text-right"><span id="twt">0.00</span></td>
                                </tr>
                                <tr>
                                    <td><?= lang("total_paying"); ?></td>
                                    <td class="text-right"><span id="total_paying">0.00</span></td>
                                    <td><?= lang("balance"); ?></td>
                                    <td class="text-right"><span id="balance">0.00</span></td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-3 text-center">
                        <span style="font-size: 1.2em; font-weight: bold;"><?= lang('quick_cash'); ?></span>

                        <div class="btn-group btn-group-vertical">
                            <button type="button" class="btn btn-lg btn-info quick-cash" id="quick-payable">0.00
                            </button>
                            <?php
                            foreach (lang('quick_cash_notes') as $cash_note_amount) {
                                echo '<button type="button" class="btn btn-lg btn-warning quick-cash">' . $cash_note_amount . '</button>';
                            }
                            ?>
                            <button type="button" class="btn btn-lg btn-danger"
                                    id="clear-cash-notes"><?= lang('clear'); ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-block btn-lg btn-primary" id="submit-sale"><?= lang('submit'); ?></button>
            </div>
        </div>
    </div>

    <!--added by vikas singh 12-10-2016 -->
     <?php echo form_close(); ?>
     <!--end added by vikas singh 12-10-2016 -->

</div>

<div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="prModalLabel"></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= lang('product_tax') ?></label>
                            <div class="col-sm-8">
                                <?php
                                $tr[""] = "";
                                
                                foreach ($tax_rates as $tax) {
                                    $tr[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('ptax', $tr, "", 'id="ptax" class="form-control pos-input-tip" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($Settings->product_serial) { ?>
                        <div class="form-group">
                            <label for="pserial" class="col-sm-4 control-label"><?= lang('serial_no') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control kb-text" id="pserial">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="pquantity" class="col-sm-4 control-label"><?= lang('quantity') ?></label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control kb-pad" id="pquantity">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="poption" class="col-sm-4 control-label"><?= lang('product_option') ?></label>

                        <div class="col-sm-8">
                            <div id="poptions-div"></div>
                        </div>
                    </div>
                    <?php if ($Settings->product_discount) { ?>
                        <div class="form-group">
                            <label for="pdiscount"
                                   class="col-sm-4 control-label"><?= lang('product_discount') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control kb-pad" id="pdiscount">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="pprice" class="col-sm-4 control-label"><?= lang('unit_price') ?></label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control kb-pad" id="pprice">
                        </div>
                    </div>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th style="width:25%;"><?= lang('net_unit_price'); ?></th>
                            <th style="width:25%;"><span id="net_price"></span></th>
                            <th style="width:25%;"><?= lang('product_tax'); ?></th>
                            <th style="width:25%;"><span id="pro_tax"></span></th>
                        </tr>
                    </table>
                    <input type="hidden" id="punit_price" value=""/>
                    <input type="hidden" id="old_tax" value=""/>
                    <input type="hidden" id="old_qty" value=""/>
                    <input type="hidden" id="old_price" value=""/>
                    <input type="hidden" id="row_id" value=""/>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="editItem"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="gcModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                        class="fa fa-2x">&times;</i></button>
                <h4 class="modal-title" id="myModalLabel"><?= lang('sell_gift_card'); ?></h4>
            </div>
            <div class="modal-body">
                <p><?= lang('enter_info'); ?></p>

                <div class="alert alert-danger gcerror-con" style="display: none;">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <span id="gcerror"></span>
                </div>
                <div class="form-group">
                    <?= lang("card_no", "gccard_no"); ?> *
                    <div class="input-group">
                        <?php echo form_input('gccard_no', '', 'class="form-control" id="gccard_no"'); ?>
                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                            <a href="#" id="genNo"><i class="fa fa-cogs"></i></a>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="gcname" value="<?= lang('gift_card') ?>" id="gcname"/>

                <div class="form-group">
                    <?= lang("value", "gcvalue"); ?> *
                    <?php echo form_input('gcvalue', '', 'class="form-control" id="gcvalue"'); ?>
                </div>
                <div class="form-group">
                    <?= lang("price", "gcprice"); ?> *
                    <?php echo form_input('gcprice', '', 'class="form-control" id="gcprice"'); ?>
                </div>
                <div class="form-group">
                    <?= lang("customer", "gccustomer"); ?>
                    <?php echo form_input('gccustomer', '', 'class="form-control" id="gccustomer"'); ?>
                </div>
                <div class="form-group">
                    <?= lang("expiry_date", "gcexpiry"); ?>
                    <?php echo form_input('gcexpiry', '', 'class="form-control date" id="gcexpiry"'); ?>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" id="addGiftCard" class="btn btn-primary"><?= lang('sell_gift_card') ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="mModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="mModalLabel"><?= lang('add_product_manually') ?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="mcode" class="col-sm-4 control-label"><?= lang('product_code') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control kb-text" id="mcode">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mname" class="col-sm-4 control-label"><?= lang('product_name') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control kb-text" id="mname">
                        </div>
                    </div>
                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group">
                            <label for="mtax" class="col-sm-4 control-label"><?= lang('product_tax') ?> *</label>

                            <div class="col-sm-8">
                                <?php
                                $tr[""] = "";
                                foreach ($tax_rates as $tax) {
                                    $tr[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('mtax', $tr, "", 'id="mtax" class="form-control pos-input-tip" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="mquantity" class="col-sm-4 control-label"><?= lang('quantity') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control kb-pad" id="mquantity">
                        </div>
                    </div>
                    <?php if ($Settings->product_discount) { ?>
                        <div class="form-group">
                            <label for="mdiscount"
                                   class="col-sm-4 control-label"><?= lang('product_discount') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control kb-pad" id="mdiscount">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="mprice" class="col-sm-4 control-label"><?= lang('unit_price') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control kb-pad" id="mprice">
                        </div>
                    </div>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th style="width:25%;"><?= lang('net_unit_price'); ?></th>
                            <th style="width:25%;"><span id="mnet_price"></span></th>
                            <th style="width:25%;"><?= lang('product_tax'); ?></th>
                            <th style="width:25%;"><span id="mpro_tax"></span></th>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="addItemManually"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="sckModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="mModalLabel"><?= lang('shortcut_keys') ?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <table class="table table-bordered table-striped table-condensed table-hover"
                       style="margin-bottom: 0px;">
                    <thead>
                    <tr>
                        <th><?= lang('shortcut_keys') ?></th>
                        <th><?= lang('actions') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><?= $pos_settings->focus_add_item ?></td>
                        <td><?= lang('focus_add_item') ?></td>
                    </tr>
                    <tr>
                        <td><?= $pos_settings->add_manual_product ?></td>
                        <td><?= lang('add_manual_product') ?></td>
                    </tr>
                    <tr>
                        <td><?= $pos_settings->customer_selection ?></td>
                        <td><?= lang('customer_selection') ?></td>
                    </tr>
                    <tr>
                        <td><?= $pos_settings->add_customer ?></td>
                        <td><?= lang('add_customer') ?></td>
                    </tr>
                    <tr>
                        <td><?= $pos_settings->toggle_category_slider ?></td>
                        <td><?= lang('toggle_category_slider') ?></td>
                    </tr>
                    <tr>
                        <td><?= $pos_settings->toggle_subcategory_slider ?></td>
                        <td><?= lang('toggle_subcategory_slider') ?></td>
                    </tr>
                    <tr>
                        <td><?= $pos_settings->cancel_sale ?></td>
                        <td><?= lang('cancel_sale') ?></td>
                    </tr>
                    <tr>
                        <td><?= $pos_settings->suspend_sale ?></td>
                        <td><?= lang('suspend_sale') ?></td>
                    </tr>
                    <tr>
                        <td><?= $pos_settings->print_items_list ?></td>
                        <td><?= lang('print_items_list') ?></td>
                    </tr>
                    <tr>
                        <td><?= $pos_settings->finalize_sale ?></td>
                        <td><?= lang('finalize_sale') ?></td>
                    </tr>
                    <tr>
                        <td><?= $pos_settings->today_sale ?></td>
                        <td><?= lang('today_sale') ?></td>
                    </tr>
                    <tr>
                        <td><?= $pos_settings->open_hold_bills ?></td>
                        <td><?= lang('open_hold_bills') ?></td>
                    </tr>
                    <tr>
                        <td><?= $pos_settings->close_register ?></td>
                        <td><?= lang('close_register') ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="dsModal" tabindex="-1" role="dialog" aria-labelledby="dsModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                        class="fa fa-2x">&times;</i></button>
                <h4 class="modal-title" id="dsModalLabel"><?= lang('edit_order_discount'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <?= lang("order_discount", "order_discount_input"); ?>
                    <?php echo form_input('order_discount_input', '', 'class="form-control kb-pad" id="order_discount_input"'); ?>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" id="updateOrderDiscount" class="btn btn-primary"><?= lang('update') ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="txModal" tabindex="-1" role="dialog" aria-labelledby="txModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                        class="fa fa-2x">&times;</i></button>
                <h4 class="modal-title" id="txModalLabel"><?= lang('edit_order_tax'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <?= lang("order_tax", "order_tax_input"); ?>
                    <?php
                    if(!empty($tax_rates)){
                        $tr[""] = "";
                        foreach ($tax_rates as $tax) {
                            $tr[$tax->id] = $tax->name;
                        }
                         echo form_dropdown('order_tax_input', $tr, $Settings->default_tax_rate, 'id="order_tax_input" class="form-control pos-input-tip" style="width:100%;"');
                    }else{
                            $tr[0] = 'no tax';
                             echo form_dropdown('order_tax_input', $tr[0], "", 'id="order_tax_input" class="form-control pos-input-tip" style="width:100%;"');
                    }
                        
                    ?>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" id="updateOrderTax" class="btn btn-primary"><?= lang('update') ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="susModal" tabindex="-1" role="dialog" aria-labelledby="susModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                        class="fa fa-2x">&times;</i></button>
                <h4 class="modal-title" id="susModalLabel"><?= lang('suspend_sale'); ?></h4>
            </div>
            <div class="modal-body">
                <p><?= lang('type_reference_note'); ?></p>

                <div class="form-group">
                    <?= lang("reference_note", "reference_note"); ?>
                    <?php echo form_input('reference_note', $reference_note, 'class="form-control kb-text" id="reference_note"'); ?>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" id="suspend_sale" class="btn btn-primary"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>
<div id="order_tbl"><span id="order_span"></span>
    <table id="order-table" class="prT table table-striped" style="margin-bottom:0;" width="100%"></table>
</div>
<div id="bill_tbl"><span id="bill_span"></span>
    <table id="bill-table" width="100%" class="prT table table-striped" style="margin-bottom:0;"></table>
    <table id="bill-total-table" class="prT table" style="margin-bottom:0;" width="100%"></table>
</div>
<div class="modal fade in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true"></div>
<div class="modal fade in" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2"
     aria-hidden="true"></div>
<div id="modal-loading" style="display: none;">
    <div class="blackbg"></div>
    <div class="loader"></div>
</div>
<?php unset($Settings->setting_id, $Settings->smtp_user, $Settings->smtp_pass, $Settings->smtp_port, $Settings->update, $Settings->reg_ver, $Settings->allow_reg, $Settings->default_email, $Settings->mmode, $Settings->timezone, $Settings->restrict_calendar, $Settings->restrict_user, $Settings->auto_reg, $Settings->reg_notification, $Settings->protocol, $Settings->mailpath, $Settings->smtp_crypto, $Settings->corn, $Settings->customer_group, $Settings->envato_username, $Settings->purchase_code); ?>
<script type="text/javascript">
var site = <?= json_encode(array('base_url' => base_url(), 'settings' => $Settings, 'dateFormats' => $dateFormats)) ?>, pos_settings = <?= json_encode($pos_settings); ?>;
var lang = {unexpected_value: '<?=lang('unexpected_value');?>', select_above: '<?=lang('select_above');?>', r_u_sure: '<?= lang('r_u_sure'); ?>'};
</script>

<script type="text/javascript">
    var product_variant = 0, shipping = 0, p_page = 0, per_page = 0, tcp = "<?= $tcp ?>",
        //cat_id = "<?= $pos_settings->default_category ?>", ocat_id = "<?= $pos_settings->default_category ?>", sub_cat_id = 0, osub_cat_id,
        cat_id = "<?= $pos_settings->default_category ?>", ocat_id = "<?= $pos_settings->default_category ?>", sub_cat_id = 0, osub_cat_id,end_cat_id = 0, oend_cat_id,
        count = 1, an = 1, DT = <?= $Settings->default_tax_rate ?>,
        product_tax = 0, invoice_tax = 0, total_discount = 0, total = 0, total_paid = 0, grand_total = 0,
        KB = <?= $pos_settings->keyboard ?>, tax_rates = <?php echo json_encode($tax_rates); ?>;
    var protect_delete = <?php if(!$Owner && !$Admin) { echo $pos_settings->pin_code ? '1' : '0'; } else { echo '0'; } ?>;
    //var audio_success = new Audio('<?= $assets ?>sounds/sound2.mp3');
    //var audio_error = new Audio('<?= $assets ?>sounds/sound3.mp3');
    var lang_total = '<?=lang('total');?>', lang_items = '<?=lang('items');?>', lang_discount = '<?=lang('discount');?>', lang_tax2 = '<?=lang('order_tax');?>', lang_total_payable = '<?=lang('total_payable');?>';
    var java_applet = <?=$pos_settings->java_applet?>, order_data = '', bill_data = '';
    function widthFunctions(e) {
        var wh = $(window).height(),
            lth = $('#left-top').height(),
            lbh = $('#left-bottom').height();
        $('#item-list').css("height", wh - 140);
        $('#item-list').css("min-height", 515);
        $('#left-middle').css("height", wh - lth - lbh - 100);
        $('#left-middle').css("min-height", 325);
        $('#product-list').css("height", wh - lth - lbh - 105);
        $('#product-list').css("min-height", 320);
    }
    $(window).bind("resize", widthFunctions);
    $(document).ready(function () {
		$('#ptax').prop('disabled',true);
        <?php if ($sid) { ?>
        localStorage.setItem('positems', JSON.stringify(<?=$items;?>));
        <?php } ?>
        <?php if($this->session->userdata('remove_posls')) { ?>
        if (localStorage.getItem('positems')) {
            localStorage.removeItem('positems');
        }
        if (localStorage.getItem('posdiscount')) {
            localStorage.removeItem('posdiscount');
        }
        if (localStorage.getItem('postax2')) {
            localStorage.removeItem('postax2');
        }
        if (localStorage.getItem('posshipping')) {
            localStorage.removeItem('posshipping');
        }
        if (localStorage.getItem('poswarehouse')) {
            localStorage.removeItem('poswarehouse');
        }
        if (localStorage.getItem('posnote')) {
            localStorage.removeItem('posnote');
        }
        if (localStorage.getItem('poscustomer')) {
            localStorage.removeItem('poscustomer');
        }
        if (localStorage.getItem('posbiller')) {
            localStorage.removeItem('posbiller');
        }
        if (localStorage.getItem('poscurrency')) {
            localStorage.removeItem('poscurrency');
        }
        if (localStorage.getItem('posnote')) {
            localStorage.removeItem('posnote');
        }
        if (localStorage.getItem('staffnote')) {
            localStorage.removeItem('staffnote');
        }
        <?php $this->sma->unset_data('remove_posls'); } ?>
        widthFunctions();
        <?php if($suspend_sale) { ?>
        localStorage.setItem('postax2', <?=$suspend_sale->order_tax_id;?>);
        localStorage.setItem('posdiscount', '<?=$suspend_sale->order_discount_id;?>');
        localStorage.setItem('poswarehouse', '<?=$suspend_sale->warehouse_id;?>');
        localStorage.setItem('poscustomer', '<?=$suspend_sale->customer_id;?>');
        localStorage.setItem('posbiller', '<?=$suspend_sale->biller_id;?>');
        <?php } ?>
        <?php if($this->input->get('customer')) { ?>
        if (!localStorage.getItem('positems')) {
            localStorage.setItem('poscustomer', <?=$this->input->get('customer');?>);
        } else if (!localStorage.getItem('poscustomer')) {
            localStorage.setItem('poscustomer', <?= $customer->id; ?>);
        }
        <?php } else { ?>
        if (!localStorage.getItem('poscustomer')) {
            localStorage.setItem('poscustomer', <?= $customer->id; ?>);
        }
        <?php } ?>
        if (!localStorage.getItem('postax2')) {
            localStorage.setItem('postax2', <?=$Settings->default_tax_rate2;?>);
        }
        $('.select').select2({minimumResultsForSearch: 6});
        var cutomers = [{
            id: <?= $customer->id; ?>,
            text: '<?= $customer->company == '-' ? $customer->name : $customer->company; ?>'
        }];
        
        
       var custm_id = '<?php echo $customer->id; ?>';
   
       var pcustids;
        if(custm_id >1 )
        {
           pcustids = '<?php echo $customer->id; ?>';
        }
        else
        { 
           pcustids = <?php echo (!empty($_GET["customer"])) ? $_GET["customer"] : $pos_settings->default_customer; ?>
        }
        $('#poscustomer').val(pcustids).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: "<?= site_url('customers/getCustomer') ?>/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        console.log(JSON.stringify(data));
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term + '-' + $('#customer_biller option:selected').val(),
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'No Match Found'}]};
                    }
                }
            }
        });
        if (KB) {
            display_keyboards();

            var result = false;
            $('#poscustomer').on('select2-opening', function () {
                $('.select2-input').addClass('kb-text');
                display_keyboards();
                $('.select2-input').bind('change.keyboard', function (e, keyboard, el) {
                    if (el && el.value != '' && el.value.length >= 10) {
                        $('.select2-input').addClass('select2-active');
                        $.ajax({
                            type: "get",
                            async: false,
                            url: "<?= site_url('customers/suggestions') ?>/" + el.value,
                            dataType: "json",
                            success: function (res) {
                                if (res.results != null) {
                                    $('#poscustomer').select2({data: res}).select2('open');
                                    $('.select2-input').removeClass('select2-active');
                                    result = true;
                                } else {
                                    result = false;
                                }
                            }
                        });
                        if (!result) {
                            bootbox.alert('no_match_found');
                            $('#poscustomer').select2('close');
                            $('#test').click();
                        }
                    }
                });
            });

            $('#poscustomer').on('select2-close', function () {
                $('.select2-input').removeClass('kb-text');
                $('#test').click();
                $('select, .select').select2('destroy');
                $('select, .select').select2({minimumResultsForSearch: 6});
            });
            $(document).bind('click', '#test', function () {
                var kb = $('#test').keyboard().getkeyboard();
                kb.close();
                //kb.destroy();
                $('#add-item').focus();
            });

        }

        $(document).on('change', '#posbiller', function () {
            $('#biller').val($(this).val());
        });

        <?php for($i=1; $i<=5; $i++) { ?>
        $('#paymentModal').on('change', '#amount_<?=$i?>', function (e) {
            $('#amount_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModal').on('blur', '#amount_<?=$i?>', function (e) {
            $('#amount_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModal').on('select2-close', '#paid_by_<?=$i?>', function (e) {
            $('#paid_by_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModal').on('change', '#pcc_no_<?=$i?>', function (e) {
            $('#cc_no_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModal').on('change', '#pcc_holder_<?=$i?>', function (e) {
            $('#cc_holder_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModal').on('change', '#gift_card_no_<?=$i?>', function (e) {
            $('#paying_gift_card_no_val_<?=$i?>').val($(this).val());
        });
		
		 $('#paymentModal').on('change', '#credit_voucher_no_<?=$i?>', function (e) {
            $('#paying_credit_voucher_no_val_<?=$i?>').val($(this).val());
        });
		
        $('#paymentModal').on('change', '#pcc_month_<?=$i?>', function (e) {
            $('#cc_month_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModal').on('change', '#pcc_year_<?=$i?>', function (e) {
            $('#cc_year_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModal').on('change', '#pcc_type_<?=$i?>', function (e) {
            $('#cc_type_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModal').on('change', '#pcc_cvv2_<?=$i?>', function (e) {
            $('#cc_cvv2_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModal').on('change', '#cheque_no_<?=$i?>', function (e) {
            $('#cheque_no_val_<?=$i?>').val($(this).val());
        });
        $('#paymentModal').on('change', '#payment_note_<?=$i?>', function (e) {
            $('#payment_note_val_<?=$i?>').val($(this).val());
        });
        <?php } ?>

        $('#payment').click(function () {
            <?php if ($sid) { ?>
            suspend = $('<span></span>');
            suspend.html('<input type="hidden" name="delete_id" value="<?php echo $sid; ?>" />');
            suspend.appendTo("#hidesuspend");
            <?php } ?>
            var twt = formatDecimal((total + invoice_tax) - order_discount);
            if (count == 1) {
                bootbox.alert('<?= lang('x_total'); ?>');
                return false;
            }
            gtotal = formatDecimal(twt);
            <?php if($pos_settings->rounding) { ?>
            round_total = roundNumber(gtotal, <?=$pos_settings->rounding?>);
            var rounding = formatDecimal(0 - (gtotal - round_total));
            $('#twt').text(formatMoney(round_total) + ' (' + formatMoney(rounding) + ')');
            $('#quick-payable').text(round_total);
            <?php } else { ?>
            $('#twt').text(formatMoney(gtotal));
            $('#quick-payable').text(gtotal);
            <?php } ?>
            $('#item_count').text(count - 1);
            $('#paymentModal').appendTo("body").modal('show');
            $('#amount_1').focus();
        });
        $('#paymentModal').on('shown.bs.modal', function(e){
            // $('#quick-payable').click();
            $('#amount_1').focus();
        });
        
        var pi = 'amount_1', pa = 2;
        $(document).on('click', '.quick-cash', function () {
            var $quick_cash = $(this);
            var amt = $quick_cash.contents().filter(function () {
                return this.nodeType == 3;
            }).text();
           
            var th = site.settings.thousands_sep == 0 ? '' : site.settings.thousands_sep;
            var $pi = $('#' + pi);
            var abc = $('div.btn-group-vertical').children().first();
           
        if($(this).is(abc) == true)
        {     
            amt = formatDecimal(amt.split(th).join(""));
           
        }
        else{
            amt = formatDecimal(amt.split(th).join("")) * 1 + $pi.val() * 1;
        }
            $pi.val(formatDecimal(amt)).focus();
            var note_count = $quick_cash.find('span');
            if (note_count.length == 0) {
                //commented by vikas singh 25-10-2016 to remove count of currency
                 /*$quick_cash.append('<span class="badge">1</span>'); */
            } else {
                /* note_count.text(parseInt(note_count.text()) + 1); */
            }
        });

        $(document).on('click', '#clear-cash-notes', function () {
            $('.quick-cash').find('.badge').remove();
            $('#' + pi).val('0').focus();
        });

        $(document).on('change', '.gift_card_no', function () {
            var cn = $(this).val() ? $(this).val() : '';
            var payid = $(this).attr('id'),
                id = payid.substr(payid.length - 1);
            if (cn != '') {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "sales/validate_gift_card/" + cn,
                    dataType: "json",
                    success: function (data) {
                        if (data === false) {
                            $('#gift_card_no_' + id).parent('.form-group').addClass('has-error');
                            bootbox.alert('<?=lang('incorrect_gift_card')?>');
                        } else if (data.customer_id !== null && data.customer_id !== $('#poscustomer').val()) {
                            $('#gift_card_no_' + id).parent('.form-group').addClass('has-error');
                            bootbox.alert('<?=lang('gift_card_not_for_customer')?>');
                        } else {
                            $('#gc_details_' + id).html('<small>Card No: ' + data.card_no + '<br>Value: ' + data.value + ' - Balance: ' + data.balance + '</small>');
                            $('#gift_card_no_' + id).parent('.form-group').removeClass('has-error');
                            //calculateTotals();
                            //$('#amount_' + id).val(data.balance).focus();
                        }
                    }
                });
            }
        });
		
	    $(document).on('change', '.credit_voucher_no', function () {
            var cn = $(this).val() ? $(this).val() : '';
            var payid = $(this).attr('id'),
            id = payid.substr(payid.length - 1);
            if (cn != '') {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "sales/validate_credit_voucher/" + cn,
                    dataType: "json",
                    success: function (data) {
                        if (data === false) {
                            $('#credit_voucher_no_' + id).parent('.form-group').addClass('has-error');
                            bootbox.alert('<?=lang('incorrect_credit_voucher')?>');
                        } else if (data.customer_id !== null && data.customer_id !== $('#poscustomer').val()) {
                            $('#credit_voucher_no_' + id).parent('.form-group').addClass('has-error');
                            bootbox.alert('<?=lang('credit_voucher_not_for_customer')?>');
                        } else {
                            $('#cv_details_' + id).html('<small>Card No: ' + data.card_no + '<br>Value: ' + data.value + ' - Balance: ' + data.balance + '</small>');
                            $('#credit_voucher_no_' + id).parent('.form-group').removeClass('has-error');
                            //calculateTotals();
                            //$('#amount_' + id).val(data.balance).focus();
                        }
                    }
                });
            }
        });
    
   // added by vikas singh 24-10-16
   
    var maxField = 5; //Input fields increment limitation
    var addButton = $(".addButton"); //Add button selector
    var wrapper = $('#payments'); //Input field wrapper
   
    var x = 1, fieldCount = 1; //Initial field counter is 1
     //Initial field counter is 1
    
    $(addButton).click(function(){ //Once add button is clicked
      
        if(fieldCount < maxField){ //Check maximum number of input fields
            
            var fieldHTML ="";
           // alert("x"+x+"max"+maxField)
            x++; //Increment field counter
            fieldCount++; //Increment field counter
            fieldHTML += '<div id="payments">';
            fieldHTML += '<div class="well well-sm well_1">';
            fieldHTML += '<a href="javascript:void(0);" class="remove_button" title="Remove field" style="position: absolute; margin-left: 691px;"><i class="fa fa-2x">×</i></a>';
            fieldHTML += '<div class="payment">';
            fieldHTML += '<div class="row">';
            fieldHTML += '<div class="col-sm-5">';
            fieldHTML += '<div class="form-group">';
            fieldHTML += '<?= lang("amount", "amount_'+x+'"); ?>';
            fieldHTML += '<input name="amountc['+x+']" type="text" id="amount_'+x+'" class="pa form-control kb-pad amount addmorePay"/>';
            fieldHTML += '</div>';   
            fieldHTML += '</div>';                         
            fieldHTML += '<div class="col-sm-5 col-sm-offset-1">';                                
            fieldHTML += '<div class="form-group">';                                     
            fieldHTML += '<?= lang("paying_by", "paid_by_'+x+'"); ?>';                                        
            fieldHTML += '<select name="paid_by1['+x+']" id="paid_by_'+x+'" class="form-control paid_by addmorePay">';                                             
            fieldHTML += '<option value="ss">Select an option</option>';                                                
            fieldHTML += '<option value="cash"><?= lang("cash"); ?></option>';
            fieldHTML += '<option value="CC"><?= lang("cc"); ?></option>';                                                
            fieldHTML += '<option value="Cheque"><?= lang("cheque"); ?></option>';
            fieldHTML += '</select>'; 
            fieldHTML += '</div>';                                       
            fieldHTML += '</div>';                                         
            fieldHTML += '</div>';                                             
            fieldHTML += '<div class="row">';                                               
            fieldHTML += '<div class="col-sm-11">';                                                 
            fieldHTML += '<div class="form-group gc_'+x+'" style="display:none;">'; 
            fieldHTML += '<?= lang("gift_card_no", "gift_card_no_'+x+'"); ?>';                                                 
            fieldHTML += '<input name="cpaying_gift_card_no['+x+']" type="text" id="gift_card_no_'+x+'"class="pa form-control kb-pad gift_card_no addmorePay" />';                                            
            fieldHTML += '<div id="gc_details_'+x+'"></div>';
            fieldHTML += '</div>';                                  
            fieldHTML += '<div class="form-group cv_'+x+'" style="display: none;">';                                   
            fieldHTML += '<?= lang("credit_voucher_no", "credit_voucher_no_'+x+'"); ?>';                                        
            fieldHTML += '<input name="cpaying_credit_voucher_no['+x+']" type="text" id="credit_voucher_no_'+x+'" class="pa form-control kb-pad credit_voucher_no addmorePay"/>';                                           
            fieldHTML += '<div id="cv_details_'+x+'"></div>';                                                                                           
            fieldHTML += '</div>'; 
            fieldHTML += '<div class="pcc_'+x+'" style="display:none;">';                                                
            fieldHTML += '<div class="form-group">';  
            fieldHTML += '<input type="text" id="swipe_'+x+'" class="form-control swipe"placeholder="<?= lang('swipe') ?>"/>'; 
            fieldHTML += '</div>';                                                 
            fieldHTML += '<div class="row">';                                             
            fieldHTML += '<div class="col-md-6">';											
            fieldHTML += '<div class="form-group">';
            fieldHTML += '<input name="ccc_no['+x+']" type="text" id="pcc_no_'+x+'" class="form-control addmorePay cardno" maxlength="20" placeholder="<?= lang('cc_no') ?>"/>';						
            fieldHTML += '</div>';                                                
            fieldHTML += '</div>';                                                    
            fieldHTML += '<div class="col-md-6">'; 
            fieldHTML += '<div class="form-group">';
            fieldHTML += '<input name="ccc_holer['+x+']" type="text" id="pcc_holder_'+x+'"class="form-control addmorePay"  placeholder="<?= lang('cc_holder') ?>"/>';                                                        
            fieldHTML += '</div>';                                                
            fieldHTML += '</div>';
            fieldHTML += '<div class="col-md-3">';
            fieldHTML += '<div class="form-group">';                                                  
            fieldHTML += '<select name="ccc_type['+x+']" id="pcc_type_'+x+'"class="form-control pcc_type addmorePay" placeholder="<?= lang('card_type') ?>">';                                                     
            fieldHTML += '<option value="Visa"><?= lang("Visa"); ?></option>';                                                         
            fieldHTML += '<option value="MasterCard"><?= lang("MasterCard"); ?></option>';   
            fieldHTML += '<option value="Amex"><?= lang("Amex"); ?></option>';                                                             
            fieldHTML += '<option value="Discover"><?= lang("Discover"); ?></option>';                                                                  
            fieldHTML += '</select>';  
            fieldHTML += '</div>';                                                
            fieldHTML += '</div>';                                                               
            fieldHTML += '<div class="col-md-3">';                                                                
            fieldHTML += '<div class="form-group">';                                                      
            fieldHTML += '<select name="ccc_month['+x+']" id="pcc_month_'+x+'"class="form-control pcc_type cmonth addmorePay" placeholder="<?= lang('card_type') ?>">';
            fieldHTML += '<option value="">Month</option>'; 
            fieldHTML += '</select>';  
            fieldHTML += '</div>';                                                
            fieldHTML += '</div>';                                                             
            fieldHTML += '<div class="col-md-3">';                                                                  
            fieldHTML += '<div class="form-group">';                                                                  
            fieldHTML += '<select name="ccc_year['+x+']" id="pcc_year_'+x+'"class="form-control pcc_type cyear addmorePay" placeholder="<?= lang('card_type') ?>">';
            fieldHTML += '<option value="">Year</option>';  
            fieldHTML += '</select>';  
            fieldHTML += '</div>';                                                           
            fieldHTML += '</div>';
            fieldHTML += '<div class="col-md-3">';     
            fieldHTML += '<div class="form-group">';                                                            
            fieldHTML += '<input name="ccc_cvv2['+x+']" type="text" id="pcc_cvv2_'+x+'"class="form-control addmorePay" placeholder="<?= lang('cvv2') ?>" maxlength="4"/>'; 
            fieldHTML += '</div>';                                                           
            fieldHTML += '</div>';
            fieldHTML += '</div>';                                                           
            fieldHTML += '</div>';
            fieldHTML += '<div class="pcheque_'+x+'" style="display:none;">';
            fieldHTML += '<div class="form-group"><?= lang("cheque_no", "cheque_no_'+x+'"); ?>';                                                            
            fieldHTML += '<input name="ccheque_no['+x+']" type="text" id="cheque_no_'+x+'"class="form-control cheque_no addmorePay" maxlength="6"/>';                                            
            fieldHTML += '</div>';
            fieldHTML += '</div>';                                                 
            fieldHTML += '<div class="form-group">';  
            fieldHTML += '<?= lang('payment_note', 'payment_note'); ?>';                                                    
            fieldHTML += '<textarea name="payment_note['+x+']" id="payment_note_'+x+'"class="pa form-control kb-text payment_note"></textarea>'; 
            fieldHTML += '</div>';                                                           
            fieldHTML += '</div>';
            fieldHTML += '</div>';                                                           
            fieldHTML += '</div>';                                               
            fieldHTML += '</div>';                                                           
            $(wrapper).append(fieldHTML); // Add field html
            // add year list using jquery
           
           $('#paymentModal').css('overflow-y', 'scroll');
            $('.paid_by').change(function () {
            $('#paid_by_val_'+x).val($(this).val());   
            if($(this).val() == "cash")
            {
            
            if ($('select option[value="' + $(this).val() + '"]:selected').length > 1)
            {
               bootbox.alert('Payment method is already selected');
                
               // self.select2('val','ss').attr('selected',true);
               $("#paid_by_"+x+" option[value='ss']").prop('selected', true);
            
            }
            }
            });
           $('.addmorePay').each(function () {
               
           
           $(document).bind('click', '#amount_+x+', function () {
	      display_keyboards();
              $('#amount_+x+').addClass('kb-text');
						
           });
           
           $('.cardno').bind('keyup blur',function(){ 
            var node = $(this);
            node.val(node.val().replace(/[^0-9]+/i, '') ); }
           );
   
   
           $(this).rules("add", {
           required: true
           });
	$('input[name="amountc['+x+']"]').rules("add", {
           required:true			   
          
           });
		   
        $('input[name="ccc_no['+x+']"]').rules("add", {
           required:true,			   
           creditcard: true
           });
		   
		   $('input[name="ccc_cvv2['+x+']"]').rules("add", {  
           required:true,
           cvvnumber:true,
		   messages:{
			  cvvnumber: "Please enter valid cvv number" 
		   }
         });
		 
        $('input[name="ccheque_no['+x+']"]').rules("add", {  
           required:true,
           number:true,
           maxlength:6,
           minlength:6
		   
           });
           });
		   
        }
         else{
              bootbox.alert('<?= lang('max_reached') ?>');
                return false;
        }
        
        var d = new Date(),
        n = d.getMonth(),
        y = d.getFullYear();

         for (i = new Date().getFullYear() ; i < 2036; i++) {
        
            $('.cyear').append("<option val="+i+">"+i+"</option>");
                  
           }
           
     selectValues = { "1": "01", "2": "02", "3": "03", "4": "04", "5": "05", "6": "06", "7": "07", "8": "08", "9": "09", "10": "10", "11": "11", "12": "12"};
     $.each(selectValues, function(key, value) {   
     $('.cmonth').append($("<option></option>").attr("value",key).text(value)); 
     $('.cmonth option:eq('+n+')').prop('selected', true);
     
     });
     
    });
    
    $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        fieldCount--; //Decrement field counter
    });
    
//  end by vikas singh 
    
//        $(document).on('click', '.addButton', function () {
//         var p_val = $(this).val(),
//                id = $(this).attr('id'),
//                pa_no = id.substr(id.length - 1);
//          $('.pcheque_' + pa_no).hide();
//                $('.pcc_' + pa_no).hide();
//                $('.pcash_' + pa_no).show();
//                $('#payment_note_' + pa_no).focus();
//
//            if (pa <= 5) {
//                $('#paid_by_1, #pcc_type_1, #pcc_month_1, #pcc_year_1').select2('destroy');
//
//                var phtml = $('#payments').html(),
//                    update_html = phtml.replace(/_1/g, '_' + pa);
//                pi = 'amount_' + pa;
//                $('#multi-payment').append('<button type="button" class="close close-payment" style="margin: -10px 0px 0 0;"><i class="fa fa-2x">&times;</i></button>' + update_html);
//
//                $('#paid_by_1, #pcc_type_1, #pcc_month_1, #pcc_year_1, #paid_by_' + pa + ', #pcc_type_' + pa + ', #pcc_month_' + pa+ ', #pcc_year_' + pa).select2({minimumResultsForSearch: 6});
//                var p_val = 'cash',
//                id = 'paid_by_' + pa,
//                pa_no = id.substr(id.length - 1);
//                $('.pcheque_' + pa_no).hide();
//                $('.pcc_' + pa_no).hide();
//                $('.pcash_' + pa_no).show();
//                $('#payment_note_' + pa_no).focus();
//                read_card();
//                pa++;
//            } else {
//                bootbox.alert('<?= lang('max_reached') ?>');
//                return false;
//            }
//            $('#paymentModal').css('overflow-y', 'scroll');
//        });
//
//        $(document).on('click', '.close-payment', function () {
//            $(this).next().remove();
//            $(this).remove();
//            pa--;
//        });
       pi = 'amount_' + pa;
       
        $(document).on('focus', '.amount', function () {
            pi = $(this).attr('id');
            calculateTotals();
        }).on('blur', '.amount', function () {
            calculateTotals();
        });

        function calculateTotals() {
            var total_paying = 0;
            var ia = $(".amount");
            $.each(ia, function (i) {
                total_paying += parseFloat($(this).val());
            });
            $('#total_paying').text(formatMoney(total_paying));
            <?php if($pos_settings->rounding) { ?>
            $('#balance').text(formatMoney(total_paying - round_total));
            $('#balance_' + pi).val(formatDecimal(total_paying - round_total));
            total_paid = total_paying;
            grand_total = round_total;
            <?php } else { ?>
            $('#balance').text(formatMoney(total_paying - gtotal));
            $('#balance_' + pi).val(formatDecimal(total_paying - gtotal));
            total_paid = total_paying;
            grand_total = gtotal;
            <?php } ?>
        }

        $("#add_item").autocomplete({
            
            source: function (request, response) {
                if (!$('#poscustomer').val()) {
                    $('#add_item').val('').removeClass('ui-autocomplete-loading');
                    bootbox.alert('<?=lang('select_above');?>');
                    //response('');
                    $('#add_item').focus();
                    return false;
                }
               
                $.ajax({
                    type: 'get',
                    url: '<?= site_url('sales/suggestions'); ?>',
                    dataType: "json",
                    data: {
                        term: request.term,
                        warehouse_id: $("#poswarehouse").val(),
                        customer_id: $("#poscustomer").val()
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            delay: 200,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');
                }
//                else if (ui.content.length == 1 && ui.content[0].id != 0) {
//                    ui.item = ui.content[0];
//                    console.log(ui.item);
//                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
//                    $(this).autocomplete('close');
//                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');

                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    //console.log('ui.item :'+JSON.stringify(ui.item));
                    var row = add_invoice_item(ui.item);
                    if (row)
                        $(this).val('');
                } else {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });

        $('#add_item').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                $(this).autocomplete("search");
            }
        });

        <?php if($pos_settings->tooltips) { //echo '$(".pos-tip").tooltip();';
         } ?>

        $('#product-list, #category-list, #subcategory-list').perfectScrollbar({suppressScrollX: true});
        $('select, .select').select2({minimumResultsForSearch: 6});

        $(document).on('click', '.product', function (e) {
            
            $('#modal-loading').show();
            code = $(this).val(),
                wh = $('#poswarehouse').val(),
                cu = $('#poscustomer').val();

           /* $.ajax({
                type: "get",
                url: "<?= site_url('pos/getProductDataByCode') ?>",
                data: {code: code, warehouse_id: wh, customer_id: cu},
                dataType: "json",
                success: function (data) {
                    e.preventDefault();
                    if (data !== null) {
                        add_invoice_item(data);
                        $('#modal-loading').hide();
                    } else {
                        
                        bootbox.alert('<?= lang('no_match_found') ?>');
                        $('#modal-loading').hide();
                    }
                }
            });*/
            $.ajax({
                type: "get",
                url: "<?= site_url('pos/getProductDataByCode') ?>",
                //data: {code: code, warehouse_id: wh, customer_id: cu, cat_id: cat_id},
                  data: {code: code, warehouse_id: wh, customer_id: cu},
                dataType: "json",
                success: function (data) {
                    e.preventDefault();
                    if (data !== null) {
                        //console.log(data);
                        //$.each(data,function(i,v){
                           //add_invoice_item(data[i]);
                          //console.log(data[i]);
                       // });
                        add_invoice_item(data);
                        $('#modal-loading').hide();
                    } else {
                        //audio_error.play();
                        bootbox.alert('<?= lang('no_match_found') ?>');
                        $('#modal-loading').hide();
                    }
                }
            });

        });
		
		$(document).on('change', '#poswarehouse', function () {
                
			var wid = $(this).val();
                       
			$.ajax({
				type:"POST",
				url:"<?=site_url('pos/ajaxproducts')?>",
				data:{'warehouse_id':wid},
				dataType:"json",
				success:function(data){
                                    
                                   // return true;
                                    //alert(data);
					if(data!=null){
                                            console.log("data",data);
						$('#item-list').html(data);
					}
				}
			});
			
		});

        $(document).on('click', '.category', function () {
            if (cat_id != $(this).val()) {
                $('#open-category').click();
                $('#modal-loading').show();
                cat_id = $(this).val();
                $.ajax({
                    type: "get",
                    url: "<?= site_url('pos/ajaxcategorydata'); ?>",
                    data: {category_id: cat_id},
                    dataType: "json",
                    success: function (data) {
                        $('#item-list').empty();
                        var newPrs = $('<div></div>');
                        newPrs.html(data.products);
                        newPrs.appendTo("#item-list");
                        $('#subcategory-list').empty();
                        var newScs = $('<div></div>');
                        newScs.html(data.subcategories);
                        newScs.appendTo("#subcategory-list");
                        
                        $('#endcategory-list').empty();
                        var newScs1 = $('<div></div>');
                        newScs1.html(data.endcategories);
                        newScs1.appendTo("#endcategory-list");
                        
                        tcp = data.tcp;
                    }
                }).done(function () {
                    p_page = 'n';
                    $('#category-' + cat_id).addClass('active');
                    $('#category-' + ocat_id).removeClass('active');
                    ocat_id = cat_id;
                    $('#modal-loading').hide();
                });
            }
        });
        $('#category-' + cat_id).addClass('active');

                $(document).on('click', '.subcategory', function () {
            if (sub_cat_id != $(this).val()) {
                $('#open-subcategory').click();
                $('#modal-loading').show();
                sub_cat_id = $(this).val();
                p_page = 'n'; // Add by ajay/ankit for N level category
                $.ajax({
                    type: "get",
                    url: "<?= site_url('pos/ajaxproducts'); ?>",
                    data: {category_id: cat_id, subcategory_id: sub_cat_id, per_page: p_page},
                    dataType: "html",
                    success: function (data) {
                        $('#item-list').empty();
                        var newPrs = $('<div></div>');
                        newPrs.html(data);
                        newPrs.appendTo("#item-list");
                        $.ajax({
                            type: "get",
                            url: "<?= site_url('pos/ajaxsubcategorydata'); ?>",
                           data: {subcategory_id: sub_cat_id},
                           dataType: "json",
                           success: function (data) {
//                               $('#item-list').empty();
//                        var newPrs = $('<div></div>');
//                        newPrs.html(data.products);
//                        newPrs.appendTo("#item-list");
                                                
                        $('#endcategory-list').empty();
                        var newScs1 = $('<div></div>');
                        newScs1.html(data.endcategories);
                        newScs1.appendTo("#endcategory-list");
                        
                        tcp = data.tcp;
                    }
                            
                        });
                        
                    }
                }).done(function () {
                    p_page = 'n';
                    $('#subcategory-' + sub_cat_id).addClass('active');
                    $('#subcategory-' + osub_cat_id).removeClass('active');
                    $('#modal-loading').hide();
                });
            }
        });
        
        $(document).on('click', '.endcategory', function () {
            if (end_cat_id != $(this).val()) {
                $('#open-endcategory').click();
                $('#modal-loading').show();
                end_cat_id = $(this).val();
                p_page = 'n'; // Add by ajay/ankit for N level category
                $.ajax({
                    type: "get",
                    url: "<?= site_url('pos/ajaxproducts'); ?>",
                    data: {category_id: cat_id, subcategory_id: sub_cat_id, endcategory_id: end_cat_id, per_page: p_page},
                    dataType: "html",
                    success: function (data) {
                        
                        $('#item-list').empty();
                        var newPrs = $('<div></div>');
                        newPrs.html(data);
                        newPrs.appendTo("#item-list");
                                       
                    }
                }).done(function () {
                    p_page = 'n';
                    $('#endcategory-' + end_cat_id).addClass('active');
                    $('#endcategory-' + oend_cat_id).removeClass('active');
                    $('#modal-loading').hide();
                });
            }
        });

        $('#next').click(function () {
            if (p_page == 'n') {
                p_page = 0
            }
            p_page = p_page + <?php echo $pos_settings->pro_limit; ?>;
            if (tcp >= <?php echo $pos_settings->pro_limit; ?> && p_page < tcp) {
                $('#modal-loading').show();
                $.ajax({
                    type: "get",
                    url: "<?= site_url('pos/ajaxproducts'); ?>",
                    data: {category_id: cat_id, subcategory_id: sub_cat_id, per_page: p_page},
                    dataType: "html",
                    success: function (data) {
                        $('#item-list').empty();
                        var newPrs = $('<div></div>');
                        newPrs.html(data);
                        newPrs.appendTo("#item-list");
                    }
                }).done(function () {
                    $('#modal-loading').hide();
                });
            } else {
                p_page = p_page - <?php echo $pos_settings->pro_limit; ?>;
            }
        });

        $('#previous').click(function () {
            if (p_page == 'n') {
                p_page = 0;
            }
            if (p_page != 0) {
                $('#modal-loading').show();
                p_page = p_page - <?php echo $pos_settings->pro_limit; ?>;
                if (p_page == 0) {
                    p_page = 'n'
                }
                $.ajax({
                    type: "get",
                    url: "<?= site_url('pos/ajaxproducts'); ?>",
                    data: {category_id: cat_id, subcategory_id: sub_cat_id, per_page: p_page},
                    dataType: "html",
                    success: function (data) {
                        $('#item-list').empty();
                        var newPrs = $('<div></div>');
                        newPrs.html(data);
                        newPrs.appendTo("#item-list");
                    }

                }).done(function () {
                    $('#modal-loading').hide();
                });

            }
        });

        $(document).on('change', '.paid_by', function () {
        
             //added by vikas singh 20-10-16
//                    self = $(this);
//                    choosen = $(this).val();	
//                    $('select.paid_by').not(self).each(function(){
//                       alert(($(this).val()));
//			if($(this).val() != 'CC'){
//                            alert("hello");
//                            if($(this).val() == choosen)
//                            {
//				bootbox.alert('Payment method is already selected');
//                                    //self.prepend("<option value=''>Select Option</option>");
//                                self.select2('val','ss').attr('selected',true);
//                            }
//			}
//                        
//                    });
        // end
            var p_val = $(this).val(),
                id = $(this).attr('id'),
                pa_no = id.substr(id.length - 1);
              
            $('#rpaidby').val(p_val);
            if (p_val == 'cash' || p_val == 'other') {
                $('.pcheque_' + pa_no).hide();
                $('.pcc_' + pa_no).hide();
                $('.pcash_' + pa_no).show();
                $('#payment_note_' + pa_no).focus();
            } else if (p_val == 'CC' || p_val == 'stripe' || p_val == 'ppp') {
                $('.pcheque_' + pa_no).hide();
                $('.pcash_' + pa_no).hide();
                $('.pcc_' + pa_no).show();
                $('#swipe_' + pa_no).focus();
            } else if (p_val == 'Cheque') {
                $('.pcc_' + pa_no).hide();
                $('.pcash_' + pa_no).hide();
                $('.pcheque_' + pa_no).show();
                $('#cheque_no_' + pa_no).focus();
            } else {
                $('.pcheque_' + pa_no).hide();
                $('.pcc_' + pa_no).hide();
                $('.pcash_' + pa_no).hide();
            }
            if (p_val == 'gift_card') {
                $('.gc_' + pa_no).show();
                $('.ngc_' + pa_no).hide();
                $('#gift_card_no_' + pa_no).focus();
            } else {
                $('.ngc_' + pa_no).show();
                $('.gc_' + pa_no).hide();
                $('#gc_details_' + pa_no).html('');
            }
			
			 if (p_val == 'credit_voucher') {
                $('.cv_' + pa_no).show();
                $('.ngc_' + pa_no).hide();
                $('#credit_voucher_no_' + pa_no).focus();
            } else {
                $('.ngc_' + pa_no).show();
                $('.cv_' + pa_no).hide();
                $('#cv_details_' + pa_no).html('');
            }
        });

       
        $('#suspend').click(function () {
            if (count <= 1) {
                bootbox.alert('<?= lang('x_suspend'); ?>');
                return false;
            } else {
                $('#susModal').modal();
            }
        });
        $('#suspend_sale').click(function () {
            ref = $('#reference_note').val();
            if (!ref || ref == '') {
                bootbox.alert('<?= lang('type_reference_note'); ?>');
                return false;
            } else {
                suspend = $('<span></span>');
                <?php if ($sid) { ?>
                suspend.html('<input type="hidden" name="delete_id" value="<?php echo $sid; ?>" /><input type="hidden" name="suspend" value="yes" /><input type="hidden" name="suspend_note" value="' + ref + '" />');
                <?php } else { ?>
                suspend.html('<input type="hidden" name="suspend" value="yes" /><input type="hidden" name="suspend_note" value="' + ref + '" />');
                <?php } ?>
                suspend.appendTo("#hidesuspend");
                $('#total_items').val(count - 1);
                $('#pos-sale-form').submit();

            }
        });
    });
    <?php if($pos_settings->java_applet) { ?>
    $(document).ready(function () {
        $('#print_order').click(function () {
            printBill(order_data);
        });
        $('#print_bill').click(function () {
            printBill(bill_data);
        });
    });
    <?php } else { ?>
    $(document).ready(function () {
        $('#print_order').click(function () {
            Popup($('#order_tbl').html());
        });
        $('#print_bill').click(function () {
            Popup($('#bill_tbl').html());
        });
    });
    <?php } ?>
    $(function () {
        $(".alert").effect("shake");
        setTimeout(function () {
            $(".alert").hide('blind', {}, 500)
        }, 8000);
        <?php if($pos_settings->display_time) { ?>
        var now = new moment();
        $('#display_time').text(now.format((site.dateFormats.js_sdate).toUpperCase() + " HH:mm"));
        setInterval(function () {
            var now = new moment();
            $('#display_time').text(now.format((site.dateFormats.js_sdate).toUpperCase() + " HH:mm"));
        }, 1000);
        <?php } ?>
    });

    // added by vikas singh 12-10-2016
    $(document).ready(function() {
       
      $('.cardno').bind('keyup blur',function(){ 
            var node = $(this);
            node.val(node.val().replace(/[^0-9]+/i, '') );
        }
       );
    
      $(".formToValidate").validate();
        $(".checkBox").each(function (item) {
            $(this).rules("add", {
                required: true,
                minlength:3
            });
        });
 
   $("#pos-payment-form").validate({
        
           rules:{
               'ccc_no[0]':{
                            required:true,
                            creditcard: true
                        },
            
                        
              'ccc_month[0]':{
                            required:true
                          
                        },
              'ccc_year[0]':{
                            required:true
                          
                        },
              'ccheque_no[0]':{
                           required:true,
                           number:true,
                           maxlength:6,
                           minlength:6
                           
                      }   
     
           },
           
           messages:{
               
               'ccc_no[0]':{
                           required:'Please enter credit card number',
                           creditcard:'Please enter valid credit card no'
                   
               },
               
               'ccc_month[0]':{
                            required:"choose month of expire"
                          
                        },
               'ccc_year[0]':{
                          required:"choose year of expire"

               },
               
               'ccheque_no[0]':{
                           required:"Please enter a valid cheque number",
                           number:"Cheque number must be a number"
                           
                      }   
              
               
           },
           
           submitHandler:function(form){
                
           var rv;
           $('.paid_by').each(function() {
             
           var paidmethod = $(this).val();
           
           var amountc = $(this).closest('div.payment').find('.amount').val();
      
           if(paidmethod == 'cash' && amountc > 200000 )
             {    
                bootbox.alert("Cash payment method can not accept more than Rs.200000, kindly choose another payment method");
                rv = false;
             }
            });
            
            if(rv != undefined){
               
                return false;
            }
            
                //added by vikas 20-10-2016
            var pa_methods = [];

            $('select.paid_by').each(function(){
                pa_methods.push($(this).val());
            });

            if(pa_methods.indexOf('ss') > -1){
                bootbox.alert("Please Select an Option");
                return false;
            } 
               // end
               //alert(total_paid);
               //alert(grand_total);
                if (total_paid < grand_total) {
                     bootbox.alert("Invalid amount");
                     return false;
                   // bootbox.confirm("Invalid amount", function (res) {
               
//                bootbox.confirm("<?= lang('paid_l_t_payable'); ?>", function (res) {
//                    if (res == true) {
//                        $('#pos_note').val(localStorage.getItem('posnote'));
//                        $('#staff_note').val(localStorage.getItem('staffnote'));
//                        $('#submit-sale').text('<?=lang('loading');?>').attr('disabled', true);
//                   
//                       $('#pos-sale-form').submit();
//                    }
//                });
//                return false;
            } else {
               
               
                $('#pos_note').val(localStorage.getItem('posnote'));
                   
                $('#staff_note').val(localStorage.getItem('staffnote'));
               
                //console.log($('#pos_note').val(), $('#staff_note').val());
                 
                //$(this).text('<?=lang('loading');?>').attr('disabled', true);
                
                $('#pos-sale-form').submit();
            }
            }
            });
             
             
             $.validator.addMethod("cvvnumber",
                     function(value, element) {
                        return /^[0-9]{3,4}$/.test(value);
                    }); 
                 
            });
            
           // end by vikas singh 12-10-2016 
           

    <?php if(!$pos_settings->java_applet) { ?>
    function Popup(data) {
        var mywindow = window.open('', 'sma_pos_print', 'height=500,width=300');
        mywindow.document.write('<html><head><title>Print</title>');
        mywindow.document.write('<link rel="stylesheet" href="<?= $assets ?>styles/helpers/bootstrap.min.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');
        mywindow.print();
        mywindow.close();
        return true;
    }
    <?php } ?>
</script>
<?php
$s2_lang_file = read_file('./assets/config_dumps/s2_lang.js');
foreach (lang('select2_lang') as $s2_key => $s2_line) {
    $s2_data[$s2_key] = str_replace(array('{', '}'), array('"+', '+"'), $s2_line);
}
$s2_file_date = $this->parser->parse_string($s2_lang_file, $s2_data, true);
?>
<script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/perfect-scrollbar.min.js"></script>

<script type="text/javascript" src="<?= $assets ?>js/select2.min.js"></script> 

<script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/jquery.calculator.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/additional-methods.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>pos/js/plugins.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>pos/js/parse-track-data.js"></script>
<script type="text/javascript" src="<?= $assets ?>pos/js/pos.ajax.js"></script>
<?php if ($pos_settings->java_applet) { ?>
    <script type="text/javascript" src="<?= $assets ?>pos/qz/js/deployJava.js"></script>
    <script type="text/javascript" src="<?= $assets ?>pos/qz/qz-functions.js"></script>
    <script type="text/javascript">
        deployQZ('themes/<?=$Settings->theme?>/assets/pos/qz/qz-print.jar', '<?= $assets ?>pos/qz/qz-print_jnlp.jnlp');
        function printBill(bill) {
            usePrinter("<?= $pos_settings->receipt_printer; ?>");
            printData(bill);
        }
        <?php
        $printers = json_encode(explode('|', $pos_settings->pos_printers));
        echo $printers.';';
        ?>
        function printOrder(order) {
            for (index = 0; index < printers.length; index++) {
                usePrinter(printers[index]);
                printData(order);
            }
        }
    </script>
	
<?php } ?>
    

<script type="text/javascript" charset="UTF-8"><?= $s2_file_date ?></script>
<div id="ajaxCall"><i class="fa fa-spinner fa-pulse"></i></div>
</body>
</html>
