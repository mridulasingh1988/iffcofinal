
<div class="clearfix"></div>
<?= '</div></div></div></div></div>'; ?>
<div class="clearfix"></div>
<footer>
<a href="#" id="toTop" class="blue" style="position: fixed; bottom: 30px; right: 30px; font-size: 30px; display: none;">
    <i class="fa fa-chevron-circle-up"></i>
</a>

    <p style="text-align:center;">&copy; <?= date('Y') . " " . $Settings->site_name; ?> (v<?= $Settings->version; ?>
        ) <?php if ($_SERVER["REMOTE_ADDR"] == '127.0.0.1') {
            echo ' - Page rendered in <strong>{elapsed_time}</strong> seconds';
        } ?></p>
</footer>
<?= '</div>'; ?>
<div class="modal fade in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
<div class="modal fade in" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true"></div>

<div class="modal" id="lotModal" tabindex="-1" role="dialog" aria-labelledby="lotModalLabel" aria-hidden="true" style="z-index:20000">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">
                        <i class="fa fa-2x">&times;</i>
                    </span>
                    <span class="sr-only"><?=lang('close');?></span>
                </button> -->
                <h4 class="modal-title" id="lotModalLabel"></h4>
            </div>
            
            <div class="modal-body" id="pr_popover_content">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group">
                            <?=lang("lot_no");?>*
                            <input id="lot_no" class="form-control" name="lot_no" value="" readonly="readonly" required="required"
                                   onclick="this.select();" type="text">
                            <a id="genlot" href="javascript:void(0)" style="display:inline;">
                                <i class="fa fa-cogs"></i>
                            </a><div style="width:252px;float:left;color:blue">Please click on the icon to generate lot</div>
                            <div class="clearfix"></div>
                        </div>
                    
                        <div class="form-group">
                            <?=lang("lot_qty");?>*
                            <input type="text" class="form-control" name="edit_lot_qty" id="edit_lot_qty" required="required" onkeypress="return isNumberKey(event,this)"/>
                        </div>
                    
                        <div class="form-group">
                            <?=lang("batch_no");?>
                            <input type="text" class="form-control" name="edit_batch_no" id="edit_batch_no" required/>
                        </div>
                    
                        <div class="form-group">
                            <?=lang("mfg_date");?>
                            <input type="text" class="form-control" name="edit_mfg_date" id="edit_mfg_date" readonly="readonly" />
                        </div>
                    
                        <div class="form-group">
                            <?=lang("exp_date");?>
                            <input type="text" class="form-control" name="edit_exp_date" value="" id="edit_exp_date" readonly = "readonly"/>
                        </div>
                    </div>
<!--                    
                    <div class="col-sm-8">
                        <div class="form-group">
                            <button type="button" class="btn btn-primary" id="editLot"><?= lang('submit') ?></button>
                        </div>
                    </div>-->
                </div>
                
            </div>
            
            <div class="modal-footer">
                <input type="hidden" id="item_id" value="" />
                <input type="hidden" id="gp_item_id" value="" />
                <input type="hidden" id="ro_val" value="" />
                <input type="hidden" id="lot_remaining_qty" value="" />
                <input type="hidden" id="lot_total_qty" value="" />
                <input type="hidden" id="is_batch" value="" />
                <button type="button" class="btn btn-primary editLot" id="editLot"><?= lang('submit') ?></button>
                <button type="button" class="btn btn-danger" id="cancelButton"><?= lang('cancel') ?></button>
                
            </div>
            
        </div>
    </div>
</div>
<div class="modal" id="binModal" tabindex="-1" role="dialog" aria-labelledby="binModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">
                        <i class="fa fa-2x">&times;</i>
                    </span>
                    <span class="sr-only"><?=lang('close');?></span>
                </button> -->
                <h4 class="modal-title" id="binModalLabel"></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                   <div class="col-sm-8">
                        <div class="form-group">
                            <?=lang("lot_no");?>*
                            <input type="text" class="form-control" name="bin_lot_no" value="" readonly="readonly" id="bin_lot_no"/>
                        </div>
                    </div>
                    
                    <div class="col-sm-8">
                        <div class="form-group">
                            <?=lang("bin_qty");?>
                            <input type="text" class="form-control" name="bin_qty" value="" id="bin_qty" onkeypress="return isNumberKey(event,this)"/>
                        </div>
                    </div>
                    
                    <div class="col-sm-8">
                        <div class="form-group">
                            <?=lang("bin_name");?>
                            <?php                            
                          //  $bin[0] = 'Select';
                            foreach ($all_bin as $val) {
                                $bin[$val->id] = $val->bin_nm;
                            }
                            echo form_dropdown('bin_id', $bin, $bin[0], 'id="bin_id" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . '" required="required" style="width:100%;" ');
                            ?>
                            <!-- <select name="edit_bin_name" id="edit_bin_name" class="form-control">
                                <option value="">--Select--</option>
                                <option value="1" selected="selected">BIN002</option>
                            </select> -->
                        </div>
                    </div>
                    
                    <div class="col-sm-8">
                        <div class="form-group">
                            <input type="hidden" id="bin_item_id" value="" />
                            <input type="hidden" id="bin_remaining_qty" value="" />
                            <button type="button" class="btn btn-primary editBin" id="editBin"><?= lang('submit') ?></button>
                            
                            <button type="button" class="btn btn-danger" id="cancelButton"><?= lang('cancel') ?></button>
                        </div>
                    </div>
               
            </div>
            <div class="modal-footer">
<!--                <button type="button" class="btn btn-primary" id="editLot"><?= lang('submit') ?></button>-->
            </div>
        </div>
    </div>
</div>

<div id="modal-loading" style="display: none;">
    <div class="blackbg"></div>
    <div class="loader"></div>
</div>
<div id="ajaxCall"><i class="fa fa-spinner fa-pulse"></i></div>
<?php unset($Settings->setting_id, $Settings->smtp_user, $Settings->smtp_pass, $Settings->smtp_port, $Settings->update, $Settings->reg_ver, $Settings->allow_reg, $Settings->default_email, $Settings->mmode, $Settings->timezone, $Settings->restrict_calendar, $Settings->restrict_user, $Settings->auto_reg, $Settings->reg_notification, $Settings->protocol, $Settings->mailpath, $Settings->smtp_crypto, $Settings->corn, $Settings->customer_group, $Settings->envato_username, $Settings->purchase_code); ?>
<script type="text/javascript">
var dt_lang = <?=$dt_lang?>, dp_lang = <?=$dp_lang?>, site = <?=json_encode(array('base_url' => base_url(), 'settings' => $Settings, 'dateFormats' => $dateFormats))?>;
var lang = {paid: '<?=lang('paid');?>', pending: '<?=lang('pending');?>', completed: '<?=lang('completed');?>', ordered: '<?=lang('ordered');?>', received: '<?=lang('received');?>', partial: '<?=lang('partial');?>', sent: '<?=lang('sent');?>', r_u_sure: '<?=lang('r_u_sure');?>', due: '<?=lang('due');?>', returned: '<?=lang('returned');?>', transferring: '<?=lang('transferring');?>', active: '<?=lang('active');?>', inactive: '<?=lang('inactive');?>', unexpected_value: '<?=lang('unexpected_value');?>', select_above: '<?=lang('select_above');?>','item_details':'<?php echo lang('item_details')?>','stock_details':'<?php echo lang('stock_details')?>','item_name':'<?php echo lang('item_name')?>','unit':'<?php echo lang('unit')?>','bill_qty':'<?= lang("bill_qty"); ?>','recv_qty':'<?= lang("recv_qty"); ?>','rej_qty':'<?= lang("rej_qty"); ?>','short_qty':'<?= lang("short_qty"); ?>','qty':'<?= lang("qty"); ?>','action':'<?= lang("action"); ?>','lot_name':'<?= lang("lot_name"); ?>','bin_name':'<?= lang("bin_name"); ?>'};
var segment_id = '<?php echo $_SESSION['segment_id']?>';
//var wh_id = '<?php echo $_SESSION['segment_id']?>';
</script>
<?php
$s2_lang_file = read_file('./assets/config_dumps/s2_lang.js');
foreach (lang('select2_lang') as $s2_key => $s2_line) {
    $s2_data[$s2_key] = str_replace(array('{', '}'), array('"+', '+"'), $s2_line);
}
$s2_file_date = $this->parser->parse_string($s2_lang_file, $s2_data, true);
?>
<script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.dtFilter.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/select2.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/jquery-ui.min.js"></script>
<?php if($this->uri->segment(2) !='open_register')
{?>
<script type="text/javascript" src="<?= $assets ?>js/bootstrapValidator.min.js"></script>
<?php }?>
<!--<script type="text/javascript" src="<?= $assets ?>pos/js/jquery.validate.min.js"></script>-->
<script type="text/javascript" src="<?= $assets ?>js/jquery.validate.min.js"></script>

<script type="text/javascript" src="<?= $assets ?>js/additional-methods.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/validation.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/jquery.calculator.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/core.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/perfect-scrollbar.min.js"></script>
<?= ($m == 'purchases' && ($v == 'add' || $v == 'edit' || $v == 'purchase_by_csv')) ? '<script type="text/javascript" src="' . $assets . 'js/purchases.js"></script>' : ''; ?>
<?= ($m == 'transfers' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/transfers.js"></script>' : ''; ?>
<?= ($m == 'sales' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/sales.js"></script>' : ''; ?>
<?= ($m == 'quotes' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/quotes.js"></script>' : ''; ?>

<script type="text/javascript" charset="UTF-8">var r_u_sure = "<?=lang('r_u_sure')?>";
    <?=$s2_file_date?>
    $.extend(true, $.fn.dataTable.defaults, {"oLanguage":<?=$dt_lang?>});
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
    $(window).load(function () {
        $('.mm_<?=$m?>').addClass('active');
        $('.mm_<?=$m?>').find("ul").first().slideToggle();
        $('#<?=$m?>_<?=$v?>').addClass('active');
        $('.mm_<?=$m?> a .chevron').removeClass("closed").addClass("opened");
    });
</script>
</body>
</html>