<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_customer'); ?></h4>
        </div>
        <?php $attrib = array('role' => 'form','id'=>'add-customer-form');
        echo form_open_multipart("customers/add/".$page, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
           
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group person required">
                        <?= lang("name", "name"); ?>
                        <?php echo form_input('name', '', 'class="form-control tip" id="name" data-bv-notempty="true"'); ?>
                    </div>
                    <div class="form-group person required">
                        <?= lang("father_name", "father_name"); ?>
                        <?php echo form_input('father_name', '', 'class="form-control tip" id="father_name" data-bv-notempty="true"'); ?>
                    </div>
<!--                    <div class="form-group">
                        <?= lang("vat_no", "vat_no"); ?>
                        <?php echo form_input('vat_no', '', 'class="form-control" id="vat_no"'); ?>
                    </div>-->
                    <!--<div class="form-group company">
                    <?= lang("contact_person", "contact_person"); ?>
                    <?php echo form_input('contact_person', '', 'class="form-control" id="contact_person" data-bv-notempty="true"'); ?>
                </div>-->
                    <div class="form-group">
                        <?= lang("email_address", "email_address"); ?>
                      
                        <input type="email" name="email" class="form-control" id="email_address"/>
                    </div>
                    <div class="form-group required">
                        <?= lang("phone", "phone"); ?>
                        <input type="text" name="phone" class="form-control" required="required" id="phone" />
                    </div>
                    <div class="form-group required">
                        <?= lang("address", "address"); ?>
                          
                           
                        <?php echo form_textarea('address', '', 'class="form-control" id="address" required="required" style="height: 50px;"'); ?>
                    </div>
                    <div class="form-group required">
                       <?= lang("city", "city"); ?>
                        <?php echo form_input('city', '', 'class="form-control" id="city" required="required"'); ?>   
                    </div>
                    <div class="form-group required">                            
                        <?= lang("Type of Customer","Type of Customer");?>
                        <?php
                        $customer_type=array('endcustomer'=>'End Customer','retailer'=>'Retailer','wholeseller'=>'Wholeseller');
                        echo form_dropdown('customer_type', $customer_type,$this->settings,'class="form-control tip select customer_type" id="customer_type" style="width:100%;" required="required"'); ?>
                                                     
                    </div>
                    
                    <!---
                     <div class="form-group">
                        <?= lang("username","username");?>
                        <?php echo form_input('username','','class="form-control" id="username" data-bv-notempty="true"')?>
                    </div>
                    <div class="form-group">
                        <?= lang("imei_number","imei_number");?>
                        <?php echo form_input('imei_number','','class="form-control" id="imei_number"')?>
                    </div>
                    -->
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group required">
                                <?= lang("postal_code", "postal_code"); ?>
                                <?php echo form_input('postal_code', '', 'class="form-control" id="postal_code"'); ?>
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group required">

                                <?= lang("country", "country"); ?>
                                    <?php echo '<div id="countrycombox">'. form_dropdown('country', $country, $this->settings,'class="form-control tip" id="country" style="width:100%;" required="required"').'</div>'; ?>
                            </div>
                        </div>    
                    <div class="col-md-6">
                      <div class="form-group required">
                         <?= lang("state", "state"); 
                                        foreach ($state_default as $value){
                                            $arrStates[$value->state_id] = $value->state_name;
                                        }
                                        ?>
                            <?php echo form_dropdown('state',array_filter($arrStates),$this->settings,'class="form-control" id="statelist" style="width:100%;" required="required"'); ?>
                    </div>
                    </div>
                 
                     </div>                   
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group required">
                                <div class="controls">
                                    <?= lang("id_proof1", "id_proof"); ?>
                                    <?php
                                    $id_proof=array('pan_card'=>'Pan Card','voter_id'=>'Voter Id Card','rasan_card'=>'Rasan Card','other'=>'Other','not_available'=>'Not Available');
                                    echo form_dropdown('id_proof', $id_proof,$this->settings,'class="form-control tip select" id="id_proof" style="width:100%;" required="required"'); ?>
                                </div>                          
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class= "form-group required">
                               <?= lang("id_proof_no","id_proof_no");?>
                               <?php echo form_input('id_proof_no','','class="form-control" id="id_proof_no" required="required" maxlength=20')?>
                           </div>
                        </div>             
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class= "form-group">
                               <?= lang("Adhaar_Card_No","adhaar_card_no");?>
                               <?php echo form_input('adhaar_card_no','','class="form-control" id="adhaar_card_no" maxlength=20')?>
                           </div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="nominees">
                            <div>
                                <div class="col-md-5">
                                    <div class= "form-group required">
                                        <?= lang("Nominee","Nominee");?>
                                        <?php echo form_input('nominee[]','','class="form-control nominee" required="required"')?>

                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group required">


                                        <?= lang("dob", "dob"); ?>
                                        <?php echo form_input('dob_nominee[]', '', 'class="form-control input-tip datetime" required="required"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                          <?= lang("Add", "Add"); ?>
                                        <button type="button" class="btn btn-default addBtn"><i class="fa fa-plus"></i></button>



                                </div>
                            </div>  
                        </div>

                    </div>
 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group has-feedback required">
                                <?= lang("loyalty_card_id","loyalty_card_id");?>
                                <?php //echo form_input('loyalty_card_id','','class="form-control" id="loyalty_card_id" data-bv-notempty="true"')?> 
                                <div class="input-group">
                                    <input id="loyalty_card_id" class="form-control" type="text" value="" name="loyalty_card_id" data-bv-field="loyalty_card_id">
                                    <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                        <a id="genNo5" href="#">
                                            <i class="fa fa-cogs"></i>
                                        </a>
                                    </div>
                                </div>
                                <i class="form-control-feedback" style="display: none; top: 0px; z-index: 100;" data-bv-icon-for="loyalty_card_id"></i>
                                    <small class="help-block" style="display: none;" data-bv-validator="notEmpty" readonly="readonly" data-bv-for="loyalty_card_id" data-bv-result="NOT_VALIDATED">Please enter/select a value</small>
                            </div>
                        </div>
                    </div>
             </div>     
                          
                    <!---
                    <div class="form-group">
                        <?= lang("doc_name","doc_name");?>
                        <?php echo form_input('doc_name','pan_card','class="form-control" id="doc_name"')?>
                    </div>
                    <div class="form-group">
                        <?= lang("customer_id","customer_id");?>
                        <?php echo form_input('customer_id',$cust_id->id + 1,'class="form-control" id="customer_id" data-bv-notempty="true"')?>
                    </div>
                    <div class="row">
                    <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("lat","lat");?>
                        <?php echo form_input('lat','','class="form-control" id="lat"')?>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("long","long");?>
                        <?php echo form_input('long','','class="form-control" id="long"')?>
                    </div>
                    </div>
                    </div>
                   
                <div class="form-group">
                        <?= lang("ccf1", "cf1"); ?>
                        <?php echo form_input('cf1', '', 'class="form-control" id="cf1"'); ?>
 
 
                    </div>
                    <div class="form-group">
                        <?= lang("ccf2", "cf2"); ?>
                        <?php echo form_input('cf2', '', 'class="form-control" id="cf2"'); ?>
 
 
                    </div>
                    <div class="form-group">
                        <?= lang("ccf3", "cf3"); ?>
                        <?php echo form_input('cf3', '', 'class="form-control" id="cf3"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("ccf4", "cf4"); ?>
                        <?php echo form_input('cf4', '', 'class="form-control" id="cf4"'); ?>
 
 
                    </div>
                    <div class="form-group">
                        <?= lang("ccf5", "cf5"); ?>
                        <?php echo form_input('cf5', '', 'class="form-control" id="cf5"'); ?>
 
 
                    </div>
                    <div class="form-group">
                        <?= lang("ccf6", "cf6"); ?>
                        <?php echo form_input('cf6', '', 'class="form-control" id="cf6"'); ?>
                    </div>                 -->
            </div>
             <!-- Customer informations Altered By Anil 26-06-17 Start -->   

                <div id = "gst_info">     
                    <div class="row">                                              
                        <div class="col-md-4">
                            <div class= "form-group required">
                                <?= lang("GST No","GST No");?>
                                <?php echo form_input('gst_no','','class="form-control" required="required"')?>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group required">

                                <?= lang("Effective Date ", "Effective Date "); ?>
                                <?php echo form_input('gst_effect_date', '', 'class="form-control input-tip datetime" id="gst_effect_date" required="required"'); ?>
                            </div>
                        </div>                        
                        <div class="col-md-4">
                            <div class= "form-group">
                                <?= lang("mFMS ID","mFMS ID");?>
                                <?php echo form_input('mfms_id','','class="form-control"')?>

                            </div>
                        </div>                         
                    </div>

                    <div class="row">
                            <div class="col-md-6"><?= lang("Information of Licenses","Information of Licenses");?></div>   
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2">
                            <div class= "form-group">                               
                                <label for="Products">Products</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class= "form-group required">
                                <label for="licencenumber">Licence No</label>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group required">
                                <label for="expirydate">Date of Expiry</label>
                            </div>
                        </div>
                        <div class="col-md-1"></div> 
                    </div>
   
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2">
                            <div class= "form-group">                                
                            <label for="Fertilizers">Fertilizers</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class= "form-group">                                
                                <?php echo form_input('fertilizers_licence_no','','class="form-control" id="fertilizers_licence_no"')?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group ">                                
                                <?php echo form_input('fertilizers_exp_date', '', 'class="form-control input-tip datetime" id="fertilizers_exp_date"'); ?>
                            </div>
                        </div>
                        <div class="col-md-1"></div> 
                    </div>           

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2">
                            <div class= "form-group">
                                <label for="Pesticides">Pesticides</label>  
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class= "form-group">                                
                                <?php echo form_input('pesticides_licence_no','','class="form-control" id="pesticides_licence_no"') ?>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">                                
                                <?php echo form_input('pesticides_exp_date', '', 'class="form-control input-tip datetime" id="pesticides_exp_date"'); ?>
                            </div>
                        </div>
                        <div class="col-md-1"></div> 
                    </div> 

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2">
                            <div class= "form-group">  
                                <label for="Seeds">Seeds</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class= "form-group">                                
                                <?php echo form_input('seeds_licence_no','','class="form-control" id ="seeds_licence_no"')?>
                            </div>
                        </div>
                        <div class="col-md-4">                            
                            <div class="form-group">                                                             
                                <?php echo form_input('seeds_exp_date','', 'class="form-control input-tip datetime" id="seeds_exp_date" readonly="readonly" '); ?>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>             

             <!-- Customer informations Altered By Anil 26-06-17 End -->
        </div>
        <div class="modal-footer">
            <?php echo form_submit('add_customer', lang('add_customer'), 'class="btn btn-primary" id ="add_customer"'); ?>           
        
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<script type="text/javascript">   

    $(document).ready(function(){
        var phoneurls ="<?php echo base_url();?>customers/checkPhoneNumber";
        var emailurls ="<?php echo base_url();?>customers/checkEmailId";
     
                var MAX_OPTIONS = 4;
                $("#add-customer-form").validate({
            rules: {
                name: "required",
                father_name: "required",

                vat_no: {
                    required: true,
                    digits:true
                },
                contact_person: {
                    required: true
                },
                phone: {
                    required: true,
                    digits:true,
                    mobchecktest:true,
                    remote: {
			url: phoneurls,
			type: "post",
			data: {
		phone: function(){ return $("#phone").val(); }
			}
		}
                },
                
                email: {
		
		email: true,
		remote: {
			url: emailurls,
			type: "post",
			data: {
				email: function(){ return $("#email_address").val(); }
			}
		   }
	        },

                
                address: {
                    required:true
                },
                city:{required:true},
                state:{required:true},
                country:{required:true},
                postal_code:{digits:true,postalcodechecktest:true},
                username:{required:true,namecharcheck:true},
                id_proof_no:{required:true},
                loyalty_card_id:{checkLoyaltyNumber:true}
//                loyalty_card_id:{loyalty_card_number_check:true},
//                adhaar_card_no:{
//                            digits:true,
//                            required:true,
//                            minlength:12,
//                            maxlength:12,   
//                            adhaarNoTest:true 
//                        }
                    },
			messages: {
				name: "Please enter your name",
				father_name: "Please enter your father name",

				contact_person: {
					required: "Please enter contact person",
				},
				phone: {
					required: "Please enter valid mobile number",
					mobchecktest: "Please enter mobile number stating with 7,8,9 and 10 digits length",
                                        remote: 'Phone number already exist!'
				},
				email: {
                                        email: 'Please enter a valid email address',
                                        remote: 'Email Id already exist!'
				},
				address: "Please enter address",
				city: "Please enter city",
                                state: "Please enter state",
                                country: "Please enter country",
                                postal_code:{digits:"Please enter digits only",postalcodechecktest:"Please enter valid postal code"},
                                username:{required:'Username cannot be left empty',namecharcheck:"Please alphanumeric username"},
                                id_proof_no:{required:'Please enter id proof number'},
                                
//                adhaar_card_no:{
//                    required:"Please enter your adhaar card no",
//                    digits: 'Please use numeric characters only.',
//                    minlength: 'The aadhaar number must be 12 characters long',
//                    maxlength: 'The aadhaar number must be 12 characters long',
//                    adhaarNoTest:'Please enter valid adhaar card no'
//                },
//              loyalty_card_id:{loyalty_card_number_check:"Please select valid loyalty card id"}

			},

            errorElement: "em",
                errorPlacement: function ( error, element ) {
                    // Add the `help-block` class to the error element
                    error.addClass( "help-block" );

                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents( ".form-group" ).addClass( "has-feedback" );

                    if ( element.prop( "type" ) === "checkbox" ) {
                        error.insertAfter( element.parent( "label" ) );
                    } else {
                        error.insertAfter( element );
                    }

                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if ( !element) {
                        $( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
                    }
                },
                success: function ( label, element ) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if ( !$( element )) {
                        $( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
                    }
                },
                highlight: function ( element, errorClass, validClass ) {
                    $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                    $( element ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
                },
                unhighlight: function ( element, errorClass, validClass ) {
                    $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                    $( element ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
                }
                        });
                
                $.validator.addMethod("mobchecktest",
                     function(value, element) {
                        return /^[9 8 7]\d{9}$/.test(value);
                }); 
                
                $.validator.addMethod("adhaarNoTest",
                     function(value, element) {
                        return validate(value);
                }); 
                
                $.validator.addMethod("postalcodechecktest",
                     function(value, element) {
                        return /^\d{6}$/.test(value);
                }); 
                
                $.validator.addMethod("namecharcheck",
                     function(value, element) {
                        return /^[a-zA-Z\s]*$/.test(value);
                });  
                
                $.validator.addMethod("loyalty_card_number_check",
                     function(value, element) {
                        return /^\d{12}$/.test(value);
                }); 
                
                $.validator.addMethod("checkLoyaltyNumber", 
                    function(value, element) {             
                        $.ajax({
                            type:"POST",
                            async: false,
                            url: '<?=  base_url("customers/getLoyaltyNumberAvaibility")?>', // script to validate in server side
                            data: {loyaltynumber: value},
                            success: function(data) {
                            //console.log('data :' + data);
                                result = ((data == 1)) ? false : true;
                            }
                        });
                        return result; 
                    }, 
                    "This Loyalty Number is already taken! Try another."
        );
                
 
  var maxField = 5; //Input fields increment limitation
    var addBtn = $(".addBtn"); //Add button selector
    var wrapper = $('#nominees'); //Input field wrapper
   
    var x = 1; //Initial field counter is 1
    var y = 1;

    $(addBtn).click(function(){ //Once add button is clicked
        if(y < maxField){ //Check maximum number of input fields
            
            var cfieldHTML ="";
           // alert("x"+x+"max"+maxField)
            x++; //Increment field counter
            y++; //Increment field counter
            cfieldHTML += '<div>';
            cfieldHTML += '<div class="col-md-5">';
            cfieldHTML += '<div class= "form-group">';
            cfieldHTML += '<label for="nominee_one">Nominee</label>';
            cfieldHTML += '<input type="text" name="nominee[]" id="nominee_'+x+'" value="" class="form-control nominee"/>';
            cfieldHTML += '</div>';
            cfieldHTML += '</div>';
            cfieldHTML += '<div class="col-md-5">';
            cfieldHTML += '<div class="form-group">';
            cfieldHTML += '<label for="nominee_one">dob</label>';
            cfieldHTML += '<input type="text" name="dob_nominee[]" id="dob_nominee_'+x+'" value="" class="form-control input-tip datetime"/>';    
            cfieldHTML += '</div>'; 
            cfieldHTML += '</div>'; 
          
            cfieldHTML += '<button type="button" class="btn btn-default remove_button" style="margin-left: 15px;margin-top: 45px;"><i class="fa fa-minus"></i></button>';
            cfieldHTML += '</div>';
            cfieldHTML += '</div>';
            cfieldHTML += '</div>';
            $(wrapper).append(cfieldHTML); // Add field html
            
            $('#myModal').css('overflow-y', 'scroll');
            var dt = new Date();
            dt.setFullYear(new Date().getFullYear()-18);
            $('.datetime').datetimepicker({
                changeMonth: true,
                        autoclose:true,
                        showSecond: false,
                        minView: 2,
                        format: 'yyyy-mm-dd',
                        endDate: dt,
            }).on('change', function() {
                $(this).valid();  // triggers the validation test
                // '$(this)' refers to '$("#datepicker")'
            });
            $('.datetime').attr('readonly',true);           
        }
         else{
              bootbox.alert('Maximum 5 Nominees are allowed');
                return false;
        }     
        
     
    });
    // Added By Anil
    $('#loyalty_card_id').attr('readonly',true);   
    $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove();
        //$(this).parent('div').remove(); //Remove field html
        y--; //Decrement field counter
    });
 
 
 // comment by vikas singh       
//        $(document).on('click', '.addBtn', function() {
//             //alert('MAX_OPTIONS :' + MAX_OPTIONS + $('#add-customer-form').find(':visible[name="nominee[]"]').length);
//            if ($('#add-customer-form').find('input:visible[name="nominee[]"]').length > MAX_OPTIONS) {
//                    $('#add-customer-form').find('.addBtn').attr('disabled', 'disabled');
//                    return false;
//
//            }
//            var $template = $('#taskTemplate'),
//                $clone    = $template
//                                .clone()
//                                .removeClass('hide')
//                                .removeAttr('id')
//                                .insertBefore($template);
//
//            // Add new fields
//            // Note that we DO NOT need to pass the set of validators
//            // because the new field has the same name with the original one
//            // which its validators are already set
//
//           $($clone.find('[name="nominee[]"]')).rules('add', {
//                required: true,
//                messages: {
//                    required: "enter nominee name"
//                }
//            });
//            
//             $($clone.find('[name="dob_nominee[]"]')).rules('add', {
//                required: true,
//                messages: {
//                    required: "enter nominee dob"
//                }
//            });
//          
//        })
//        
//         $(document).on('click', '.removeButton', function() {
//            var $row = $(this).closest('.form-group');
//            //$($row.find('[name="nominee[]"]')).rules("remove","required");
//            //$($row.find('[name="dob_nominee[]"]')).rules("remove","required");
//            if ($('#add-customer-form').find(':visible[name="nominee[]"]').length < MAX_OPTIONS) {
//                $('#add-customer-form').find('.addBtn').removeAttr('disabled');
//            }
//            // Remove element containing the fields
//            $row.remove();
//        });
        
       
        //if ($('#add-customer-form').find(':visible[name="nominee[]"]').length >= MAX_OPTIONS) {
        //   $('#add-customer-form').find('.addBtn').attr('disabled', 'disabled');
        //}
//JS added by Anil start

         var today = new Date();
         var lastDate = new Date(today.getFullYear(), today.getMonth(0), 31);
         var startDate = new Date(today.getFullYear(), 0, 1);  

        $("#fertilizers_exp_date").datetimepicker({           
            autoclose:true,
            showSecond: false,
            minView: 2,
            format: 'yyyy-mm-dd',
            startDate: today, 
            pickerPosition: "top-right"                
            //endDate: today
        }); 
        $("#pesticides_exp_date").datetimepicker({
            autoclose:true,
            showSecond: false,
            minView: 2,
            format: 'yyyy-mm-dd',
            startDate: today,
            pickerPosition: "top-right"           
        });
        $("#seeds_exp_date").datetimepicker({
            autoclose:true,
            showSecond: false,
            minView: 2,
            format: 'yyyy-mm-dd',
            startDate: today, 
            pickerPosition: "top-right"            
        });
         $("#gst_effect_date").datetimepicker({
            autoclose:true,
            showSecond: false,
            minView: 2,
            format: 'yyyy-mm-dd',
            pickerPosition: "top-right" ,
           // startDate: today,            
        });

        $('#gst_info').css("display", "none");        
        $('.customer_type').on('change',function(){
            var cust_val = $(this).val();
            
            if(cust_val === 'retailer' || cust_val === 'wholeseller'){
                $('#gst_info').css("display", "block");
            }else{
                $('#gst_info').css("display", "none");
            }       

        });

        $('#add_customer').click(function(){
           
            var ferti_licence = $('#fertilizers_licence_no').val();
            var ferti_exp_date = $('#fertilizers_exp_date').val();
            var pesti_licence_no = $('#pesticides_licence_no').val();
            var pesti_exp_date = $('#pesticides_exp_date').val();
            var seeds_licence_no = $('#seeds_licence_no').val();
            var seeds_exp_date = $('#seeds_exp_date').val();
            var gst_info = $('#gst_info').is(":visible");
            var loyalty_id = $('#loyalty_card_id').val();
                        
            if(loyalty_id === ''){
                alert('Please fill Loyalty card id');
                return false;
            }
           
           if(gst_info==true)
           {
            if((ferti_licence=='' || ferti_exp_date=='') && (pesti_licence_no=='' || pesti_exp_date=='') && (seeds_licence_no=='' || seeds_exp_date=='')){
                alert('Please fill atleast one products details');
                return false;
            }else{
            return true;
            } 
           }
 

        });    
        
    //JS added by Anil End 
    

    });
    
 // end   
        var d = [
        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
        [1, 2, 3, 4, 0, 6, 7, 8, 9, 5],
        [2, 3, 4, 0, 1, 7, 8, 9, 5, 6],
        [3, 4, 0, 1, 2, 8, 9, 5, 6, 7],
        [4, 0, 1, 2, 3, 9, 5, 6, 7, 8],
        [5, 9, 8, 7, 6, 0, 4, 3, 2, 1],
        [6, 5, 9, 8, 7, 1, 0, 4, 3, 2],
        [7, 6, 5, 9, 8, 2, 1, 0, 4, 3],
        [8, 7, 6, 5, 9, 3, 2, 1, 0, 4],
        [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
    ];

    // permutation table p
    var p = [
        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
        [1, 5, 7, 6, 2, 8, 3, 0, 9, 4],
        [5, 8, 0, 3, 7, 9, 6, 1, 4, 2],
        [8, 9, 1, 6, 0, 4, 3, 5, 2, 7],
        [9, 4, 5, 3, 1, 2, 6, 8, 7, 0],
        [4, 2, 8, 6, 5, 7, 3, 9, 0, 1],
        [2, 7, 9, 3, 8, 0, 6, 4, 1, 5],
        [7, 0, 4, 6, 9, 1, 3, 2, 5, 8]
    ];

    // inverse table inv
    var inv = [0, 4, 3, 2, 1, 5, 6, 7, 8, 9];

    // converts string or number to an array and inverts it
    function invArray(array) {

        if (Object.prototype.toString.call(array) === "[object Number]") {
            array = String(array);
        }

        if (Object.prototype.toString.call(array) === "[object String]") {
            array = array.split("").map(Number);
        }

        return array.reverse();

    }

    // generates checksum
    function generate(array) {

        var c = 0;
        var invertedArray = invArray(array);

        for (var i = 0; i < invertedArray.length; i++) {
            c = d[c][p[((i + 1) % 8)][invertedArray[i]]];
        }

        return inv[c];
    }

    // validates checksum
    function validate(array) {

        var c = 0;
        var invertedArray = invArray(array);

        for (var i = 0; i < invertedArray.length; i++) {
            c = d[c][p[(i % 8)][invertedArray[i]]];
        }

        return (c === 0);
    }

    var dt = new Date();
    dt.setFullYear(new Date().getFullYear()-18);
    $('.datetime').datetimepicker({
        changeMonth: true,
                autoclose:true,
                showSecond: false,
                minView: 2,
                format: 'yyyy-mm-dd',
                endDate: dt,
    }).on('change', function() {
        $(this).valid();  // triggers the validation test
        // '$(this)' refers to '$("#datepicker")'
    });
   
    $('.datetime').attr('readonly',true);
    $('#customer_id').attr('readonly',true);
    $('#id_proof').change(function(){
        $('#doc_name').val($('#id_proof').find('option:selected').attr('value'));
        $('#doc_name').attr('readonly',true);
        
        var id_proof= $(this).val();
        
        if(id_proof=='not_available')
        {
            $("#id_proof_no").parent().hide();
             
        }
        else
        {
            $("#id_proof_no").parent().show();
           
        }
    });
    
    $('#genNo5').click(function () {
        var no = generateCardNo();
        $(this).parent().parent('.input-group').children('input').val(no);
        $(this).parent().parent('.input-group').children('input').valid();
        //$('#add-customer-form').bootstrapValidator('revalidateField', 'loyalty_card_id');
        return false;
    });




</script>
<script type="text/javascript">
    $(document).ready(function () { 

        $('#countrycombox select').change(function () {
            var selCountry = $(this).val();

            console.log(selCountry);
            $.ajax({   
                url: "customers/ajax_call_country", 
                async: false,
                type: "POST", 
                data: "country_id="+selCountry, 
                dataType: "json", 
                success: function(data) {

                    $('#statelist').html(data);
                }
            })
        });
    });
</script>    
