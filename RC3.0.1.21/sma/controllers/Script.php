<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Script extends MY_Controller
{
function __construct()
    {
        parent::__construct();
        $this->load->model('csv_model');
        $this->load->model('msync_model');
        $this->upload_path = 'assets/csv/';
        $this->allowed_file_size = '1024';
       
        
    }
public function PoSync()
{
  
  
  $this->csv_model->poMaster();
  $this->csv_model->poSchdlMaster();
  
    //$this->session->set_flashdata('message', "PO And PO Schedule Data Sync Successfully");
    //redirect("welcome");
    return true;
  

  

} 
function index()
{
  redirect("script/import_csv");
}

function import_csv()
    {
      //print_r($_SERVER); die;
      $h = $h1 = $csv = $csv1 = '';
    $halfpath = str_replace('/index.php', '/', $_SERVER['SCRIPT_NAME']);
     $fullpath = $_SERVER['DOCUMENT_ROOT'].$halfpath;
     $p=0;
        if (isset($_FILES["userfile"])) {
          
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = TRUE;
                $files = $_FILES;
                //print_r($_FILES);
                //echo $_FILES['userfile']['name'][1]; die;
                
                if($_FILES['userfile']['name'][0] == 'sma_drft_po.csv' && $_FILES['userfile']['name'][1] == 'sma_drft_po_dlv_schdl.csv')
                {
                
              $csv = $_FILES['userfile']['name'][0];
              $csv1 = $_FILES['userfile']['name'][1];
              $h = $fullpath.$this->upload_path.$csv;
              $h1 = $fullpath.$this->upload_path.$csv1;
             // echo "h->".$h."<br>h1->".$h1; die;
             
              $files = $_FILES;
                $cpt = count($_FILES['userfile']['name']);
                 for($i=0; $i<$cpt; $i++)
                {
                  $p++;
                $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                 $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                 $_FILES['userfile']['size']= $files['userfile']['size'][$i];
                 $this->upload->initialize($config);
                 $this->upload->do_upload();
                
                
             }
              if (!$this->upload->do_upload()) {

                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect("script/import_csv");
                }
              } else {
                    $this->session->set_flashdata('error', "Please Select Correct File Name like For PO => 'sma_drft_po' And For Schedule => 'sma_drft_po_dlv_schdl'");
                    redirect("script/import_csv");

              }
              }
        if ($p>=2 && $this->PoSync()) {
            unlink($h); unlink($h1); 
            $this->session->set_flashdata('message', "PO And PO Schedule Data Sync Successfully");
            redirect('script/import_csv');
        } else {

             $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('script'), 'page' => lang('script')), array('link' => '#', 'page' => lang('import_po_by_csv')));
            $meta = array('page_title' => lang('import_script_by_csv'), 'bc' => $bc);
            $this->page_construct('script/import_csv', $meta, $this->data);

        }
    
  }

    


}
?>
