<?php defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', 36000);
ini_set('mysql.connect_timeout', 3000);
ini_set('default_socket_timeout', 3000); 
class Syncin extends MY_Controller
{

    function __construct()
    {
        parent::__construct();        
    }
   
    function syncin(){

     // for new location segment entry.
     $remote_host = REMOTE_HOST;
     $remote_username = REMOTE_USERNAME;
     $remote_password = REMOTE_PWD;
     $remote_dbname = REMOTE_DB;

        $remote_connection = mysqli_connect($remote_host, $remote_username, $remote_password, $remote_dbname) or die("Connection Error " . mysqli_error($remote_connection)); 

        if($remote_connection === false){
            die("ERROR: Could not connect. " . mysqli_connect_error());
        }

           $sql2= "select * from sma_segment WHERE prj_actv='Y'";
           $segment_data = mysqli_query($remote_connection, $sql2) or die("Selection Error " . mysqli_error($remote_connection));
           //print_r($segment_data); die;
           
    while($cols = mysqli_fetch_all($segment_data,MYSQLI_ASSOC)) {
            
        foreach($cols as $col){
            
           $location =$this->db->select('id')
                    ->from('sma_location')
                    ->where('id',$col['id'])
                    ->get();        
      
            $count = $location->num_rows($location);

                if($count ==0)
                  {
              
                  $query = "INSERT ignore INTO sma_location(id,org_id,prj_doc_id,   parent_proj_id,proj_name,prj_actv) VALUES('".$col['id']."','".$col['org_id']."','".$col['prj_doc_id']."','".$col['parent_proj_id']."','".$col['proj_name']."','".$col['prj_actv']."')"; 
                  $this->db->query($query);
                  }

        } 
     }
     // end new location segment query.
     
     
        $directory = $_SERVER["DOCUMENT_ROOT"].'/'.PROJECT_NAME.'/sync-folder/';
        $this->load->model('site');
        $segmentList = $this->site->getAllSegments();  
        /*echo "<pre>";
        print_r($segmentList);
        exit;*/
        $count = empty($segmentList)?0:count($segmentList);
        if($count>0){
            redirect("welcome");
        }

        $meta = array('page_title' => lang('Sync Data from Central server'));
        $this->load->model('site');
        $segmentList = $this->site->getAllLocation();
        
        $this->data['segmentList'] = $segmentList;

        if(isset($_POST['segment_id']) && ($_POST['segment_id']!='')){

            $segment_id = $_POST['segment_id'];
            $remote_host = REMOTE_HOST;
            $remote_username = REMOTE_USERNAME;
            $remote_password = REMOTE_PWD;
            $remote_dbname = REMOTE_DB;

            /*$remote_host = 'localhost';
            $remote_username = 'root';
            $remote_password = '';
            $remote_dbname = 'iffco_final_live';*/

            $remote_connection = mysqli_connect($remote_host, $remote_username, $remote_password, $remote_dbname) or die("Connection Error " . mysqli_error($remote_connection));
            ;

             $sql3 = "UPDATE `sma_csv_history` SET flag=0 WHERE segment_id='".$segment_id."'";
            mysqli_query($remote_connection, $sql3) or die("Selection Error " . mysqli_error($connection));
         
            $sql = "select * from sma_csv_history where segment_id=".$segment_id." AND flag=0 group by file_name ORDER BY created_date ASC ";
            $csv_data = mysqli_query($remote_connection, $sql) or die("Selection Error " . mysqli_error($connection));
            $csv_list = mysqli_fetch_array($csv_data, MYSQLI_ASSOC);
           
            mysqli_autocommit($remote_connection, FALSE);
            $files_list = array();
            foreach($csv_data as $rows){
            //   echo $row['file_name'];exit;
                $csv_date = explode("/",$rows['file_name']);
                $csv_folder = $csv_date[count($csv_date)-2];
                
                if (!is_dir($directory.$csv_folder)) { //create the folder if it's not already exists
                    mkdir($directory.$csv_folder, 0755, TRUE);                
                }  
                $copy = copy( $rows['file_name'], $directory.$csv_folder.'/'.$csv_date[count($csv_date)-1] );            
                /* Add notice for success/failure */
                if( !$copy ) {
                    echo "Failed to copy csv for date".$rows['created_date'];
                    //exit;
                }else{
                     $files_list[] = $directory.$csv_folder.'/'.$csv_date[count($csv_date)-1];
                     $q1 = "UPDATE sma_csv_history SET flag = '1' WHERE id='".$rows['id']."' ";
                     //mysqli_query($remote_connection, $q1);
                    if(mysqli_query($remote_connection, $q1))
                    {
                     /* commit insert */
                        mysqli_commit($remote_connection);
                    }
                    else{
                         /* Rollback */
                        mysqli_rollback($remote_connection);

                    }
                }               
            }


            $host = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // open connection to mysql database
            $connection = mysqli_connect($host, $username, $password, $dbname) or die("Connection Error " . mysqli_error($connection));
            
            $sql = "select * from sma_location where id=".$segment_id."";
            $seg_result = mysqli_query($connection, $sql) or die("Selection Error " . mysqli_error($connection));
            $seg_row = mysqli_fetch_array($seg_result, MYSQLI_ASSOC);
            $projName = explode(" ",$seg_row['proj_name'])[0]."_".$segment_id;
            
            
            if(count($files_list)>0){
                foreach($files_list as $fileName){
                    $zip = new ZipArchive;
                    $csv_date = explode("/",$fileName);
                    $csv_folder = $csv_date[count($csv_date)-2];
                    
                if ($zip->open($fileName) === TRUE) {

                    $csv_path = $directory.$csv_folder."/".$projName."/";
                    $zip->extractTo($csv_path);
                    $zip->close();
                 
                //echo "csv path".CSV_PATH;exit;
                // Count File
                $sql = "select count(*) as count from sma_sync_table";
                $result = mysqli_query($connection, $sql) or die("Selection Error " . mysqli_error($connection));
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                $num_table=$row['count'];
                for ($x = 1; $x <= $num_table; $x++){               
                    
                    $sql = "select * from sma_sync_table where id='$x'";
                    $result = mysqli_query($connection, $sql) or die("Selection Error " . mysqli_error($connection));
                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                    $table = $row['table_name'];
                    // fetch mysql table rows
                    $csv_filename = $table.".csv"; // to be changed
                    //$csv_filename ='sma_segment_2017-02-05.csv';
                    // Name of your CSV file
                    $csv_file = $csv_path . $csv_filename;
                   //  echo $csv_file;exit;  
                    if (($handle = fopen($csv_file, "r")) !== FALSE) {
                        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        $col = array();
                        $num = count($data);
                        for ($c=0; $c < $num; $c++) {
                           $col[$c] = $data[$c];
                        }
                        /*if($table=='sma_tax_rates'){
                            echo "<pre>".$csv_filename;exit;
                            print_r($col);exit;
                        }*/
                         
                        switch ($table) {
                            case "sma_segment":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                if(isset($col[7]) && ($col[7]=='1' && (is_null($col[6])))){
                                
                                    $query = "UPDATE sma_segment SET `proj_name`='".$col5."',`prj_actv`= '".$col6."' WHERE id=".$col1."";
                                }else{
                                    $query = "INSERT ignore INTO sma_segment
                                    (id,org_id,prj_doc_id,parent_proj_id,proj_name,prj_actv) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."')";
                                }
                                break;
                            case "sma_warehouses":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                               if(isset($col[20]) && ($col[20]=='1' && (is_null($col[19])))){
                                
                                    $query = "UPDATE `sma_warehouses` SET `wh_type`='".$col4."',`name`='".$col7."',`wh_onrshp_type`='".$col8."',`wh_strg_type`='".$col9."',`adds_id`='".$col10."',`wh_desc`='".$col11."',`actv`='".$col12."',`segment_id`='".$col17."',`address`='".$col19."' WHERE id='".$col1."'";
                                }else{

                                    $query = "INSERT ignore INTO sma_warehouses
                                    (id,org_id,wh_id,wh_type,usr_id_create,usr_id_create_dt,name,wh_onrshp_type,wh_strg_type,adds_id,wh_desc,actv,inactv_resn,inactv_dt,wh_ent_id,prj_id,segment_id,biller_id,address) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."')";
                                }
                                break;
                             case "sma_users":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];
                                $col22 = $col[21];
                                $col23 = $col[22];
                                $col24 = $col[23];
                                $col25 = $col[24];
                                $col26 = $col[25];
                                $col27 = $col[26];
                                $col28 = $col[27];
                                $col29 = $col[28];
                                $col30 = $col[29];
                                $col31 = $col[30];
                                $col32 = $col[31];
                                $col33 = $col[32];
                                if(isset($col[34]) && ($col[34]=='1' && (is_null($col[33])))){
                               
                                    $query = "UPDATE `sma_users` SET `email`='".$col7."',`active`='".$col14."',`first_name`='".$col15."',`last_name`='".$col16."',`company`='".$col17."',`phone`='".$col18."',`gender`='".$col20."',`warehouse_id`='".$col22."',`segment_id`='".$col23."' WHERE id='".$col1."'";
                                }else{
                                    $query = "INSERT ignore INTO sma_users
                                    (`id`, `last_ip_address`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `avatar`, `gender`, `group_id`, `warehouse_id`, `segment_id`, `biller_id`, `company_id`, `show_cost`, `show_price`, `award_points`, `show_discount`, `upd_flg`, `usr_id`, `pwid`, `org_id`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col24."','".$col25."','".$col26."','".$col27."','".$col28."','".$col29."','".$col30."','".$col31."','".$col32."','".$col33."')";
                                }
                                break;
                            case "sma_products":
                                 $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];
                                $col22 = $col[21];
                                $col23 = $col[22];
                                $col24 = $col[23];
                                $col25 = $col[24];
                                $col26 = $col[25];
                                $col27 = $col[26];
                                $col28 = $col[27];
                                $col29 = $col[28];
                                $col30 = $col[29];
                                $col31 = $col[30];
                                $col32 = $col[31];
                                $col33 = $col[32];
                                $col34 = $col[33];
                                $col35 = $col[34];
                                $col36 = $col[35];
                                $col37 = $col[36];
                                $col38 = $col[37];
                                $col39 = $col[38];
                                $col40 = $col[39];
                                $col41 = $col[40];
                                $col42 = $col[41];
                                $col43 = $col[42];
                                $col44 = $col[43];
                                $col45 = $col[44];
                                $col46 = $col[45];
                                $col47 = $col[46];

                                // && (is_null($col[47]))
                                if(isset($col[48]) && ($col[48]=='1')){
                                  

                                    $query = "UPDATE `sma_products` SET `name`='".$col3."',`unit`='".$col4."',`cost`='".$col5."',`price`='".$col6."',`alert_quantity`='".$col7."',`category_id`='".$col9."',`subcategory_id`='".$col10."',`quantity`='".$col17."',`tax_rate`='".$col18."',`track_quantity`='".$col19."',`details`='".$col20."',`warehouse`='".$col21."',`barcode_symbology`='".$col22."',`product_details`='".$col24."',`tax_method`='".$col25."',`serialized`='".$col38."',`serial_number`='".$col39."',`grp_id`='".$col44."' WHERE id='".$col1."'";

                                }else{
                                    $query = "INSERT ignore INTO sma_products
                                    (`id`, `code`, `name`, `unit`, `cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `barcode_symbology`, `file`, `product_details`, `tax_method`, `type`, `supplier1`, `supplier1price`, `supplier2`, `supplier2price`, `supplier3`, `supplier3price`, `supplier4`, `supplier4price`, `supplier5`, `supplier5price`, `max_discount`, `serialized`, `serial_number`, `lot_no`, `sr_no`, `org_id`, `wh_id`, `grp_id`, `trsfr_flg`, `tupd_flg`, `psale`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col24."','".$col25."','".$col26."','".$col27."','".$col28."','".$col29."','".$col30."','".$col31."','".$col32."','".$col33."','".$col34."','".$col35."','".$col36."','".$col37."','".$col38."','".$col39."','".$col40."','".$col41."','".$col42."','".$col43."','".$col44."','".$col45."','".$col46."','".$col47."')";
                                }
                            break;
                            case "sma_purchase_items":
                                
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];
                                $col22 = $col[21];
                                $col23 = $col[22];
                                $col24 = $col[23];
                                $col25 = $col[24];
                                $col26 = $col[25];
                                $col27 = $col[26];
                                $col28 = $col[27];
                                $col29 = $col[28];
                                $col30 = $col[29];
                                $col31 = $col[30];
                                $col32 = $col[31];
                                $col33 = $col[32];
                                $col34 = $col[33];
                                $col35 = $col[34];
                                $col36 = $col[35];
                                $col37 = $col[36];
                                $col38 = $col[37];
                                $col39 = $col[38];
                                $col40 = $col[39];
                                $col41 = $col[40];
                                if(isset($col[42]) && ($col[42]=='1' && (is_null($col[41])))){
                                
                                    $query = "UPDATE `sma_purchase_items` SET `purchase_id`='".$col2."',`transfer_id`='".$col3."',`category_id`='".$col4."',`product_id`='".$col5."',`product_code`='".$col6."',`product_name`='".$col7."',`itm_desc`='".$col8."',`long_desc`='".$col9."',`option_id`='".$col10."',`net_unit_cost`='".$col11."',`quantity`='".$col12."',`alert_quantity`='".$col13."',`track_quantity`='".$col14."',`type`='".$col15."',`warehouse_id`='".$col16."',`taxable`='".$col18."',`attribute`='".$col19."',`item_tax`='".$col20."',`tax_rate`='".$col21."',`tax_method`='".$col22."',`tax_rate_id`='".$col23."',`tax`='".$col24."',`discount`='".$col25."',`item_discount`='".$col26."',`expiry`='".$col27."',`subtotal`='".$col28."',`quantity_balance`='".$col29."',`is_serialized`='".$col31."',`status`='".$col33."',`unit_cost`='".$col34."',`real_unit_cost`='".$col35."' WHERE warehouse_id='".$col16."' AND product_code='".$col6."'";
                                }else{
                                    $query = "INSERT ignore INTO sma_purchase_items
                                    (`id`, `purchase_id`, `transfer_id`, `category_id`, `product_id`, `product_code`, `product_name`, `itm_desc`, `long_desc`, `option_id`, `net_unit_cost`, `quantity`, `alert_quantity`, `track_quantity`, `type`, `warehouse_id`, `image`, `taxable`, `attribute`, `item_tax`, `tax_rate`, `tax_method`, `tax_rate_id`, `tax`, `discount`, `item_discount`, `expiry`, `subtotal`, `quantity_balance`, `date`, `is_serialized`, `user_id`, `status`, `unit_cost`, `real_unit_cost`, `lot_no`, `sr_no`, `org_id`, `wh_id`, `grp_id`, `psale`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col24."','".$col25."','".$col26."','".$col27."','".$col28."','".$col29."','".$col30."','".$col31."','".$col32."','".$col33."','".$col34."','".$col35."','".$col36."','".$col37."','".$col38."','".$col39."','".$col40."','".$col41."')";
                                }

                            break;
                            
                            case "sma_item_stk_profile":
                            $col1 = $col[0];
                            $col2 = $col[1];
                            $col3 = $col[2];
                            $col4 = $col[3];
                            $col5 = $col[4];
                            $col6 = $col[5];
                            $col7 = $col[6];
                            $col8 = $col[7];
                            $col9 = $col[8];
                            $col10 = $col[9];
                            $col11 = $col[10];
                            $col12 = $col[11];
                            $col13 = $col[12];
                            $col14 = $col[13];
                            $col15 = $col[14];
                            $col16 = $col[15];
                            $col17 = $col[16];
                            $col18 = $col[17];
                            $col19 = $col[18];
                            if(isset($col[20]) && ($col[20]=='1' && (is_null($col[19])))){
                           
                                $query = "UPDATE `sma_item_stk_profile` SET `fy_id`='".$col6."',`item_uom_id`='".$col8."',`available_stk`='".$col10."',`rejected_stk`='".$col11."',`purchase_price`='".$col14."',`sales_price`='".$col15."' WHERE id='".$col1."'";
                            }else{

                            $query = "INSERT ignore INTO sma_item_stk_profile
                            (`id`, `org_id`, `segment_id`, `warehouse_id`, `wh_id`, `fy_id`, `item_id`, `item_uom_id`, `total_stk`, `available_stk`, `rejected_stk`, `rework_stk`, `ordered_stk`, `purchase_price`, `sales_price`, `created_date`, `code`, `create_flg`, `upd_flg`) 
                            VALUES
                            ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."')";
                        }
                        break;
                        case "sma_item_stk_lot":
                            $col1 = $col[0];
                            $col2 = $col[1];
                            $col3 = $col[2];
                            $col4 = $col[3];
                            $col5 = $col[4];
                            $col6 = $col[5];
                            $col7 = $col[6];
                            $col8 = $col[7];
                            $col9 = $col[8];
                            $col10 = $col[9];
                            $col11 = $col[10];
                            $col12 = $col[11];
                            $col13 = $col[12];
                            $col14 = $col[13];
                            $col15 = $col[14];
                            $col16 = $col[15];
                            $col17 = $col[16];
                            $col18 = $col[17];
                            $col19 = $col[18];
                            if(isset($col[20]) && ($col[20]=='1' && (is_null($col[19])))){
                            
                                $query = "UPDATE `sma_item_stk_lot` SET `batch_no`='".$col9."',`total_stk`='".$col11."',`rejected_stk`='".$col12."',`mfg_date`='".$col13."',`exp_date`='".$col14."' WHERE id='".$col1."'";
                            }else{
                                $query = "INSERT ignore INTO sma_item_stk_lot
                                (`id`, `org_id`, `segment_id`, `warehouse_id`, `wh_id`, `lot_no`, `item_stk_id`, `item_id`, `batch_no`, `itm_uom_id`, `total_stk`, `rejected_stk`, `mfg_date`, `exp_date`, `reworkable_stk`, `created_date`, `code`, `create_flg`, `upd_flg`) 
                                VALUES
                                ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."')";
                            }
                        break;
                        case "sma_item_stk_lot_bin":
                            $col1 = $col[0];
                            $col2 = $col[1];
                            $col3 = $col[2];
                            $col4 = $col[3];
                            $col5 = $col[4];
                            $col6 = $col[5];
                            $col7 = $col[6];
                            $col8 = $col[7];
                            $col9 = $col[8];
                            $col10 = $col[9];
                            $col11 = $col[10];
                            $col12 = $col[11];
                            $col13 = $col[12];
                            $col14 = $col[13];
                            $col15 = $col[14];
                            $col16 = $col[15];
                            
                            if(isset($col[17]) && ($col[17]=='1' && (is_null($col[16])))){
                          //  if((isset($col[17])) && ($col[17]==1)){
                                $query = "UPDATE `sma_item_stk_lot_bin` SET `lot_id`='".$col7."',`bin_id`='".$col8."',`itm_uom_id`='".$col9."',`total_stk`='".$col10."',`rejected_stk`='".$col11."' WHERE id='".$col1."'";
                            }else{
                                $query = "INSERT ignore INTO sma_item_stk_lot_bin
                                (`id`, `org_id`, `segment_id`, `warehouse_id`, `wh_id`, `item_id`, `lot_id`, `bin_id`, `itm_uom_id`, `total_stk`, `rejected_stk`, `reworkable_stk`, `created_date`, `code`, `create_flg`, `upd_flg`) 
                                VALUES
                                ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."')";
                            }
                        break;
                        case "sma_tax_rates":                        
                            $col1 = $col[0];
                            $col2 = $col[1];
                            $col3 = $col[2];
                            $col4 = $col[3];
                            $col5 = $col[4];
                            $col6 = $col[5];
                            $col7 = $col[6];
                            $col8 = $col[7];
                            if(isset($col[9]) && ($col[9]=='1' && (is_null($col[8])))){
                            
                                $query = "UPDATE `sma_tax_rates` SET `name`='".$col2."',`code`='".$col3."',`rate`='".$col4."',`type`='".$col5."',`org_id`='".$col6."',`wh_id`='".$col7."',`grp_id`='".$col8."' WHERE id='".$col1."'";
                            }else{
                                $query = "INSERT ignore INTO sma_tax_rates
                                (`id`, `name`, `code`, `rate`, `type`, `org_id`, `wh_id`, `grp_id`) 
                                VALUES
                                ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."')";                        
                            }
                        break;
                            case "sma_emrs":

                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                
                                if(isset($col[19]) && ($col[19]=='1' && (is_null($col[18])))){
                                
                                    $query = "UPDATE `sma_emrs` SET `supplier_id`='".$col9."',`emrs_no`='".$col10."',`emrs_dt`='".$col11."',`emrs_status`='".$col15."',`emrs_status_dt`='".$col16."',`remarks`='".$col17."',`emrs_mode`='".$col18."' WHERE id='".$col1."'";                                                            
                                }else{                               
                                    $query = "INSERT IGNORE INTO sma_emrs
                                    (`id`, `org_id`, `segment_id`, `warehouse_id`, `wh_id`, `fy_id`, `doc_id`, `emrs_type`, `supplier_id`, `emrs_no`, `emrs_dt`, `org_id_req_to`, `wh_id_required_to`, `reqd_dt`, `emrs_status`, `emrs_status_dt`, `remarks`, `emrs_mode`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."')";
                                }     
                               // echo $query;exit;                   
                            break;
                            case "sma_emrs_doc_src":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];

                                if(isset($col[9]) && ($col[9]=='1' && (is_null($col[8])))){
                                
                                    $query = "UPDATE `sma_emrs_doc_src` SET `doc_id`='".$col6."',`doc_no`='".$col7."',`doc_date`='".$col8."' WHERE id='".$col1."'";
                                }else{
                                    $query = "INSERT IGNORE INTO sma_emrs_doc_src
                                    (`id`, `org_id`, `segment_id`, `warehouse_id`, `wh_id`, `doc_id`, `doc_no`, `doc_date`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."')";
                                }
                            
                            break;
                            case "sma_emrs_item":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                if(isset($col[19]) && ($col[19]=='1' && (is_null($col[18])))){
                                
                                    $query = "UPDATE `sma_emrs_item` SET `item_id`='".$col8."',`req_qty`='".$col9."',`bal_qty`='".$col10."',`itm_uom_bs`='".$col11."',`uom_conv_fctr`='".$col12."',`req_qty_bs`='".$col13."',`itm_price_bs`='".$col14."',`itm_amt_bs`='".$col15."',`line_seq_no`='".$col16."',`code`='".$col17."',`status`='".$col18."' WHERE id='".$col1."'";
                                }else{

                                    $query = "INSERT IGNORE INTO sma_emrs_item
                                    (`id`, `org_id`, `segment_id`, `warehouse_id`, `wh_id`,`doc_id`, `doc_no_src_id`, `item_id`, `req_qty`, `bal_qty`, `itm_uom_bs`, `uom_conv_fctr`, `req_qty_bs`, `itm_price_bs`, `itm_amt_bs`, `line_seq_no`, `code`, `status`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."')";
                                }
                               
                                break;
                            case "sma_bin":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                             if(isset($col[16]) && ($col[16]=='1' && (is_null($col[15])))){  
                            
                                $query = "UPDATE `sma_bin` SET `bin_nm`='".$col7."',`bin_desc`='".$col8."',`storage_type`='".$col9."',`blocked`='".$col10."',`blk_resn`='".$col11."',`blk_dt_frm`='".$col12."',`blk_dt_to`='".$col13."',`bin_ent_id`='".$col14."',`create_date`='".$col15."' WHERE id='".$col1."'";
                            }else{
                                $query = "INSERT IGNORE INTO sma_bin
                                    (id,org_id,segment_id,warehouse_id,wh_id,bin_id,bin_nm,bin_desc,storage_type,blocked,blk_resn,blk_dt_frm,blk_dt_to,bin_ent_id,create_date) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."')";
                            }
                            break;
                            case "sma_categories":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                if(isset($col[10]) && ($col[10]=='1' && (is_null($col[9])))){  
                                    $query = "UPDATE `sma_categories` SET `code`='".$col2."',`name`='".$col3."',`parent_id`='".$col4."',`level`='".$col5."',`status`='".$col6."',`grp_id_parent`='".$col7."',`tax_rate_id`='".$col9."' WHERE id='".$col1."'";
                                }else{
                                    $query = "INSERT IGNORE INTO sma_categories
                                    (`id`, `code`, `name`, `parent_id`, `level`, `status`, `grp_id_parent`, `image`, `tax_rate_id`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."')";
                                }
                                break;
                            case "sma_companies":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];
                                $col22 = $col[21];
                                $col23 = $col[22];
                                $col24 = $col[23];
                                $col25 = $col[24];
                                $col26 = $col[25];
                                $col27 = $col[26];
                                $col28 = $col[27];
                                $col29 = $col[28];
                                $col30 = $col[29];
                                $col31 = $col[30];
                                $col32 = $col[31];
                                $col33 = $col[32];
                                $col34 = $col[33];
                                $col35 = $col[34];
                                $col36 = $col[35];
                                $col37 = $col[36];
                                $col38 = $col[37];
                                $col39 = $col[38];
                                $col40 = $col[39];
                                $col41 = $col[40];
                                $col42 = $col[41];
                                $col43 = $col[42];
                                $col44 = $col[43];
                                $col45 = $col[44];
                                $col46 = $col[45];
                                $col47 = $col[46];
                                $col48 = $col[47];
                                $col49 = $col[48];

                                if(isset($col[50]) && ($col[50]=='1' && (is_null($col[49])))){                                   
                                    $query = "UPDATE `sma_companies` SET `name`='".$col6."',`vat_no`='".$col9."',`address`='".$col10."',`phone`='".$col17."',`email`='".$col18."' WHERE id='".$col1."'";
                                }else{
                                    $select = "Select * from sma_companies where eo_id='".$col44."' AND eo_type='".$col43."'";
                                    $select_result = mysqli_query($connection, $select) or die("Selection Error " . mysqli_error($connection)); 
                                    $count = mysqli_num_rows($select_result);

                                   /* $select1 = "Select * from sma_companies where '".$col48."'!='".$segment_id."'";
                                    $select_result1 = mysqli_query($remote_connection, $select1) or die("Selection Error " . mysqli_error($remote_connection)); 
                                    $count1 = mysqli_num_rows($selec;t_result1);*/

                                    $select2 = "Select * from sma_companies where segment_id='".$col48."' AND loyalty_card_id='".$col21."' AND segment_id is not null AND loyalty_card_id!=''";
                                    $select_result2 = mysqli_query($connection, $select2) or die("Selection Error " . mysqli_error($connection)); 
                                    $count2 = mysqli_num_rows($select_result2);

                                 /*   echo $count."---->".$count1."--------->",$count2."<br>";
                                    print_r($select);
                                    echo "-------<br>";
                                    print_r($select1);
                                    echo "-------<br>";
                                    print_r($select2);*/
                                    /*exit; */
                                    if($count==0 && $count2==0){
                                       // echo $count."----".$count1;
                                        $query = "INSERT INTO sma_companies
                                        (`group_id`, `group_name`, `customer_group_id`, `customer_group_name`, `name`, `father_name`, `company`, `vat_no`, `address`, `city`, `state`, `postal_code`, `country`, `country_id`, `state_id`, `phone`, `email`, `id_proof`, `id_proof_no`, `loyalty_card_id`, `nominees1`, `dob_nominees1`, `nominees2`, `dob_nominees2`, `nominees3`, `dob_nominees3`, `nominees4`, `dob_nominees4`, `nominees5`, `dob_nominees5`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `invoice_footer`, `payment_term`, `logo`, `award_points`, `upd_flg`, `eo_type`, `eo_id`, `org_id`, `wh_id`, `biller_id`, `segment_id`, `sync_flg`) 
                                        VALUES
                                        ('".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col24."','".$col25."','".$col26."','".$col27."','".$col28."','".$col29."','".$col30."','".$col31."','".$col32."','".$col33."','".$col34."','".$col35."','".$col36."','".$col37."','".$col38."','".$col39."','".$col40."','".$col41."','".$col42."','".$col43."','".$col44."','".$col45."','".$col46."','".$col47."','".$col48."','".$col49."')";
                                    }

                            }
                            break;
                            case "sma_currencies":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $query = "INSERT ignore INTO sma_currencies
                                (`id`, `code`, `name`, `rate`, `auto_update`, `curr_id`) 
                                VALUES
                                ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."')";
                                
                                break;
                                
                            case "sma_drft_po_dlv_schdl":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];
                                $col22 = $col[21];
                                $col23 = $col[22];
                                $col24 = $col[23];
                                $col25 = $col[24];
                                $col26 = $col[25];
                                $col27 = $col[26];
                                $col28 = $col[27];
                                
                                if((isset($col[30])) && ($col[30]==1)){
                                    $query = "UPDATE `sma_drft_po_dlv_schdl` SET `tot_qty`='".$col6."',`dlv_mode`='".$col7."',`dlv_qty`='".$col8."',`tlrnc_qty_type`='".$col9."',`tlrnc_qty_val`='".$col10."',`dlv_dt`='".$col11."',`dlv_adds_id`='".$col13."',`usr_id_create_dt`='".$col14."',`dlv_schdl_no`='".$col15."',`itm_uom_desc`='".$col16."',`tlrnc_days_val`='".$col19."',`use_rented_wh`='".$col28."' WHERE id='".$col1."'";
                                }else{
                                    $query = "INSERT IGNORE INTO sma_drft_po_dlv_schdl
                                    (`id`, `org_id`, `po_id`, `itm_id`, `itm_name`, `tot_qty`, `dlv_mode`, `dlv_qty`, `tlrnc_qty_type`, `tlrnc_qty_val`, `dlv_dt`, `wh_id`, `dlv_adds_id`, `usr_id_create_dt`, `dlv_schdl_no`, `itm_uom_desc`, `bal_qty`, `tmp_rcpt_qty`, `tlrnc_days_val`, `segment_id`, `warehouse_id`, `prj_id`, `ro_no`, `ro_dt`, `item_id`, `flg`, `status`, `use_rented_wh`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col24."','".$col25."','".$col26."','".$col27."','".$col28."')";
                                }
                            break;              
                            case "sma_drft_po":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];

                                if((isset($col[22])) && ($col[22]==1)){
                                    $query = "UPDATE `sma_drft_po` SET `po_dt`='".$col4."',`customer_id`='".$col5."',`bill_adds_id`='".$col6."',`tlrnc_days`='".$col7."',`curr_id_sp`='".$col8."',`curr_name`='".$col9."',`curr_conv_fctr`='".$col10."',`tlrnc_qty_type`='".$col11."',`tlrnc_qty_val`='".$col12."',`usr_id_create_dt`='".$col13."',`auth_po_no`='".$col14."',`fy_id`='".$col15."',`po_status`='".$col18."',`po_mode`='".$col19."',`doc_id`='".$col20."',`modify_date`='".date('Y-m-d H:i:s')."' WHERE id='".$col1."'";
                                }else{                               
                                    $query = "INSERT IGNORE INTO sma_drft_po
                                    (`id`, `org_id`, `po_id`, `po_dt`, `customer_id`, `bill_adds_id`, `tlrnc_days`, `curr_id_sp`, `curr_name`, `curr_conv_fctr`, `tlrnc_qty_type`, `tlrnc_qty_val`, `usr_id_create_dt`, `auth_po_no`, `fy_id`, `flg`, `status`, `po_status`, `po_mode`, `doc_id`, `modify_date`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."')";
                                }
                            break;
                            default:
                                $query="";
                        }
                        if($query!=''){
                            $result = mysqli_query($connection, $query) or die("Selection Error " . mysqli_error($connection)); 
                        }
                    }
                    echo $$table;
                    fclose($handle);
                }
            }
                $tmpdir = $_SERVER["DOCUMENT_ROOT"].'/'.PROJECT_NAME.'/tmp-folder/';
                $dateWiseTmpDir = $_SERVER["DOCUMENT_ROOT"].'/'.PROJECT_NAME.'/tmp-folder/'.$csv_folder.'/';
                
                if (!is_dir($tmpdir)) { //create the folder if it's not already exists
                    mkdir($tmpdir, 0755, TRUE);                
                }  
                if (!is_dir($dateWiseTmpDir)) {
                    mkdir($dateWiseTmpDir, 0755, TRUE);
                }         
                copy($fileName,$dateWiseTmpDir.$projName.".zip");
                unlink($fileName);
                //unlink($remote_file_url);
               // $this->site->rrmdir($remote_file_url);
                $this->site->rrmdir($fileName);
                $this->site->rrmdir($directory.$csv_folder."/".$projName);
                }
            }
            }

            
            $this->session->set_flashdata('message', "Congratulations!!!! Data Synced Successfully");
            redirect('login');
        }
        $this->page_construct('syncin', $meta, $this->data);        
    }

 function recove_invoices(){
   // for new location segment entry.
     $remote_host = REMOTE_HOST;
     $remote_username = REMOTE_USERNAME;
     $remote_password = REMOTE_PWD;
     $remote_dbname = REMOTE_DB;

        $remote_connection = mysqli_connect($remote_host, $remote_username, $remote_password, $remote_dbname) or die("Connection Error " . mysqli_error($remote_connection)); 

        if($remote_connection === false){
            die("ERROR: Could not connect. " . mysqli_connect_error());
        }

           $sql2= "select * from sma_segment WHERE prj_actv='Y'";
           $segment_data = mysqli_query($remote_connection, $sql2) or die("Selection Error " . mysqli_error($remote_connection));
           //print_r($segment_data); die;
           
    while($cols = mysqli_fetch_all($segment_data,MYSQLI_ASSOC)) {
            
        foreach($cols as $col){
            
           $location =$this->db->select('id')
                    ->from('sma_location')
                    ->where('id',$col['id'])
                    ->get();        
      
            $count = $location->num_rows($location);

                if($count ==0)
                  {
              
                  $query = "INSERT ignore INTO sma_location(id,org_id,prj_doc_id,   parent_proj_id,proj_name,prj_actv) VALUES('".$col['id']."','".$col['org_id']."','".$col['prj_doc_id']."','".$col['parent_proj_id']."','".$col['proj_name']."','".$col['prj_actv']."')"; 
                  $this->db->query($query);
                  }

        } 
     }
     // end new location segment query.
     } 
}
