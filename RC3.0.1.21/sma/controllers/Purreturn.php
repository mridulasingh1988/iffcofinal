<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Purreturn extends MY_Controller
{

    function __construct()
    {

        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
        
        if ($this->Customer || $this->Supplier) {           
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->load->model('purchasereturn_model');
        $this->load->helper('text');
        /*$this->pos_settings = $this->pos_model->getSetting();
        $this->pos_settings->pin_code = $this->pos_settings->pin_code ? md5($this->pos_settings->pin_code) : NULL;
        $this->data['pos_settings'] = $this->pos_settings;
        */

        $this->session->set_userdata('last_activity', now());
        $this->lang->load('pos', $this->Settings->language);
        $this->load->library('form_validation');
    }

    function add($id=NULL)
    { 
       
        $returnData = $this->purchasereturn_model->getPrchseRetData(); 
     
        $this->data['returnType'] = $returnData['return_type'];      
        $this->data['warehouseList'] = $returnData['warehouse_detail'];
        //$this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->page_construct('purreturn/add', $meta, $this->data);
    }

    function getRoDetails(){
        if($this->input->post('po_id')){
            $poId = $this->input->post('po_id');
            //echo "po id".$this->input->post('po_id');
            $roDetail = $this->mrn_model->getRoDetail($poId);
                $response[0] = 'Select Ro No.';                
            foreach ($roDetail as $key => $value) {
                $response[$value->ro_dt."~".$value->id] = $value->ro_no;
            }
            echo json_encode($response);
             exit;
        }
    }
    function getPoDetails(){
        if($this->input->post('wh_id')){
            $whId = $this->input->post('wh_id');
            //echo "po id".$this->input->post('po_id');
            $roDetail = $this->mrn_model->getPoDetail($whId);
                $response[0] = 'Select Ro No.';                
            foreach ($roDetail as $key => $value) {
                $response[$value->ro_dt] = $value->ro_no;
            }
            echo json_encode($response);
             exit;
        }
    }
    function getRoProducts(){
        if($this->input->post('ro_no') && $this->input->post('po_no') && $this->input->post('sup_id')){
            $po_no = $this->input->post('po_no');
            $ro_no = $this->input->post('ro_no');
            $sup_id = $this->input->post('sup_id');
            $products = $this->mrn_model->getRoDetail($po_no,$ro_no);

            echo json_encode($products);
            exit;
        }
    }

    function listing(){
      $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('list_purchases')));
        $meta = array('page_title' => lang('mrn'), 'bc' => $bc);
        

        $this->page_construct('mrn/listing', $meta, $this->data);        
    }


    function getMrn()
    {
       // CASE WHEN sma_drft_po.status = 1 THEN 'Completed' WHEN sma_drft_po.status = 0 THEN 'Pending' ELSE 'Short Close' END
       // $this->sma->checkPermissions('index');
        $this->load->library('datatables');
        $this->datatables
            ->select("sma_mrn_receipt.id as id, rcpt_no, DATE_FORMAT(rcpt_dt,'%M %D, %Y') as date,name,CASE WHEN sma_mrn_receipt.status = 1 THEN 'Stock Updated' WHEN sma_mrn_receipt.status = 0 THEN 'Pending' ELSE '' END AS status ")
            ->from("mrn_receipt")
            ->join("mrn_rcpt_src","mrn_receipt.id=mrn_rcpt_src.mrn_rcpt_id",'inner')
            //->join("drft_po","mrn_rcpt_src.po_id=drft_po.id")
            ->join("companies","mrn_receipt.supplier_id=companies.id",'left')
            ->where('mrn_receipt.segment_id',$_SESSION['segment_id'])                   
            ->group_by("mrn_rcpt_src.mrn_rcpt_id")
           // ->order_by("mrn_receipt.id",DESC)     
            ->add_column("Actions", "<center><a class=\"tip\" title='" . $this->lang->line("View") . "' href='" . site_url('mrn/view/$1') . "' data-toggle='modal' data-target='#myModal' ><i class=\"fa fa-eye\"></i></a> <a class=\"tip edit-icon\" title='" . $this->lang->line("Edit") . "' href='" . site_url('mrn/add/$1') . "' ><i class=\"fa fa-edit\"></i></a>&nbsp;<a class=\"tip delete-icon\" title='" . $this->lang->line("Delete") . "' id='$1' href='javascript:void(0)' ><i class=\"fa fa-trash\"></i></a></center>", "id");
          
        echo $this->datatables->generate();
    }

    function view($id){
        $mrnDetail = $this->mrn_model->getMrnDetails($id);
        $this->data['mrnDetail'] = $mrnDetail;
        $this->load->view($this->theme . 'mrn/view', $this->data);

    }
    function deletedata(){
        if($this->input->post('bin_id'))
            $response = $this->mrn_model->deletebin($this->input->post('bin_id'));
       
        else if($this->input->post('lot_id'))
            $response = $this->mrn_model->deletelot($this->input->post('lot_id'));
        
        echo json_encode(array("response"=>$response));
    }

    function saveBin(){
        if($this->input->post('bin_data')){
            $binData = $this->input->post('bin_data');
            $response = $this->mrn_model->saveBinData($binData);

            echo json_encode(array("response"=>$response));
        }
    }
    function deletemrn(){
        if(($this->input->post('src_id') && $this->input->post('ro_id')) || ($this->input->post('rcpt_id'))){
            $srcData = $this->input->post('src_id');
            $roId = $this->input->post('ro_id');
            $rcptId = $this->input->post('rcpt_id');
            $response = $this->mrn_model->deleteMrnData($srcData, $roId, $rcptId);
           
            echo json_encode(array("response"=>$response));
        }
    } 

    //added by vikas singh for list of PO.
    
    function getPoList() {       
       if($this->input->post('supplier_id')){
            $supplier_id = $this->input->post('supplier_id');
            $arrPoLists = $this->mrn_model->getPoList($supplier_id);
            $response['0~0'] = 'Select Po No.';                           
            foreach ($arrPoLists as $key => $val) {
              //  echo "val".$val->id;
               $response[$val->id."~".$val->auth_po_no] = $val->auth_po_no;
            } 
            
            echo json_encode($response);
            exit;
        }     

    }   

}