<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Mrn extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
		
        if ($this->Customer || $this->Supplier) {			
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->load->model('mrn_model');
        $this->load->helper('text');
        /*$this->pos_settings = $this->pos_model->getSetting();
        $this->pos_settings->pin_code = $this->pos_settings->pin_code ? md5($this->pos_settings->pin_code) : NULL;
        $this->data['pos_settings'] = $this->pos_settings;
        */

        $this->session->set_userdata('last_activity', now());
        $this->lang->load('pos', $this->Settings->language);
        $this->load->library('form_validation');
    }

    function add($id=NULL)
    { 
       // $this->load->model('site');
       // $this->site->checkColumn('lot_no','sma_item_stk_lot_bin');
        $this->load->model('site');       
        $this->site->checkColumn('rcpt_no','sma_mrn_rcpt_item');
        $this->site->checkColumn('ro_no','sma_mrn_rcpt_item');
        $this->site->checkColumn('ro_no','sma_mrn_rcpt_item_lot');
        $this->site->checkColumn('ro_no','sma_mrn_rcpt_lot_bin');
        $this->site->checkColumn('lot_no','sma_mrn_rcpt_lot_bin');
        
        /*$aa = $this->db->query("SELECT * 
        FROM information_schema.COLUMNS 
        WHERE        
        TABLE_NAME = 'sma_mrn_rcpt_src' 
        AND COLUMN_NAME = 'rcpt_no'")->result_id->num_rows;       
       
        if($aa==1){

            $mrn_id = "ALTER TABLE `sma_mrn_rcpt_src` ADD `rcpt_no` VARCHAR(255) NULL AFTER `sync_flg`";
            $this->db->query($mrn_id);
        }

        $aa = $this->db->query("SELECT * 
        FROM information_schema.COLUMNS 
        WHERE           
        TABLE_NAME = 'sma_mrn_rcpt_item' 
        AND COLUMN_NAME = 'ro_no'")->result_id->num_rows;
        if($aa==1){

            $mrn_item_id = "ALTER  TABLE `sma_mrn_rcpt_item` ADD `ro_no` VARCHAR(255) NULL AFTER `pos_upd_flg`";
            $this->db->query($mrn_item_id);
        }


        $aa = $this->db->query("SELECT * 
        FROM information_schema.COLUMNS 
        WHERE           
        TABLE_NAME = 'sma_mrn_rcpt_item' 
        AND COLUMN_NAME = 'rcpt_no'")->result_id->num_rows;
        if($aa==1){
            $mrn_item_id2 = "ALTER TABLE `sma_mrn_rcpt_item` ADD `rcpt_no` VARCHAR(255) NULL AFTER `ro_no`;";
            $this->db->query($mrn_item_id2);
        }


        $aa = $this->db->query("SELECT * 
        FROM information_schema.COLUMNS 
        WHERE           
        TABLE_NAME = 'sma_mrn_rcpt_item_lot' 
        AND COLUMN_NAME = 'ro_no'")->result_id->num_rows;
        if($aa==1){
            $mrn_item_lot = "ALTER  TABLE `sma_mrn_rcpt_item_lot` ADD `ro_no` VARCHAR(255) NULL AFTER `pos_upd_flg`";
            $this->db->query($mrn_item_lot);
        }

        $aa = $this->db->query("SELECT * 
        FROM information_schema.COLUMNS 
        WHERE           
        TABLE_NAME = 'sma_mrn_rcpt_item_lot' 
        AND COLUMN_NAME = 'rcpt_no'")->result_id->num_rows;
        if($aa==1){

            $mrn_item_lot2 = "ALTER  TABLE `sma_mrn_rcpt_item_lot` ADD `rcpt_no` VARCHAR(255) NULL AFTER `ro_no`;";
            $this->db->query($mrn_item_lot2);
        }

        $aa = $this->db->query("SELECT * 
        FROM information_schema.COLUMNS 
        WHERE           
        TABLE_NAME = 'sma_mrn_rcpt_lot_bin' 
        AND COLUMN_NAME = 'ro_no'")->result_id->num_rows;
        if($aa==1){

            $mrn_item_bin = "ALTER  TABLE `sma_mrn_rcpt_lot_bin` ADD `ro_no` VARCHAR(255) NULL AFTER `pos_upd_flg`";
            $this->db->query($mrn_item_bin);
        }

        $aa = $this->db->query("SELECT * 
        FROM information_schema.COLUMNS 
        WHERE           
        TABLE_NAME = 'sma_mrn_rcpt_lot_bin' 
        AND COLUMN_NAME = 'rcpt_no'")->result_id->num_rows;

        if($aa==1){     

            $mrn_item_bin2 = "ALTER TABLE `sma_mrn_rcpt_lot_bin` ADD `rcpt_no` VARCHAR(255) NULL AFTER `ro_no`;";
            $this->db->query($mrn_item_bin2);
        }*/

        //$this->db->query("UPDATE sma_mrn_rcpt_src set")
        $src_seg_id = "ALTER TABLE `sma_mrn_rcpt_src` CHANGE `segment_id` `segment_id` VARCHAR(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL";

            $this->db->query($src_seg_id);
        if($_SESSION['segment_id'])
         {
            $query123 = "UPDATE sma_mrn_rcpt_src SET segment_id ='".$_SESSION['segment_id']."'  WHERE segment_id!='".$_SESSION['segment_id']."'";
            $this->db->query($query123);    
         }

        $rcpt_itm_id = "ALTER TABLE `sma_mrn_rcpt_item` CHANGE `item_uom_id` `item_uom_id` VARCHAR(255) NULL DEFAULT NULL";

        $this->db->query($rcpt_itm_id); 

        $rcpt_lot_id = "ALTER TABLE `sma_mrn_rcpt_item_lot` CHANGE `itm_uom_id` `itm_uom_id` VARCHAR(255) NULL DEFAULT NULL";

        $this->db->query($rcpt_lot_id);  

        $lot_bin_id = "ALTER TABLE `sma_mrn_rcpt_lot_bin` CHANGE `itm_uom_id` `itm_uom_id` VARCHAR(255) NULL DEFAULT NULL";

        $this->db->query($lot_bin_id); 
        
        $poList = $this->mrn_model->getWareHouseDetail($_SESSION['warehouse_id']); 
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('mrn_add')));
        $meta = array('page_title' => lang('mrn'), 'bc' => $bc);
        $wh_id = $this->site->get_warehouse_ids('1');
        $allBin = $this->mrn_model->getAllBin($wh_id[0][id]);

        $this->data['all_bin'] = $allBin;   
        $this->data['warehouse_name'] = $poList['warehouse_name'];
        $this->data['po_list'] = $poList['po_data']; 
        $this->data['currency'] = $poList['po_data'][0]->curr_name; 
        $this->data['conv_factor'] = $poList['po_data'][0]->curr_conv_fctr; 
        //$this->sma->checkPermissions('index');
        $this->data['tp_list'] = $poList['tp_details'];
        $this->data['supplier_details'] = $poList['supplier_details'];
        //echo "<pre>";print_r($poList);exit;
      
        if(!$id){
            if(isset($_POST) && !empty($_POST)){ 
                if(!empty($_POST['po_no']) && !empty($_POST['ro_no'])){ 
                    //$_POST['supplier_id'] = $poList['po_data'][0]->customer_id;
                    $_POST['curr_id'] = $poList['po_data'][0]->curr_id_sp;
                    $data = $this->mrn_model->saveMrnData($_POST,$poList['po_data'][0]);
                  
                    if($data!=0){     
                        if($_POST['submit-type']==1)               
                            $this->mrn_model->setPoFlag($_POST['po_list'],$_POST['submit-type']);                
                        $this->session->set_flashdata('success', "Mrn Successfully created");
                        redirect('mrn/listing');
                    }else{
                        $this->session->set_flashdata('error','Failed to create Mrn. Please try again!');
                    }
                }else{
                     $this->session->set_flashdata('error','Please enter required fields');
                }    
            }
        }

        if($id){

            //echo "asdkjbsa";exit;
            $mrnData = $this->db->select("mrn_rcpt_src.po_id as po_no,mrn_rcpt_src.id as schl_id,mrn_receipt.*,mrn_rcpt_src.*,mrn_rcpt_item.*,mrn_rcpt_item_lot.*,mrn_rcpt_item_lot.lot_no as pro_lot_no,mrn_rcpt_lot_bin.*,products.*,mrn_rcpt_item.id as mrn_itm_id,bin.bin_nm,mrn_rcpt_lot_bin.id as bin_mstr_id,mrn_rcpt_item.item_id as item,mrn_rcpt_src.id as mrn_src_id,mrn_rcpt_item.rej_qty,drft_po.auth_po_no")
            ->from('mrn_receipt')
            ->join('mrn_rcpt_src', 'mrn_receipt.id=mrn_rcpt_src.mrn_rcpt_id', 'inner')
            ->join('drft_po', 'mrn_rcpt_src.po_id=drft_po.id', 'inner')
            //->join('drft_po_dlv_schdl', 'drft_po.id=drft_po_dlv_schdl.po_id', 'inner')
            ->join('mrn_rcpt_item', 'mrn_rcpt_src.id=mrn_rcpt_item.mrn_rcpt_src_id', 'inner')
            ->join('mrn_rcpt_item_lot', 'mrn_rcpt_item.id=mrn_rcpt_item_lot.mrn_rcpt_item_id', 'left')
            ->join('mrn_rcpt_lot_bin', 'mrn_rcpt_item_lot.id=mrn_rcpt_lot_bin.lot_id', 'left')
            ->join('products', 'mrn_rcpt_item.item_id=products.id', 'inner')
            ->join('bin', 'mrn_rcpt_lot_bin.bin_id=bin.id', 'left')
            ->where("mrn_receipt.id", $id)
           // ->where("drft_po.id", $id)         
            ->order_by("mrn_rcpt_src.po_id",ASC)
            ->get()->result();
            /*echo "<pre>";
            print_r($mrnData);exit;*/

            $this->data['data']['sel_sup_id'] = $mrnData[0]->supplier_id;
            $this->data['data']['wh_id'] = $mrnData[0]->wh_id; 
            $this->data['data']['dn_no'] = $mrnData[0]->dn_no;  
            $this->data['data']['dn_dt'] = $mrnData[0]->dn_dt;   
            $this->data['data']['tp_id'] = $mrnData[0]->tp_id;   
            $this->data['data']['tpt_lr_no'] = $mrnData[0]->tpt_lr_no;
            $this->data['data']['tpt_lr_dt'] = $mrnData[0]->tpt_lr_dt;  
            $this->data['data']['vehicle_no'] = $mrnData[0]->vehicle_no;  
            $this->data['data']['addl_amt'] = $mrnData[0]->addl_amt;
            $this->data['data']['remarks'] = $mrnData[0]->remarks;       
            $tmp_po_id = $mrnData[0]->po_id;
            $this->data['data']['po_list'] = $this->mrn_model->getPoList($mrnData[0]->supplier_id);
            /*echo "<pre>";
            print_r($this->data['data']['po_list']);exit;*/
           /* echo "<pre>";
            print_r($this->mrn_model->getPoList($mrnData[0]->supplier_id));
            exit;*/
            $po_list = array();
            if($tmp_po_id){
                  
                foreach($mrnData as $row){

                     array_push($po_list,$row->po_id);
                    if($row->po_id!=$tmp_po_id){
                        if(!in_array($row->ro_no, $data[$row->po_id]['ro_val']))
                            $data[$row->po_id]['ro_val'][] = $row->ro_no;
                        $data[$row->po_id][$row->ro_no][] = $row;
                        $tmp_po_id = $row->po_id;
                       
                    }else{
                        if(!in_array($row->ro_no, $data[$row->po_id]['ro_val']))
                            $data[$row->po_id]['ro_val'][] = $row->ro_no;                
                       // echo $row->po_id;exit;
                        $data[$tmp_po_id][$row->ro_no][] = $row;
                    }            
                }
            }
            /*echo "<pre>";
            print_r(array_values($data));
            exit;*/
            foreach($data as $val){
                foreach($val['ro_val'] as $ro_val){
                    foreach ($val[$ro_val] as $key => $row) {                       
                       // echo "rented".$row->item_id."============".$row->use_rented_wh."<br>=============";
                        if($row->use_rented_wh==1){
                          
                              /* echo $row->po_no.",". $row->ro_no.",". $row->item_id."";
                            echo "===========================================";*/
                            $roList = $this->mrn_model->getDlvData($row->po_no, $row->ro_no, $row->item_id);

                            $billQty = $this->mrn_model->getItemFromMrn($row->item_id,$roList[0]->dlv_qty,$roList[0]->bal_qty,$row->ro_no);
                          

                            $itemVal = $this->mrn_model->getItemFromGp($row->item_id, $roList[0]->dlv_qty, $roList[0]->bal_qty, $row->ro_no, $billQty['receivedQty']);
                         
                            $row->remainQty = $itemVal;
                            $row->maxQty = $itemVal+$row->rcpt_qty;
                           /* echo $itemVal."?".$row->rcpt_qty;
                            echo "==========================";*/
                            /*if($billQty['receivedQty']!=0){
                                if($billQty['remainQty']==0 || $billQty['remainQty']<0){
                                    unset($roList[$key]);
                                }else{
                                    $value->dlv_qty = $billQty['remainQty'];
                                }
                            }*/
                        }
                    }
                    
                }
            }
           
            $this->data['ro_data'] = $data;
             $poStatus = $this->db->from("mrn_receipt")
            ->where("id",$id)->get()->row();
            if($poStatus->status==1){
                $this->session->set_flashdata('error', "Stock already updated");
                redirect('mrn/listing');

            }
            
            $this->data['mrn_id'] = $id;
            if(isset($_POST) && !empty($_POST)){
                /*echo "<pre>";
                print_r($_POST);
                exit;*/

                if(!empty($_POST['po_no']) && !empty($_POST['ro_no'])){
                    $data = $this->mrn_model->saveMrnData($_POST,$poList['po_data'][0],$id);
                    if($data!=0){
                        if($_POST['submit-type']==1){                    
                            $this->mrn_model->setPoFlag($_POST['po_list'],$_POST['submit-type']);
                        }                
                    }
                 }
                 if((!empty($_POST['mrn_list'])) && (isset($_POST['mrn_list']))){ 
                   
                    $res = $this->mrn_model->editMrnData($id,$_POST);

                    if($res==2){
                        
                        if($_POST['submit-type']==1){

                            $this->mrn_model->setPoFlag($po_list,$_POST['submit-type'],$id);  
                        }
                    }
                }
               if(($res==2) || ($data!=0)){                                  
                    $this->session->set_flashdata('success', "Mrn Successfully created");
                    redirect('mrn/listing');
                }else{
                    $this->session->set_flashdata('error','Failed to create Mrn. Please try again!');
                }
                redirect('mrn/listing');
            }
         }

        //$this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->page_construct('mrn/add_mrn', $meta, $this->data);
    }

    function getRoDetails(){
        if($this->input->post('po_id')){
            $poId = $this->input->post('po_id');
            //echo "po id".$this->input->post('po_id');
            $roDetail = $this->mrn_model->getRoDetail($poId);
            $response[0] = 'Select Ro No.';                
            foreach ($roDetail as $key => $value) {
                $response[$value->ro_dt."~".$value->id] = $value->ro_no;
            }
            echo json_encode($response);
            exit;
        }
    }
    function getPoDetails(){
        if($this->input->post('wh_id')){
            $whId = $this->input->post('wh_id');
            //echo "po id".$this->input->post('po_id');
            $roDetail = $this->mrn_model->getPoDetail($whId);
                $response[0] = 'Select Ro No.';                
            foreach ($roDetail as $key => $value) {
                $response[$value->ro_dt] = $value->ro_no;
            }
            echo json_encode($response);
             exit;
        }
    }
    function getRoProducts(){
        if($this->input->post('ro_no') && $this->input->post('po_no') && $this->input->post('sup_id')){
            $po_no = $this->input->post('po_no');
            $ro_no = $this->input->post('ro_no');
            $sup_id = $this->input->post('sup_id');
            $products = $this->mrn_model->getRoDetail($po_no,$ro_no);

            echo json_encode($products);
            exit;
        }
    }

    function listing(){
      $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('mrn_list')));
        $meta = array('page_title' => lang('mrn'), 'bc' => $bc);
        

        $this->page_construct('mrn/listing', $meta, $this->data);        
    }


    function getMrn()
    {
       // CASE WHEN sma_drft_po.status = 1 THEN 'Completed' WHEN sma_drft_po.status = 0 THEN 'Pending' ELSE 'Short Close' END
       // $this->sma->checkPermissions('index');
        $this->load->library('datatables');
        $this->datatables
            ->select("sma_mrn_receipt.id as id, sma_mrn_receipt.rcpt_no, DATE_FORMAT(rcpt_dt,'%M %D, %Y') as date,name,CASE WHEN sma_mrn_receipt.status = 1 THEN 'Stock Updated' WHEN sma_mrn_receipt.status = 0 THEN 'Pending' ELSE '' END AS status ")
            ->from("mrn_receipt")
            ->join("mrn_rcpt_src","mrn_receipt.id=mrn_rcpt_src.mrn_rcpt_id",'inner')
            ->join("mrn_rcpt_item","mrn_rcpt_src.id=mrn_rcpt_item.mrn_rcpt_src_id")
            ->join("companies","mrn_receipt.supplier_id=companies.eo_id",'left')
            ->where('mrn_receipt.segment_id',$_SESSION['segment_id'])  
            //->order("mrn_receipt.id",DESC)                   
            ->group_by("mrn_rcpt_src.mrn_rcpt_id")
               
            ->add_column("Actions", "<cesnter><a class=\"tip\" title='" . $this->lang->line("View") . "' href='" . site_url('mrn/view/$1') . "' data-toggle='modal' data-target='#myModal' ><i class=\"fa fa-eye\"></i></a> <a class=\"tip edit-icon\" title='" . $this->lang->line("Edit") . "' href='" . site_url('mrn/add/$1') . "' ><i class=\"fa fa-edit\"></i></a>&nbsp;<a class=\"tip delete-icon\" title='" . $this->lang->line("Delete") . "' id='$1' href='javascript:void(0)' ><i class=\"fa fa-trash\"></i></a></center>", "id");
          
        echo $this->datatables->generate();
    }

    function view($id){
        $mrnDetail = $this->mrn_model->getMrnDetails($id);

        $this->data['mrnDetail'] = $mrnDetail;
        $this->load->view($this->theme . 'mrn/view', $this->data);

    }
    function deletedata(){
        if($this->input->post('bin_id'))
            $response = $this->mrn_model->deletebin($this->input->post('bin_id'));
       
        else if($this->input->post('lot_id'))
            $response = $this->mrn_model->deletelot($this->input->post('lot_id'));
        
        echo json_encode(array("response"=>$response));
    }

    function saveBin(){
        if($this->input->post('bin_data')){
            $binData = $this->input->post('bin_data');
            $response = $this->mrn_model->saveBinData($binData);

            echo json_encode(array("response"=>$response));
        }
    }
    function deletemrn(){
        if(($this->input->post('src_id') && $this->input->post('ro_id')) || ($this->input->post('rcpt_id'))){
            $srcData = $this->input->post('src_id');
            $roId = $this->input->post('ro_id');
            $rcptId = $this->input->post('rcpt_id');
            $response = $this->mrn_model->deleteMrnData($srcData, $roId, $rcptId);
           
            echo json_encode(array("response"=>$response));
        }
    } 

    //added by vikas singh for list of PO.
    
    function getPoList() {       
       if($this->input->post('supplier_id')){
            $supplier_id = $this->input->post('supplier_id');           
            $arrPoLists = $this->mrn_model->getPoList($supplier_id);
            $response['0~0'] = 'Select Po No.';                           
            foreach ($arrPoLists as $key => $val) {
              //  echo "val".$val->id;
               $response[$val->id."~".$val->auth_po_no] = $val->auth_po_no;
            } 
            
            echo json_encode($response);
            exit;
        }     

    }   

}

