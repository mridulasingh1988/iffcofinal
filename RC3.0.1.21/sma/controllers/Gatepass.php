<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Gatepass extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
		
        if ($this->Customer || $this->Supplier) {			
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->load->model('gatepass_model');
        $this->load->helper('text');
        /*$this->pos_settings = $this->pos_model->getSetting();
        $this->pos_settings->pin_code = $this->pos_settings->pin_code ? md5($this->pos_settings->pin_code) : NULL;
        $this->data['pos_settings'] = $this->pos_settings;
        */

        $this->load->model('site');
        $this->session->set_userdata('last_activity', now());
        $this->lang->load('pos', $this->Settings->language);
        $this->load->library('form_validation');
        
    }

    function in($gpId=null)
    {
        $wh_id = $this->site->get_warehouse_ids('2');
        
        $poList = $this->gatepass_model->getWareHouseDetail($gpData, $wh_id[0][id]);

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('gatepass_in')));
        $meta = array('page_title' => lang('gatepass'), 'bc' => $bc);

        $allBin = $this->gatepass_model->getAllBin($wh_id[0][id]);
        $this->data['gatepass_type'] = $poList['gatepass_type']; 
        $this->data['all_bin'] = $allBin; 
        $this->data['warehouse_name'] = $poList['warehouse_name'];
        $this->data['tp_list'] = $poList['tp_details'];
        $this->data['supplier_details'] = $poList['supplier_details'];
        //$this->data['sto_data'] = $poList['sto_data'];

        
        $this->data['gp'] = $this->gatepass_model->getGPID($gpId);

        if($gpId){
            if($this->uri->segment(3) != null){
                $gpId = $this->uri->segment(3);
            }
            else{
                $gpId = '';
            }
            $gpData = $this->data['gp'];
            

            $q = $this->db->select("gp.*, gp_src.*, gp_item.*, gp_item.id as gpItemId,  gp_item.pending_qty as p_qty, purchase_items.product_name, purchase_items.product_code, emrs_item.id as emrs_id")
            ->from('gp_item')
            ->join("gp_src","gp_src.id = gp_item.gp_src_id AND gp_src.warehouse_id = gp_item.warehouse_id", "inner")
            ->join("gp","gp.id=gp_src.gp_id  AND gp.warehouse_id = gp_src.warehouse_id", "inner")
            ->join("emrs_item","gp_src.doc_no=emrs_item.doc_no_src_id  AND gp_item.item_id = emrs_item.item_id", "inner")
            ->join("purchase_items","gp_item.item_id = purchase_items.product_id", "inner")
            ->where("gp.id",$gpId)
            ->group_by("gp_item.item_id")
            //->group_by("gp.id")
            ->get()->result();
           
            $data = array();
            $lot_data = array();
            $bin_data = array();
            
            $tmp_id = $q[0]->id;
            $this->data['data']['warehouse_id'] = $q[0]->warehouse_id;
            $this->data['data']['gp_type'] = $q[0]->gp_type;
            $this->data['data']['supplier_id'] = $q[0]->supplier_id;
            $this->data['data']['tp_id'] = $q[0]->tp_id;
            $this->data['data']['tpt_lr_no'] = $q[0]->tp_lr_no;
            $this->data['data']['tpt_lr_dt'] = $q[0]->tp_lr_dt;
            $this->data['data']['vehicle_no'] = $q[0]->vehicle_no;
            $this->data['data']['addl_amt'] = $q[0]->addl_amt;
            $this->data['data']['remarks'] = $q[0]->remarks;

            foreach($q as $row){
                
                if($row->id!=$tmp_id){

                    if(!in_array($row->doc_no, $data[$row->doc_no]))
                        $data[$row->doc_no]['sto_no'] = $row->doc_no;
                        $data[$row->doc_no]['sto_date'] = $row->doc_dt;
                        $data[$row->doc_no]['sto_data'][] = $row;
                        
                        //lot table subquery
                        $lotq = $this->db->select('lot_no, id, item_id, lot_qty, mrn_gp_item_id as gpItemId')
                        ->from('gp_item_lot')
                        ->where('item_id',$row->item_id)
                        ->where('mrn_gp_item_id',$row->id)
                        ->get()->result();

                        $lot_data[$row->gpItemId]['lot_data'] = $lotq;

                        foreach($lotq as $key=>$val){
                            
                            //bin table subquery
                            $this->db->select('gp_lot_bin.lot_id, gp_lot_bin.id, gp_lot_bin.item_id, gp_lot_bin.bin_id, gp_lot_bin.bin_qty, bin.bin_nm, gp_item_lot.lot_no')
                            ->from('gp_lot_bin')
                            ->join('bin','bin.id = gp_lot_bin.bin_id')
                            ->join('gp_item_lot','gp_lot_bin.lot_id = gp_item_lot.id')
                            ->where('gp_lot_bin.item_id',$val->item_id)
                            ->where('gp_lot_bin.lot_id',$val->id);
                            $binq = $this->db->get()->result();

                            $bin_data[$val->id]['bin_data'][] = $binq;
                        }
                        $tmp_id = $row->id;
                }
                else
                {
                    if(!in_array($row->doc_no, $data[$row->doc_no]));
                        $data[$row->doc_no]['sto_no'] = $row->doc_no;
                        $data[$row->doc_no]['sto_date'] = $row->doc_dt;
                        $data[$row->doc_no]['sto_data'][] = $row;

                        $lotq = $this->db->select('lot_no, id, item_id, lot_qty, mrn_gp_item_id as gpItemId')
                        ->from('gp_item_lot')
                        ->where('item_id',$row->item_id)
                        ->where('mrn_gp_item_id',$row->id)
                        ->get()->result();

                        $lot_data[$row->gpItemId]['lot_data'] = $lotq;

                        foreach($lotq as $key=>$val){
                            
                            //bin table subquery
                            $this->db->select('gp_lot_bin.lot_id, gp_lot_bin.id, gp_lot_bin.item_id, gp_lot_bin.bin_id, gp_lot_bin.bin_qty, bin.bin_nm, gp_item_lot.lot_no')
                            ->from('gp_lot_bin')
                            ->join('bin','bin.id = gp_lot_bin.bin_id')
                            ->join('gp_item_lot','gp_lot_bin.lot_id = gp_item_lot.id')
                            ->where('gp_lot_bin.item_id',$val->item_id)
                            ->where('gp_lot_bin.lot_id',$val->id);
                            $binq = $this->db->get()->result();

                            $bin_data[$val->id]['bin_data'][] = $binq;
                        }
                }
            }

            $this->data['lot_list'] = $lot_data;
            $this->data['bin_list'] = $bin_data;
            //echo "<pre>";print_r($lot_data);//exit;
            //echo "<pre>";print_r($data);exit;
            $this->data['sto_list_data'] = $data;

            $gpStatus = $this->db->from("gp")
            ->where("id",$gpId)->get()->row();
            if($gpStatus->status==1){
                $this->session->set_flashdata('error', "Invoice already updated");
                redirect('gatepass/listing');
            }

            $this->data['gpItemId'] = $gpId;
            //echo "<pre>";print_r($_POST);exit;
            if(isset($_POST) && !empty($_POST)){

                if(isset($_POST['sto_list']) && !empty($_POST['sto_id'])){
                    //echo "In add";exit;
                    $data = $this->gatepass_model->saveGatepassData($_POST,$gpId);
                    if($data!=0){
                        if($_POST['submit-type']==1){                    
                            $this->gatepass_model->setPoFlag($_POST['submit-type']);
                        }                
                    }
                 }
                 if((!empty($_POST['gp_item_list'])) && (isset($_POST['gp_item_list']))){

                 //echo "In update"; exit;
                    $res = $this->gatepass_model->updateGatepassData($_POST,$gpId);
                    //echo $res;exit;
                    if($res==2){
                        //echo "in form-control";
                        if($_POST['submit-type']==1)
                        $this->gatepass_model->setPoFlag($_POST['submit-type'],$gpId);  
                    }
                }
                if(($res==2) || ($data!=0)){  
                //echo "Success -------- ".$data;exit;                                
                    $this->session->set_flashdata('success', "Gatepass Successfully created");
                    redirect('gatepass/listing');
                }else{
                    $this->session->set_flashdata('error','Failed to create Mrn. Please try again!');
                }
                //echo "Failed!!!!";exit;
                redirect('gatepass/listing');
            }
            //echo "<pre>";print_r($this->data['lot_list']);exit;
        }
        if(!$gpId){

            if(isset($_POST) && !empty($_POST)){ 
                if(!empty($_POST['sto_no']) && !empty($_POST['sto_date'])){
                    $data = $this->gatepass_model->savegatepassData($_POST);
                    if($data==2){
                        //$this->gatepass_model->setPoFlag($poList);
                        $this->session->set_flashdata('message', "Gatepass Successfully created");
                        redirect('gatepass/listing');
                    }else{
                        $this->session->set_flashdata('error','Failed to create gatepass. Please try again!');
                    }
                }
                else{
                    $this->session->set_flashdata('error','Please enter required fields');
                }
            } 
        }
        $this->page_construct('gatepass/gatepass_in', $meta, $this->data);
    }

    public function deletesto(){
        if($this->input->post('sto_no') && $this->input->post('item_id') && $this->input->post('gp_id')){
            $stoNo = $this->input->post('sto_no');
            $itemId = $this->input->post('item_id');
            $gpId = $this->input->post('gp_id');
            $response = $this->gatepass_model->deleteGpData($sto_no, $itemId, $gpId);
            
            echo json_encode(array("response"=>$response));
        }
    }

    
    public function getGPType($type_id=0, $wh_name=0, $sup_name=0){
        if($type_id == '' && $wh_name == ''){
            $type_id = $_GET['gp_type_id'];
            $wh_name = $_GET['wh_name'];
            $sup_name = $_GET['sup_name'];
        }

       /* $this->db->select('emrs_doc_src.*, emrs_item.doc_no_src_id, emrs_item.bal_qty');
        $this->db->from('emrs')
        ->join('emrs_doc_src', 'emrs.doc_id = emrs_doc_src.doc_id', 'inner')
        ->join('emrs_item', 'emrs_doc_src.doc_no = emrs_item.doc_no_src_id', 'inner')
        ->where('emrs_item.bal_qty >', 0)
        ->where('emrs.emrs_type', $type_id)
        ->where('emrs.warehouse_id', $wh_name)
        ->where('emrs_doc_src.warehouse_id', $wh_name)
        ->where('emrs.supplier_id', $sup_name)
        ->where('emrs.emrs_status',1079);
        $this->db->group_by('id');

        $sto_detail = $this->db->get()->result();*/
        $w = "SELECT `sma_emrs_doc_src` . * , `sma_emrs_item`.`doc_no_src_id` , `sma_emrs_item`.`bal_qty`
        FROM `sma_emrs` INNER JOIN `sma_emrs_doc_src` ON `sma_emrs`.`doc_id` = `sma_emrs_doc_src`.`doc_id`
        INNER JOIN `sma_emrs_item` ON `sma_emrs_doc_src`.`doc_no` = `sma_emrs_item`.`doc_no_src_id`
        AND `sma_emrs_doc_src`.`doc_id` = `sma_emrs_item`.`doc_id` WHERE `sma_emrs_item`.`bal_qty` >0
        AND `sma_emrs`.`emrs_type` = '".$type_id."' AND `sma_emrs`.`warehouse_id` = '".$wh_name."' AND `sma_emrs_doc_src`.`warehouse_id` = '".$wh_name."'
        AND `sma_emrs`.`supplier_id` = '".$sup_name."' AND `sma_emrs`.`emrs_status` =1079 GROUP BY `id` ";
    $req = $this->db->query($w);
    $sto_detail = $req->result();
        if(count($sto_detail)>0){
            foreach($sto_detail as $row){              
               $result[] = $row;          
            }
            echo json_encode($result);
        }
        else{
            echo 0;
        }
        
    }
// added by vikas singh for getpass out 
    
        public function getGPTypeOut($type_id=0, $wh_name=0, $sup_name=0){
        if($type_id == '' && $wh_name == ''){
            $type_id = $_GET['gp_type_id'];
            $wh_name = $_GET['wh_name'];
            $sup_name = $_GET['sup_name'];
        }
   // echo $type_id; die;
        $this->db->select('emrs_doc_src.*, emrs_item.doc_no_src_id, emrs_item.bal_qty');
        $this->db->from('emrs')
        ->join('emrs_doc_src', 'emrs.doc_id = emrs_doc_src.doc_id', 'inner')
        ->join('emrs_item', 'emrs_doc_src.doc_no = emrs_item.doc_no_src_id', 'inner')
        ->where('emrs_item.bal_qty >', 0)
        ->where('emrs.emrs_type', $type_id)
        ->where('emrs.warehouse_id', $wh_name)
        ->where('emrs_doc_src.warehouse_id', $wh_name)
        ->where('emrs.supplier_id', $sup_name)
        ->where('emrs.emrs_status',1079);
        $this->db->group_by('id');

        $sto_detail = $this->db->get()->result();
        //print_r($sto_detail); die;
       $HTML = "<option value='' selected='selected'></option>";
        foreach ($sto_detail as $value) {

                   $HTML .= "<option value='".$value->doc_id."-".$value->doc_no."' >".$value->doc_no."</option>";
              }
             echo json_encode($HTML);
    }
    
    
    
    function getBinProductsById(){
        if($this->input->post('bin_id')){
            $bin_id = $this->input->post('bin_id');
            $response = $this->gatepass_model->deleteBinData($bin_id);
            echo json_encode(array("response"=>$response));
        }
    }

    function getLotProductsById(){
        if($this->input->post('lot_id')){
            $lot_id = $this->input->post('lot_id');
            $response = $this->gatepass_model->deleteLotData($lot_id);
            echo json_encode(array("response"=>$response));
        }
    }


    function getStoProductsList(){
        if($this->input->post('sto_no') && $this->input->post('sto_date')){
            $sto_no1 = $this->input->post('sto_no');
           // print_r($sto_no); die;
            $a = explode('-',$sto_no1);
            $sto_no = $a[0];
            $sto_text = $this->input->post('sto_text');
            $sto_date = $this->input->post('sto_date');
            $products = $this->gatepass_model->getStoProductsList($sto_no,$sto_date,$sto_text);
            echo json_encode($products);            
        }
    }


    // add By Ankit

    // AK
    function out()
    {  
        $a = $this->site->get_warehouse_ids(2);
        //print_r($a); echo ;die;
        $poList = $this->gatepass_model->getWareHouseDetail_out($a[0]['id']);    
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('gatepass_out')));
        $meta = array('page_title' => lang('gatepass'), 'bc' => $bc);
        //$allBin = $this->gatepass_model->getAllBin($a[0]['id']);
        $this->data['gatepass_type'] = $this->gatepass_model->getGatepasstype(); 
        $this->data['all_bin'] = $allBin;   
        $this->data['warehouse_name'] = $poList['warehouse_name']->name;
        $this->data['po_list'] = $poList['po_data']; 
        $this->data['tp_list'] = $poList['tp_details'];
        $this->data['supplier_details'] = $poList['supplier_details'];
        $this->data['sto_no'] = $poList['sto_data']; 
        //echo "<pre>";print_r($poList); die;
        //print_r($this->data); die;
       if(isset($_POST) && !empty($_POST)){
           // print_r($_POST); die;
            if(!empty($_POST['sto_no']) && !empty($_POST['sto_date'])){
               // $data = $this->gatepass_model->savegatepassOutdata($_POST,$poList['sto_data'][0]);  
                $data = $this->gatepass_model->save_gatePassOut($_POST,$poList['sto_data'][0]);        
                // print_r($_POST); die;
                if($data>0){
                        $this->session->set_flashdata('message', "Gatepass Successfully created");
                        redirect('gatepass/listing');
                }else{
                    $this->session->set_flashdata('error','Failed to create gatepass. Please try again!');
                }
            }else{

                 $this->session->set_flashdata('error','Please enter required fields');
            }    

        }          
              $this->page_construct('gatepass/gatepass_out', $meta, $this->data);
    }

    // Add By Ankit
    // AK
    function outhold()
    {
        if(isset($_POST) && !empty($_POST)){
            // print_r($_POST); die;
             if(!empty($_POST['sto_no_edit']) && !empty($_POST['sto_date_edit'])){
                  //$data = $this->gatepass_model->hold_outgatepass($_POST); 
                  $data = $this->gatepass_model->save_gatePassOutHold($_POST);               
                    // print_r($_POST); die;
                if($data>0){
                    $this->session->set_flashdata('message', "Gatepass Successfully created");
                        redirect('gatepass/listing');
                    
                }else{
                    $this->session->set_flashdata('error','Failed to create gatepass. Please try again!');
                    redirect('gatepass/out');
                }
            }else{

                 $this->session->set_flashdata('error','Please enter required fields');
                 redirect('gatepass/out');
            }    

        } 
        return false;

    }

    function get_invoice_list()
    {
        
        if (isset($_POST) && isset($_POST['gp_type_id'])) {
            // echo $_POST['gp_type_id']; die;
             $gp_type_id = $_POST['gp_type_id'];

            if($gp_type_id > 0){
             
            if($gp_type_id =='1080' || $gp_type_id =='1081')
            {
                $type = 2;
            }
            else if($gp_type_id =='1083' || $gp_type_id =='1084'){
            
                $type = 3;
            }
            else if($gp_type_id =='1085' || $gp_type_id =='1086'){
            
                $type = 4;
            }
        }
            $a = $this->site->get_warehouse_ids($type);
            $pr = $this->gatepass_model->getTypeList($a[0]['id'],$gp_type_id);
             // $HTML ="";
              //if (!isset($_POST['gpid'])) {
                   $HTML = "<option value=''>Select STO/RO No.</option>";
             //  }
            // $new =array();
                  foreach ($pr as $value) {

                         $HTML .= "<option value='".$value->id."'>".$value->doc_no."</option>";
                    }
                 echo $HTML;
                     //exit;

        } 

    }
    
    function getStoDetails(){
      
        if($this->input->post('sto_id')){
            $stoId = $this->input->post('sto_id');
            //echo "stoid=>".$stoId."<br>po id=> ".$this->input->post('po_id'); die;
            $stoDetail = $this->gatepass_model->getStoDetail($stoId);
      
            echo json_encode($stoDetail);
             exit;
        }
    }
    
    function getStoProducts(){
        if($this->input->post('sto_no') && $this->input->post('sto_date')){
            $sto_no1 = $this->input->post('sto_no');
           // print_r($sto_no); die;
            $a = explode('-',$sto_no1);
            $sto_no = $a[0];
            $sto_text = $a[1];
            $sto_date = $this->input->post('sto_date');
            $itemid = $this->input->post('itemid');
            $products = $this->gatepass_model->getStoProducts($sto_no,$sto_date,$sto_text);
            //print_r($products); die;
            echo json_encode($products);
            exit;
        }
    }
    // Add By Ankit
    function getStoProducts_Edit(){
        $products = array();
        if($this->input->post('sto_no') && $this->input->post('sto_date')){
            $sto_no = $this->input->post('sto_no');
            $sto_date = $this->input->post('sto_date');
            $sto_no_edit = $this->input->post('sto_no_edit');
            $itemid = $this->input->post('itemid');
            //print_r($itemid); die;
            if($sto_no_edit == $sto_no)
            {
                 $products = $this->gatepass_model->getStoProductsEdit($sto_no,$sto_date,$itemid);

            }
            else {
                $products = $this->gatepass_model->getStoProducts($sto_no,$sto_date);
            }
           
            //print_r($products); die;
            echo json_encode($products);
            exit;
        }
    }
  // Ankit-Hm update function
    function listing(){
      $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('Gatepass View')));
        $meta = array('page_title' => lang('gatepass'), 'bc' => $bc);
        

        $this->page_construct('gatepass/list', $meta, $this->data);        
    }

    function stockout($id = NULL,$qty = NULL){

        $products = array();
        if ($this->input->get('id') && $this->input->get('qty')) {
            $this->data['id'] = $this->input->get('id');
            $this->data['qty'] = $this->input->get('qty');
           $this->data['products'] = $this->gatepass_model->getproductbyid($this->input->get('id'));
            $this->data['sto_no'] = $this->input->get('sto_no');
            $this->data['stock'] = $this->input->get('stock');
            //$products = $this->gatepass_model->getproductbyid($this->input->get('id'));
            
        }
        // if(isset($_POST) && !empty($_POST)){
        //     print_r($POST);
        // }
         //print_r($products); die;
         $this->data['modal_js'] = $this->site->modal_js();

         $this->load->view($this->theme . 'gatepass/stockout', $this->data);
        
    }

    // ---------------Ankit-HM Start-------------
    function getgatepass()
    {
       // $this->sma->checkPermissions('index');
        $this->load->library('datatables');
        $this->datatables
            ->select($this->db->dbprefix('gp') . ".id as id, cust_name,  
                CASE
                WHEN ".$this->db->dbprefix('gatepass_types') . ".name like 'sales in' THEN 'Sales-In' 
                WHEN ".$this->db->dbprefix('gatepass_types') . ".name like 'sales out' THEN 'Sales-Out' 
                WHEN ".$this->db->dbprefix('gatepass_types') . ".name like 'External Material Out' THEN 'Stock-Out'
                WHEN ".$this->db->dbprefix('gatepass_types') . ".name like 'External Material In' THEN 'Stock-In'
                WHEN ".$this->db->dbprefix('gatepass_types') . ".name like 'exhibition in' THEN 'Exhibition-In'
                WHEN ".$this->db->dbprefix('gatepass_types') . ".name like 'exhibition out' THEN 'Exhibition-Out'
                ELSE ".$this->db->dbprefix('gatepass_types') . ".name END AS gp_type, 
                DATE_FORMAT(doc_dt,'%D %M, %Y'), DATE_FORMAT(gp_date,'%D %M, %Y'), companies.name")
            ->from("gp")
            ->join("gp_src","gp.id=gp_src.gp_id",'inner')
            ->join("companies","gp.supplier_id=companies.eo_id",'inner')
            ->join("gatepass_types","gp.gp_type=gatepass_types.type_id",'inner')
            ->where("gp.status", '1')
            //->where("gp.gp_type", 'gatepass_types.type_id')
            ->group_by("gp.id");
         $this->datatables->add_column("Actions", "<center><a class=\"tip\" title='" . $this->lang->line("view_gp") . "' href='" . site_url('gatepass/view/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-eye\"></i></a>  </center>", "id");
         echo $this->datatables->generate();
    }
    // Add New Function By Ankit
    function getStoProductsHold(){
        //echo "11111111"; die;
        if($this->input->post('sto_no') && $this->input->post('sto_date') && $this->input->post('gpid')){
            $sto_no = $this->input->post('sto_no');
            $sto_date = $this->input->post('sto_date');
            $gpid = $this->input->post('gpid'); 
            $products = $this->gatepass_model->getStoProductsHold($sto_no,$sto_date,$gpid);
            //print_r($products); //die;
            echo json_encode($products);
            exit;
        }
    }
    
 // Add New Function By Ankit
 function getgatepasshold_in()
    {
       // $this->sma->checkPermissions('index');
        $this->load->library('datatables');
        $this->datatables
            ->select($this->db->dbprefix('gp') . ".id as id, cust_name,  
                CASE
                WHEN ".$this->db->dbprefix('gatepass_types') . ".name like 'sales in' THEN 'Sales-In' 
                WHEN ".$this->db->dbprefix('gatepass_types') . ".name like 'sales out' THEN 'Sales-Out' 
                WHEN ".$this->db->dbprefix('gatepass_types') . ".name like 'External Material Out' THEN 'Stock-Out'
                WHEN ".$this->db->dbprefix('gatepass_types') . ".name like 'External Material In' THEN 'Stock-In'
                WHEN ".$this->db->dbprefix('gatepass_types') . ".name like 'exhibition in' THEN 'Exhibition-In'
                WHEN ".$this->db->dbprefix('gatepass_types') . ".name like 'exhibition out' THEN 'Exhibition-Out'
                ELSE ".$this->db->dbprefix('gatepass_types') . ".name END AS gp_type, 
                DATE_FORMAT(doc_dt,'%D %M, %Y'), DATE_FORMAT(gp_date,'%D %M, %Y'), companies.name")
            ->from("gp")
            ->join("gp_src","gp.id=gp_src.gp_id",'inner')
            ->join("companies","gp.supplier_id=companies.eo_id",'inner')
            ->join("gatepass_types","gp.gp_type=gatepass_types.type_id",'inner')
            ->where("gp.status", '0')
            ->group_by("gp.id");
          $this->datatables->add_column("Actions", "<center><a class=\"tip\" title='" . $this->lang->line("view_gp") . "' href='" . site_url('gatepass/view/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-eye\"></i></a> &nbsp; <a class=\"tip\" title='" . $this->lang->line("edit") . "' href='" . site_url('gatepass/in/$1') . "'><i class=\"fa fa-edit\"></i></a> </center>", "id");
         echo $this->datatables->generate();
    } 

// Add New Function By Ankit
 function getgatepass_order()
    {
       // $this->sma->checkPermissions('index');
        $this->load->library('datatables');
        $this->datatables
            ->select("sma_emrs.id as id, sma_emrs.emrs_no, DATE_FORMAT(sma_emrs.emrs_dt,'%M %D, %Y'),CASE WHEN sma_emrs_item.status = 0 THEN 'Open' WHEN sma_emrs_item.status = 1 THEN 'Closed'  WHEN sma_emrs_item.status = 2 THEN 'In Process' WHEN sma_emrs_item.status = 3 THEN 'Cancelled' END")
            ->from("sma_emrs")
            ->join("sma_emrs_doc_src","sma_emrs.doc_id=sma_emrs_doc_src.doc_id",'inner')
            ->join("sma_emrs_item","sma_emrs_item.doc_id=sma_emrs_doc_src.doc_id AND sma_emrs_item.doc_no_src_id=sma_emrs_doc_src.doc_no",'inner')
            
            ->group_by("sma_emrs.doc_id,sma_emrs.emrs_no")
            ->where('sma_emrs.segment_id', $_SESSION['segment_id'])
            ->add_column("Actions", "<center><a class=\"tip\" title='" . $this->lang->line("View") . "' href='" . site_url('gatepass/vieworder/$1') . "' data-toggle='modal' data-target='#myModal' ><i class=\"fa fa-eye\"></i></a> </center>", "id");
          
        echo $this->datatables->generate();
    }    
// Add New Function By Ankit
function getgatepasshold_out()
    {
       // $this->sma->checkPermissions('index');
        $this->load->library('datatables');
        $this->datatables
            ->select($this->db->dbprefix('gp') . ".id as id, cust_name, gp.doc_no, DATE_FORMAT(doc_dt,'%D %M, %Y'),DATE_FORMAT(gp_date,'%D %M, %Y'), companies.name")
            ->from("gp")
            ->join("gp_src","gp.id=gp_src.gp_id",'inner')
            ->join("companies","gp.supplier_id=companies.eo_id",'inner')
            ->where("gp.status", '0')
            ->where("gp.gp_type!=", '1084')
            ->where("gp.gp_type!=", '1085')
            ->where("gp.gp_type!=", '1080');
          $this->datatables->add_column("Actions", "<center><a class=\"tip\" title='" . $this->lang->line("view_gp") . "' href='" . site_url('gatepass/view/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-eye\"></i></a> &nbsp; <a class=\"tip\" title='" . $this->lang->line("edit") . "' href='" . site_url('gatepass/gatepass_out_hold/$1') . "'><i class=\"fa fa-edit\"></i></a> </center>", "id");
         echo $this->datatables->generate();
    }  
// Add By Ankt
    function gatepass_out_hold($id = NULL)
    {
        //echo $id; die;
        $a = $this->site->get_warehouse_ids(2);
        //print_r($a); echo ;die;
        
        
        
        $poList = $this->gatepass_model->getWareHouseDetail_out($a[0]['id']);    
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('gatepass_out')));
        $meta = array('page_title' => lang('gatepass'), 'bc' => $bc);
        $allBin = $this->gatepass_model->getAllBin($a[0]['id']);
        $this->data['gatepass_type'] = $this->gatepass_model->getGatepasstype(); 
        $this->data['gpdata'] = $this->gatepass_model->getGatepassByID($id);
        $this->data['all_bin'] = $allBin;   
        $this->data['warehouse_name'] = $poList['warehouse_name']->name;
         $this->data['warehouse'] = $this->gatepass_model->getWhID();            
        $this->data['po_list'] = $poList['po_data']; 
       // $this->data['currency'] = $poList['po_data'][0]->curr_name;
        //$this->data['conv_factor'] = $poList['po_data'][0]->curr_conv_fctr; 
        $this->data['tp_list'] = $poList['tp_details'];
        $this->data['supplier_details'] = $poList['supplier_details'];
        $this->data['sto_no'] = $poList['sto_data']; 
        //print_r($poList); die;
        //print_r($this->data); die;

       if(isset($_POST) && !empty($_POST)){
            print_r($_POST); die;
            if(!empty($_POST['sto_no']) && !empty($_POST['sto_date'])){
               // $_POST['supplier_id'] = $poList['sto_data'][0]->customer_id;               
                //$_POST['curr_id'] = $poList['po_data'][0]->curr_id_sp;
                
                $data = $this->gatepass_model->savegatepassOutdata($_POST,$poList['sto_data'][0]);               
                // print_r($_POST); die;
                if($data>0){
                    //$this->gatepass_model->setPoFlag($_POST['sto_list']);  
                    $this->session->set_flashdata('success', "Gatepass Successfully created");              
                    redirect('gatepass/out');
                    
                }else{
                    $this->session->set_flashdata('error','Failed to create gatepass. Please try again!');
                }
            }else{

                 $this->session->set_flashdata('error','Please enter required fields');
            }    

        }          

            //$this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->page_construct('gatepass/gatepass_out_hold', $meta, $this->data);
    }
// Add By Ankit
    function stockouthold($id = NULL,$qty = NULL){
       //echo "id:=>".$this->input->get('id')."<br>Qty=>".$this->input->get('qty')."<br>stono:=>".$this->input->get('sto_no')."<br>stock=>".$this->input->get('stock')."<br>gpid=>".$this->input->get('gpid')."<br><br>";
        // die;
        $products = array();
        if ($this->input->get('id') && $this->input->get('qty')) {
            $this->data['id'] = $this->input->get('id');
            $this->data['qty'] = $this->input->get('qty');
            $this->data['sto_no'] = $this->input->get('sto_no');
            $this->data['gpid'] = $this->input->get('gpid');
            
            if(!empty($this->input->get('stock1')))
            {
                $this->data['stock'] = $this->input->get('stock1');
                $this->data['products'] = $this->gatepass_model->getholdproductbyid($this->input->get('id'),$this->input->get('sto_no'),$this->input->get('gpid'));
            }
            else{
            $this->data['stock'] = $this->input->get('stock');
           $this->data['products'] = $this->gatepass_model->getholdproductbyid($this->input->get('id'),$this->input->get('sto_no'),$this->input->get('gpid'));
            } 
        }
          //echo "id:=>".$this->input->get('id')."<br>Qty=>".$this->input->get('qty')."<br>stono:=>".$this->input->get('sto_no')."<br>stock".$this->input->get('stock')."<br><br>";
          //echo "<pre>";print_r($this->data); die;
         $this->data['modal_js'] = $this->site->modal_js();

         $this->load->view($this->theme . 'gatepass/stockouthold', $this->data);
        
    }
    
// ---------------Ankit-HM End-------------




    function view($id = NULL)
    {
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['gp'] = $this->gatepass_model->getGatepassByID($id);

        $gpData = $this->data['gp'];

        //supplier details
        $this->db->select('name');
        $suppliers = $this->db->from('companies')
        ->where("eo_id", $gpData->supplier_id);       
        $supplier_detail = $this->db->get()->row();
        
        $this->data['supplier'] = $supplier_detail->name;

        //warehouse details
        $this->db->select('name');
        $warehouse = $this->db->from('warehouses')
        ->where("id", $gpData->warehouse_id);       
        $wh_detail = $this->db->get()->row();
        
        $this->data['warehouse'] = $wh_detail->name;

        //product details
        $this->db->select('products.name, gp_item.item_uom_id, gp_item.req_qty, gp_item.act_rcpt_qty, gp_item.item_price, gp_item.item_amt, gp_item.pending_qty, gp_src.doc_no');
        $product = $this->db->from('gp_item')
        ->join("products", "gp_item.item_id = products.id","left")              
        ->join("gp_src", "gp_item.gp_src_id = gp_src.id","left")
       /* ->join("emrs_item", "emrs_item.doc_id = gp_src.doc_id","left")*/       
        ->where("gp_src.gp_id", $gpData->id);
        $this->data['emrs_detail'] = $this->db->get()->result();
        $this->data['warehouse'] = $wh_detail->name;
        
        $bc = array(array('link' => base_url(), 'page' => lang('home')), 
            array('link' => site_url('gatepass'), 'page' => lang('gatepass')), 
            array('link' => '#', 'page' => lang('gatepass')));
        $meta = array('page_title' => lang('pass_view'), 'bc' => $bc);
        
        $this->load->view($this->theme . 'gatepass/view', $this->data);
    }

    function vieworder($id = NULL)
    {
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
            //echo $id; die;
        }
        
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['gp'] = $this->gatepass_model->getGatepassOrderByID($id);

        $gpData = $this->data['gp'];

        //supplier details
        $this->db->select('name');
        $suppliers = $this->db->from('companies')
        ->where("eo_id", $gpData->supplier_id);       
        $supplier_detail = $this->db->get()->row();
        
        $this->data['supplier'] = $supplier_detail->name;

        //warehouse details
        $this->db->select('name');
        $warehouse = $this->db->from('warehouses')
        ->where("id", $gpData->warehouse_id);       
        $wh_detail = $this->db->get()->row();
        
        $this->data['warehouse'] = $wh_detail->name;

        //product details
        $this->db->select('products.name, products.unit, emrs_item.req_qty, emrs_item.bal_qty, emrs_item.doc_no_src_id,emrs.emrs_dt');
        $this->db->from('emrs_item')
        ->join("products", "emrs_item.item_id = products.id","left")              
        ->join("emrs_doc_src", "emrs_item.doc_id = emrs_doc_src.doc_id","left")
        ->join("emrs", "emrs.doc_id = emrs_doc_src.doc_id","left")
       /* ->join("emrs_item", "emrs_item.doc_id = gp_src.doc_id","left")*/       
        ->where("emrs.id", $gpData->id);
        $this->data['emrs_detail'] = $this->db->get()->result();
        $this->data['warehouse'] = $wh_detail->name;
        
        $bc = array(array('link' => base_url(), 'page' => lang('home')), 
            array('link' => site_url('gatepass'), 'page' => lang('gatepass')), 
            array('link' => '#', 'page' => lang('gatepass')));
        $meta = array('page_title' => lang('pass_view'), 'bc' => $bc);
        
        $this->load->view($this->theme . 'gatepass/vieworder', $this->data);
    }

    
    
    // /* Stock Out Function Added By Anil Start */
    
    // function stockOut(){
        
    //     //$this->sma->checkPermissions(false, true);
        
    //      $this->data['modal_js'] = $this->site->modal_js();
    //      $this->load->view($this->theme . 'gatepass/stockout', $this->data);
        
    // }
    
    // /* Stock Out Function Added By Anil End */
    
   //added by vikas singh for list of warehouses segment wise and gatepass type.
    
    function ajax_call_warehousesbysegment() {       
       
        $gp_type_id = $_POST['gp_type_id'];
        //echo $gp_type_id; die;
	    $arrWarehouses = $this->gatepass_model->getWarehouseListbyGatepassType($gp_type_id);
           
            foreach ($arrWarehouses as $key=>$warehouses) {
                
                $arrWarehouses[$warehouses->id] = $warehouses->name;
            }
            //$finalDropDown = array_merge(array('1' => 'Please Select'), $arrWarehouses);
            
            echo json_encode(form_dropdown('warehouse_name',array_filter($arrWarehouses, 'strlen'),'1','id="warehouselist" class="form-control input-tip" style="width:100%;" required="required"'));
            exit;
        
    } 
    
}
