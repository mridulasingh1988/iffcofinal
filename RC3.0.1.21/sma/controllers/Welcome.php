<?php defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', 36000);
class Welcome extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
        $this->load->library('form_validation');
        $this->load->model('db_model');
        $this->load->model("csv_model");
        $this->load->model("msync_model");

        //$this->load->library("multipledb");
    }

    public function index()
    {   
        // for change invoice no. in reference_no as segement wise         
       

        if($_SESSION['segment_id'])
        {
          $query -
              $query = "delete from sma_companies where id!=1 AND segment_id!='' AND segment_id!='".$_SESSION['segment_id']."'";
              $this->db->query($query);
              $query1 = "delete from sma_companies where name like 'SALES  IFFCO SO - ASSAM' AND id=88112";
              $this->db->query($query1);
             
        }
         // $query2 = "ALTER TABLE `sma_sales` CHANGE `biller_id` `biller_id` VARCHAR(20) NULL DEFAULT NULL";
         //      $this->db->query($query2);
         //  $query3 = "ALTER TABLE `sma_sales` CHANGE `customer_id` `customer_id` BIGINT(20) NOT NULL";
         //  $this->db->query($query3);


         /*$query4 = "ALTER TABLE `sma_companies` ADD `adhar_card_no` VARCHAR(255) NULL AFTER `pos_upd_flg`";
         $this->db->query($query4); */
          //------------------------Add By Ankit to Implimente Sale- LOT/Bin -------------

          /*$w ="DROP TABLE IF EXISTS sma_sale_item_lot123";
          $w1="DROP TABLE IF EXISTS sma_sale_item_bin123";
          $w2="create table sma_sale_item_lot123 (select * from sma_sale_item_lot)";
          $w3="create table sma_sale_item_bin123 (select * from sma_sale_item_bin)";
          $w4="TRUNCATE TABLE sma_sale_item_lot";
          $w5="TRUNCATE TABLE sma_sale_item_bin";
          $w6="INSERT INTO sma_sale_item_lot(org_id,wh_id,warehouse_id,code,sale_id,sale_item_id,lot_id,lot_no,quantity,created_date) SELECT c.org_id,c.wh_id,a.warehouse_id,a.product_code,a.sale_id,a.id,d.id,a.lot_no,a.quantity,b.date FROM sma_sale_items a INNER JOIN sma_sales b ON (b.id = a.sale_id) INNER JOIN sma_products c ON (c.id = a.product_id AND c.warehouse = a.warehouse_id) INNER JOIN sma_item_stk_lot d ON (d.item_id = a.product_id AND d.warehouse_id = a.warehouse_id AND d.lot_no = a.lot_no) WHERE 1";
          $w7="INSERT INTO sma_sale_item_bin(org_id,wh_id,warehouse_id,code,sale_id,sale_item_id,lot_id,bin_id,bin_name,quantity,created_date) SELECT c.org_id,c.wh_id,a.warehouse_id,a.product_code,a.sale_id,a.id,d.id,e.id,z.bin_nm,a.quantity,b.date FROM sma_sale_items a INNER JOIN sma_sales b ON (b.id = a.sale_id) INNER JOIN sma_products c ON (c.id = a.product_id AND c.warehouse = a.warehouse_id) INNER JOIN sma_item_stk_lot d ON (d.item_id = a.product_id AND d.warehouse_id = a.warehouse_id AND 
          d.lot_no = a.lot_no) INNER JOIN sma_bin z ON (z.id = a.bin_id) INNER JOIN sma_item_stk_lot_bin e ON (e.bin_id = a.bin_id AND a.product_id = e.item_id)  WHERE 1";
          $this->db->query($w);
          $this->db->query($w1);
          $this->db->query($w2);
          $this->db->query($w3);
          $this->db->query($w4);
          $this->db->query($w5);
          $this->db->query($w6);
          $this->db->query($w7);*/

          $sale_biller_id = "UPDATE sma_sales a INNER JOIN sma_companies b ON a.customer_id = b.id SET a.biller_id=CASE WHEN (b.segment_id is null OR b.segment_id=0) AND b.eo_id is not null THEN b.eo_id ELSE b.phone END WHERE a.customer_id = b.id";
          $this->db->query($sale_biller_id);

          $cus_id = "ALTER TABLE `sma_companies` CHANGE `id` `id` BIGINT(20) NOT NULL AUTO_INCREMENT";

            $this->db->query($cus_id);

        $query = "SHOW COLUMNS FROM sma_pos_register LIKE 'upd_flg'";
        $uflg= $this->db->query($query);
        $uflg->num_rows();

        if($uflg->num_rows()==0) { 

            $alter = "ALTER TABLE `sma_pos_register` ADD `upd_flg` INT(2) NULL DEFAULT NULL AFTER `sync_flg`"; 
            $this->db->query($alter);
        }
          

          //------------------------------------------------------------------------------
              
       
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['sales'] = $this->db_model->getLatestSales();
        $this->data['quotes'] = $this->db_model->getLastestQuotes();
        $this->data['purchases'] = $this->db_model->getLatestPurchases();
        $this->data['transfers'] = $this->db_model->getLatestTransfers();
        $this->data['customers'] = $this->db_model->getLatestCustomers();
        $this->data['suppliers'] = $this->db_model->getLatestSuppliers();
        $this->data['chatData'] = $this->db_model->getChartData();
        $this->data['stock'] = $this->db_model->getStockValue();
        $this->data['bs'] = $this->db_model->getBestSeller();
        $lmsdate = date('Y-m-d', strtotime('first day of last month')) . ' 00:00:00';
        $lmedate = date('Y-m-d', strtotime('last day of last month')) . ' 23:59:59';
        $this->data['lmbs'] = $this->db_model->getBestSeller($lmsdate, $lmedate);
        $bc = array(array('link' => '#', 'page' => lang('dashboard')));
        $meta = array('page_title' => lang('dashboard'), 'bc' => $bc);
        $this->page_construct('dashboard', $meta, $this->data);
    }

    function promotions()
    {
        $this->load->view($this->theme . 'promotions', $this->data);
    }

    function image_upload()
    {
        if (DEMO) {
            $error = array('error' => $this->lang->line('disabled_in_demo'));
            echo json_encode($error);
            exit;
        }
        $this->security->csrf_verify();
        if (isset($_FILES['file'])) {
            $this->load->library('upload');
            $config['upload_path'] = 'assets/uploads/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '500';
            $config['max_width'] = $this->Settings->iwidth;
            $config['max_height'] = $this->Settings->iheight;
            $config['encrypt_name'] = TRUE;
            $config['overwrite'] = FALSE;
            $config['max_filename'] = 25;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('file')) {
                $error = $this->upload->display_errors();
                $error = array('error' => $error);
                echo json_encode($error);
                exit;
            }
            $photo = $this->upload->file_name;
            $array = array(
                'filelink' => base_url() . 'assets/uploads/images/' . $photo
            );
            echo stripslashes(json_encode($array));
            exit;

        } else {
            $error = array('error' => 'No file selected to upload!');
            echo json_encode($error);
            exit;
        }
    }

    function set_data($ud, $value)
    {
        $this->session->set_userdata($ud, $value);
        echo true;
    }

    function hideNotification($id = NULL)
    {
        $this->session->set_userdata('hidden' . $id, 1);
        echo true;
    }

    function language($lang = false)
    {
        if ($this->input->get('lang')) {
            $lang = $this->input->get('lang');
        }
        //$this->load->helper('cookie');
        $folder = 'sma/language/';
        $languagefiles = scandir($folder);
        if (in_array($lang, $languagefiles)) {
            $cookie = array(
                'name' => 'language',
                'value' => $lang,
                'expire' => '31536000',
                'prefix' => 'sma_',
                'secure' => false
            );

            $this->input->set_cookie($cookie);
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }

    function download($file)
    {
        $this->load->helper('download');
        force_download('./files/'.$file, NULL);
        exit();
    }

    // Add By Ankit For sync Data at inter store -----------------------
    function POSImport()
    {
        if($this->msync_model->syncMRN())
        {  
            $this->session->set_flashdata('message', "MRN And GatePass Data Sync Intermediate to POS DB Successfully");
            redirect('welcome');
        }    
 
    }
    // function intrmExport()
    // {
    //     if($this->msync_model->syncToIntermediate())
    //     {
    //         $this->session->set_flashdata('message', "Data Sync POS DB To Intermediate Successfully");
    //         redirect('welcome');
    //     }
       
 
    // }

    // End Add By Ankit For sync Data at inter store -----------------------

    // Add By Ankit For sync Data at inter store -----------------------
    function POSsyncImport()
    {

        if($this->msync_model->syncToPos())
        {  
            $this->session->set_flashdata('message', "Data Sync Intermediate to POS DB Successfully");
            redirect('welcome');
        }
        $this->session->set_flashdata('error', "Data Not Sync Intermediate to POS DB Successfully, please contact administrator.");
        redirect('welcome');
        //$this->session->set_flashdata('error', "Work in progress..");
        //redirect('welcome');
    }
    function intrmsyncExport()
    {
        if($this->msync_model->syncToIntermediate())
        {
            $this->session->set_flashdata('message', "Data Sync POS DB To Intermediate Successfully");
            redirect('welcome');
        }
        $this->session->set_flashdata('error', "Data Not Sync POS DB To Intermediate, please contact administrator.");
        redirect('welcome');
       // $this->session->set_flashdata('error', "Work in progress..");
       // redirect('welcome');
    }
    function updatepricesls()
    {
        if($this->msync_model->Update_price_sls())
        {
            $this->session->set_flashdata('message', "Central POS price Update Successfully");
            redirect('welcome');
        }
        $this->session->set_flashdata('error', "Price Not Updated, please contact administrator.");
        redirect('welcome');
       
    }
   function emptyPOS()
    {
        if($this->msync_model->emptyPOS())
        {
            if($this->msync_model->intrmPOSIDempty())
            {
                $this->session->set_flashdata('message', "POS Data Base Empty Successfully");
                redirect('welcome');
            }
        }
        $this->session->set_flashdata('error', "POS Data Base Empty, please contact administrator.");
        redirect('welcome');
       // $this->session->set_flashdata('error', "Work in progress..");
        //redirect('welcome');
    } 

    function exportData(){        
        $this->msync_model->syncData();
        exit;
    }

    function createCSVlocation()
    {
       // for expense tabel

        $query = "SHOW COLUMNS FROM sma_expenses LIKE 'sync_flg'";
        $sflg= $this->db->query($query);
        $sflg->num_rows();

        if($sflg->num_rows()==0) { 

            $alter = "ALTER TABLE `sma_expenses` ADD `sync_flg` VARCHAR(2) NULL DEFAULT NULL AFTER `pos_upd_flg`"; 
            $this->db->query($alter);
        }

        $query = "SHOW COLUMNS FROM sma_expenses LIKE 'upd_flg'";
        $uflg= $this->db->query($query);
        $uflg->num_rows();

        if($uflg->num_rows()==0) { 

            $alter = "ALTER TABLE `sma_expenses` ADD `upd_flg` VARCHAR(2) NULL DEFAULT NULL AFTER `sync_flg`"; 
            $this->db->query($alter);
        }

        $query = "SELECT id FROM sma_syncout_table WHERE table_name LIKE 'sma_expenses'";
        $tflg= $this->db->query($query);
        $tflg->num_rows();

        if($tflg->num_rows()==0) { 

            $alter = "INSERT INTO  `sma_syncout_table` (`id` ,`table_name` ,`status`)VALUES (30 ,'sma_expenses',  '1')"; 
            $this->db->query($alter);
        }


        // end expense table

       
        $this->msync_model->syncoutData();
        if(1)
        {
            $this->session->set_flashdata('message', "CSV Create and Send to Central POS DB Successfully");
                redirect('welcome');
            
        }
        $this->session->set_flashdata('error', "CSV Not Create, Please Contact Administrator.");
        redirect('welcome');
    }

    function syncin(){       
        
        
         // column addition for GST No.

        $query = "SHOW COLUMNS FROM sma_purchase_items LIKE 'hsn_code'";
        $uflg= $this->db->query($query);
        $uflg->num_rows();

        if($uflg->num_rows()==0) { 

            $alter = "ALTER TABLE `sma_purchase_items` ADD `hsn_code` VARCHAR( 20 ) NULL DEFAULT NULL"; 
            $this->db->query($alter);
        }

        $query = "SHOW COLUMNS FROM sma_products LIKE 'hsn_code'";
        $uflg= $this->db->query($query);
        $uflg->num_rows();

        if($uflg->num_rows()==0) { 

            $alter = "ALTER TABLE  `sma_products` ADD  `hsn_code` VARCHAR( 20 ) NULL DEFAULT NULL"; 
            $this->db->query($alter);
        }  
        
        $query = "SHOW COLUMNS FROM sma_segment LIKE 'gst_regn_no'";
        $uflg= $this->db->query($query);
        $uflg->num_rows();

        if($uflg->num_rows()==0) { 

            $alter = "ALTER TABLE  `sma_segment` ADD  `gst_regn_no` VARCHAR( 40 ) NULL DEFAULT NULL , ADD  `gst_regn_dt` DATE NULL DEFAULT NULL ,ADD  `gst_regn_flg` VARCHAR( 1 ) NULL DEFAULT NULL"; 
            $this->db->query($alter);
        }

        $query = "SHOW COLUMNS FROM sma_segment LIKE 'org_desc'";
        $uflg= $this->db->query($query);
        $uflg->num_rows();

        if($uflg->num_rows()==0) { 

            $alter = "ALTER TABLE `sma_segment` ADD `org_desc` VARCHAR( 255 ) NULL DEFAULT NULL"; 
            $this->db->query($alter);
        }  
        
        $query = "SHOW COLUMNS FROM sma_segment LIKE 'tax_code'";
        $uflg= $this->db->query($query);
        $uflg->num_rows();

        if($uflg->num_rows()==0) { 

            $alter = "ALTER TABLE  `sma_segment` ADD  `tax_code` VARCHAR( 10 ) NULL DEFAULT NULL AFTER  `org_desc` ,ADD  `adds_loc` VARCHAR( 100 ) NULL DEFAULT NULL AFTER  `tax_code` ,ADD  `adds` VARCHAR( 4000 ) NULL DEFAULT NULL AFTER  `adds_loc`"; 
            $this->db->query($alter);
        } 

        $query = "SHOW COLUMNS FROM sma_companies LIKE 'gst_no'";
        $uflg= $this->db->query($query);
        $uflg->num_rows();

        if($uflg->num_rows()==0) { 

            $alter = "ALTER TABLE `sma_companies`  ADD `gst_no` VARCHAR(20) NULL DEFAULT NULL,  ADD `gst_effect_date` DATE NULL DEFAULT NULL,  ADD `customer_type` VARCHAR(50) NULL DEFAULT NULL,  ADD `mfms_id` VARCHAR(20) NULL DEFAULT NULL,  ADD `fertilizers_licence_no` VARCHAR(20) NULL DEFAULT NULL,  ADD `fertilizers_exp_date` DATE NULL DEFAULT NULL,  ADD `pesticides_licence_no` VARCHAR(20) NULL DEFAULT NULL,  ADD `pesticides_exp_date` DATE NULL DEFAULT NULL,  ADD `seeds_licence_no` VARCHAR(20) NULL DEFAULT NULL,  ADD `seeds_exp_date` DATE NULL DEFAULT NULL"; 
            $this->db->query($alter);
        } 

        $query = "SHOW COLUMNS FROM sma_payments LIKE 'bank_name'";
        $uflg= $this->db->query($query);
        $uflg->num_rows();

        if($uflg->num_rows()==0) { 

            $alter = "ALTER TABLE `sma_payments` ADD `bank_name` VARCHAR( 255 ) NULL DEFAULT NULL ,ADD `eo_type` VARCHAR( 50 ) NULL DEFAULT NULL"; 
            $this->db->query($alter);
        } 
        
       // end GST query      
        
        

        $directory = $_SERVER["DOCUMENT_ROOT"].'/'.PROJECT_NAME.'/sync-folder/';
        $meta = array('page_title' => lang('Sync Data from Central server'));
        $this->load->model('site');
        $segmentList = $this->site->getAllSegments();
        $this->data['segmentList'] = $segmentList;
        $segment_id = $_SESSION['segment_id'];

        if(isset($segment_id) && ($segment_id!='')){         
            $remote_host = REMOTE_HOST;
            $remote_username = REMOTE_USERNAME;
            $remote_password = REMOTE_PWD;
            $remote_dbname = REMOTE_DB;

            /*$remote_host = 'localhost';
            $remote_username = 'root';
            $remote_password = '';
            $remote_dbname = 'iffco_final_live';*/

            $remote_connection = mysqli_connect($remote_host, $remote_username, $remote_password, $remote_dbname) or die("Connection Error " . mysqli_error($remote_connection));
            
            $host = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // open connection to mysql database
            $connection = mysqli_connect($host, $username, $password, $dbname) or die("Connection Error " . mysqli_error($connection));

            $query= $this->db->query("SELECT id FROM `sma_warehouses` where name like '%GODOWN%' LIMIT 1");
            $warehouse_id1=$query->result();
            $warehouse_id= $warehouse_id1[0]->id;
            $segment_id = $this->session->userdata('segment_id');
     
        //  Update price sma_purchase_items

             $sql = "select net_unit_cost,item_tax,tax,subtotal,unit_cost,real_unit_cost,warehouse_id,product_code,id from sma_purchase_items where warehouse_id=".$warehouse_id." AND price_flg=1";
             $purchase_data = mysqli_query($remote_connection, $sql) or die("Selection Error " . mysqli_error($remote_connection));
             $price_list1 = mysqli_fetch_all($purchase_data,MYSQLI_ASSOC); 

          foreach ($price_list1 as  $col1) {

                $col= array_values($col1); 

                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];
                $col4 = $col[3];
                $col5 = $col[4];
                $col6 = $col[5];
                $col7 = $col[6];
                $col8 = $col[7];
                $col9 = $col[8];
                
                $query = "UPDATE `sma_purchase_items` SET `net_unit_cost`='".$col1."',`item_tax`='".$col2."',`tax`='".$col3."',`subtotal`='".$col4."',`unit_cost`='".$col5."',`real_unit_cost`='".$col6."' WHERE warehouse_id='".$col7."' AND product_code='".$col8."'";
                $this->db->query($query);

                $upd = "UPDATE `sma_purchase_items` SET `price_flg`=NULL WHERE id='".$col9."' AND warehouse_id='".$col7."'";
                 mysqli_query($remote_connection, $upd);

            }

    //  Update price sma_products

             $sql = "select id,price,warehouse from sma_products where warehouse=".$warehouse_id." AND price_flg=1";
             $product_data = mysqli_query($remote_connection, $sql) or die("Selection Error " . mysqli_error($remote_connection));
             $product_list1 = mysqli_fetch_all($product_data,MYSQLI_ASSOC); 

          foreach ($product_list1 as  $col1) {

                $col= array_values($col1); 

                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];

                 $query = "UPDATE `sma_products` SET `price`='".$col2."' WHERE id='".$col1."' AND warehouse='".$col3."'";
                 $this->db->query($query);

                 $upd = "UPDATE `sma_products` SET `price_flg`=NULL WHERE id='".$col1."' AND warehouse='".$col3."'";
                 mysqli_query($remote_connection, $upd);

            }


            $sql = "select * from sma_csv_history where segment_id=".$segment_id." AND flag=0 ORDER BY created_date ASC";
            $csv_data = mysqli_query($remote_connection, $sql) or die("Selection Error " . mysqli_error($remote_connection));
            $csv_list = mysqli_fetch_array($csv_data, MYSQLI_ASSOC);           
            mysqli_autocommit($remote_connection, FALSE);
            $files_list = array();

            /*mysqli_query($connection, 'DROP TABLE IF EXISTS sma_product123') or die("Drop product Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'DROP TABLE IF EXISTS sma_purchase_item123') or die("Drop Purchase Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'DROP TABLE IF EXISTS sma_drft_po_dlv_schdl123') or die("Drop Po Schedule Error " . mysqli_error($connection));
              mysqli_query($connection, 'DROP TABLE IF EXISTS sma_drft_po123') or die("Drop Po Schedule Error " . mysqli_error($connection));
              mysqli_query($connection, 'DROP TABLE IF EXISTS sma_company123') or die("Drop Company Error " . mysqli_error($connection));
              mysqli_query($connection, 'DROP TABLE IF EXISTS sma_item_stk_profile123') or die("Drop STKProfile Error " . mysqli_error($connection));
              mysqli_query($connection, 'DROP TABLE IF EXISTS sma_item_stk_lot123') or die("Drop STKLot Error " . mysqli_error($connection));
              mysqli_query($connection, 'DROP TABLE IF EXISTS sma_item_stk_lot_bin123') or die("Drop STKLotBin Error " . mysqli_error($connection));

              mysqli_query($connection, 'create table sma_product123 (select * from sma_products)') or die("Create Product Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'create table sma_purchase_item123 (select * from sma_purchase_items)') or die("Create Purchase Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'create table sma_drft_po_dlv_schdl123 (select * from sma_drft_po_dlv_schdl)') or die("Create PO Schedule Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'create table sma_drft_po123 (select * from sma_drft_po)') or die("Create PO Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'create table sma_item_stk_profile123 (select * from sma_item_stk_profile)') or die("Create PO Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'create table sma_item_stk_lot123 (select * from sma_item_stk_lot)') or die("Create LotMaster Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'create table sma_item_stk_lot_bin123 (select * from sma_item_stk_lot_bin)') or die("Create LotBin Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'create table sma_company123 (select * from sma_companies)') or die("Create Company123 Master Error " . mysqli_error($connection));
     
              mysqli_query($connection, 'TRUNCATE TABLE sma_warehouses') or die("TRUNCATE Wharehouse Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_users') or die("TRUNCATE User Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_products') or die("TRUNCATE Product Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_purchase_items') or die("TRUNCATE Purchase Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_tax_rates') or die("TRUNCATE Tax Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_bin') or die("TRUNCATE Bin Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_categories') or die("TRUNCATE Category Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_drft_po') or die("TRUNCATE Category Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_drft_po_dlv_schdl') or die("TRUNCATE Category Master Error " . mysqli_error($connection));
        
              mysqli_query($connection, 'TRUNCATE TABLE sma_item_stk_profile') or die("TRUNCATE profile table Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_item_stk_lot') or die("TRUNCATE profile lot table Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_item_stk_lot_bin') or die("TRUNCATE profile lot bin table Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_companies') or die("TRUNCATE Company Master Error " . mysqli_error($connection));
     
              mysqli_query($connection, 'TRUNCATE TABLE sma_emrs') or die("TRUNCATE Emrs Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_emrs_doc_src') or die("TRUNCATE Emrs DOC SRC Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_emrs_item') or die("TRUNCATE Emrs Item Master Error " . mysqli_error($connection));*/

            foreach($csv_data as $rows){
                
               //   echo $row['file_name'];exit;
                $csv_date = explode("/",$rows['file_name']);
                $csv_folder = $csv_date[count($csv_date)-2];
                
                if (!is_dir($directory.$csv_folder)) { //create the folder if it's not already exists
                    mkdir($directory.$csv_folder, 0755, TRUE);                
                }  
                $copy = copy( $rows['file_name'], $directory.$csv_folder.'/'.$csv_date[count($csv_date)-1] );   

                /* Add notice for success/failure */
                if( !$copy ) {
                    echo "Failed to copy csv for date".$rows['created_date'];
                    //exit;
                }else{
                     $files_list[] = $directory.$csv_folder.'/'.$csv_date[count($csv_date)-1];
                     $q1 = "UPDATE sma_csv_history SET flag = '1' WHERE id='".$rows['id']."' ";
                    if(mysqli_query($remote_connection, $q1))
                    {
                     /* commit insert */
                        mysqli_commit($remote_connection);
                    }
                    else{
                         /* Rollback */
                        mysqli_rollback($remote_connection);

                    }
                }               
            }

            
            
            $sql = "select * from sma_segment where id=".$segment_id."";
            $seg_result = mysqli_query($connection, $sql) or die("Selection Error " . mysqli_error($connection));
            $seg_row = mysqli_fetch_array($seg_result, MYSQLI_ASSOC);
            $projName = explode(" ",$seg_row['proj_name'])[0]."_".$segment_id;
            


            if(count($files_list)>0){
                foreach($files_list as $fileName){
                    $zip = new ZipArchive;
                    $csv_date = explode("/",$fileName);
                    $csv_folder = $csv_date[count($csv_date)-2];
                   
                if ($zip->open($fileName) === TRUE) {
                    $csv_path = $directory.$csv_folder."/".$projName."/";
                    $zip->extractTo($csv_path);
                    $zip->close();
                 /*else {
                    echo 'Failed to extract zip file';
                    exit;
                }  */             
                // path where your CSV file is located
               // define('CSV_PATH',$_SERVER["DOCUMENT_ROOT"].'/'.PROJECT_NAME.'/sync-folder/'.$projName.'/');
                //define('CSV_PATH',$csv_path);
                //echo "csv path".CSV_PATH;exit;
                // Count File

                $sql = "select count(*) as count from sma_sync_table";
                $result = mysqli_query($connection, $sql) or die("Selection Error " . mysqli_error($connection));
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                $num_table=$row['count'];
                for ($x = 1; $x <= $num_table; $x++){               
                    
                    $sql = "select * from sma_sync_table where id='$x'";
                    $result = mysqli_query($connection, $sql) or die("Selection Error " . mysqli_error($connection));
                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                    $table=$row['table_name'];
                    // fetch mysql table rows
                    $csv_filename = $table.".csv"; // to be changed
                    //$csv_filename ='sma_segment_2017-02-05.csv';
                    // Name of your CSV file
                    $csv_file = $csv_path . $csv_filename;
                     //echo $csv_file;exit;  
                    if (($handle = fopen($csv_file, "r")) !== FALSE) {
                        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        $col = array();
                        $num = count($data);
                        for ($c=0; $c < $num; $c++) {
                           $col[$c] = $data[$c];
                        }
                        /*if($table=='sma_tax_rates'){
                            echo "<pre>".$csv_filename;exit;
                            print_r($col);exit;
                        }*/
                         
                        switch ($table) {
                            case "sma_segment":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];

                                
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                //if(isset($col[7]) && ($col[7]=='1' && (is_null($col[6])))){
                                if(($col7 ==1 && $col8==1) || ($col7 !=1 && $col8==1)){ 
                                
                                    $query = "UPDATE sma_segment SET `proj_name`='".$col5."',`prj_actv`= '".$col6."',`gst_regn_no`= '".$col10."',`gst_regn_dt`= '".$col11."',`gst_regn_flg`= '".$col12."',`org_desc`= '".$col13."',`tax_code`= '".$col14."',`adds_loc`= '".$col15."',`adds`= '".$col16."' WHERE id=".$col1."";
                                }else{
                                   $query = "INSERT ignore INTO sma_segment
                                    (id,org_id,prj_doc_id,parent_proj_id,proj_name,prj_actv,gst_regn_no,gst_regn_dt,gst_regn_flg,org_desc,tax_code,adds_loc,adds) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."')";
                                } 
                                break;
                            case "sma_warehouses":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                               if(isset($col[20]) && ($col[20]=='1' && (is_null($col[19])))){
                                
                                    $query = "UPDATE `sma_warehouses` SET `wh_type`='".$col4."',`name`='".$col7."',`wh_onrshp_type`='".$col8."',`wh_strg_type`='".$col9."',`adds_id`='".$col10."',`wh_desc`='".$col11."',`actv`='".$col12."',`segment_id`='".$col17."',`address`='".$col19."' WHERE id='".$col1."'";
                                }else{

                                    $query = "INSERT ignore INTO sma_warehouses
                                    (id,org_id,wh_id,wh_type,usr_id_create,usr_id_create_dt,name,wh_onrshp_type,wh_strg_type,adds_id,wh_desc,actv,inactv_resn,inactv_dt,wh_ent_id,prj_id,segment_id,biller_id,address) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."')";
                                }
                                break;
                             case "sma_users":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];
                                $col22 = $col[21];
                                $col23 = $col[22];
                                $col24 = $col[23];
                                $col25 = $col[24];
                                $col26 = $col[25];
                                $col27 = $col[26];
                                $col28 = $col[27];
                                $col29 = $col[28];
                                $col30 = $col[29];
                                $col31 = $col[30];
                                $col32 = $col[31];
                                $col33 = $col[32];
                                if(isset($col[34]) && ($col[34]=='1' && (is_null($col[33])))){
                               
                                    $query = "UPDATE `sma_users` SET `email`='".$col7."',`active`='".$col14."',`first_name`='".$col15."',`last_name`='".$col16."',`company`='".$col17."',`phone`='".$col18."',`gender`='".$col20."',`warehouse_id`='".$col22."',`segment_id`='".$col23."' WHERE id='".$col1."'";
                                }else{
                                    $query = "INSERT ignore INTO sma_users
                                    (`id`, `last_ip_address`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `avatar`, `gender`, `group_id`, `warehouse_id`, `segment_id`, `biller_id`, `company_id`, `show_cost`, `show_price`, `award_points`, `show_discount`, `upd_flg`, `usr_id`, `pwid`, `org_id`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col24."','".$col25."','".$col26."','".$col27."','".$col28."','".$col29."','".$col30."','".$col31."','".$col32."','".$col33."')";
                                }
                                break;
                            case "sma_products":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];
                                $col22 = $col[21];
                                $col23 = $col[22];
                                $col24 = $col[23];
                                $col25 = $col[24];
                                $col26 = $col[25];
                                $col27 = $col[26];
                                $col28 = $col[27];
                                $col29 = $col[28];
                                $col30 = $col[29];
                                $col31 = $col[30];
                                $col32 = $col[31];
                                $col33 = $col[32];
                                $col34 = $col[33];
                                $col35 = $col[34];
                                $col36 = $col[35];
                                $col37 = $col[36];
                                $col38 = $col[37];
                                $col39 = $col[38];
                                $col40 = $col[39];
                                $col41 = $col[40];
                                $col42 = $col[41];
                                $col43 = $col[42];
                                $col44 = $col[43];
                                $col45 = $col[44];
                                $col46 = $col[45];
                                $col47 = $col[46];
                                $col55 = $col[54];
                                // && (is_null($col[47]))
                                if(isset($col[48]) && ($col[48]=='1')){
                                  

                                    $query = "UPDATE `sma_products` SET `name`='".$col3."',`unit`='".$col4."',`cost`='".$col5."',`price`='".$col6."',`alert_quantity`='".$col7."',`category_id`='".$col9."',`subcategory_id`='".$col10."',`quantity`='".$col17."',`tax_rate`='".$col18."',`track_quantity`='".$col19."',`details`='".$col20."',`warehouse`='".$col21."',`barcode_symbology`='".$col22."',`product_details`='".$col24."',`tax_method`='".$col25."',`serialized`='".$col38."',`serial_number`='".$col39."',`grp_id`='".$col44."',`hsn_code`='".$col55."' WHERE id='".$col1."'";

                                }else{
                                    $query = "INSERT ignore INTO sma_products
                                    (`id`, `code`, `name`, `unit`, `cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `barcode_symbology`, `file`, `product_details`, `tax_method`, `type`, `supplier1`, `supplier1price`, `supplier2`, `supplier2price`, `supplier3`, `supplier3price`, `supplier4`, `supplier4price`, `supplier5`, `supplier5price`, `max_discount`, `serialized`, `serial_number`, `lot_no`, `sr_no`, `org_id`, `wh_id`, `grp_id`, `trsfr_flg`, `tupd_flg`, `psale`, `hsn_code`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col24."','".$col25."','".$col26."','".$col27."','".$col28."','".$col29."','".$col30."','".$col31."','".$col32."','".$col33."','".$col34."','".$col35."','".$col36."','".$col37."','".$col38."','".$col39."','".$col40."','".$col41."','".$col42."','".$col43."','".$col44."','".$col45."','".$col46."','".$col47."','".$col55."')";
                                }
                              //  echo $query;exit;
                            break;
                            case "sma_purchase_items":
                                
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];
                                $col22 = $col[21];
                                $col23 = $col[22];
                                $col24 = $col[23];
                                $col25 = $col[24];
                                $col26 = $col[25];
                                $col27 = $col[26];
                                $col28 = $col[27];
                                $col29 = $col[28];
                                $col30 = $col[29];
                                $col31 = $col[30];
                                $col32 = $col[31];
                                $col33 = $col[32];
                                $col34 = $col[33];
                                $col35 = $col[34];
                                $col36 = $col[35];
                                $col37 = $col[36];
                                $col38 = $col[37];
                                $col39 = $col[38];
                                $col40 = $col[39];
                                $col41 = $col[40];
                                $col47 = $col[46];

                                // && (is_null($col[41])))

                                if(isset($col[42]) && ($col[42]=='1')){
                                
                                    $query = "UPDATE `sma_purchase_items` SET `purchase_id`='".$col2."',`transfer_id`='".$col3."',`category_id`='".$col4."',`product_id`='".$col5."',`product_code`='".$col6."',`product_name`='".$col7."',`itm_desc`='".$col8."',`long_desc`='".$col9."',`option_id`='".$col10."',`net_unit_cost`='".$col11."',`quantity`='".$col12."',`alert_quantity`='".$col13."',`track_quantity`='".$col14."',`type`='".$col15."',`warehouse_id`='".$col16."',`taxable`='".$col18."',`attribute`='".$col19."',`item_tax`='".$col20."',`tax_rate`='".$col21."',`tax_method`='".$col22."',`tax_rate_id`='".$col23."',`tax`='".$col24."',`discount`='".$col25."',`item_discount`='".$col26."',`expiry`='".$col27."',`subtotal`='".$col28."',`quantity_balance`='".$col29."',`is_serialized`='".$col31."',`status`='".$col33."',`unit_cost`='".$col34."',`real_unit_cost`='".$col35."',`hsn_code`='".$col47."' WHERE warehouse_id='".$col16."' AND product_code='".$col6."'";
                                }else{
                                    $query = "INSERT ignore INTO sma_purchase_items
                                    (`id`, `purchase_id`, `transfer_id`, `category_id`, `product_id`, `product_code`, `product_name`, `itm_desc`, `long_desc`, `option_id`, `net_unit_cost`, `quantity`, `alert_quantity`, `track_quantity`, `type`, `warehouse_id`, `image`, `taxable`, `attribute`, `item_tax`, `tax_rate`, `tax_method`, `tax_rate_id`, `tax`, `discount`, `item_discount`, `expiry`, `subtotal`, `quantity_balance`, `date`, `is_serialized`, `user_id`, `status`, `unit_cost`, `real_unit_cost`, `lot_no`, `sr_no`, `org_id`, `wh_id`, `grp_id`, `psale`, `hsn_code`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col24."','".$col25."','".$col26."','".$col27."','".$col28."','".$col29."','".$col30."','".$col31."','".$col32."','".$col33."','".$col34."','".$col35."','".$col36."','".$col37."','".$col38."','".$col39."','".$col40."','".$col41."','".$col47."')";
                                }

                            break;
                            
                            case "sma_item_stk_profile":
                            $col1 = $col[0];
                            $col2 = $col[1];
                            $col3 = $col[2];
                            $col4 = $col[3];
                            $col5 = $col[4];
                            $col6 = $col[5];
                            $col7 = $col[6];
                            $col8 = $col[7];
                            $col9 = $col[8];
                            $col10 = $col[9];
                            $col11 = $col[10];
                            $col12 = $col[11];
                            $col13 = $col[12];
                            $col14 = $col[13];
                            $col15 = $col[14];
                            $col16 = $col[15];
                            $col17 = $col[16];
                            $col18 = $col[17];
                            $col19 = $col[18];
                            if(isset($col[20]) && ($col[20]=='1' && (is_null($col[19])))){
                           
                                $query = "UPDATE `sma_item_stk_profile` SET `fy_id`='".$col6."',`item_uom_id`='".$col8."',`available_stk`='".$col10."',`rejected_stk`='".$col11."',`purchase_price`='".$col14."',`sales_price`='".$col15."' WHERE id='".$col1."'";
                            }else{

                            $query = "INSERT ignore INTO sma_item_stk_profile
                            (`id`, `org_id`, `segment_id`, `warehouse_id`, `wh_id`, `fy_id`, `item_id`, `item_uom_id`, `total_stk`, `available_stk`, `rejected_stk`, `rework_stk`, `ordered_stk`, `purchase_price`, `sales_price`, `created_date`, `code`, `create_flg`, `upd_flg`) 
                            VALUES
                            ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."')";
                        }
                        break;
                        case "sma_item_stk_lot":
                            $col1 = $col[0];
                            $col2 = $col[1];
                            $col3 = $col[2];
                            $col4 = $col[3];
                            $col5 = $col[4];
                            $col6 = $col[5];
                            $col7 = $col[6];
                            $col8 = $col[7];
                            $col9 = $col[8];
                            $col10 = $col[9];
                            $col11 = $col[10];
                            $col12 = $col[11];
                            $col13 = $col[12];
                            $col14 = $col[13];
                            $col15 = $col[14];
                            $col16 = $col[15];
                            $col17 = $col[16];
                            $col18 = $col[17];
                            $col19 = $col[18];
                            if(isset($col[20]) && ($col[20]=='1' && (is_null($col[19])))){
                            
                                $query = "UPDATE `sma_item_stk_lot` SET `batch_no`='".$col9."',`total_stk`='".$col11."',`rejected_stk`='".$col12."',`mfg_date`='".$col13."',`exp_date`='".$col14."' WHERE id='".$col1."'";
                            }else{
                                $query = "INSERT ignore INTO sma_item_stk_lot
                                (`id`, `org_id`, `segment_id`, `warehouse_id`, `wh_id`, `lot_no`, `item_stk_id`, `item_id`, `batch_no`, `itm_uom_id`, `total_stk`, `rejected_stk`, `mfg_date`, `exp_date`, `reworkable_stk`, `created_date`, `code`, `create_flg`, `upd_flg`) 
                                VALUES
                                ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."')";
                            }
                        break;
                        case "sma_item_stk_lot_bin":
                            $col1 = $col[0];
                            $col2 = $col[1];
                            $col3 = $col[2];
                            $col4 = $col[3];
                            $col5 = $col[4];
                            $col6 = $col[5];
                            $col7 = $col[6];
                            $col8 = $col[7];
                            $col9 = $col[8];
                            $col10 = $col[9];
                            $col11 = $col[10];
                            $col12 = $col[11];
                            $col13 = $col[12];
                            $col14 = $col[13];
                            $col15 = $col[14];
                            $col16 = $col[15];
                            
                            if(isset($col[17]) && ($col[17]=='1' && (is_null($col[16])))){
                          //  if((isset($col[17])) && ($col[17]==1)){
                                $query = "UPDATE `sma_item_stk_lot_bin` SET `lot_id`='".$col7."',`bin_id`='".$col8."',`itm_uom_id`='".$col9."',`total_stk`='".$col10."',`rejected_stk`='".$col11."' WHERE id='".$col1."'";
                            }else{
                                $query = "INSERT ignore INTO sma_item_stk_lot_bin
                                (`id`, `org_id`, `segment_id`, `warehouse_id`, `wh_id`, `item_id`, `lot_id`, `bin_id`, `itm_uom_id`, `total_stk`, `rejected_stk`, `reworkable_stk`, `created_date`, `code`, `create_flg`, `upd_flg`) 
                                VALUES
                                ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."')";
                            }
                        break;
                        case "sma_tax_rates":                        
                            $col1 = $col[0];
                            $col2 = $col[1];
                            $col3 = $col[2];
                            $col4 = $col[3];
                            $col5 = $col[4];
                            $col6 = $col[5];
                            $col7 = $col[6];
                            $col8 = $col[7];
                            if(isset($col[9]) && ($col[9]=='1' && (is_null($col[8])))){
                            
                                $query = "UPDATE `sma_tax_rates` SET `name`='".$col2."',`code`='".$col3."',`rate`='".$col4."',`type`='".$col5."',`org_id`='".$col6."',`wh_id`='".$col7."',`grp_id`='".$col8."' WHERE id='".$col1."'";
                            }else{
                                $query = "INSERT ignore INTO sma_tax_rates
                                (`id`, `name`, `code`, `rate`, `type`, `org_id`, `wh_id`, `grp_id`) 
                                VALUES
                                ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."')";                        
                            }
                        break;
                            case "sma_emrs":

                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                
                                if(isset($col[19]) && ($col[19]=='1' && (is_null($col[18])))){
                                
                                    $query = "UPDATE `sma_emrs` SET `supplier_id`='".$col9."',`emrs_no`='".$col10."',`emrs_dt`='".$col11."',`emrs_status`='".$col15."',`emrs_status_dt`='".$col16."',`remarks`='".$col17."',`emrs_mode`='".$col18."' WHERE id='".$col1."'";                                                            
                                }else{                               
                                    $query = "INSERT IGNORE INTO sma_emrs
                                    (`id`, `org_id`, `segment_id`, `warehouse_id`, `wh_id`, `fy_id`, `doc_id`, `emrs_type`, `supplier_id`, `emrs_no`, `emrs_dt`, `org_id_req_to`, `wh_id_required_to`, `reqd_dt`, `emrs_status`, `emrs_status_dt`, `remarks`, `emrs_mode`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."')";
                                }     
                               // echo $query;exit;                   
                            break;
                            case "sma_emrs_doc_src":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];

                                if(isset($col[9]) && ($col[9]=='1' && (is_null($col[8])))){
                                
                                    $query = "UPDATE `sma_emrs_doc_src` SET `doc_id`='".$col6."',`doc_no`='".$col7."',`doc_date`='".$col8."' WHERE id='".$col1."'";
                                }else{
                                    $query = "INSERT IGNORE INTO sma_emrs_doc_src
                                    (`id`, `org_id`, `segment_id`, `warehouse_id`, `wh_id`, `doc_id`, `doc_no`, `doc_date`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."')";
                                }
                            
                            break;
                            case "sma_emrs_item":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                if(isset($col[19]) && ($col[19]=='1' && (is_null($col[18])))){
                                
                                    $query = "UPDATE `sma_emrs_item` SET `item_id`='".$col8."',`req_qty`='".$col9."',`bal_qty`='".$col10."',`itm_uom_bs`='".$col11."',`uom_conv_fctr`='".$col12."',`req_qty_bs`='".$col13."',`itm_price_bs`='".$col14."',`itm_amt_bs`='".$col15."',`line_seq_no`='".$col16."',`code`='".$col17."',`status`='".$col18."' WHERE id='".$col1."'";
                                }else{

                                    $query = "INSERT IGNORE INTO sma_emrs_item
                                    (`id`, `org_id`, `segment_id`, `warehouse_id`, `wh_id`,`doc_id`, `doc_no_src_id`, `item_id`, `req_qty`, `bal_qty`, `itm_uom_bs`, `uom_conv_fctr`, `req_qty_bs`, `itm_price_bs`, `itm_amt_bs`, `line_seq_no`, `code`, `status`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."')";
                                }
                               
                                break;
                            case "sma_bin":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                             if(isset($col[16]) && ($col[16]=='1' && (is_null($col[15])))){  
                            
                                $query = "UPDATE `sma_bin` SET `bin_nm`='".$col7."',`bin_desc`='".$col8."',`storage_type`='".$col9."',`blocked`='".$col10."',`blk_resn`='".$col11."',`blk_dt_frm`='".$col12."',`blk_dt_to`='".$col13."',`bin_ent_id`='".$col14."',`create_date`='".$col15."' WHERE id='".$col1."'";
                            }else{
                                $query = "INSERT IGNORE INTO sma_bin
                                    (id,org_id,segment_id,warehouse_id,wh_id,bin_id,bin_nm,bin_desc,storage_type,blocked,blk_resn,blk_dt_frm,blk_dt_to,bin_ent_id,create_date) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."')";
                            }
                            break;
                            case "sma_categories":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                if(isset($col[10]) && ($col[10]=='1' && (is_null($col[9])))){  
                                    $query = "UPDATE `sma_categories` SET `code`='".$col2."',`name`='".$col3."',`parent_id`='".$col4."',`level`='".$col5."',`status`='".$col6."',`grp_id_parent`='".$col7."',`tax_rate_id`='".$col9."' WHERE id='".$col1."'";
                                }else{
                                    $query = "INSERT IGNORE INTO sma_categories
                                    (`id`, `code`, `name`, `parent_id`, `level`, `status`, `grp_id_parent`, `image`, `tax_rate_id`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."')";
                                }
                                break;
                            case "sma_companies":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];
                                $col22 = $col[21];
                                $col23 = $col[22];
                                $col24 = $col[23];
                                $col25 = $col[24];
                                $col26 = $col[25];
                                $col27 = $col[26];
                                $col28 = $col[27];
                                $col29 = $col[28];
                                $col30 = $col[29];
                                $col31 = $col[30];
                                $col32 = $col[31];
                                $col33 = $col[32];
                                $col34 = $col[33];
                                $col35 = $col[34];
                                $col36 = $col[35];
                                $col37 = $col[36];
                                $col38 = $col[37];
                                $col39 = $col[38];
                                $col40 = $col[39];
                                $col41 = $col[40];
                                $col42 = $col[41];
                                $col43 = $col[42];
                                $col44 = $col[43];
                                $col45 = $col[44];
                                $col46 = $col[45];
                                $col47 = $col[46];
                                $col48 = $col[47];
                                $col49 = $col[48];

                                $col50 = $col[49];
                                $col51 = $col[50];
                                $col52 = $col[51];
                                $col53 = $col[52];
                                $col54 = $col[53];
                                $col55 = $col[54];
                                

                                //if(isset($col[50]) && ($col[50]=='1' && (is_null($col[49])))){     
                                //if(isset($col[50]) && ($col[50]=='1')){   
                             if(($col50 ==1 && $col51==1 && isset($col54) ) || (isset($col54)) || ($col50 !=1 && $col51==1 && isset($col54))){   
                                  $query = "UPDATE `sma_companies` SET `name`='".$col6."',`vat_no`='".$col9."',`address`='".mysqli_real_escape_string($col10)."',`phone`='".$col17."',`email`='".$col18."',`gst_no`='".$col154."',`gst_effect_date`='".$col155."' WHERE id='".$col1."'";
                                }else{
                                    $select = "Select * from sma_companies where eo_id='".$col44."' AND eo_type='".$col43."'";
                                    $select_result = mysqli_query($connection, $select) or die("Selection Error " . mysqli_error($connection)); 
                                    $count = mysqli_num_rows($select_result);

                                   

                                    $select2 = "Select * from sma_companies where segment_id='".$col48."' AND loyalty_card_id='".$col21."' AND segment_id is not null AND loyalty_card_id!=''";
                                    $select_result2 = mysqli_query($connection, $select2) or die("Selection Error " . mysqli_error($connection)); 
                                    $count2 = mysqli_num_rows($select_result2);

                                    
                                    if($count==0 && $count2==0){
                                       $query = "INSERT INTO sma_companies
                                        (`group_id`, `group_name`, `customer_group_id`, `customer_group_name`, `name`, `father_name`, `company`, `vat_no`, `address`, `city`, `state`, `postal_code`, `country`, `country_id`, `state_id`, `phone`, `email`, `id_proof`, `id_proof_no`, `loyalty_card_id`, `nominees1`, `dob_nominees1`, `nominees2`, `dob_nominees2`, `nominees3`, `dob_nominees3`, `nominees4`, `dob_nominees4`, `nominees5`, `dob_nominees5`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `invoice_footer`, `payment_term`, `logo`, `award_points`, `upd_flg`, `eo_type`, `eo_id`, `org_id`, `wh_id`, `biller_id`, `segment_id`, `sync_flg`,`gst_no`,`gst_effect_date`) 
                                        VALUES
                                        ('".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".mysqli_real_escape_string($col10)."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col24."','".$col25."','".$col26."','".$col27."','".$col28."','".$col29."','".$col30."','".$col31."','".$col32."','".$col33."','".$col34."','".$col35."','".$col36."','".$col37."','".$col38."','".$col39."','".$col40."','".$col41."','".$col42."','".$col43."','".$col44."','".$col45."','".$col46."','".$col47."','".$col48."','".$col49."','".$col54."','".$col55."')";
                                    }
                            } 
                            break;
                            case "sma_currencies":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $query = "INSERT ignore INTO sma_currencies
                                (`id`, `code`, `name`, `rate`, `auto_update`, `curr_id`) 
                                VALUES
                                ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."')";
                                
                                break;
                                
                            case "sma_drft_po_dlv_schdl":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];
                                $col22 = $col[21];
                                $col23 = $col[22];
                                $col24 = $col[23];
                                $col25 = $col[24];
                                $col26 = $col[25];
                                $col27 = $col[26];
                                $col28 = $col[27];
                                
                                if((isset($col[29])) && ($col[29]==1)){
                                    $query = "UPDATE `sma_drft_po_dlv_schdl` SET `tot_qty`='".$col6."',`dlv_mode`='".$col7."',`dlv_qty`='".$col8."',`tlrnc_qty_type`='".$col9."',`tlrnc_qty_val`='".$col10."',`dlv_dt`='".$col11."',`dlv_adds_id`='".$col13."',`usr_id_create_dt`='".$col14."',`dlv_schdl_no`='".$col15."',`itm_uom_desc`='".$col16."',`tlrnc_days_val`='".$col19."',`use_rented_wh`='".$col28."' WHERE id='".$col1."'";
                                }else{
                                    $query = "INSERT IGNORE INTO sma_drft_po_dlv_schdl
                                    (`id`, `org_id`, `po_id`, `itm_id`, `itm_name`, `tot_qty`, `dlv_mode`, `dlv_qty`, `tlrnc_qty_type`, `tlrnc_qty_val`, `dlv_dt`, `wh_id`, `dlv_adds_id`, `usr_id_create_dt`, `dlv_schdl_no`, `itm_uom_desc`, `bal_qty`, `tmp_rcpt_qty`, `tlrnc_days_val`, `segment_id`, `warehouse_id`, `prj_id`, `ro_no`, `ro_dt`, `item_id`, `flg`, `status`, `use_rented_wh`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col24."','".$col25."','".$col26."','".$col27."','".$col28."')";
                                }
                            break;              
                            case "sma_drft_po":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];

                                if((isset($col[22])) && ($col[22]==1)){
                                    $query = "UPDATE `sma_drft_po` SET `po_dt`='".$col4."',`customer_id`='".$col5."',`bill_adds_id`='".$col6."',`tlrnc_days`='".$col7."',`curr_id_sp`='".$col8."',`curr_name`='".$col9."',`curr_conv_fctr`='".$col10."',`tlrnc_qty_type`='".$col11."',`tlrnc_qty_val`='".$col12."',`usr_id_create_dt`='".$col13."',`auth_po_no`='".$col14."',`fy_id`='".$col15."',`po_status`='".$col18."',`po_mode`='".$col19."',`doc_id`='".$col20."',`modify_date`='".date('Y-m-d H:i:s')."' WHERE id='".$col1."'";
                                }else{                               
                                    $query = "INSERT IGNORE INTO sma_drft_po
                                    (`id`, `org_id`, `po_id`, `po_dt`, `customer_id`, `bill_adds_id`, `tlrnc_days`, `curr_id_sp`, `curr_name`, `curr_conv_fctr`, `tlrnc_qty_type`, `tlrnc_qty_val`, `usr_id_create_dt`, `auth_po_no`, `fy_id`, `flg`, `status`, `po_status`, `po_mode`, `doc_id`, `modify_date`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."')";
                                }
                            break;
                            case "sma_cheque_details":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];

                                if((isset($col[17])) && ($col[17]==1)){
                                    $query = "UPDATE `sma_cheque_details` SET status='".$col12."', clearance_date='".$col14."' WHERE reference_no='".$col16."' AND id='".$col20."' AND sale_id='".$col19."'";
                                }                              
                              
                            break;
                            default:
                                $query="";
                        }
                        if($query!=''){
                            if($table=='sma_cheque_details'){
                              if($col12=='108'){
                                //AND reference_no='".$col16."'
                                $upd_query = "UPDATE sma_payments SET type='bounced' WHERE amount='".$col11."' AND reference_no='".$col16."'";
                               
                                $result = mysqli_query($connection, $upd_query) or die("Selection Error " . mysqli_error($connection));
                              }
                            }
                            $result = mysqli_query($connection, $query) or die("Selection Error " . mysqli_error($connection)); 
                        }
                    }
                    echo $$table;
                    fclose($handle);
                }
            }
           

               // transfer data
                $tmpdir = $_SERVER["DOCUMENT_ROOT"].'/'.PROJECT_NAME.'/tmp-folder/';
                $dateWiseTmpDir = $_SERVER["DOCUMENT_ROOT"].'/'.PROJECT_NAME.'/tmp-folder/'.$csv_folder.'/';
                
                if (!is_dir($tmpdir)) { //create the folder if it's not already exists
                    mkdir($tmpdir, 0755, TRUE);                
                }  
                if (!is_dir($dateWiseTmpDir)) {
                    mkdir($dateWiseTmpDir, 0755, TRUE);
                }         
                copy($fileName,$dateWiseTmpDir.$projName.".zip");
                unlink($fileName);
                //unlink($remote_file_url);
               // $this->site->rrmdir($remote_file_url);
                $this->site->rrmdir($fileName);
                $this->site->rrmdir($directory.$csv_folder."/".$projName);

                }
                }

            }
  /*           mysqli_query($connection, 'UPDATE sma_sales, (SELECT sma_users.id FROM sma_users LIMIT 1) src, (SELECT sma_warehouses.id FROM sma_warehouses WHERE sma_warehouses.name LIKE "%GODOWN%") src2 SET sma_sales.created_by = src.id,sma_sales.warehouse_id= src2.id WHERE 1') or die("Update Sales(user,warehouse) Master Error " . mysqli_error($connection));
  mysqli_query($connection, 'UPDATE sma_sale_items INNER JOIN sma_sales ON (sma_sales.id = sma_sale_items.sale_id) LEFT JOIN sma_products ON (sma_products.code = sma_sale_items.product_code AND sma_products.name = sma_sale_items.product_name) INNER JOIN sma_warehouses ON (sma_warehouses.id = sma_products.warehouse) SET sma_sale_items.product_id = sma_products.id, sma_sale_items.warehouse_id= sma_products.warehouse WHERE ((sma_products.code = sma_sale_items.product_code AND sma_products.name = sma_sale_items.product_name) AND sma_warehouses.name LIKE "%GODOWN%")') or die("Update Sales item(itmid,warehouse) Master Error " . mysqli_error($connection));
  mysqli_query($connection, 'UPDATE sma_sale_items,(SELECT sma_bin.id FROM sma_bin WHERE sma_bin.bin_nm LIKE "STORAGE" LIMIT 1) src SET sma_sale_items.bin_id = src.id WHERE (sma_sale_items.lot_no LIKE "LOT#00001")') or die("Update Sales item(binid) Master Error " . mysqli_error($connection));
  mysqli_query($connection, 'UPDATE sma_payments,(SELECT sma_users.id FROM sma_users LIMIT 1) src SET sma_payments.created_by = src.id') or die("Update Payments(userby) Master Error " . mysqli_error($connection));
  mysqli_query($connection, 'UPDATE sma_costing INNER JOIN sma_purchase_item123 ON (sma_costing.purchase_item_id = sma_purchase_item123.id) INNER JOIN sma_purchase_items ON (sma_purchase_items.product_code = sma_purchase_item123.product_code AND sma_purchase_items.product_name = sma_purchase_item123.product_name) INNER JOIN sma_warehouses ON (sma_warehouses.id = sma_purchase_items.warehouse_id)  SET sma_costing.product_id = sma_purchase_items.product_id,sma_costing.purchase_item_id = sma_purchase_items.id WHERE ((sma_purchase_items.product_code = sma_purchase_item123.product_code AND sma_purchase_items.product_name = sma_purchase_item123.product_name) AND sma_warehouses.name LIKE "%GODOWN%")') or die("Update costing(itemid) Master Error " . mysqli_error($connection));
  
  mysqli_query($connection, 'UPDATE sma_pos_register,(SELECT sma_users.id FROM sma_users LIMIT 1) src SET sma_pos_register.closed_by = src.id,sma_pos_register.user_id = src.id') or die("Update register(userby) Master Error " . mysqli_error($connection));*/


/*mysqli_query($connection, 'UPDATE sma_item_stk_profile a INNER JOIN sma_products p ON (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) INNER JOIN sma_product123 z ON (p.name = z.name AND p.code = z.code) INNER JOIN sma_item_stk_profile123 c ON (z.id = c.item_id AND z.warehouse = c.warehouse_id) INNER JOIN sma_warehouses w ON (w.id = p.warehouse) SET a.total_stk = c.total_stk, a.available_stk = c.available_stk WHERE (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) AND (w.name LIKE "%GODOWN%")') or die("Update stk profile(warehoue) Master Error " . mysqli_error($connection));


mysqli_query($connection, 'UPDATE sma_item_stk_lot a INNER JOIN sma_products p ON (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) INNER JOIN sma_product123 z ON (p.name = z.name AND p.code = z.code) INNER JOIN sma_item_stk_lot123 c ON (z.id = c.item_id AND z.warehouse = c.warehouse_id) INNER JOIN sma_warehouses w ON (w.id = p.warehouse) SET a.total_stk = c.total_stk WHERE (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) AND (w.name LIKE "%GODOWN%")') or die("Update stk profile(warehoue) Master Error " . mysqli_error($connection));

mysqli_query($connection, 'UPDATE sma_item_stk_lot_bin a INNER JOIN sma_products p ON (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) INNER JOIN sma_product123 z ON (p.name = z.name AND p.code = z.code) INNER JOIN sma_item_stk_lot_bin123 c ON (z.id = c.item_id AND z.warehouse = c.warehouse_id) INNER JOIN sma_warehouses w ON (w.id = p.warehouse) SET a.total_stk = c.total_stk WHERE (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) AND (w.name LIKE "%GODOWN%")') or die("Update stk profile(warehoue) Master Error " . mysqli_error($connection));*/


// mysqli_query($connection, 'UPDATE sma_item_stk_profile a INNER JOIN sma_products p ON (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) INNER JOIN sma_product123 z ON (p.name = z.name AND p.code = z.code) INNER JOIN sma_item_stk_profile123 c ON (z.id = c.item_id AND z.warehouse = c.warehouse_id) INNER JOIN sma_warehouses w ON (w.id = p.warehouse) SET a.total_stk = c.total_stk, a.available_stk = c.available_stk WHERE (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) AND (w.name LIKE "%GODOWN%")') or die("Update stk profile Error " . mysqli_error($connection));


// mysqli_query($connection, 'UPDATE sma_item_stk_lot a INNER JOIN sma_products p ON (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) INNER JOIN sma_product123 z ON (p.name = z.name AND p.code = z.code) INNER JOIN sma_item_stk_lot123 c ON (z.id = c.item_id AND z.warehouse = c.warehouse_id) INNER JOIN sma_warehouses w ON (w.id = p.warehouse) SET a.total_stk = c.total_stk WHERE (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) AND (w.name LIKE "%GODOWN%")') or die("Update stk lot Error " . mysqli_error($connection));

// mysqli_query($connection, 'UPDATE sma_item_stk_lot_bin a INNER JOIN sma_products p ON (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) INNER JOIN sma_product123 z ON (p.name = z.name AND p.code = z.code) INNER JOIN sma_item_stk_lot_bin123 c ON (z.id = c.item_id AND z.warehouse = c.warehouse_id) INNER JOIN sma_warehouses w ON (w.id = p.warehouse) SET a.total_stk = c.total_stk WHERE (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) AND (w.name LIKE "%GODOWN%")') or die("Update stk lot Error " . mysqli_error($connection));

// mysqli_query($connection, 'INSERT INTO sma_item_stk_profile (org_id,segment_id,warehouse_id,fy_id,item_id,item_uom_id,total_stk,available_stk,rejected_stk,rework_stk,ordered_stk,sales_price,created_date,code,wh_id) SELECT a.org_id,a.segment_id,a.warehouse_id,a.fy_id,a.item_id,a.item_uom_id,a.total_stk,a.available_stk, a.rejected_stk,a.rework_stk,a.ordered_stk,a.sales_price,a.created_date,a.code,a.id FROM sma_item_stk_profile123 a WHERE a.wh_id IS NULL OR a.wh_id = ""') or die("Insert stock profile error " . mysqli_error($connection));


// mysqli_query($connection, 'INSERT INTO sma_item_stk_lot(org_id,segment_id,warehouse_id,lot_no,item_stk_id,item_id,batch_no, itm_uom_id,total_stk,rejected_stk,mfg_date,exp_date,reworkable_stk,created_date,code,wh_id) SELECT a.org_id,a.segment_id,a.warehouse_id,a.lot_no,a.item_stk_id,a.item_id,a.batch_no, a.itm_uom_id,a.total_stk,a.rejected_stk,a.mfg_date,a.exp_date,a.reworkable_stk,a.created_date,a.code,a.id FROM sma_item_stk_lot123 a WHERE a.wh_id IS NULL OR a.wh_id = ""') or die("Insert stock lot error " . mysqli_error($connection));

// mysqli_query($connection, 'INSERT INTO sma_item_stk_lot_bin(org_id,segment_id,warehouse_id,item_id,lot_id,bin_id,itm_uom_id,total_stk,rejected_stk,reworkable_stk,created_date,code,wh_id) SELECT a.org_id,a.segment_id,a.warehouse_id,a.item_id,a.lot_id,a.bin_id,a.itm_uom_id,a.total_stk, a.rejected_stk,a.reworkable_stk,a.created_date,a.code,a.id FROM sma_item_stk_lot_bin123 a WHERE a.wh_id IS NULL OR a.wh_id = ""') or die("Insert stock lot error " . mysqli_error($connection));



// mysqli_query($connection, 'UPDATE sma_item_stk_lot a INNER JOIN sma_item_stk_profile b ON (b.wh_id = a.item_stk_id) SET a.item_stk_id = b.id WHERE b.wh_id = a.item_stk_id') or die("updation stock lot map id error " . mysqli_error($connection));


// mysqli_query($connection, 'UPDATE sma_item_stk_profile a SET a.wh_id = null WHERE a.wh_id NOT LIKE "%WH%"') or die("updation stock profile map id error" . mysqli_error($connection));
// mysqli_query($connection, 'UPDATE sma_item_stk_lot_bin a INNER JOIN sma_item_stk_lot b ON (b.wh_id = a.lot_id) SET a.lot_id = b.id WHERE b.wh_id = a.lot_id') or die("updation stock profile map id error" . mysqli_error($connection));



// mysqli_query($connection, 'UPDATE sma_item_stk_lot a SET a.wh_id = null WHERE a.wh_id NOT LIKE "%WH%"') or die("updation stock lot map id error" . mysqli_error($connection));

// mysqli_query($connection, 'UPDATE sma_item_stk_lot_bin a SET a.wh_id = null WHERE a.wh_id NOT LIKE "%WH%"') or die("updation stock bin map id error" . mysqli_error($connection));



// mysqli_query($connection, 'UPDATE sma_item_stk_profile a INNER JOIN sma_product123 b ON (a.item_id = b.id) INNER JOIN sma_products p ON (p.name = b.name AND p.code = b.code) INNER JOIN sma_warehouses w ON (w.id = p.warehouse) SET a.item_id = p.id WHERE ((a.item_id = b.id) AND w.name LIKE "%GODOWN%" AND (a.wh_id IS NULL OR a.wh_id = ""))') or die("updation stock profile map  2 id error" . mysqli_error($connection));

// mysqli_query($connection, 'UPDATE sma_item_stk_lot a INNER JOIN sma_product123 b ON (a.item_id = b.id) INNER JOIN sma_products p ON (p.name = b.name AND p.code = b.code) INNER JOIN sma_warehouses w ON (w.id = p.warehouse) SET a.item_id = p.id WHERE ((a.item_id = b.id) AND w.name LIKE "%GODOWN%" AND (a.wh_id IS NULL OR a.wh_id = ""))') or die("updation stock lot map 3 id error" . mysqli_error($connection));

// mysqli_query($connection, 'UPDATE sma_item_stk_lot_bin a INNER JOIN sma_product123 b ON (a.item_id = b.id) INNER JOIN sma_products p ON (p.name = b.name AND p.code = b.code) INNER JOIN sma_warehouses w ON (w.id = p.warehouse) SET a.item_id = p.id WHERE ((a.item_id = b.id) AND w.name LIKE "%GODOWN%" AND (a.wh_id IS NULL OR a.wh_id = ""))') or die("updation stock bin map 3 id error" . mysqli_error($connection));



 
//   mysqli_query($connection, 'Update sma_products p INNER JOIN sma_product123 b ON (p.name = b.name AND p.code = b.code) SET p.price = b.price WHERE ((p.name = b.name AND p.code = b.code) AND b.price>1)') or die("Update Product(price) Master Error " . mysqli_error($connection));
//   mysqli_query($connection, 'Update sma_purchase_items p INNER JOIN sma_purchase_item123 b ON (p.product_code = b.product_code AND p.product_name = b.product_name) SET p.net_unit_cost = b.net_unit_cost,p.item_tax = b.item_tax, p.tax = b.tax ,p.subtotal = b.subtotal , p.unit_cost= b.unit_cost WHERE ((p.product_code = b.product_code AND p.product_name = b.product_name) AND b.subtotal>1)') or die("Update Purchase(price) Master Error " . mysqli_error($connection));

//    mysqli_query($connection, 'update sma_drft_po_dlv_schdl as a inner join sma_drft_po_dlv_schdl123 as b on (a.itm_name=b.itm_name and a.ro_no=b.ro_no) set a.bal_qty=b.bal_qty,a.status=b.status where (a.itm_name=b.itm_name and a.ro_no=b.ro_no) AND b.status!=0 ') or die("Update Purchase(price) Master Error " . mysqli_error($connection));

//     mysqli_query($connection, 'update sma_drft_po as a inner join sma_drft_po123 as b on (a.po_id=b.po_id AND a.po_dt=b.po_dt) set a.po_status=b.po_status where (a.po_id=b.po_id AND a.po_dt=b.po_dt) and b.po_status=218 ') or die("Update Purchase(price) Master Error " . mysqli_error($connection));
  
//   /*mysqli_query($connection, 'UPDATE sma_drft_po_dlv_schdl, (SELECT sma_warehouses.id FROM sma_warehouses WHERE sma_warehouses.name LIKE "%GODOWN%") src SET sma_drft_po_dlv_schdl.warehouse_id = src.id WHERE 1') or die("Update Po Schdl(warehouse) Master Error " . mysqli_error($connection));
//   mysqli_query($connection, 'UPDATE sma_drft_po_dlv_schdl a INNER JOIN sma_products p ON (p.name = a.itm_name AND p.warehouse = a.warehouse_id) SET a.itm_id = p.id WHERE (p.name = a.itm_name AND p.warehouse = a.warehouse_id)') or die("Update Po Schdl(itmid) Master Error " . mysqli_error($connection));*/
//   mysqli_query($connection, 'UPDATE sma_mrn_receipt,(SELECT sma_users.id FROM sma_users LIMIT 1) src SET sma_mrn_receipt.user_id = src.id WHERE 1') or die("Update Mrn Receipt(usrid) Master Error " . mysqli_error($connection));
//   mysqli_query($connection, 'UPDATE sma_mrn_receipt, (SELECT sma_warehouses.id FROM sma_warehouses WHERE sma_warehouses.name LIKE "%GODOWN%") src SET sma_mrn_receipt.wh_id = src.id WHERE 1') or die("Update Mrn Receipt(whid) Master Error " . mysqli_error($connection));
//   mysqli_query($connection, 'UPDATE sma_mrn_rcpt_src, (SELECT sma_warehouses.id FROM sma_warehouses WHERE sma_warehouses.name LIKE "%GODOWN%") src SET sma_mrn_rcpt_src.wh_id = src.id WHERE 1') or die("Update Mrn ReceiptSRC(whid) Master Error " . mysqli_error($connection));
//   mysqli_query($connection, 'UPDATE sma_mrn_rcpt_src a INNER JOIN sma_drft_po_dlv_schdl b ON (a.ro_no = b.ro_no) SET a.po_id=b.po_id WHERE (a.ro_no = b.ro_no)') or die("Update Mrn ReceiptSRC(poid) Master Error " . mysqli_error($connection));
//     mysqli_query($connection, 'UPDATE sma_mrn_receipt a INNER JOIN sma_mrn_rcpt_src b ON (a.id = b.mrn_rcpt_id) INNER JOIN sma_drft_po_dlv_schdl c ON (b.ro_no = c.ro_no) INNER JOIN sma_drft_po d ON (d.id = c.po_id)  SET a.supplier_id=d.customer_id WHERE (a.id = b.mrn_rcpt_id)') or die("Update Mrn Receipt(cid) Master Error " . mysqli_error($connection));
//     mysqli_query($connection, 'UPDATE sma_mrn_receipt, (SELECT sma_companies.id FROM sma_companies WHERE sma_companies.name LIKE "%DEFAULT TRANSPORT" LIMIT 1) src SET sma_mrn_receipt.tp_id = src.id WHERE 1') or die("Update Mrn Receipt(tid) Master Error " . mysqli_error($connection));
//   /*mysqli_query($connection, 'UPDATE sma_mrn_rcpt_item, (SELECT sma_warehouses.id FROM sma_warehouses WHERE sma_warehouses.name LIKE "%GODOWN%") src SET sma_mrn_rcpt_item.wh_id = src.id WHERE 1') or die("Update Mrn Item(whid) Master Error " . mysqli_error($connection));
//   mysqli_query($connection, 'UPDATE sma_mrn_rcpt_item a INNER JOIN sma_drft_po_dlv_schdl b ON (a.po_id = b.po_id AND a.wh_id = b.warehouse_id) SET a.item_id = b.itm_id WHERE (a.po_id = b.po_id AND a.wh_id = b.warehouse_id)') or die("Update Mrn Item(itmid) Master Error " . mysqli_error($connection));
//   */
//  mysqli_query($connection, 'UPDATE sma_mrn_rcpt_item a INNER JOIN sma_mrn_rcpt_src z ON (z.id = a.mrn_rcpt_src_id) SET a.po_id= z.po_id WHERE (z.id = a.mrn_rcpt_src_id)') or die("Update Mrn Item(poid) Master Error " . mysqli_error($connection));
//     mysqli_query($connection, 'UPDATE sma_mrn_rcpt_item a INNER JOIN sma_product123 p ON (p.id = a.item_id AND p.warehouse = a.wh_id) INNER JOIN sma_products pn ON (pn.name = p.name AND pn.code = p.code) INNER JOIN sma_warehouses w ON (pn.warehouse = w.id) SET a.item_id= pn.id WHERE ((p.id = a.item_id AND p.warehouse = a.wh_id) AND w.name LIKE "%GODOWN%")') or die("Update Mrn Item(itmid) Master Error " . mysqli_error($connection));
//     mysqli_query($connection, 'UPDATE sma_mrn_rcpt_item, (SELECT sma_warehouses.id FROM sma_warehouses WHERE sma_warehouses.name LIKE "%GODOWN%") src SET sma_mrn_rcpt_item.wh_id = src.id WHERE 1') or die("Update Mrn Item(whid) Master Error " . mysqli_error($connection));
//   mysqli_query($connection, 'UPDATE sma_mrn_rcpt_item_lot a INNER JOIN sma_mrn_rcpt_item b ON (b.id = a.mrn_rcpt_item_id) SET a.item_id = b.item_id,a.wh_id = b.wh_id WHERE (b.id = a.mrn_rcpt_item_id)') or die("Update Mrn Item LOT(itmid,whid) Master Error " . mysqli_error($connection));
//   mysqli_query($connection, 'UPDATE sma_mrn_rcpt_lot_bin a INNER JOIN sma_mrn_rcpt_item_lot b ON (b.id = a.lot_id) SET a.item_id = b.item_id,a.wh_id = b.wh_id WHERE (b.id = a.lot_id)') or die("Update Mrn Item LOT Bin(itmid,whid) Master Error " . mysqli_error($connection));
//   mysqli_query($connection, 'UPDATE sma_mrn_rcpt_lot_bin, (SELECT sma_bin.id FROM sma_bin WHERE sma_bin.bin_nm LIKE "STORAGE" LIMIT 1) src SET sma_mrn_rcpt_lot_bin.bin_id = src.id WHERE (sma_mrn_rcpt_lot_bin.bin_id !=0)') or die("Update Mrn Item LOT Bin(binid) Master Error " . mysqli_error($connection));
 
//   mysqli_query($connection, 'INSERT INTO sma_companies (group_id,group_name,customer_group_id,customer_group_name,name,father_name,company,vat_no,address,city,state,postal_code,country,country_id,state_id,phone,email,id_proof,id_proof_no,loyalty_card_id,nominees1,dob_nominees1,upd_flg,eo_type,eo_id,org_id,wh_id,biller_id,segment_id,sync_flg) SELECT b.group_id,b.group_name,b.customer_group_id,b.customer_group_name,b.name,b.father_name,b.company,b.vat_no,b.address,b.city,b.state,b.postal_code,b.country,b.country_id,b.state_id,b.phone,b.email,b.id_proof,b.id_proof_no,b.loyalty_card_id,b.nominees1,b.dob_nominees1,b.upd_flg,b.eo_type,b.eo_id,b.org_id,b.wh_id,b.biller_id,b.segment_id,b.sync_flg FROM sma_company123 b WHERE (b.segment_id> 0 AND b.name!= "Walk-in Customer") group by b.phone') or die("Insert Companies(new record) Master Error " . mysqli_error($connection));
//   mysqli_query($connection, 'UPDATE sma_sales a INNER JOIN sma_companies b ON (a.customer = b.name) SET a.customer_id= b.id WHERE (a.customer = b.name)') or die("Update Sales(customerID) Master Error " . mysqli_error($connection));
            
            $this->session->set_flashdata('message', "Congratulations!!!! Data Synced Successfully");
            redirect('auth/logout');
          //  redirect('welcome');
        }
        $this->page_construct('syncin', $meta, $this->data);        
    }



    function syncinexisting(){          
        $directory = $_SERVER["DOCUMENT_ROOT"].'/'.PROJECT_NAME.'/sync-folder/';
        $meta = array('page_title' => lang('Sync Data from Central server'));
        $this->load->model('site');
        $segmentList = $this->site->getAllSegments();
        $this->data['segmentList'] = $segmentList;
        $segment_id = $_SESSION['segment_id'];

        if(isset($segment_id) && ($segment_id!='')){

            $remote_host = REMOTE_HOST;
            $remote_username = REMOTE_USERNAME;
            $remote_password = REMOTE_PWD;
            $remote_dbname = REMOTE_DB;

            /*$remote_host = 'localhost';
            $remote_username = 'root';
            $remote_password = '';
            $remote_dbname = 'iffco_final_live';*/

            $remote_connection = mysqli_connect($remote_host, $remote_username, $remote_password, $remote_dbname) or die("Connection Error " . mysqli_error($remote_connection));
            $host = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // open connection to mysql database
            $connection = mysqli_connect($host, $username, $password, $dbname) or die("Connection Error " . mysqli_error($connection));
            $sql = "select * from sma_csv_history where segment_id=".$segment_id." AND flag=0 ORDER BY created_date ASC";
            $csv_data = mysqli_query($remote_connection, $sql) or die("Selection Error " . mysqli_error($remote_connection));
            $csv_list = mysqli_fetch_array($csv_data, MYSQLI_ASSOC);
            
            mysqli_autocommit($remote_connection, FALSE);
            $files_list = array();
            
            mysqli_query($connection, 'DROP TABLE IF EXISTS sma_product123') or die("Drop product Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'DROP TABLE IF EXISTS sma_purchase_item123') or die("Drop Purchase Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'DROP TABLE IF EXISTS sma_drft_po_dlv_schdl123') or die("Drop Po Schedule Error " . mysqli_error($connection));
              mysqli_query($connection, 'DROP TABLE IF EXISTS sma_drft_po123') or die("Drop Po Schedule Error " . mysqli_error($connection));
              mysqli_query($connection, 'DROP TABLE IF EXISTS sma_company123') or die("Drop Company Error " . mysqli_error($connection));
              mysqli_query($connection, 'DROP TABLE IF EXISTS sma_item_stk_profile123') or die("Drop STKProfile Error " . mysqli_error($connection));
              mysqli_query($connection, 'DROP TABLE IF EXISTS sma_item_stk_lot123') or die("Drop STKLot Error " . mysqli_error($connection));
              mysqli_query($connection, 'DROP TABLE IF EXISTS sma_item_stk_lot_bin123') or die("Drop STKLotBin Error " . mysqli_error($connection));

              mysqli_query($connection, 'create table sma_product123 (select * from sma_products)') or die("Create Product Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'create table sma_purchase_item123 (select * from sma_purchase_items)') or die("Create Purchase Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'create table sma_drft_po_dlv_schdl123 (select * from sma_drft_po_dlv_schdl)') or die("Create PO Schedule Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'create table sma_drft_po123 (select * from sma_drft_po)') or die("Create PO Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'create table sma_item_stk_profile123 (select * from sma_item_stk_profile)') or die("Create PO Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'create table sma_item_stk_lot123 (select * from sma_item_stk_lot)') or die("Create LotMaster Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'create table sma_item_stk_lot_bin123 (select * from sma_item_stk_lot_bin)') or die("Create LotBin Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'create table sma_company123 (select * from sma_companies)') or die("Create Company123 Master Error " . mysqli_error($connection));
     
              mysqli_query($connection, 'TRUNCATE TABLE sma_warehouses') or die("TRUNCATE Wharehouse Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_users') or die("TRUNCATE User Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_products') or die("TRUNCATE Product Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_purchase_items') or die("TRUNCATE Purchase Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_tax_rates') or die("TRUNCATE Tax Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_bin') or die("TRUNCATE Bin Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_categories') or die("TRUNCATE Category Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_drft_po') or die("TRUNCATE Category Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_drft_po_dlv_schdl') or die("TRUNCATE Category Master Error " . mysqli_error($connection));
        
              mysqli_query($connection, 'TRUNCATE TABLE sma_item_stk_profile') or die("TRUNCATE profile table Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_item_stk_lot') or die("TRUNCATE profile lot table Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_item_stk_lot_bin') or die("TRUNCATE profile lot bin table Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_companies') or die("TRUNCATE Company Master Error " . mysqli_error($connection));
     
              mysqli_query($connection, 'TRUNCATE TABLE sma_emrs') or die("TRUNCATE Emrs Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_emrs_doc_src') or die("TRUNCATE Emrs DOC SRC Master Error " . mysqli_error($connection));
              mysqli_query($connection, 'TRUNCATE TABLE sma_emrs_item') or die("TRUNCATE Emrs Item Master Error " . mysqli_error($connection));

            foreach($csv_data as $rows){
                
            //   echo $row['file_name'];exit;
                $csv_date = explode("/",$rows['file_name']);
                $csv_folder = $csv_date[count($csv_date)-2];
                
                if (!is_dir($directory.$csv_folder)) { //create the folder if it's not already exists
                    mkdir($directory.$csv_folder, 0755, TRUE);                
                }  
                $copy = copy( $rows['file_name'], $directory.$csv_folder.'/'.$csv_date[count($csv_date)-1] );   

                /* Add notice for success/failure */
                if( !$copy ) {
                    echo "Failed to copy csv for date".$rows['created_date'];
                    //exit;
                }else{
                     $files_list[] = $directory.$csv_folder.'/'.$csv_date[count($csv_date)-1];
                     $q1 = "UPDATE sma_csv_history SET flag = '1' WHERE id='".$rows['id']."' ";
                    if(mysqli_query($remote_connection, $q1))
                    {
                     /* commit insert */
                        mysqli_commit($remote_connection);
                    }
                    else{
                         /* Rollback */
                        mysqli_rollback($remote_connection);

                    }
                }               
            }

            
            
            $sql = "select * from sma_segment where id=".$segment_id."";
            $seg_result = mysqli_query($connection, $sql) or die("Selection Error " . mysqli_error($connection));
            $seg_row = mysqli_fetch_array($seg_result, MYSQLI_ASSOC);
            $projName = explode(" ",$seg_row['proj_name'])[0]."_".$segment_id;
            
            

            if(count($files_list)>0){
                foreach($files_list as $fileName){
                    $zip = new ZipArchive;
                    $csv_date = explode("/",$fileName);
                    $csv_folder = $csv_date[count($csv_date)-2];
                    
                if ($zip->open($fileName) === TRUE) {
                    $csv_path = $directory.$csv_folder."/".$projName."/";
                    $zip->extractTo($csv_path);
                    $zip->close();
                 /*else {
                    echo 'Failed to extract zip file';
                    exit;
                }  */             
                // path where your CSV file is located
               // define('CSV_PATH',$_SERVER["DOCUMENT_ROOT"].'/'.PROJECT_NAME.'/sync-folder/'.$projName.'/');
                define('CSV_PATH',$csv_path);
                //echo "csv path".CSV_PATH;exit;
                // Count File

                $sql = "select count(*) as count from sma_sync_table";
                $result = mysqli_query($connection, $sql) or die("Selection Error " . mysqli_error($connection));
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                $num_table=$row['count'];
                for ($x = 1; $x <= $num_table; $x++){               
                    
                    $sql = "select * from sma_sync_table where id='$x'";
                    $result = mysqli_query($connection, $sql) or die("Selection Error " . mysqli_error($connection));
                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                    $table=$row['table_name'];
                    // fetch mysql table rows
                    $csv_filename = $table.".csv"; // to be changed
                    //$csv_filename ='sma_segment_2017-02-05.csv';
                    // Name of your CSV file
                    $csv_file = $csv_path . $csv_filename;
                   //  echo $csv_file;exit;  
                    if (($handle = fopen($csv_file, "r")) !== FALSE) {
                        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        $col = array();
                        $num = count($data);
                        for ($c=0; $c < $num; $c++) {
                           $col[$c] = $data[$c];
                        }
                        /*if($table=='sma_tax_rates'){
                            echo "<pre>".$csv_filename;exit;
                            print_r($col);exit;
                        }*/
                         
                        switch ($table) {
                            case "sma_segment":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                if(isset($col[7]) && ($col[7]=='1' && (is_null($col[6])))){
                                
                                    $query = "UPDATE sma_segment SET `proj_name`='".$col5."',`prj_actv`= '".$col6."' WHERE id=".$col1."";
                                }else{
                                    $query = "INSERT ignore INTO sma_segment
                                    (id,org_id,prj_doc_id,parent_proj_id,proj_name,prj_actv) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."')";
                                }
                                break;
                            case "sma_warehouses":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                               if(isset($col[20]) && ($col[20]=='1' && (is_null($col[19])))){
                                
                                    $query = "UPDATE `sma_warehouses` SET `wh_type`='".$col4."',`name`='".$col7."',`wh_onrshp_type`='".$col8."',`wh_strg_type`='".$col9."',`adds_id`='".$col10."',`wh_desc`='".$col11."',`actv`='".$col12."',`segment_id`='".$col17."',`address`='".$col19."' WHERE id='".$col1."'";
                                }else{

                                    $query = "INSERT ignore INTO sma_warehouses
                                    (id,org_id,wh_id,wh_type,usr_id_create,usr_id_create_dt,name,wh_onrshp_type,wh_strg_type,adds_id,wh_desc,actv,inactv_resn,inactv_dt,wh_ent_id,prj_id,segment_id,biller_id,address) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."')";
                                }
                                break;
                             case "sma_users":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];
                                $col22 = $col[21];
                                $col23 = $col[22];
                                $col24 = $col[23];
                                $col25 = $col[24];
                                $col26 = $col[25];
                                $col27 = $col[26];
                                $col28 = $col[27];
                                $col29 = $col[28];
                                $col30 = $col[29];
                                $col31 = $col[30];
                                $col32 = $col[31];
                                $col33 = $col[32];
                                if(isset($col[34]) && ($col[34]=='1' && (is_null($col[33])))){
                               
                                    $query = "UPDATE `sma_users` SET `email`='".$col7."',`active`='".$col14."',`first_name`='".$col15."',`last_name`='".$col16."',`company`='".$col17."',`phone`='".$col18."',`gender`='".$col20."',`warehouse_id`='".$col22."',`segment_id`='".$col23."' WHERE id='".$col1."'";
                                }else{
                                    $query = "INSERT ignore INTO sma_users
                                    (`id`, `last_ip_address`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `avatar`, `gender`, `group_id`, `warehouse_id`, `segment_id`, `biller_id`, `company_id`, `show_cost`, `show_price`, `award_points`, `show_discount`, `upd_flg`, `usr_id`, `pwid`, `org_id`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col24."','".$col25."','".$col26."','".$col27."','".$col28."','".$col29."','".$col30."','".$col31."','".$col32."','".$col33."')";
                                }
                                break;
                            case "sma_products":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];
                                $col22 = $col[21];
                                $col23 = $col[22];
                                $col24 = $col[23];
                                $col25 = $col[24];
                                $col26 = $col[25];
                                $col27 = $col[26];
                                $col28 = $col[27];
                                $col29 = $col[28];
                                $col30 = $col[29];
                                $col31 = $col[30];
                                $col32 = $col[31];
                                $col33 = $col[32];
                                $col34 = $col[33];
                                $col35 = $col[34];
                                $col36 = $col[35];
                                $col37 = $col[36];
                                $col38 = $col[37];
                                $col39 = $col[38];
                                $col40 = $col[39];
                                $col41 = $col[40];
                                $col42 = $col[41];
                                $col43 = $col[42];
                                $col44 = $col[43];
                                $col45 = $col[44];
                                $col46 = $col[45];
                                $col47 = $col[46];
                                if(isset($col[48]) && ($col[48]=='1' && (is_null($col[47])))){
                                
                                    $query = "UPDATE `sma_products` SET `name`='".$col3."',`unit`='".$col4."',`cost`='".$col5."',`price`='".$col6."',`alert_quantity`='".$col7."',`category_id`='".$col9."',`subcategory_id`='".$col10."',`quantity`='".$col17."',`tax_rate`='".$col18."',`track_quantity`='".$col19."',`details`='".$col20."',`warehouse`='".$col21."',`barcode_symbology`='".$col22."',`product_details`='".$col24."',`tax_method`='".$col25."',`serialized`='".$col38."',`serial_number`='".$col39."',`grp_id`='".$col44."' WHERE id='".$col1."'";

                                }else{
                                    $query = "INSERT ignore INTO sma_products
                                    (`id`, `code`, `name`, `unit`, `cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `barcode_symbology`, `file`, `product_details`, `tax_method`, `type`, `supplier1`, `supplier1price`, `supplier2`, `supplier2price`, `supplier3`, `supplier3price`, `supplier4`, `supplier4price`, `supplier5`, `supplier5price`, `max_discount`, `serialized`, `serial_number`, `lot_no`, `sr_no`, `org_id`, `wh_id`, `grp_id`, `trsfr_flg`, `tupd_flg`, `psale`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col24."','".$col25."','".$col26."','".$col27."','".$col28."','".$col29."','".$col30."','".$col31."','".$col32."','".$col33."','".$col34."','".$col35."','".$col36."','".$col37."','".$col38."','".$col39."','".$col40."','".$col41."','".$col42."','".$col43."','".$col44."','".$col45."','".$col46."','".$col47."')";
                                }
                              //  echo $query;exit;
                            break;
                            case "sma_purchase_items":
                                
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];
                                $col22 = $col[21];
                                $col23 = $col[22];
                                $col24 = $col[23];
                                $col25 = $col[24];
                                $col26 = $col[25];
                                $col27 = $col[26];
                                $col28 = $col[27];
                                $col29 = $col[28];
                                $col30 = $col[29];
                                $col31 = $col[30];
                                $col32 = $col[31];
                                $col33 = $col[32];
                                $col34 = $col[33];
                                $col35 = $col[34];
                                $col36 = $col[35];
                                $col37 = $col[36];
                                $col38 = $col[37];
                                $col39 = $col[38];
                                $col40 = $col[39];
                                $col41 = $col[40];
                                if(isset($col[42]) && ($col[42]=='1' && (is_null($col[41])))){
                                
                                    $query = "UPDATE `sma_purchase_items` SET `purchase_id`='".$col2."',`transfer_id`='".$col3."',`category_id`='".$col4."',`product_id`='".$col5."',`product_code`='".$col6."',`product_name`='".$col7."',`itm_desc`='".$col8."',`long_desc`='".$col9."',`option_id`='".$col10."',`net_unit_cost`='".$col11."',`quantity`='".$col12."',`alert_quantity`='".$col13."',`track_quantity`='".$col14."',`type`='".$col15."',`warehouse_id`='".$col16."',`taxable`='".$col18."',`attribute`='".$col19."',`item_tax`='".$col20."',`tax_rate`='".$col21."',`tax_method`='".$col22."',`tax_rate_id`='".$col23."',`tax`='".$col24."',`discount`='".$col25."',`item_discount`='".$col26."',`expiry`='".$col27."',`subtotal`='".$col28."',`quantity_balance`='".$col29."',`is_serialized`='".$col31."',`status`='".$col33."',`unit_cost`='".$col34."',`real_unit_cost`='".$col35."' WHERE warehouse_id='".$col16."' AND product_code='".$col6."'";
                                }else{
                                    $query = "INSERT ignore INTO sma_purchase_items
                                    (`id`, `purchase_id`, `transfer_id`, `category_id`, `product_id`, `product_code`, `product_name`, `itm_desc`, `long_desc`, `option_id`, `net_unit_cost`, `quantity`, `alert_quantity`, `track_quantity`, `type`, `warehouse_id`, `image`, `taxable`, `attribute`, `item_tax`, `tax_rate`, `tax_method`, `tax_rate_id`, `tax`, `discount`, `item_discount`, `expiry`, `subtotal`, `quantity_balance`, `date`, `is_serialized`, `user_id`, `status`, `unit_cost`, `real_unit_cost`, `lot_no`, `sr_no`, `org_id`, `wh_id`, `grp_id`, `psale`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col24."','".$col25."','".$col26."','".$col27."','".$col28."','".$col29."','".$col30."','".$col31."','".$col32."','".$col33."','".$col34."','".$col35."','".$col36."','".$col37."','".$col38."','".$col39."','".$col40."','".$col41."')";
                                }

                            break;
                            
                            case "sma_item_stk_profile":
                            $col1 = $col[0];
                            $col2 = $col[1];
                            $col3 = $col[2];
                            $col4 = $col[3];
                            $col5 = $col[4];
                            $col6 = $col[5];
                            $col7 = $col[6];
                            $col8 = $col[7];
                            $col9 = $col[8];
                            $col10 = $col[9];
                            $col11 = $col[10];
                            $col12 = $col[11];
                            $col13 = $col[12];
                            $col14 = $col[13];
                            $col15 = $col[14];
                            $col16 = $col[15];
                            $col17 = $col[16];
                            $col18 = $col[17];
                            $col19 = $col[18];
                            if(isset($col[20]) && ($col[20]=='1' && (is_null($col[19])))){
                           
                                $query = "UPDATE `sma_item_stk_profile` SET `fy_id`='".$col6."',`item_uom_id`='".$col8."',`available_stk`='".$col10."',`rejected_stk`='".$col11."',`purchase_price`='".$col14."',`sales_price`='".$col15."' WHERE id='".$col1."'";
                            }else{

                            $query = "INSERT ignore INTO sma_item_stk_profile
                            (`id`, `org_id`, `segment_id`, `warehouse_id`, `wh_id`, `fy_id`, `item_id`, `item_uom_id`, `total_stk`, `available_stk`, `rejected_stk`, `rework_stk`, `ordered_stk`, `purchase_price`, `sales_price`, `created_date`, `code`, `create_flg`, `upd_flg`) 
                            VALUES
                            ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."')";
                        }
                        break;
                        case "sma_item_stk_lot":
                            $col1 = $col[0];
                            $col2 = $col[1];
                            $col3 = $col[2];
                            $col4 = $col[3];
                            $col5 = $col[4];
                            $col6 = $col[5];
                            $col7 = $col[6];
                            $col8 = $col[7];
                            $col9 = $col[8];
                            $col10 = $col[9];
                            $col11 = $col[10];
                            $col12 = $col[11];
                            $col13 = $col[12];
                            $col14 = $col[13];
                            $col15 = $col[14];
                            $col16 = $col[15];
                            $col17 = $col[16];
                            $col18 = $col[17];
                            $col19 = $col[18];
                            if(isset($col[20]) && ($col[20]=='1' && (is_null($col[19])))){
                            
                                $query = "UPDATE `sma_item_stk_lot` SET `batch_no`='".$col9."',`total_stk`='".$col11."',`rejected_stk`='".$col12."',`mfg_date`='".$col13."',`exp_date`='".$col14."' WHERE id='".$col1."'";
                            }else{
                                $query = "INSERT ignore INTO sma_item_stk_lot
                                (`id`, `org_id`, `segment_id`, `warehouse_id`, `wh_id`, `lot_no`, `item_stk_id`, `item_id`, `batch_no`, `itm_uom_id`, `total_stk`, `rejected_stk`, `mfg_date`, `exp_date`, `reworkable_stk`, `created_date`, `code`, `create_flg`, `upd_flg`) 
                                VALUES
                                ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."')";
                            }
                        break;
                        case "sma_item_stk_lot_bin":
                            $col1 = $col[0];
                            $col2 = $col[1];
                            $col3 = $col[2];
                            $col4 = $col[3];
                            $col5 = $col[4];
                            $col6 = $col[5];
                            $col7 = $col[6];
                            $col8 = $col[7];
                            $col9 = $col[8];
                            $col10 = $col[9];
                            $col11 = $col[10];
                            $col12 = $col[11];
                            $col13 = $col[12];
                            $col14 = $col[13];
                            $col15 = $col[14];
                            $col16 = $col[15];
                            
                            if(isset($col[17]) && ($col[17]=='1' && (is_null($col[16])))){
                          //  if((isset($col[17])) && ($col[17]==1)){
                                $query = "UPDATE `sma_item_stk_lot_bin` SET `lot_id`='".$col7."',`bin_id`='".$col8."',`itm_uom_id`='".$col9."',`total_stk`='".$col10."',`rejected_stk`='".$col11."' WHERE id='".$col1."'";
                            }else{
                                $query = "INSERT ignore INTO sma_item_stk_lot_bin
                                (`id`, `org_id`, `segment_id`, `warehouse_id`, `wh_id`, `item_id`, `lot_id`, `bin_id`, `itm_uom_id`, `total_stk`, `rejected_stk`, `reworkable_stk`, `created_date`, `code`, `create_flg`, `upd_flg`) 
                                VALUES
                                ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."')";
                            }
                        break;
                        case "sma_tax_rates":                        
                            $col1 = $col[0];
                            $col2 = $col[1];
                            $col3 = $col[2];
                            $col4 = $col[3];
                            $col5 = $col[4];
                            $col6 = $col[5];
                            $col7 = $col[6];
                            $col8 = $col[7];
                            if(isset($col[9]) && ($col[9]=='1' && (is_null($col[8])))){
                            
                                $query = "UPDATE `sma_tax_rates` SET `name`='".$col2."',`code`='".$col3."',`rate`='".$col4."',`type`='".$col5."',`org_id`='".$col6."',`wh_id`='".$col7."',`grp_id`='".$col8."' WHERE id='".$col1."'";
                            }else{
                                $query = "INSERT ignore INTO sma_tax_rates
                                (`id`, `name`, `code`, `rate`, `type`, `org_id`, `wh_id`, `grp_id`) 
                                VALUES
                                ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."')";                        
                            }
                        break;
                            case "sma_emrs":

                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                
                                if(isset($col[19]) && ($col[19]=='1' && (is_null($col[18])))){
                                
                                    $query = "UPDATE `sma_emrs` SET `supplier_id`='".$col9."',`emrs_no`='".$col10."',`emrs_dt`='".$col11."',`emrs_status`='".$col15."',`emrs_status_dt`='".$col16."',`remarks`='".$col17."',`emrs_mode`='".$col18."' WHERE id='".$col1."'";                                                            
                                }else{                               
                                    $query = "INSERT IGNORE INTO sma_emrs
                                    (`id`, `org_id`, `segment_id`, `warehouse_id`, `wh_id`, `fy_id`, `doc_id`, `emrs_type`, `supplier_id`, `emrs_no`, `emrs_dt`, `org_id_req_to`, `wh_id_required_to`, `reqd_dt`, `emrs_status`, `emrs_status_dt`, `remarks`, `emrs_mode`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."')";
                                }     
                               // echo $query;exit;                   
                            break;
                            case "sma_emrs_doc_src":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];

                                if(isset($col[9]) && ($col[9]=='1' && (is_null($col[8])))){
                                
                                    $query = "UPDATE `sma_emrs_doc_src` SET `doc_id`='".$col6."',`doc_no`='".$col7."',`doc_date`='".$col8."' WHERE id='".$col1."'";
                                }else{
                                    $query = "INSERT IGNORE INTO sma_emrs_doc_src
                                    (`id`, `org_id`, `segment_id`, `warehouse_id`, `wh_id`, `doc_id`, `doc_no`, `doc_date`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."')";
                                }
                            
                            break;
                            case "sma_emrs_item":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                if(isset($col[19]) && ($col[19]=='1' && (is_null($col[18])))){
                                
                                    $query = "UPDATE `sma_emrs_item` SET `item_id`='".$col8."',`req_qty`='".$col9."',`bal_qty`='".$col10."',`itm_uom_bs`='".$col11."',`uom_conv_fctr`='".$col12."',`req_qty_bs`='".$col13."',`itm_price_bs`='".$col14."',`itm_amt_bs`='".$col15."',`line_seq_no`='".$col16."',`code`='".$col17."',`status`='".$col18."' WHERE id='".$col1."'";
                                }else{

                                    $query = "INSERT IGNORE INTO sma_emrs_item
                                    (`id`, `org_id`, `segment_id`, `warehouse_id`, `wh_id`,`doc_id`, `doc_no_src_id`, `item_id`, `req_qty`, `bal_qty`, `itm_uom_bs`, `uom_conv_fctr`, `req_qty_bs`, `itm_price_bs`, `itm_amt_bs`, `line_seq_no`, `code`, `status`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."')";
                                }
                               
                                break;
                            case "sma_bin":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                             if(isset($col[16]) && ($col[16]=='1' && (is_null($col[15])))){  
                            
                                $query = "UPDATE `sma_bin` SET `bin_nm`='".$col7."',`bin_desc`='".$col8."',`storage_type`='".$col9."',`blocked`='".$col10."',`blk_resn`='".$col11."',`blk_dt_frm`='".$col12."',`blk_dt_to`='".$col13."',`bin_ent_id`='".$col14."',`create_date`='".$col15."' WHERE id='".$col1."'";
                            }else{
                                $query = "INSERT IGNORE INTO sma_bin
                                    (id,org_id,segment_id,warehouse_id,wh_id,bin_id,bin_nm,bin_desc,storage_type,blocked,blk_resn,blk_dt_frm,blk_dt_to,bin_ent_id,create_date) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."')";
                            }
                            break;
                            case "sma_categories":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                if(isset($col[10]) && ($col[10]=='1' && (is_null($col[9])))){  
                                    $query = "UPDATE `sma_categories` SET `code`='".$col2."',`name`='".$col3."',`parent_id`='".$col4."',`level`='".$col5."',`status`='".$col6."',`grp_id_parent`='".$col7."',`tax_rate_id`='".$col9."' WHERE id='".$col1."'";
                                }else{
                                    $query = "INSERT IGNORE INTO sma_categories
                                    (`id`, `code`, `name`, `parent_id`, `level`, `status`, `grp_id_parent`, `image`, `tax_rate_id`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."')";
                                }
                                break;
                            case "sma_companies":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];
                                $col22 = $col[21];
                                $col23 = $col[22];
                                $col24 = $col[23];
                                $col25 = $col[24];
                                $col26 = $col[25];
                                $col27 = $col[26];
                                $col28 = $col[27];
                                $col29 = $col[28];
                                $col30 = $col[29];
                                $col31 = $col[30];
                                $col32 = $col[31];
                                $col33 = $col[32];
                                $col34 = $col[33];
                                $col35 = $col[34];
                                $col36 = $col[35];
                                $col37 = $col[36];
                                $col38 = $col[37];
                                $col39 = $col[38];
                                $col40 = $col[39];
                                $col41 = $col[40];
                                $col42 = $col[41];
                                $col43 = $col[42];
                                $col44 = $col[43];
                                $col45 = $col[44];
                                $col46 = $col[45];
                                $col47 = $col[46];
                                $col48 = $col[47];
                                $col49 = $col[48];

                                if(isset($col[50]) && ($col[50]=='1' && (is_null($col[49])))){                                   
                                    $query = "UPDATE `sma_companies` SET `name`='".$col6."',`vat_no`='".$col9."',`address`='".$col10."',`phone`='".$col17."',`email`='".$col18."' WHERE id='".$col1."'";
                                }else{
                                    $query = "INSERT IGNORE INTO sma_companies
                                    (`id`, `group_id`, `group_name`, `customer_group_id`, `customer_group_name`, `name`, `father_name`, `company`, `vat_no`, `address`, `city`, `state`, `postal_code`, `country`, `country_id`, `state_id`, `phone`, `email`, `id_proof`, `id_proof_no`, `loyalty_card_id`, `nominees1`, `dob_nominees1`, `nominees2`, `dob_nominees2`, `nominees3`, `dob_nominees3`, `nominees4`, `dob_nominees4`, `nominees5`, `dob_nominees5`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `invoice_footer`, `payment_term`, `logo`, `award_points`, `upd_flg`, `eo_type`, `eo_id`, `org_id`, `wh_id`, `biller_id`, `segment_id`, `sync_flg`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col24."','".$col25."','".$col26."','".$col27."','".$col28."','".$col29."','".$col30."','".$col31."','".$col32."','".$col33."','".$col34."','".$col35."','".$col36."','".$col37."','".$col38."','".$col39."','".$col40."','".$col41."','".$col42."','".$col43."','".$col44."','".$col45."','".$col46."','".$col47."','".$col48."','".$col49."')";
                            }
                            break;
                            case "sma_currencies":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $query = "INSERT ignore INTO sma_currencies
                                (`id`, `code`, `name`, `rate`, `auto_update`, `curr_id`) 
                                VALUES
                                ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."')";
                                
                                break;
                                
                            case "sma_drft_po_dlv_schdl":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];
                                $col22 = $col[21];
                                $col23 = $col[22];
                                $col24 = $col[23];
                                $col25 = $col[24];
                                $col26 = $col[25];
                                $col27 = $col[26];
                                $col28 = $col[27];
                                
                                if((isset($col[29])) && ($col[29]==1)){
                                    $query = "UPDATE `sma_drft_po_dlv_schdl` SET `tot_qty`='".$col6."',`dlv_mode`='".$col7."',`dlv_qty`='".$col8."',`tlrnc_qty_type`='".$col9."',`tlrnc_qty_val`='".$col10."',`dlv_dt`='".$col11."',`dlv_adds_id`='".$col13."',`usr_id_create_dt`='".$col14."',`dlv_schdl_no`='".$col15."',`itm_uom_desc`='".$col16."',`tlrnc_days_val`='".$col19."',`use_rented_wh`='".$col28."' WHERE id='".$col1."'";
                                }else{
                                    $query = "INSERT IGNORE INTO sma_drft_po_dlv_schdl
                                    (`id`, `org_id`, `po_id`, `itm_id`, `itm_name`, `tot_qty`, `dlv_mode`, `dlv_qty`, `tlrnc_qty_type`, `tlrnc_qty_val`, `dlv_dt`, `wh_id`, `dlv_adds_id`, `usr_id_create_dt`, `dlv_schdl_no`, `itm_uom_desc`, `bal_qty`, `tmp_rcpt_qty`, `tlrnc_days_val`, `segment_id`, `warehouse_id`, `prj_id`, `ro_no`, `ro_dt`, `item_id`, `flg`, `status`, `use_rented_wh`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col24."','".$col25."','".$col26."','".$col27."','".$col28."')";
                                }
                            break;              
                            case "sma_drft_po":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];

                                if((isset($col[22])) && ($col[22]==1)){
                                    $query = "UPDATE `sma_drft_po` SET `po_dt`='".$col4."',`customer_id`='".$col5."',`bill_adds_id`='".$col6."',`tlrnc_days`='".$col7."',`curr_id_sp`='".$col8."',`curr_name`='".$col9."',`curr_conv_fctr`='".$col10."',`tlrnc_qty_type`='".$col11."',`tlrnc_qty_val`='".$col12."',`usr_id_create_dt`='".$col13."',`auth_po_no`='".$col14."',`fy_id`='".$col15."',`po_status`='".$col18."',`po_mode`='".$col19."',`doc_id`='".$col20."',`modify_date`='".date('Y-m-d H:i:s')."' WHERE id='".$col1."'";
                                }else{                               
                                    $query = "INSERT IGNORE INTO sma_drft_po
                                    (`id`, `org_id`, `po_id`, `po_dt`, `customer_id`, `bill_adds_id`, `tlrnc_days`, `curr_id_sp`, `curr_name`, `curr_conv_fctr`, `tlrnc_qty_type`, `tlrnc_qty_val`, `usr_id_create_dt`, `auth_po_no`, `fy_id`, `flg`, `status`, `po_status`, `po_mode`, `doc_id`, `modify_date`) 
                                    VALUES
                                    ('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."')";
                                }
                            break;
                            default:
                                $query="";
                        }
                        if($query!=''){
                            $result = mysqli_query($connection, $query) or die("Selection Error " . mysqli_error($connection)); 
                        }
                    }
                    echo $$table;
                    fclose($handle);
                }
            }
 
                $tmpdir = $_SERVER["DOCUMENT_ROOT"].'/'.PROJECT_NAME.'/tmp-folder/';
                $dateWiseTmpDir = $_SERVER["DOCUMENT_ROOT"].'/'.PROJECT_NAME.'/tmp-folder/'.$csv_folder.'/';
                
                if (!is_dir($tmpdir)) { //create the folder if it's not already exists
                    mkdir($tmpdir, 0755, TRUE);                
                }  
                if (!is_dir($dateWiseTmpDir)) {
                    mkdir($dateWiseTmpDir, 0755, TRUE);
                }         
                copy($fileName,$dateWiseTmpDir.$projName.".zip");
                unlink($fileName);
                //unlink($remote_file_url);
               // $this->site->rrmdir($remote_file_url);
                $this->site->rrmdir($fileName);
                $this->site->rrmdir($directory.$csv_folder."/".$projName);

                }
                }

            }
             mysqli_query($connection, 'UPDATE sma_sales, (SELECT sma_users.id FROM sma_users LIMIT 1) src, (SELECT sma_warehouses.id FROM sma_warehouses WHERE sma_warehouses.name LIKE "%GODOWN%") src2 SET sma_sales.created_by = src.id,sma_sales.warehouse_id= src2.id WHERE 1') or die("Update Sales(user,warehouse) Master Error " . mysqli_error($connection));
  mysqli_query($connection, 'UPDATE sma_sale_items INNER JOIN sma_sales ON (sma_sales.id = sma_sale_items.sale_id) LEFT JOIN sma_products ON (sma_products.code = sma_sale_items.product_code AND sma_products.name = sma_sale_items.product_name) INNER JOIN sma_warehouses ON (sma_warehouses.id = sma_products.warehouse) SET sma_sale_items.product_id = sma_products.id, sma_sale_items.warehouse_id= sma_products.warehouse WHERE ((sma_products.code = sma_sale_items.product_code AND sma_products.name = sma_sale_items.product_name) AND sma_warehouses.name LIKE "%GODOWN%")') or die("Update Sales item(itmid,warehouse) Master Error " . mysqli_error($connection));
  mysqli_query($connection, 'UPDATE sma_sale_items,(SELECT sma_bin.id FROM sma_bin WHERE sma_bin.bin_nm LIKE "STORAGE" LIMIT 1) src SET sma_sale_items.bin_id = src.id WHERE (sma_sale_items.lot_no LIKE "LOT#00001")') or die("Update Sales item(binid) Master Error " . mysqli_error($connection));
  mysqli_query($connection, 'UPDATE sma_payments,(SELECT sma_users.id FROM sma_users LIMIT 1) src SET sma_payments.created_by = src.id') or die("Update Payments(userby) Master Error " . mysqli_error($connection));
  mysqli_query($connection, 'UPDATE sma_costing INNER JOIN sma_purchase_item123 ON (sma_costing.purchase_item_id = sma_purchase_item123.id) INNER JOIN sma_purchase_items ON (sma_purchase_items.product_code = sma_purchase_item123.product_code AND sma_purchase_items.product_name = sma_purchase_item123.product_name) INNER JOIN sma_warehouses ON (sma_warehouses.id = sma_purchase_items.warehouse_id)  SET sma_costing.product_id = sma_purchase_items.product_id,sma_costing.purchase_item_id = sma_purchase_items.id WHERE ((sma_purchase_items.product_code = sma_purchase_item123.product_code AND sma_purchase_items.product_name = sma_purchase_item123.product_name) AND sma_warehouses.name LIKE "%GODOWN%")') or die("Update costing(itemid) Master Error " . mysqli_error($connection));
  
  mysqli_query($connection, 'UPDATE sma_pos_register,(SELECT sma_users.id FROM sma_users LIMIT 1) src SET sma_pos_register.closed_by = src.id,sma_pos_register.user_id = src.id') or die("Update register(userby) Master Error " . mysqli_error($connection));


/*mysqli_query($connection, 'UPDATE sma_item_stk_profile a INNER JOIN sma_products p ON (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) INNER JOIN sma_product123 z ON (p.name = z.name AND p.code = z.code) INNER JOIN sma_item_stk_profile123 c ON (z.id = c.item_id AND z.warehouse = c.warehouse_id) INNER JOIN sma_warehouses w ON (w.id = p.warehouse) SET a.total_stk = c.total_stk, a.available_stk = c.available_stk WHERE (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) AND (w.name LIKE "%GODOWN%")') or die("Update stk profile(warehoue) Master Error " . mysqli_error($connection));


mysqli_query($connection, 'UPDATE sma_item_stk_lot a INNER JOIN sma_products p ON (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) INNER JOIN sma_product123 z ON (p.name = z.name AND p.code = z.code) INNER JOIN sma_item_stk_lot123 c ON (z.id = c.item_id AND z.warehouse = c.warehouse_id) INNER JOIN sma_warehouses w ON (w.id = p.warehouse) SET a.total_stk = c.total_stk WHERE (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) AND (w.name LIKE "%GODOWN%")') or die("Update stk profile(warehoue) Master Error " . mysqli_error($connection));

mysqli_query($connection, 'UPDATE sma_item_stk_lot_bin a INNER JOIN sma_products p ON (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) INNER JOIN sma_product123 z ON (p.name = z.name AND p.code = z.code) INNER JOIN sma_item_stk_lot_bin123 c ON (z.id = c.item_id AND z.warehouse = c.warehouse_id) INNER JOIN sma_warehouses w ON (w.id = p.warehouse) SET a.total_stk = c.total_stk WHERE (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) AND (w.name LIKE "%GODOWN%")') or die("Update stk profile(warehoue) Master Error " . mysqli_error($connection));*/


mysqli_query($connection, 'UPDATE sma_item_stk_profile a INNER JOIN sma_products p ON (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) INNER JOIN sma_product123 z ON (p.name = z.name AND p.code = z.code) INNER JOIN sma_item_stk_profile123 c ON (z.id = c.item_id AND z.warehouse = c.warehouse_id) INNER JOIN sma_warehouses w ON (w.id = p.warehouse) SET a.total_stk = c.total_stk, a.available_stk = c.available_stk WHERE (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) AND (w.name LIKE "%GODOWN%")') or die("Update stk profile Error " . mysqli_error($connection));


mysqli_query($connection, 'UPDATE sma_item_stk_lot a INNER JOIN sma_products p ON (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) INNER JOIN sma_product123 z ON (p.name = z.name AND p.code = z.code) INNER JOIN sma_item_stk_lot123 c ON (z.id = c.item_id AND z.warehouse = c.warehouse_id) INNER JOIN sma_warehouses w ON (w.id = p.warehouse) SET a.total_stk = c.total_stk WHERE (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) AND (w.name LIKE "%GODOWN%")') or die("Update stk lot Error " . mysqli_error($connection));

mysqli_query($connection, 'UPDATE sma_item_stk_lot_bin a INNER JOIN sma_products p ON (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) INNER JOIN sma_product123 z ON (p.name = z.name AND p.code = z.code) INNER JOIN sma_item_stk_lot_bin123 c ON (z.id = c.item_id AND z.warehouse = c.warehouse_id) INNER JOIN sma_warehouses w ON (w.id = p.warehouse) SET a.total_stk = c.total_stk WHERE (p.id = a.item_id AND p.warehouse = a.warehouse_id AND p.code = a.code) AND (w.name LIKE "%GODOWN%")') or die("Update stk lot Error " . mysqli_error($connection));

mysqli_query($connection, 'INSERT INTO sma_item_stk_profile (org_id,segment_id,warehouse_id,fy_id,item_id,item_uom_id,total_stk,available_stk,rejected_stk,rework_stk,ordered_stk,sales_price,created_date,code,wh_id) SELECT a.org_id,a.segment_id,a.warehouse_id,a.fy_id,a.item_id,a.item_uom_id,a.total_stk,a.available_stk, a.rejected_stk,a.rework_stk,a.ordered_stk,a.sales_price,a.created_date,a.code,a.id FROM sma_item_stk_profile123 a WHERE a.wh_id IS NULL OR a.wh_id = ""') or die("Insert stock profile error " . mysqli_error($connection));


mysqli_query($connection, 'INSERT INTO sma_item_stk_lot(org_id,segment_id,warehouse_id,lot_no,item_stk_id,item_id,batch_no, itm_uom_id,total_stk,rejected_stk,mfg_date,exp_date,reworkable_stk,created_date,code,wh_id) SELECT a.org_id,a.segment_id,a.warehouse_id,a.lot_no,a.item_stk_id,a.item_id,a.batch_no, a.itm_uom_id,a.total_stk,a.rejected_stk,a.mfg_date,a.exp_date,a.reworkable_stk,a.created_date,a.code,a.id FROM sma_item_stk_lot123 a WHERE a.wh_id IS NULL OR a.wh_id = ""') or die("Insert stock lot error " . mysqli_error($connection));

mysqli_query($connection, 'INSERT INTO sma_item_stk_lot_bin(org_id,segment_id,warehouse_id,item_id,lot_id,bin_id,itm_uom_id,total_stk,rejected_stk,reworkable_stk,created_date,code,wh_id) SELECT a.org_id,a.segment_id,a.warehouse_id,a.item_id,a.lot_id,a.bin_id,a.itm_uom_id,a.total_stk, a.rejected_stk,a.reworkable_stk,a.created_date,a.code,a.id FROM sma_item_stk_lot_bin123 a WHERE a.wh_id IS NULL OR a.wh_id = ""') or die("Insert stock lot error " . mysqli_error($connection));



mysqli_query($connection, 'UPDATE sma_item_stk_lot a INNER JOIN sma_item_stk_profile b ON (b.wh_id = a.item_stk_id) SET a.item_stk_id = b.id WHERE b.wh_id = a.item_stk_id') or die("updation stock lot map id error " . mysqli_error($connection));


mysqli_query($connection, 'UPDATE sma_item_stk_profile a SET a.wh_id = null WHERE a.wh_id NOT LIKE "%WH%"') or die("updation stock profile map id error" . mysqli_error($connection));
mysqli_query($connection, 'UPDATE sma_item_stk_lot_bin a INNER JOIN sma_item_stk_lot b ON (b.wh_id = a.lot_id) SET a.lot_id = b.id WHERE b.wh_id = a.lot_id') or die("updation stock profile map id error" . mysqli_error($connection));



mysqli_query($connection, 'UPDATE sma_item_stk_lot a SET a.wh_id = null WHERE a.wh_id NOT LIKE "%WH%"') or die("updation stock lot map id error" . mysqli_error($connection));

mysqli_query($connection, 'UPDATE sma_item_stk_lot_bin a SET a.wh_id = null WHERE a.wh_id NOT LIKE "%WH%"') or die("updation stock bin map id error" . mysqli_error($connection));



mysqli_query($connection, 'UPDATE sma_item_stk_profile a INNER JOIN sma_product123 b ON (a.item_id = b.id) INNER JOIN sma_products p ON (p.name = b.name AND p.code = b.code) INNER JOIN sma_warehouses w ON (w.id = p.warehouse) SET a.item_id = p.id WHERE ((a.item_id = b.id) AND w.name LIKE "%GODOWN%" AND (a.wh_id IS NULL OR a.wh_id = ""))') or die("updation stock profile map  2 id error" . mysqli_error($connection));

mysqli_query($connection, 'UPDATE sma_item_stk_lot a INNER JOIN sma_product123 b ON (a.item_id = b.id) INNER JOIN sma_products p ON (p.name = b.name AND p.code = b.code) INNER JOIN sma_warehouses w ON (w.id = p.warehouse) SET a.item_id = p.id WHERE ((a.item_id = b.id) AND w.name LIKE "%GODOWN%" AND (a.wh_id IS NULL OR a.wh_id = ""))') or die("updation stock lot map 3 id error" . mysqli_error($connection));

mysqli_query($connection, 'UPDATE sma_item_stk_lot_bin a INNER JOIN sma_product123 b ON (a.item_id = b.id) INNER JOIN sma_products p ON (p.name = b.name AND p.code = b.code) INNER JOIN sma_warehouses w ON (w.id = p.warehouse) SET a.item_id = p.id WHERE ((a.item_id = b.id) AND w.name LIKE "%GODOWN%" AND (a.wh_id IS NULL OR a.wh_id = ""))') or die("updation stock bin map 3 id error" . mysqli_error($connection));



 
  mysqli_query($connection, 'Update sma_products p INNER JOIN sma_product123 b ON (p.name = b.name AND p.code = b.code) SET p.price = b.price WHERE ((p.name = b.name AND p.code = b.code) AND b.price>1)') or die("Update Product(price) Master Error " . mysqli_error($connection));
  mysqli_query($connection, 'Update sma_purchase_items p INNER JOIN sma_purchase_item123 b ON (p.product_code = b.product_code AND p.product_name = b.product_name) SET p.net_unit_cost = b.net_unit_cost,p.item_tax = b.item_tax, p.tax = b.tax ,p.subtotal = b.subtotal , p.unit_cost= b.unit_cost WHERE ((p.product_code = b.product_code AND p.product_name = b.product_name) AND b.subtotal>1)') or die("Update Purchase(price) Master Error " . mysqli_error($connection));

  /* mysqli_query($connection, 'update sma_drft_po_dlv_schdl as a inner join sma_drft_po_dlv_schdl123 as b on (a.itm_name=b.itm_name and a.ro_no=b.ro_no) set a.bal_qty=b.bal_qty,a.status=b.status where (a.itm_name=b.itm_name and a.ro_no=b.ro_no) AND b.status!=0 ') or die("Update Purchase(price) Master Error " . mysqli_error($connection));*/


    mysqli_query($connection, 'update sma_drft_po_dlv_schdl a inner join sma_drft_po_dlv_schdl123 b on (a.itm_name=b.itm_name and a.ro_no=b.ro_no and a.item_id=b.item_id and a.tot_qty=b.tot_qty) INNER JOIN sma_drft_po123 c ON (c.id = b.po_id) INNER JOIN sma_drft_po d ON (a.po_id = d.id) SET a.bal_qty=b.bal_qty,a.status=b.status WHERE (a.itm_name=b.itm_name and a.ro_no=b.ro_no and a.item_id=b.item_id and a.tot_qty=b.tot_qty) AND b.status!=0 AND (c.auth_po_no = d.auth_po_no AND date(a.dlv_dt) = date(b.dlv_dt) AND c.doc_id = d.doc_id)') or die("Update Purchase(price) Master Error " . mysqli_error($connection));
    mysqli_query($connection, 'update sma_drft_po as a inner join sma_drft_po123 as b on (a.po_id=b.po_id AND a.po_dt=b.po_dt) set a.po_status=b.po_status where (a.po_id=b.po_id AND a.po_dt=b.po_dt) and b.po_status=218 ') or die("Update Purchase(price) Master Error " . mysqli_error($connection));
  
  /*mysqli_query($connection, 'UPDATE sma_drft_po_dlv_schdl, (SELECT sma_warehouses.id FROM sma_warehouses WHERE sma_warehouses.name LIKE "%GODOWN%") src SET sma_drft_po_dlv_schdl.warehouse_id = src.id WHERE 1') or die("Update Po Schdl(warehouse) Master Error " . mysqli_error($connection));
  mysqli_query($connection, 'UPDATE sma_drft_po_dlv_schdl a INNER JOIN sma_products p ON (p.name = a.itm_name AND p.warehouse = a.warehouse_id) SET a.itm_id = p.id WHERE (p.name = a.itm_name AND p.warehouse = a.warehouse_id)') or die("Update Po Schdl(itmid) Master Error " . mysqli_error($connection));*/
 
  mysqli_query($connection, 'UPDATE sma_mrn_receipt,(SELECT sma_users.id FROM sma_users LIMIT 1) src SET sma_mrn_receipt.user_id = src.id WHERE 1') or die("Update Mrn Receipt(usrid) Master Error " . mysqli_error($connection));


   mysqli_query($connection, 'UPDATE sma_mrn_receipt, (SELECT sma_warehouses.id FROM sma_warehouses WHERE sma_warehouses.name LIKE "%GODOWN%") src SET sma_mrn_receipt.wh_id = src.id WHERE 1') or die("Update Mrn Receipt(whid) Master Error " . mysqli_error($connection));

  mysqli_query($connection, 'UPDATE sma_mrn_rcpt_src, (SELECT sma_warehouses.id FROM sma_warehouses WHERE sma_warehouses.name LIKE "%GODOWN%") src SET sma_mrn_rcpt_src.wh_id = src.id WHERE 1') or die("Update Mrn ReceiptSRC(whid) Master Error " . mysqli_error($connection));
  mysqli_query($connection, 'UPDATE sma_mrn_rcpt_src a INNER JOIN sma_drft_po_dlv_schdl b ON (a.ro_no = b.ro_no) SET a.po_id=b.po_id WHERE (a.ro_no = b.ro_no)') or die("Update Mrn ReceiptSRC(poid) Master Error " . mysqli_error($connection));
    mysqli_query($connection, 'UPDATE sma_mrn_receipt a INNER JOIN sma_mrn_rcpt_src b ON (a.id = b.mrn_rcpt_id) INNER JOIN sma_drft_po_dlv_schdl c ON (b.ro_no = c.ro_no) INNER JOIN sma_drft_po d ON (d.id = c.po_id)  SET a.supplier_id=d.customer_id WHERE (a.id = b.mrn_rcpt_id)') or die("Update Mrn Receipt(cid) Master Error " . mysqli_error($connection));
    mysqli_query($connection, 'UPDATE sma_mrn_receipt, (SELECT sma_companies.id FROM sma_companies WHERE sma_companies.name LIKE "%DEFAULT TRANSPORT" LIMIT 1) src SET sma_mrn_receipt.tp_id = src.id WHERE 1') or die("Update Mrn Receipt(tid) Master Error " . mysqli_error($connection));
  /*mysqli_query($connection, 'UPDATE sma_mrn_rcpt_item, (SELECT sma_warehouses.id FROM sma_warehouses WHERE sma_warehouses.name LIKE "%GODOWN%") src SET sma_mrn_rcpt_item.wh_id = src.id WHERE 1') or die("Update Mrn Item(whid) Master Error " . mysqli_error($connection));
  mysqli_query($connection, 'UPDATE sma_mrn_rcpt_item a INNER JOIN sma_drft_po_dlv_schdl b ON (a.po_id = b.po_id AND a.wh_id = b.warehouse_id) SET a.item_id = b.itm_id WHERE (a.po_id = b.po_id AND a.wh_id = b.warehouse_id)') or die("Update Mrn Item(itmid) Master Error " . mysqli_error($connection));
  */
 mysqli_query($connection, 'UPDATE sma_mrn_rcpt_item a INNER JOIN sma_mrn_rcpt_src z ON (z.id = a.mrn_rcpt_src_id) SET a.po_id= z.po_id WHERE (z.id = a.mrn_rcpt_src_id)') or die("Update Mrn Item(poid) Master Error " . mysqli_error($connection));
    mysqli_query($connection, 'UPDATE sma_mrn_rcpt_item a INNER JOIN sma_product123 p ON (p.id = a.item_id AND p.warehouse = a.wh_id) INNER JOIN sma_products pn ON (pn.name = p.name AND pn.code = p.code) INNER JOIN sma_warehouses w ON (pn.warehouse = w.id) SET a.item_id= pn.id WHERE ((p.id = a.item_id AND p.warehouse = a.wh_id) AND w.name LIKE "%GODOWN%")') or die("Update Mrn Item(itmid) Master Error " . mysqli_error($connection));
    mysqli_query($connection, 'UPDATE sma_mrn_rcpt_item, (SELECT sma_warehouses.id FROM sma_warehouses WHERE sma_warehouses.name LIKE "%GODOWN%") src SET sma_mrn_rcpt_item.wh_id = src.id WHERE 1') or die("Update Mrn Item(whid) Master Error " . mysqli_error($connection));
  mysqli_query($connection, 'UPDATE sma_mrn_rcpt_item_lot a INNER JOIN sma_mrn_rcpt_item b ON (b.id = a.mrn_rcpt_item_id) SET a.item_id = b.item_id,a.wh_id = b.wh_id WHERE (b.id = a.mrn_rcpt_item_id)') or die("Update Mrn Item LOT(itmid,whid) Master Error " . mysqli_error($connection));
  mysqli_query($connection, 'UPDATE sma_mrn_rcpt_lot_bin a INNER JOIN sma_mrn_rcpt_item_lot b ON (b.id = a.lot_id) SET a.item_id = b.item_id,a.wh_id = b.wh_id WHERE (b.id = a.lot_id)') or die("Update Mrn Item LOT Bin(itmid,whid) Master Error " . mysqli_error($connection));
  mysqli_query($connection, 'UPDATE sma_mrn_rcpt_lot_bin, (SELECT sma_bin.id FROM sma_bin WHERE sma_bin.bin_nm LIKE "STORAGE" LIMIT 1) src SET sma_mrn_rcpt_lot_bin.bin_id = src.id WHERE (sma_mrn_rcpt_lot_bin.bin_id !=0)') or die("Update Mrn Item LOT Bin(binid) Master Error " . mysqli_error($connection));
 
  mysqli_query($connection, 'INSERT INTO sma_companies (group_id,group_name,customer_group_id,customer_group_name,name,father_name,company,vat_no,address,city,state,postal_code,country,country_id,state_id,phone,email,id_proof,id_proof_no,loyalty_card_id,nominees1,dob_nominees1,upd_flg,eo_type,eo_id,org_id,wh_id,biller_id,segment_id,sync_flg) SELECT b.group_id,b.group_name,b.customer_group_id,b.customer_group_name,b.name,b.father_name,b.company,b.vat_no,b.address,b.city,b.state,b.postal_code,b.country,b.country_id,b.state_id,b.phone,b.email,b.id_proof,b.id_proof_no,b.loyalty_card_id,b.nominees1,b.dob_nominees1,b.upd_flg,b.eo_type,b.eo_id,b.org_id,b.wh_id,b.biller_id,b.segment_id,b.sync_flg FROM sma_company123 b WHERE (b.segment_id> 0 AND b.name!= "Walk-in Customer") group by b.phone') or die("Insert Companies(new record) Master Error " . mysqli_error($connection));
  $query = "UPDATE sma_companies SET id = CONCAT(".$_SESSION['segment_id'].", '', id) WHERE id in ( select id from ( SELECT id FROM sma_companies AS a WHERE a.id NOT LIKE '".$_SESSION['segment_id']."%' AND a.loyalty_card_id != 0 ) AS arbitraryTableName )";
  mysqli_query($connection, $query) or die("Update customer with segment done Master Error " . mysqli_error($connection));
  mysqli_query($connection, 'UPDATE sma_sales a INNER JOIN sma_companies b ON (a.customer = b.name) SET a.customer_id= b.id WHERE (a.customer = b.name)') or die("Update Sales(customerID) Master Error " . mysqli_error($connection));
            
            $this->session->set_flashdata('message', "Congratulations!!!! Data Synced Successfully");
            redirect('auth/logout');
          //  redirect('welcome');
        }
        $this->page_construct('syncin', $meta, $this->data);        
    }


    public function syncincentral(){
        $this->msync_model->syncincentral();
        exit;
    }


    function syncout(){

        $directory = $_SERVER["DOCUMENT_ROOT"].'/'.PROJECT_NAME.'/syncout-folder/';
        $meta = array('page_title' => lang('Sync Data from local to Central Server'));
        $this->load->model('site');
        $segmentList = $this->site->getAllSegments();
        $this->data['segmentList'] = $segmentList;
        
        foreach($segmentList as $segment){
            $segment_id = $segment->id;
      //  if(isset($segment_id) && ($segment_id!='')){

            // $remote_host = REMOTE_HOST;
            // $remote_username = REMOTE_USERNAME;
            // $remote_password = REMOTE_PWD;
            // $remote_dbname = REMOTE_DB;

            $remote_host = $this->db->hostname;
            $remote_username = $this->db->username;
            $remote_password = $this->db->password;
            $remote_dbname = $this->db->database;

            $remote_connection = mysqli_connect($remote_host, $remote_username, $remote_password, $remote_dbname) or die("Connection Error " . mysqli_error($remote_connection)); 
            // ;
            $sql = "select * from sma_syncout_history where flag=0 ORDER BY created_date ASC";
            $csv_data = mysqli_query($remote_connection, $sql) or die("Selection Error " . mysqli_error($remote_connection));

            $csv_list = mysqli_fetch_array($csv_data, MYSQLI_ASSOC);
            // mysqli_autocommit($remote_connection, FALSE);
            $files_list = array();

            foreach($csv_data as $fileRecord){

              // $file_name = $_SERVER['DOCUMENT_ROOT'].'/'.$fileRecord['file_name']; 

               $file_name = '/home/iebl/'.$fileRecord['file_name'];
                // echo $file_name;exit;
                $zip = new ZipArchive;
//                $zip->open($file_name) === FALSE
               // echo var_dump($zip->open($file_name));exit;
                if ($zip->open($file_name) === TRUE) {                   
                    $csv_folder = $directory;
                    $folderName = explode("/",$fileRecord['file_name'])[1]; 

                    $directory.$folderName;                                  
                    copy($file_name,$directory.$folderName);
                   //$extract_path = $directory.explode(".",explode("/",$fileRecord['file_name'])[1])[0]."/";
                  // $extract_path= 'C:/xampp/htdocs/iffcov9/syncout-folder/syncout-folder/';   
                    $extract_path = '/home/iebl/uploads/'.explode(".",explode("/",$fileRecord['file_name'])[1])[0]."/"; 
                  
                    $zip->extractTo($extract_path);
                    $zip->close();              
                  

                    $host = $this->db->hostname;
                    $username = $this->db->username;
                    $password = $this->db->password;
                    $dbname = $this->db->database;

                    // open connection to mysql database
                    $connection = mysqli_connect($host, $username, $password, $dbname) or die("Connection Error " . mysqli_error($connection));
                    
                    $sql = "select * from sma_segment where id=".$segment_id."";
                    $seg_result = mysqli_query($connection, $sql) or die("Selection Error " . mysqli_error($connection));
                    $seg_row = mysqli_fetch_array($seg_result, MYSQLI_ASSOC);
                    $projName = explode(" ",$seg_row['proj_name'])[0]."_".$segment_id;
                   
                    // if(count($files_list)>0){
                        // foreach($files_list as $fileName){
                        //     $zip = new ZipArchive;
                        //     $csv_date = explode("/",$fileName);
                        //     $csv_folder = $csv_date[count($csv_date)-2];
                            
                        // if ($zip->open($fileName) === TRUE) {
                        //     $csv_path = $directory.$csv_folder."/".$projName."/";
                        //     $zip->extractTo($csv_path);
                        //     $zip->close();
                         /*else {
                            echo 'Failed to extract zip file';
                            exit;
                        }  */             
                        // path where your CSV file is located
                       //define('CSV_PATH',$_SERVER["DOCUMENT_ROOT"].'/'.PROJECT_NAME.'/sync-folder/'.$projName.'/');
                        //define('CSV_PATH',$csv_path);
                        //echo "csv path".CSV_PATH;exit;
                        // Count File
                        //added by vikas singh
                      //  $csv_path = $_SERVER["DOCUMENT_ROOT"].'/'.PROJECT_NAME.'/syncout-folder/'.$projName.'/';
                        $csv_path = $_SERVER["DOCUMENT_ROOT"].'/'.PROJECT_NAME.'/syncout-folder/';
                       
                        $sql = "select count(*) as count from sma_syncout_table";
                        $result = mysqli_query($connection, $sql) or die("Selection Error " . mysqli_error($connection));
                        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                        $num_table=$row['count'];
                        for ($x = 1; $x <= $num_table; $x++){               
                            
                            $sql = "select * from sma_syncout_table where id='$x'";
                            $result = mysqli_query($connection, $sql) or die("Selection Error " . mysqli_error($connection));
                            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                            $table=$row['table_name'];
                            // fetch mysql table rows
                            $csv_filename = $table.".csv"; // to be changed

                            //$csv_filename ='sma_segment_2017-02-05.csv';
                            // Name of your CSV file
                            $csv_file = $extract_path . $csv_filename;
                            // echo $csv_file;exit;  
                            if (($handle = fopen($csv_file, "r")) !== FALSE) {                                
                              $i=0;
                             while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {


                                $col = array();
                                $num = count($data); 
                                for ($c=0; $c < $num; $c++) {
                                  $col[$c] = $data[$c];

                                }
                               
                                  
                            //     if($table=='sma_sales'){
                            //     //     //echo "<pre>".$csv_filename;exit;
                            //         print_r($col);exit;
                            // }
                            //  die;
							//echo $table."<br>";
                        switch ($table) {

                           /********Sales Details UPDATE****************/

                            case "sma_sales":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];
                                $col22 = $col[21];
                                $col23 = $col[22];
                                $col24 = $col[23];
                                $col25 = $col[24];
                                $col26 = $col[25];
                                $col27 = $col[26];
                                $col28 = $col[27];
                                $col29 = $col[28];
                                $col30 = $col[29];
                                $col31 = $col[30];
                                $col32 = $col[31];
                                $col33 = $col[32];
                                $col34 = $col[33];
                                $col35 = $col[34];
                                $col36 = $col[35];
                                $col37 = $col[36];

                             
                              $select = "SELECT sid FROM sma_sales where sid =".$col1." AND warehouse_id=".$col8;
                              $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                              $count = mysqli_num_rows($select_result);

                              if($count <= 0)
                                {
                                  $query = "INSERT ignore INTO sma_sales
                                    (id,date,reference_no,customer_id,customer,biller_id,biller,warehouse_id,note,staff_note,total,product_discount,order_discount_id,total_discount,order_discount,product_tax,order_tax_id,order_tax,total_tax,shipping,grand_total,sale_status,payment_status,payment_term,due_date,created_by,updated_by,updated_at,total_items,pos,paid,return_id,surcharge,attachment,sid,manual,manual_inv_no) 
                                    VALUES
                                    ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col24."','".$col25."','".$col26."','".$col27."','".$col28."','".$col29."','".$col30."','".$col31."','".$col32."','".$col33."','".$col34."','".$col1."','".$col36."','".$col37."')"; 
                                }

                                break;
                           
                            case "sma_sale_items":

                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];
                                $col22 = $col[21];
                                $col23 = $col[22];
                                $col24 = $col[23];

                             $select = "SELECT a.id FROM sma_sale_items a where a.sid='".$col1."' AND a.warehouse_id=".$col13;
                             
                              $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                              $count = mysqli_num_rows($select_result);

                              if($count == 0)
                                {
                                    
                                   $query = "INSERT ignore INTO sma_sale_items
                                    (id,sale_id,product_id,product_code,product_name,item_uom_id,product_type,option_id,net_unit_price,unit_price,quantity,expiry,  warehouse_id,item_tax,tax_rate_id,tax,discount,item_discount,subtotal,lot_no,bin_id,serial_no,real_unit_price,sid,itm_id) 
                                    VALUES
                                    ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col1."','".$col2."')"; 
                                   

                              }  
                            
                            break;


                            case "sma_sale_item_lot":
                             
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];

                           $select = "SELECT a.id FROM sma_sale_item_lot a where a.lid='".$col1."' and a.warehouse_id='".$col4."'";

                            $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                             $count = mysqli_num_rows($select_result); 

                             if($count == 0)
                                 { 

                            
                                  $query = "INSERT ignore INTO sma_sale_item_lot
                                    (id,org_id,wh_id,warehouse_id,code,sale_id,sale_item_id,lot_id,lot_no,quantity,created_date,lid,sid,itm_id) 
                                    VALUES
                                    ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col1."','".$col6."','".$col7."')";

                             }
                                break; 
                           

                            case "sma_sale_item_bin":

                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                            

                            $select = "SELECT a.id FROM sma_sale_item_bin a where a.bid='".$col1."' and a.warehouse_id='".$col4."'"; 
   

                            $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                             $count = mysqli_num_rows($select_result); 

                             if($count == 0)
                                 {                           
                                    $query = "INSERT ignore INTO sma_sale_item_bin
                                    (id,org_id,wh_id,warehouse_id,code,sale_id,sale_item_id,lot_id,bin_id,bin_name,quantity,created_date,bid,sid,itm_id) 
                                    VALUES
                                    ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col1."','".$col6."','".$col7."')"; 

                             }
                                break; 
                           

                            case "sma_payments":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];
                                $col22 = $col[21];
                                $col23 = $col[22];
                                $col24 = $col[23];
                              
                              $select = "SELECT a.id FROM sma_payments a where a.pid ='".$col1."' AND a.created_by ='".$col18."'"; 

                              $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                              $count = mysqli_num_rows($select_result);

                              if($count == 0)
                                {

                                  $query = "INSERT ignore INTO sma_payments
                                    (id,date,sale_id,return_id,purchase_id,reference_no,transaction_id,paid_by,cheque_no,cc_no,cv_no,cc_holder,cc_month,cc_year,cc_type,amount,currency,created_by,attachment,type,note,pos_paid,pos_balance,pid,sid) 
                                    VALUES
                                    ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col1."','".$col3."')"; 

                               
                              }
                                break;

                            case "sma_costing":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];

                            $select = "SELECT a.id FROM sma_costing a where a.cid='".$col1."' and a.itm_id='".$col4."' and a.product_id='".$col3."'"; 
                            $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                             $count = mysqli_num_rows($select_result); 

                             if($count == 0)
                                 {                             
                                  $query = "INSERT ignore INTO sma_costing
                                    (id,date,product_id,sale_item_id,sale_id,purchase_item_id,quantity,purchase_net_unit_cost,purchase_unit_cost,sale_net_unit_price,sale_unit_price,quantity_balance,inventory,overselling,option_id,cid,sid,itm_id) 
                                    VALUES
                                    ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col17."','".$col1."','".$col5."','".$col4."')"; 

                             }
                                break; 
                           
                            //  /********Companies UPDATE****************/
                              case "sma_companies":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];
                                $col22 = $col[21];
                                $col23 = $col[22];
                                $col24 = $col[23];
                                $col25 = $col[24];
                                $col26 = $col[25];
                                $col27 = $col[26];
                                $col28 = $col[27];
                                $col29 = $col[28];
                                $col30 = $col[29];
                                $col31 = $col[30];
                                $col32 = $col[31];
                                $col33 = $col[32];
                                $col34 = $col[33];
                                $col35 = $col[34];
                                $col36 = $col[35];
                                $col37 = $col[36];
                                $col38 = $col[37];
                                $col39 = $col[38];
                                $col40 = $col[39];
                                $col41 = $col[40];
                                $col42 = $col[41];
                                $col43 = $col[42];
                                $col44 = $col[43];
                                $col45 = $col[44];
                                $col46 = $col[45];
                                $col47 = $col[46];
                                $col48 = $col[47];
                                $col49 = $col[48];
                                $col50 = $col[49];
                                $col51 = $col[50];
                                $col52 = $col[51];

                          
                             $select = "SELECT id FROM sma_companies where cid ='".$col1."' AND segment_id='".$col48."'";
                             $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                             $count = mysqli_num_rows($select_result);

                              if($count <= 0)
                                {

                                  $query = "INSERT ignore INTO sma_companies
                                    (id,group_id,group_name,customer_group_id,customer_group_name,name,father_name,company,vat_no,address,city,state,postal_code,country,country_id,state_id,phone,email,id_proof,id_proof_no,loyalty_card_id,nominees1,dob_nominees1,nominees2,dob_nominees2,nominees3,dob_nominees3,nominees4,dob_nominees4,nominees5,dob_nominees5,cf1,cf2,cf3,cf4,cf5,cf6,invoice_footer,payment_term,logo,award_points,upd_flg,eo_type,eo_id,org_id,wh_id,biller_id,segment_id,cid,adhar_card_no) 
                                    VALUES
                                    ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col24."','".$col25."','".$col26."','".$col27."','".$col28."','".$col29."','".$col30."','".$col31."','".$col32."','".$col33."','".$col34."','".$col35."','".$col36."','".$col37."','".$col38."','".$col39."','".$col40."','".$col41."','".$col42."','".$col43."','".$col44."','".$col45."','".$col46."','".$col47."','".$col48."','".$col1."','".$col52."')"; 
                              
                                }
                                break; 

                            case "sma_drft_po":
                          
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col14 = $col[13];
                                $col18 = $col[17];
                                $col20 = $col[19];

                                $query= "UPDATE sma_drft_po SET po_status =".$col18." WHERE po_id ='".$col3."' AND po_dt ='".$col4."' AND auth_po_no='".$col14."' AND doc_id ='".$col20."'"; 
                                
                            

                                break;

                             case "sma_drft_po_dlv_schdl":
                            
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col17 = $col[16];
                                $col20 = $col[19];
                                $col21 = $col[20];
                                $col23 = $col[22];
                                $col24 = $col[23];
                                $col25 = $col[24];
                                $col27 = $col[26];
  
                                
                                $query="UPDATE sma_drft_po_dlv_schdl a INNER JOIN sma_drft_po b ON (b.id=a.po_id) SET a.bal_qty='".$col17."', a.status =".$col27." WHERE a.po_id ='".$col3."' AND a.ro_no ='".$col23."' AND a.ro_dt ='".$col24."' AND a.item_id ='".$col25."' AND a.warehouse_id=".$col21." AND a.segment_id =".$col20." AND b.po_status=218 AND (b.id=a.po_id)";
                            
                          
                               break;

                           /******** MRN UPDATE****************/

                            case "sma_mrn_receipt":

                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];
                                $col22 = $col[21];
                                $col23 = $col[22];
                                $col24 = $col[23];
                                $col25 = $col[24];
                                $col26 = $col[25];
                                $col27 = $col[26];
                                $col28 = $col[27];
                                $col29 = $col[28];
                                $col30 = $col[29];
                                $col31 = $col[30];
                               
                            $select = "SELECT a.id FROM sma_mrn_receipt a where a.mid ='".$col1."' AND a.segment_id='".$col4."'"; 
                             $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                             $count = mysqli_num_rows($select_result); 

                              if($count == 0)
                                {
                                 $query = "INSERT ignore INTO sma_mrn_receipt
                                    (id,ho_id,org_id,segment_id,wh_id,warehouse_id,fy_id,rcpt_no,rcpt_dt,rcpt_src_type, ge_doc_id,supplier_id,org_id_src,wh_id_src,dn_no,dn_dt,sto_no,sto_date,tp_id,tpt_lr_no,tpt_lr_dt,vehicle_no,status,rmda_stat,addl_amt,curr_id,remarks,user_id,created_date,mod_date,mid) 
                                    VALUES
                                    ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col24."','".$col25."','".$col26."','".$col28."','".$col29."','".$col30."','".$col31."','".$col1."')";  
                               }
                             break;

                              case "sma_mrn_rcpt_src":

                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col15 = $col[14];
                               
                                 
                             $select = "SELECT id FROM sma_mrn_rcpt_src where sid =".$col1." AND segment_id=".$col3;
                             $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                             $count = mysqli_num_rows($select_result); 
                              if($count ==0)
                              {
                                $query = "INSERT ignore INTO sma_mrn_rcpt_src
                                    (id,org_id,segment_id,wh_id,warehouse_id,mrn_rcpt_id,po_id,po_date,ro_no,ro_date,gatepass_no,gatepass_date,use_rented_wh,sid,rcpt_no) 
                                    VALUES
                                    ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col1."','".$col15."')"; 
                               }
                             break; 

                             case "sma_mrn_rcpt_item":

                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];
                                $col24 = $col[23];
                                $col25 = $col[24];

                            $select = "SELECT id FROM sma_mrn_rcpt_item where itm_id =".$col1." AND segment_id=".$col3;
                            $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                            $count = mysqli_num_rows($select_result); 

                              if($count ==0)
                              {
                                $query = "INSERT ignore INTO sma_mrn_rcpt_item
                                    (id,org_id,segment_id,wh_id,warehouse_id,mrn_rcpt_src_id,po_id,po_date,dlv_schdl_no,item_id,code,item_uom_id,pending_qty,dlv_note_qty,tot_rcpt_qty,rwk_qty,rej_qty,rcpt_qty,purchase_price,created_date,itm_id,ro_no,rcpt_no) VALUES
                                      ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."',NULL,'".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col21."','".$col1."','".$col24."','".$col25."')"; 
                            }

                             break;
                            
                             case "sma_mrn_rcpt_item_lot":

                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col23 = $col[22];
                                $col24 = $col[23];
                               
                            $select = "SELECT id FROM sma_mrn_rcpt_item_lot where lid =".$col1." AND segment_id=".$col3;
                            $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                            $count = mysqli_num_rows($select_result); 

                            if($count ==0)
                              { 
                              
                               $query = "INSERT ignore INTO sma_mrn_rcpt_item_lot
                                    (id,org_id,segment_id,wh_id,warehouse_id,mrn_rcpt_item_id,lot_no,dlv_schdl_no,item_id,code,itm_uom_id,lot_qty,rej_qty,rewrk_qty,mfg_dt,expiry_dt,batch_no,itm_uom_bs,created_date,lid,ro_no,rcpt_no)VALUES('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."',NULL,'".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col1."','".$col23."','".$col24."')"; 
                                }

                              break;

                          case "sma_mrn_rcpt_lot_bin":

                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col19 = $col[18];
                                $col20 = $col[19];
                             
                              $select = "SELECT a.id FROM sma_mrn_rcpt_lot_bin a where a.segment_id='".$col3."' AND a.bid='".$col1."'";

                              $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                            
                              $count = mysqli_num_rows($select_result); 

                              if($count ==0)
                                {


                                 $query = "INSERT ignore INTO sma_mrn_rcpt_lot_bin
                                    (id,org_id,segment_id,wh_id,warehouse_id,lot_id,item_id,code,itm_uom_id,bin_id,bin_qty,itm_uom_bs,rej_qty,rewrk_qty,created_date,bid,ro_no,rcpt_no)VALUES 
                                      ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col1."','".$col19."','".$col20."')"; 
                               
							               }


                             break;


                           /******** STOCK UPDATE****************/

                             case "sma_item_stk_profile":

                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                        				$col10 = $col[9];
                        				$col11 = $col[10];
								
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
								
                                if($col19 ==1 && $col18!=1){
                               
                               $query="UPDATE sma_item_stk_profile SET total_stk='".$col9."', available_stk='".$col10."', rejected_stk='".$col11."',rework_stk='".$col12."',ordered_stk='".$col13."', upd_flg ='1' WHERE item_id ='".$col7."' AND code ='".$col17."' AND warehouse_id='".$col4."' AND segment_id ='".$col3."' "; 
                                
                               }
                               

                              if(($col19 ==1 && $col18==1) || ($col19 !=1 && $col18==1)){ 
                                
                            $select = "SELECT id FROM sma_item_stk_profile where sid ='".$col1."' AND segment_id =".$col3." AND warehouse_id ='".$col4."'"; 
                            $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                            $count = mysqli_num_rows($select_result);

                            if($count == 0)
                                { 
                                $query = "INSERT ignore INTO sma_item_stk_profile
                                    (id,org_id,segment_id,warehouse_id,wh_id,fy_id,item_id,item_uom_id, total_stk,available_stk,rejected_stk,rework_stk,ordered_stk,purchase_price,sales_price,created_date,code,create_flg,upd_flg,sid)VALUES 
                                      ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','1','".$col19."','".$col1."')"; 

                               }
                             }

                             break;

                         case "sma_item_stk_lot":

                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                
                                if(($col19 ==1 && $col18!=1)){
                               
                                $query="UPDATE sma_item_stk_lot SET total_stk='".$col11."', rejected_stk='".$col12."', reworkable_stk='".$col15."',upd_flg ='1' WHERE item_id ='".$col8."' AND code ='".$col17."' AND lot_no ='".$col6."' AND warehouse_id=".$col4." AND segment_id =".$col3; 
                                
                               }
                            
                               if(($col19 ==1 && $col18==1) || ($col19 !=1 && $col18==1)){

                                $select = "SELECT a.id FROM sma_item_stk_lot a where a.segment_id ='".$col3."' AND a.warehouse_id ='".$col4."' AND a.lid = '".$col1."'";
                                 $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                                 $count = mysqli_num_rows($select_result); 

                              if($count == 0)
                                 { 
                            
                                $select1 = "SELECT sid FROM `sma_item_stk_profile` WHERE item_id ='".$col8."' and segment_id=".$col3." and warehouse_id =".$col4." LIMIT 1";
                                $select_result1 = mysqli_query($remote_connection, $select1) or die("Selection Error " . mysqli_error($remote_connection)); 
                                $sid = mysqli_fetch_array($select_result1);
                                $item_sid= $sid['sid'];
                                $query = "INSERT ignore INTO sma_item_stk_lot
                                    (id,org_id,segment_id,warehouse_id,wh_id,lot_no,item_stk_id,item_id,batch_no,itm_uom_id,total_stk,rejected_stk,mfg_date,exp_date,reworkable_stk,created_date,code,create_flg,upd_flg,lid,sid)VALUES 
                                      ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','1','".$col19."','".$col1."','".$item_sid."')"; 
                                 }
                               }

                             break;


                             case "sma_item_stk_lot_bin":

                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];   
                             
                               if(($col16 ==1 && $col15!=1)){

                              $query="UPDATE sma_item_stk_lot_bin SET total_stk='".$col10."', rejected_stk='".$col11."', reworkable_stk='".$col12."',upd_flg ='1' WHERE item_id ='".$col6."' AND code ='".$col14."' AND bin_id ='".$col8."' AND lot_id ='".$col7."' AND warehouse_id=".$col4." AND segment_id =".$col3;
                                
                               }


                               if(($col16 !=1 && $col15==1) || ($col16 ==1 && $col15==1)){

                            $select = "SELECT a.id FROM sma_item_stk_lot_bin a where a.segment_id ='".$col3."' AND a.warehouse_id ='".$col4."' AND a.bid='".$col1."' ";
                             
                            $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                            $count = mysqli_num_rows($select_result); 

                            if($count == 0)
                                { 

                               $select1 = "SELECT lid FROM `sma_item_stk_lot` WHERE item_id ='".$col8."' and segment_id=".$col3." and warehouse_id =".$col4." and id =".$col7." LIMIT 1";
                                $select_result1 = mysqli_query($remote_connection, $select1) or die("Selection Error " . mysqli_error($remote_connection)); 
                                $lid = mysqli_fetch_array($select_result1);
                                $item_lid= $sid['lid'];
                                $query = "INSERT ignore INTO sma_item_stk_lot_bin
                                    (id,org_id,segment_id,warehouse_id,wh_id,item_id,lot_id,bin_id,itm_uom_id,total_stk,rejected_stk,reworkable_stk,created_date,code,create_flg,upd_flg,bid,lid)VALUES 
                                      ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','1','".$col16."','".$col1."','".$item_lid."')"; 

                               }
                              } 

                             break;

                           /***************POS REGISTER*******************/

                             case "sma_pos_register":

                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                             

                            $select = "SELECT id FROM sma_pos_register where user_id =".$col3." AND rid ='".$col1."'";

                            $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                            $count = mysqli_num_rows($select_result); 

                            if($count == 0)
                            { 

                                $query = "INSERT ignore INTO sma_pos_register
                                    (id,date,user_id,cash_in_hand,status,total_cash,total_cheques,total_cc_slips,total_cash_submitted,total_cheques_submitted,total_cc_slips_submitted,note,closed_at,transfer_opened_bills,closed_by,rid)VALUES 
                                      ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".mysqli_real_escape_string($col12)."','".$col13."','".$col14."','".$col15."','".$col1."')"; 

                                }
                             break;

                         // BANK DEPOSIT

                            case "sma_expenses":

                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                if(($col12 ==1 && $col11!=1)){
                               
                                $query="UPDATE sma_expenses SET date='".$col2."', reference='".$col3."', amount='".$col4."',note='".mysqli_real_escape_string($col5)."',created_by='".$col6."',attachment='".$col7."',register_id='".$col8."',bank_eo_id='".$col13."',update_at='".$col15."', upd_flg ='1' WHERE created_by ='".$col6."' AND eid =".$col1; 
                                
                               }
                            
                            $select = "SELECT id FROM sma_expenses where created_by =".$col6." AND eid ='".$col1."'";

                            $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                            $count = mysqli_num_rows($select_result); 

                            if($count == 0)
                                { 

                                $query = "INSERT ignore INTO sma_expenses
                                    (id,date,reference,amount,note,created_by,attachment,   register_id,bank_eo_id,manual,update_at,eid)VALUES 
                                      ('','".$col2."','".$col3."','".$col4."','".mysqli_real_escape_string($col5)."','".$col6."','".$col7."','".$col8."','".$col13."','".$col14."','".$col15."','".$col1."')"; 

                                }
                             break;

                             
                            case "sma_cheque_details":

                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20]; 


                                //cdid =".$col1." AND sid ='".$col6."' AND warehouse_id='".$col4."' AND
                              $select = "SELECT id FROM sma_cheque_details where  segment_id='".$col3."' AND wh_id='".$col5."' AND reference_no='".$col16."'";

                              $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                              $count = mysqli_num_rows($select_result); 

                              if($count == 0)
                              { 
                                $query = "INSERT ignore INTO sma_cheque_details
                                    (`id`, `org_id`, `segment_id`, `warehouse_id`, `wh_id`, `sale_id`, `cheque_no`, `customer_name`, `bank_name`, `cheque_date`, `amount`, `status`, `clearance_date`, `transaction_date`, `remarks`, `reference_no`, `sync_flg`, `upd_flg`, `sid`, `cdid`,`eo_type`)VALUES 
                                      ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','','','".$col6."','".$col1."','".$col21."')"; 
                               
                              }
                              break;


                              // ------------------------ Gate Pass Syncout code --------------------------//

                           case "sma_gp":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15]; 
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                                $col21 = $col[20];
                                $col22 = $col[21];
                                $col23 = $col[22];
                                $col24 = $col[23];
                                $col25 = $col[24];
								                $col26 = $col[25];
								
                             
                             $select = "SELECT a.gp_no FROM sma_gp a where a.gp_no ='".$col12."'"; 
                             $select_result = mysqli_query($remote_connection, $select) or die("Selection GP Error " . mysqli_error($remote_connection)); 
                             $count = mysqli_num_rows($select_result); 

                              if($count == 0)
                                {
                                  $query = "INSERT ignore INTO sma_gp
                                    (id,ho_id,org_id,wh_id,segment_id,warehouse_id,fy_id,gp_type,supplier_id,doc_no,doc_dt,gp_no,gp_date,
                                    tp_id,tpt_lr_no,tpt_lr_dt,vehicle_no,status,rcpt_date,addl_amt,cust_name,remarks,curr_id,user_id,created_date,
                                    mod_date,gpid)
                                    VALUES
                                    ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col24."','".$col25."','".$col26."','".$col1."')";  
                               }
                             break;

                          case "sma_gp_src":

                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                
                                $col11 = $col[10];
                                                             
                                 
                             $select = "SELECT id FROM sma_gp_src where doc_no ='".$col8."' AND gp_no='".$col11."'";
                             $select_result = mysqli_query($remote_connection, $select) or die("Selection GP SRC Error " . mysqli_error($remote_connection)); 
                             $count = mysqli_num_rows($select_result); 
                              if($count ==0)
                              {
                                $query = "INSERT ignore INTO sma_gp_src
                                    (id,org_id,wh_id,segment_id,warehouse_id,gp_id,doc_id,doc_no,doc_date,gp_no,gpsrcid) 
                                    VALUES
                                    ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col11."','".$col1."')"; 
                               }
                             break;  


                            case "sma_gp_item":

                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col17 = $col[16];
                                

                            $select = "SELECT id FROM sma_gp_item where warehouse_id =".$col5." AND gpitmid=".$col1." AND gp_no='".$col17."'";
                            $select_result = mysqli_query($remote_connection, $select) or die("Selection GP Item Error " . mysqli_error($remote_connection)); 
                            $count = mysqli_num_rows($select_result); 

                              if($count ==0)
                              {
                                $query = "INSERT ignore INTO sma_gp_item
                                    (id,org_id,wh_id,segment_id,warehouse_id,gp_src_id,item_id,code,item_uom_id,req_qty,pending_qty,dlv_note_qty,
                                    	act_rcpt_qty,item_price,item_amt,gp_no,gpitmid) VALUES
                                      ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col17."','".$col1."')"; 
                            }

                             break;


                            case "sma_gp_item_lot":

                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];  
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col18 = $col[17];
                               
                               
                             $select = "SELECT id FROM sma_gp_item_lot where segment_id =".$col4." AND gplotid=".$col1." AND gp_no='".$col17."'"; 
                            $select_result = mysqli_query($remote_connection, $select) or die("Selection GP LOT Error " . mysqli_error($remote_connection)); 
                            $count = mysqli_num_rows($select_result); 

                            if($count ==0)
                              { 
                              
                                $query = "INSERT ignore INTO sma_gp_item_lot
                                    (id,org_id,wh_id,segment_id,warehouse_id,mrn_gp_item_id,lot_no,item_id,code,item_uom_id,lot_qty,mfg_dt,
                                        expiry_dt,batch_no,itm_uom_bs,pending_qty,gp_no,gplotid)VALUES('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."',NULL,'".$col18."','".$col1."')"; 
                                 }

                              break;


                            case "sma_gp_lot_bin":
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];                              
                                $col15 = $col[14];
                                $col16 = $col[15];                           
                             
                            $select = "SELECT a.id FROM sma_gp_lot_bin a where a.segment_id='".$col3."' AND a.gpbid='".$col1."'";

                            $select_result = mysqli_query($remote_connection, $select) or die("Selection GP Bin Error " . mysqli_error($remote_connection)); 
                            
                           $count = mysqli_num_rows($select_result); 

                            if($count ==0)
                              {

                                $query = "INSERT ignore INTO sma_gp_lot_bin
                                    (id,org_id,wh_id,segment_id,warehouse_id,lot_id,item_id,code,item_uom_id,bin_id,bin_qty,itm_uom_bs,pending_qty,gp_no,lot_no,gpbid)VALUES 
                                      ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col15."','".$col16."','".$col1."')"; 
                               
							   }

                             break;

                             case "sma_cons_item_stk_profile":
                                                   
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                        				$col10 = $col[9];
                        				$col11 = $col[10];								
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                $col20 = $col[19];
                             
                             if($col20 ==1 && $col19!=1){
                               
                                    $query="UPDATE sma_cons_item_stk_profile SET total_stk='".$col10."', available_stk='".$col11."', rejected_stk='".$col12."',rework_stk='".$col14."',ordered_stk='".$col15."', upd_flg ='1' WHERE item_id ='".$col7."' AND code ='".$col8."' AND warehouse_id='".$col3."' AND segment_id ='".$col4."' "; 
                                
                                }                               

                                if(($col20 ==1 && $col19==1) || ($col20 !=1 && $col19==1)){
                                
                                    $select = "SELECT id FROM sma_cons_item_stk_profile` where sid ='".$col1."' AND segment_id =".$col4." AND warehouse_id ='".$col3."'"; 
                                    $select_result = mysqli_query($remote_connection, $select) or die("Selection Cons STK Error " . mysqli_error($remote_connection)); 
                                    $count = mysqli_num_rows($select_result);

                                    if($count == 0){                                 
                                        $query = "INSERT ignore INTO sma_cons_item_stk_profile
                                            (id,org_id,warehouse_id,segment_id,wh_id,fy_id,item_id,code,item_uom_id, total_stk,available_stk,rejected_stk,bal_qty,rework_stk,ordered_stk,purchase_price,sales_price,created_date,create_flg,sid)VALUES 
                                            ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','1','".$col1."')"; 

                                    }
                                }

                             break;

//----------------------------------------------


                    case "sma_cons_item_stk_lot":
                                
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];
                                $col19 = $col[18];
                                
                                if(($col19 ==1 && $col18!=1)){
                               
                                $query="UPDATE sma_cons_item_stk_lot SET total_stk='".$col12."', rejected_stk='".$col13."', reworkable_stk='".$col16."',upd_flg ='1' WHERE item_id ='".$col8."' AND code ='".$col9."' AND lot_no ='".$col6."' AND warehouse_id=".$col3." AND segment_id =".$col4; 
                                
                               }
                            
                               if(($col19 ==1 && $col18==1) || ($col19 !=1 && $col18==1)){

                                 $select = "SELECT a.id FROM sma_cons_item_stk_lot a where a.segment_id ='".$col4."' AND a.warehouse_id ='".$col3."' AND a.lid = '".$col1."'";
                                 $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                                 $count = mysqli_num_rows($select_result); 

                              if($count == 0)
                                 { 
                            
                                $select1 = "SELECT lid FROM `sma_cons_item_stk_lot` WHERE item_id ='".$col8."' and segment_id=".$col4." and warehouse_id =".$col3." LIMIT 1";
                                $select_result1 = mysqli_query($remote_connection, $select1) or die("Selection CON LOT Error " . mysqli_error($remote_connection)); 
                                $sid = mysqli_fetch_array($select_result1);
                                //$item_sid= $sid['sid'];
                                $query = "INSERT ignore INTO sma_cons_item_stk_lot
                                    (id,org_id,warehouse_id,segment_id,wh_id,lot_no,item_stk_id,item_id,code,batch_no,item_uom_id,total_stk,rejected_stk,mfg_date,exp_date,reworkable_stk,created_date,create_flg,upd_flg,lid,sid)VALUES 
                                      ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','1','".$col19."','".$col1."','".$col7."')"; 
                                 }
                               }
                              
                             
                             break;


//-----------------------------------------------------------------------------------------------

                               case "sma_cons_item_stk_lot_bin":

                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];								
                             
                               if(($col16 ==1 && $col15!=1)){

                                    $query="UPDATE sma_cons_item_stk_lot_bin SET total_stk='".$col11."', rejected_stk='".$col12."', reworkable_stk='".$col13."',upd_flg ='1' WHERE item_id ='".$col6."' AND code ='".$col17."' AND bin_id ='".$col9."' AND lot_id ='".$col8."' AND warehouse_id=".$col3." AND segment_id =".$col4;
                                
                               }

                               if(($col16 !=1 && $col15==1) || ($col16 ==1 && $col15==1)){

                                    $select = "SELECT a.id FROM sma_cons_item_stk_lot_bin a where a.segment_id ='".$col4."' AND a.warehouse_id ='".$col3."' AND a.bid='".$col1."' ";
                                    
                                    $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                                    $count = mysqli_num_rows($select_result); 

                                    if($count == 0){
                                        
                                        
                                        $select1 = "SELECT bid FROM `sma_cons_item_stk_lot_bin` WHERE item_id ='".$col6."' and segment_id=".$col4." and warehouse_id =".$col3." and id =".$col8." LIMIT 1";
                                        $select_result1 = mysqli_query($remote_connection, $select1) or die("Selection Error " . mysqli_error($remote_connection)); 
                                        $lid = mysqli_fetch_array($select_result1);
                                        //$item_lid= $sid['lid'];
                                        $query = "INSERT ignore INTO sma_cons_item_stk_lot_bin
                                            (id,org_id,warehouse_id,segment_id,wh_id,item_id,code,lot_id,bin_id,item_uom_id,total_stk,rejected_stk,reworkable_stk,created_date,create_flg,lot_no,bid)VALUES 
                                            ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','1','".$col17."','".$col1."')"; 
                                     
                                    }
                              } 

                             break;

                             
                             case "sma_emrs_item":

                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];
                                $col11 = $col[10];
                                $col12 = $col[11];
                                $col13 = $col[12];
                                $col14 = $col[13];
                                $col15 = $col[14];
                                $col16 = $col[15];
                                $col17 = $col[16];
                                $col18 = $col[17];   

                            $query="UPDATE sma_emrs_item a SET a.req_qty='".$col9."',a.bal_qty='".$col10."', a.status =".$col18." WHERE a.status = 1";
                            
                          

                        // -------------------------- End Gate Pass syncout code --------------------//

                        // ----------------------- Stock Adjestment code ----------------------------//

                            case "sma_stk_adjt":
                             
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                

                           $select = "SELECT a.id FROM sma_stk_adjt a where a.adjid='".$col1."' and a.doc_id='".$col4."'";

                            $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                             $count = mysqli_num_rows($select_result); 

                             if($count == 0)
                                 { 

                            
                                  $query = "INSERT ignore INTO sma_stk_adjt
                                    (id,org_id,wh_id,doc_id,doc_dt,prj_id,note,adjid) 
                                    VALUES
                                    ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col1."')";

                             }
                                break;

                            case "sma_stk_adjt_itm":
                             
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];

                           $select = "SELECT a.id FROM sma_stk_adjt_itm a where a.adjitmid='".$col1."' and a.doc_id='".$col4."'";

                            $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                             $count = mysqli_num_rows($select_result); 

                             if($count == 0)
                                 { 

                            
                                  $query = "INSERT ignore INTO sma_stk_adjt_itm
                                    (id,org_id,wh_id,doc_id,itm_id,itm_uom,qty,type,adjitmid) 
                                    VALUES
                                    ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col1."')";

                             }
                                break;   

                            case "sma_stk_adjt_lot":
                             
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];

                           $select = "SELECT a.id FROM sma_stk_adjt_lot a where a.adjlotid='".$col1."' and a.doc_id='".$col4."'";

                            $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                             $count = mysqli_num_rows($select_result); 

                             if($count == 0)
                                 { 

                            
                                  $query = "INSERT ignore INTO sma_stk_adjt_lot
                                    (id,org_id,wh_id,doc_id,lot_no,itm_id,itm_uom,qty,type,adjlotid) 
                                    VALUES
                                    ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col1."')";

                             }
                                break;  

                           case "sma_stk_adjt_bin":
                             
                                $col1 = $col[0];
                                $col2 = $col[1];
                                $col3 = $col[2];
                                $col4 = $col[3];
                                $col5 = $col[4];
                                $col6 = $col[5];
                                $col7 = $col[6];
                                $col8 = $col[7];
                                $col9 = $col[8];
                                $col10 = $col[9];

                           $select = "SELECT a.id FROM sma_stk_adjt_bin a where a.adjbinid='".$col1."' and a.doc_id='".$col4."'";

                            $select_result = mysqli_query($remote_connection, $select) or die("Selection Error " . mysqli_error($remote_connection)); 
                             $count = mysqli_num_rows($select_result); 

                             if($count == 0)
                                 { 

                            
                                  $query = "INSERT ignore INTO sma_stk_adjt_bin
                                    (id,org_id,wh_id,doc_id,lot_no,bin_id,itm_id,itm_uom,qty,type,adjbinid) 
                                    VALUES
                                    ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col1."')";

                             }
                                break;           
                        // ----------------------- End Stock Adjestment code -----------------------//    
							 


                            default:
                                  $query="";
                                  $update ="";
                              }

                                if($query!=''){
                                      
                                  $result = mysqli_query($remote_connection, $query) or die("Selection Error " . mysqli_error($remote_connection)); 

                                }
                            }

                        }

                        //echo $table;
                        fclose($handle);
                        //}
                    //}
                       //  $tmpdir = $_SERVER["DOCUMENT_ROOT"].'/'.PROJECT_NAME.'/tmp-folder/';
                       //  $dateWiseTmpDir = $_SERVER["DOCUMENT_ROOT"].'/'.PROJECT_NAME.'/tmp-folder/'.$csv_folder.'/';
                        
                       //  if (!is_dir($tmpdir)) { //create the folder if it's not already exists
                       //      mkdir($tmpdir, 0755, TRUE);                
                       //  }  
                       //  if (!is_dir($dateWiseTmpDir)) {
                       //      mkdir($dateWiseTmpDir, 0755, TRUE);
                       //  }         
                       //  copy($fileName,$dateWiseTmpDir.$projName.".zip");
                       //  unlink($fileName);
                       //  //unlink($remote_file_url);
                       // // $this->site->rrmdir($remote_file_url);
                       //  $this->site->rrmdir($fileName);
                       //  $this->site->rrmdir($directory.$csv_folder."/".$projName);

                        }
                        
                         /************sales data update********************/ 
               $update = "UPDATE sma_sale_items INNER JOIN sma_sales ON (sma_sales.sid = sma_sale_items.sale_id AND sma_sales.warehouse_id = sma_sale_items.warehouse_id) SET sma_sale_items.sale_id = sma_sales.id, sma_sale_items.csv_flag=1 WHERE (sma_sales.sid = sma_sale_items.sale_id AND sma_sales.warehouse_id = sma_sale_items.warehouse_id) AND sma_sale_items.csv_flag=0";

               $update1 = "UPDATE sma_payments JOIN sma_sales ON (sma_sales.sid = sma_payments.sale_id) SET sma_payments.sale_id = sma_sales.id,sma_payments.csv_flag=1 WHERE (sma_sales.created_by = sma_payments.created_by AND sma_payments.csv_flag=0)";


               $update2 = "UPDATE sma_costing JOIN sma_sale_items ON (sma_sale_items.sid = sma_costing.sale_item_id AND sma_sale_items.product_id=sma_costing.product_id) SET sma_costing.sale_id = sma_sale_items.sale_id, sma_costing.sale_item_id = sma_sale_items.id, sma_costing.csv_flag=1 WHERE (sma_sale_items.sid = sma_costing.sale_item_id AND sma_sale_items.product_id=sma_costing.product_id) AND sma_costing.csv_flag=0";

              $update11 = "UPDATE sma_sale_item_lot JOIN sma_sale_items ON (sma_sale_items.sid = sma_sale_item_lot.sale_item_id AND sma_sale_items.warehouse_id=sma_sale_item_lot.warehouse_id AND sma_sale_item_lot.code=sma_sale_items.product_code) SET sma_sale_item_lot.sale_id = sma_sale_items.sale_id, sma_sale_item_lot.sale_item_id = sma_sale_items.id,sma_sale_item_lot.csv_flag=1 WHERE (sma_sale_items.sid = sma_sale_item_lot.sale_item_id AND sma_sale_items.warehouse_id=sma_sale_item_lot.warehouse_id AND sma_sale_item_lot.code=sma_sale_items.product_code) AND sma_sale_item_lot.csv_flag=0";

              $update12 = "UPDATE sma_sale_item_bin JOIN sma_sale_items ON (sma_sale_items.sid = sma_sale_item_bin.sale_item_id AND sma_sale_items.warehouse_id=sma_sale_item_bin.warehouse_id AND sma_sale_item_bin.code=sma_sale_items.product_code) SET sma_sale_item_bin.sale_id = sma_sale_items.sale_id, sma_sale_item_bin.sale_item_id = sma_sale_items.id,sma_sale_item_bin.csv_flag=1 WHERE (sma_sale_items.sid = sma_sale_item_bin.sale_item_id AND sma_sale_items.warehouse_id=sma_sale_item_bin.warehouse_id AND sma_sale_item_bin.code=sma_sale_items.product_code) AND sma_sale_item_bin.csv_flag=0";


            /*********for mrn update*************/
             $update3 = "UPDATE sma_mrn_rcpt_src INNER JOIN sma_mrn_receipt ON (sma_mrn_receipt.mid = sma_mrn_rcpt_src.mrn_rcpt_id  AND sma_mrn_receipt.segment_id = sma_mrn_rcpt_src.segment_id AND sma_mrn_receipt.warehouse_id = sma_mrn_rcpt_src.warehouse_id) SET sma_mrn_rcpt_src.mrn_rcpt_id = sma_mrn_receipt.id, sma_mrn_rcpt_src.csv_flag=1  WHERE (sma_mrn_receipt.mid = sma_mrn_rcpt_src.mrn_rcpt_id  AND sma_mrn_receipt.segment_id = sma_mrn_rcpt_src.segment_id AND sma_mrn_receipt.warehouse_id = sma_mrn_rcpt_src.warehouse_id) AND sma_mrn_rcpt_src.csv_flag=0";

             $update4 = "UPDATE sma_mrn_rcpt_item INNER JOIN sma_mrn_rcpt_src ON (sma_mrn_rcpt_src.sid = sma_mrn_rcpt_item.mrn_rcpt_src_id AND sma_mrn_rcpt_src.segment_id = sma_mrn_rcpt_item.segment_id AND sma_mrn_rcpt_src.warehouse_id = sma_mrn_rcpt_item.warehouse_id) SET sma_mrn_rcpt_item.mrn_rcpt_src_id = sma_mrn_rcpt_src.id,sma_mrn_rcpt_item.csv_flag=1  WHERE (sma_mrn_rcpt_src.sid = sma_mrn_rcpt_item.mrn_rcpt_src_id AND sma_mrn_rcpt_src.segment_id = sma_mrn_rcpt_item.segment_id AND sma_mrn_rcpt_src.warehouse_id = sma_mrn_rcpt_item.warehouse_id) AND sma_mrn_rcpt_item.csv_flag=0";

              $update5 = "UPDATE sma_mrn_rcpt_item_lot INNER JOIN sma_mrn_rcpt_item ON (sma_mrn_rcpt_item.itm_id = sma_mrn_rcpt_item_lot.mrn_rcpt_item_id AND sma_mrn_rcpt_item_lot.segment_id = sma_mrn_rcpt_item.segment_id AND sma_mrn_rcpt_item_lot.warehouse_id = sma_mrn_rcpt_item.warehouse_id) SET sma_mrn_rcpt_item_lot.mrn_rcpt_item_id = sma_mrn_rcpt_item.id,sma_mrn_rcpt_item_lot.csv_flag=1 WHERE (sma_mrn_rcpt_item.itm_id = sma_mrn_rcpt_item_lot.mrn_rcpt_item_id AND sma_mrn_rcpt_item_lot.segment_id = sma_mrn_rcpt_item.segment_id AND sma_mrn_rcpt_item_lot.warehouse_id = sma_mrn_rcpt_item.warehouse_id) AND sma_mrn_rcpt_item_lot.csv_flag=0";

              $update6 = "UPDATE sma_mrn_rcpt_lot_bin SET lid =lot_id where csv_flag=0";

              $update7 = "UPDATE sma_mrn_rcpt_lot_bin INNER JOIN sma_mrn_rcpt_item_lot ON (sma_mrn_rcpt_item_lot.lid = sma_mrn_rcpt_lot_bin.lot_id AND sma_mrn_rcpt_item_lot.segment_id = sma_mrn_rcpt_lot_bin.segment_id AND sma_mrn_rcpt_item_lot.warehouse_id = sma_mrn_rcpt_lot_bin.warehouse_id) SET sma_mrn_rcpt_lot_bin.lot_id = sma_mrn_rcpt_item_lot.id, sma_mrn_rcpt_lot_bin.csv_flag=1 WHERE (sma_mrn_rcpt_item_lot.lid = sma_mrn_rcpt_lot_bin.lot_id AND sma_mrn_rcpt_item_lot.segment_id = sma_mrn_rcpt_lot_bin.segment_id AND sma_mrn_rcpt_item_lot.warehouse_id = sma_mrn_rcpt_lot_bin.warehouse_id) AND sma_mrn_rcpt_lot_bin.csv_flag=0";

              /************************for STOCK UPDATE***********************/

              $update8 = "UPDATE sma_item_stk_lot INNER JOIN sma_item_stk_profile ON (sma_item_stk_profile.sid = sma_item_stk_lot.item_stk_id AND sma_item_stk_lot.warehouse_id=sma_item_stk_profile.warehouse_id AND sma_item_stk_lot.segment_id=sma_item_stk_profile.segment_id AND sma_item_stk_profile.item_id = sma_item_stk_lot.item_id) SET sma_item_stk_lot.item_stk_id = sma_item_stk_profile.id,sma_item_stk_lot.csv_flag=1 WHERE (sma_item_stk_profile.warehouse_id = sma_item_stk_lot.warehouse_id AND sma_item_stk_profile.segment_id = sma_item_stk_lot.segment_id AND sma_item_stk_profile.item_id = sma_item_stk_lot.item_id) AND sma_item_stk_lot.csv_flag=0";
              
              $update9 = "UPDATE sma_item_stk_lot_bin INNER JOIN sma_item_stk_lot ON (sma_item_stk_lot.lid = sma_item_stk_lot_bin.lot_id AND sma_item_stk_lot_bin.warehouse_id=sma_item_stk_lot.warehouse_id AND sma_item_stk_lot_bin.segment_id=sma_item_stk_lot.segment_id) SET sma_item_stk_lot_bin.lot_id = sma_item_stk_lot.id, sma_item_stk_lot_bin.csv_flag=1 WHERE (sma_item_stk_lot.warehouse_id = sma_item_stk_lot_bin.warehouse_id AND sma_item_stk_lot.segment_id = sma_item_stk_lot_bin.segment_id AND sma_item_stk_lot.item_id = sma_item_stk_lot_bin.item_id AND sma_item_stk_lot.lid = sma_item_stk_lot_bin.lot_id) AND sma_item_stk_lot_bin.csv_flag=0";
              $update10 = "UPDATE sma_sales a INNER JOIN sma_companies b ON (a.customer = b.name) inner JOIN sma_warehouses c ON (c.id = a.warehouse_id) SET a.customer_id = b.id WHERE CASE WHEN length(a.biller_id)=10 OR b.segment_id = c.segment_id THEN a.biller_id = b.phone ELSE a.biller_id = b.eo_id END";
              
               /*********for GP update*************/

              $update22 = "UPDATE sma_gp_src a INNER JOIN sma_gp b ON (a.gp_no = b.gp_no AND a.org_id = b.org_id AND a.wh_id = b.wh_id) SET a.gp_id = b.id WHERE (a.gp_no = b.gp_no AND a.org_id = b.org_id AND a.wh_id = b.wh_id) AND (a.sync_flg IS NULL OR a.sync_flg='')";
              $update23 = "UPDATE sma_gp_item a INNER JOIN sma_gp_src b ON (a.gp_no = b.gp_no AND a.org_id = b.org_id AND a.wh_id = b.wh_id) SET a.gp_src_id = b.id WHERE (a.gp_no = b.gp_no AND a.org_id = b.org_id AND a.wh_id = b.wh_id) AND (a.sync_flg IS NULL OR a.sync_flg='')";
              $update24 = "UPDATE sma_gp_item_lot a INNER JOIN sma_gp_item b ON (a.gp_no = b.gp_no AND a.org_id = b.org_id AND a.wh_id = b.wh_id AND a.code = b.code) SET a.mrn_gp_item_id = b.id WHERE (a.gp_no = b.gp_no AND a.org_id = b.org_id AND a.wh_id = b.wh_id AND a.code = b.code) AND (a.sync_flg IS NULL OR a.sync_flg='')";
              $update25 = "UPDATE sma_gp_lot_bin a INNER JOIN sma_gp_item_lot b ON (a.gp_no = b.gp_no AND a.org_id = b.org_id AND a.wh_id = b.wh_id AND a.code = b.code AND a.lot_no = b.lot_no) SET a.lot_id = b.id WHERE (a.gp_no = b.gp_no AND a.org_id = b.org_id AND a.wh_id = b.wh_id AND a.code = b.code AND a.lot_no = b.lot_no) AND (a.sync_flg IS NULL OR a.sync_flg='')";
              
              $update26 = "UPDATE sma_cons_item_stk_lot a INNER JOIN sma_cons_item_stk_profile b ON (a.sid = b.id AND a.org_id = b.org_id AND a.wh_id = b.wh_id AND a.code = b.code) SET a.item_stk_id = b.id WHERE (a.sid = b.id AND a.org_id = b.org_id AND a.wh_id = b.wh_id AND a.code = b.code) AND (a.create_flg='1')";
              $update27 = "UPDATE sma_cons_item_stk_lot_bin a INNER JOIN sma_cons_item_stk_lot b ON (a.org_id = b.org_id AND a.wh_id = b.wh_id AND a.code = b.code AND a.lot_no = b.lot_no) SET a.lot_id = b.id WHERE (a.org_id = b.org_id AND a.wh_id = b.wh_id AND a.code = b.code AND a.lot_no = b.lot_no) AND (a.create_flg='1')";			  
// ================================================= GP END=======================================
			 


              $update_cheque = "UPDATE sma_cheque_details INNER JOIN sma_sales ON (sma_sales.sid = sma_cheque_details.sale_id AND sma_sales.warehouse_id = sma_cheque_details.warehouse_id) SET sma_cheque_details.sale_id = sma_sales.id WHERE (sma_sales.sid = sma_cheque_details.sale_id AND sma_sales.warehouse_id = sma_cheque_details.warehouse_id)";
              $result = mysqli_query($remote_connection, $update_cheque) or die("Selection Error " . mysqli_error($remote_connection)); 
             if($update!='' && $update1 !=''){


                    $result = mysqli_query($remote_connection, $update) or die("Selection Error " . mysqli_error($remote_connection)); 
                    $result = mysqli_query($remote_connection, $update1) or die("Selection Error " . mysqli_error($remote_connection)); 
                    $result = mysqli_query($remote_connection, $update2) or die("Selection Error " . mysqli_error($remote_connection)); 
                    $result = mysqli_query($remote_connection, $update3) or die("Selection Error " . mysqli_error($remote_connection)); 
                    $result = mysqli_query($remote_connection, $update4) or die("Selection Error " . mysqli_error($remote_connection)); 
                    $result = mysqli_query($remote_connection, $update5) or die("Selection Error " . mysqli_error($remote_connection)); 
                    $result = mysqli_query($remote_connection, $update6) or die("Selection Error " . mysqli_error($remote_connection)); 
                    $result = mysqli_query($remote_connection, $update7) or die("Selection Error " . mysqli_error($remote_connection)); 
                    $result = mysqli_query($remote_connection, $update8) or die("Selection Error " . mysqli_error($remote_connection)); 
                    $result = mysqli_query($remote_connection, $update9) or die("Selection Error " . mysqli_error($remote_connection));
                    $result = mysqli_query($remote_connection, $update11) or die("Selection Error " . mysqli_error($remote_connection)); 
                    $result = mysqli_query($remote_connection, $update12) or die("Selection Error " . mysqli_error($remote_connection));
                    $result = mysqli_query($remote_connection, $update_cheque) or die("Selection Error " . mysqli_error($remote_connection));  
                    
                    $result = mysqli_query($remote_connection, $update22) or die("Selection Error " . mysqli_error($remote_connection));
                    $result = mysqli_query($remote_connection, $update23) or die("Selection Error " . mysqli_error($remote_connection));
                    $result = mysqli_query($remote_connection, $update24) or die("Selection Error " . mysqli_error($remote_connection));
                    $result = mysqli_query($remote_connection, $update25) or die("Selection Error " . mysqli_error($remote_connection));
                    $result = mysqli_query($remote_connection, $update26) or die("Selection Error " . mysqli_error($remote_connection));
                    $result = mysqli_query($remote_connection, $update27) or die("Selection Error " . mysqli_error($remote_connection));					


                    $tmpdir = $_SERVER["DOCUMENT_ROOT"].'/'.PROJECT_NAME.'/tmpout-folder/';
                       //  $dateWiseTmpDir = $_SERVER["DOCUMENT_ROOT"].'/'.PROJECT_NAME.'/tmp-folder/'.$csv_folder.'/';

                        
                    $fileName = $file_name;
                   /* echo $file_name."<br>";
                    echo $folder;exit;*/
                    copy($file_name,$tmpdir.$folderName);
                    $this->site->rrmdir($file_name);
                    $this->site->rrmdir($folder);
                    $q1 = "UPDATE sma_syncout_history SET flag = '1' WHERE id='".$fileRecord['id']."' ";
                    mysqli_query($remote_connection, $q1);

                }
            }
        }
           // die('here');
            $this->session->set_flashdata('message', "Congratulations!!!! Data Synced Successfully");
            redirect('welcome');
        }
        $this->page_construct('syncin', $meta, $this->data);        
    }

    function restructuredata(){   

      $warehouseList = $this->db->select('warehouse_id as warehouseList')->from('sales')->order_by('warehouse_id',ASC)->group_by(
        'warehouse_id')->get()->result();
      
      if (count($warehouseList)>0) {
       
        $this->msync_model->restructureData($warehouseList);
      }else{
        echo "asdas";exit;
      }

    }

    // for missing location invoices entry.
function recover_invoices(){
   
     $remote_host = REMOTE_HOST;
     $remote_username = REMOTE_USERNAME;
     $remote_password = REMOTE_PWD;
     $remote_dbname = REMOTE_DB;

     // $remote_host = 'localhost';
     // $remote_username = 'root';
     // $remote_password = '';
     // $remote_dbname = 'iffco_central';

     $remote_connection = mysqli_connect($remote_host, $remote_username, $remote_password, $remote_dbname) or die("Connection Error " . mysqli_error($remote_connection)); 

     if($remote_connection === false){
        die("ERROR: Could not connect. " . mysqli_connect_error());
     }
    
     $query= $this->db->query("SELECT id FROM `sma_warehouses` where name like '%GODOWN%' LIMIT 1");
     $warehouse_id1=$query->result();
     $warehouse_id= $warehouse_id1[0]->id;
     $segment_id = $this->session->userdata('segment_id');

// sales table data 

     $salesdata =$this->db->select('*')
                ->from('sma_sales')
                ->get();
                
     $salesdata1 = $salesdata->result_array();

     foreach ($salesdata1 as $salesdatas) {
        
          $col=array_values($salesdatas);
          
                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];
                $col4 = $col[3];
                $col5 = $col[4];
                $col6 = $col[5];
                $col7 = $col[6];
                $col8 = $col[7];
                $col9 = $col[8];
                $col10 = $col[9];
                $col11 = $col[10];
                $col12 = $col[11];
                $col13 = $col[12];
                $col14 = $col[13];
                $col15 = $col[14];
                $col16 = $col[15];
                $col17 = $col[16];
                $col18 = $col[17];
                $col19 = $col[18];
                $col20 = $col[19];
                $col21 = $col[20];
                $col22 = $col[21];
                $col23 = $col[22];
                $col24 = $col[23];
                $col25 = $col[24];
                $col26 = $col[25];
                $col27 = $col[26];
                $col28 = $col[27];
                $col29 = $col[28];
                $col30 = $col[29];
                $col31 = $col[30];
                $col32 = $col[31];
                $col33 = $col[32];
                $col34 = $col[33];
                $col35 = $col[34];
                $col36 = $col[35];
                $col37 = $col[36];

          $sql2= "select sid from sma_sales WHERE warehouse_id='".$col8."' AND sid='".$col1."'";
          $select_result = mysqli_query($remote_connection, $sql2) or die("Selection Error " . mysqli_error($remote_connection)); 
          $count = mysqli_num_rows($select_result);

              if($count == 0)
                {
                  $query = "INSERT ignore INTO sma_sales
                    (id,date,reference_no,customer_id,customer,biller_id,biller,warehouse_id,note,staff_note,total,product_discount,order_discount_id,total_discount,order_discount,product_tax,order_tax_id,order_tax,total_tax,shipping,grand_total,sale_status,payment_status,payment_term,due_date,created_by,updated_by,updated_at,total_items,pos,paid,return_id,surcharge,attachment,sid,manual,manual_inv_no) 
                    VALUES
                    ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col24."','".$col25."','".$col26."','".$col27."','".$col28."','".$col29."','".$col30."','".$col31."','".$col32."','".$col33."','".$col34."','".$col1."','".$col36."','".$col37."')"; 
                mysqli_query($remote_connection, $query);
                $this->db->query("UPDATE sma_sales SET sync_flg=1 WHERE id='".$col1."'");   

                }
         

          } 

    // sma_sale_items table data 

     $sma_sale_items = $this->db->select('*')
                ->from('sma_sale_items')
                ->get();
                
     $sma_sale_items1 = $sma_sale_items->result_array();

     foreach ($sma_sale_items1 as $sma_sale_itemss) {
        
          $col=array_values($sma_sale_itemss);
          
                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];
                $col4 = $col[3];
                $col5 = $col[4];
                $col6 = $col[5];
                $col7 = $col[6];
                $col8 = $col[7];
                $col9 = $col[8];
                $col10 = $col[9];
                $col11 = $col[10];
                $col12 = $col[11];
                $col13 = $col[12];
                $col14 = $col[13];
                $col15 = $col[14];
                $col16 = $col[15];
                $col17 = $col[16];
                $col18 = $col[17];
                $col19 = $col[18];
                $col20 = $col[19];
                $col21 = $col[20];
                $col22 = $col[21];
                $col23 = $col[22];
                $col24 = $col[23];

          $sql2= "SELECT a.id FROM sma_sale_items a where a.sid='".$col1."' AND a.warehouse_id=".$col13;
          $select_result = mysqli_query($remote_connection, $sql2) or die("Selection Error " . mysqli_error($remote_connection)); 
          $count = mysqli_num_rows($select_result);

              if($count == 0)
                {
                
                 // $select1 = "SELECT id FROM `sma_sales` WHERE warehouse_id =".$col13." and sid =".$col2." LIMIT 1";
                 // $select_result1 = mysqli_query($remote_connection, $select1) or die("Selection Error " . mysqli_error($remote_connection)); 
                 // $sid = mysqli_fetch_array($select_result1);

                 // $item_sid= $sid['id'];

                $query = "INSERT ignore INTO sma_sale_items(id,sale_id,product_id,product_code,product_name,item_uom_id,product_type,option_id,net_unit_price,unit_price,quantity,expiry,  warehouse_id,item_tax,tax_rate_id,tax,discount,item_discount,subtotal,lot_no,bin_id,serial_no,real_unit_price,sid,itm_id) VALUES('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col1."','".$col2."')"; 
                                   
                mysqli_query($remote_connection, $query);
                $this->db->query("UPDATE sma_sale_items SET sync_flg=1 WHERE id='".$col1."'");   

                }
         

          } 

    // sma_payments table data 

     $sma_payments = $this->db->select('*')
                ->from('sma_payments')
                ->get();
                
     $sma_payments1 = $sma_payments->result_array();

     foreach ($sma_payments1 as $sma_paymentss) {
        
          $col=array_values($sma_paymentss);
          
                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];
                $col4 = $col[3];
                $col5 = $col[4];
                $col6 = $col[5];
                $col7 = $col[6];
                $col8 = $col[7];
                $col9 = $col[8];
                $col10 = $col[9];
                $col11 = $col[10];
                $col12 = $col[11];
                $col13 = $col[12];
                $col14 = $col[13];
                $col15 = $col[14];
                $col16 = $col[15];
                $col17 = $col[16];
                $col18 = $col[17];
                $col19 = $col[18];
                $col20 = $col[19];
                $col21 = $col[20];
                $col22 = $col[21];
                $col23 = $col[22];
                $col24 = $col[23];

          $sql2= "SELECT a.id FROM sma_payments a where a.pid ='".$col1."' AND a.created_by ='".$col18."'";
          $select_result = mysqli_query($remote_connection, $sql2) or die("Selection Error " . mysqli_error($remote_connection)); 
          $count = mysqli_num_rows($select_result);

              if($count == 0)
                {
                // $select1 = "SELECT id FROM `sma_sales` WHERE created_by =".$col18." and sid =".$col3." LIMIT 1";
                //  $select_result1 = mysqli_query($remote_connection, $select1) or die("Selection Error " . mysqli_error($remote_connection)); 
                //  $sid = mysqli_fetch_array($select_result1);

                //  $item_sid= $sid['id'];
                  $query = "INSERT ignore INTO sma_payments
                    (id,date,sale_id,return_id,purchase_id,reference_no,transaction_id,paid_by,cheque_no,cc_no,cv_no,cc_holder,cc_month,cc_year,cc_type,amount,currency,created_by,attachment,type,note,pos_paid,pos_balance,pid,sid) 
                    VALUES
                    ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col1."','".$col3."')"; 
                    
                mysqli_query($remote_connection, $query);
                $this->db->query("UPDATE sma_payments SET sync_flg=1 WHERE id='".$col1."'");   

                }
         

          } 

    // sma_costing table data 

     $sma_costing = $this->db->select('*')
                ->from('sma_costing')
                ->get();
                
     $sma_costing1 = $sma_costing->result_array();

     foreach ($sma_costing1 as $sma_costings) {
        
          $col=array_values($sma_costings);
          
                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];
                $col4 = $col[3];
                $col5 = $col[4];
                $col6 = $col[5];
                $col7 = $col[6];
                $col8 = $col[7];
                $col9 = $col[8];
                $col10 = $col[9];
                $col11 = $col[10];
                $col12 = $col[11];
                $col13 = $col[12];
                $col14 = $col[13];
                $col15 = $col[14];
                $col16 = $col[15];
                $col17 = $col[16];

          $sql2= "SELECT a.id FROM sma_costing a where a.cid='".$col1."' and a.itm_id='".$col4."' and a.product_id='".$col3."'";
          $select_result = mysqli_query($remote_connection, $sql2) or die("Selection Error " . mysqli_error($remote_connection)); 
          $count = mysqli_num_rows($select_result);

              if($count == 0)
                {
                   $query = "INSERT ignore INTO sma_costing(id,date,product_id,sale_item_id,sale_id,purchase_item_id,quantity,purchase_net_unit_cost,purchase_unit_cost,sale_net_unit_price,sale_unit_price,quantity_balance,inventory,overselling,option_id,cid,sid,itm_id) VALUES('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col17."','".$col1."','".$col5."','".$col4."')";    
                mysqli_query($remote_connection, $query);
                $this->db->query("UPDATE sma_costing SET sync_flg=1 WHERE id='".$col1."'");   

                }
          } 

    // sma_sale_item_lot table data 

     $sma_sale_item_lot = $this->db->select('*')
                ->from('sma_sale_item_lot')
                ->get();
                
     $sma_sale_item_lot1 = $sma_sale_item_lot->result_array();

     foreach ($sma_sale_item_lot1 as $sma_sale_item_lots) {
        
          $col=array_values($sma_sale_item_lots);
          
                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];
                $col4 = $col[3];
                $col5 = $col[4];
                $col6 = $col[5];
                $col7 = $col[6];
                $col8 = $col[7];
                $col9 = $col[8];
                $col10 = $col[9];
                $col11 = $col[10];

          $sql2= "SELECT a.id FROM sma_sale_item_lot a where a.lid='".$col1."' and a.warehouse_id='".$col4."'";
          $select_result = mysqli_query($remote_connection, $sql2) or die("Selection Error " . mysqli_error($remote_connection)); 
          $count = mysqli_num_rows($select_result);

              if($count == 0)
                {

               $query = "INSERT ignore INTO sma_sale_item_lot(id,org_id,wh_id,warehouse_id,code,sale_id,sale_item_id,lot_id,lot_no,quantity,created_date,lid,sid,itm_id) VALUES('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col1."','".$col6."','".$col7."')";
   
                mysqli_query($remote_connection, $query);
                $this->db->query("UPDATE sma_sale_item_lot SET sync_flg=1 WHERE id='".$col1."'");   

                }
          }  

   // sma_sale_item_bin table data

    $sma_sale_item_bin = $this->db->select('*')
                ->from('sma_sale_item_bin')
                ->get();
                
     $sma_sale_item_bin1 = $sma_sale_item_bin->result_array();

     foreach ($sma_sale_item_bin1 as $sma_sale_item_bins) {
        
          $col=array_values($sma_sale_item_bins);
          
                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];
                $col4 = $col[3];
                $col5 = $col[4];
                $col6 = $col[5];
                $col7 = $col[6];
                $col8 = $col[7];
                $col9 = $col[8];
                $col10 = $col[9];
                $col11 = $col[10];
                $col12 = $col[11];

          $sql2= "SELECT a.id FROM sma_sale_item_bin a where a.bid='".$col1."' and a.warehouse_id='".$col4."'";
          $select_result = mysqli_query($remote_connection, $sql2) or die("Selection Error " . mysqli_error($remote_connection)); 
          $count = mysqli_num_rows($select_result);

              if($count == 0)
                {
                    
                $query = "INSERT ignore INTO sma_sale_item_bin(id,org_id,wh_id,warehouse_id,code,sale_id,sale_item_id,lot_id,bin_id,bin_name,quantity,created_date,bid,sid,itm_id) VALUES('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col1."','".$col6."','".$col7."')"; 
                mysqli_query($remote_connection, $query);
                $this->db->query("UPDATE sma_sale_item_bin SET sync_flg=1 WHERE id='".$col1."'");   

                }
          } 

    // sma_companies table data
	
    $sma_companies = $this->db->select('*')
                ->from('sma_companies')
                ->where('segment_id !=', 0)
                ->where('name !=', 'Walk-in Customer')
                ->where('group_name', 'customer')
                ->get();
                
     $sma_companies1 = $sma_companies->result_array();

     foreach ($sma_companies1 as $sma_companiess) {
        
          $col=array_values($sma_companiess);
          
                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];
                $col4 = $col[3];
                $col5 = $col[4];
                $col6 = $col[5];
                $col7 = $col[6];
                $col8 = $col[7];
                $col9 = $col[8];
                $col10 = $col[9];
                $col11 = $col[10];
                $col12 = $col[11];
                $col13 = $col[12];
                $col14 = $col[13];
                $col15 = $col[14];
                $col16 = $col[15];
                $col17 = $col[16];
                $col18 = $col[17];
                $col19 = $col[18];
                $col20 = $col[19];
                $col21 = $col[20];
                $col22 = $col[21];
                $col23 = $col[22];
                $col24 = $col[23];
                $col25 = $col[24];
                $col26 = $col[25];
                $col27 = $col[26];
                $col28 = $col[27];
                $col29 = $col[28];
                $col30 = $col[29];
                $col31 = $col[30];
                $col32 = $col[31];
                $col33 = $col[32];
                $col34 = $col[33];
                $col35 = $col[34];
                $col36 = $col[35];
                $col37 = $col[36];
                $col38 = $col[37];
                $col39 = $col[38];
                $col40 = $col[39];
                $col41 = $col[40];
                $col42 = $col[41];
                $col43 = $col[42];
                $col44 = $col[43];
                $col45 = $col[44];
                $col46 = $col[45];
                $col47 = $col[46];
                $col48 = $col[47];
                $col49 = $col[48];
                $col50 = $col[49];
                $col51 = $col[50];
                $col52 = $col[51];

          $sql2= "SELECT id FROM sma_companies where cid ='".$col1."' AND segment_id='".$col48."'";
          $select_result = mysqli_query($remote_connection, $sql2) or die("Selection Error " . mysqli_error($remote_connection)); 
          $count = mysqli_num_rows($select_result);

              if($count == 0)
                {
                    
                $query = "INSERT ignore INTO sma_companies(id,group_id,group_name,customer_group_id,customer_group_name,name,father_name,company,vat_no,address,city,state,postal_code,country,country_id,state_id,phone,email,id_proof,id_proof_no,loyalty_card_id,nominees1,dob_nominees1,nominees2,dob_nominees2,nominees3,dob_nominees3,nominees4,dob_nominees4,nominees5,dob_nominees5,cf1,cf2,cf3,cf4,cf5,cf6,invoice_footer,payment_term,logo,award_points,upd_flg,eo_type,eo_id,org_id,wh_id,biller_id,segment_id,cid,adhar_card_no) VALUES('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col24."','".$col25."','".$col26."','".$col27."','".$col28."','".$col29."','".$col30."','".$col31."','".$col32."','".$col33."','".$col34."','".$col35."','".$col36."','".$col37."','".$col38."','".$col39."','".$col40."','".$col41."','".$col42."','".$col43."','".$col44."','".$col45."','".$col46."','".$col47."','".$col48."','".$col1."','".$col52."')";
                mysqli_query($remote_connection, $query);
                $this->db->query("UPDATE sma_companies SET sync_flg=1 WHERE id='".$col1."'"); 
                }
          }  



    // sma_mrn_receipt table data
    $sma_mrn_receipt = $this->db->select('*')
                ->from('sma_mrn_receipt')
                ->get();
                
     $sma_mrn_receipt1 = $sma_mrn_receipt->result_array();

     foreach ($sma_mrn_receipt1 as $sma_mrn_receipts) {
        
          $col=array_values($sma_mrn_receipts);
          
                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];
                $col4 = $col[3];
                $col5 = $col[4];
                $col6 = $col[5];
                $col7 = $col[6];
                $col8 = $col[7];
                $col9 = $col[8];
                $col10 = $col[9];
                $col11 = $col[10];
                $col12 = $col[11];
                $col13 = $col[12];
                $col14 = $col[13];
                $col15 = $col[14];
                $col16 = $col[15];
                $col17 = $col[16];
                $col18 = $col[17];
                $col19 = $col[18];
                $col20 = $col[19];
                $col21 = $col[20];
                $col22 = $col[21];
                $col23 = $col[22];
                $col24 = $col[23];
                $col25 = $col[24];
                $col26 = $col[25];
                $col27 = $col[26];
                $col28 = $col[27];
                $col29 = $col[28];
                $col30 = $col[29];
                $col31 = $col[30];

          $sql2= "SELECT a.id FROM sma_mrn_receipt a where a.mid ='".$col1."' AND a.segment_id='".$col4."'";
          $select_result = mysqli_query($remote_connection, $sql2) or die("Selection Error " . mysqli_error($remote_connection)); 
          $count = mysqli_num_rows($select_result);

              if($count == 0)
                {
                    
                $query = "INSERT ignore INTO sma_mrn_receipt(id,ho_id,org_id,segment_id,wh_id,warehouse_id,fy_id,rcpt_no,rcpt_dt,rcpt_src_type, ge_doc_id,supplier_id,org_id_src,wh_id_src,dn_no,dn_dt,sto_no,sto_date,tp_id,tpt_lr_no,tpt_lr_dt,vehicle_no,status,rmda_stat,addl_amt,curr_id,remarks,user_id,created_date,mod_date,mid) VALUES('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col24."','".$col25."','".$col26."','".$col28."','".$col29."','".$col30."','".$col31."','".$col1."')";  
                mysqli_query($remote_connection, $query);
                $this->db->query("UPDATE sma_mrn_receipt SET sync_flg=1 WHERE id='".$col1."'"); 
                }
          }  

    // sma_mrn_rcpt_src table data
    
    $sma_mrn_rcpt_src = $this->db->select('*')
                ->from('sma_mrn_rcpt_src')
                ->get();
                
     $sma_mrn_rcpt_src1 = $sma_mrn_rcpt_src->result_array();

     foreach ($sma_mrn_rcpt_src1 as $sma_mrn_rcpt_srcs) {
        
          $col=array_values($sma_mrn_rcpt_srcs);
          
                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];
                $col4 = $col[3];
                $col5 = $col[4];
                $col6 = $col[5];
                $col7 = $col[6];
                $col8 = $col[7];
                $col9 = $col[8];
                $col10 = $col[9];
                $col11 = $col[10];
                $col12 = $col[11];
                $col13 = $col[12];
                $col15 = $col[14];

          $sql2= "SELECT id FROM sma_mrn_rcpt_src where sid =".$col1." AND segment_id=".$col3;
          $select_result = mysqli_query($remote_connection, $sql2) or die("Selection Error " . mysqli_error($remote_connection)); 
          $count = mysqli_num_rows($select_result);

              if($count == 0)
                {
                    
                $query = "INSERT ignore INTO sma_mrn_rcpt_src(id,org_id,segment_id,wh_id,warehouse_id,mrn_rcpt_id,po_id,po_date,ro_no,ro_date,gatepass_no,gatepass_date,use_rented_wh,sid,rcpt_no) VALUES('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col1."','".$col15."')"; 
                                
                mysqli_query($remote_connection, $query);
                $this->db->query("UPDATE sma_mrn_rcpt_src SET sync_flg=1 WHERE id='".$col1."'"); 
                }
          }                
    
    // sma_mrn_rcpt_item table data
    
    $sma_mrn_rcpt_item = $this->db->select('*')
                ->from('sma_mrn_rcpt_item')
                ->get();
                
     $sma_mrn_rcpt_item1 = $sma_mrn_rcpt_item->result_array();

     foreach ($sma_mrn_rcpt_item1 as $sma_mrn_rcpt_items) {
        
          $col=array_values($sma_mrn_rcpt_items);
          
                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];
                $col4 = $col[3];
                $col5 = $col[4];
                $col6 = $col[5];
                $col7 = $col[6];
                $col8 = $col[7];
                $col9 = $col[8];
                $col10 = $col[9];
                $col11 = $col[10];
                $col12 = $col[11];
                $col13 = $col[12];
                $col14 = $col[13];
                $col15 = $col[14];
                $col16 = $col[15];
                $col17 = $col[16];
                $col18 = $col[17];
                $col19 = $col[18];
                $col20 = $col[19];
                $col21 = $col[20];
                $col24 = $col[23];
                $col25 = $col[24];
          $sql2= "SELECT id FROM sma_mrn_rcpt_item where itm_id =".$col1." AND segment_id=".$col3;
          $select_result = mysqli_query($remote_connection, $sql2) or die("Selection Error " . mysqli_error($remote_connection)); 
          $count = mysqli_num_rows($select_result);

              if($count == 0)
                {
                    
                $query = "INSERT ignore INTO sma_mrn_rcpt_item(id,org_id,segment_id,wh_id,warehouse_id,mrn_rcpt_src_id,po_id,po_date,dlv_schdl_no,item_id,code,item_uom_id,pending_qty,dlv_note_qty,tot_rcpt_qty,rwk_qty,rej_qty,rcpt_qty,purchase_price,created_date,itm_id,rcpt_no,ro_no) VALUES('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."',NULL,'".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col21."','".$col1."','".$col24."','".$col25."')";             
                mysqli_query($remote_connection, $query);
                $this->db->query("UPDATE sma_mrn_rcpt_item SET sync_flg=1 WHERE id='".$col1."'"); 
                }
          } 

    // sma_mrn_rcpt_item_lot table data
    
    $sma_mrn_rcpt_item_lot = $this->db->select('*')
                ->from('sma_mrn_rcpt_item_lot')
                ->get();
                
     $sma_mrn_rcpt_item_lot1 = $sma_mrn_rcpt_item_lot->result_array();

     foreach ($sma_mrn_rcpt_item_lot1 as $sma_mrn_rcpt_item_lots) {
        
          $col=array_values($sma_mrn_rcpt_item_lots);
          
                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];
                $col4 = $col[3];
                $col5 = $col[4];
                $col6 = $col[5];
                $col7 = $col[6];
                $col8 = $col[7];
                $col9 = $col[8];
                $col10 = $col[9];
                $col11 = $col[10];
                $col12 = $col[11];
                $col13 = $col[12];
                $col14 = $col[13];
                $col15 = $col[14];
                $col16 = $col[15];
                $col17 = $col[16];
                $col18 = $col[17];
                $col19 = $col[18];
                $col20 = $col[19];
                $col23 = $col[22];
                $col24 = $col[23];
          $sql2= "SELECT id FROM sma_mrn_rcpt_item_lot where lid =".$col1." AND segment_id=".$col3;
          $select_result = mysqli_query($remote_connection, $sql2) or die("Selection Error " . mysqli_error($remote_connection)); 
          $count = mysqli_num_rows($select_result);

              if($count == 0)
                {
                    
                $query = "INSERT ignore INTO sma_mrn_rcpt_item_lot(id,org_id,segment_id,wh_id,warehouse_id,mrn_rcpt_item_id,lot_no,dlv_schdl_no,item_id,code,itm_uom_id,lot_qty,rej_qty,rewrk_qty,mfg_dt,expiry_dt,batch_no,itm_uom_bs,created_date,lid,ro_no,rcpt_no)VALUES('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."',NULL,'".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col1."','".$col23."','".$col24."')"; 
                                             
                mysqli_query($remote_connection, $query);
                $this->db->query("UPDATE sma_mrn_rcpt_item_lot SET sync_flg=1 WHERE id='".$col1."'"); 
                }
          } 

    // sma_mrn_rcpt_lot_bin table data
    
    $sma_mrn_rcpt_lot_bin = $this->db->select('*')
                ->from('sma_mrn_rcpt_lot_bin')
                ->get();
                
     $sma_mrn_rcpt_lot_bin1 = $sma_mrn_rcpt_lot_bin->result_array();

     foreach ($sma_mrn_rcpt_lot_bin1 as $sma_mrn_rcpt_lot_bins) {
        
          $col=array_values($sma_mrn_rcpt_lot_bins);
          
                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];
                $col4 = $col[3];
                $col5 = $col[4];
                $col6 = $col[5];
                $col7 = $col[6];
                $col8 = $col[7];
                $col9 = $col[8];
                $col10 = $col[9];
                $col11 = $col[10];
                $col12 = $col[11];
                $col13 = $col[12];
                $col14 = $col[13];
                $col15 = $col[14];
                $col16 = $col[15];
                $col19 = $col[18];
                $col20 = $col[19];

          $sql2= "SELECT a.id FROM sma_mrn_rcpt_lot_bin a where a.segment_id='".$col3."' AND a.bid='".$col1."'";
          $select_result = mysqli_query($remote_connection, $sql2) or die("Selection Error " . mysqli_error($remote_connection)); 
          $count = mysqli_num_rows($select_result);

              if($count == 0)
                {
                    
               $query = "INSERT ignore INTO sma_mrn_rcpt_lot_bin(id,org_id,segment_id,wh_id,warehouse_id,lot_id,item_id,code,itm_uom_id,bin_id,bin_qty,itm_uom_bs,rej_qty,rewrk_qty,created_date,bid,ro_no,rcpt_no)VALUES ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col1."','".$col19."','".$col20."')"; 
                                               
                mysqli_query($remote_connection, $query);
                $this->db->query("UPDATE sma_mrn_rcpt_lot_bin SET sync_flg=1 WHERE id='".$col1."'"); 
                }
          } 

    // sma_drft_po table data
    
    $sma_drft_po = $this->db->select('*')
                ->from('sma_drft_po')
                ->get();
                
     $sma_drft_po1 = $sma_drft_po->result_array();

     foreach ($sma_drft_po1 as $sma_drft_pos) {
        
          $col=array_values($sma_drft_pos);
          
                $col3 = $col[2];
                $col4 = $col[3];
                $col14 = $col[13];
                $col18 = $col[17];
                $col20 = $col[19];

               $query= "UPDATE sma_drft_po SET po_status =".$col18." WHERE po_id ='".$col3."' AND po_dt ='".$col4."' AND auth_po_no='".$col14."' AND doc_id ='".$col20."'";                              
                mysqli_query($remote_connection, $query);
                
          }        

    //sma_drft_po_dlv_schdl table data
    
    $sma_drft_po_dlv_schdl = $this->db->select('*')
                ->from('sma_drft_po_dlv_schdl')
                ->get();
                
     $sma_drft_po_dlv_schdl1 = $sma_drft_po_dlv_schdl->result_array();

     foreach ($sma_drft_po_dlv_schdl1 as $sma_drft_po_dlv_schdls) {
        
          $col=array_values($sma_drft_po_dlv_schdls);
          
                $col3 = $col[2];
                $col4 = $col[3];
                $col17 = $col[16];
                $col20 = $col[19];
                $col21 = $col[20];
                $col23 = $col[22];
                $col24 = $col[23];
                $col25 = $col[24];
                $col27 = $col[26];

               $query="UPDATE sma_drft_po_dlv_schdl a INNER JOIN sma_drft_po b ON (b.id=a.po_id) SET a.bal_qty='".$col17."', a.status =".$col27." WHERE a.po_id ='".$col3."' AND a.ro_no ='".$col23."' AND a.ro_dt ='".$col24."' AND a.item_id ='".$col25."' AND a.warehouse_id=".$col21." AND a.segment_id =".$col20." AND b.po_status=218 AND (b.id=a.po_id)";                                        
                mysqli_query($remote_connection, $query);
                
          }

  // sma_item_stk_profile table data
    
    $sma_item_stk_profile = $this->db->select('*')
                ->from('sma_item_stk_profile')
                ->get();
                
     $sma_item_stk_profile1 = $sma_item_stk_profile->result_array();

     foreach ($sma_item_stk_profile1 as $sma_item_stk_profiles) {
        
          $col=array_values($sma_item_stk_profiles);
          
                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];
                $col4 = $col[3];
                $col5 = $col[4];
                $col6 = $col[5];
                $col7 = $col[6];
                $col8 = $col[7];
                $col9 = $col[8];
                $col10 = $col[9];
                $col11 = $col[10];
                $col12 = $col[11];
                $col13 = $col[12];
                $col14 = $col[13];
                $col15 = $col[14];
                $col16 = $col[15];
                $col17 = $col[16];
                $col18 = $col[17];
                $col19 = $col[18];


                        
                    $sql2= "SELECT id FROM sma_item_stk_profile where org_id ='".$col2."' AND code ='".$col17."' AND wh_id ='".$col5."'"; 
                    $select_result = mysqli_query($remote_connection, $sql2) or die("Selection Error " . mysqli_error($remote_connection)); 
                    $count = mysqli_num_rows($select_result);

                    if($count == 0)
                        { 
                       $query = "INSERT ignore INTO sma_item_stk_profile(id,org_id,segment_id,warehouse_id,wh_id,fy_id,item_id,item_uom_id, total_stk,available_stk,rejected_stk,rework_stk,ordered_stk,purchase_price,sales_price,created_date,code,create_flg,upd_flg,sid)VALUES ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','1','".$col19."','".$col1."')"; 
                       mysqli_query($remote_connection, $query);
                       $this->db->query("UPDATE sma_item_stk_profile SET create_flg='' WHERE id='".$col1."'"); 

                       }
					   else{
						   $query="UPDATE sma_item_stk_profile SET total_stk='".$col9."', available_stk='".$col10."', rejected_stk='".$col11."',rework_stk='".$col12."',ordered_stk='".$col13."', upd_flg ='1' WHERE org_id ='".$col2."' AND code ='".$col17."' AND wh_id ='".$col5."' AND warehouse_id='".$col4."'"; 
                    
						   mysqli_query($remote_connection, $query);
						   $this->db->query("UPDATE sma_item_stk_profile SET upd_flg='' WHERE id='".$col1."'");
					   }
					   
                     //}
                                               
          } 


    // // sma_item_stk_lot table data
    
    $sma_item_stk_lot = $this->db->select('*')
                ->from('sma_item_stk_lot')
                ->get();
                
     $sma_item_stk_lot1 = $sma_item_stk_lot->result_array();

     foreach ($sma_item_stk_lot1 as $sma_item_stk_lots) {
        
          $col=array_values($sma_item_stk_lots);
          
                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];
                $col4 = $col[3];
                $col5 = $col[4];
                $col6 = $col[5];
                $col7 = $col[6];
                $col8 = $col[7];
                $col9 = $col[8];
                $col10 = $col[9];
                $col11 = $col[10];
                $col12 = $col[11];
                $col13 = $col[12];
                $col14 = $col[13];
                $col15 = $col[14];
                $col16 = $col[15];
                $col17 = $col[16];
                $col18 = $col[17];
                $col19 = $col[18];

                
                                
                    $sql2= "SELECT a.id FROM sma_item_stk_lot a where a.segment_id ='".$col3."' AND a.warehouse_id ='".$col4."'AND code ='".$col17."' AND lot_no ='".$col6."'";
                    $select_result = mysqli_query($remote_connection, $sql2) or die("Selection Error " . mysqli_error($remote_connection)); 
                    $count = mysqli_num_rows($select_result);

                    if($count == 0)
                        { 
                       $query = "INSERT ignore INTO sma_item_stk_lot(id,org_id,segment_id,warehouse_id,wh_id,lot_no,item_stk_id,item_id,batch_no,itm_uom_id,total_stk,rejected_stk,mfg_date,exp_date,reworkable_stk,created_date,code,create_flg,upd_flg,lid,sid)VALUES ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','1','".$col19."','".$col1."','".$item_sid."')"; 
                          mysqli_query($remote_connection, $query);
                       $this->db->query("UPDATE sma_item_stk_lot SET create_flg='' WHERE id='".$col1."'"); 

                       }
					   else{
						   
				    $query="UPDATE sma_item_stk_lot SET total_stk='".$col11."', rejected_stk='".$col12."', reworkable_stk='".$col15."',upd_flg ='1' WHERE item_id ='".$col8."' AND code ='".$col17."' AND lot_no ='".$col6."' AND warehouse_id=".$col4." AND segment_id =".$col3;           
                    mysqli_query($remote_connection, $query);
                    $this->db->query("UPDATE sma_item_stk_lot SET upd_flg='' WHERE id='".$col1."'");
					   }
                     
                                               
          } 


    // // sma_item_stk_lot_bin table data
    
	$delete= "DELETE FROM `sma_item_stk_lot_bin` WHERE warehouse_id=".$warehouse_id;
	mysqli_query($remote_connection, $delete);
    $sma_item_stk_lot_bin = $this->db->select('*')
                ->from('sma_item_stk_lot_bin')
                ->get();
                
     $sma_item_stk_lot_bin1 = $sma_item_stk_lot_bin->result_array();

     foreach ($sma_item_stk_lot_bin1 as $sma_item_stk_lot_bins) {
        
          $col=array_values($sma_item_stk_lot_bins);
          
                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];
                $col4 = $col[3];
                $col5 = $col[4];
                $col6 = $col[5];
                $col7 = $col[6];
                $col8 = $col[7];
                $col9 = $col[8];
                $col10 = $col[9];
                $col11 = $col[10];
                $col12 = $col[11];
                $col13 = $col[12];
                $col14 = $col[13];
                $col15 = $col[14];
                $col16 = $col[15];
                $col19 = $col[18];				

                        $select1 = "SELECT lid FROM `sma_item_stk_lot` WHERE item_id ='".$col8."' and segment_id=".$col3." and warehouse_id =".$col4." and id =".$col7." LIMIT 1";
                        $select_result1 = mysqli_query($remote_connection, $select1) or die("Selection Error " . mysqli_error($remote_connection)); 
                        $lid = mysqli_fetch_array($select_result1);
                        $item_lid= $sid['lid'];
						
                         $query = "INSERT ignore INTO sma_item_stk_lot_bin(id,org_id,segment_id,warehouse_id,wh_id,item_id,lot_id,bin_id,itm_uom_id,total_stk,rejected_stk,reworkable_stk,created_date,code,create_flg,upd_flg,bid,lid,lot_no)VALUES ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','1','".$col16."','".$col1."','".$item_lid."','".$col19."')"; 
            
                        mysqli_query($remote_connection, $query);
                        $this->db->query("UPDATE sma_item_stk_lot_bin SET create_flg='' WHERE id='".$col1."'"); 
                         
                    
                                               
          } 

     // sma_expenses table data
    
    $sma_expenses = $this->db->select('*')
                ->from('sma_expenses')
                ->get();       
    $sma_expenses1 = $sma_expenses->result_array();

     foreach ($sma_expenses1 as $sma_expensess) {
        
          $col=array_values($sma_expensess);
          
                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];
                $col4 = $col[3];
                $col5 = $col[4];
                $col6 = $col[5];
                $col7 = $col[6];
                $col8 = $col[7];
                $col9 = $col[8];
                $col10 = $col[9];
                $col11 = $col[10];
                $col12 = $col[11];
                $col13 = $col[12];
                $col14 = $col[13];
                $col15 = $col[14];

               if(($col12 ==1 && $col11==1)){

                 $query="UPDATE sma_expenses SET date='".$col2."', reference='".$col3."', amount='".$col4."',note='".mysqli_real_escape_string($col5)."',created_by='".$col6."',attachment='".$col7."',register_id='".$col8."',bank_eo_id='".$col13."',update_at='".$col15."', upd_flg ='1' WHERE created_by ='".$col6."' AND eid =".$col1;                      
                    mysqli_query($remote_connection, $query);
                    $this->db->query("UPDATE sma_expenses SET upd_flg='' WHERE id='".$col1."'");

                   }
            
                    $sql2= "SELECT id FROM sma_expenses where created_by =".$col6." AND eid ='".$col1."'";
                    $select_result = mysqli_query($remote_connection, $sql2) or die("Selection Error " . mysqli_error($remote_connection)); 
                    $count = mysqli_num_rows($select_result);

                    if($count == 0)
                        { 

                       $query = "INSERT ignore INTO sma_expenses(id,date,reference,amount,note,created_by,attachment,   register_id,bank_eo_id,manual,update_at,eid)VALUES ('','".$col2."','".$col3."','".$col4."','".mysqli_real_escape_string($col5)."','".$col6."','".$col7."','".$col8."','".$col13."','".$col14."','".$col15."','".$col1."')"; 
                        mysqli_query($remote_connection, $query);
                        $this->db->query("UPDATE sma_expenses SET sync_flg=1 WHERE id='".$col1."'");    
                     }
                                               
          } 

    // sma_pos_register table data

     $query = "SHOW COLUMNS FROM sma_pos_register LIKE 'upd_flg'";
        $uflg= $this->db->query($query);
        $uflg->num_rows();

        if($uflg->num_rows()==0) { 

            $alter = "ALTER TABLE `sma_pos_register` ADD `upd_flg` INT(2) NULL DEFAULT NULL AFTER `sync_flg`"; 
            $this->db->query($alter);
        }
    $sma_pos_register = $this->db->select('*')
                ->from('sma_pos_register')
                ->get();       
    $sma_pos_register1 = $sma_pos_register->result_array();

     foreach ($sma_pos_register1 as $sma_pos_registers) {
        
          $col=array_values($sma_pos_registers);
          
                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];
                $col4 = $col[3];
                $col5 = $col[4];
                $col6 = $col[5];
                $col7 = $col[6];
                $col8 = $col[7];
                $col9 = $col[8];
                $col10 = $col[9];
                $col11 = $col[10];
                $col12 = $col[11];
                $col13 = $col[12];
                $col14 = $col[13];
                $col15 = $col[14];
                $col16 = $col[15];
                $col17 = $col[16];

               if(($col17 ==1 && $col16==1)){

                 $query="UPDATE sma_pos_register SET date='".$col2."', cash_in_hand='".$col4."', total_cash='".$col6."',total_cash_submitted='".$col9."',closed_at='".$col13."',note='".mysqli_real_escape_string($col12)."' WHERE user_id ='".$col3."' AND rid =".$col1;                  
                    mysqli_query($remote_connection, $query);
                    $this->db->query("UPDATE sma_pos_register SET upd_flg=NULL WHERE id='".$col1."'");

                   }
            
                   $sql2= "SELECT id FROM sma_pos_register where user_id =".$col3." AND rid ='".$col1."'";
                    $select_result = mysqli_query($remote_connection, $sql2) or die("Selection Error " . mysqli_error($remote_connection)); 
                    $count = mysqli_num_rows($select_result);

                    if($count == 0)
                        { 

                      $query = "INSERT ignore INTO sma_pos_register(id,date,user_id,cash_in_hand,status,total_cash,total_cheques,total_cc_slips,total_cash_submitted,total_cheques_submitted,total_cc_slips_submitted,note,closed_at,transfer_opened_bills,closed_by,rid)VALUES ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".mysqli_real_escape_string($col12)."','".$col13."','".$col14."','".$col15."','".$col1."')"; 
 
                        mysqli_query($remote_connection, $query);
                        $this->db->query("UPDATE sma_pos_register SET sync_flg=1 WHERE id='".$col1."'");    
                     }
                                               
          } 

      // sma_cheque_details table data
    
       $sma_cheque_details = $this->db->select('*')
                    ->from('sma_cheque_details')
                    ->get();
                    
       $sma_cheque_details1 = $sma_cheque_details->result_array();

        foreach ($sma_cheque_details1 as $sma_cheque_detailss) {
            
              $col=array_values($sma_cheque_detailss);
          
                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];
                $col4 = $col[3];
                $col5 = $col[4];
                $col6 = $col[5];
                $col7 = $col[6];
                $col8 = $col[7];
                $col9 = $col[8];
                $col10 = $col[9];
                $col11 = $col[10];
                $col12 = $col[11];
                $col13 = $col[12];
                $col14 = $col[13];
                $col15 = $col[14];
                $col16 = $col[15];
                $col17 = $col[16];
                $col18 = $col[17];
                $col19 = $col[18];
                $col20 = $col[19];
                $col21 = $col[20]; 

          $sql2= "SELECT id FROM sma_cheque_details where  segment_id='".$col3."' AND wh_id='".$col5."' AND reference_no='".$col16."'";
          $select_result = mysqli_query($remote_connection, $sql2) or die("Selection Error " . mysqli_error($remote_connection)); 
           $count = mysqli_num_rows($select_result);

              if($count == 0)
                {
                    
                 $query = "INSERT ignore INTO sma_cheque_details(`id`, `org_id`, `segment_id`, `warehouse_id`, `wh_id`, `sale_id`, `cheque_no`, `customer_name`, `bank_name`, `cheque_date`, `amount`, `status`, `clearance_date`, `transaction_date`, `remarks`, `reference_no`, `sync_flg`, `upd_flg`, `sid`, `cdid`,`eo_type`)VALUES  ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','','','".$col6."','".$col1."','".$col21."')";
                                    
                mysqli_query($remote_connection, $query);
                $this->db->query("UPDATE sma_cheque_details SET sync_flg=1 WHERE id='".$col1."'"); 
                }
         } 

        $sql = "INSERT INTO `sma_hard_syncout_history`(`id`,`segment_id`, `created_date`) VALUES ('','".$segment_id."','".date('Y-m-d H:i:s')."')";
            $csv_data = mysqli_query($remote_connection, $sql) or die("Selection Error " . mysqli_error($remote_connection));
        $this->session->set_flashdata('message', "Congratulations!!!! Data Sync-Out Successfully");
            redirect('welcome');
        
        $this->page_construct('syncin', $meta, $this->data);  
     
    }
	
 function recover_stk(){
   
     $remote_host = REMOTE_HOST;
     $remote_username = REMOTE_USERNAME;
     $remote_password = REMOTE_PWD;
     $remote_dbname = REMOTE_DB;

      /* $remote_host = 'localhost';
      $remote_username = 'root';
      $remote_password = '';
      $remote_dbname = 'iffco_central'; */

     $remote_connection = mysqli_connect($remote_host, $remote_username, $remote_password, $remote_dbname) or die("Connection Error " . mysqli_error($remote_connection)); 

     if($remote_connection === false){
        die("ERROR: Could not connect. " . mysqli_connect_error());
     }
    
     $query= $this->db->query("SELECT id FROM `sma_warehouses` where name like '%GODOWN%' LIMIT 1");
     $warehouse_id1=$query->result();
     $warehouse_id= $warehouse_id1[0]->id;
     $segment_id = $this->session->userdata('segment_id');
	 
	  // sma_item_stk_profile table data
    
    $sma_item_stk_profile = $this->db->select('*')
                ->from('sma_item_stk_profile')
                ->get();
                
     $sma_item_stk_profile1 = $sma_item_stk_profile->result_array();

     foreach ($sma_item_stk_profile1 as $sma_item_stk_profiles) {
        
          $col=array_values($sma_item_stk_profiles);
          
                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];
                $col4 = $col[3];
                $col5 = $col[4];
                $col6 = $col[5];
                $col7 = $col[6];
                $col8 = $col[7];
                $col9 = $col[8];
                $col10 = $col[9];
                $col11 = $col[10];
                $col12 = $col[11];
                $col13 = $col[12];
                $col14 = $col[13];
                $col15 = $col[14];
                $col16 = $col[15];
                $col17 = $col[16];
                $col18 = $col[17];
                $col19 = $col[18];


                        
                    $sql2= "SELECT id FROM sma_item_stk_profile where org_id ='".$col2."' AND code ='".$col17."' AND wh_id ='".$col5."'"; 
                    $select_result = mysqli_query($remote_connection, $sql2) or die("Selection Error " . mysqli_error($remote_connection)); 
                    $count = mysqli_num_rows($select_result);

                    if($count == 0)
                        { 
                       $query = "INSERT ignore INTO sma_item_stk_profile(id,org_id,segment_id,warehouse_id,wh_id,fy_id,item_id,item_uom_id, total_stk,available_stk,rejected_stk,rework_stk,ordered_stk,purchase_price,sales_price,created_date,code,create_flg,upd_flg,sid)VALUES ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','1','".$col19."','".$col1."')"; 
                       mysqli_query($remote_connection, $query);
                       $this->db->query("UPDATE sma_item_stk_profile SET create_flg='' WHERE id='".$col1."'"); 

                       }
					   else{
						   $query="UPDATE sma_item_stk_profile SET total_stk='".$col9."', available_stk='".$col10."', rejected_stk='".$col11."',rework_stk='".$col12."',ordered_stk='".$col13."', upd_flg ='1' WHERE org_id ='".$col2."' AND code ='".$col17."' AND wh_id ='".$col5."' AND warehouse_id='".$col4."'"; 
                    
						   mysqli_query($remote_connection, $query);
						   $this->db->query("UPDATE sma_item_stk_profile SET upd_flg='' WHERE id='".$col1."'");
					   }
					   
                     //}
                                               
          } 


    // // sma_item_stk_lot table data
    
    $sma_item_stk_lot = $this->db->select('*')
                ->from('sma_item_stk_lot')
                ->get();
                
     $sma_item_stk_lot1 = $sma_item_stk_lot->result_array();

     foreach ($sma_item_stk_lot1 as $sma_item_stk_lots) {
        
          $col=array_values($sma_item_stk_lots);
          
                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];
                $col4 = $col[3];
                $col5 = $col[4];
                $col6 = $col[5];
                $col7 = $col[6];
                $col8 = $col[7];
                $col9 = $col[8];
                $col10 = $col[9];
                $col11 = $col[10];
                $col12 = $col[11];
                $col13 = $col[12];
                $col14 = $col[13];
                $col15 = $col[14];
                $col16 = $col[15];
                $col17 = $col[16];
                $col18 = $col[17];
                $col19 = $col[18];

                
                                
                    $sql2= "SELECT a.id FROM sma_item_stk_lot a where a.segment_id ='".$col3."' AND a.warehouse_id ='".$col4."'AND code ='".$col17."' AND lot_no ='".$col6."'";
                    $select_result = mysqli_query($remote_connection, $sql2) or die("Selection Error " . mysqli_error($remote_connection)); 
                    $count = mysqli_num_rows($select_result);

                    if($count == 0)
                        { 
                       $query = "INSERT ignore INTO sma_item_stk_lot(id,org_id,segment_id,warehouse_id,wh_id,lot_no,item_stk_id,item_id,batch_no,itm_uom_id,total_stk,rejected_stk,mfg_date,exp_date,reworkable_stk,created_date,code,create_flg,upd_flg,lid,sid)VALUES ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','1','".$col19."','".$col1."','".$item_sid."')"; 
                          mysqli_query($remote_connection, $query);
                       $this->db->query("UPDATE sma_item_stk_lot SET create_flg='' WHERE id='".$col1."'"); 

                       }
					   else{
						   
				    $query="UPDATE sma_item_stk_lot SET total_stk='".$col11."', rejected_stk='".$col12."', reworkable_stk='".$col15."',upd_flg ='1' WHERE item_id ='".$col8."' AND code ='".$col17."' AND lot_no ='".$col6."' AND warehouse_id=".$col4." AND segment_id =".$col3;           
                    mysqli_query($remote_connection, $query);
                    $this->db->query("UPDATE sma_item_stk_lot SET upd_flg='' WHERE id='".$col1."'");
					   }
                     
                                               
          } 


    // // sma_item_stk_lot_bin table data
    
	$delete= "DELETE FROM `sma_item_stk_lot_bin` WHERE warehouse_id=".$warehouse_id;
	mysqli_query($remote_connection, $delete);
    $sma_item_stk_lot_bin = $this->db->select('*')
                ->from('sma_item_stk_lot_bin')
                ->get();
                
     $sma_item_stk_lot_bin1 = $sma_item_stk_lot_bin->result_array();

     foreach ($sma_item_stk_lot_bin1 as $sma_item_stk_lot_bins) {
        
          $col=array_values($sma_item_stk_lot_bins);
          
                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];
                $col4 = $col[3];
                $col5 = $col[4];
                $col6 = $col[5];
                $col7 = $col[6];
                $col8 = $col[7];
                $col9 = $col[8];
                $col10 = $col[9];
                $col11 = $col[10];
                $col12 = $col[11];
                $col13 = $col[12];
                $col14 = $col[13];
                $col15 = $col[14];
                $col16 = $col[15];
                $col19 = $col[18];				

                        $select1 = "SELECT lid FROM `sma_item_stk_lot` WHERE item_id ='".$col8."' and segment_id=".$col3." and warehouse_id =".$col4." and id =".$col7." LIMIT 1";
                        $select_result1 = mysqli_query($remote_connection, $select1) or die("Selection Error " . mysqli_error($remote_connection)); 
                        $lid = mysqli_fetch_array($select_result1);
                        $item_lid= $sid['lid'];
						
                         $query = "INSERT ignore INTO sma_item_stk_lot_bin(id,org_id,segment_id,warehouse_id,wh_id,item_id,lot_id,bin_id,itm_uom_id,total_stk,rejected_stk,reworkable_stk,created_date,code,create_flg,upd_flg,bid,lid,lot_no)VALUES ('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','1','".$col16."','".$col1."','".$item_lid."','".$col19."')"; 
            
                        mysqli_query($remote_connection, $query);
                        $this->db->query("UPDATE sma_item_stk_lot_bin SET create_flg='' WHERE id='".$col1."'"); 
                         
                    
                                               
          } 

	echo "stock recovered";
 }
 
 function recover_companies(){
   
     $remote_host = REMOTE_HOST;
     $remote_username = REMOTE_USERNAME;
     $remote_password = REMOTE_PWD;
     $remote_dbname = REMOTE_DB;

     // $remote_host = 'localhost';
     // $remote_username = 'root';
     // $remote_password = '';
     // $remote_dbname = 'iffco_central';

     $remote_connection = mysqli_connect($remote_host, $remote_username, $remote_password, $remote_dbname) or die("Connection Error " . mysqli_error($remote_connection)); 

     if($remote_connection === false){
        die("ERROR: Could not connect. " . mysqli_connect_error());
     }
    
     $query= $this->db->query("SELECT id FROM `sma_warehouses` where name like '%GODOWN%' LIMIT 1");
     $warehouse_id1=$query->result();
     $warehouse_id= $warehouse_id1[0]->id;
     $segment_id = $this->session->userdata('segment_id');
     $sma_companies = $this->db->select('*')
                ->from('sma_companies')
                ->where('segment_id !=', 0)
                ->where('name !=', 'Walk-in Customer')
                ->where('group_name', 'customer')
                ->get();
                
     $sma_companies1 = $sma_companies->result_array();

     foreach ($sma_companies1 as $sma_companiess) {
        
          $col=array_values($sma_companiess);
          
                $col1 = $col[0];
                $col2 = $col[1];
                $col3 = $col[2];
                $col4 = $col[3];
                $col5 = $col[4];
                $col6 = $col[5];
                $col7 = $col[6];
                $col8 = $col[7];
                $col9 = $col[8];
                $col10 = $col[9];
                $col11 = $col[10];
                $col12 = $col[11];
                $col13 = $col[12];
                $col14 = $col[13];
                $col15 = $col[14];
                $col16 = $col[15];
                $col17 = $col[16];
                $col18 = $col[17];
                $col19 = $col[18];
                $col20 = $col[19];
                $col21 = $col[20];
                $col22 = $col[21];
                $col23 = $col[22];
                $col24 = $col[23];
                $col25 = $col[24];
                $col26 = $col[25];
                $col27 = $col[26];
                $col28 = $col[27];
                $col29 = $col[28];
                $col30 = $col[29];
                $col31 = $col[30];
                $col32 = $col[31];
                $col33 = $col[32];
                $col34 = $col[33];
                $col35 = $col[34];
                $col36 = $col[35];
                $col37 = $col[36];
                $col38 = $col[37];
                $col39 = $col[38];
                $col40 = $col[39];
                $col41 = $col[40];
                $col42 = $col[41];
                $col43 = $col[42];
                $col44 = $col[43];
                $col45 = $col[44];
                $col46 = $col[45];
                $col47 = $col[46];
                $col48 = $col[47];
                $col49 = $col[48];
                $col50 = $col[49];
                $col51 = $col[50];
                $col52 = $col[51];

          $sql2= "SELECT id FROM sma_companies where cid ='".$col1."' AND segment_id='".$col48."'";
          $select_result = mysqli_query($remote_connection, $sql2) or die("Selection Error " . mysqli_error($remote_connection)); 
          $count = mysqli_num_rows($select_result);

              if($count == 0)
                {
                    
                $query = "INSERT ignore INTO sma_companies(id,group_id,group_name,customer_group_id,customer_group_name,name,father_name,company,vat_no,address,city,state,postal_code,country,country_id,state_id,phone,email,id_proof,id_proof_no,loyalty_card_id,nominees1,dob_nominees1,nominees2,dob_nominees2,nominees3,dob_nominees3,nominees4,dob_nominees4,nominees5,dob_nominees5,cf1,cf2,cf3,cf4,cf5,cf6,invoice_footer,payment_term,logo,award_points,upd_flg,eo_type,eo_id,org_id,wh_id,biller_id,segment_id,cid,adhar_card_no) VALUES('','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."','".$col7."','".$col8."','".$col9."','".$col10."','".$col11."','".$col12."','".$col13."','".$col14."','".$col15."','".$col16."','".$col17."','".$col18."','".$col19."','".$col20."','".$col21."','".$col22."','".$col23."','".$col24."','".$col25."','".$col26."','".$col27."','".$col28."','".$col29."','".$col30."','".$col31."','".$col32."','".$col33."','".$col34."','".$col35."','".$col36."','".$col37."','".$col38."','".$col39."','".$col40."','".$col41."','".$col42."','".$col43."','".$col44."','".$col45."','".$col46."','".$col47."','".$col48."','".$col1."','".$col52."')";
                mysqli_query($remote_connection, $query);
                $this->db->query("UPDATE sma_companies SET sync_flg=1 WHERE id='".$col1."'"); 
				echo "customer data recovered";
                }
          }  

	 echo "customer data recovered";
 }

}

