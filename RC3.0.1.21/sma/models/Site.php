<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Site extends CI_Model
{

    public function __construct() {
        parent::__construct();

        $host = $this->db->hostname;
        $username = $this->db->username;
        $password = $this->db->password;
        $dbname = $this->db->database;    
        $connection = mysqli_connect($host, $username, $password, $dbname) or die("Connection Error " . mysqli_error($connection));
          
        $this->checkColumn('rcpt_no','sma_mrn_rcpt_item');
        $this->checkColumn('ro_no','sma_mrn_rcpt_item');
        $this->checkColumn('ro_no','sma_mrn_rcpt_item_lot');
        $this->checkColumn('ro_no','sma_mrn_rcpt_lot_bin');
        $this->checkColumn('lot_no','sma_mrn_rcpt_lot_bin');
        $this->checkColumn('sale_ref_no','sma_sale_items');
        $this->checkColumn('sale_ref_no','sma_sale_item_lot');
        $this->checkColumn('sale_ref_no','sma_sale_item_bin');
        $this->checkColumn('lot_no','sma_sale_item_bin');
        $this->checkColumn('lot_no','sma_item_stk_lot_bin');

        mysqli_query($connection, 'UPDATE sma_item_stk_lot_bin INNER JOIN sma_item_stk_lot AS c ON (sma_item_stk_lot_bin.lot_id=c.id AND sma_item_stk_lot_bin.item_id=c.item_id) 
        SET sma_item_stk_lot_bin.lot_no=c.lot_no WHERE sma_item_stk_lot_bin.lot_id=c.id AND sma_item_stk_lot_bin.item_id=c.item_id') or die("Create sma_sale_item_lot Master Error " . mysqli_error($connection));

        mysqli_query($connection, 'UPDATE sma_mrn_rcpt_lot_bin INNER JOIN sma_mrn_rcpt_item_lot AS c ON (sma_mrn_rcpt_lot_bin.lot_id=c.id AND sma_mrn_rcpt_lot_bin.item_id=c.item_id) SET sma_mrn_rcpt_lot_bin.lot_no=c.lot_no WHERE sma_mrn_rcpt_lot_bin.lot_id=c.id AND sma_mrn_rcpt_lot_bin.item_id=c.item_id ') or die("Create sma_sale_item_lot Master Error " . mysqli_error($connection));
    }

    public function get_total_qty_alerts() {
        $this->db->where('quantity < alert_quantity', NULL, FALSE)->where('track_quantity', 1);
        return $this->db->count_all_results('products');
    }

    public function get_expiring_qty_alerts() {
        $date = date('Y-m-d', strtotime('+3 months'));
        $this->db->select('SUM(quantity_balance) as alert_num')->where('expiry <', $date);
        $q = $this->db->get('purchase_items');
        if ($q->num_rows() > 0) {
            $res = $q->row();
            return (INT) $res->alert_num;
        }
        return FALSE;
    }

    public function get_setting() {
        $q = $this->db->get('settings');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getDateFormat($id) {
        $q = $this->db->get_where('date_format', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllCompanies($group_name) {
        $q = $this->db->get_where('companies', array('group_name' => $group_name));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
   // added by Vikas Singh for list segment on create user 
    public function getAllSegments() {
        $this->db->select("segment.id, segment.proj_name,segment.org_id");
        $this->db->from("segment");
        $this->db->join("warehouses","segment.id=warehouses.segment_id","inner");
        $this->db->group_by("warehouses.segment_id");
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

     public function getAllLocation($id=0) {
        $this->db->select("id,proj_name");
        $this->db->from("location");
        if($id!=0){
            $this->db->where("id",$id);
        }
        $this->db->order_by("proj_name",ASC);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getWarehouse($warehouse_id='',$segment_id) {

        $this->db->select("*");
        $this->db->from("warehouses");
        if($warehouse_id!='')
            $this->db->where("id",$warehouse_id);
        $this->db->where("segment_id",$segment_id);
        $q = $this->db->get()->row();
        if (count($q) > 0) {     

            return $q;
        }
        return FALSE;
    }


    public function getWarehouseList($segment_id) {
        $this->db->select("GROUP_CONCAT(wh_id) AS wh_id");
        $this->db->from("warehouses");       
        $this->db->where("segment_id",$segment_id);
        $this->db->group_by("segment_id");
        $q = $this->db->get()->row();
        if (count($q) > 0) {    

            return $q;
        }
        return FALSE;
    }




    public function getCompanyByID($id) {
        $q = $this->db->get_where('companies', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCustomerGroupByID($id) {
        $q = $this->db->get_where('customer_groups', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getUser($id = NULL) {
        if (!$id) {
            $id = $this->session->userdata('user_id');
        }
        $q = $this->db->get_where('users', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductByID($id) {
        $q = $this->db->get_where('products', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllCurrencies() {
        $q = $this->db->get('currencies');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCurrencyByCode($code) {
        $q = $this->db->get_where('currencies', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllTaxRates() {
        $q = $this->db->get('tax_rates');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getTaxRateByID($id) {       
        $q = $this->db->get_where('tax_rates', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllWarehouses() {
        //print_r($this->session->userdata());die;
          $get_wh_id = $this->site->get_warehouse_ids(1);

          $values =array();
          $pwh_id = array();
        for($i=0; $i< count($get_wh_id); $i++)
        {
            $values = $get_wh_id[$i]['id'];

            array_push($pwh_id ,$values);
        }  
        $get_wh_idss = implode('_',$pwh_id);
           
        $wh_id= $get_wh_idss;
        $wh_ids = explode("_", $wh_id);
        $data = array();
        for($i=0;$i<count($wh_ids); $i++)
        {
        $this->db->select("*");
        $this->db->from("warehouses");
        $this->db->where("id",$wh_ids[$i]);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
           
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            //print_r($data); die;
            //return $data;
        }
        
        
            }
        if(!empty($data))
        {
            return $data;
        }
       else 
           {
                return false;
           }
            
        
    }

    public function getWarehouseByID($id) {
      
        $q = $this->db->get_where('warehouses', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllCategories() {
        $this->db->order_by('name');
        $q = $this->db->get('categories');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    // Add BY Ankit for 3 level category at report

    public function getAllCategories_N() {
        $this->db->order_by('name');
        $q = $this->db->get_where('categories', array('level' => 3));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCategoryByID($id) {
        $q = $this->db->get_where('categories', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getGiftCardByID($id) {
        $q = $this->db->get_where('gift_cards', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getGiftCardByNO($no) {
        $q = $this->db->get_where('gift_cards', array('card_no' => $no), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateInvoiceStatus() {
        $date = date('Y-m-d');
        $q = $this->db->get_where('invoices', array('status' => 'unpaid'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                if ($row->due_date < $date) {
                    $this->db->update('invoices', array('status' => 'due'), array('id' => $row->id));
                }
            }
            $this->db->update('settings', array('update' => $date), array('setting_id' => '1'));
            return true;
        }
    }

    public function modal_js() {
        return '<script type="text/javascript">' . file_get_contents($this->data['assets'] . 'js/modal.js') . '</script>';
    }

    public function getReference($field) {
        $q = $this->db->get_where('order_ref', array('ref_id' => '1'), 1);
        if ($q->num_rows() > 0) {
            $ref = $q->row();
            switch ($field) {
                case 'so':
                    $prefix = $this->Settings->sales_prefix;
                    break;
                case 'qu':
                    $prefix = $this->Settings->quote_prefix;
                    break;
                case 'po':
                    $prefix = $this->Settings->purchase_prefix;
                    break;
                case 'to':
                    $prefix = $this->Settings->transfer_prefix;
                    break;
                case 'do':
                    $prefix = $this->Settings->delivery_prefix;
                    break;
                case 'pay':
                    $prefix = $this->Settings->payment_prefix;
                    break;
                case 'pos':
                    $prefix = isset($this->Settings->sales_prefix) ? $this->Settings->sales_prefix . '/POS' : '';
                    break;
                case 're':
                    $prefix = $this->Settings->return_prefix;
                    break;
                case 'ex':
                    $prefix = $this->Settings->expense_prefix;
                    break;
                default:
                    $prefix = '';
            }

            $ref_no = (!empty($prefix)) ? $prefix . '/' : '';

            if ($this->Settings->reference_format == 1) {
                $ref_no .= date('Y') . "/" . sprintf("%04s", $ref->{$field});
            } elseif ($this->Settings->reference_format == 2) {
                //$ref_no .= date('Y') . "/" . date('m') . "/" . sprintf("%04s", $ref->{$field});
                $rno = sprintf($ref->{$field});
            } elseif ($this->Settings->reference_format == 3) {
                $ref_no .= sprintf("%04s", $ref->{$field});
            } else {
                $ref_no .= $this->getRandomReference();
            }

           // return $ref_no;
             return $rno;
        }
        return FALSE;
    }

    public function getRandomReference($len = 12) {
        $result = '';
        for ($i = 0; $i < $len; $i++) {
            $result .= mt_rand(0, 9);
        }

        if ($this->getSaleByReference($result)) {
            $this->getRandomReference();
        }

        return $result;
    }

    public function getSaleByReference($ref) {
        $this->db->like('reference_no', $ref, 'before');
        $q = $this->db->get('sales', 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateReference($field) {
        $q = $this->db->get_where('order_ref', array('ref_id' => '1'), 1);
        if ($q->num_rows() > 0) {
            $ref = $q->row();
            $this->db->update('order_ref', array($field => $ref->{$field} + 1), array('ref_id' => '1'));
            return TRUE;
        }
        return FALSE;
    }

    public function checkPermissions() {
        $q = $this->db->get_where('permissions', array('group_id' => $this->session->userdata('group_id')), 1);
        if ($q->num_rows() > 0) {
            return $q->result_array();
        }
        return FALSE;
    }

    public function getNotifications() {
        $date = date('Y-m-d H:i:s', time());
        $this->db->where("from_date <=", $date);
        $this->db->where("till_date >=", $date);
        if (!$this->Owner) {
            if ($this->Supplier) {
                $this->db->where('scope', 4);
            } elseif ($this->Customer) {
                $this->db->where('scope', 1)->or_where('scope', 3);
            } elseif (!$this->Customer && !$this->Supplier) {
                $this->db->where('scope', 2)->or_where('scope', 3);
            }
        }
        $q = $this->db->get("notifications");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getUpcomingEvents() {
        $dt = date('Y-m-d');
        $this->db->where('date >=', $dt)->order_by('date')->limit(5);
        if ($this->Settings->restrict_calendar) {
            $q = $this->db->get_where('calendar', array('user_id' => $this->session->userdata('iser_id')));
        } else {
            $q = $this->db->get('calendar');
        }
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getUserGroup($user_id = false) {
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $group_id = $this->getUserGroupID($user_id);
        $q = $this->db->get_where('groups', array('id' => $group_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getUserGroupID($user_id = false) {
        $user = $this->getUser($user_id);
        return $user->group_id;
    }

    public function getWarehouseProductsVariants($option_id, $warehouse_id = NULL) {
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPurchasedItem($where_clause) {
        $orderby = ($this->Settings->accounting_method == 1) ? 'asc' : 'desc';
        $this->db->order_by('date', $orderby);
        $this->db->order_by('purchase_id', $orderby);
        $q = $this->db->get_where('purchase_items', $where_clause);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function syncVariantQty($variant_id, $warehouse_id, $product_id = NULL) {
        $balance_qty = $this->getBalanceVariantQuantity($variant_id);
        $wh_balance_qty = $this->getBalanceVariantQuantity($variant_id, $warehouse_id);
        if ($this->db->update('product_variants', array('quantity' => $balance_qty), array('id' => $variant_id))) {
            if ($this->getWarehouseProductsVariants($variant_id, $warehouse_id)) {
                $this->db->update('warehouses_products_variants', array('quantity' => $wh_balance_qty), array('option_id' => $variant_id, 'warehouse_id' => $warehouse_id));
            } else {
                if($wh_balance_qty) {
                    $this->db->insert('warehouses_products_variants', array('quantity' => $wh_balance_qty, 'option_id' => $variant_id, 'warehouse_id' => $warehouse_id, 'product_id' => $product_id));
                }
            }
            return TRUE;
        }
        return FALSE;
    }

    public function getWarehouseProducts($product_id, $warehouse_id = NULL) {
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function syncProductQty($product_id, $warehouse_id) {
        $balance_qty = $this->getBalanceQuantity($product_id);
        $wh_balance_qty = $this->getBalanceQuantity($product_id, $warehouse_id);
        if ($this->db->update('products', array('quantity' => $balance_qty,'psale'=>'1'), array('id' => $product_id))) {
            if ($this->getWarehouseProducts($product_id, $warehouse_id)) {
                $this->db->update('warehouses_products', array('quantity' => $wh_balance_qty), array('product_id' => $product_id, 'warehouse_id' => $warehouse_id));
            } else {
                if( ! $wh_balance_qty) { $wh_balance_qty = 0; }
                $this->db->insert('warehouses_products', array('quantity' => $wh_balance_qty, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id));
            }
            return TRUE;
        }
        return FALSE;
    }

    public function getSaleByID($id) {
        $q = $this->db->get_where('sales', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSalePayments($sale_id) {
        $q = $this->db->get_where('payments', array('sale_id' => $sale_id,'type'=>'received'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function syncSalePayments($id, $cheque_data) {
        $sale = $this->getSaleByID($id);
        $payments = $this->getSalePayments($id);
        $paid = 0;
        foreach ($payments as $payment) {
            if ($payment->type == 'returned') {
                $paid -= $payment->amount;
            } else {
                $paid += $payment->amount;
            }
        }

         if((count($cheque_data)>0) && (isset($cheque_data))){
             $payment_status = 'pending';
         }else{
            $payment_status = $paid <= 0 ? 'pending' : $sale->payment_status;
         }
         
        if(count($cheque_data)>0){
            $payment_status = 'due';
        }else{   
            if ($paid <= 0 && $sale->due_date <= date('Y-m-d')) {
                $payment_status = 'due';
            } elseif ($this->sma->formatDecimal($sale->grand_total) > $this->sma->formatDecimal($paid) && $paid > 0) {
                $payment_status = 'partial';
            } elseif ($this->sma->formatDecimal($sale->grand_total) <= $this->sma->formatDecimal($paid)) {
                $payment_status = 'paid';
            }
        }

        if ($this->db->update('sales', array('paid' => $paid, 'payment_status' => $payment_status), array('id' => $id))) {
            return true;
        }

        return FALSE;
    }

    public function getPurchaseByID($id) {
        $q = $this->db->get_where('purchases', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPurchasePayments($purchase_id) {
        $q = $this->db->get_where('payments', array('purchase_id' => $purchase_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function syncPurchasePayments($id) {
        $purchase = $this->getPurchaseByID($id);
        $payments = $this->getPurchasePayments($id);
        $paid = 0;
        foreach ($payments as $payment) {
            $paid += $payment->amount;
        }

        $payment_status = $paid <= 0 ? 'pending' : $purchase->payment_status;
        if ($this->sma->formatDecimal($purchase->grand_total) > $this->sma->formatDecimal($paid) && $paid > 0) {
            $payment_status = 'partial';
        } elseif ($this->sma->formatDecimal($purchase->grand_total) <= $this->sma->formatDecimal($paid)) {
            $payment_status = 'paid';
        }

        if ($this->db->update('purchases', array('paid' => $paid, 'payment_status' => $payment_status), array('id' => $id))) {
            return true;
        }

        return FALSE;
    }

    private function getBalanceQuantity($product_id, $warehouse_id = NULL) {
        $this->db->select('SUM(COALESCE(quantity_balance, 0)) as stock', False);
        $this->db->where('product_id', $product_id)->where('quantity_balance !=', 0);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get('purchase_items');
        if ($q->num_rows() > 0) {
            $data = $q->row();
            return $data->stock;
        }
        return 0;
    }

    private function getBalanceVariantQuantity($variant_id, $warehouse_id = NULL) {
        $this->db->select('SUM(COALESCE(quantity_balance, 0)) as stock', False);
        $this->db->where('option_id', $variant_id)->where('quantity_balance !=', 0);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get('purchase_items');
        if ($q->num_rows() > 0) {
            $data = $q->row();
            return $data->stock;
        }
        return 0;
    }

    public function calculateAVCost($product_id, $warehouse_id, $net_unit_price, $unit_price, $quantity, $product_name, $option_id, $item_quantity) {
        /*echo $product_id.",". $warehouse_id.",". $net_unit_price.",". $unit_price.",". $quantity.",". $product_name.",". $option_id.",". $item_quantity;exit;*/
        $real_item_qty = $quantity;
        
        if ($pis = $this->getPurchasedItems($product_id, $warehouse_id, $option_id)) {
            
            $cost_row = array();
            $quantity = $item_quantity;
            $balance_qty = $pis[0]->available_stk;
            $total_net_unit_cost = 0;
            $total_unit_cost = 0;
            //echo $product_id; echo "jjjj";
            foreach ($pis as $pi) {
               // print_r($pi); die;
                $total_net_unit_cost += $pi->net_unit_cost;
                $total_unit_cost += ($pi->unit_cost ? $pi->unit_cost : $pi->net_unit_cost + ($pi->item_tax / $pi->quantity));
            }
            $as = sizeof($pis);
            $avg_net_unit_cost = $total_net_unit_cost / $as;
            $avg_unit_cost = $total_unit_cost / $as;

            foreach ($pis as $pi) {
                /*echo "<pre>";
                print_r($pi);
                echo "<pre>";
                echo "bala".$balance_qty."quewhyr".$quantity;exit;*/
                if (!empty($pi) && $pi->quantity > 0 && $balance_qty <= $quantity && $quantity > 0) {
                   
                    if ($pi->quantity_balance >= $quantity && $quantity > 0) {
                        $balance_qty = $pi->quantity_balance - $quantity;
                        $cost_row = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => $pi->id, 'quantity' => $real_item_qty, 'purchase_net_unit_cost' => $avg_net_unit_cost, 'purchase_unit_cost' => $avg_unit_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => $balance_qty, 'inventory' => 1, 'option_id' => $option_id);
                        $quantity = 0;
                    } elseif ($quantity > 0) {
                        $quantity = $quantity - $pi->quantity_balance;
                        $balance_qty = $quantity;
                        $cost_row = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => $pi->id, 'quantity' => $pi->quantity_balance, 'purchase_net_unit_cost' => $avg_net_unit_cost, 'purchase_unit_cost' => $avg_unit_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => 0, 'inventory' => 1, 'option_id' => $option_id);
                    }
                    
                }
                if (empty($cost_row)) {
                    break;
                }
                $cost[] = $cost_row;
                if ($quantity == 0) {
                    break;
                }
            }
        }
        if ($quantity > 0 && !$this->Settings->overselling) {
            $this->session->set_flashdata('error', sprintf(lang("quantity_out_of_stock_for_%s"), ($pi->product_name ? $pi->product_name : $product_name)));
            redirect($_SERVER["HTTP_REFERER"]);
        } elseif ($quantity > 0) {
            $cost[] = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $real_item_qty, 'purchase_net_unit_cost' => NULL, 'purchase_unit_cost' => NULL, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => NULL, 'overselling' => 1, 'inventory' => 1);
            $cost[] = array('pi_overselling' => 1, 'product_id' => $product_id, 'quantity_balance' => (0 - $quantity), 'warehouse_id' => $warehouse_id, 'option_id' => $option_id);
        }
        return $cost;
    }

    public function calculateCost($product_id, $warehouse_id, $net_unit_price, $unit_price, $quantity, $product_name, $option_id, $item_quantity) {
        $pis = $this->getPurchasedItems($product_id, $warehouse_id, $option_id);
        $real_item_qty = $quantity;
        $quantity = $item_quantity;
        $balance_qty = $quantity;
        foreach ($pis as $pi) {
            if (!empty($pi) && $balance_qty <= $quantity && $quantity > 0) {
                $purchase_unit_cost = $pi->unit_cost ? $pi->unit_cost : ($pi->net_unit_cost + ($pi->item_tax / $pi->quantity));
                if ($pi->quantity_balance >= $quantity && $quantity > 0) {
                    $balance_qty = $pi->quantity_balance - $quantity;
                    $cost_row = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => $pi->id, 'quantity' => $real_item_qty, 'purchase_net_unit_cost' => $pi->net_unit_cost, 'purchase_unit_cost' => $purchase_unit_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => $balance_qty, 'inventory' => 1, 'option_id' => $option_id);
                    $quantity = 0;
                } elseif ($quantity > 0) {
                    $quantity = $quantity - $pi->quantity_balance;
                    $balance_qty = $quantity;
                    $cost_row = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => $pi->id, 'quantity' => $pi->quantity_balance, 'purchase_net_unit_cost' => $pi->net_unit_cost, 'purchase_unit_cost' => $purchase_unit_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => 0, 'inventory' => 1, 'option_id' => $option_id);
                }
            }
            $cost[] = $cost_row;
            if ($quantity == 0) {
                break;
            }
        }
        if ($quantity > 0) {
            $this->session->set_flashdata('error', sprintf(lang("quantity_out_of_stock_for_%s"), ($pi->product_name ? $pi->product_name : $product_name)));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        return $cost;
    }

    public function getPurchasedItems($product_id, $warehouse_id, $option_id = NULL) {
        $orderby = ($this->Settings->accounting_method == 1) ? 'asc' : 'desc';
        $this->db->select('purchase_items.id, available_stk AS quantity, available_stk as quantity_balance, net_unit_cost, unit_cost, item_tax');
        $this->db->where('item_id', $product_id)->where('purchase_items.warehouse_id', $warehouse_id)->where('available_stk !=', 0)->where('product_id', $product_id);
        /*if ($option_id) {
            $this->db->where('option_id', $option_id);
        }*/
        $this->db->group_by('id');
        $this->db->order_by('date', $orderby);

        $this->db->join('item_stk_profile','purchase_items.product_id=item_stk_profile.item_id', 'inner');
        $this->db->order_by('purchase_id', $orderby);
        $q = $this->db->get('purchase_items');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductComboItems($pid, $warehouse_id = NULL)
    {
        $this->db->select('products.id as id, combo_items.item_code as code, combo_items.quantity as qty, products.name as name, products.type as type, combo_items.unit_price as unit_price, warehouses_products.quantity as quantity')
            ->join('products', 'products.code=combo_items.item_code', 'left')
            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->group_by('combo_items.id');
        if($warehouse_id) {
            $this->db->where('warehouses_products.warehouse_id', $warehouse_id);
        }
        $q = $this->db->get_where('combo_items', array('combo_items.product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return FALSE;
    }

    public function item_costing($item, $pi = NULL) {
        $item_quantity = $pi ? $item['aquantity'] : $item['quantity'];
        
        if (!isset($item['option_id']) || $item['option_id'] == 'null') {
            $item['option_id'] = NULL;
        }

        if ($this->Settings->accounting_method != 2 && !$this->Settings->overselling) {

            if ($this->site->getProductByID($item['product_id'])) {
                if ($item['product_type'] == 'standard') {
                    $cost = $this->site->calculateCost($item['product_id'], $item['warehouse_id'], $item['net_unit_price'], $item['unit_price'], $item['quantity'], $item['product_name'], $item['option_id'], $item_quantity);
                } elseif ($item['product_type'] == 'combo') {
                    $combo_items = $this->getProductComboItems($item['product_id'], $item['warehouse_id']);
                    foreach ($combo_items as $combo_item) {
                        $pr = $this->getProductByCode($combo_item->code);
                        if ($pr->tax_rate) {
                            $pr_tax = $this->site->getTaxRateByID($pr->tax_rate);
                            if ($pr->tax_method) {
                                $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $pr_tax->rate) / (100 + $pr_tax->rate));
                                $net_unit_price = $combo_item->unit_price - $item_tax;
                                $unit_price = $combo_item->unit_price;
                            } else {
                                $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $pr_tax->rate) / 100);
                                $net_unit_price = $combo_item->unit_price;
                                $unit_price = $combo_item->unit_price + $item_tax;
                            }
                        } else {
                            $net_unit_price = $combo_item->unit_price;
                            $unit_price = $combo_item->unit_price;
                        }
                        if ($pr->type == 'standard') {
                            $cost = $this->site->calculateCost($pr->id, $item['warehouse_id'], $net_unit_price, $unit_price, ($combo_item->qty * $item['quantity']), $pr->name, NULL, $item_quantity);
                        } else {
                            $cost = array(array('date' => date('Y-m-d'), 'product_id' => $pr->id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => ($combo_item->qty * $item['quantity']), 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $combo_item->unit_price, 'sale_unit_price' => $combo_item->unit_price, 'quantity_balance' => NULL, 'inventory' => NULL));
                        }
                    }
                } else {
                    $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $item['net_unit_price'], 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => NULL, 'inventory' => NULL));
                }
            } elseif ($item['product_type'] == 'manual') {
                $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $item['net_unit_price'], 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => NULL, 'inventory' => NULL));
            }

        } else {
            if ($this->site->getProductByID($item['product_id'])) {

                if ($item['product_type'] == 'standard') {
                    $cost = $this->site->calculateAVCost($item['product_id'], $item['warehouse_id'], $item['net_unit_price'], $item['unit_price'], $item['quantity'], $item['product_name'], $item['option_id'], $item_quantity);
                } elseif ($item['product_type'] == 'combo') {
                    $combo_items = $this->getProductComboItems($item['product_id'], $item['warehouse_id']);
                    foreach ($combo_items as $combo_item) {
                        $cost = $this->site->calculateAVCost($combo_item->id, $item['warehouse_id'], ($combo_item->qty * $item['quantity']), $item['unit_price'], $item['quantity'], $item['product_name'], $item['option_id'], $item_quantity);
                    }
                } else {
                    $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $item['net_unit_price'], 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => NULL, 'inventory' => NULL));
                }
            } elseif ($item['product_type'] == 'manual') {
                $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $item['net_unit_price'], 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => NULL, 'inventory' => NULL));
            }

        }
        return $cost;
    }

    public function costing($items) {        
        $citems = array();
        foreach ($items as $item) {
            $pr = $this->getProductByID($item['product_id']);            
            if ($pr->type == 'standard') {
                if (isset($citems['p' . $item['product_id'] . 'o' . $item['option_id']])) {
                    $citems['p' . $item['product_id'] . 'o' . $item['option_id']]['aquantity'] += $item['quantity'];
                } else {
                    $citems['p' . $item['product_id'] . 'o' . $item['option_id']] = $item;
                    $citems['p' . $item['product_id'] . 'o' . $item['option_id']]['aquantity'] = $item['quantity'];
                }
               /* echo "<pre>";
                print_r($citems);
                exit;*/
            } elseif ($pr->type == 'combo') {
                $combo_items = $this->getProductComboItems($item['product_id'], $item['warehouse_id']);
                foreach ($combo_items as $combo_item) {
                    if ($combo_item->type == 'standard') {
                        if (isset($citems['p' . $combo_item->id . 'o' . $item['option_id']])) {
                            $citems['p' . $combo_item->id . 'o' . $item['option_id']]['aquantity'] += ($combo_item->qty*$item['quantity']);
                        } else {
                            $cpr = $this->getProductByID($combo_item->id);
                            if ($cpr->tax_rate) {
                                $cpr_tax = $this->site->getTaxRateByID($cpr->tax_rate);
                                if ($cpr->tax_method) {
                                    $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $cpr_tax->rate) / (100 + $cpr_tax->rate));
                                    $net_unit_price = $combo_item->unit_price - $item_tax;
                                    $unit_price = $combo_item->unit_price;
                                } else {
                                    $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $cpr_tax->rate) / 100);
                                    $net_unit_price = $combo_item->unit_price;
                                    $unit_price = $combo_item->unit_price + $item_tax;
                                }
                            } else {
                                $net_unit_price = $combo_item->unit_price;
                                $unit_price = $combo_item->unit_price;
                            }
                            $cproduct = array('product_id' => $combo_item->id, 'product_name' => $cpr->name, 'product_type' => $combo_item->type, 'quantity' => ($combo_item->qty*$item['quantity']), 'net_unit_price' => $net_unit_price, 'unit_price' => $unit_price, 'warehouse_id' => $item['warehouse_id'], 'item_tax' => $item_tax, 'tax_rate_id' => $cpr->tax_rate, 'tax' => ($cpr_tax->type == 1 ? $cpr_tax->rate.'%' : $cpr_tax->rate), 'option_id' => NULL);
                            $citems['p' . $combo_item->id . 'o' . $item['option_id']] = $cproduct;
                            $citems['p' . $combo_item->id . 'o' . $item['option_id']]['aquantity'] = ($combo_item->qty*$item['quantity']);
                        }
                    }
                }
            }
        }
        // $this->sma->print_arrays($combo_items, $citems);
        $cost = array();
        foreach ($citems as $item) {
            $item['aquantity'] = $citems['p' . $item['product_id'] . 'o' . $item['option_id']]['aquantity'];
            $cost[] = $this->item_costing($item, TRUE);
        }
        return $cost;
    }

    public function syncQuantity($sale_id = NULL, $purchase_id = NULL, $oitems = NULL, $product_id = NULL) {
        if ($sale_id) {

            $sale_items = $this->getAllSaleItems($sale_id);
            foreach ($sale_items as $item) {
                if ($item->product_type == 'standard') {
                    $this->syncProductQty($item->product_id, $item->warehouse_id);
                    if (isset($item->option_id) && !empty($item->option_id)) {
                        $this->syncVariantQty($item->option_id, $item->warehouse_id, $item->product_id);
                    }
                } elseif ($item->product_type == 'combo') {
                    $combo_items = $this->getProductComboItems($item->product_id, $item->warehouse_id);
                    foreach ($combo_items as $combo_item) {
                        if($combo_item->type == 'standard') {
                            $this->syncProductQty($combo_item->id, $item->warehouse_id);
                        }
                    }
                }
            }

        } elseif ($purchase_id) {

            $purchase_items = $this->getAllPurchaseItems($purchase_id);
            foreach ($purchase_items as $item) {
                $this->syncProductQty($item->product_id, $item->warehouse_id);
                if (isset($item->option_id) && !empty($item->option_id)) {
                    $this->syncVariantQty($item->option_id, $item->warehouse_id, $item->product_id);
                }
            }

        } elseif ($oitems) {

            foreach ($oitems as $item) {
                if (isset($item->product_type)) {
                    if ($item->product_type == 'standard') {
                        $this->syncProductQty($item->product_id, $item->warehouse_id);
                        if (isset($item->option_id) && !empty($item->option_id)) {
                            $this->syncVariantQty($item->option_id, $item->warehouse_id, $item->product_id);
                        }
                    } elseif ($item->product_type == 'combo') {
                        $combo_items = $this->getProductComboItems($item->product_id, $item->warehouse_id);
                        foreach ($combo_items as $combo_item) {
                            if($combo_item->type == 'standard') {
                                $this->syncProductQty($combo_item->id, $item->warehouse_id);
                            }
                        }
                    }
                } else {
                    $this->syncProductQty($item->product_id, $item->warehouse_id);
                    if (isset($item->option_id) && !empty($item->option_id)) {
                        $this->syncVariantQty($item->option_id, $item->warehouse_id, $item->product_id);
                    }
                }
            }

        } elseif ($product_id) {
            $warehouses = $this->getAllWarehouses();
            foreach ($warehouses as $warehouse) {
                $this->syncProductQty($product_id, $warehouse->id);
                if ($product_variants = $this->getProductVariants($product_id)) {
                    foreach ($product_variants as $pv) {
                        $this->syncVariantQty($pv->id, $warehouse->id, $product_id);
                    }
                }
            }
        }
    }

    public function getProductVariants($product_id)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $product_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllSaleItems($sale_id) {
        $q = $this->db->get_where('sale_items', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllPurchaseItems($purchase_id) {
        $q = $this->db->get_where('purchase_items', array('purchase_id' => $purchase_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function syncPurchaseItems($data = array()) {
        
        if (!empty($data)) {
            
            foreach ($data as $items) {
               
                foreach ($items as $item) {
                    
                    if (isset($item['pi_overselling'])) {
                        unset($item['pi_overselling']);
                        $option_id = (isset($item['option_id']) && !empty($item['option_id'])) ? $item['option_id'] : NULL;
                        $clause = array('purchase_id' => NULL, 'transfer_id' => NULL, 'product_id' => $item['product_id'], 'warehouse_id' => $item['warehouse_id'], 'option_id' => $option_id);
                        if ($pi = $this->site->getPurchasedItem($clause)) {
                            $quantity_balance = $pi->quantity_balance + $item['quantity_balance'];
                            $this->db->update('purchase_items', array('quantity_balance' => $quantity_balance), $clause);
                            //$avl_balance = 
                             //$this->db->update('item_stk_profile', array('available_stk' => $avl_balance), $clause);
                        } else {
                            $clause['quantity'] = 0;
                            $clause['item_tax'] = 0;
                            $clause['quantity_balance'] = $item['quantity_balance'];
                            $this->db->insert('purchase_items', $clause);
                        }
                    } else {
                        if ($item['inventory']) {
                            $this->db->update('purchase_items', array('quantity_balance' => $item['quantity_balance']), array('id' => $item['purchase_item_id']));
                        }
                    }
                }
            }
            return TRUE;
        }
        return FALSE;
    }

    public function getProductByCode($code)
    {
        $q = $this->db->get_where('products', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
	
	 /*
     * Added by Ajay
     * on 01-04-2016
     * This retrieves all sales executives of a biller
     */

    public function getSalesExecutivesByBiller($biller_id = NULL,$group_id = NULL) {
        if (!empty($biller_id) && !empty($group_id)) {
            $q = $this->db->get_where('users', array('biller_id' => $biller_id,'group_id'=>$group_id));
            if ($q->num_rows() > 0) {
                return $q->result_array();
            }
            return FALSE;
        }else if(!empty($biller_id)){
			$q = $this->db->get_where('users',array('biller_id' => $biller_id));
            if ($q->num_rows() > 0) {
                return $q->result_array();
            }
            return FALSE;
		}else if(!empty($group_id)){
			$q = $this->db->get_where('users',array('group_id'=>$group_id));
            if ($q->num_rows() > 0) {
                return $q->result_array();
            }
            return FALSE;
		}
    }
	
	/***********************************Credit Voucher********************/
	 public function getCreditVoucherByID($id) {
        $q = $this->db->get_where('credit_voucher', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
	
	public function getCreditVoucherByNO($no) {
        $q = $this->db->get_where('credit_voucher', array('card_no' => $no), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    
        // Add by Ankit for implemented N level category
    public function getNlevelCategories() {
        $this->db->order_by('name');
       // $q = $this->db->get('categories');
        $q = $this->db->get_where("categories", array('parent_id' => 0));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
    
        // Add By Ankit/Ajay for implements N level catogory
    public function getNEndsCategoriesByCategoryID($category_id)
    {
        //echo "cat=>".$category_id;
        $this->db->order_by('name');
        $q = $this->db->get_where("categories", array('parent_id' => $category_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            //echo "<pre>";print_r($data);
            return $data;
        }

        return FALSE;
    }
     // Add By Ankit/Ajay for implements N level catogory
    public function getNEndCategoriesByCategoryID($category_id)
    {
        $cat = array();
        $this->db->order_by('name');
        $q = $this->db->get_where("categories", array('parent_id' => $category_id));
         if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row->id;
            }
            //echo "<pre>";print_r($data);
            //$s= $data[0]->id;
        }
        for($i=0;$i<count($data);$i++)
        {
        $this->db->order_by('name');
        $q = $this->db->get_where("categories", array('parent_id' => $data[$i]));
         if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $cat[] = $row;
            }

            //return $cat;
        }
            
        }
        

        return $cat;
    }
    // added by vikas singh  
  public function get_warehouse_ids($whtype) {
   // print_r($_SESSION); die;
    $warehouse_id = $_SESSION['pwid'];
  
    $wh_id_array = explode(',',$warehouse_id);

    $this->db->select('id, name, wh_id, wh_type');
    $this->db->from('warehouses');
    $this->db->where_in('id',$wh_id_array);
    $this->db->where('segment_id',$_SESSION['segment_id']);
    $this->db->where('wh_type',$whtype);
    if($whtype==1){

        $where = "name NOT LIKE '%EXB%'";
        $this->db->where($where);
    }
    $query = $this->db->get();
    $ar= $query->result();
   
    $pwh_id = array();
    $values = array();
    for($i=0; $i< count($ar); $i++)
    {
        $values['id'] = $ar[$i]->id;
        $values['name'] = $ar[$i]->name;
        $values['wh_id'] = $ar[$i]->wh_id;
       // $values = $ar[$i]->id; 
        array_push($pwh_id ,$values);
    }
   // $get_wh_id = implode('_',$pwh_id);
    //$_SESSION['get_wh_id'] = $get_wh_id;
    return $pwh_id;
 }

 public function get_warehouse_salein($whtype) {
   //print_r($_SESSION); die;
     $where123 = "name LIKE '%CON%'";
    $warehouse_id = $_SESSION['pwid'];
  
    $wh_id_array = explode(',',$warehouse_id);

    $this->db->select('id, name, wh_id, wh_type');
    $this->db->from('warehouses');
    $this->db->where_in('id',$wh_id_array);
    $this->db->where('segment_id',$_SESSION['segment_id']);
    $this->db->where('wh_type',$whtype);
    $this->db->where($where123);
    if($whtype==1){

        $where = "name NOT LIKE '%EXB%'";
        $this->db->where($where);
    }
    $query = $this->db->get();
    $ar= $query->result();// print_r($ar); die;
   
    $pwh_id = array();
    $values = array();
    for($i=0; $i< count($ar); $i++)
    {
        $values['id'] = $ar[$i]->id;
        $values['name'] = $ar[$i]->name;
        $values['wh_id'] = $ar[$i]->wh_id;
       // $values = $ar[$i]->id; 
        array_push($pwh_id ,$values);
    }
   // $get_wh_id = implode('_',$pwh_id);
    //$_SESSION['get_wh_id'] = $get_wh_id;
    return $pwh_id;
 }
	
	/*********************************************************************/
// added by vikas singh
    public function syncQtyPurchaseItems($data = array(),$sale_id,$sale_item_array,$reference_no) {      

        if (!empty($data)) {
           
             foreach ($data as $items) {
                   
                    $this->db->select("products.wh_id,item_stk_profile.item_id,item_stk_profile.warehouse_id,item_stk_profile.org_id,item_stk_profile.available_stk,item_stk_lot.total_stk as slot_total,item_stk_lot_bin.total_stk as sbin_total");
                    $this->db->from("item_stk_profile");
                    $this->db->join("products","products.id = item_stk_profile.item_id","inner");
                     
                    $this->db->join("item_stk_lot","item_stk_lot.item_stk_id = item_stk_profile.id AND item_stk_lot.warehouse_id = item_stk_profile.warehouse_id","inner");
                    $this->db->join("item_stk_lot_bin","item_stk_lot_bin.lot_id = item_stk_lot.id AND item_stk_lot_bin.warehouse_id = item_stk_lot.warehouse_id","inner");
                    $this->db->where("item_stk_profile.item_id",$items['product_id']);
                    $this->db->where("item_stk_lot.lot_no",$items['lot_no']);
                    $this->db->where("item_stk_lot_bin.bin_id",$items['bin_id']);
                    $q= $this->db->get();
                    $qtydata = $q->result();
                    $available_stk= $qtydata[0]->available_stk - $items['quantity'];
                    $slot_total=    $qtydata[0]->slot_total - $items['quantity'] ;
                    $sbin_total=    $qtydata[0]->sbin_total - $items['quantity'];
                    $item_id = $items['product_id'];
                    $lot_no = $items['lot_no'];
                    $bin_id = $items['bin_id'];
                    $segment_id = $_SESSION['segment_id'];
                    
                    if($item_id >0)
                    {

                       $this->db->query("update sma_item_stk_profile SET available_stk = ".$available_stk.", upd_flg=1 where item_id= ".$item_id." and segment_id= ".$segment_id."");
                     
                      
                         // if($lot_data!='' && $bin_data!=''){
                            $lot_Data = $this->db->select("*")->from("item_stk_lot")->where("item_id",$items['product_id'])->where("total_stk >",0)->order_by("id",ASC)->get()->result();
                            
                            $minus_qty = 0;
                            //$sum = $items['quantity'];
                            
                            /*echo "<pre>";
                            print_r($lot_Data);
                            echo "=============";*/
                            //$sum = 26;
                            $lots = array();
                            $stock_sale = array();
                            $bins = array();
                            $stock_saleb = array();
                            $bin_sum = $items['quantity'];
                            $sum = $items['quantity'];

                            //echo "sum".$sum;
                            foreach ($lot_Data as $key => $value) {
                             $bin_data = $this->db->select("bin.bin_nm,item_stk_lot_bin.*")->from("item_stk_lot_bin")->join('bin','bin.id=item_stk_lot_bin.bin_id','inner')->where("item_id",$items['product_id'])->where("lot_id",$value->id)->where("total_stk >",0)->order_by("id",ASC)->get()->result();
                           //  echo "<pre>";
                             //print_r($bin_data);exit;
                                foreach($bin_data  as $bin_val){
                                   // echo "bin cum".$bin_sum."<br>";
                                    if(($bin_sum<=$bin_val->total_stk) && ($bin_sum!=0)){
                                        
                                        $left_qty_bin = $bin_val->total_stk-$bin_sum;
                                        $dtb['left_quantity'] = $left_qty_bin;
                                        $dtb['id'] = $bin_val->id;
                                        $stock_sale_bin['bin_id'] = $bin_val->id;
                                        $stock_sale_bin['lot_id'] = $bin_val->lot_id;
                                        $stock_sale_bin['bin_name'] = $bin_val->bin_nm;
                                       // $stock_sale_bin['lot_no'] = $bin_val->lot_no;
                                        $stock_sale_bin['quantity'] = $bin_sum;                                     
                                        array_push($bins,$dtb);
                                        array_push($stock_saleb,$stock_sale_bin);                                  
                                        
                                        break; 
                                    }
                                    else if($bin_sum > $bin_val->total_stk){                
                                       
                                        $dtb['left_quantity'] = 0;
                                        $dtb['id'] = $bin_val->id;
                                        
                                        $stock_sale_bin['bin_id'] = $bin_val->id;
                                        $stock_sale_bin['lot_id'] = $bin_val->lot_id;
                                        $stock_sale_bin['bin_name'] = $bin_val->bin_nm;
                                       // $stock_sale_lot['lot_no'] = $value->lot_no;
                                        
                                        $stock_sale_bin['quantity'] = $bin_val->total_stk; 
                                        $bin_sum = $bin_sum - $bin_val->total_stk;

                                        array_push($bins,$dtb);
                                        array_push($stock_saleb,$stock_sale_bin);
                                       //  echo "asd".$sum;exit;
                                    }
                                }

                            // lot quantity calculation


                                if($sum<=$value->total_stk){
                                   //  echo $value->total_stk."------".$sum;
                                    $left_qty = $value->total_stk-$sum;
                                    $dt['left_quantity'] = $left_qty;
                                    $dt['id'] = $value->id;
                                    $stock_sale_lot['lot_id'] = $value->id;
                                    $stock_sale_lot['lot_no'] = $value->lot_no;
                                    $stock_sale_lot['quantity'] = $sum;                                     
                                    array_push($lots,$dt);
                                    array_push($stock_sale,$stock_sale_lot);
                                    break; 
                                }
                                if($sum > $value->total_stk){                
                                   
                                    $dt['left_quantity'] = 0;
                                    $dt['id'] = $value->id;
                                    
                                    $stock_sale_lot['lot_id'] = $value->id;
                                    $stock_sale_lot['lot_no'] = $value->lot_no;
                                    $stock_sale_lot['quantity'] = $value->total_stk; 
                                    $sum = $sum - $value->total_stk;
                                    array_push($lots,$dt);
                                    array_push($stock_sale,$stock_sale_lot);
                                   //  echo "asd".$sum;exit;
                                }
                            }
                            /*echo "<pre>";
                           // print_r($stock_saleb);
                            echo "-----------------";
                            print_r($stock_saleb);
                            echo "----------------------<br>";
                            print_r($bins);
                            exit;
                            exit;*/
                        //}
                        

                        if(count($lots)>0){

                            foreach($lots as $lot_val){
                               
                                $this->db->query("update sma_item_stk_lot SET total_stk = ".$lot_val['left_quantity'].",upd_flg=1 where item_id= ".$item_id." and id = '".$lot_val['id']."' and segment_id= ".$segment_id.""); 

                            }
                            foreach($bins as $bins_val){
                               
                                $this->db->query("update sma_item_stk_lot_bin SET total_stk = ".$bins_val['left_quantity'].",upd_flg=1 where item_id= ".$item_id." and id= ".$bins_val['id']." and segment_id= ".$segment_id.""); 

                            }
                           

                          //  print_r($stock_sale_lot);exit;    
                            // inserting lot and bin data into sale invoice
                            foreach($stock_sale as $lot){
                               // echo "<pre>";
                                //print_r($lots_data);exit;
                                $sale_lot = array("org_id"=>$qtydata[0]->org_id,"wh_id"=>$qtydata[0]->wh_id,"warehouse_id"=>$qtydata[0]->warehouse_id,"code"=>$items['product_code'],"sale_id"=>$sale_id,"sale_item_id"=>$items['sale_item_id'],"lot_id"=>$lot['lot_id'],"lot_no"=>$lot["lot_no"],"quantity"=>$lot['quantity'],"created_date"=>date("Y-m-d H:i:s"),'sale_ref_no'=>$reference_no);
                                $this->db->insert('sale_item_lot',$sale_lot);

                            }
                            foreach($stock_saleb as $bin){
                                $sale_bin = array("org_id"=>$qtydata[0]->org_id,"wh_id"=>$qtydata[0]->wh_id,"warehouse_id"=>$qtydata[0]->warehouse_id,"code"=>$items['product_code'],"sale_id"=>$sale_id,"sale_item_id"=>$items['sale_item_id'],"lot_id"=>$bin['lot_id'],"bin_id"=>$bin["bin_id"],"bin_name"=>$bin["bin_name"],"quantity"=>$bin['quantity'],"created_date"=>date("Y-m-d H:i:s"),'sale_ref_no'=>$reference_no,'lot_no'=>$lot["lot_no"]);

                                $this->db->insert('sale_item_bin',$sale_bin);                                

                            }
                        }                       

                    }
         
            } 
            return TRUE;
        }
        return FALSE;
    }

    public function rrmdir($dir) {
      
       if (is_dir($dir)) {
         $objects = scandir($dir);
         foreach ($objects as $object) {
           if ($object != "." && $object != "..") {
             if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object);
           }
         }
         reset($objects);
         rmdir($dir);
       }
     }
     
     /*
	* Added by Ajay
	* on 23-06-2016
	* delete suspended bills on close register of logged in user
	*/
	public function deleteSuspendedBills(){
            $suspended_bills = $this->getSuspendBills();
            $suspended_items = $this->getSuspendedItems($suspended_bills);
            foreach($suspended_bills as $key=>$val){
		$this->db->where('id',$val)->delete('suspended_bills');
            }
            $bills = $this->db->affected_rows();
            foreach($suspended_items as $k=>$v){
		$this->db->where('id',$v)->delete('suspended_items');
            }
            $items = $this->db->affected_rows();
            return array($bills,$items);
	}
	
	public function getSuspendBills(){
            $q = $this->db->where(array('created_by'=>$_SESSION['user_id']))->get('suspended_bills');	
            if ($q->num_rows() > 0) {
		$sus_id = array();
		foreach($q->result() as $k=>$v){
                    array_push($sus_id,$v->id);
		}		
            return $sus_id;
            }	
            return FALSE;
	}
	
	public function getSuspendedItems($bills){
        $sus_items = array();
        if(count($bills) > 0){
		  foreach($bills as $key=>$val){
                    $q = $this->db->select('id')->where('suspend_id',$val)->get('suspended_items');
                    if ($q->num_rows() > 0) {
                        foreach($q->result() as $k => $v){
                            $sus_items[] = $v->id;
			}
                    }	
                }
		return $sus_items;
            }
            return FALSE;
	}

    public function getPaymentMode(){
       /* $this->db->select("*");
        $this->db->from("payment_modes");        
        $this->db->where("status",1);      
        $paymentData = $this->db->get()->result();
        if(count($paymentData)>0)
            return $paymentData;*/

    }


    public function syncProductQtyNew($id,$item_details){
        // echo "<pre>";
        // print_r($item_details);
        // exit;
        if($id){           
            foreach($item_details as $row){            
                $qty_to_be_deducted = $row['quantity'];
                $lot_Data = $this->db->select('id,lot_id,quantity')->from('sale_item_lot')->where('sale_id',$id)->where('sale_item_id',$row['sale_item_id'])->order_by('id',DESC)->get()->result();                
                
              $this->db->where('item_id',$row['product_id']);
               $this->db->set('available_stk', 'available_stk + ' . (int) $row['quantity'], FALSE);
              $this->db->update('item_stk_profile'); 

                //$this->db->update('item_stk_lot_bin',$bin_data);
                 
                // echo "lot qty".$qty_to_be_deducted."====<br>";
                 foreach ($lot_Data as $key => $value) { // lot calculation
                    $lot_qty = $value->quantity;                    
                    if($qty_to_be_deducted==0){
                        break;
                    }
                    if($qty_to_be_deducted<$lot_qty){
                       
                        $lot_array[$row['sale_item_id']][$value->lot_id][] = $qty_to_be_deducted;
                       // $lot_array[$row['sale_item_id']][$row['sale_item_id'].'~'.$value->lot_id] = $qty_to_be_deducted;
                        $qty_to_be_deducted = 0;

                    }else if($qty_to_be_deducted>$lot_qty){
                        $lot_array[$row['sale_item_id']][$value->lot_id][] = $value->quantity;
                       // $lot_array[$row['sale_item_id']][$row['sale_item_id'].'~'.$row['product_id']] = $value->quantity;
                        $qty_to_be_deducted = $qty_to_be_deducted-$value->quantity;

                    }else if($qty_to_be_deducted==$lot_qty){
                        $lot_array[$row['sale_item_id']][$value->lot_id][] = $value->quantity;
                      //  $lot_array[$row['sale_item_id']][$row['sale_item_id'].'~'.$row['product_id']] = $value->quantity;
                        $qty_to_be_deducted = 0;
                    }

                     if($bin_qty_to_be_deducted==0){
                        $bin_qty_to_be_deducted = $row['quantity'];
                     }
                     
                     $bin_data = $this->db->select('quantity,bin_id,id,sale_item_id')->from('sale_item_bin')->where('sale_id',$id)->where('lot_id',$value->lot_id)->where('sale_item_id',$row['sale_item_id'])->get()->result();   
                    // echo "<pre>";
                    // print_r($bin_data);
                    // echo "======================<br>";
                   
                   // echo "initially".$bin_qty_to_be_deducted."====<br>";
                    foreach ($bin_data as $keys => $bin_value) { // lot calculation
                        $bin_qty = $bin_value->quantity; 
                     //  echo "bin qty".$bin_qty."to be deducted==>".$bin_qty_to_be_deducted."item",$row['sale_item_id']."<br>==========<br>";
                        if($bin_qty_to_be_deducted==0){
                            break;
                        }
                        if($bin_qty_to_be_deducted<$bin_qty){
                            $bin_array[$row['sale_item_id']][$value->lot_id][] = $bin_qty_to_be_deducted;;
                          //  $bin_array[$row['sale_item_id']][$row['sale_item_id'].'~'.$row['product_id']] = $bin_qty_to_be_deducted;
                            $bin_qty_to_be_deducted = 0;

                        }else if($bin_qty_to_be_deducted>$bin_qty){
                            //echo "qty before".$bin_qty_to_be_deducted;
                             $bin_array[$row['sale_item_id']][$bin_value->bin_id][] = $bin_value->quantity;;
                           // $bin_array[$row['sale_item_id']][$row['sale_item_id'].'~'.$row['product_id']] = $bin_value->quantity;
                            $bin_qty_to_be_deducted = $bin_qty_to_be_deducted-$bin_value->quantity;
                          //  echo $bin_qty_to_be_deducted."==";
                            // echo "qty after".$bin_qty_to_be_deducted."<br>";
                        }else if($bin_qty_to_be_deducted==$bin_qty){                           
                            $bin_array[$row['sale_item_id']][$bin_value->bin_id][] = $bin_value->quantity;;
                           // $bin_array[$row['sale_item_id']][$row['sale_item_id'].'~'.$row['product_id']] = $bin_value->quantity;
                            $bin_qty_to_be_deducted = 0;
                        }
                    }

                }              

            }
          //  exit;
           //  echo "<pre>";
           //  print_r($lot_array);
           //  echo "=============================<br>";
           //  print_r($bin_array);
           //  //echo "---------------------------------";
           // exit;
            // deducting quantity from sales lot and bin and increasing quantity in stock lot and bin
            foreach ($item_details as $ikey => $i_row) {
               
              
                foreach ($lot_array[$i_row['sale_item_id']] as $lot_key => $datas) {  
                   foreach($datas as $val){
                      // echo "<pre>";
                      //   print_r($val);
                      //   echo "lot".$lot_key;
                      //   echo "===============";
                        $this->db->where('id',$lot_key);
                        $this->db->set('total_stk', 'total_stk + ' . (int) $val, FALSE);
                        $this->db->update('item_stk_lot');
                   }
                  
                               
                                   
                }
            }
            
           

            

            foreach ($item_details as $bkey => $i_row) {
               
                foreach ($bin_array[$i_row['sale_item_id']] as $bin_key => $bin_qty) {
                    foreach($bin_qty as $val){
                        $this->db->where('id',$bin_key);
                        $this->db->set('total_stk', 'total_stk + ' . (int) $val, FALSE);
                        $this->db->update('item_stk_lot_bin');   
                    }      
                }
            }
            return true;
          
        }
        return false;

    }

        

    public function checkColumn($column_name,$table_name){
        //echo "db name".DATABASE_NAME."===>".$table_name;exit;
        $db_name = DATABASE_NAME;
        $fields = $this->db->query("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='{$table_name}' AND COLUMN_NAME = '{$column_name}' AND TABLE_SCHEMA='{$db_name}';"); 
       
        if ($fields->result_id->num_rows==0)
        {

           // echo "ALTER TABLE {$table_name} ADD {$column_name} VARCHAR(255) DEFAULT NULL";exit;
            $result = $this->db->query("ALTER TABLE {$table_name} ADD {$column_name} VARCHAR(255) DEFAULT NULL");
            return true;
        }
        return false;

    }

}
?>
