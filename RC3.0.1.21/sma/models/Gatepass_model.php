<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Gatepass_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function getSetting()
    {
        $q = $this->db->get('pos_settings');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

 // Add By Ankit new
    public function getTypeList($warehouse_id, $typ) {
       // $q = "SELECT a.doc_no as id, a.doc_no FROM sma_emrs_doc_src a INNER JOIN sma_emrs b ON (b.warehouse_id = a.warehouse_id AND b.doc_id = a.doc_id) INNER JOIN sma_emrs_item c ON (c.warehouse_id = a.warehouse_id AND c.doc_no_src_id = a.doc_no) WHERE b.warehouse_id = '".$warehouse_id."' AND b.emrs_type = '".$typ."' AND b.emrs_status = 1 AND c.status IN('0', '2') GROUP BY (a.doc_no)";
        $q = "SELECT a.doc_no as id, a.doc_no FROM sma_emrs_doc_src a INNER JOIN sma_emrs b ON (b.warehouse_id = a.warehouse_id AND b.doc_id = a.doc_id) WHERE b.warehouse_id = '".$warehouse_id."' AND b.emrs_type = '".$typ."' AND b.emrs_status = 1 GROUP BY (a.doc_no)";
        if($a= $this->db->query($q))
           {      

           // print_r($a->result());
             
            return $a->result();

           }
        

    }

    // Add By Ankit
    public function getWareHouseDetail_out($warehouse_id=0)
    {
        $result = array('warehouse_name'=>'','po_data'=>array(),'tp_details'=>array(),'supplier_details'=>array());
        $this->db->where('id', $warehouse_id);
        $this->db->select('name,biller_id');
        $this->db->from('warehouses');
        $warehouse_detail = $this->db->get()->row();
        $result['warehouse_name'] = $warehouse_detail;
        $this->db->select('*');
        $tp_details = $this->db->from('companies')
        ->where("eo_type", 'T'); 
        $this->db->group_by("companies.eo_id");       
        $transporter_detail = $this->db->get()->result();
        $result['tp_details'] = $transporter_detail;

        $this->db->select('*');
        $tp_details = $this->db->from('companies')
        ->where("group_id", 4);       
        $supplier_detail = $this->db->get()->result();
        $result['supplier_details'] = $supplier_detail;


        return $result;
    }

    public function getWareHouseDetail($gpData,$warehouse_id=0)
    {
        $result = array('warehouse_name'=>'', 'tp_details'=>array(),'supplier_details'=>array(),'gatepass_type'=>array(), 'sto_data'=>array());

        $warehouse_detail = $this->site->get_warehouse_ids(2);        
        $result['warehouse_name'] = $warehouse_detail;

        $this->db->select('*');
        $this->db->from('gatepass_types');
        $this->db->where('status',1)->where('name like "%"','in');
        $gatepass_type = $this->db->get()->result();
        $result['gatepass_type'] = $gatepass_type;

        $this->db->select('id, name, eo_id');
        $this->db->where("eo_type", 'T');
        $this->db->group_by("companies.eo_id");
        $tp_details = $this->db->from('companies');       
        $transporter_detail = $this->db->get()->result();
        $result['tp_details'] = $transporter_detail;

        $this->db->select('id, name, eo_id');
        $this->db->where("group_id", 4);
        $supplier_detail = $this->db->from('companies');
        $supplier_detail = $this->db->get()->result();
        $result['supplier_details'] = $supplier_detail;

        $this->db->select('emrs_doc_src.*');
        $this->db->from('emrs_doc_src')
        ->join('emrs', 'emrs.warehouse_id = emrs_doc_src.warehouse_id', 'inner')
        ->where('emrs.warehouse_id', $warehouse_id);
        $this->db->group_by('id');

        $sto_detail = $this->db->get()->result();
        $sto_list = array();
        if(count($sto_detail)>0){
            foreach($sto_detail as $row){
                array_push($sto_list, $row);
            }
            $result['sto_data'] = $sto_list;
        }

        return $result;
    }

    public function deleteBinData($bin_id){
        $this->db->where('id', $bin_id);
        $this->db->delete('gp_lot_bin');
        if($this->db->affected_rows())
            return 1;
        else
            return 0;
    }

    public function deleteLotData($id){
        $this->db->where('id', $id);
        $this->db->delete('gp_item_lot');
        if($this->db->affected_rows())
            return 1;
        else
            return 0;
    }

  // Add By Ankit
    public function getGatepasstype(){
        $this->db->where("type_id",'1081');
        $this->db->or_where("type_id",'1086');
        $this->db->or_where("type_id",'1083');
        $this->db->or_where("type_id",'1088');
        $this->db->where("status",'1');
        $query = $this->db->get("gatepass_types");
         if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    // Add By Ankit
    public function getWhID(){
        $this->db->select('id,name');
        $this->db->where("wh_type >",1);
        $query = $this->db->get("sma_warehouses");
         if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }



    public function getStoDetail($sto_id)
    {

        $this->db->select("id, doc_id, doc_no, doc_date")
        ->where('doc_id', $sto_id);
        $q = $this->db->get("emrs_doc_src");
     
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            //print_r($data);exit;
            return $data;
        }
        return false;       

    }

    public function getStoProductsList($sto_no,$sto_date,$sto_text){
        $this->db->select("emrs_item.*, products.name as pr_name,products.unit,products.price, emrs.id as emrs,emrs_no")
        ->join("emrs_doc_src","emrs_doc_src.doc_id=emrs_item.doc_id AND emrs_doc_src.doc_no=emrs_item.doc_no_src_id ")
        ->join("emrs","emrs.doc_id=emrs_doc_src.doc_id")
        ->join("products","emrs_item.item_id=products.id")
        ->where("emrs_item.bal_qty > ",0)
        //->where("emrs.doc_id = ",'emrs_doc_src.doc_id')
        ->where("emrs_doc_src.doc_id",$sto_no)
        ->where("emrs_doc_src.doc_no",$sto_text)
        ->where("emrs_doc_src.doc_date",$sto_date)
        ->group_by("emrs_item.item_id");
        $q = $this->db->get('emrs_item');
        
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                if($row->bal_qty != $row->req_qty_bs)
                    $row->avail_qty = $row->bal_qty;
                else
                     $row->avail_qty = $row->req_qty_bs;
                $data[] = $row;
                
                
            }
            //echo "<pre>";print_r($data);exit;
            return $data;
        }
        return false;  

    }
    
  // Add By Ankit new
    public function getStoProducts($sto_no,$sto_date,$sto_text){
        $w = 1;
         $this->db->select("emrs_item.*,cons_item_stk_profile.available_stk,products.name,products.unit,
            products.price,emrs_item.req_qty AS Rqty, emrs_no")
        ->join("emrs_doc_src","emrs_doc_src.doc_id=emrs_item.doc_id AND emrs_doc_src.doc_no=emrs_item.doc_no_src_id ")
        ->join("emrs","emrs_doc_src.doc_id=emrs.doc_id")
        ->join("cons_item_stk_profile","cons_item_stk_profile.warehouse_id = emrs_item.warehouse_id AND cons_item_stk_profile.item_id = emrs_item.item_id")
        ->join("products","products.warehouse = emrs_item.warehouse_id AND products.code = emrs_item.code")
        ->where("emrs_doc_src.doc_id",$sto_no)
        ->where("emrs_doc_src.doc_no",$sto_text)
        ->where("emrs_doc_src.doc_date",$sto_date)
        ->where("emrs_item.req_qty >", '0')        
        ->where("emrs_item.status!=",'1');
         //->or_where("emrs_item.status!=",'3');
        $q = $this->db->get('emrs_item');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
               // foreach (($q->result()) as $row) {
                if($row->bal_qty != $row->req_qty)
                    $row->req_qty = $row->bal_qty;
                else
                     $row->req_qty = $row->req_qty;
                     $data[] = $row;
                
                
            //}
                //$data[] = $row;
            }

          // echo "hi...<pre>";print_r($data);exit;

            return $data;
        }
        return $w;  

    }
    // Add By Ankit new
    public function getStoProductsEdit($sto_no,$sto_date,$itemid){
        $w = 1;
       // print_r($itemid); die;
        $this->db->select("emrs_item.*,cons_item_stk_profile.available_stk,products.name,products.unit,products.price")
        ->join("emrs_doc_src","emrs_doc_src.doc_id=emrs_item.doc_id")
        ->join("cons_item_stk_profile","cons_item_stk_profile.warehouse_id = emrs_item.warehouse_id AND cons_item_stk_profile.item_id = emrs_item.item_id")
        ->join("products","products.warehouse = emrs_item.warehouse_id AND products.code = emrs_item.code")
        ->where_not_in("emrs_item.item_id",$itemid)
        ->where("emrs_doc_src.doc_no",$sto_no)
        ->where("emrs_doc_src.doc_date",$sto_date)
        ->where("emrs_item.status!=",'1')
        //->where("emrs_item.status!=",'3')
        ->where("emrs_item.req_qty >",0);
        $q = $this->db->get('emrs_item');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                foreach (($q->result()) as $row) {
                if($row->bal_qty != $row->req_qty)
                    $row->req_qty = $row->bal_qty;
                else
                     $row->req_qty = $row->req_qty;
                $data[] = $row;
                
                
            }
                //$data[] = $row;
            }

           //echo "<pre>";print_r($data);exit;

            return $data;
        }
        return $w;  

    }
    
    public function getGPProducts($gpId){
        $this->db->select("gp.id, gp_src.*, gp_item.*, gp_item.pending_qty as p_qty, gp_item_lot.*, gp_lot_bin.*, products.name")
        ->join("gp_src","gp_src.gp_id=gp.id", "inner")
        ->join("gp_item","gp_item.gp_src_id=gp_src.id", "inner")
        ->join("gp_item_lot","gp_item_lot.mrn_gp_item_id=gp_item.id", "inner")
        ->join("gp_lot_bin","gp_lot_bin.lot_id=gp_item_lot.id", "inner")
        ->join("products","gp_item.item_id=products.id", "inner")
        ->where("gp.id",$gpId);
        //$this->db->group_by('gp_id');
        $q = $this->db->get('gp');
        if ($q->num_rows() > 0) {
        //    echo "<pre>";print_r($q->result());
           return $q->result();
        }
        return false;  

    }

    public function getAllBin($warehouse_id){
  //  echo $warehouse_id;       
        $allBin = $this->db->from('bin')
        ->where("warehouse_id",$warehouse_id)
        ->get()->result();
       return $allBin;
    }

    public function savegatepassData($postData, $ins_gp=null){
 // echo "Add lot <br> <pre>";print_r($postData);exit;

        if(isset($postData))
        {          
            $stoData = $postData;

            if($_POST['submit-type'] == 2){
                $gp_status = 0;
            }
            else{
                $gp_status = 1;
            }
        
            $count = 0;

            if(!$ins_gp)
            {                 
                foreach($postData['sto_no_new'] as $key=>$val){ //echo "val===>".$val."<br/>"; 
                    $checkflg = $this->findrono($val,$_POST['sto_list123']['undefined'],$_POST['actual_qty']['undefined']);
                //}
                //echo "<pre>";print_r($checkflg); die;
                 
                //if($aaaa=1){
                    $this->db->select('id')->from('gp')->order_by('id','desc')->limit(1);
                    $maxId = $this->db->get()->row();
                    //echo "Id --- ".$maxId->id;
                    $mmax = $maxId->id+1;
                    $doc_id123 = $val;
                    $lr_date = $postData['lr_date'];
                        if($lr_date != ''){
                            $lr_date = $postData['lr_date'];
                        }else{
                            $lr_date = NULL;
                        }
                    
                    if($checkflg)  
                    {                
                    $gpData = array(
                        'ho_id' => 1,
                        'org_id'=>$stoData['org_id'],
                        'wh_id' => $stoData['wh_id'][0],
                        'segment_id'=>$stoData['segment_id'],
                        'warehouse_id' => $stoData['warehouse_id'],
                        'fy_id'=>1,
                        'gp_type'=>$_POST['gatepass_type'],
                        'gp_no'=> $stoData['segment_id'].'/GP/'.(isset($maxId->id)?$mmax:1),
                        'supplier_id'=> $postData['supplier_id'],
                        //'doc_no'=> $stoData['sto_no_new'][0],//$_POST['sto_no'][0],
                        'doc_no' => $val,
                        'doc_dt'=> $_POST['date'][$key],
                        //'doc_dt'=> $_POST['sto_date'],
                        'tp_id' => $_POST['lr_name'],
                        'tpt_lr_no' => $_POST['lr_no'],
                        'tpt_lr_dt' =>  $lr_date,
                        'rcpt_date' => date('Y-m-d',strtotime($_POST['rcpt_date'])),
                        'vehicle_no'=> $_POST['vehicle_no'],
                        'remarks' => $_POST['remarks'],
                        'status' => $gp_status,
                        'addl_amt' => $_POST['other_charges'],
                        'curr_id'=>73,
                        'cust_name' =>'Not Applicable',
                        'user_id' => $_SESSION['user_id'],
                        'created_date' => date('Y-m-d H:i:s'),
                        'gp_date' => date('Y-m-d H:i:s')
                    );
                    $this->db->insert('gp', $gpData);
                    $ins_gp = $this->db->insert_id(); 
                    if($ins_gp) 
                    {
                          $gp_srcData = array(
                        'org_id'=>$stoData['org_id'],
                        'wh_id'=>$stoData['wh_id'][0],
                        'segment_id'=>$stoData['segment_id'],
                        'gp_no'=> $stoData['segment_id'].'/GP/'.(isset($maxId->id)?$mmax:1),
                        'warehouse_id' => $stoData['warehouse_id'],
                        'gp_id' => $ins_gp,
                        'doc_id' => $postData['doc_id'][$key],
                        'doc_no' => $val,
                        'doc_date' => $_POST['date'][$key]
                    );

                    if($this->db->insert('gp_src',$gp_srcData))
                    {
                        $gp_rcpt_src_id = $this->db->insert_id();  
                        //echo "<pre>";print_r($postData['item_id']['undefined']); die;

                        foreach($postData['item_id']['undefined'] as $bill_key=>$bill_val){
                            //echo $_POST['actual_qty']['undefined'][$bill_key];die;
                            
                            if($_POST['actual_qty']['undefined'][$bill_key]>0)
                            {
                            //echo $doc_id123."---".$postData['sto_list123']['undefined'][$bill_key]; die;
                                if($doc_id123 == $postData['sto_list123']['undefined'][$bill_key] )
                                {
                                $gpItemData = array(
                                    'org_id'=>$stoData['org_id'],
                                    'wh_id'=>$stoData['wh_id'][0],
                                    'segment_id'=>$stoData['segment_id'],
                                    'gp_no'=> $stoData['segment_id'].'/GP/'.(isset($maxId->id)?$mmax:1),
                                    'warehouse_id' => $stoData['warehouse_id'],
                                    'gp_src_id' => $gp_rcpt_src_id,
                                    'item_id' => $bill_val,
                                    'code' => $_POST['code']['undefined'][$bill_key],
                                    'item_uom_id' => $_POST['item_uom']['undefined'][$bill_key],
                                    'req_qty' => $_POST['req_qty']['undefined'][$bill_key],
                                    'pending_qty' => $_POST['short_qty']['undefined'][$bill_key],
                                    'dlv_note_qty' => $_POST['bill_qty']['undefined'][$bill_key],
                                    'act_rcpt_qty' => $_POST['actual_qty']['undefined'][$bill_key],
                                    'item_price' => $_POST['rate']['undefined'][$bill_key],
                                    'item_amt' => $_POST['amount']['undefined'][$bill_key]
                                );
                                //echo "<pre>";print_r($gpItemData);die;

                                if($this->db->insert('gp_item',$gpItemData))
                                {
                                    $gp_item_id = $this->db->insert_id();
                                  // echo "hi..."; die;
                                    if($_POST['submit-type'] == 1)
                                    {

                                        $this->db->select('id, item_id, total_stk, available_stk, bal_qty')
                                        ->where('item_id',$_POST['item_id']['undefined'][$bill_key]);
                                        $this->db->limit(1);
                                        $q = $this->db->get('cons_item_stk_profile');

                                        if($q->num_rows() > 0)
                                        {
                                            foreach($q->result() as $res){
                                                $stk_item_id = $res->id;
                                                $stk_total = $res->total_stk;
                                                $stk_avl = $res->available_stk;
                                                //$stk_short = $res->rejected_stk;
                                                $stk_short = $res->bal_qty;
                                            }

                                            $consItemData = array(
                                                'total_stk' => $stk_total+$_POST['actual_qty']['undefined'][$bill_key],
                                                'available_stk' => $stk_avl+$_POST['actual_qty']['undefined'][$bill_key],
                                                'warehouse_id' => $stoData['warehouse_id'],
                                              // 'rejected_stk' => $stk_short - $_POST['actual_qty']['undefined'][$bill_key]
                                                'bal_qty' => $stk_short - $_POST['actual_qty']['undefined'][$bill_key]
                                            );
                                        //echo "If ----- <br> <pre>";print_r($consItemData);exit;
                                            $this->db->where('id',$stk_item_id);
                                            $this->db->update('cons_item_stk_profile',$consItemData);
                                        }
                                        else
                                        {

                                            $short_stk = $_POST['bill_qty']['undefined'][$bill_key] - $_POST['actual_qty']['undefined'][$bill_key];
                                            $consItemData = array(
                                                'fy_id' => 1,
                                                'org_id'=>$stoData['org_id'],
                                                'wh_id'=>$stoData['wh_id'][0],
                                                'segment_id'=>$stoData['segment_id'],
                                                'warehouse_id' => $stoData['warehouse_id'],
                                                'item_id' => $_POST['item_id']['undefined'][$bill_key],
                                                'code' => $_POST['code']['undefined'][$bill_key],
                                                'item_uom_id' => $_POST['item_uom']['undefined'][$bill_key],
                                                'total_stk' => $_POST['actual_qty']['undefined'][$bill_key],
                                                'available_stk' => $_POST['actual_qty']['undefined'][$bill_key],
                                                //'rejected_stk' => $short_stk
                                                'bal_qty' => $short_stk
                                            );
                                        //echo "Else --- <br> <pre>";print_r($consItemData);exit;
                                            $this->db->insert('cons_item_stk_profile',$consItemData);
                                            $stk_item_id = $this->db->insert_id();
                                        }
                                    }

                                    foreach($_POST['lot_data'][$bill_key] as $lot_key=>$lot_val)
                                    {
                                        $lot_data = json_decode($lot_val,true)[0];

                                        $gpLotData = array(
                                            'org_id'=>$stoData['org_id'],
                                            'wh_id'=>$stoData['wh_id'][0],
                                            'segment_id'=>$stoData['segment_id'],
                                            'gp_no'=> $stoData['segment_id'].'/GP/'.(isset($maxId->id)?$mmax:1),
                                            'warehouse_id' => $stoData['warehouse_id'],
                                            'mrn_gp_item_id' => $gp_item_id,
                                            'item_id' => $_POST['item_id']['undefined'][$bill_key],
                                            'code' => $_POST['code']['undefined'][$bill_key],
                                            'item_uom_id' => $_POST['item_uom']['undefined'][$bill_key],
                                            'itm_uom_bs' => $_POST['item_uom']['undefined'][$bill_key],
                                            'lot_no' => $lot_data['lot_no'],
                                            'lot_qty' => $lot_data['lot_qty'],
                                            'batch_no' => $lot_data['batch_no'],
                                            'mfg_dt' => $lot_data['mfg_date'],
                                            'expiry_dt' => $lot_data['exp_date']           
                                        );

                                        if($this->db->insert('gp_item_lot',$gpLotData)){
                                        $lot_id = $this->db->insert_id();

                                        if($_POST['submit-type'] == 1)
                                        {

                                            $consItemLotStk = array(
                                            'org_id'=>$stoData['org_id'],
                                            'wh_id'=>$stoData['wh_id'][0],
                                            'segment_id'=>$stoData['segment_id'],
                                            'warehouse_id' => $stoData['warehouse_id'],
                                            'item_stk_id' => $stk_item_id,
                                            'item_id' => $_POST['item_id']['undefined'][$bill_key],
                                            'code' => $_POST['code']['undefined'][$bill_key],
                                            'item_uom_id' => $_POST['item_uom']['undefined'][$bill_key],
                                            'lot_no' => $lot_data['lot_no'],
                                            'total_stk' => $lot_data['lot_qty'], //$_POST['bill_qty'][$key][$bill_key],
                                            'rejected_stk' => $_POST['short_qty']['undefined'][$bill_key],
                                            'batch_no' => $lot_data['batch_no'],
                                            'mfg_date' => $lot_data['mfg_date'],
                                            'exp_date' => $lot_data['exp_date'],
                                            'created_date' => date("Y-m-d H:i:s")
                                            );  

                                            $this->db->insert('cons_item_stk_lot',$consItemLotStk);
                                        }

                                        foreach($_POST['bin_data'][$lot_data['lot_no']] as $bin_key=>$bin_val)
                                        {
                                            $bin_data = json_decode($bin_val,true)[0];
                                            //echo "<pre>";print_r($bin_data);
                                             $gpBinData = array(
                                                'org_id'=>$stoData['org_id'],
                                                'wh_id'=>$stoData['wh_id'][0],
                                                'segment_id'=>$stoData['segment_id'],
                                                'gp_no'=> $stoData['segment_id'].'/GP/'.(isset($maxId->id)?$mmax:1),
                                                'warehouse_id' => $stoData['warehouse_id'],
                                                'lot_id' => $lot_id,
                                                'lot_no' => $lot_data['lot_no'],
                                                'item_id' => $_POST['item_id']['undefined'][$bill_key],
                                                'code' => $_POST['code']['undefined'][$bill_key],
                                                'item_uom_id' => $_POST['item_uom']['undefined'][$bill_key],
                                                'itm_uom_bs' => $_POST['item_uom']['undefined'][$bill_key],
                                                //'pending_qty' => $_POST['short_qty'][$key][$bill_key],
                                                'bin_qty' => $bin_data['bin_qty'],
                                                'bin_id' => $bin_data['bin_id']        
                                            );                                      

                                            $binId = $this->db->insert('gp_lot_bin',$gpBinData);

                                            if($_POST['submit-type'] == 1)
                                            {

                                            $check_exist_lot = $this->db->select("*")->from("cons_item_stk_lot_bin")->where("lot_id",$lot_id)->where("bin_id",$bin_data['bin_id'])->get()->row();
                                
                                        // echo $check_exist_lot->total_stk; echo "hi..";die;
                                            if(count($check_exist_lot)>0){
                                                $bin_qty = $bin_data['bin_qty']+$check_exist_lot->total_stk;
                                                $this->db->where('lot_id',$lot_id);
                                                $this->db->where('bin_id',$bin_data['bin_id']);
                                                $this->db->update('cons_item_stk_lot_bin',array('total_stk'=>$bin_qty));
                                            } else {
                                                $consItemLotBin = array(
                                                    'org_id'=>$stoData['org_id'],
                                                    'wh_id'=>$stoData['wh_id'][0],
                                                    'segment_id'=>$stoData['segment_id'],
                                                    'warehouse_id' => $stoData['warehouse_id'],
                                                    'item_id' => $_POST['item_id']['undefined'][$bill_key],
                                                    'code' => $_POST['code']['undefined'][$bill_key],
                                                    'item_uom_id' => $_POST['item_uom']['undefined'][$bill_key],
                                                    'lot_id' => $lot_id,
                                                    'lot_no' => $lot_data['lot_no'],
                                                    //'bin_id' => $binId,
                                                    'bin_id' => $bin_data['bin_id'], 
                                                    'total_stk' => $bin_data['bin_qty'], //$_POST['bill_qty'][$key][$bill_key],
                                                    'rejected_stk' => $_POST['short_qty']['undefined'][$bill_key],
                                                    'created_date' => date("Y-m-d H:i:s")
                                                );

                                                $this->db->insert('cons_item_stk_lot_bin',$consItemLotBin);
                                            }

                                            }
                                        }//bin_data loop
                                    }//lot data loop
                                } // gp item lot insert

                                if($_POST['submit-type'] == 1){
                                   // echo "<pre>";print_r($_POST['actual_qty']['undefined']); echo "<br/>";// die;
                                    foreach ($_POST['actual_qty']['undefined'] as $sto=>$sto_ids)
                                    {
                                       //echo "hi...".$_POST['actual_qty']['undefined'][$bill_key]; 
                                      // echo "sto=>".$sto."<br/>"."sto_ids=>".$sto_ids."<br/>"."item_id=>".$_POST['item_id']['undefined'][$bill_key]."<br/>";
                                       
                                      // die;
                                        if($_POST['actual_qty']['undefined'][$bill_key]>0)
                                        {
                                        //$this->updateStock($sto_ids, $_POST['item_id']['undefined'][$bill_key], $_POST['actual_qty']['undefined'][$bill_key], $_POST['submit-type']);
                                        $this->updateStock($sto,$sto_ids, $_POST['item_id']['undefined'][$bill_key], $_POST['submit-type']);
                                        
                                        }
                                    }
                                }
                                else{
                                    foreach ($_POST['sto_id']['undefined'] as $sto=>$sto_ids)
                                    {
                                        $this->updateStock($sto_ids, $_POST['item_id']['undefined'][$bill_key],0, $_POST['submit-type']);
                                    }
                                }
                                $count++;
                                 } // $doc123 if close
                            } // actual qty check
                        
                        }//post item id loop
                    }

                    }                                 
                }  

                } // checkflg if condition

            } // end foreach
            /*else{
                $ins_gp = $gpId;
            }*/ 
            //print_r($gpData); die;
           // print_r($postData['sto_list']); die;
            
            /*if ($ins_gp)
            {
                foreach($postData['sto_no_new'] as $key=>$val)
                {*/
                   // print_r($postData['sto_no_new']); die;
                    // $gp_srcData = array(
                    //     'org_id'=>$stoData['org_id'],
                    //     'wh_id'=>$stoData['wh_id'][0],
                    //     'segment_id'=>$stoData['segment_id'],
                    //     'warehouse_id' => $stoData['warehouse_id'],
                    //     'gp_id' => $ins_gp,
                    //     'doc_id' => $postData['doc_id'][$key],
                    //     'doc_no' => $val,
                    //     'doc_date' => $_POST['date'][$key]
                    // );

                    // if($this->db->insert('gp_src',$gp_srcData))
                    // {
                    //     $gp_rcpt_src_id = $this->db->insert_id();                                           
                    //     foreach($postData['item_id'][$key] as $bill_key=>$bill_val){
                    //         echo "<pre>";print_r($postData['item_id'][$key]);exit;
                            
                    //         if($_POST['actual_qty'][$key][$bill_key]!='')
                    //         {
                    //             $gpItemData = array(
                    //                 'org_id'=>$stoData['org_id'],
                    //                 'wh_id'=>$stoData['wh_id'][0],
                    //                 'segment_id'=>$stoData['segment_id'],
                    //                 'warehouse_id' => $stoData['warehouse_id'],
                    //                 'gp_src_id' => $gp_rcpt_src_id,
                    //                 'item_id' => $bill_val,
                    //                 'code' => $_POST['code'][$key][$bill_key],
                    //                 'item_uom_id' => $_POST['item_uom'][$key][$bill_key],
                    //                 'pending_qty' => $_POST['short_qty'][$key][$bill_key],
                    //                 'dlv_note_qty' => $_POST['bill_qty'][$key][$bill_key],
                    //                 'act_rcpt_qty' => $_POST['actual_qty'][$key][$bill_key],
                    //                 'item_price' => $_POST['rate'][$key][$bill_key],
                    //                 'item_amt' => $_POST['amount'][$key][$bill_key]
                    //             );
                    //             //echo "<pre>";print_r($gpItemData);exit;

                    //             if($this->db->insert('gp_item',$gpItemData))
                    //             {
                    //                 $gp_item_id = $this->db->insert_id();
                                   
                    //                 if($_POST['submit-type'] == 1)
                    //                 {

                    //                     $this->db->select('id, item_id, total_stk, available_stk, rejected_stk')
                    //                     ->where('item_id',$_POST['item_id'][$key][$bill_key]);
                    //                     $this->db->limit(1);
                    //                     $q = $this->db->get('cons_item_stk_profile');

                    //                     if($q->num_rows() > 0)
                    //                     {
                    //                         foreach($q->result() as $res){
                    //                             $stk_item_id = $res->id;
                    //                             $stk_total = $res->total_stk;
                    //                             $stk_avl = $res->available_stk;
                    //                             $stk_short = $res->rejected_stk;
                    //                         }

                    //                         $consItemData = array(
                    //                             'total_stk' => $stk_total+$_POST['actual_qty'][$key][$bill_key],
                    //                             'available_stk' => $stk_avl+$_POST['actual_qty'][$key][$bill_key],
                    //                             'warehouse_id' => $stoData['warehouse_id'],
                    //                             'rejected_stk' => $stk_short - $_POST['actual_qty'][$key][$bill_key]
                    //                         );
                    //                     //echo "If ----- <br> <pre>";print_r($consItemData);exit;
                    //                         $this->db->where('id',$stk_item_id);
                    //                         $this->db->update('cons_item_stk_profile',$consItemData);
                    //                     }
                    //                     else
                    //                     {

                    //                         $short_stk = $_POST['bill_qty'][$key][$bill_key] - $_POST['actual_qty'][$key][$bill_key];
                    //                         $consItemData = array(
                    //                             'fy_id' => 1,
                    //                             'org_id'=>$stoData['org_id'],
                    //                             'wh_id'=>$stoData['wh_id'][0],
                    //                             'segment_id'=>$stoData['segment_id'],
                    //                             'warehouse_id' => $stoData['warehouse_id'],
                    //                             'item_id' => $_POST['item_id'][$key][$bill_key],
                    //                             'code' => $_POST['code'][$key][$bill_key],
                    //                             'item_uom_id' => $_POST['item_uom'][$key][$bill_key],
                    //                             'total_stk' => $_POST['actual_qty'][$key][$bill_key],
                    //                             'available_stk' => $_POST['actual_qty'][$key][$bill_key],
                    //                             'rejected_stk' => $short_stk
                    //                         );
                    //                     //echo "Else --- <br> <pre>";print_r($consItemData);exit;
                    //                         $this->db->insert('cons_item_stk_profile',$consItemData);
                    //                         $stk_item_id = $this->db->insert_id();
                    //                     }
                    //                 }

                    //                 foreach($_POST['lot_data'][$bill_key] as $lot_key=>$lot_val)
                    //                 {
                    //                     $lot_data = json_decode($lot_val,true)[0];

                    //                     $gpLotData = array(
                    //                         'org_id'=>$stoData['org_id'],
                    //                         'wh_id'=>$stoData['wh_id'][0],
                    //                         'segment_id'=>$stoData['segment_id'],
                    //                         'warehouse_id' => $stoData['warehouse_id'],
                    //                         'mrn_gp_item_id' => $gp_item_id,
                    //                         'item_id' => $_POST['item_id'][$key][$bill_key],
                    //                         'code' => $_POST['code'][$key][$bill_key],
                    //                         'item_uom_id' => $_POST['item_uom'][$key][$bill_key],
                    //                         'itm_uom_bs' => $_POST['item_uom'][$key][$bill_key],
                    //                         'lot_no' => $lot_data['lot_no'],
                    //                         'lot_qty' => $lot_data['lot_qty'],
                    //                         'batch_no' => $lot_data['batch_no'],
                    //                         'mfg_dt' => $lot_data['mfg_date'],
                    //                         'expiry_dt' => $lot_data['exp_date']           
                    //                     );

                    //                     if($this->db->insert('gp_item_lot',$gpLotData)){
                    //                     $lot_id = $this->db->insert_id();

                    //                     if($_POST['submit-type'] == 1)
                    //                     {

                    //                         $consItemLotStk = array(
                    //                         'org_id'=>$stoData['org_id'],
                    //                         'wh_id'=>$stoData['wh_id'][0],
                    //                         'segment_id'=>$stoData['segment_id'],
                    //                         'warehouse_id' => $stoData['warehouse_id'],
                    //                         'item_stk_id' => $stk_item_id,
                    //                         'item_id' => $_POST['item_id'][$key][$bill_key],
                    //                         'code' => $_POST['code'][$key][$bill_key],
                    //                         'item_uom_id' => $_POST['item_uom'][$key][$bill_key],
                    //                         'lot_no' => $lot_data['lot_no'],
                    //                         'total_stk' => $lot_data['lot_qty'], //$_POST['bill_qty'][$key][$bill_key],
                    //                         'rejected_stk' => $_POST['short_qty'][$key][$bill_key],
                    //                         'batch_no' => $lot_data['batch_no'],
                    //                         'mfg_date' => $lot_data['mfg_date'],
                    //                         'exp_date' => $lot_data['exp_date'],
                    //                         'created_date' => date("Y-m-d H:i:s")
                    //                         );  

                    //                         $this->db->insert('cons_item_stk_lot',$consItemLotStk);
                    //                     }

                    //                     foreach($_POST['bin_data'][$lot_data['lot_no']] as $bin_key=>$bin_val)
                    //                     {
                    //                         $bin_data = json_decode($bin_val,true)[0];

                    //                         $gpBinData = array(
                    //                             'org_id'=>$stoData['org_id'],
                    //                             'wh_id'=>$stoData['wh_id'][0],
                    //                             'segment_id'=>$stoData['segment_id'],
                    //                             'warehouse_id' => $stoData['warehouse_id'],
                    //                             'lot_id' => $lot_id,
                    //                             'item_id' => $_POST['item_id'][$key][$bill_key],
                    //                             'code' => $_POST['code'][$key][$bill_key],
                    //                             'item_uom_id' => $_POST['item_uom'][$key][$bill_key],
                    //                             'itm_uom_bs' => $_POST['item_uom'][$key][$bill_key],
                    //                             //'pending_qty' => $_POST['short_qty'][$key][$bill_key],
                    //                             'bin_qty' => $bin_data['bin_qty'],
                    //                             'bin_id' => $bin_data['bin_id']        
                    //                         );

                    //                         $binId = $this->db->insert('gp_lot_bin',$gpBinData);

                    //                         if($_POST['submit-type'] == 1)
                    //                         {
                    //                             $consItemLotBin = array(
                    //                                 'org_id'=>$stoData['org_id'],
                    //                                 'wh_id'=>$stoData['wh_id'][0],
                    //                                 'segment_id'=>$stoData['segment_id'],
                    //                                 'warehouse_id' => $stoData['warehouse_id'],
                    //                                 'item_id' => $_POST['item_id'][$key][$bill_key],
                    //                                 'code' => $_POST['code'][$key][$bill_key],
                    //                                 'item_uom_id' => $_POST['item_uom'][$key][$bill_key],
                    //                                 'lot_id' => $lot_id,
                    //                                 'bin_id' => $binId,
                    //                                 'total_stk' => $bin_data['bin_qty'], //$_POST['bill_qty'][$key][$bill_key],
                    //                                 'rejected_stk' => $_POST['short_qty'][$key][$bill_key],
                    //                                 'created_date' => date("Y-m-d H:i:s")
                    //                             );
                    //                             $this->db->insert('cons_item_stk_lot_bin',$consItemLotBin);
                    //                         }
                    //                     }//bin_data loop
                    //                 }//lot data loop
                    //             } // gp item lot insert

                    //             if($_POST['submit-type'] == 1){
                    //                 foreach ($_POST['sto_id'][$key] as $sto=>$sto_ids)
                    //                 {
                    //                     $this->updateStock($sto_ids, $_POST['item_id'][$key][$bill_key], $_POST['actual_qty'][$key][$bill_key], $_POST['submit-type']);
                    //                 }
                    //             }
                    //             else{
                    //                 foreach ($_POST['sto_id'][$key] as $sto=>$sto_ids)
                    //                 {
                    //                     $this->updateStock($sto_ids, $_POST['item_id'][$key][$bill_key],0, $_POST['submit-type']);
                    //                 }
                    //             }
                    //             $count++;
                    //         } // actual qty check
                    //     }//post item id loop
                    // }//insert gp src 
             //   }//sto-list loop
           // }//insert gp                    
            //print_r($gp_srcData); die;
        }//isset post data
        }
        //echo $count;exit;
        if($count>0)
            return 2;
        else
            return 1;
       
    }

    public function updategatepassData($postData, $id){
        //echo "<pre>";print_r($postData);exit;
        if(isset($postData))
        {
            $stoData = $postData;
            //print_r($stoData);
            //echo $stoData['org_id'];exit;
            $count = 0;
                $gpData = array(
                'supplier_id'=> $postData['supplier_id'],
                'tp_id' => $_POST['lr_name'],
                'tpt_lr_no' => $_POST['lr_no'],
                'tpt_lr_dt' => date('Y-m-d',strtotime($_POST['lr_date'])),
                'vehicle_no'=> $_POST['vehicle_no'],
                'addl_amt' => $_POST['other_charges'],
                'user_id' => $_SESSION['user_id'],
                'mod_date' => date('Y-m-d H:i:s')
                );
            
                $this->db->where("id",$id);
                $ins_gp = $this->db->update('gp', $gpData);

            if ($ins_gp)
            {
                $sto_receipt_id = $id;
                
                foreach($postData['gp_item_list'] as $key=>$val)
                {

                    $this->db->where("gp_id",$id);
                    $this->db->where("doc_no",$key);
                    $gpSrcDetails = $this->db->from('gp_src')->get()->row();
                    $gp_src_id = $gpSrcDetails->id;

                    if($gp_src_id != '')
                    {
                        $gp_rcpt_src_id = $gp_src_id;
                        
                        foreach($_POST['gp_bill_qty'][$key] as $bill_key=>$bill_val)
                        {
                            //echo $bill_key."<pre>";print_r($bill_val);echo "</br>";
                            if($_POST['gp_actual_qty'][$key][$bill_key]!='')
                            {
                                $this->db->where("gp_src_id",$gp_rcpt_src_id);
                                $this->db->where("id",$bill_key);
                                
                                $gp_item_details = $this->db->from("gp_item")->get()->row();
                                
                                $gp_item_id = $gp_item_details->id;//exit;
                                
                                $gpItemData = array(
                                    'gp_src_id' => $gp_rcpt_src_id,
                                    'item_id' => $_POST['gp_item_id'][$key][$bill_key],
                                    'item_uom_id' => $_POST['gp_item_uom'][$key][$bill_key],
                                    'pending_qty' => $_POST['gp_short_qty'][$key][$bill_key],
                                    'dlv_note_qty' => $_POST['gp_bill_qty'][$key][$bill_key],
                                    'act_rcpt_qty' => $_POST['gp_actual_qty'][$key][$bill_key],
                                    'item_price' => $_POST['gp_rate'][$key][$bill_key],
                                    'item_amt' => $_POST['gp_amount'][$key][$bill_key]
                                );
                                
                                $this->db->where("id",$gp_item_id);
                                $gp_item_data = $this->db->update('gp_item',$gpItemData);
                            
                            if($gp_item_data)
                            {
                                $gp_item_id = $gp_item_id;//exit;
                                
                                if($_POST['submit-type'] == 1)
                                {
                                    $this->db->select('id, item_id, total_stk, available_stk, rejected_stk')
                                    ->where('item_id',$_POST['gp_item_id'][$key][$bill_key]);
                                    $this->db->limit(1);
                                    $q = $this->db->get('cons_item_stk_profile');

                                    if($q->num_rows() > 0)
                                    {
                                        foreach($q->result() as $res){
                                            $stk_item_id = $res->id;
                                            $stk_total = $res->total_stk;
                                            $stk_avl = $res->available_stk;
                                            $stk_short = $res->rejected_stk;
                                        }

                                        $consItemData = array(
                                            'total_stk' => $stk_total+$_POST['gp_actual_qty'][$key][$bill_key],
                                            'available_stk' => $stk_avl+$_POST['gp_actual_qty'][$key][$bill_key],
                                            'rejected_stk' => $stk_short - $_POST['gp_actual_qty'][$key][$bill_key]
                                        );
                                        $this->db->where('id',$stk_item_id);
                                        $this->db->update('cons_item_stk_profile',$consItemData);
                                    }
                                    else
                                    {
                                        $short_stk = ($_POST['gp_bill_qty'][$key][$bill_key])-($_POST['gp_actual_qty'][$key][$bill_key]);
                                        $consItemData = array(
                                            'org_id'=>$stoData['org_id'],
                                            'segment_id'=>$stoData['segment_id'],
                                            'warehouse_id' => $stoData['warehouse_id'],
                                            'item_id' => $_POST['gp_item_id'][$key][$bill_key],
                                            'item_uom_id' => $_POST['gp_item_uom'][$key][$bill_key],
                                            'total_stk' => $_POST['gp_actual_qty'][$key][$bill_key],
                                            'available_stk' => $_POST['gp_actual_qty'][$key][$bill_key],
                                            'rejected_stk' => $short_stk
                                        );
                                        $this->db->insert('cons_item_stk_profile',$consItemData);
                                        $stk_item_id = $this->db->insert_id();
                                    }
                                }

                                $xyz = 0;
                                if(isset($postData['lot_data']))
                                {
                                    foreach($postData['lot_data'] as $lot_key=>$lotval)
                                    {
                                        $lotdata = json_decode($lotval[$xyz],true);
                                        foreach($lotdata as $lot_data)
                                        {
                                            $gpLotData = array(
                                                'org_id'=>$stoData['org_id'],
                                                'segment_id'=>$stoData['segment_id'],
                                                'warehouse_id' => $stoData['warehouse_id'],
                                                'item_id' => $_POST['gp_item_id'][$key][$bill_key],
                                                'mrn_gp_item_id' => $gp_item_id,
                                                'item_uom_id' => $_POST['gp_item_uom'][$key][$bill_key],
                                                'itm_uom_bs' => $_POST['gp_item_uom'][$key][$bill_key],
                                                'lot_no' => $lot_data['lot_no'],
                                                'lot_qty' => $lot_data['lot_qty'],
                                                'batch_no' => $lot_data['batch_no'],
                                                'mfg_dt' => $lot_data['mfg_date'],
                                                'expiry_dt' => $lot_data['exp_date']
                                            );
                                            $gpLotUpdate = $this->db->insert("gp_item_lot",$gpLotData);
                                            $lot_id =  $this->db->insert_id();
                                            
                                            if($gpLotUpdate){
                                                if($_POST['submit-type'] == 1)
                                                {
                                                    $consItemLotStk = array(
                                                        'org_id'=>$stoData['org_id'],
                                                        'segment_id'=>$stoData['segment_id'],
                                                        'warehouse_id' => $stoData['warehouse_id'],
                                                        'item_stk_id' => $stk_item_id,
                                                        'item_id' => $_POST['gp_item_id'][$key][$bill_key],
                                                        'item_uom_id' => $_POST['gp_item_uom'][$key][$bill_key],
                                                        'lot_no' => $lot_data['lot_no'],
                                                        'total_stk' => $_POST['gp_bill_qty'][$key][$bill_key],
                                                        'rejected_stk' => $_POST['gp_short_qty'][$key][$bill_key],
                                                        'batch_no' => $lot_data['batch_no'],
                                                        'mfg_date' => $lot_data['mfg_date'],
                                                        'exp_date' => $lot_data['exp_date'],
                                                        'created_date' => date("Y-m-d H:i:s")
                                                    );
                                                    $this->db->insert('cons_item_stk_lot',$consItemLotStk);
                                                }
                                                //echo "<pre>";print_r($_POST['bin_data'][$lot_data['lot_no']]);exit;
                                                foreach($_POST['bin_data'][$lot_data['lot_no']] as $binkey=>$binval)
                                                {
                                                    $binData = json_decode($binval,true)[0];
                                                    $gpBinData123 = array(
                                                        'org_id'=>$stoData['org_id'],
                                                        'segment_id'=>$stoData['segment_id'],
                                                        'warehouse_id' => $stoData['warehouse_id'],
                                                        'lot_id' => $lot_id,
                                                        'item_id' => $_POST['gp_item_id'][$key][$bill_key],
                                                        'item_uom_id' => $_POST['gp_item_uom'][$key][$bill_key],
                                                        'itm_uom_bs' => $_POST['gp_item_uom'][$key][$bill_key],
                                                        'bin_qty' => $binData['bin_qty'],
                                                        'bin_id' => $binData['bin_id']        
                                                    );
                                                    $this->db->insert('gp_lot_bin',$gpBinData123);
                                                    $binId =  $this->db->insert_id();

                                                    if($_POST['submit-type'] == 1)
                                                    {
                                                        $consItemLotBin = array(
                                                            'org_id'=>$stoData['org_id'],
                                                            'segment_id'=>$stoData['segment_id'],
                                                            'warehouse_id' => $stoData['warehouse_id'],
                                                           'item_id' => $_POST['gp_item_id'][$key][$bill_key],
                                                            'item_uom_id' => $_POST['gp_item_uom'][$key][$bill_key],
                                                            'lot_id' => $lot_id,
                                                            'bin_id' => $binId,
                                                            'total_stk' => $_POST['gp_bill_qty'][$key][$bill_key],
                                                            'rejected_stk' => $_POST['gp_short_qty'][$key][$bill_key],
                                                            'created_date' => date("Y-m-d H:i:s")
                                                        );

                                                        $this->db->insert('cons_item_stk_lot_bin',$consItemLotBin);
                                                    }

                                                }//post bin_data loop
                                            }//gplotUpdate loop
                                        } //lotdata data
                                        $xyz++;
                                    }//lot-data loop
                                }
                                else
                                { 
                                    //echo "Else <pre>";print_r($bill_key);exit;
                                    $lot_value = $this->db->from('gp_item_lot')
                                    ->where('item_id',$_POST['gp_item_id'][$key][$bill_key])
                                    ->where('mrn_gp_item_id',$bill_key)
                                    ->get()->result();
                                    
                                    $gp_lot_itemid = $_POST['gp_item_id'][$key][$bill_key];
                                    //echo "<pre>";print_r($lot_value);exit;

                                    foreach($lot_value as $lot_key=>$lot_data)
                                    {
                                        $gpLotData123 = array(
                                            'item_id' => $_POST['gp_item_id'][$key][$bill_key],
                                            'mrn_gp_item_id' => $gp_item_id,
                                            'item_uom_id' => $_POST['gp_item_uom'][$key][$bill_key],
                                            'itm_uom_bs' => $_POST['gp_item_uom'][$key][$bill_key],
                                            'lot_no' => $lot_data->lot_no,
                                            'lot_qty' => $lot_data->lot_qty,
                                            'batch_no' => $lot_data->batch_no,
                                            'mfg_dt' => $lot_data->mfg_date,
                                            'expiry_dt' => $lot_data->exp_date
                                        );

                                        $this->db->where("id",$lot_data->id);
                                        $this->db->where("mrn_gp_item_id",$gp_item_id);
                                        $this->db->where("item_id",$gp_lot_itemid);
                                        $gpLotUpdate = $this->db->update("gp_item_lot",$gpLotData123);
                                        $lot_id = $lot_data->id;

                                        if($gpLotUpdate)
                                        {
                                            if($_POST['submit-type'] == 1)
                                            {
                                                $consItemLotStk = array(
                                                    'org_id'=>$stoData['org_id'],
                                                    'segment_id'=>$stoData['segment_id'],
                                                    'warehouse_id' => $stoData['warehouse_id'],
                                                    'item_stk_id' => $stk_item_id,
                                                    'item_id' => $_POST['gp_item_id'][$key][$bill_key],
                                                    'item_uom_id' => $_POST['gp_item_uom'][$key][$bill_key],
                                                    'lot_no' => $lot_data->lot_no,
                                                    'total_stk' => $_POST['gp_bill_qty'][$key][$bill_key],
                                                    'rejected_stk' => $_POST['gp_short_qty'][$key][$bill_key],
                                                    'batch_no' => $lot_data->batch_no,
                                                    //'mfg_date' => $lot_data->mfg_date,
                                                    //'exp_date' => $lot_data->exp_date,
                                                    'created_date' => date("Y-m-d H:i:s")
                                                );
                                                $this->db->insert('cons_item_stk_lot',$consItemLotStk);
                                            }

                                            foreach($_POST['bin_data'][$lot_data->lot_no] as $binkey=>$binval)
                                            {
                                                
                                                $binData = json_decode($binval,true)[0];
                                                //echo "<pre>";print_r($binData);
                                                $gpBinData123 = array(
                                                    'org_id'=>$stoData['org_id'],
                                                    'segment_id'=>$stoData['segment_id'],
                                                    'warehouse_id' => $stoData['warehouse_id'],
                                                    'lot_id' => $lot_id,
                                                    'item_id' => $_POST['gp_item_id'][$key][$bill_key],
                                                    'item_uom_id' => $_POST['gp_item_uom'][$key][$bill_key],
                                                    'itm_uom_bs' => $_POST['gp_item_uom'][$key][$bill_key],
                                                    'bin_qty' => $binData['bin_qty'],
                                                    'bin_id' => $binData['bin_id']        
                                                );

                                                $this->db->insert('gp_lot_bin',$gpBinData123);
                                                $binId =  $this->db->insert_id();

                                                if($_POST['submit-type'] == 1)
                                                {
                                                    $consItemLotBin = array(
                                                        'org_id'=>$stoData['org_id'],
                                                        'segment_id'=>$stoData['segment_id'],
                                                        'warehouse_id' => $stoData['warehouse_id'],
                                                        'item_id' => $_POST['gp_item_id'][$key][$bill_key],
                                                        'item_uom_id' => $_POST['gp_item_uom'][$key][$bill_key],
                                                        'lot_id' => $lot_id,
                                                        'bin_id' => $binId,
                                                        'total_stk' => $_POST['gp_bill_qty'][$key][$bill_key],
                                                        'rejected_stk' => $_POST['gp_short_qty'][$key][$bill_key],
                                                        'created_date' => date("Y-m-d H:i:s")
                                                    );
                                                    $this->db->insert('cons_item_stk_lot_bin',$consItemLotBin);
                                                }
                                            }//post bin data loop
                                        }//gpLotUpdate check loop
                                    }//lot-value loop
                                }//lot-else loop
                            }//gp-item-data
                            //echo "<pre>";print_r($_POST['gp_id_list']);
                            //echo "";priint_r($_POST['gp_item_id'][$key][$bill_key]);exit;
                          //  echo $_POST['submit-type'];

                            if($_POST['submit-type'] == 1){
                                foreach ($_POST['gp_id_list'][$key] as $sto_ids)
                                {
                                    $this->updateStock($sto_ids, $_POST['gp_item_id'][$key][$bill_key], $_POST['gp_actual_qty'][$key][$bill_key], $_POST['submit-type']);
                                }
                            }
                            else{
                                foreach ($_POST['gp_id_list'][$key] as $sto_ids)
                                {
                                    $this->updateStock($sto_ids, $_POST['gp_item_id'][$key][$bill_key], 0, $_POST['submit-type']);
                                }
                            }
                            $count++;
                        }//actual qty check
                    } //bill qty loop
                }//gp-src-id check loop
            }//gp item list loop
        }
    }
    if($count>0)
        return 2;
    else
        return 1;
   
    }

    public function updateStock($sto_id, $qty, $item_id, $status=""){
        //echo $sto_id;exit;
        if($item_id != '' && $sto_id != ''){
            $this->db->select('bal_qty, id, item_id')->where('id',$sto_id)->where('item_id',$item_id);
            $q = $this->db->get('emrs_item');
            //echo "<pre>";print_r($q->result());exit;
            
            if($q->num_rows() > 0){
                foreach($q->result() as $res){
                    //echo (float)$res->bal_qty;
                    $bal = (float)$res->bal_qty - (float)$qty;

                    if($status == '2'){
                        $set_status = "2";
                    }
                    else if($status == '1'){
                        $set_status = "0";
                    }

                    if($bal <= 0){
                        $set_status = "1";
                        $emrs_stat = '1039';
                    } 

                    $this->db->where('id',$res->id);
                    $this->db->where('item_id',$res->item_id);
                    $update = $this->db->update('emrs_item', array('bal_qty'=>$bal,'status'=>$set_status));
                    
                    if($update){
                        return 1;
                    }
                }
            }
            return 0;
        }
    }

    public function setPoFlag($submit_type,$gp_id=0){
        
        if($submit_type==1 && ($gp_id!=0)){
            $status = array('status'=>1);
            $this->db->where('id',$gp_id);
            $this->db->update('gp',$status);
        }
        return true;
    }

    public function deleteGpData($sto_no, $itemId, $gpId){
        if($sto_no = '' && $itemId != '' && $gpId != ''){
            $this->db->where("id",$gpId);
            $this->db->where("doc_no",$sto_no);
            $this->db->where("item_id",$itemId);

            $this->db->delete("gp");

            $this->db->where("gp_id",$gpId);
            $this->db->where("doc_no",$sto_no);
            $this->db->delete("gp_src");
        }
    }


    public function getGatepassByID($id) {
        //echo $id;
        $q = $this->db->get_where('gp', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getGatepassOrderByID($id) {
        //echo $id;
        $q = $this->db->get_where('emrs', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getGPID($id) {
        //echo $id;
        $q = $this->db->get_where('gp', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    
    
    
 // Add By Ankit
    public function  savegatepassOutdata($postData,$stoData){
     // echo 'PostData==>'; print_r($postData); die;
        
        $sr = array(); $lotdata = array();$lotdata1 = array(); $ar = array();
        $q=0; $q1=0; $flag = 0; 
        //print_r($postData['lotdata']);die;
        $roarray= array();
       if(!empty($postData['lotdata']))
        {
          foreach ($postData['lotdata'] as $k => $v) {
                  foreach ($v as $k1 => $v1) {

                  if(!empty($v1))
                  {
                    // echo "key=>".$k."<br>";
                    $roarray[] = $k;
                  }
              }
          }
      }

       if(!empty($postData['lotdata']))
        {
          foreach ($postData['lotdata'] as $key => $value) {
                   $sr[$key] = $value;
                   foreach ($value as $key1 => $value1) {
                           if(!empty($value1)){
                            $sr1[] = json_decode($value1);
                            foreach ($sr1[$q] as $key2 => $value2) {
                                   $ar[$q1][$value2->name] = $value2->value;
                                   
                              }
                              $q++;
                              $q1++;
                          }
                        }
          }
          //print_r($ar); die;
        for ($i=0; $i <= count($ar) ; $i++) { 
            unset($ar[$i]['stock_val']);
        }

       // echo "<br><br>";print_r($ar); die;
          for($j=0;$j<count($ar);$j++)
            { 
                 $aa= array_chunk($ar[$j], 2);
                 for($i=1;$i<count($aa);$i++)
                     {
                         $lotdata[] = array_merge($aa[0],$aa[$i]);
                     }
            }
        }
       // echo "<pre>"; print_r($lotdata); die;
        $a = $this->site->get_warehouse_ids(2);
        if(isset($postData)){
            $gp_rcpt_src_id = array(); $ins_gp="";$ins_gp_edit=""; $ins_itm=array(); $ins_itm22 = array(); $lot_id="";
            $sto_receipt_id_edit = "";
            $i = 0;
            $count = 0;
            foreach($postData['rono'] as $key=>$val){
                //========================
                $rocount = 0;
                if (in_array($val, $roarray)) {

                         $query= "SELECT MAX(id) as ids FROM sma_gp LIMIT 1";
                        $qu = $this->db->query($query);
                      $ap= $qu->result();
                     $maxIdX = $ap[0]->ids;
                        

                //------------------------
                  

            if($postData['submit-sale'] == 1 || $postData['submit-sale'] == 3)
            {
                $date11 = str_replace('-', '/', $_POST['lr_date']);
                //echo $date11; die; 
            $gpData = array(
                'ho_id' => 1,
                'org_id'=>$postData['org_id'][0],
                'segment_id'=>$postData['segment_id'][0],
                'warehouse_id' => $postData['warehouse_name'],
                'fy_id'=>1,
                'gp_type'=>$_POST['gatepass_type'],
                'gp_no'=> $postData['segment_id'][0].'/GPO/'.(isset($maxIdX)?$maxIdX:0),
                'supplier_id'=>$_POST['supplier_id'],
                'doc_no'=> $val,
                'doc_dt'=> $postData['date'][$i],
                'gp_date' => date('Y-m-d'),
                'tp_id' => $_POST['lr_name'],
                'tpt_lr_no' => $_POST['lr_no'],
                'tpt_lr_dt' => $_POST['lr_date'],
                'vehicle_no'=> $_POST['vehicle_no'],
                'status' => 1,
                'addl_amt' => $_POST['other_charges'],
                'remarks' => $_POST['remarks'],
                //'curr_id' => $_POST['curr_id'],
                'user_id' => $_SESSION['user_id'],
                'created_date' => date('Y-m-d H:i:s'),
                'cust_name' => $_POST['sec_cust_name']
            );
           }

           else{
            $gpData = array(
                'ho_id' => 1,
                'org_id'=>$postData['org_id'][0],
                'segment_id'=>$postData['segment_id'][0],
                'warehouse_id' => $postData['warehouse_name'],
                'fy_id'=>1,
                'gp_type'=>$_POST['gatepass_type'],
                'gp_no'=> $postData['segment_id'][0].'/GPO/'.(isset($maxIdX)?$maxIdX:0),
                'supplier_id'=>$_POST['supplier_id'],
                'doc_no'=> $val,
                'doc_dt'=> $postData['date'][$i],
                'gp_date' => date('Y-m-d'),
                'tp_id' => $_POST['lr_name'],
                'tpt_lr_no' => $_POST['lr_no'],
                'tpt_lr_dt' => $_POST['lr_date'],//date('Y-m-d',strtotime($date11)),
                'vehicle_no'=> $_POST['vehicle_no'],
                'status' => 0,
                'addl_amt' => $_POST['other_charges'],
                'remarks' => $_POST['remarks'],
                //'curr_id' => $_POST['curr_id'],
                'user_id' => $_SESSION['user_id'],
                'created_date' => date('Y-m-d H:i:s'),
                'cust_name' => $_POST['sec_cust_name']
            );

           }
          // print_r($gpData); die;
            if($postData['submit-sale'] == 3)
            {   

               if($postData['sto_list']['undefined'][$h]!= 'undefined')
                     {
                        $ins_gp_edit = $this->db->insert('gp', $gpData);
                        $sto_receipt_id_edit = $this->db->insert_id();
                     }
                else{

                    $this->db->where('id', $postData['gpid']);
                    $ins_gp = $this->db->update('gp', $gpData);
                   }     

              }
            else{
              $ins_gp = $this->db->insert('gp', $gpData);
              $sto_receipt_id = $this->db->insert_id();
            }
            //echo $ins_gp; die;
           // die;
            if($postData['submit-sale'] == 1 || $postData['submit-sale'] == 2){
            if ($ins_gp) {
                
                          // $sto_receipt_id = $this->db->insert_id();
                                 $gp_srcData = array(
                                     'org_id' => $postData['org_id'][0],
                                     'warehouse_id' => $postData['warehouse_name'],
                                     'segment_id'=>$postData['segment_id'][0],
                                     'gp_id' => $sto_receipt_id,
                                     'doc_no'=> $val,
                                     'doc_date'=> $postData['date'][$i]
                                 ); 
                                if($this->db->insert('gp_src',$gp_srcData)){
                                $gp_rcpt_src_id[] = $this->db->insert_id();
                               }  
                           
                       }
            }

            if($postData['submit-sale'] == 3){
            if ($ins_gp_edit) {
                
                          
                                 $gp_srcData = array(
                                     'org_id' => $postData['org_id'][0],
                                     'warehouse_id' => $postData['warehouse_name'],
                                     'segment_id'=>$postData['segment_id'][0],
                                     'gp_id' => $sto_receipt_id_edit,
                                     'doc_no'=> $val,
                                     'doc_date'=> $postData['date'][$i]
                                 ); 
                                if($this->db->insert('gp_src',$gp_srcData)){
                                $gp_rcpt_src_id[] = $this->db->insert_id();
                               }  
                           
                       }
            }
          $i++;
          }
           $rocount++;
         }
            // die;
            // print_r($gp_rcpt_src_id); die;
            if($postData['submit-sale'] == 1 || $postData['submit-sale'] == 2){
            if(!empty($gp_rcpt_src_id)){
                               $j =0;
                                foreach($postData['req_qty']['undefined'] as $bill_key=>$bill_val){
                                                $rocount = 0;
                                                   if (in_array($postData['sto_list123']['undefined'][$j], $roarray)) {
                                                 $gpItemData = array(
                                                      'org_id' => $postData['org_id'][0],
                                                      'warehouse_id' => $postData['warehouse_name'], 
                                                       'segment_id'=>$postData['segment_id'][0],
                                                       'gp_src_id' => $this->getsrc_id($postData['sto_list123']['undefined'][$j]),
                                                       'item_id' => $postData['item_id']['undefined'][$bill_key],
                                                       'item_uom_id' => $postData['item_uom'][0],
                                                       'pending_qty' => $postData['short_qty']['undefined'][$bill_key],
                                                       'dlv_note_qty' => $postData['req_qty']['undefined'][$bill_key],
                                                       'act_rcpt_qty' => $postData['actual_qty']['undefined'][$bill_key],
                                                       'item_price' => $postData['rate']['undefined'][$bill_key],
                                                       'item_amt' => $postData['amount']['undefined'][$bill_key]
                                                      );
                                             if(!empty($postData['actual_qty']['undefined'][$bill_key]))
                                             {   
                                            if($this->db->insert('gp_item',$gpItemData)){
                                                $ins_itm[] = $this->db->insert_id();
                                                if($postData['submit-sale'] == 1)
                                                {

                                                $this->update_stk($postData['warehouse_name'],$postData['item_id']['undefined'][$bill_key],$postData['actual_qty']['undefined'][$bill_key]);

                                                $this->update_cons_stk($postData['warehouse_name'],$postData['item_id']['undefined'][$bill_key],$postData['actual_qty']['undefined'][$bill_key],$postData['sto_list123']['undefined'][$j]);
                                                }
                                                             
                                                         }


                                                   }

                                                 $j++;
                                                 }
                                                     $rocount++;
                                            }

                                     }
        }
        if($postData['submit-sale'] == 3)
        {
            $j =0;
                                foreach($postData['req_qty']['undefined'] as $bill_key=>$bill_val){


                                                 $gpItemData = array(
                                                      'org_id' => $postData['org_id'][0],
                                                      'warehouse_id' => $postData['warehouse_name'], 
                                                       'segment_id'=>$postData['segment_id'][0],
                                                       //'gp_src_id' => $this->getsrc_id($postData['sto_list123']['undefined'][$j]),
                                                       'item_id' => $postData['item_id']['undefined'][$bill_key],
                                                       'item_uom_id' => $postData['item_uom'][0],
                                                       'pending_qty' => $postData['short_qty']['undefined'][$bill_key],
                                                       'dlv_note_qty' => $postData['req_qty']['undefined'][$bill_key],
                                                       'act_rcpt_qty' => $postData['actual_qty']['undefined'][$bill_key],
                                                       'item_price' => $postData['rate']['undefined'][$bill_key],
                                                       'item_amt' => $postData['amount']['undefined'][$bill_key]
                                                      );
                                                 //print_r($gpItemData);
                                                 //echo $postData['eid']['undefined'][$j];
                                                 $this->db->where('id',$postData['eid']['undefined'][$j]);
                                                 if($this->db->update('gp_item',$gpItemData)){
                                                $ins_itm22[] = $this->db->insert_id();
                                                //echo "hi==>"; print_r($ins_itm22); die;
//                                                if($postData['submit-sale'] == 1)
//                                                {

                                                $this->update_stk($postData['warehouse_name'],$postData['item_id']['undefined'][$bill_key],$postData['actual_qty']['undefined'][$bill_key]);

                                                $this->update_cons_stk($postData['warehouse_name'],$postData['item_id']['undefined'][$bill_key],$postData['actual_qty']['undefined'][$bill_key],$postData['sto_list_edit123']['undefined'][$j]);
                                               // }
                                                             
                                                         } 
                                                  $j++;
                                                 }
            
        }
              //die;
        if($postData['submit-sale'] == 1 || $postData['submit-sale'] == 2){    
        //print_r($ins_itm);  die; 
             if(!empty($ins_itm))
               {
                  $ldb = $postData['lotdata'];
                 // print_r($ldb); die;
                  //print_r($lotdata); die;
                  if(!empty($ldb)){
                        foreach ($lotdata as $k) {
                                         $rocount = 0;
                                          if (in_array($k[1], $roarray)) {    
                                    //echo $k[0]; die;
                                    if(!empty($k[3])){ 
                                    $dbs = $this->get_lot_info($k[2]);
                                   // print_r($dbs);  die;//echo $dbs[0]->total_stk; die;
                                      $gpItemLotData = array(
                                             'org_id' => $postData['org_id'][0],
                                             'warehouse_id' => $postData['warehouse_name'], 
                                              'segment_id'=>$postData['segment_id'][0],
                                              'mrn_gp_item_id' => $this->get_gp_itm_id($k[0],$k[1]),
                                              'lot_no' => $k[2],
                                              'item_id' => $k[0],
                                               'item_uom_id' => $postData['item_uom'][0],
                                               'lot_qty' => $k[3],
                                               'mfg_dt' => $dbs[0]->mfg_date,
                                               'expiry_dt' => $dbs[0]->exp_date,
                                               'batch_no' => $dbs[0]->batch_no,
                                               'itm_uom_bs' =>$dbs[0]->item_uom_id
                                               // 'pending_qty' => 
                                              );
                                      if(!empty($gpItemLotData['lot_qty']))
                                       {
                                          //$this->db->insert('gp_item_lot',$gpItemLotData);
                                          if($this->db->insert('gp_item_lot',$gpItemLotData))
                                          {
                                            $lot_id = $this->db->insert_id();
                                            if($postData['submit-sale'] == 1)
                                                {
                                                   $this->update_stk_lot($k[2],$k[0],$k[3]);
                                                }
                                            //echo "lot_id=>".$lot_id."<br>";
                                               $gpItemLotBinData = array(
                                               'org_id' => $postData['org_id'][0],
                                               'warehouse_id' => $postData['warehouse_name'],
                                               'segment_id'=>$postData['segment_id'][0],
                                               'lot_id' => $lot_id,
                                               'item_id' => $k[0],
                                               'item_uom_id' => $postData['item_uom'][0],
                                               'bin_id' => $this->get_bin_id($k[2]),
                                               'bin_qty' => $k[3],
                                               'itm_uom_bs' =>$dbs[0]->item_uom_id
                                               // 'pending_qty' => 
                                              );
                                              // print_r($gpItemLotBinData); die;
                                               $this->db->insert('gp_lot_bin',$gpItemLotBinData);
                                               if($postData['submit-sale'] == 1)
                                                {
                                                   $this->update_stk_lot_bin($k[2],$k[0],$k[3]);
                                                }   
                                               $flag++;
                                             }
                                       }

                                     }
                                  }
                                                     $rocount++;

                                    }  
                                    
                                }
                }
        }   
        if($postData['submit-sale'] == 3){  
       // print_r($ins_itm22); die;    
             if(!empty($ins_itm22))
               {
                  $ldb = $postData['lotdata'];
                 // print_r($ldb); die;
                  //print_r($lotdata); echo "<br><br><br>"; die;
                  if(!empty($ldb)){
                        foreach ($lotdata as $k) {
                                    $s = 0;
                                    $dbs = $this->get_lot_info($k[2]);
                                   // print_r($dbs);  echo $dbs[0]->total_stk; die;
                                      $gpItemLotData = array(
//                                             'org_id' => $postData['org_id'][0],
//                                              'warehouse_id' => $a[0]['id'], 
//                                              'segment_id'=>$postData['segment_id'][0],
//                                              //'mrn_gp_item_id' => $this->get_gp_itm_id($k[0],$k[1]),
//                                              'lot_no' => $k[2],
//                                              'item_id' => $k[0],
//                                               'item_uom_id' => $postData['item_uom'][0],
                                               'lot_qty' => $k[3]
//                                               'mfg_dt' => $dbs[0]->mfg_date,
//                                               'expiry_dt' => $dbs[0]->exp_date,
//                                               'batch_no' => $dbs[0]->batch_no,
//                                               'itm_uom_bs' =>$dbs[0]->item_uom_id
                                               // 'pending_qty' => 
                                              );
                                      if(!empty($gpItemLotData['lot_qty']))
                                       {
                                          $this->db->where('mrn_gp_item_id',$postData['eid']['undefined'][$s]);
                                          if($this->db->update('gp_item_lot',$gpItemLotData))
                                          {
                                            $lot_id = $this->db->insert_id();
                                            $this->update_stk_lot($k[2],$k[0],$k[3]);
                                                
                                            //echo "lot_id=>".$lot_id."<br>";
                                               $gpItemLotBinData = array(
//                                               'org_id' => $postData['org_id'][0],
//                                               'warehouse_id' => $a[0]['id'], 
//                                               'segment_id'=>$postData['segment_id'][0],
                                               //'lot_id' => $lot_id,
                                               //'item_id' => $k[0],
                                               //'item_uom_id' => $postData['item_uom'][0],
                                              // 'bin_id' => $this->get_bin_id($k[2]),
                                               'bin_qty' => $k[3]
                                               //'itm_uom_bs' =>$dbs[0]->item_uom_id
                                               // 'pending_qty' => 
                                              );
                                              // print_r($gpItemLotBinData); die;
                                               $this->db->where('lot_id',$lot_id);
                                               $this->db->update('gp_lot_bin',$gpItemLotBinData);
                                               $this->update_stk_lot_bin($k[2],$k[0],$k[3]);
                                                   
                                               $flag++;


                                          }
                                       }
                                       $s++;
                                      }  
                                    
                                }
                }

       }
         if($flag>0)
         {
            return $flag;
         }
         else
         {
            return false;
         }       
    }            
        
        
    }

// Add By Ankit
public function  hold_outgatepass($postData){
     // echo 'PostData==>'; print_r($postData); die;
        
        $sr = array(); $lotdata = array();$lotdata1 = array(); $ar = array();
        $q=0; $q1=0; $flag = 0; $roarray= array(); $aqty = array(); $uncount = 0; $b=0; $ex= array();
        //print_r($postData['lotdata']);die;
        if(!empty($postData['lotdata']))
        {
           foreach ($postData['sto_list']['undefined'] as $k5 => $v5) {
              if($v5 == 'undefined')
              {
                  $uncount++;
              }

            }
          foreach ($postData['lotdata'] as $k => $v) {
                  foreach ($v as $k1 => $v1) {
                     $ex[] = json_decode($v1);
                  if(!empty($v1))
                  {
                    $roarray[] = $k;
                  }
              }
          }
        }
        //print_r($ex); die;
        //foreach ($ex as $newlotdata)
       // echo $uncount; die;
        if($uncount == 0)
        {
            $this->db->where('id', $postData['gpid']);
            $this->db->delete('gp');
            $this->db->where('gp_id', $postData['gpid']);
            $this->db->delete('gp_src');
            
            // $q = "DELETE FROM sma_gp a INNER JOIN  sma_gp_src b ON (a.id = b.gp_id) INNER JOIN sma_gp_item c ON (b.id = c.gp_src_id) INNER JOIN sma_gp_item_lot d ON (d.mrn_gp_item_id = c.id) INNER JOIN sma_gp_lot_bin e ON (e.lot_id = d.id) WHERE a.id = ".$postData['gpid']."";
            // $this->db->query($q);
        }
      // print_r($roarray); die;
      //echo 'hi---><br>'; print_r($ex[0]); die;
//        foreach ($postData['lotdata'] as $key => $value) {
//            print_r($value);echo "<br>";
//        }
        if(!empty($postData['lotdata']))
        {
          foreach ($postData['lotdata'] as $key => $value) {
                   $sr[$key] = $value;
                   foreach ($value as $key1 => $value1) {
                           if(!empty($value1)){
                            $sr1[] = json_decode($value1);
                            foreach ($sr1[$q] as $key2 => $value2) {
                                   $ar[$q1][$value2->name] = $value2->value;
                                   
                              }
                              $q++;
                              $q1++;
                          }
                        }
          }
         // print_r($ar); die;
        for ($i=0; $i <= count($ar) ; $i++) { 
            unset($ar[$i]['stock_val']);
        }

       // echo "<br><br>";print_r($ar); die;
          for($j=0;$j<count($ar);$j++)
            { 
                 $aa= array_chunk($ar[$j], 2);
                 for($i=1;$i<count($aa);$i++)
                     {
                         $lotdata[] = array_merge($aa[0],$aa[$i]);
                     }
            }
        }

        foreach ($postData['actual_qty']['undefined'] as $key => $value) {
           $aqty[]=$value;
        }
       // print_r($lotdata); die;
       
        if(isset($postData)){
            $gp_rcpt_src_id = array(); $ins_gp="";$ins_gp_edit=""; $ins_itm=array(); $ins_itm22 = array(); $lot_id="";
            $sto_receipt_id = ""; $h = 0; $gp_rcpt_src_id = array(); $m=0; $ins_itms= array(); $updtgp12 = ""; $ins_gpp =""; $updtgp = $postData['gpid'];
            $i = 0;
            $count = 0;
            foreach($postData['rno']['undefined'] as $key=>$val){
                     
                    // if (in_array($val, $roarray)) {
                            
                             $query= "SELECT MAX(id) as ids FROM sma_gp LIMIT 1";
                             $qu = $this->db->query($query);
                             $ap= $qu->result();
                             $maxIdX = $ap[0]->ids;
            if($postData['submit-sale'] == 11)
                {
                     $date11 = str_replace('-', '/', $_POST['lr_date']);
                     $gpData = array(
                'ho_id' => 1,
                'org_id'=>$postData['org_id'][0],
                'segment_id'=>$postData['segment_id'][0],
                'warehouse_id' => $postData['warehouse_name'],
                'fy_id'=>1,
                'gp_type'=>$_POST['gatepass_type'],
                'gp_no'=> $postData['segment_id'][0].'/GP/'.(isset($maxIdX)?$maxIdX:0),
                'supplier_id'=>$_POST['supplier_id'],
                'doc_no'=> $val,
                'doc_dt'=> $postData['date'][$i],
                'gp_date' => date('Y-m-d'),
                'tp_id' => $_POST['lr_name'],
                'tpt_lr_no' => $_POST['lr_no'],
                'tpt_lr_dt' => date('Y-m-d',strtotime($date11)),
                'vehicle_no'=> $_POST['vehicle_no'],
                'status' => 1,
                'addl_amt' => $_POST['other_charges'],
                'remarks' => $_POST['remarks'],
                //'curr_id' => $_POST['curr_id'],
                'user_id' => $_SESSION['user_id'],
                'created_date' => date('Y-m-d H:i:s'),
                'cust_name' => $_POST['sec_cust_name']
            );
                     if($postData['sto_list']['undefined'][$h]!= 'undefined')
                     {
                        if(!empty($aqty[$h]))
                           {
                              $this->db->insert('gp', $gpData);
                              $sto_receipt_id = $this->db->insert_id();
                           }

                        
                     }
                     
                    else{
                          if(!empty($aqty[$h]))
                           {
                         // print_r($gpData); die;
                          $this->db->where('id', $postData['gpid']);
                          $ins_gp = $this->db->update('gp', $gpData);
                          $updtgp = $postData['gpid'];
                          }
                    }
                if($sto_receipt_id)
                {
                    
                                $gp_srcData = array(
                                     'org_id' => $postData['org_id'][0],
                                     'warehouse_id' => $postData['warehouse_name'],
                                     'segment_id'=>$postData['segment_id'][0],
                                     'gp_id' => $sto_receipt_id,
                                     'doc_no'=> $val,
                                     'doc_date'=> $postData['date'][$i]
                                 );
                                 if($postData['sto_list']['undefined'][$h]!= 'undefined')
                                   { 
                                      if(!empty($aqty[$h]))
                                         {
                                      if($this->db->insert('gp_src',$gp_srcData)){
                                       $gp_rcpt_src_id[] = $this->db->insert_id();
                                       }
                                    } 
                              }   
                 }

                    

                 }

                 if($postData['submit-sale'] == 22)
                {
                     $date11 = str_replace('-', '/', $_POST['lr_date']);
                     $gpData = array(
                'ho_id' => 1,
                'org_id'=>$postData['org_id'][0],
                'segment_id'=>$postData['segment_id'][0],
                'warehouse_id' => $postData['warehouse_name'],
                'fy_id'=>1,
                'gp_type'=>$_POST['gatepass_type'],
                'gp_no'=> $postData['segment_id'][0].'/GP/'.(isset($maxIdX)?$maxIdX:0),
                'supplier_id'=>$_POST['supplier_id'],
                'doc_no'=> $val,
                'doc_dt'=> $postData['date'][$i],
                'gp_date' => date('Y-m-d'),
                'tp_id' => $_POST['lr_name'],
                'tpt_lr_no' => $_POST['lr_no'],
                'tpt_lr_dt' => date('Y-m-d',strtotime($date11)),
                'vehicle_no'=> $_POST['vehicle_no'],
                'status' => 0,
                'addl_amt' => $_POST['other_charges'],
                'remarks' => $_POST['remarks'],
                //'curr_id' => $_POST['curr_id'],
                'user_id' => $_SESSION['user_id'],
                'created_date' => date('Y-m-d H:i:s'),
                'cust_name' => $_POST['sec_cust_name']
            );
                     if($postData['sto_list']['undefined'][$h]!= 'undefined')
                     {
                        if(!empty($aqty[$h]))
                         {
                          $this->db->insert('gp', $gpData);
                          $sto_receipt_id = $this->db->insert_id();

                        }
                     }
                     else{
                          if(!empty($aqty[$h]))
                           {
                          
                          $this->db->where('id', $postData['gpid']);
                          $ins_gp = $this->db->update('gp', $gpData);
                          $updtgp = $postData['gpid'];
                          }
                    }

                     if($sto_receipt_id)
                        {
                                $gp_srcData = array(
                                     'org_id' => $postData['org_id'][0],
                                     'warehouse_id' => $postData['warehouse_name'],
                                     'segment_id'=>$postData['segment_id'][0],
                                     'gp_id' => $sto_receipt_id,
                                     'doc_no'=> $val,
                                     'doc_date'=> $postData['date'][$i]
                                 ); 
                                if($postData['sto_list']['undefined'][$h]!= 'undefined')
                                   { 
                                    if(!empty($aqty[$h]))
                                   {
                                   if($this->db->insert('gp_src',$gp_srcData)){
                                       $gp_rcpt_src_id[] = $this->db->insert_id();
                                   } 
                                  }
                                }   
                         }
                    
                 }
              // } 

               $h++;
            }
           
         // echo $updtgp;
           //print_r($gp_rcpt_src_id); die;
           if($postData['submit-sale'] == 11) {
           foreach($postData['req_qty']['undefined'] as $bill_key=>$bill_val){
//                                       $gpItemData = array(
//                                                      'org_id' => $postData['org_id'][0],
//                                                      'warehouse_id' => $postData['warehouse_name'], 
//                                                       'segment_id'=>$postData['segment_id'][0],
//                                                       'gp_src_id' => $this->getsrc_id($postData['rno']['undefined'][$m]),
//                                                       'item_id' => $postData['item_id']['undefined'][$bill_key],
//                                                       'item_uom_id' => $postData['item_uom'][0],
//                                                       'pending_qty' => $postData['short_qty']['undefined'][$bill_key],
//                                                       'dlv_note_qty' => $postData['req_qty']['undefined'][$bill_key],
//                                                       'act_rcpt_qty' => $postData['actual_qty']['undefined'][$bill_key],
//                                                       'item_price' => $postData['rate']['undefined'][$bill_key],
//                                                       'item_amt' => $postData['amount']['undefined'][$bill_key]
//                                                      );
                                       // print_r($gpItemData); die;
                                       if(!empty($postData['actual_qty']['undefined'][$bill_key]))
                                             { 

                                               if($postData['sto_list']['undefined'][$m]!= 'undefined')
                                                {
                                                   $gpItemData = array(
                                                      'org_id' => $postData['org_id'][0],
                                                      'warehouse_id' => $postData['warehouse_name'], 
                                                       'segment_id'=>$postData['segment_id'][0],
                                                       'gp_src_id' => $this->getsrc_id($postData['rno']['undefined'][$m]),
                                                       'item_id' => $postData['item_id']['undefined'][$bill_key],
                                                       'item_uom_id' => $postData['item_uom'][0],
                                                       'pending_qty' => $postData['short_qty']['undefined'][$bill_key],
                                                       'dlv_note_qty' => $postData['req_qty']['undefined'][$bill_key],
                                                       'act_rcpt_qty' => $postData['actual_qty']['undefined'][$bill_key],
                                                       'item_price' => $postData['rate']['undefined'][$bill_key],
                                                       'item_amt' => $postData['amount']['undefined'][$bill_key]
                                                      );
                                                     if($this->db->insert('gp_item',$gpItemData)){
                                                              $ins_itm[] = $this->db->insert_id();
                                                              //echo "insert..";
                                                              $this->update_stk($postData['warehouse_name'],$postData['item_id']['undefined'][$bill_key],$postData['actual_qty']['undefined'][$bill_key]);

                                                $this->update_cons_stk($postData['warehouse_name'],$postData['item_id']['undefined'][$bill_key],$postData['actual_qty']['undefined'][$bill_key],$postData['rno']['undefined'][$m]);



                                                      }
                                                }
                                                else{
                                                    $gpItemData = array(
                                                      'org_id' => $postData['org_id'][0],
                                                      'warehouse_id' => $postData['warehouse_name'], 
                                                       'segment_id'=>$postData['segment_id'][0],
                                                      // 'gp_src_id' => $this->getsrc_id($postData['rno']['undefined'][$m]),
                                                       'item_id' => $postData['item_id']['undefined'][$bill_key],
                                                       'item_uom_id' => $postData['item_uom'][0],
                                                       'pending_qty' => $postData['short_qty']['undefined'][$bill_key],
                                                       'dlv_note_qty' => $postData['req_qty']['undefined'][$bill_key],
                                                       'act_rcpt_qty' => $postData['actual_qty']['undefined'][$bill_key],
                                                       'item_price' => $postData['rate']['undefined'][$bill_key],
                                                       'item_amt' => $postData['amount']['undefined'][$bill_key]
                                                      );

                                                    //print_r($gpItemData);
                                                    $itmids = $this->get_item_id($updtgp,$postData['item_id']['undefined'][$bill_key]);
                                                    $this->db->where('id', $itmids);
                                                    $ins_gpp = $this->db->update('gp_item', $gpItemData);

                                                    $this->update_stk($postData['warehouse_name'],$postData['item_id']['undefined'][$bill_key],$postData['actual_qty']['undefined'][$bill_key]);

                                                $this->update_cons_stk($postData['warehouse_name'],$postData['item_id']['undefined'][$bill_key],$postData['actual_qty']['undefined'][$bill_key],$postData['rno']['undefined'][$m]);

                                                }       
                                            }

                                         $m++;
                                        }
                                   }

                  // die;
            if($postData['submit-sale'] == 22) {
           foreach($postData['req_qty']['undefined'] as $bill_key=>$bill_val){
//                                       $gpItemData = array(
//                                                      'org_id' => $postData['org_id'][0],
//                                                      'warehouse_id' => $postData['warehouse_name'], 
//                                                       'segment_id'=>$postData['segment_id'][0],
//                                                       'gp_src_id' => $this->getsrc_id($postData['rno']['undefined'][$m]),
//                                                       'item_id' => $postData['item_id']['undefined'][$bill_key],
//                                                       'item_uom_id' => $postData['item_uom'][0],
//                                                       'pending_qty' => $postData['short_qty']['undefined'][$bill_key],
//                                                       'dlv_note_qty' => $postData['req_qty']['undefined'][$bill_key],
//                                                       'act_rcpt_qty' => $postData['actual_qty']['undefined'][$bill_key],
//                                                       'item_price' => $postData['rate']['undefined'][$bill_key],
//                                                       'item_amt' => $postData['amount']['undefined'][$bill_key]
//                                                      );
                                       // print_r($gpItemData); die;
                                       if(!empty($postData['actual_qty']['undefined'][$bill_key]))
                                             { 

                                               if($postData['sto_list']['undefined'][$m]!= 'undefined')
                                                {  
                                                   $gpItemData = array(
                                                      'org_id' => $postData['org_id'][0],
                                                      'warehouse_id' => $postData['warehouse_name'], 
                                                       'segment_id'=>$postData['segment_id'][0],
                                                       'gp_src_id' => $this->getsrc_id($postData['rno']['undefined'][$m]),
                                                       'item_id' => $postData['item_id']['undefined'][$bill_key],
                                                       'item_uom_id' => $postData['item_uom'][0],
                                                       'pending_qty' => $postData['short_qty']['undefined'][$bill_key],
                                                       'dlv_note_qty' => $postData['req_qty']['undefined'][$bill_key],
                                                       'act_rcpt_qty' => $postData['actual_qty']['undefined'][$bill_key],
                                                       'item_price' => $postData['rate']['undefined'][$bill_key],
                                                       'item_amt' => $postData['amount']['undefined'][$bill_key]
                                                      );
                                                     if($this->db->insert('gp_item',$gpItemData)){
                                                              $ins_itm[] = $this->db->insert_id();
                                                         }
                                                 }
                                                 else{
                                                     $gpItemData = array(
                                                      'org_id' => $postData['org_id'][0],
                                                      'warehouse_id' => $postData['warehouse_name'], 
                                                       'segment_id'=>$postData['segment_id'][0],
                                                       //'gp_src_id' => $this->getsrc_id($postData['rno']['undefined'][$m]),
                                                       'item_id' => $postData['item_id']['undefined'][$bill_key],
                                                       'item_uom_id' => $postData['item_uom'][0],
                                                       'pending_qty' => $postData['short_qty']['undefined'][$bill_key],
                                                       'dlv_note_qty' => $postData['req_qty']['undefined'][$bill_key],
                                                       'act_rcpt_qty' => $postData['actual_qty']['undefined'][$bill_key],
                                                       'item_price' => $postData['rate']['undefined'][$bill_key],
                                                       'item_amt' => $postData['amount']['undefined'][$bill_key]
                                                      );
                                                    $itmids = $this->get_item_id($updtgp,$postData['item_id']['undefined'][$bill_key]);
                                                    $this->db->where('id', $itmids);
                                                    $ins_gpp = $this->db->update('gp_item', $gpItemData);
                                                 }

                                                       
                                            }

                                         $m++;
                                        }
                                   } 
                    if($postData['submit-sale'] == 11) {
                        foreach ($lotdata as $k) {
                            if(!empty($k[3])){
                                     $dbs = $this->get_lot_info($k[2]);
                                     $gp_item_id = $this->get_gp_itm_id($k[0],$k[1]);
                                     $lot_gp_id = $this->lot_table_id($gp_item_id,$k[2]);
                                     // print_r($dbs);  die;//echo $dbs[0]->total_stk; die;
                                      $gpItemLotData = array(
                                             'org_id' => $postData['org_id'][0],
                                             'warehouse_id' => $postData['warehouse_name'], 
                                              'segment_id'=>$postData['segment_id'][0],
                                              'mrn_gp_item_id' => $gp_item_id,
                                              'lot_no' => $k[2],
                                              'item_id' => $k[0],
                                               'item_uom_id' => $postData['item_uom'][0],
                                               'lot_qty' => $k[3],
                                               'mfg_dt' => $dbs[0]->mfg_date,
                                               'expiry_dt' => $dbs[0]->exp_date,
                                               'batch_no' => $dbs[0]->batch_no,
                                               'itm_uom_bs' =>$dbs[0]->item_uom_id
                                               // 'pending_qty' => 
                                              );
                                     // if($postData['sto_list']['undefined'][$b]!= 'undefined'){
                                      
                                      if($lot_gp_id!=0){
                                          
                                          $this->db->where('id', $lot_gp_id);
                                          if($this->db->update('gp_item_lot', $gpItemLotData))
                                            {
                                             $this->update_stk_lot($k[2],$k[0],$k[3]);
                                             $this->update_gp_bin($lot_gp_id,$k[3]);
                                             $this->update_stk_lot_bin($k[2],$k[0],$k[3]);
                                             $flag++;
                                            }
                                          
                                      }
                                      else {
                                             if($this->db->insert('gp_item_lot',$gpItemLotData))
                                              {
                                                $lot_id = $this->db->insert_id();
                                                $this->update_stk_lot($k[2],$k[0],$k[3]);
                                                $gpItemLotBinData = array(
                                               'org_id' => $postData['org_id'][0],
                                               'warehouse_id' => $postData['warehouse_name'],
                                               'segment_id'=>$postData['segment_id'][0],
                                               'lot_id' => $lot_id,
                                               'item_id' => $k[0],
                                               'item_uom_id' => $postData['item_uom'][0],
                                               'bin_id' => $this->get_bin_id($k[2]),
                                               'bin_qty' => $k[3],
                                               'itm_uom_bs' =>$dbs[0]->item_uom_id
                                               // 'pending_qty' => 
                                              );
                                              // print_r($gpItemLotBinData); die;
                                                 $this->db->insert('gp_lot_bin',$gpItemLotBinData);
                                                 $this->update_stk_lot_bin($k[2],$k[0],$k[3]);
                                                 $flag++;
                                            }
                                          
                                          
                                      }
                                    
                                      
                            }
                           $b++; 
                        }
                     }
                          
                 if($postData['submit-sale'] == 22) {
                        foreach ($lotdata as $k) {
                            if(!empty($k[3])){
                                     $dbs = $this->get_lot_info($k[2]);
                                     $gp_item_id = $this->get_gp_itm_id($k[0],$k[1]);
                                     $lot_gp_id = $this->lot_table_id($gp_item_id,$k[2]);
                                     // print_r($dbs);  die;//echo $dbs[0]->total_stk; die;
                                      $gpItemLotData = array(
                                             'org_id' => $postData['org_id'][0],
                                             'warehouse_id' => $postData['warehouse_name'], 
                                              'segment_id'=>$postData['segment_id'][0],
                                              'mrn_gp_item_id' => $gp_item_id,
                                              'lot_no' => $k[2],
                                              'item_id' => $k[0],
                                               'item_uom_id' => $postData['item_uom'][0],
                                               'lot_qty' => $k[3],
                                               'mfg_dt' => $dbs[0]->mfg_date,
                                               'expiry_dt' => $dbs[0]->exp_date,
                                               'batch_no' => $dbs[0]->batch_no,
                                               'itm_uom_bs' =>$dbs[0]->item_uom_id
                                               // 'pending_qty' => 
                                              );
                                     // if($postData['sto_list']['undefined'][$b]!= 'undefined'){
                                      
                                      if($lot_gp_id!=0){
                                          
                                          $this->db->where('id', $lot_gp_id);
                                          if($this->db->update('gp_item_lot', $gpItemLotData))
                                            {
                                             //$this->update_stk_lot($k[2],$k[0],$k[3]);
                                             $this->update_gp_bin($lot_gp_id,$k[3]);
                                             //$this->update_stk_lot_bin($k[2],$k[0],$k[3]);
                                             $flag++;
                                            }
                                          
                                      }
                                      else {
                                             if($this->db->insert('gp_item_lot',$gpItemLotData))
                                              {
                                                $lot_id = $this->db->insert_id();
                                                //$this->update_stk_lot($k[2],$k[0],$k[3]);
                                                $gpItemLotBinData = array(
                                               'org_id' => $postData['org_id'][0],
                                               'warehouse_id' => $postData['warehouse_name'],
                                               'segment_id'=>$postData['segment_id'][0],
                                               'lot_id' => $lot_id,
                                               'item_id' => $k[0],
                                               'item_uom_id' => $postData['item_uom'][0],
                                               'bin_id' => $this->get_bin_id($k[2]),
                                               'bin_qty' => $k[3],
                                               'itm_uom_bs' =>$dbs[0]->item_uom_id
                                               // 'pending_qty' => 
                                              );
                                              // print_r($gpItemLotBinData); die;
                                                 $this->db->insert('gp_lot_bin',$gpItemLotBinData);
                                                 //$this->update_stk_lot_bin($k[2],$k[0],$k[3]);
                                                 $flag++;
                                            }
                                          
                                          
                                      }
                                    
                                      
                            }
                           $b++; 
                        }
                     }

         } 
         
         if($flag>0)
         {
             return TRUE;
             
         }
         else{
             return false;
         }
       
     } 

//AK
    public function save_gatePassOut($postData,$stoData){
      //echo 'PostData==>'; print_r($postData); die;
      $roarray =array();
      foreach ($postData['actual_qty_new'] as $k => $v) {
               
                  foreach ($v as $k1 => $v1) {

                  if(!empty($v1) && $v1!='NaN' && $v1!=0)
                  {
                    // echo "key=>".$k."<br>";
                    $roarray[] = $k;
                  }
                }
            }
      if(isset($postData)){
                 $ins_gp=''; $ins_gpid=''; $gp_src_id = array();$i=0; $j =0; $m=0; $lot_id=''; $mm = 0;
                 $gpnumber=''; $lot_gp_no='';
                 
           // print_r($roarray); die;
                 foreach($postData['rono'] as $key=>$val){
                    if (in_array($val, $roarray)) {
                            $query= "SELECT MAX(id) as ids FROM sma_gp LIMIT 1";
                            $qu = $this->db->query($query);
                            $ap= $qu->result();
                            $maxIdX = $ap[0]->ids+1;
                            $gpnumber = $postData['segment_id'][0].'/GPO/'.(isset($maxIdX)?$maxIdX:1);
           // added by anil
                 $lr_date = $postData['lr_date'];
                    if($lr_date != ''){
                        $lr_date = $postData['lr_date'];
                    }else{
                        $lr_date = NULL;
                    }                 
                 $gpData = array(
                'ho_id' => 1,
                'org_id'=>$postData['org_id'][0],
                'wh_id' => $postData['wh_id'][0],
                'segment_id'=>$postData['segment_id'][0],
                'warehouse_id' => $postData['warehouse_name'],
                'fy_id'=>1,
                'gp_type'=>$_POST['gatepass_type'],
                'gp_no'=> $postData['segment_id'][0].'/GPO/'.(isset($maxIdX)?$maxIdX:1),
                'supplier_id'=>$_POST['supplier_id'],
                'doc_no'=> $val,
                'doc_dt'=> $postData['date'][$i],
                'gp_date' => date('Y-m-d'),
                'tp_id' => $_POST['lr_name'],
                'tpt_lr_no' => $_POST['lr_no'],
                'tpt_lr_dt' => $lr_date,
                'vehicle_no'=> $_POST['vehicle_no'],
                //'status' => 1,
                'addl_amt' => $_POST['other_charges'],
                'rcpt_date' => $_POST['rcpt_date'],
                'remarks' => $_POST['remarks'],
                'curr_id' => 73,//$_POST['curr_id'],
                'user_id' => $_SESSION['user_id'],
                'created_date' => date('Y-m-d H:i:s'),
                'cust_name' => $_POST['sec_cust_name']
            );
                
           // echo "<pre>"; print_r($gpData); die;
            if($postData['submit-sale'] == 1)
                {
                    $gpData['status'] = 1;
                    $ins_gp = $this->db->insert('gp', $gpData);
                    $ins_gpid = $this->db->insert_id();

                }
            else {
                    $gpData['status'] = 0;
                    $ins_gp = $this->db->insert('gp', $gpData);
                    $ins_gpid = $this->db->insert_id();
                 }
            if($postData['submit-sale'] == 1 || $postData['submit-sale'] == 2)
            {
                if ($ins_gp) {
                                $gp_srcData = array(
                                     'org_id' => $postData['org_id'][0],
                                     'wh_id' => $postData['wh_id'][0],
                                     'warehouse_id' => $postData['warehouse_name'],
                                     'segment_id'=>$postData['segment_id'][0],
                                     'gp_id' => $ins_gpid,
                                     'doc_id' => $postData['doc_id'][$mm],
                                     'doc_no'=> $val,
                                     'gp_no' => $gpnumber,
                                     'doc_date'=> $postData['date'][$i]
                                 );
                                 //echo "<pre>"; print_r($gp_srcData); die; 
                                if($this->db->insert('gp_src',$gp_srcData)){
                                      $gp_src_id[] = $this->db->insert_id();
                               }  
                           
                       }
            }
         } // Roarray if condition end

         $i++; $mm++;
        } // rono foreach loop for insert value in GP and GP SRC table


           if($postData['submit-sale'] == 1 || $postData['submit-sale'] == 2){
            
                              foreach($postData['req_qty']['undefined'] as $bill_key=>$bill_val){
                                                if (in_array($postData['sto_list123']['undefined'][$j], $roarray)) {
                                                 $gpItemData = array(
                                                      'org_id' => $postData['org_id'][0],
                                                      'wh_id' => $postData['wh_id'][0],
                                                      'warehouse_id' => $postData['warehouse_name'], 
                                                       'segment_id'=>$postData['segment_id'][0],
                                                       'gp_src_id' => $this->getsrc_id($postData['sto_list123']['undefined'][$j]),
                                                       'item_id' => $postData['item_id']['undefined'][$bill_key],
                                                       //'item_uom_id' => $postData['item_uom'][0],
                                                       'item_uom_id' => $postData['item_uom']['undefined'][$bill_key],
                                                       'code' => $postData['code']['undefined'][$bill_key],
                                                       'req_qty' => $postData['sreq_qty']['undefined'][$bill_key],
                                                       'pending_qty' => $postData['short_qty']['undefined'][$bill_key],
                                                       'dlv_note_qty' => $postData['req_qty']['undefined'][$bill_key],
                                                       'act_rcpt_qty' => $postData['actual_qty']['undefined'][$bill_key],
                                                       'item_price' => $postData['rate']['undefined'][$bill_key],
                                                       'gp_no' => $this->getsrc_gp_no($postData['sto_list123']['undefined'][$j]),
                                                       'item_amt' => $postData['amount']['undefined'][$bill_key]
                                                      );
                                                 //echo "<pre>"; print_r($gpItemData); die;
                                             if(!empty($postData['actual_qty']['undefined'][$bill_key]))
                                             {   
                                            if($this->db->insert('gp_item',$gpItemData)){
                                                $ins_itm[] = $this->db->insert_id();
                                                if($postData['submit-sale'] == 1)
                                                {

                                                $this->update_stk($postData['warehouse_name'],$postData['item_id']['undefined'][$bill_key],$postData['actual_qty']['undefined'][$bill_key]);

                                                $this->update_cons_stk($postData['warehouse_name'],$postData['item_id']['undefined'][$bill_key],$postData['actual_qty']['undefined'][$bill_key],$postData['sto_list123']['undefined'][$j]);
                                                }
                                                $flag++; // Add extra at time of hold return success full
                                                             
                                                         }


                                                   }

                                                 $j++;
                                                 }
                                                     
                                            }

                                     
          }  // end item if condition  

         // if($postData['submit-sale'] == 1 || $postData['submit-sale'] == 2){
          if($postData['submit-sale'] == 1){  
             foreach($postData['actual_qty']['undefined'] as $bill_key=>$bill_val){
                    $gpitemid = $postData['item_id']['undefined'][$bill_key];
                    $rno = $postData['sto_list123']['undefined'][$m];
                    if(!empty($bill_val)){
                                  //echo "id=>".$gpitemid."<br>rno=>".$rno."<br>qty=>".$bill_val; die;
                                   $dbs = $this->get_lot_info_new($postData['warehouse_name'],$gpitemid,$bill_val);
                                   //print_r($dbs); die;
                                   if(!empty($dbs)){
                                        foreach ($dbs as $key) {
                                            $gpItemLotData = array(
                                             'org_id' => $postData['org_id'][0],
                                             'wh_id' => $postData['wh_id'][0],
                                             'warehouse_id' => $postData['warehouse_name'], 
                                              'segment_id'=>$postData['segment_id'][0],
                                              'mrn_gp_item_id' => $this->get_gp_itm_id($gpitemid,$rno),
                                              'lot_no' => $key->lot_no,
                                              'item_id' => $key->item_id,
                                               'item_uom_id' => $key->item_uom_id,//$postData['item_uom'][0],
                                               'code' => $key->code,
                                               'lot_qty' => $key->total_stk,
                                               'mfg_dt' => $key->mfg_date,
                                               'expiry_dt' => $key->exp_date,
                                               'batch_no' => $key->batch_no,
                                               'gp_no' => $this->get_gp_itm_gp_no($gpitemid,$rno),
                                               'itm_uom_bs' =>$key->item_uom_id
                                               );
                                                 if($this->db->insert('gp_item_lot',$gpItemLotData))
                                                 {
                                                    $lot_id = $this->db->insert_id();
                                                    $lot_gp_no = $this->get_lot_gp_number($lot_id);
                                                    if($postData['submit-sale'] == 1)
                                                      {
                                                          $this->update_stk_lot_new($key->lot_no,$gpitemid,$key->total_stk);
                                                       }
                                                          //echo "lot_id=>".$lot_id."<br>";
                                        $bindl = $this->get_lotbin_info_new($key->id,$key->item_id,$key->total_stk);

                                        if(!empty($bindl)){
                                        foreach ($bindl as $k) {
                                                     $gpItemLotBinData = array(
                                                         'org_id' => $postData['org_id'][0],
                                                         'wh_id' => $postData['wh_id'][0],
                                                          'warehouse_id' => $postData['warehouse_name'],
                                                          'segment_id'=>$postData['segment_id'][0],
                                                          'lot_id' => $lot_id,
                                                          'item_id' => $k->item_id,
                                                          'code' => $k->code,
                                                          'item_uom_id' => $k->item_uom_id,//$postData['item_uom'][0],
                                                          'bin_id' => $k->bin_id,
                                                          'bin_qty' => $k->total_stk,
                                                          'gp_no' => $lot_gp_no,
                                                          'lot_no' => $k->lot_no,
                                                          'itm_uom_bs' =>$k->item_uom_id
                                                           // 'pending_qty' => 
                                                         );

                                                    if($this->db->insert('gp_lot_bin',$gpItemLotBinData))
                                                     {
                                                      if($postData['submit-sale'] == 1)
                                                         {
                                                             $this->update_stk_lot_bin_new($k->id,$k->item_id,$k->total_stk);
                                                         }   
                                                      $flag++;
                                                    }

                                                       }
                                                     }  

                                                 }
                                            }
                                         }
                                      }
                                $m++;  
                            }

                
               //echo "endd=>>>";print_r($gpItemLotData); die;

          } // end if statments of lot_bin
        
      
       } // end if statements of postlotdata


        
      // die;
         if($flag>0)
         {
            return $flag;
         }
         else
         {
            return false;
         }       
    } 

// AK --------------------HOLD
public function  save_gatePassOutHold($postData){
   // echo 'PostData==>'; print_r($postData); die;
      if(isset($postData)){
                 $updtgp = $postData['gpid'];
                 $ins_gp=''; $ins_gpid=''; $gp_src_id = array();$roarray =array();$i=0; $j =0; $m=0; $lot_id=''; $z=0;
                 $aqty = array(); $h=0; $mrnid=""; $uncount = 0; $frono = ""; $uc=0; $ar = array(); $b = 0; $counts = "";
                 foreach ($postData['actual_qty']['undefined'] as $key => $value) { 
                           $aqty[]=$value;
                 }
                 foreach ($postData['actual_qty']['undefined'] as $key1 => $value1) {
                          if($value1 == '' && $postData['sto_list']['undefined'][$b] == 'undefined')
                           {
                              $ar[] = $postData['item_id']['undefined'][$key1];
                           }
                           $b++;
                 }
                 foreach ($postData['actual_qty_new'] as $k => $v) {
                     foreach ($v as $k1 => $v1) {
                          if (strpos($k1, 'a') == false)
                             {
                               if(!empty($v1) && $v1!='NaN' && $v1!=0)
                                 {
                                    // echo "key=>".$k."<br>";
                                     $roarray[] = $k;
                                 }
                              }
                         }
                }
                foreach ($postData['sto_list']['undefined'] as $k5 => $v5) {
                    if($v5 == 'undefined')
                    {
                       $uncount++;
                    }

                }
             
              if(!empty($ar))
              {
                $str = implode(",", $ar);
                $q = "DELETE FROM sma_gp_item where gp_src_id = ".$postData['gpid']." AND item_id IN(".$str.")";
                $this->db->query($q);
                $q1 = "SELECT COUNT(gp_src_id) as cn FROM sma_gp_item WHERE gp_src_id = ".$postData['gpid']."";
                $d = $this->db->query($q1);
                $p = $d->result();
                $counts = $p[0]->cn;
                
              }
            
              if($uncount == 0)
              {
                 $this->db->where('id', $postData['gpid']);
                 $this->db->delete('gp');
                 $this->db->where('gp_id', $postData['gpid']);
                 $this->db->delete('gp_src');
                 $this->db->where('gp_src_id', $postData['gpid']);
                 $this->db->delete('gp_item');
                 $uc =1;
              }
              if($counts == 0)
              {
                 $this->db->where('id', $postData['gpid']);
                 $this->db->delete('gp');
                 $this->db->where('gp_id', $postData['gpid']);
                 $this->db->delete('gp_src');
                 $uc =1;
              }
                 
              if($uc == 1)
              {
                   $frono = $postData['rono'];
              }
              else{
                     $frono = array_slice($postData['rono'], 1);
              }
               
           // print_r($ar); die;
           // print_r($frono); die;
           // print_r($roarray); die;
               foreach($frono as $key=>$val){
                            if (in_array($val, $roarray)) {
                            $query= "SELECT MAX(id) as ids FROM sma_gp LIMIT 1";
                            $qu = $this->db->query($query);
                            $ap= $qu->result();
                            $maxIdX = $ap[0]->ids;
                 $gpDataro = $gpData = array(
                'ho_id' => 1,
                'org_id'=>$postData['org_id'][0],
                'segment_id'=>$postData['segment_id'][0],
                'warehouse_id' => $postData['warehouse_name'],
                'fy_id'=>1,
                'gp_type'=>$_POST['gatepass_type'],
                'gp_no'=> $postData['segment_id'][0].'/GPO/'.(isset($maxIdX)?$maxIdX:0),
                'supplier_id'=>$_POST['supplier_id'],
                //'doc_no'=> $val,
                //'doc_dt'=> $postData['date'][$key],
                'gp_date' => date('Y-m-d'),
                'tp_id' => $_POST['lr_name'],
                'tpt_lr_no' => $_POST['lr_no'],
                'tpt_lr_dt' => $_POST['lr_date'],
                'vehicle_no'=> $_POST['vehicle_no'],
                //'status' => 1,
                'addl_amt' => $_POST['other_charges'],
                'remarks' => $_POST['remarks'],
                //'curr_id' => $_POST['curr_id'],
                'user_id' => $_SESSION['user_id'],
                'created_date' => date('Y-m-d H:i:s'),
                'cust_name' => $_POST['sec_cust_name']
            );

             if($postData['submit-sale'] == 11)
                {
                    //-----------update hold RO
                    if($uc!= 1){
                    $gpDataro = array('status' => 1, 'supplier_id'=>$_POST['supplier_id'], 'gp_date' => date('Y-m-d'),'tp_id' => $_POST['lr_name'],'tpt_lr_no' => $_POST['lr_no'],'tpt_lr_dt' => $_POST['lr_date'],'vehicle_no'=> $_POST['vehicle_no'],'addl_amt' => $_POST['other_charges'],'remarks' => $_POST['remarks'],'cust_name' => $_POST['sec_cust_name']);
                    $this->db->where('id', $postData['gpid']);
                    $ins_gp1 = $this->db->update('gp', $gpDataro);
                      }
                    $updtgp = $postData['gpid'];
                    //----- Insert RO
                    $gpData['status'] = 1;
                    $gpData['doc_no'] = $val;
                    $gpData['doc_dt'] = $postData['date'][$key];
                    $ins_gp = $this->db->insert('gp', $gpData);
                    $ins_gpid = $this->db->insert_id();
                    

                }
            else {
                    // $gpData['status'] = 0;
                    // $ins_gp = $this->db->insert('gp', $gpData);
                    // $ins_gpid = $this->db->insert_id();
                   //-----------update hold RO
                    if($uc!= 1){
                    $gpDataro = array('status' => 0, 'supplier_id'=>$_POST['supplier_id'], 'gp_date' => date('Y-m-d'),'tp_id' => $_POST['lr_name'],'tpt_lr_no' => $_POST['lr_no'],'tpt_lr_dt' => $_POST['lr_date'],'vehicle_no'=> $_POST['vehicle_no'],'addl_amt' => $_POST['other_charges'],'remarks' => $_POST['remarks'],'cust_name' => $_POST['sec_cust_name']);
                    $this->db->where('id', $postData['gpid']);
                    $ins_gp1 = $this->db->update('gp', $gpDataro);
                      }
                    $updtgp = $postData['gpid'];
                    //----- Insert RO
                    $gpData['status'] = 0;
                    $gpData['doc_no'] = $val;
                    $gpData['doc_dt'] = $postData['date'][$key];
                    $ins_gp = $this->db->insert('gp', $gpData);
                    $ins_gpid = $this->db->insert_id();



                 }
            if($postData['submit-sale'] == 11 || $postData['submit-sale'] == 22)
            {
                if ($ins_gp) {
                                $gp_srcData = array(
                                     'org_id' => $postData['org_id'][0],
                                     'warehouse_id' => $postData['warehouse_name'],
                                     'segment_id'=>$postData['segment_id'][0],
                                     'gp_id' => $ins_gpid,
                                     'doc_no'=> $val,
                                     'doc_date'=> $postData['date'][$key]
                                 ); 
                                if($this->db->insert('gp_src',$gp_srcData)){
                                      $gp_src_id[] = $this->db->insert_id();
                               }  
                           
                       }
             }    
            
            } // Roarray if condition end 
       
         $i++; 
        } // rno foreach loop for insert value in GP and GP SRC table

        //die;
           if($postData['submit-sale'] == 11){
            
                              foreach($postData['req_qty']['undefined'] as $bill_key=>$bill_val){
                                               $gpItemData = array(
                                                      'org_id' => $postData['org_id'][0],
                                                      'warehouse_id' => $postData['warehouse_name'], 
                                                       'segment_id'=>$postData['segment_id'][0],
                                                       'item_id' => $postData['item_id']['undefined'][$bill_key],
                                                       'item_uom_id' => $postData['item_uom'][0],
                                                       'pending_qty' => $postData['short_qty']['undefined'][$bill_key],
                                                       'dlv_note_qty' => $postData['req_qty']['undefined'][$bill_key],
                                                       'act_rcpt_qty' => $postData['actual_qty']['undefined'][$bill_key],
                                                       'item_price' => $postData['rate']['undefined'][$bill_key],
                                                       'item_amt' => $postData['amount']['undefined'][$bill_key]
                                                      );
                                                 if(!empty($postData['actual_qty']['undefined'][$bill_key]))
                                                   { 

                                                     if($postData['sto_list']['undefined'][$j]!= 'undefined')
                                                      {
                                                        $gpItemData['gp_src_id'] = $this->getsrc_id($postData['rno']['undefined'][$j]);
                                                        if($this->db->insert('gp_item',$gpItemData)){
                                                              $ins_itm[] = $this->db->insert_id();
                                                              //echo "insert..";
                                                              $this->update_stk($postData['warehouse_name'],$postData['item_id']['undefined'][$bill_key],$postData['actual_qty']['undefined'][$bill_key]);

                                                $this->update_cons_stk($postData['warehouse_name'],$postData['item_id']['undefined'][$bill_key],$postData['actual_qty']['undefined'][$bill_key],$postData['rno']['undefined'][$j]);
                                                       }
                                                    } //sto_list if then insert item and update
                                                    else{
                                                          //print_r($gpItemData);
                                                    $itmids = $this->get_item_id($updtgp,$postData['item_id']['undefined'][$bill_key]);
                                                    $this->db->where('id', $itmids);
                                                    $ins_gpp = $this->db->update('gp_item', $gpItemData);

                                                    $this->update_stk($postData['warehouse_name'],$postData['item_id']['undefined'][$bill_key],$postData['actual_qty']['undefined'][$bill_key]);

                                                $this->update_cons_stk($postData['warehouse_name'],$postData['item_id']['undefined'][$bill_key],$postData['actual_qty']['undefined'][$bill_key],$postData['rno']['undefined'][$j]);

                                                      }
                                                   $flag++;   
                                                 } // Actual qty if
                                            $j++;
                                        }
                               }  // end item - 11 if condition  
                           // die;  

         if($postData['submit-sale'] == 22){
                                  foreach($postData['req_qty']['undefined'] as $bill_key=>$bill_val){
                                               $gpItemData = array(
                                                      'org_id' => $postData['org_id'][0],
                                                      'warehouse_id' => $postData['warehouse_name'], 
                                                       'segment_id'=>$postData['segment_id'][0],
                                                       'item_id' => $postData['item_id']['undefined'][$bill_key],
                                                       'item_uom_id' => $postData['item_uom'][0],
                                                       'pending_qty' => $postData['short_qty']['undefined'][$bill_key],
                                                       'dlv_note_qty' => $postData['req_qty']['undefined'][$bill_key],
                                                       'act_rcpt_qty' => $postData['actual_qty']['undefined'][$bill_key],
                                                       'item_price' => $postData['rate']['undefined'][$bill_key],
                                                       'item_amt' => $postData['amount']['undefined'][$bill_key]
                                                      );
                                                 if(!empty($postData['actual_qty']['undefined'][$bill_key]))
                                                   { 

                                                     if($postData['sto_list']['undefined'][$j]!= 'undefined')
                                                      {
                                                        $gpItemData['gp_src_id'] = $this->getsrc_id($postData['rno']['undefined'][$j]);
                                                        if($this->db->insert('gp_item',$gpItemData)){
                                                              $ins_itm[] = $this->db->insert_id();
                                                              //echo "insert..";
                                                              }
                                                       } //sto_list if then insert item and update
                                                    else{
                                                          //print_r($gpItemData);
                                                    $itmids = $this->get_item_id($updtgp,$postData['item_id']['undefined'][$bill_key]);
                                                    $this->db->where('id', $itmids);
                                                    $ins_gpp = $this->db->update('gp_item', $gpItemData);
                                                     }
                                                   $flag++;
                                                 } // Actual qty if
                                            $j++;
                                        }
                               }  // end item - 22 if condition
               //die;
//---------------------------------------------------------------------

         if($postData['submit-sale'] == 11){  
             foreach($postData['actual_qty']['undefined'] as $bill_key=>$bill_val){
                    $gpitemid = $postData['item_id']['undefined'][$bill_key];
                    $rno = $postData['rno']['undefined'][$z];
                    if(!empty($bill_val)){
                                  //echo "id=>".$gpitemid."<br>qty=>".$bill_val; die;
                                   $dbs = $this->get_lot_info_new($postData['warehouse_name'],$gpitemid,$bill_val);
                                   //print_r($dbs); die;
                                   if(!empty($dbs)){
                                        foreach ($dbs as $key) {
                                            $gpItemLotData = array(
                                             'org_id' => $postData['org_id'][0],
                                             'warehouse_id' => $postData['warehouse_name'], 
                                              'segment_id'=>$postData['segment_id'][0],
                                              'mrn_gp_item_id' => $this->get_gp_itm_id($gpitemid,$rno),
                                              'lot_no' => $key->lot_no,
                                              'item_id' => $key->item_id,
                                               'item_uom_id' => $postData['item_uom'][0],
                                               'lot_qty' => $key->total_stk,
                                               'mfg_dt' => $key->mfg_date,
                                               'expiry_dt' => $key->exp_date,
                                               'batch_no' => $key->batch_no,
                                               'itm_uom_bs' =>$key->item_uom_id
                                               );
                                                 if($this->db->insert('gp_item_lot',$gpItemLotData))
                                                 {
                                                    $lot_id = $this->db->insert_id();
                                                    if($postData['submit-sale'] == 11)
                                                      {
                                                          $this->update_stk_lot_new($key->lot_no,$gpitemid,$key->total_stk);
                                                       }
                                                          //echo "lot_id=>".$lot_id."<br>";
                                        $bindl = $this->get_lotbin_info_new($key->id,$key->item_id,$key->total_stk);

                                        if(!empty($bindl)){
                                        foreach ($bindl as $k) {
                                                     $gpItemLotBinData = array(
                                                         'org_id' => $postData['org_id'][0],
                                                          'warehouse_id' => $postData['warehouse_name'],
                                                          'segment_id'=>$postData['segment_id'][0],
                                                          'lot_id' => $lot_id,
                                                          'item_id' => $k->item_id,
                                                          'item_uom_id' => $postData['item_uom'][0],
                                                          'bin_id' => $k->bin_id,
                                                          'bin_qty' => $k->total_stk,
                                                          'itm_uom_bs' =>$k->item_uom_id
                                                           // 'pending_qty' => 
                                                         );

                                                    if($this->db->insert('gp_lot_bin',$gpItemLotBinData))
                                                     {
                                                      if($postData['submit-sale'] == 11)
                                                         {
                                                             $this->update_stk_lot_bin_new($k->id,$k->item_id,$k->total_stk);
                                                         }   
                                                      $flag++;
                                                    }

                                                       }
                                                     }  

                                                 }
                                            }
                                         }
                                      }
                                $z++;  
                            }

                
               //echo "endd=>>>";print_r($gpItemLotData); die;

          } // end if statments of lot_bin





//---------------------------------------------------------------------                              
//        if($postData['submit-sale'] == 11){
//              foreach($postData['actual_qty']['undefined'] as $bill_key=>$bill_val){
//                     $gpitemid = $postData['item_id']['undefined'][$bill_key];
//                     $rno = $postData['rno']['undefined'][$z];
//                     if(!empty($bill_val)){
//                                   //echo "id=>".$gpitemid."<br>qty=>".$bill_val."<br>RoNO=>".$rno; die;
//                                    $dbs = $this->get_lot_info_new($postData['warehouse_name'],$gpitemid,$bill_val);
//                                    //print_r($dbs); die;
//                                    if(!empty($dbs)){
//                                         foreach ($dbs as $key) {
//                                             $gpItemLotData = array(
//                                              'org_id' => $postData['org_id'][0],
//                                              'warehouse_id' => $postData['warehouse_name'], 
//                                               'segment_id'=>$postData['segment_id'][0],
//                                               //'mrn_gp_item_id' => $this->get_gp_itm_id($gpitemid,$rno),
//                                               'lot_no' => $key->lot_no,
//                                               'item_id' => $key->item_id,
//                                                'item_uom_id' => $postData['item_uom'][0],
//                                                'lot_qty' => $key->total_stk,
//                                                'mfg_dt' => $key->mfg_date,
//                                                'expiry_dt' => $key->exp_date,
//                                                'batch_no' => $key->batch_no,
//                                                'itm_uom_bs' =>$key->item_uom_id
//                                                );
//                                            // echo $updtgp; die;
//                                             //print_r($gpItemLotData); die;
//                                             if($postData['sto_list']['undefined'][$z]!= 'undefined')
//                                                 { //echo "undifined condition";
//                                                 $gpItemLotData['mrn_gp_item_id'] = $this->get_gp_itm_id($gpitemid,$rno);
//                                                  if($this->db->insert('gp_item_lot',$gpItemLotData))
//                                                  {
//                                                     $lot_id = $this->db->insert_id();
//                                                     if($postData['submit-sale'] == 11)
//                                                       {
//                                                           $this->update_stk_lot_new($key->lot_no,$gpitemid,$key->total_stk);
//                                                        }
//                                                           //echo "lot_id=>".$lot_id."<br>";
//                                         $bindl = $this->get_lotbin_info_new($key->id,$key->item_id,$key->total_stk);

//                                         if(!empty($bindl)){
//                                         foreach ($bindl as $k) {
//                                                      $gpItemLotBinData = array(
//                                                          'org_id' => $postData['org_id'][0],
//                                                           'warehouse_id' => $postData['warehouse_name'],
//                                                           'segment_id'=>$postData['segment_id'][0],
//                                                           'lot_id' => $lot_id,
//                                                           'item_id' => $k->item_id,
//                                                           'item_uom_id' => $postData['item_uom'][0],
//                                                           'bin_id' => $k->bin_id,
//                                                           'bin_qty' => $k->total_stk,
//                                                           'itm_uom_bs' =>$k->item_uom_id
//                                                            // 'pending_qty' => 
//                                                          );

//                                                     if($this->db->insert('gp_lot_bin',$gpItemLotBinData))
//                                                      {
//                                                       if($postData['submit-sale'] == 11)
//                                                          {
//                                                              $this->update_stk_lot_bin_new($k->id,$k->item_id,$k->total_stk);
//                                                          }   
//                                                       $flag++;
//                                                     }

//                                                        }
//                                                      }  

//                                                  }

//                                                } // end undifine if condition 

//                                                else{ 
//                                                 $mrnid = $this->lot_table_id_new($updtgp,$rno,$gpitemid,$key->lot_no);
//                                                  //echo $mrnid; die;
//                                                 $this->db->where('id', $mrnid);
//                                                 if($this->db->update('gp_item_lot', $gpItemLotData))
//                                                  {
//                                                     //$lot_id = $this->db->insert_id();
//                                                     if($postData['submit-sale'] == 11)
//                                                       {
//                                                           $this->update_stk_lot_new($key->lot_no,$gpitemid,$key->total_stk);
//                                                        }
//                                                           //echo "lot_id=>".$lot_id."<br>";
//                                              $bindl = $this->get_lotbin_info_new($key->id,$key->item_id,$key->total_stk);

//                                         if(!empty($bindl)){
//                                         foreach ($bindl as $k) {
//                                                      $gpItemLotBinData = array(
//                                                          'org_id' => $postData['org_id'][0],
//                                                           'warehouse_id' => $postData['warehouse_name'],
//                                                           'segment_id'=>$postData['segment_id'][0],
//                                                           'lot_id' => $mrnid,//$lot_id,
//                                                           'item_id' => $k->item_id,
//                                                           'item_uom_id' => $postData['item_uom'][0],
//                                                           'bin_id' => $k->bin_id,
//                                                           'bin_qty' => $k->total_stk,
//                                                           'itm_uom_bs' =>$k->item_uom_id
//                                                            // 'pending_qty' => 
//                                                          );
//                                                     //print_r($gpItemLotBinData); echo "<br>"; 
//                                                     $this->db->where('lot_id', $mrnid);
//                                                     $this->db->delete('gp_lot_bin');
//                                                     if($this->db->insert('gp_lot_bin',$gpItemLotBinData))
//                                                      {
//                                                       if($postData['submit-sale'] == 11)
//                                                          {
//                                                              $this->update_stk_lot_bin_new($k->id,$k->item_id,$k->total_stk);
//                                                          }   
//                                                       $flag++;
//                                                     }

//                                                        }
//                                                      }  

//                                                  }

//                                                } // end else condition of undifined
                                              


//                                             } //end dbs loop
//                                          }
//                                       }
//                                 $z++;  
//                             }

                
//                //echo "endd=>>>";print_r($gpItemLotData); die;

//           } // end if statments of lot_bin submit 11


// if($postData['submit-sale'] == 22){
//              foreach($postData['actual_qty']['undefined'] as $bill_key=>$bill_val){
//                     $gpitemid = $postData['item_id']['undefined'][$bill_key];
//                     $rno = $postData['rno']['undefined'][$z];
//                     if(!empty($bill_val)){
//                                   //echo "id=>".$gpitemid."<br>qty=>".$bill_val."<br>RoNO=>".$rno; die;
//                                    $dbs = $this->get_lot_info_new($postData['warehouse_name'],$gpitemid,$bill_val);
//                                    //print_r($dbs); die;
//                                    if(!empty($dbs)){
//                                         foreach ($dbs as $key) {
//                                             $gpItemLotData = array(
//                                              'org_id' => $postData['org_id'][0],
//                                              'warehouse_id' => $postData['warehouse_name'], 
//                                               'segment_id'=>$postData['segment_id'][0],
//                                               //'mrn_gp_item_id' => $this->get_gp_itm_id($gpitemid,$rno),
//                                               'lot_no' => $key->lot_no,
//                                               'item_id' => $key->item_id,
//                                                'item_uom_id' => $postData['item_uom'][0],
//                                                'lot_qty' => $key->total_stk,
//                                                'mfg_dt' => $key->mfg_date,
//                                                'expiry_dt' => $key->exp_date,
//                                                'batch_no' => $key->batch_no,
//                                                'itm_uom_bs' =>$key->item_uom_id
//                                                );
//                                            // echo $updtgp; die;
//                                             //print_r($gpItemLotData); die;
//                                             if($postData['sto_list']['undefined'][$z]!= 'undefined')
//                                                 { //echo "undifined condition";
//                                                 $gpItemLotData['mrn_gp_item_id'] = $this->get_gp_itm_id($gpitemid,$rno);
//                                                  if($this->db->insert('gp_item_lot',$gpItemLotData))
//                                                  {
//                                                     $lot_id = $this->db->insert_id();
//                                                     if($postData['submit-sale'] == 1122)
//                                                       {
//                                                     //$this->update_stk_lot_new($key->lot_no,$gpitemid,$key->total_stk);
//                                                        }
//                                                           //echo "lot_id=>".$lot_id."<br>";
//                                         $bindl = $this->get_lotbin_info_new($key->id,$key->item_id,$key->total_stk);

//                                         if(!empty($bindl)){
//                                         foreach ($bindl as $k) {
//                                                      $gpItemLotBinData = array(
//                                                          'org_id' => $postData['org_id'][0],
//                                                           'warehouse_id' => $postData['warehouse_name'],
//                                                           'segment_id'=>$postData['segment_id'][0],
//                                                           'lot_id' => $lot_id,
//                                                           'item_id' => $k->item_id,
//                                                           'item_uom_id' => $postData['item_uom'][0],
//                                                           'bin_id' => $k->bin_id,
//                                                           'bin_qty' => $k->total_stk,
//                                                           'itm_uom_bs' =>$k->item_uom_id
//                                                            // 'pending_qty' => 
//                                                          );

//                                                     if($this->db->insert('gp_lot_bin',$gpItemLotBinData))
//                                                      {
//                                                       if($postData['submit-sale'] == 1122)
//                                                          {
//                                                         //$this->update_stk_lot_bin_new($k->id,$k->item_id,$k->total_stk);
//                                                          }   
//                                                       $flag++;
//                                                     }

//                                                        }
//                                                      }  

//                                                  }

//                                                } // end undifine if condition 

//                                                else{ 
//                                                 $mrnid = $this->lot_table_id_new($updtgp,$rno,$gpitemid,$key->lot_no);
//                                                  //echo $mrnid; die;
//                                                 $this->db->where('id', $mrnid);
//                                                 if($this->db->update('gp_item_lot', $gpItemLotData))
//                                                  {
//                                                     //$lot_id = $this->db->insert_id();
//                                                     if($postData['submit-sale'] == 1122)
//                                                       {
//                                                     //$this->update_stk_lot_new($key->lot_no,$gpitemid,$key->total_stk);
//                                                        }
//                                                           //echo "lot_id=>".$lot_id."<br>";
//                                              $bindl = $this->get_lotbin_info_new($key->id,$key->item_id,$key->total_stk);

//                                         if(!empty($bindl)){
//                                         foreach ($bindl as $k) {
//                                                      $gpItemLotBinData = array(
//                                                          'org_id' => $postData['org_id'][0],
//                                                           'warehouse_id' => $postData['warehouse_name'],
//                                                           'segment_id'=>$postData['segment_id'][0],
//                                                           'lot_id' => $mrnid,//$lot_id,
//                                                           'item_id' => $k->item_id,
//                                                           'item_uom_id' => $postData['item_uom'][0],
//                                                           'bin_id' => $k->bin_id,
//                                                           'bin_qty' => $k->total_stk,
//                                                           'itm_uom_bs' =>$k->item_uom_id
//                                                            // 'pending_qty' => 
//                                                          );
//                                                     //print_r($gpItemLotBinData); echo "<br>"; 
//                                                     $this->db->where('lot_id', $mrnid);
//                                                     $this->db->delete('gp_lot_bin');
//                                                     if($this->db->insert('gp_lot_bin',$gpItemLotBinData))
//                                                      {
//                                                       if($postData['submit-sale'] == 1122)
//                                                          {
//                                                         //$this->update_stk_lot_bin_new($k->id,$k->item_id,$k->total_stk);
//                                                          }   
//                                                       $flag++;
//                                                     }

//                                                        }
//                                                      }  

//                                                  }

//                                                } // end else condition of undifined
                                              


//                                             } //end dbs loop
//                                          }
//                                       }
//                                 $z++;  
//                             }

                
//                //echo "endd=>>>";print_r($gpItemLotData); die;

//           } // end if statments of lot_bin Hold 22
     } // end if statements of postlotdata
     

      // die;
         if($flag>0)
         {
            return $flag;
         }
         else
         {
            return false;
         }       
    }        

    


  public function getsrc_id($rono)
    {
       // echo "rono=>".$rono."<br>";
        $this->db->select('id');
        $this->db->order_by("id", "desc");
        $q = $this->db->get_where('gp_src',array('doc_no' => $rono),1);
        $ar = $q->result();
        //echo "id=>".$ar[0]->id."<br>";
        return $ar[0]->id;
    }

    public function getsrc_gp_no($rono)
    {
       // echo "rono=>".$rono."<br>";
        $this->db->select('gp_no');
        $this->db->order_by("id", "desc");
        $q = $this->db->get_where('gp_src',array('doc_no' => $rono),1);
        $ar = $q->result();
        //echo "id=>".$ar[0]->id."<br>";
        return $ar[0]->gp_no;
    }

    public function get_lot_gp_number($lotid)
    {
        echo "lot_id=>".$lotid."<br>";
        $this->db->select('gp_no');
        //$this->db->order_by("id", "desc");
        $q = $this->db->get_where('gp_item_lot',array('id' => $lotid),1);
        $ar = $q->result();
        //print_r($ar); die;
        //echo "lot_id=>".$ar[0]->gp_no."<br>"; die;
        return $ar[0]->gp_no;
    }

    public function getproductbyid($id)
    {
        $this->db->select("cons_item_stk_lot.*,products.serial_number")
        //$this->db->select("cons_item_stk_lot.*")
        ->join("products","products.id = cons_item_stk_lot.item_id")
        ->where("cons_item_stk_lot.item_id",$id);
        $q = $this->db->get('cons_item_stk_lot');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            //echo "<pre>";print_r($data);exit;
            return $data;
        }
        
    }  

  public function get_lot_info($lotno)
    {
         
        if(!empty($lotno))
        {
           $q = "SELECT `sma_cons_item_stk_lot`.* FROM `sma_cons_item_stk_lot` WHERE `sma_cons_item_stk_lot`.`lot_no` = '".$lotno."'";
           if($a= $this->db->query($q))
           {      $ar= $a->result();
                  //print_r($ar); die;
                  return $ar;

           }
           
           else
           {
            return false;
           }

    }
    }
    //added by vikas singh to get warehouse list segment wise and gatepass type
    
    function getWarehouseListbyGatepassType($gp_type_id) {
        
        if($gp_type_id > 0){
             
            if($gp_type_id =='1080' || $gp_type_id =='1081')
            {
                $type = 2;
            }
            else if($gp_type_id =='1083' || $gp_type_id =='1084'){
            
                $type = 3;
            }
            else if($gp_type_id =='1085' || $gp_type_id =='1086'){
            
                $type = 4;
            }
            else if($gp_type_id =='1088'){
            
                $type = 2;
            }
            $segment_id = $_SESSION['segment_id'];
            $this->db->select('*');
            $this->db->where('segment_id',$segment_id);
            $this->db->where('wh_type',$type);
            $q = $this->db->get('warehouses');
            //echo "<pre>";
            //print_r($segment_id);exit;
            return $q->result();   
        }
    }

    public function get_gp_itm_id ($itmid,$stono) 
    {
        if(!empty($itmid) && !empty($stono))
        {
            $q= "SELECT `sma_gp_item`.`id` FROM `sma_gp_item` INNER JOIN `sma_gp` ON `sma_gp`.`id` = `sma_gp_item`.`gp_src_id` WHERE `sma_gp`.`doc_no` = '".$stono."' AND `sma_gp_item`.`item_id` = '".$itmid."' ORDER BY `sma_gp_item`.`id` DESC LIMIT 1";
            if($a= $this->db->query($q))
               {      $ar= $a->result();
                     // print_r($ar[0]->id); die;
                      return $ar[0]->id;

               }
               
               else
               {
                return false;
               }

        }
        
    }

    public function get_gp_itm_gp_no ($itmid,$stono) 
    {
        if(!empty($itmid) && !empty($stono))
        {
            $q= "SELECT `sma_gp_item`.`gp_no` FROM `sma_gp_item` INNER JOIN `sma_gp` ON `sma_gp`.`id` = `sma_gp_item`.`gp_src_id` WHERE `sma_gp`.`doc_no` = '".$stono."' AND `sma_gp_item`.`item_id` = '".$itmid."' ORDER BY `sma_gp_item`.`id` DESC LIMIT 1";
            if($a= $this->db->query($q))
               {      $ar= $a->result();
                     // print_r($ar[0]->id); die;
                      return $ar[0]->gp_no;

               }
               
               else
               {
                return false;
               }

        }
        
    }

    // AK
    public function lot_table_id_new($updtgp,$rno,$gpitemid,$lotno)
    {
        //echo $updtgp."=ro=".$rno."=itemid=".$gpitemid."=lotno=".$lotno; die;
        if(!empty($updtgp) && !empty($gpitemid))
        {
            $q= "SELECT z.id FROM sma_gp_item_lot z INNER JOIN sma_gp_item a ON (a.id = z.mrn_gp_item_id) INNER JOIN sma_gp_src b ON (b.id = a.gp_src_id) INNER JOIN sma_gp c ON (c.id = b.gp_id) WHERE c.id = ".$updtgp." AND c.doc_no= '".$rno."' AND a.item_id= ".$gpitemid." AND z.lot_no='".$lotno."'";
            if($a= $this->db->query($q))
               {      $ar= $a->result();
                      //print_r($ar[0]->id); die;
                      return $ar[0]->id;

               }

        }
        return false;

    }

    public function get_bin_id($lotno)
    { //echo $lotno."<br>";
        if(!empty($lotno))
        {
           // $q = "SELECT `sma_cons_item_stk_lot_bin`.`bin_id` FROM `sma_cons_item_stk_lot_bin` JOIN `sma_cons_item_stk_lot` ON `sma_cons_item_stk_lot`.`id` = `sma_cons_item_stk_lot_bin`.`lot_id` JOIN `sma_gp_item_lot` ON `sma_gp_item_lot`.`lot_no` = `sma_cons_item_stk_lot`.`lot_no` WHERE `sma_gp_item_lot`.`lot_no` = '".$lotno."' LIMIT 1";
            $q="SELECT `sma_cons_item_stk_lot_bin`.`bin_id` FROM `sma_cons_item_stk_lot_bin` JOIN `sma_cons_item_stk_lot` ON (`sma_cons_item_stk_lot`.`id` = `sma_cons_item_stk_lot_bin`.`lot_id` AND `sma_cons_item_stk_lot`.`item_id` = `sma_cons_item_stk_lot_bin`.`item_id`) WHERE `sma_cons_item_stk_lot`.`lot_no` = '".$lotno."' LIMIT 1";
            if($a= $this->db->query($q))
               {      $ar= $a->result();
                      //print_r($ar); die;
                      return $ar[0]->bin_id;

               }
               
               else
               {
                return false;
               }
         }   
    }

    public function update_stk($wid,$item_id,$qty)
    {
        //echo "Qty=>".$qty."=>wid=>".$wid."=>itemid=>".$item_id;
        $q = "SELECT available_stk FROM sma_cons_item_stk_profile WHERE warehouse_id = ".$wid." AND item_id = ".$item_id."";
            if($a= $this->db->query($q))
               {      $ar= $a->result();
                      //print_r($ar); die;
                      $p = $ar[0]->available_stk;
                      $total = $p - $qty;
                      //echo $total; die;
                      $q1 = "UPDATE sma_cons_item_stk_profile SET available_stk = ".$total.", upd_flg='1'  WHERE warehouse_id = ".$wid." AND item_id = ".$item_id."";
                      $this->db->query($q1);



               }

    }

    public function update_cons_stk($wid,$item_id,$qty,$rono)
    {
        // $q = "SELECT req_qty_bs,status,itm_price_bs FROM sma_emrs_item WHERE warehouse_id = ".$wid." AND item_id = ".$item_id." AND doc_no_src_id = '".$rono."'";
        $q = "SELECT bal_qty,status,itm_price_bs FROM sma_emrs_item WHERE warehouse_id = ".$wid." AND item_id = ".$item_id." AND doc_no_src_id = '".$rono."'";
        if($a= $this->db->query($q))
       {      
        $ar= $a->result();
              //print_r($ar); die;
          $p = $ar[0]->bal_qty;
          $price = $ar[0]->itm_price_bs;
          $total = $p - $qty;
          $a = $total * $price;
          if($total!=0){
              // $q1 = "UPDATE sma_emrs_item SET req_qty_bs = ".$total.", itm_amt_bs = ".$a.", status = 2  WHERE warehouse_id = ".$wid." AND item_id = ".$item_id." AND doc_no_src_id = '".$rono."'";
            $q1 = "UPDATE sma_emrs_item SET bal_qty = ".$total.", itm_amt_bs = ".$a.", status = 2  WHERE warehouse_id = ".$wid." AND item_id = ".$item_id." AND doc_no_src_id = '".$rono."'";
           }
           else{
               $q1 = "UPDATE sma_emrs_item SET bal_qty = ".$total.", itm_amt_bs = ".$a.", status = 1  WHERE warehouse_id = ".$wid." AND item_id = ".$item_id." AND doc_no_src_id = '".$rono."'";
              }
          $this->db->query($q1);
       }
    }

    public function update_stk_lot($lot,$itemid,$qty)
    {
        $q = "SELECT total_stk FROM sma_cons_item_stk_lot WHERE item_id = ".$itemid." AND lot_no = '".$lot."'";
            if($a= $this->db->query($q))
               {      
                $ar= $a->result();
                      //print_r($ar); die;
                  $p = $ar[0]->total_stk;
                  $total = $p - $qty;
                  $q1 = "UPDATE sma_cons_item_stk_lot SET total_stk = ".$total.", upd_flg='1'  WHERE item_id = ".$itemid." AND lot_no = '".$lot."'";
                  $this->db->query($q1);
               }
    }

    public function update_stk_lot_bin($lot,$itemid,$qty)
    {
        $q = "SELECT a.total_stk,a.lot_id FROM sma_cons_item_stk_lot_bin a INNER JOIN sma_cons_item_stk_lot b ON (a.lot_id = b.id)  WHERE b.item_id = ".$itemid." AND b.lot_no = '".$lot."'";
            if($a= $this->db->query($q))
           {      $ar= $a->result();
                 // print_r($ar); die;
                  $p = $ar[0]->total_stk;
                  $total = $p - $qty;
                  $q1 = "UPDATE sma_cons_item_stk_lot_bin a INNER JOIN sma_cons_item_stk_lot b ON (a.lot_id = b.id) SET a.total_stk = ".$total.", a.upd_flg='1'  WHERE b.item_id = ".$itemid." AND b.lot_no = '".$lot."'";
                  $this->db->query($q1);
           }
    }

// ---------------Ankit-HM Start-------------
    // Add by Ankit
    public function getStoProductsHold($sto_no,$sto_date,$gpid){
        $q= "select a.id as eid,a.act_rcpt_qty,a.dlv_note_qty,a.gp_src_id,a.item_amt,a.item_id as id,a.item_id,a.item_price,a.item_uom_id,a.org_id,a.pending_qty,a.segment_id,a.warehouse_id,c.id as cid,c.doc_no,c.doc_dt,d.name,d.unit,d.price,e.available_stk FROM sma_gp_item a INNER JOIN sma_gp_src b ON (a.warehouse_id = b.warehouse_id AND a.gp_src_id = b.id) INNER JOIN sma_gp c ON (c.id = b.gp_id AND c.doc_no = b.doc_no) INNER JOIN sma_products d ON (d.id = a.item_id) INNER JOIN sma_cons_item_stk_profile e ON (e.warehouse_id = a.warehouse_id AND e.item_id = a.item_id) WHERE c.status = 0 AND c.id = ".$gpid." AND c.doc_no= '".$sto_no."' AND c.doc_dt='".$sto_date."' ";
            if($a= $this->db->query($q))
           {      $ar= $a->result();
                  return $ar;
            
           }

        return false;  

    }
    // Add by Ankit
    public function getholdproductbyid($id,$stono,$gpid)
    {
        $this->db->select("gp_item_lot.id,gp_item_lot.lot_no,gp_item_lot.lot_qty,products.serial_number,cons_item_stk_lot.total_stk,cons_item_stk_lot.rejected_stk")
        ->join("gp_item","gp_item.id = gp_item_lot.mrn_gp_item_id")
        ->join("gp_src","gp_item.gp_src_id = gp_src.id")
        ->join("gp","gp.id = gp_src.gp_id")
        ->join("cons_item_stk_lot","cons_item_stk_lot.lot_no = gp_item_lot.lot_no")        
        ->join("products","products.id = gp_item_lot.item_id")
        ->where("gp.status",'0')
        ->where("gp.id",$gpid)        
        ->where("gp.doc_no",$stono)        
        ->where("gp_item_lot.item_id",$id);
        $q = $this->db->get('gp_item_lot');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            //echo "<pre>";print_r($data);exit;
            return $data;
        }
        
    }

    // Add By Ankit
    function get_item_id($id,$itemid)
    {
        $q = "SELECT c.id FROM sma_gp a INNER JOIN  sma_gp_src b ON (a.id = b.gp_id) INNER JOIN sma_gp_item c ON (b.id = c.gp_src_id) WHERE a.id = ".$id." AND c.item_id = ".$itemid." ";
        
        if($a= $this->db->query($q))
           {      $ar= $a->result();
                  //print_r($ar); die;
                  return $ar[0]->id;
            
           }

        return false; 
    }

    // Add By Ankit
    function get_lot_id($id)
    {
        $q = "SELECT d.id FROM sma_gp a INNER JOIN  sma_gp_src b ON (a.id = b.gp_id) INNER JOIN sma_gp_item c ON (b.id = c.gp_src_id) INNER JOIN sma_gp_item_lot d ON (d.mrn_gp_item_id = c.id) INNER JOIN sma_gp_lot_bin e ON (e.lot_id = d.id) WHERE a.id = ".$id."";
        if($a= $this->db->query($q))
           {      $ar= $a->result();
                  //print_r($ar); die;
                  return $ar[0]->id;
            
           }

        return false; 

    }

    // Add By Ankit
    function get_lotbin_id($id)
    {
        $q = "SELECT e.id FROM sma_gp a INNER JOIN  sma_gp_src b ON (a.id = b.gp_id) INNER JOIN sma_gp_item c ON (b.id = c.gp_src_id) INNER JOIN sma_gp_item_lot d ON (d.mrn_gp_item_id = c.id) INNER JOIN sma_gp_lot_bin e ON (e.lot_id = d.id) WHERE a.id = ".$id."";
        if($a= $this->db->query($q))
           {      $ar= $a->result();
                  //print_r($ar); die;
                  return $ar[0]->id;
            
           }

        return false; 

    }

    // Add by Ankit
    // ankit new
    function lot_table_id($gp_item_id,$lot_no)
    {
        $count = 0;
        $q = "SELECT id FROM `sma_gp_item_lot` WHERE lot_no = '".$lot_no."' AND mrn_gp_item_id= ".$gp_item_id."";
        if($a= $this->db->query($q))
           {      $ar= $a->result();
                  //print_r($ar); die;
                  return $ar[0]->id;
            
           }
           return $count;
    }
    // Add by Ankit
    // ankit new
    function update_gp_bin($lot_gp_id,$qty)
    {
        $q = "UPDATE sma_gp_lot_bin SET bin_qty = ".$qty." WHERE lot_id = ".$lot_gp_id."";
        if($this->db->query($q))
        {
            return TRUE;
        }
        return false;
    }

    //AK
    public function get_lot_info_new($whid, $itmid, $qtys)
    {
        $sr = array();
        if(!empty($itmid) && !empty($qtys))
        {
           $qty =  number_format($qtys, 6);
           $q = "SELECT a.* FROM sma_cons_item_stk_lot a WHERE a.warehouse_id = ".$whid." AND a.item_id = ".$itmid." AND a.total_stk > 0  ORDER BY a.id ASC ";
           if($a= $this->db->query($q))
           {     
               $ar= $a->result();
              // print_r($ar); die;
           }
           if(!empty($ar))
           {
              foreach ($ar as $key) {
                   if($key->total_stk >= $qty)
                     { 
                        $key->total_stk = number_format($qty, 6);
                         $sr[]= $key;
                         //print_r($sr); die;
                         return $sr;
                     }
                  else {
                      $sr[]= $key;
                      $qty = $qty - $key->total_stk;
                   }
                 
              }

           }
           
          return false;
           

       }
    }

    //AK 

    public function get_lotbin_info_new($id, $itmid, $qtys)
    {
        $sr = array(); //echo $qtys;
        if(!empty($itmid) && !empty($qtys))
        {
           $qty =  number_format($qtys, 6);
           $q = "SELECT a.* FROM sma_cons_item_stk_lot_bin a WHERE a.lot_id = ".$id." AND a.item_id = ".$itmid." AND a.total_stk > 0  ORDER BY a.id ASC ";
           if($a= $this->db->query($q))
           {     
               $ar= $a->result();
              // print_r($ar); die;
           }
           if(!empty($ar))
           {
              foreach ($ar as $key) {
                   if($key->total_stk >= $qty)
                     { 
                        $key->total_stk = number_format($qty, 6);
                         $sr[]= $key;
                         //print_r($sr); die;
                         return $sr;
                     }
                  else {
                      $sr[]= $key;
                      $qty = $qty - $key->total_stk;
                   }
                 
              }

           }
           
          return false;
           

       }
    }

 // AK 

    public function update_stk_lot_new($lot,$itemid,$qty)
    {
        $q = "SELECT total_stk FROM sma_cons_item_stk_lot WHERE item_id = ".$itemid." AND lot_no = '".$lot."'";
            if($a= $this->db->query($q))
               {      
                $ar= $a->result();
                      //print_r($ar); die;
                  $p = $ar[0]->total_stk;
                  $total = $p - $qty;
                  $q1 = "UPDATE sma_cons_item_stk_lot SET total_stk = ".$total."  WHERE item_id = ".$itemid." AND lot_no = '".$lot."'";
                  $this->db->query($q1);
               }
    }

// AK
   
public function update_stk_lot_bin_new($binid,$itemid,$qty)
    {
        $q = "SELECT a.total_stk FROM sma_cons_item_stk_lot_bin a WHERE a.item_id = ".$itemid." AND a.id = ".$binid."";
            if($a= $this->db->query($q))
           {      $ar= $a->result();
                 // print_r($ar); die;
                  $p = $ar[0]->total_stk;
                  $total = $p - $qty;
                  $q1 = "UPDATE sma_cons_item_stk_lot_bin a SET a.total_stk = ".$total."  WHERE a.item_id = ".$itemid." AND a.id = ".$binid."";
                  $this->db->query($q1);
           }
    }
   


    
    // ---------------Ankit-HM End-------------  

public function findrono($doc_no,$doc_array,$qty_array)
{
    //echo "doc==>".$doc_no."<br/>";
   // print_r($doc_array); echo "<br/>";//die;
   // print_r($qty_array); echo "<br/>"; //die;
    $a=0;
    foreach($doc_array as $val=>$key)
    {      
       if($doc_no == $key)
       {
          // echo "Key==>".$key."<br/><br/><br/>";
           $b[] = $val;
       }
    }
  //echo "<pre>";print_r($b); //die;
    if(!empty($b))
    {
        foreach($qty_array as $k => $v)
        {
          foreach($b as $k1 => $v1)
          {
           if($k == $v1)
           {
               if($v > 0)
               {
                   //$l[] =  $k;
                   return true;
               }
           }
        }
           $a++;
        }
        //return $l;
    }
    return false;
    
}    
}
