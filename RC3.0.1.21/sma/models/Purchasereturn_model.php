<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Purchasereturn_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getPrchseRetData($term, $limit = 5)
    {
        $response = array('returnType'=>array());
        $returnType = array('0'=>'Select Return Type','1'=>'Direct','2'=>'Material Return Note'); // return types
        $response['return_type'] = $returnType;
        $warehouse_detail = $this->site->get_warehouse_ids(1); // warehouse details
        $response['warehouse_detail'] = $warehouse_detail;
        return $response;
    }

   

}
