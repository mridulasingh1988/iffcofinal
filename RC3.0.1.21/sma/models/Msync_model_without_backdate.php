<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msync_model extends CI_Model
{
    
    public function __construct()
    {
        parent::__construct();
        //$this->load->library('multipledb');
        $this->load->model("auth_model");
        $this->load->database();
    }
    
    public function intrDbName()
    {
        $CI = &get_instance();
        $this->db2 = $CI->load->database('db2', TRUE);
        $intrDB = $this->db2->database;
        return $intrDB;

    }
    public function posDbName()
    {
        $posDB = $this->db->database;
        return $posDB;

    }
    // S.No.-01 (Intermediate -> POS)
    
    public function currencyMaster()
    {
        $ar = $this->currencyUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n  = '$';
        $w  = "INSERT INTO ".$posDB.".sma_currencies(code,name,rate,curr_id) SELECT a.CURR_NOTATION,a.CURR_NM,'1',a.CURR_ID FROM ".$intrDB.".intrm".$n."curr a WHERE a.POS_CURR_ID IS NULL OR a.POS_CURR_ID = ''";
        if ($this->db->query($w)) {
            $w1 = "UPDATE ".$intrDB.".intrm".$n."curr a INNER JOIN ".$posDB.".sma_currencies b ON a.CURR_ID = b.curr_id SET a.POS_CURR_ID = b.id,a.CREATE_FLG = '1' WHERE a.CURR_ID = b.curr_id";
            $this->db->query($w1);
            $msg = "Currency Records save successfully...";
            return $msg;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }
    
    public function currencyUPD()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_currencies a INNER JOIN ".$intrDB.".intrm".$n."curr b ON (a.curr_id = b.CURR_ID AND a.id = b.POS_CURR_ID) SET a.code = b.CURR_NOTATION,a.name = b.CURR_NM WHERE b.UPD_FLG= '3'";
        if ($this->db->query($w)) {
            $w1 = "UPDATE ".$intrDB.".intrm".$n."curr a INNER JOIN ".$posDB.".sma_currencies b ON (a.CURR_ID = b.curr_id AND b.id = a.POS_CURR_ID) SET a.UPD_FLG = '' WHERE (a.CURR_ID = b.curr_id AND b.id = a.POS_CURR_ID) AND a.UPD_FLG= '3'";
            $this->db->query($w1);
            return true;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }
    
    
    // S.NO-02 (Intermediate -> POS)
    public function warehouseMaster()
    {
        //echo "hii.."; die;
        // CASE WHEN a.WH_ONRSHP_TYPE=229 THEN '1' WHEN a.WH_ONRSHP_TYPE=958 THEN '2' ELSE 'NULL' END as type
        $ar = $this->warehouseMasterUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_companies( group_name, name, company, phone, country, org_id,wh_id ) SELECT 'biller', a.WH_NM, a.WH_NM, '0123456789', 'INDIA', a.ORG_ID,a.WH_ID FROM ".$intrDB.".intrm".$n."wh".$n."org a WHERE (a.POS_WH_ID IS NULL OR a.POS_WH_ID = '')";
        $wex= "INSERT INTO ".$posDB.".sma_companies( group_name, name, company, phone, country, org_id,wh_id ) SELECT 'biller', a.WH_NM, a.WH_NM, '0123456789', 'INDIA', a.ORG_ID,a.WH_ID FROM ".$intrDB.".intrm".$n."app".$n."wh".$n."org".$n."ext a WHERE (a.POS_APP_WH_ORG_ID IS NULL OR a.POS_APP_WH_ORG_ID = '')";
        if ($this->db->query($w)) {
            $this->db->query($wex);
            $w1= "INSERT INTO ".$posDB.".sma_warehouses(org_id,wh_id,wh_type,usr_id_create,usr_id_create_dt,name,wh_onrshp_type,wh_strg_type,adds_id,wh_desc,actv,inactv_resn,inactv_dt,wh_ent_id,prj_id,segment_id,address,biller_id) SELECT a.ORG_ID,a.WH_ID,'1',a.USR_ID_CREATE,DATE_FORMAT(a.USR_ID_CREATE_DT,'%Y-%m-%d'),a.WH_NM,a.WH_ONRSHP_TYPE,a.WH_STRG_TYPE,a.ADDS_ID,a.WH_DESC,a.ACTV,a.INACTV_RESN,DATE_FORMAT(a.INACTV_DT,'%Y-%m-%d'),a.WH_ENT_ID,a.PRJ_ID,z.id,'INDIA',p.id  FROM ".$intrDB.".intrm".$n."wh".$n."org a INNER JOIN ".$posDB.".sma_segment z ON ((a.PRJ_ID = z.prj_doc_id) AND (a.ORG_ID = z.org_id)) INNER JOIN ".$posDB.".sma_companies p ON((p.org_id = a.ORG_ID) AND (p.wh_id = a.WH_ID)) WHERE a.POS_WH_ID IS NULL OR a.POS_WH_ID = ''";
            $w1ex= "INSERT INTO ".$posDB.".sma_warehouses(org_id,wh_id,wh_type,usr_id_create,usr_id_create_dt,name,wh_onrshp_type,wh_strg_type,adds_id,wh_desc,actv,inactv_resn,inactv_dt,wh_ent_id,prj_id,segment_id,address,biller_id) SELECT a.ORG_ID,a.WH_ID,'2',a.USR_ID_CREATE,DATE_FORMAT(a.USR_ID_CREATE_DT,'%Y-%m-%d'),a.WH_NM,a.WH_ONRSHP_TYPE,a.WH_STRG_TYPE,a.ADDS_ID,a.WH_DESC,a.ACTV,a.INACTV_RESN,DATE_FORMAT(a.INACTV_DT,'%Y-%m-%d'),a.WH_ENT_ID,a.PRJ_ID,z.id,'INDIA',p.id  FROM ".$intrDB.".intrm".$n."app".$n."wh".$n."org".$n."ext a INNER JOIN ".$posDB.".sma_segment z ON ((a.PRJ_ID = z.prj_doc_id) AND (a.ORG_ID = z.org_id)) INNER JOIN ".$posDB.".sma_companies p ON((p.org_id = a.ORG_ID) AND (p.wh_id = a.WH_ID)) WHERE a.POS_APP_WH_ORG_ID IS NULL OR a.POS_APP_WH_ORG_ID = ''";
        if($this->db->query($w1)){
            $this->db->query($w1ex);
            $w2= "UPDATE ".$intrDB.".intrm".$n."wh".$n."org a INNER JOIN ".$posDB.".sma_warehouses b ON ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id)) SET a.POS_WH_ID = b.id,a.SYNC_FLG='1' WHERE ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id))";
            $w2ex= "UPDATE ".$intrDB.".intrm".$n."app".$n."wh".$n."org".$n."ext a INNER JOIN ".$posDB.".sma_warehouses b ON ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id)) SET a.POS_APP_WH_ORG_ID = b.id WHERE ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id))";
             if($this->db->query($w2))
             {
                $this->db->query($w2ex);
                $msg = "Warehouse ORG Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
           }

        } 
        else {
            $msg = $this->db->error();
            return $msg['message'];
           }
     }


	public function warehouseMasterUPD()
     {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_warehouses a INNER JOIN ".$intrDB.".intrm".$n."wh".$n."org b ON (a.id = b.POS_WH_ID) INNER JOIN ".$posDB.".sma_segment z ON ((b.PRJ_ID = z.prj_doc_id) AND (b.ORG_ID = z.org_id)) SET a.org_id = b.ORG_ID,a.wh_id = b.WH_ID,a.name = b.WH_NM,a.wh_onrshp_type = b.WH_ONRSHP_TYPE,a.wh_strg_type = b.WH_STRG_TYPE, a.actv = b.ACTV,a.prj_id=b.PRJ_ID,a.segment_id = z.id WHERE b.SYNC_FLG = '3' ";
        if ($this->db->query($w)) {
            $w1 = "UPDATE ".$posDB.".sma_warehouses a INNER JOIN ".$intrDB.".intrm".$n."app".$n."wh".$n."org".$n."ext b ON (a.id = b.POS_APP_WH_ORG_ID) INNER JOIN ".$posDB.".sma_segment z ON ((b.PRJ_ID = z.prj_doc_id) AND (b.ORG_ID = z.org_id)) SET a.org_id = b.ORG_ID,a.wh_id = b.WH_ID,a.name = b.WH_NM,a.wh_onrshp_type = b.WH_ONRSHP_TYPE,a.wh_strg_type = b.WH_STRG_TYPE, a.actv = b.ACTV,a.prj_id=b.PRJ_ID,a.segment_id = z.id WHERE b.UPD_FLG = '3' ";
        if($this->db->query($w1))
        {
            $w2 = "UPDATE ".$intrDB.".intrm".$n."wh".$n."org a INNER JOIN ".$posDB.".sma_warehouses b ON (a.POS_WH_ID = b.id) SET a.SYNC_FLG='' WHERE (a.POS_WH_ID = b.id) AND a.SYNC_FLG='3'";
            $w3 = "UPDATE ".$intrDB.".intrm".$n."app".$n."wh".$n."org".$n."ext a INNER JOIN ".$posDB.".sma_warehouses b ON (a.POS_APP_WH_ORG_ID = b.id) SET a.UPD_FLG='' WHERE (a.POS_APP_WH_ORG_ID = b.id) AND a.UPD_FLG='3'";
            $this->db->query($w2);
            if($this->db->query($w3))
            {
                return true;
            } else {
              $msg = $this->db->error();
              return $msg['message'];
            }
            
        } else {
              $msg = $this->db->error();
              return $msg['message'];
        }    
      } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
        
     }    
    // S.NO.-03 (Intermediate -> POS)    
    
    public function userMaster()
    {
        //$this->userUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "INSERT INTO ".$posDB.".sma_users(usr_id,username,password,salt,email,first_name,last_name,active,avatar,gender,phone,created_on,company,group_id,warehouse_id,segment_id,show_discount,org_id,pwid) SELECT a.USR_ID,a.USR_NAME,sha1(a.USR_PWD),'Null','Null',a.USR_FST_NAME,a.USR_LST_NAME, CASE WHEN a.USR_ACTV = 'Y' THEN '1' ELSE '0' END AS Activ,a.USR_IMG,a.USR_GNDR,a.USR_CONTACT_NO,a.USR_ID_CREATE_DT,'IFFCO','5',group_concat(b.id,''),b.segment_id,a.USR_DISC,a.ORG_ID,group_concat(b.id,'') FROM ".$intrDB.".intrm".$n."usr a INNER JOIN ".$posDB.".sma_warehouses b ON ((a.ORG_ID = b.org_id) AND (a.WH_ID = b.wh_id)) WHERE (a.POS_USR_ID IS NULL OR a.POS_USR_ID = '') GROUP BY a.ORG_ID,b.prj_id ";
        if ($this->db->query($w)) {
            $msg = "User Records save successfully...";
            $w1  = "UPDATE ".$intrDB.".intrm".$n."usr a INNER JOIN ".$posDB.".sma_users b ON (a.USR_ID = b.usr_id AND a.ORG_ID = b.org_id) SET a.POS_USR_ID= b.id, a.CREATE_FLG='1' WHERE (a.USR_ID = b.usr_id AND a.ORG_ID = b.org_id)";
            $this->db->query($w1);
            return $msg;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
        
    }
    public function userUPD()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_users a INNER JOIN ".$intrDB.".intrm".$n."usr b ON( a.id = b.POS_USR_ID AND a.usr_id = b.USR_ID ) INNER JOIN ".$posDB.".sma_warehouses c ON ((a.ORG_ID = c.org_id) AND (a.WH_ID = c.wh_id)) SET a.username = b.USR_NAME, a.password = SHA1(b.USR_PWD), a.email = 'NULL', a.first_name = b.USR_FST_NAME, a.last_name = b.USR_LST_NAME, a.active = CASE WHEN b.USR_ACTV = 'Y' THEN '1' ELSE '0' END, a.avatar = b.USR_IMG,a.phone = b.USR_CONTACT_NO, a.warehouse_id = group_concat(c.id,''), a.show_discount = b.USR_DISC WHERE b.UPD_FLG = '3' GROUP BY (b.USR_NAME)";
        if ($this->db->query($w)) {
            
            $w1 = "UPDATE ".$intrDB.".intrm".$n."usr a INNER JOIN ".$posDB.".sma_users b ON (a.POS_USR_ID = b.id AND a.USR_ID= b.usr_id) SET a.UPD_FLG='' WHERE (a.POS_USR_ID = b.id AND a.USR_ID= b.usr_id) AND a.UPD_FLG='3'";
            $this->db->query($w1);
            return true;
        }
        
        else {
            $msg = $this->db->error();
            return $msg['message'];
        }
        
    }
    
    
    // S.NO.-04 (Intermediate -> POS)
    
    public function taxMaster()
    {
        //$ar = $this->taxUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n  = '$';
        $w  = "INSERT INTO ".$posDB.".sma_tax_rates(name,rate,org_id,wh_id,grp_id) SELECT CONCAT(a.TAX_VAL,'% TAX'),a.TAX_VAL,a.ORG_ID,a.WH_ID,a.GRP_ID FROM ".$intrDB.".intrm".$n."itm".$n."grp".$n."org a WHERE a.POS_GRP_ID IS NULL OR a.POS_GRP_ID = ''";
        if ($this->db->query($w)) {
            $msg = "Tax Records save successfully...";
            return $msg;
        }
    }
    
    public function taxUPD()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_tax_rates a INNER JOIN ".$intrDB.".intrm".$n."itm".$n."grp".$n."org b ON (a.org_id = b.ORG_ID AND a.grp_id = b.GRP_ID AND a.wh_id = b.WH_ID) SET a.name = CONCAT(b.TAX_VAL,'% TAX'),a.rate = b.TAX_VAL WHERE b.UPD_FLG='3'";
        if ($this->db->query($w)) {
            return true;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
        
    }
    
    // S.NO.-05 (Intermediate -> POS)
    
    public function categoryMaster()
    {
        $ar = $this->categoryUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n  = '$';
        $w  = "INSERT INTO ".$posDB.".sma_categories(code, name, parent_id, grp_id_parent, tax_rate_id) SELECT a.GRP_ID, a.GRP_NM, CASE WHEN a.GRP_ID_PARENT = '0' THEN '0' ELSE 'NULL' END AS asd, a.GRP_ID_PARENT, '0' FROM ".$intrDB.".intrm".$n."itm".$n."grp a WHERE a.POS_GRP_ID IS NULL OR a.POS_GRP_ID = ''";
        if ($this->db->query($w)) {
            $ws = "UPDATE ".$posDB.".sma_categories a INNER JOIN ".$posDB.".sma_categories b ON (a.grp_id_parent = b.code) SET a.parent_id = b.id WHERE a.grp_id_parent = b.code";
            $this->db->query($ws);
            $ws1= "UPDATE ".$posDB.".sma_categories a INNER JOIN ".$posDB.".sma_categories b ON (a.id = b.parent_id) SET b.level=2 WHERE a.parent_id=0 AND b.parent_id<>0";
            $this->db->query($ws1);
            $ws2= "UPDATE ".$posDB.".sma_categories a INNER JOIN ".$posDB.".sma_categories b ON (a.id = b.parent_id) SET b.level=3 WHERE a.parent_id<>0 AND b.parent_id<>0";
            $this->db->query($ws2);

            $w1 = "UPDATE ".$intrDB.".intrm".$n."itm".$n."grp a INNER JOIN ".$posDB.".sma_categories b ON a.GRP_ID = b.code SET a.POS_GRP_ID = b.id WHERE a.GRP_ID = b.code";
            $this->db->query($w1);
             $w2 = "UPDATE ".$intrDB.".intrm".$n."itm".$n."grp".$n."org a INNER JOIN ".$posDB.".sma_categories b ON a.GRP_ID = b.code SET a.POS_GRP_ID = b.id, a.CREATE_FLG = '1' WHERE a.GRP_ID = b.code";
            $this->db->query($w2);
            $msg = "category Records save successfully...";
            return $msg;
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
        
    }
    public function categoryUPD()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_categories a INNER JOIN ".$intrDB.".intrm".$n."itm".$n."grp b ON (b.POS_GRP_ID = a.id AND b.GRP_ID = a.code) INNER JOIN ".$intrDB.".intrm".$n."itm".$n."grp".$n."org c ON (c.POS_GRP_ID = b.POS_GRP_ID AND c.GRP_ID = b.GRP_ID) SET a.code = b.GRP_ID,a.name = b.GRP_NM, a.grp_id_parent = b.GRP_ID_PARENT WHERE c.UPD_FLG='3'";
        if ($this->db->query($w)) {
            $w1 = "UPDATE ".$intrDB.".intrm".$n."itm".$n."grp".$n."org a INNER JOIN ".$posDB.".sma_categories b ON (a.GRP_ID = b.code AND a.POS_GRP_ID = b.id) SET a.UPD_FLG = 'NULL' WHERE (a.GRP_ID = b.code AND a.POS_GRP_ID = b.id)";
            $this->db->query($w1);
            return true;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
        
        
    }
    
    // S.NO.-06 (Intermediate -> POS)
    
    public function customerMaster()
    {
        //$ar = $this->customerUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n  = '$';
        $w  = "INSERT INTO ".$posDB.".sma_companies(group_id,group_name,customer_group_id,customer_group_name,name,phone,email,vat_no,address,country,eo_type,eo_id,sync_flg) SELECT CASE WHEN a.EO_TYPE = 'C' THEN '3' WHEN a.EO_TYPE = 'S' THEN '4' ELSE '7' END AS GP_ID,CASE WHEN a.EO_TYPE = 'C' THEN 'customer' WHEN a.EO_TYPE = 'T' THEN 'Transporter' ELSE 'Supplier' END AS GP_NM,CASE WHEN a.EO_TYPE = 'C' THEN '1' ELSE 'NULL' END AS GP_id_cus,CASE WHEN a.EO_TYPE = 'C' THEN 'General' ELSE 'NULL' END AS CGP_IDNM,a.EO_NM,a.EO_PHONE,'Not Available',a.VAT_NO,b.ADDRESS,'INDIA',a.EO_TYPE,a.EO_ID,'1' FROM ".$intrDB.".intrm".$n."eo a LEFT JOIN ".$intrDB.".intrm".$n."eo".$n."add b ON(b.EO_ID = a.EO_ID AND b.EO_TYPE = a.EO_TYPE)  WHERE (a.POS_EO_ID IS NULL OR a.POS_EO_ID = '')";
        if ($this->db->query($w)) {
            $msg = "customer Records save successfully...";
            //$w1  = "UPDATE ".$intrDB.".intrm".$n."eo a INNER JOIN ".$posDB.".sma_companies z ON (z.eo_id = a.EO_ID AND z.eo_type = a.EO_TYPE) LEFT JOIN ".$intrDB.".intrm".$n."eo".$n."add b ON (b.EO_ID = z.eo_id AND z.eo_type = b.EO_TYPE) RIGHT JOIN ".$intrDB.".intrm".$n."eo".$n."add".$n."org c ON (c.EO_ID = z.eo_id AND z.eo_type = c.EO_TYPE) RIGHT JOIN ".$intrDB.".intrm".$n."eo".$n."org d ON (d.EO_ID = z.eo_id AND z.eo_type = d.EO_TYPE) SET a.POS_EO_ID = z.id, b.POS_EO_ID = z.id, c.POS_EO_ID = z.id, c.CREATE_FLG = '1', d.POS_EO_ID = z.id, d.CREATE_FLG = '1' WHERE z.eo_id = a.EO_ID AND z.eo_type = a.EO_TYPE";
            $w1  = "UPDATE ".$intrDB.".intrm".$n."eo a INNER JOIN ".$posDB.".sma_companies z ON (z.eo_id = a.EO_ID AND z.eo_type = a.EO_TYPE) SET a.POS_EO_ID = z.id WHERE z.eo_id = a.EO_ID AND z.eo_type = a.EO_TYPE";
            $this->db->query($w1);
            $w2  = "UPDATE ".$intrDB.".intrm".$n."eo".$n."org a INNER JOIN ".$posDB.".sma_companies z ON (z.eo_id = a.EO_ID AND z.eo_type = a.EO_TYPE) SET a.POS_EO_ID = z.id,a.CREATE_FLG = '1' WHERE z.eo_id = a.EO_ID AND z.eo_type = a.EO_TYPE";
            $this->db->query($w2);
            return $msg;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }
    
    public function customerUPD()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_companies z INNER JOIN ".$intrDB.".intrm".$n."eo a ON a.POS_EO_ID = z.id INNER JOIN ".$intrDB.".intrm".$n."eo".$n."add b ON(b.EO_ID = a.EO_ID AND b.EO_TYPE = a.EO_TYPE) LEFT JOIN ".$intrDB.".intrm".$n."cntry c ON (c.CNTRY_ID = a.EO_CNTRY_ID) LEFT JOIN ".$intrDB.".intrm".$n."eo".$n."org d ON(d.EO_ID = a.EO_ID AND d.EO_TYPE = d.EO_TYPE) SET z.name = a.EO_NM,z.vat_no = a.VAT_NO,z.phone = a.EO_PHONE,z.email = 'Not Available',z.address = b.ADDRESS,z.country = c.CNTRY_DESC,z.father_name = a.FATHER_NAME,z.id_proof = a.ID_PROOF,z.id_proof_no = a.ID_NO,
  z.loyalty_card_id = a.LOYALTY_ID,z.nominees1 = a.NM1,z.dob_nominees1 = a.DOB_NM1,z.nominees2 = a.NM2,z.dob_nominees2 = a.DOB_NM2,z.nominees3 = a.NM3,z.dob_nominees3 = a.DOB_NM3,z.nominees4 = a.NM4,z.dob_nominees4 = a.DOB_NM4,z.nominees5 = a.NM5,z.dob_nominees5 = a.DOB_NM5,z.sync_flg = '1' WHERE a.EO_TYPE = 'C' AND d.UPD_FLG = '1'";
        if ($this->db->query($w)) {
            $w1 = "UPDATE ".$intrDB.".intrm".$n."eo".$n."org a INNER JOIN ".$posDB.".sma_companies b ON a.POS_EO_ID = b.id SET a.UPD_FLG='' WHERE a.POS_EO_ID = b.id";
            $this->db->query($w1);
            return true;
        }
        
        else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }
    
    // S.NO.-07 (Intermediate -> POS)
    
    public function productMaster()
    {
        //c.UOM_BASIC,c.PRICE_SLS
        //$ar = $this->productUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n  = '$';
        
     $w  = "INSERT INTO ".$posDB.".sma_products(code,name,unit,price,category_id,tax_rate,details,warehouse,barcode_symbology,product_details,serialized,org_id,wh_id,grp_id) SELECT a.ITM_ID,b.ITM_DESC,z.uom_desc,b.PRICE_SLS,f.id,1,b.ITM_LONG_DESC,e.id,b.ITM_LEGACY_CODE,b.ITM_LONG_DESC,b.SERIALIZED_FLG,a.ORG_ID,a.WH_ID,b.GRP_ID FROM ".$intrDB.".intrm".$n."prod".$n."org a INNER JOIN ".$intrDB.".intrm".$n."prod b ON (b.ITM_ID = a.ITM_ID) INNER JOIN ".$posDB.".sma_uom_conv_std z ON (z.uom_id = b.UOM_BASIC) INNER JOIN ".$posDB.".sma_categories f ON (f.code = b.GRP_ID) LEFT JOIN ".$posDB.".sma_warehouses e ON (e.org_id = a.ORG_ID AND e.wh_id = a.WH_ID) WHERE a.POS_ITM_ID IS NULL OR a.POS_ITM_ID = ''";        
       //$this->db->query($w); echo "hi.."; die; 
        if ($this->db->query($w)) {
           //return TRUE;
            $msg = "Product Records save successfully..."; 
            //$w2  = "UPDATE ".$intrDB.".intrm".$n."prod".$n."org a INNER JOIN ".$posDB.".sma_products b ON (a.ITM_ID = b.code AND a.ORG_ID = b.org_id AND a.WH_ID = b.wh_id) SET a.POS_ITM_ID= b.id, a.CREATE_FLG='1' WHERE (a.ITM_ID = b.code AND a.ORG_ID = b.org_id AND a.WH_ID = b.wh_id)";
            $w2  = "UPDATE ".$intrDB.".intrm".$n."prod".$n."org a SET a.POS_ITM_ID = 1,a.CREATE_FLG='1' WHERE a.POS_ITM_ID IS NULL OR a.POS_ITM_ID = '' ";
            $this->db->query($w2);
            return $msg;
        }
        else{
              $msg = $this->db->error();
            return $msg['message'];
        }

    
    }
    
    public function productUPD()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        //q.price= c.PRICE_SLS
        $w = "UPDATE ".$posDB.".sma_products q INNER JOIN ".$intrDB.".intrm".$n."prod".$n."org a ON q.id= a.POS_ITM_ID INNER JOIN ".$intrDB.".intrm".$n."prod b ON b.ITM_ID = a.ITM_ID INNER JOIN ".$posDB.".sma_warehouses e ON (e.org_id = a.ORG_ID AND e.wh_id = a.WH_ID) INNER JOIN ".$posDB.".sma_categories f ON f.code = b.GRP_ID SET q.code= a.ITM_ID,q.name= b.ITM_DESC,q.price= b.PRICE_SLS,q.category_id= f.id,q.details= b.ITM_LONG_DESC,q.warehouse= e.id,q.barcode_symbology= b.ITM_LEGACY_CODE,q.product_details= b.ITM_LONG_DESC,q.serialized= b.SERIALIZED_FLG,q.org_id= a.ORG_ID,q.grp_id=b.GRP_ID, q.tupd_flg='1' WHERE a.UPD_FLG='1'";
        if ($this->db->query($w)) {
            
            // $w1 = "UPDATE ".$intrDB.".intrm".$n."prod".$n."org a INNER JOIN ".$posDB.".sma_products b ON a.POS_ITM_ID = b.id SET a.UPD_FLG='' WHERE a.POS_ITM_ID = b.id";
            $w1 = "UPDATE ".$intrDB.".intrm".$n."prod".$n."org a INNER JOIN ".$posDB.".sma_products b ON (a.ITM_ID = b.code AND a.ORG_ID = b.org_id AND a.WH_ID = b.wh_id) SET a.UPD_FLG='' WHERE a.UPD_FLG='1'";
            $this->db->query($w1);
            return true;
        }
        
        else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }
    
    // S.NO.-08 (Intermediate -> POS)
    
    public function productWarehouseMaster()
    {
        $ar = $this->productWarehouseUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $w  = "INSERT INTO ".$posDB.".sma_warehouses_products(product_id,warehouse_id,lot_no,quantity,org_id,wh_id,grp_id) SELECT a.id,a.warehouse,a.lot_no,a.quantity,a.org_id,a.wh_id,a.grp_id FROM ".$posDB.".sma_products a WHERE a.trsfr_flg IS NULL OR a.trsfr_flg = ''";
        if ($this->db->query($w)) {
            $this->purchaseItemMaster();
            $msg = "Product Records save in warehouse successfully...";
            $w1  = "UPDATE ".$posDB.".sma_products a INNER JOIN ".$posDB.".sma_warehouses_products b ON a.id = b.product_id SET a.trsfr_flg= '1' WHERE a.id = b.product_id";
            $this->db->query($w1);
            return $msg;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }
    
    public function productWarehouseUPD()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $w = "UPDATE ".$posDB.".sma_warehouses_products z INNER JOIN ".$posDB.".sma_products a SET z.product_id = a.id,z.warehouse_id = a.warehouse,z.lot_no = a.lot_no,z.quantity = a.quantity,z.org_id = a.org_id,z.grp_id = a.grp_id WHERE a.tupd_flg= '1'";
       //$this->db->query($w); 
	if (1) {
            $this->purchaseItemUPD();
            $w1 = "UPDATE ".$posDB.".sma_products a INNER JOIN ".$posDB.".sma_warehouses_products b ON a.id = b.product_id SET a.tupd_flg='' WHERE a.id = b.product_id";
            $this->db->query($w1);
            return true;
        } else {
            
            $msg = $this->db->error();
            return $msg['message'];
        }
    }
    
    public function purchaseItemMaster()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        //$w = "INSERT INTO ".$posDB.".sma_purchase_items(product_id,product_code,product_name,itm_desc,net_unit_cost,quantity,warehouse_id,item_tax,tax_rate_id,tax,subtotal,quantity_balance,date,status,unit_cost,lot_no,sr_no,org_id,wh_id,grp_id,category_id,is_serialized,image,tax_rate,tax_method) SELECT a.id,a.code,a.name,a.product_details,(a.price -((a.price * b.rate) /(100 + b.rate))) AS unit_cost,a.quantity,a.warehouse,((a.price * b.rate) /(100 + b.rate)) AS item_tax,a.tax_rate,b.rate,a.price,a.quantity,DATE('Y-m-d'),'received',a.price,a.lot_no,a.sr_no,a.org_id,a.wh_id,a.grp_id,a.category_id,a.serialized,a.image,a.tax_rate,a.tax_method FROM ".$posDB.".sma_products a INNER JOIN ".$posDB.".sma_tax_rates b ON(b.id = a.tax_rate AND b.org_id = a.org_id AND b.wh_id = a.wh_id AND b.grp_id = a.grp_id) WHERE a.trsfr_flg IS NULL OR a.trsfr_flg = ''";
        $w = "INSERT INTO ".$posDB.".sma_purchase_items(product_id,product_code,product_name,itm_desc,net_unit_cost,quantity,warehouse_id,item_tax,tax_rate_id,tax,subtotal,quantity_balance,date,status,unit_cost,lot_no,sr_no,org_id,wh_id,grp_id,category_id,is_serialized,image,tax_rate,tax_method) SELECT a.id,a.code,a.name,a.product_details,(a.price -((a.price * b.rate) /(100 + b.rate))) AS unit_cost,a.quantity,a.warehouse,((a.price * b.rate) /(100 + b.rate)) AS item_tax,a.tax_rate,b.rate,a.price,a.quantity,DATE('Y-m-d'),'received',a.price,a.lot_no,a.sr_no,a.org_id,a.wh_id,a.grp_id,a.category_id,a.serialized,a.image,a.tax_rate,a.tax_method FROM ".$posDB.".sma_products a INNER JOIN ".$posDB.".sma_tax_rates b ON(b.id = a.tax_rate) WHERE a.trsfr_flg IS NULL OR a.trsfr_flg = ''";
        
        if ($this->db->query($w)) {
            return true;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
        
    }
    public function purchaseItemUPD()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        //$w = "UPDATE ".$posDB.".sma_purchase_items z INNER JOIN ".$posDB.".sma_products a ON (a.id = z.product_id) INNER JOIN ".$posDB.".sma_tax_rates b ON(b.id = a.tax_rate AND b.org_id = a.org_id AND b.grp_id = a.grp_id) SET z.product_code = a.code, z.product_name = a.name, z.net_unit_cost =(a.price -((a.price * b.rate) /(100 + b.rate))), z.quantity = a.quantity, z.item_tax =((a.price * b.rate) /(100 + b.rate)), z.tax_rate_id = a.tax_rate, z.tax = b.rate, z.subtotal = a.price, z.quantity_balance = a.quantity, z.lot_no = a.lot_no, z.sr_no = a.sr_no, z.org_id = a.org_id, z.wh_id = a.wh_id, z.grp_id = a.grp_id,z.category_id = a.category_id,z.itm_desc = a.details WHERE a.tupd_flg = '1'";
        $w = "UPDATE ".$posDB.".sma_purchase_items z INNER JOIN ".$posDB.".sma_products a ON (a.id = z.product_id) INNER JOIN ".$posDB.".sma_tax_rates b ON(b.id = a.tax_rate) SET z.product_code = a.code, z.product_name = a.name, z.net_unit_cost =(a.price -((a.price * b.rate) /(100 + b.rate))), z.quantity = a.quantity, z.item_tax =((a.price * b.rate) /(100 + b.rate)), z.tax_rate_id = a.tax_rate, z.tax = b.rate, z.subtotal = a.price, z.quantity_balance = a.quantity, z.lot_no = a.lot_no, z.sr_no = a.sr_no, z.org_id = a.org_id, z.wh_id = a.wh_id, z.grp_id = a.grp_id,z.category_id = a.category_id,z.itm_desc = a.details WHERE a.tupd_flg = '1'";
        
        if ($this->db->query($w)) {
            return true;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }

    /////////////////////////////
    public function binMaster(){
        $this->binUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_bin(org_id,segment_id,warehouse_id,wh_id,bin_id,bin_nm,bin_desc,storage_type,blocked,blk_resn,blk_dt_frm,blk_dt_to,bin_ent_id,create_date) SELECT a.ORG_ID,b.segment_id,b.id,a.WH_ID,a.BIN_ID,a.BIN_NM,a.BIN_DESC,a.STORAGE_TYPE,a.BLOCKED,a.BLK_RESN,a.BLK_DT_FRM,a.BLK_DT_TO,a.BIN_ENT_ID,a.USR_ID_CREATE_DT FROM ".$intrDB.".intrm".$n."bin a INNER JOIN ".$posDB.".sma_warehouses b ON (a.WH_ID = b.wh_id AND a.ORG_ID = b.org_id) WHERE a.POS_BIN_ID IS NULL OR a.POS_BIN_ID = ''";
        if ($this->db->query($w)) {
            $msg = "Bin Records save successfully...";
            $w1  = "UPDATE ".$intrDB.".intrm".$n."bin a INNER JOIN ".$posDB.".sma_bin b ON (a.WH_ID = b.wh_id AND a.ORG_ID = b.org_id) SET a.POS_BIN_ID= b.id WHERE (a.WH_ID = b.wh_id AND a.ORG_ID = b.org_id)";
            $this->db->query($w1);
            return $msg;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }

    }

    public function binUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_bin a INNER JOIN ".$intrDB.".intrm".$n."bin b ON (b.POS_BIN_ID = a.id) SET a.bin_nm = b.BIN_NM,a.bin_desc = b.BIN_DESC,a.storage_type= b.STORAGE_TYPE,a.blocked=b.BLOCKED,a.blk_resn =b.BLK_RESN,a.blk_dt_frm=b.BLK_DT_FRM,a.blk_dt_to=b.BLK_DT_TO,a.bin_ent_id = b.BIN_ENT_ID WHERE b.UPD_FLG = '3' AND (b.POS_BIN_ID = a.id)";
        if($this->db->query($w)){
            $w1 = "UPDATE ".$intrDB.".intrm".$n."bin a INNER JOIN ".$posDB.".sma_bin b ON (a.POS_BIN_ID = b.id) SET a.UPD_FLG = '' WHERE (a.POS_BIN_ID = b.id) AND a.UPD_FLG = '3'";
            if($this->db->query($w)){
                   $msg = "Bin Records Update successfully...";
                   return true;

            } else {
               $msg = $this->db->error();
               return $msg['message'];
       }

        } else {
            $msg = $this->db->error();
            return $msg['message'];
       }

    }

    /////////////////////////////
    public function poMaster(){
        $this->poMasterUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_drft_po(org_id,po_id,po_dt,customer_id,bill_adds_id,tlrnc_days,curr_id_sp,curr_name,curr_conv_fctr,tlrnc_qty_type,tlrnc_qty_val,usr_id_create_dt,auth_po_no,fy_id,po_status,po_mode,doc_id) SELECT a.ORG_ID,a.PO_ID,a.PO_DT,b.id,a.BILL_ADDS_ID,a.TLRNC_DAYS,a.CURR_ID_SP,c.name,a.CURR_CONV_FCTR,a.TLRNC_QTY_TYPE,a.TLRNC_QTY_VAL,a.USR_ID_CREATE_DT,a.AUTH_PO_NO,a.FY_ID,a.PO_STATUS,a.PO_MODE,a.DOC_ID FROM ".$intrDB.".intrm".$n."drft".$n."po a INNER JOIN ".$posDB.".sma_companies b ON (b.eo_id = a.EO_ID) INNER JOIN ".$posDB.".sma_currencies c ON (c.curr_id = a.CURR_ID_SP) WHERE a.POS_PO_ID IS NULL OR a.POS_PO_ID = '' AND b.eo_type='S'";
      if($this->db->query($w)){
          $msg = "PO Records save successfully...";
          $w1= "UPDATE ".$intrDB.".intrm".$n."drft".$n."po a INNER JOIN ".$posDB.".sma_drft_po b ON(a.PO_ID = b.po_id) SET a.POS_PO_ID = b.id WHERE (a.PO_ID = b.po_id)";
          $this->db->query($w1);
            return $msg;
  } else {
          $msg = $this->db->error();
            return $msg['message'];
   }

    }

    public function poMasterUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$posDB.".sma_drft_po m INNER JOIN ".$intrDB.".intrm".$n."drft".$n."po a ON (m.id = a.POS_PO_ID) INNER JOIN ".$posDB.".sma_companies b ON (b.eo_id = a.EO_ID) INNER JOIN ".$posDB.".sma_currencies c ON (c.curr_id = a.CURR_ID_SP) SET m.po_id = a.PO_ID,m.po_dt = a.PO_DT,m.customer_id = b.id,m.bill_adds_id=a.BILL_ADDS_ID,m.tlrnc_days=a.TLRNC_DAYS,m.curr_id_sp=a.CURR_ID_SP,m.curr_name=c.name,m.curr_conv_fctr=a.CURR_CONV_FCTR,m.tlrnc_qty_type=a.TLRNC_QTY_TYPE,m.tlrnc_qty_val=a.TLRNC_QTY_VAL,m.auth_po_no=a.AUTH_PO_NO,m.fy_id = a.FY_ID,m.po_status = a.PO_STATUS,m.po_mode=a.PO_MODE,m.doc_id=a.DOC_ID WHERE a.UPD_FLG='3' AND b.eo_type='S'";
      if($this->db->query($w)){
          $msg = "Update PO Records save successfully...";
          $w1= "UPDATE ".$intrDB.".intrm".$n."drft".$n."po a INNER JOIN ".$posDB.".sma_drft_po b ON(a.POS_PO_ID = b.id) SET a.UPD_FLG = '' WHERE (a.POS_PO_ID = b.id)";
          $this->db->query($w1);
            return TRUE;
  } else {
          $msg = $this->db->error();
            return $msg['message'];
   }

    }

/////////////////////////////////

    public function poSchdlMaster(){
        $this->poSchdlMasterUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        // a.ITM_UOM_DESC change a.ITM_UOM
        $w= "INSERT INTO ".$posDB.".sma_drft_po_dlv_schdl(org_id,po_id,itm_id,itm_name,tot_qty,dlv_mode,dlv_qty,tlrnc_qty_type,tlrnc_qty_val,dlv_dt,wh_id,dlv_adds_id,usr_id_create_dt,dlv_schdl_no,itm_uom_desc,bal_qty,tmp_rcpt_qty, tlrnc_days_val,segment_id,warehouse_id,prj_id,ro_no,ro_dt,item_id,use_rented_wh) SELECT a.ORG_ID,b.id,f.id,f.name,a.TOT_QTY,a.DLV_MODE,a.DLV_QTY,a.TLRNC_QTY_TYPE,a.TLRNC_QTY_VAL,a.DLV_DT,a.WH_ID,a.DLV_ADDS_ID,a.USR_ID_CREATE_DT,a.DLV_SCHDL_NO,a.ITM_UOM,a.BAL_QTY,a.TMP_RCPT_QTY,a.TLRNC_DAYS_VAL,z.id,c.id,a.PRJ_ID,a.RO_NO,a.RO_DT,a.ITM_ID, CASE WHEN a.USE_RENT_WH = 'Y' THEN '1' ELSE '0' END AS rentwh FROM ".$intrDB.".intrm".$n."drft".$n."po".$n."dlv".$n."schdl a INNER JOIN ".$posDB.".sma_drft_po b ON ((a.ORG_ID = b.org_id) AND (b.doc_id = a.DOC_ID)) INNER JOIN ".$posDB.".sma_warehouses c ON (c.org_id = a.ORG_ID AND c.wh_id = a.WH_ID) INNER JOIN ".$posDB.".sma_segment z ON (z.prj_doc_id = a.PRJ_ID AND z.org_id = a.ORG_ID) INNER JOIN ".$posDB.".sma_products f ON (f.org_id = a.ORG_ID AND f.wh_id = a.WH_ID AND f.code = a.ITM_ID) WHERE a.POS_PO_SCHDL_ID IS NULL OR a.POS_PO_SCHDL_ID = ''";
         if($this->db->query($w)){
          $msg = "PO schdl Records save successfully...";
           $w1= "UPDATE ".$intrDB.".intrm".$n."drft".$n."po".$n."dlv".$n."schdl a INNER JOIN ".$posDB.".sma_drft_po_dlv_schdl b ON ((a.ORG_ID = b.org_id) AND (a.ITM_ID = b.item_id) AND (a.TOT_QTY = b.tot_qty) AND (a.RO_NO = b.ro_no)) SET a.POS_PO_SCHDL_ID = b.id WHERE ((a.ORG_ID = b.org_id) AND (a.ITM_ID = b.item_id) AND (a.TOT_QTY = b.tot_qty) AND (a.RO_NO = b.ro_no))";
           $this->db->query($w1);
            return $msg;
  } else {
          $msg = $this->db->error();
            return $msg['message'];
   }
    }

    public function poSchdlMasterUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        // a.ITM_UOM_DESC change a.ITM_UOM
        $w= "UPDATE ".$posDB.".sma_drft_po_dlv_schdl m INNER JOIN ".$intrDB.".intrm".$n."drft".$n."po".$n."dlv".$n."schdl a ON (m.id = a.POS_PO_SCHDL_ID) INNER JOIN ".$posDB.".sma_drft_po b ON ((a.ORG_ID = b.org_id) AND (b.doc_id = a.DOC_ID)) INNER JOIN ".$posDB.".sma_warehouses c ON (c.org_id = a.ORG_ID AND c.wh_id = a.WH_ID) INNER JOIN ".$posDB.".sma_segment z ON (z.prj_doc_id = a.PRJ_ID AND z.org_id = a.ORG_ID) INNER JOIN ".$posDB.".sma_products f ON (f.org_id = a.ORG_ID AND f.wh_id = a.WH_ID AND f.code = a.ITM_ID) SET m.po_id=b.id,m.itm_id=f.id,m.itm_name=f.name,m.tot_qty=a.TOT_QTY,m.dlv_mode=a.DLV_MODE,m.dlv_qty=a.DLV_QTY,m.tlrnc_qty_type=a.TLRNC_QTY_TYPE,m.tlrnc_qty_val=a.TLRNC_QTY_VAL,m.dlv_dt=a.DLV_DT,m.wh_id=a.WH_ID,m.dlv_adds_id=a.DLV_ADDS_ID,m.dlv_schdl_no=a.DLV_SCHDL_NO,m.itm_uom_desc=a.ITM_UOM,m.bal_qty=a.BAL_QTY,m.tmp_rcpt_qty=a.TMP_RCPT_QTY, m.tlrnc_days_val=a.TLRNC_DAYS_VAL,m.segment_id=z.id,m.warehouse_id=c.id,m.prj_id=a.PRJ_ID,m.ro_no=a.RO_NO,m.ro_dt=a.RO_DT,m.item_id=a.ITM_ID WHERE a.UPD_FLG='3'";
         if($this->db->query($w)){
          $msg = "Update PO schdl Records save successfully...";
           $w1= "UPDATE ".$intrDB.".intrm".$n."drft".$n."po".$n."dlv".$n."schdl a INNER JOIN ".$posDB.".sma_drft_po_dlv_schdl b ON ((a.ORG_ID = b.org_id) AND (a.ITM_ID = b.item_id) AND (a.TOT_QTY = b.tot_qty) AND (a.RO_NO = b.ro_no)) SET a.UPD_FLG = '' WHERE ((a.ORG_ID = b.org_id) AND (a.ITM_ID = b.item_id) AND (a.TOT_QTY = b.tot_qty) AND (a.RO_NO = b.ro_no) ) AND a.UPD_FLG='3'";
           $this->db->query($w1);
            return TRUE;
  } else {
          $msg = $this->db->error();
            return $msg['message'];
   }
    }

    /////////////////////////////////

    public function dsAttMaster(){
        $this->dsAttUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_ds_att(att_id,att_name,att_type_id,att_actv) SELECT a.ATT_ID,a.ATT_NM,a.ATT_TYPE_ID,a.ATT_ACTV FROM ".$intrDB.".intrm".$n."ds".$n."att a WHERE a.POS_ATT_ID IS NULL OR a.POS_ATT_ID = ''";
        if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."ds".$n."att a INNER JOIN ".$posDB.".sma_ds_att b ON (a.ATT_ID = b.att_id) SET a.POS_ATT_ID = b.id,a.SYNC_FLG='1' WHERE (a.ATT_ID = b.att_id)";
             if($this->db->query($w1))
             {
                $msg = "DS ATT Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    public function dsAttUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_ds_att a INNER JOIN ".$intrDB.".intrm".$n."ds".$n."att b ON (b.POS_ATT_ID = a.id) SET a.att_id = b.ATT_ID, a.att_name = b.ATT_NM, a.att_type_id = b.ATT_TYPE_ID, a.att_actv= b.ATT_ACTV WHERE b.SYNC_FLG = '3' AND (b.POS_ATT_ID = a.id)";
        if($this->db->query($w)){
            $w1 = "UPDATE ".$intrDB.".intrm".$n."ds".$n."att a INNER JOIN ".$posDB.".sma_ds_att b ON (a.POS_ATT_ID = b.id) SET a.SYNC_FLG = '' WHERE (a.POS_ATT_ID = b.id) AND a.SYNC_FLG = '3'";
            if($this->db->query($w)){
                   $msg = "DS ATT Records Update successfully...";
                   return true;

            } else {
               $msg = $this->db->error();
               return $msg['message'];
       }

        } else {
            $msg = $this->db->error();
            return $msg['message'];
       }

    }

    /////////////////////////////////

    public function dsAttRelnMaster(){
        $this->dsAttRelnUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_ds_att_reln(att_id,att_type_id,att_id_reln, att_type_id_reln,actv) SELECT a.ATT_ID,a.ATT_TYPE_ID,a.ATT_ID_RELN,a.ATT_TYPE_ID_RELN,a.ACTV FROM ".$intrDB.".intrm".$n."ds".$n."att".$n."reln a WHERE a.POS_ATT_ID_RELN IS NULL OR a.POS_ATT_ID_RELN = ''";
        if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."ds".$n."att".$n."reln a INNER JOIN ".$posDB.".sma_ds_att_reln b ON (a.ATT_ID = b.att_id AND a.ATT_ID_RELN = b.att_id_reln) SET a.POS_ATT_ID_RELN = b.id,a.SYNC_FLG='1' WHERE (a.ATT_ID = b.att_id AND a.ATT_ID_RELN = b.att_id_reln)";
             if($this->db->query($w1))
             {
                $msg = "DS ATT RELN Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    public function dsAttRelnUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_ds_att_reln a INNER JOIN ".$intrDB.".intrm".$n."ds".$n."att".$n."reln b ON (b.POS_ATT_ID_RELN = a.id) SET a.att_id = b.ATT_ID, a.att_type_id = b.ATT_TYPE_ID, a.att_id_reln= b.ATT_ID_RELN, a.att_type_id_reln = b.ATT_TYPE_ID_RELN,a.actv = b.ACTV WHERE b.SYNC_FLG = '3' AND (b.POS_ATT_ID_RELN = a.id)";
        if($this->db->query($w)){
            $w1 = "UPDATE ".$intrDB.".intrm".$n."ds".$n."att".$n."reln a INNER JOIN ".$posDB.".sma_ds_att_reln b ON (a.POS_ATT_ID_RELN = b.id) SET a.SYNC_FLG = '' WHERE (a.POS_ATT_ID_RELN = b.id) AND a.SYNC_FLG = '3'";
            if($this->db->query($w)){
                   $msg = "DS ATT RELN Records Update successfully...";
                   return true;

            } else {
               $msg = $this->db->error();
               return $msg['message'];
       }

        } else {
            $msg = $this->db->error();
            return $msg['message'];
       }

    }

    /////////////////////////////////

    public function dsAttTypeMaster(){
        $this->dsAttTypeUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_ds_att_type(att_type_id,att_type_name,att_type_actv) SELECT a.ATT_TYPE_ID,a.ATT_TYPE_NM,a.ATT_TYPE_ACTV FROM ".$intrDB.".intrm".$n."ds".$n."att".$n."type a WHERE a.POS_ATT_TYPE_ID IS NULL OR a.POS_ATT_TYPE_ID = ''";
        if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."ds".$n."att".$n."type a INNER JOIN ".$posDB.".sma_ds_att_type b ON (a.ATT_TYPE_ID = b.att_type_id) SET a.POS_ATT_TYPE_ID = b.id,a.SYNC_FLG='1' WHERE (a.ATT_TYPE_ID = b.att_type_id)";
             if($this->db->query($w1))
             {
                $msg = "DS ATT TYPE Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    public function dsAttTypeUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_ds_att_type a INNER JOIN ".$intrDB.".intrm".$n."ds".$n."att".$n."type b ON (b.POS_ATT_TYPE_ID = a.id) SET a.att_type_id = b.ATT_TYPE_ID, a.att_type_name = b.ATT_TYPE_NM, a.att_type_actv = b.ATT_TYPE_ACTV  WHERE b.SYNC_FLG = '3' AND (b.POS_ATT_TYPE_ID = a.id)";
        if($this->db->query($w)){
            $w1 = "UPDATE ".$intrDB.".intrm".$n."ds".$n."att".$n."type a INNER JOIN ".$posDB.".sma_ds_att_type b ON (a.POS_ATT_TYPE_ID = b.id) SET a.SYNC_FLG = '' WHERE (a.POS_ATT_TYPE_ID = b.id) AND a.SYNC_FLG = '3'";
            if($this->db->query($w)){
                   $msg = "DS ATT Type Records Update successfully...";
                   return true;

            } else {
               $msg = $this->db->error();
               return $msg['message'];
       }

        } else {
            $msg = $this->db->error();
            return $msg['message'];
       }

    }

    ////////////////////////////////////////////

   public function uomClsMaster(){
        $this->uomClsUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_uom_cls(uom_class_id,uom_class_name,usr_id_create,usr_id_create_date) SELECT a. UOM_CLASS_ID,a.UOM_CLASS_NM,a.USR_ID_CREATE,DATE_FORMAT(a.USR_ID_CREATE_DT,'%Y-%m-%d') FROM ".$intrDB.".intrm".$n."uom".$n."cls a WHERE a.POS_UOM_CLASS_ID IS NULL OR a.POS_UOM_CLASS_ID = ''";
        if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."uom".$n."cls a INNER JOIN ".$posDB.".sma_uom_cls b ON (a.UOM_CLASS_ID = b.uom_class_id) SET a.POS_UOM_CLASS_ID = b.id,a.SYNC_FLG='1' WHERE (a.UOM_CLASS_ID = b.uom_class_id)";
             if($this->db->query($w1))
             {
                $msg = "UOM CLASS Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    public function uomClsUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_uom_cls a INNER JOIN ".$intrDB.".intrm".$n."uom".$n."cls b ON (b.POS_UOM_CLASS_ID = a.id) SET a.uom_class_id = b.UOM_CLASS_ID,a.uom_class_name = b.UOM_CLASS_NM WHERE b.SYNC_FLG = '3' AND (b.POS_UOM_CLASS_ID = a.id)";
        if($this->db->query($w)){
            $w1 = "UPDATE ".$intrDB.".intrm".$n."uom".$n."cls a INNER JOIN ".$posDB.".sma_uom_cls b ON (a.POS_UOM_CLASS_ID = b.id) SET a.SYNC_FLG = '' WHERE (a.POS_UOM_CLASS_ID = b.id) AND a.SYNC_FLG = '3'";
            if($this->db->query($w)){
                   $msg = "UOM CLS Records Update successfully...";
                   return true;

            } else {
               $msg = $this->db->error();
               return $msg['message'];
       }

        } else {
            $msg = $this->db->error();
            return $msg['message'];
       }

    }

    ////////////////////////////////////////////

   public function uomConStdvMaster(){
        $this->uomConStdvUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_uom_conv_std(uom_id,uom_name,uom_desc,uom_class,base_uom,conv_fctr,actv,inactv_resn,inactv_date,uom_std_ent_id,usr_id_create,usr_id_create_dt) SELECT a.UOM_ID,a.UOM_NM,a.UOM_DESC,a.UOM_CLASS,a.BASE_UOM,a.CONV_FCTR,a.ACTV,a.INACTV_RESN,DATE_FORMAT(a.INACTV_DT,'%Y-%m-%d'),a.UOM_STD_ENT_ID,a.USR_ID_CREATE,DATE_FORMAT(a.USR_ID_CREATE_DT,'%Y-%m-%d') FROM ".$intrDB.".intrm".$n."uom".$n."conv".$n."std a WHERE a.POS_UOM_ID IS NULL OR a.POS_UOM_ID = ''";
        if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."uom".$n."conv".$n."std a INNER JOIN ".$posDB.".sma_uom_conv_std b ON (a.UOM_ID = b.uom_id) SET a.POS_UOM_ID = b.id,a.SYNC_FLG='1' WHERE (a.UOM_ID = b.uom_id)";
             if($this->db->query($w1))
             {
                $msg = "UOM CONV STD Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    public function uomConStdvUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_uom_conv_std a INNER JOIN ".$intrDB.".intrm".$n."uom".$n."conv".$n."std b ON (b.POS_UOM_ID = a.id) SET a.uom_name = b.UOM_NM,a.uom_desc=b.UOM_DESC,a.uom_class=b.UOM_CLASS,a.base_uom=b.BASE_UOM, a.conv_fctr=b.CONV_FCTR,a.actv= b.ACTV,a.inactv_resn=b.INACTV_RESN,a.inactv_date=DATE_FORMAT(b.INACTV_DT,'%Y-%m-%d'),a.uom_std_ent_id=b.UOM_STD_ENT_ID WHERE b.SYNC_FLG = '3' AND (b.POS_UOM_ID = a.id)";
        if($this->db->query($w)){
            $w1 = "UPDATE ".$intrDB.".intrm".$n."uom".$n."conv".$n."std a INNER JOIN ".$posDB.".sma_uom_conv_std b ON (a.POS_UOM_ID = b.id) SET a.SYNC_FLG = '' WHERE (a.POS_UOM_ID = b.id) AND a.SYNC_FLG = '3'";
            if($this->db->query($w)){
                   $msg = "UOM CLS Records Update successfully...";
                   return true;

            } else {
               $msg = $this->db->error();
               return $msg['message'];
       }

        } else {
            $msg = $this->db->error();
            return $msg['message'];
       }

    }


    ////////////////////////////////////////////

   public function segmentMaster(){
        $ar = $this->segmentMasterUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_segment(org_id,prj_doc_id,parent_proj_id,proj_name,prj_actv) SELECT a.ORG_ID,a.PRJ_DOC_ID,a.PARENT_PROJ_ID,a.PROJ_NM,a.PRJ_ACTV FROM ".$intrDB.".intrm".$n."segment a WHERE a.POS_PRJ_ID IS NULL OR a.POS_PRJ_ID = ''";
        if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."segment a INNER JOIN ".$posDB.".sma_segment b ON ((a.PRJ_DOC_ID = b.prj_doc_id) AND (a.PARENT_PROJ_ID = b.parent_proj_id) AND (a.ORG_ID = b.org_id)) SET a.POS_PRJ_ID = b.id,a.SYNC_FLG='1' WHERE ((a.PRJ_DOC_ID = b.prj_doc_id) AND (a.PARENT_PROJ_ID = b.parent_proj_id) AND (a.ORG_ID = b.org_id))";
             if($this->db->query($w1))
             {
                $msg = "Segment Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    public function segmentMasterUPD()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_segment a INNER JOIN ".$intrDB.".intrm".$n."segment b ON (a.id = b.POS_PRJ_ID) SET a.org_id = b.ORG_ID,a.prj_doc_id = b.PRJ_DOC_ID,a.parent_proj_id = b.PARENT_PROJ_ID,a.proj_name = b.PROJ_NM,a.prj_actv= b.PRJ_ACTV WHERE b.SYNC_FLG= '3'";
        if ($this->db->query($w)) {
            $w1 = "UPDATE ".$intrDB.".intrm".$n."segment a INNER JOIN ".$posDB.".sma_segment b ON (b.id = a.POS_PRJ_ID) SET a.SYNC_FLG = '' WHERE (b.id = a.POS_PRJ_ID) AND a.SYNC_FLG= '3'";
            $this->db->query($w1);
            return true;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }

    }


   ////////////////////////////////////////////

  /* public function segmentEntMaster(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_segment_ent(org_id,prj_doc_id,prj_ent_type_id,prj_ent_id) SELECT a.ORG_ID,a.PRJ_DOC_ID,a.PRJ_ENT_TYP_ID,a.PRJ_ENT_ID FROM ".$intrDB.".intrm".$n."segment".$n."ent a WHERE a.POS_PRJ_ID IS NULL OR a.POS_PRJ_ID = ''";
        if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."segment".$n."ent a INNER JOIN ".$posDB.".sma_segment_ent b ON ((a.PRJ_DOC_ID = b.prj_doc_id) AND (a.PRJ_ENT_ID = b.prj_ent_id) AND (a.ORG_ID = b.org_id)) SET a.POS_PRJ_ID = b.id,a.SYNC_FLG='1' WHERE ((a.PRJ_DOC_ID = b.prj_doc_id) AND (a.PRJ_ENT_ID = b.prj_ent_id) AND (a.ORG_ID = b.org_id))";
             if($this->db->query($w1))
             {
                $msg = "Segment Ent Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    } */

    ////////////////////////////////////////////

   public function whORGMaster(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_wh_org(org_id,wh_id,usr_id_create,usr_id_create_dt,wh_name,wh_onrshp_type,wh_strg_type,adds_id,wh_desc,actv,inactv_resn,inactv_dt,wh_ent_id,prj_id,segment_id) SELECT a.ORG_ID,a.WH_ID,a.USR_ID_CREATE,DATE_FORMAT(a.USR_ID_CREATE_DT,'%Y-%m-%d'),a.WH_NM,a.WH_ONRSHP_TYPE,a.WH_STRG_TYPE,a.ADDS_ID,a.WH_DESC,a.ACTV,a.INACTV_RESN,DATE_FORMAT(a.INACTV_DT,'%Y-%m-%d'),a.WH_ENT_ID,a.PRJ_ID,z.id FROM ".$intrDB.".intrm".$n."wh".$n."org a INNER JOIN ".$posDB.".sma_segment z ON (a.PRJ_ID = z.prj_doc_id) ";
        if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."wh".$n."org a INNER JOIN ".$posDB.".sma_wh_org b ON ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id)) SET a.POS_WH_ID = b.id,a.SYNC_FLG='1' WHERE ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id))";
             if($this->db->query($w1))
             {
                $msg = "Warehouse ORG Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    ////////////////////////////////////////////

    public function stockItmMaster(){  
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        //$w= "INSERT INTO ".$posDB.".sma_item_stk_profile(org_id,segment_id,warehouse_id,wh_id,fy_id,item_id,item_uom_id,total_stk,available_stk,rejected_stk,rework_stk,ordered_stk,purchase_price,sales_price,created_date,code) SELECT a.ORG_ID,z.segment_id,z.id,a.WH_ID,a.FY_ID,b.id,q.uom_desc,a.TOT_STK,a.AVL_STK,a.REJ_STK,a.RWK_STK,a.ORD_STK,(b.price -((b.price * m.rate) /(100 + m.rate))),b.price,DATE_FORMAT(a.USR_ID_MOD_DT,'%Y-%m-%d'),a.ITM_ID FROM ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."itm a INNER JOIN ".$posDB.".sma_products b ON ((a.ITM_ID = b.code) AND (a.ORG_ID = b.org_id) AND (a.WH_ID = b.wh_id)) INNER JOIN ".$posDB.".sma_warehouses z ON ((a.WH_ID = z.wh_id) AND (a.ORG_ID = z.org_id)) INNER JOIN ".$posDB.".sma_uom_conv_std q ON (a.ITM_UOM_BS = q.uom_id) INNER JOIN ".$posDB.".sma_tax_rates m ON(m.id = b.tax_rate AND m.org_id = b.org_id AND m.wh_id = b.wh_id AND m.grp_id = b.grp_id) WHERE a.POS_STK_ITM_ID IS NULL OR a.POS_STK_ITM_ID = ''";
        $w= "INSERT INTO ".$posDB.".sma_item_stk_profile(org_id,segment_id,warehouse_id,wh_id,fy_id,item_id,item_uom_id,total_stk,available_stk,rejected_stk,rework_stk,ordered_stk,sales_price,created_date,code) SELECT a.ORG_ID,z.segment_id,z.id,a.WH_ID,a.FY_ID,b.id,q.uom_desc,a.TOT_STK,a.AVL_STK,a.REJ_STK,a.RWK_STK,a.ORD_STK,b.price,DATE_FORMAT(a.USR_ID_MOD_DT,'%Y-%m-%d'),a.ITM_ID FROM ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."itm a INNER JOIN ".$posDB.".sma_products b ON ((a.ITM_ID = b.code) AND (a.ORG_ID = b.org_id) AND (a.WH_ID = b.wh_id)) INNER JOIN ".$posDB.".sma_warehouses z ON ((a.WH_ID = z.wh_id) AND (a.ORG_ID = z.org_id)) INNER JOIN ".$posDB.".sma_uom_conv_std q ON (a.ITM_UOM_BS = q.uom_id) WHERE (a.POS_STK_ITM_ID IS NULL OR a.POS_STK_ITM_ID = '')";
      //  echo $w;exit;
      //$this->db->query($w);
         //return true;
        
        if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."itm a INNER JOIN ".$posDB.".sma_item_stk_profile b ON ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id) AND (a.ITM_ID = b.code)) SET a.POS_STK_ITM_ID = b.id WHERE ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id) AND (a.ITM_ID = b.code))";
             if($this->db->query($w1))
             {
                $msg = "Stock Item Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }


    ////////////////////////////////////////////

     public function stockLotMaster(){
       $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        // INSERT INTO iffco_ebiz_loc.sma_item_stk_lot(org_id,segment_id,warehouse_id,wh_id,lot_no,item_stk_id,item_id,batch_no, itm_uom_id,total_stk,rejected_stk,mfg_date,exp_date,reworkable_stk,created_date,code) SELECT a.ORG_ID,d.segment_id,d.warehouse_id,a.WH_ID,a.LOT_ID,d.id,d.item_id,a.BATCH_NO,d.item_uom_id,a.TOT_STK,a.REJ_STK,DATE_FORMAT(a.MFG_DT,'%Y-%m-%d'),DATE_FORMAT(a.EXPIRY_DT,'%Y-%m-%d'),a.RWK_STK,DATE_FORMAT(a.LOT_DT,'%Y-%m-%d'),a.ITM_ID FROM pos_intermediate_10.intrm$mm$stk$summ$lot a INNER JOIN iffco_ebiz_loc.sma_item_stk_profile d ON (a.ITM_ID = d.code AND a.ORG_ID = d.org_id AND a.WH_ID = d.wh_id) WHERE a.POS_STK_LOT_ID IS NULL OR a.POS_STK_LOT_ID = ''

       $w = "INSERT INTO ".$posDB.".sma_item_stk_lot(org_id,segment_id,warehouse_id,wh_id,lot_no,item_stk_id,item_id,batch_no, itm_uom_id,total_stk,rejected_stk,mfg_date,exp_date,reworkable_stk,created_date,code) SELECT a.ORG_ID,d.segment_id,d.warehouse_id,a.WH_ID,a.LOT_ID,d.id,d.item_id,a.BATCH_NO,d.item_uom_id,a.TOT_STK,a.REJ_STK,DATE_FORMAT(a.MFG_DT,'%Y-%m-%d'),DATE_FORMAT(a.EXPIRY_DT,'%Y-%m-%d'),a.RWK_STK,DATE_FORMAT(a.LOT_DT,'%Y-%m-%d'),a.ITM_ID FROM ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."lot a INNER JOIN ".$posDB.".sma_item_stk_profile d ON (a.ITM_ID = d.code AND a.ORG_ID = d.org_id AND a.WH_ID = d.wh_id) WHERE a.POS_STK_LOT_ID IS NULL OR a.POS_STK_LOT_ID = ''";
      //  echo $w;exit;
        //$this->db->query($w);
        //return true;
        if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."lot a INNER JOIN ".$posDB.".sma_item_stk_lot b ON ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id) AND (a.ITM_ID = b.code) AND (a.TOT_STK = b.total_stk)) SET a.POS_STK_LOT_ID = b.id WHERE ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id) AND (a.ITM_ID = b.code) AND (a.TOT_STK = b.total_stk))";
             if($this->db->query($w1))
             {
                $msg = "Stock Bin Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    } 


    ////////////////////////////////////////////

     public function stockLotBinMaster(){
        //$ar = $this->updstockLotBinMaster();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        // INSERT INTO iffco_ebiz_loc.sma_item_stk_lot_bin(org_id,segment_id,warehouse_id,wh_id,item_id,lot_id,bin_id,itm_uom_id,total_stk,rejected_stk,reworkable_stk,created_date,code) SELECT a.ORG_ID,k.segment_id,k.warehouse_id,a.WH_ID,k.item_id,k.id,c.id,k.itm_uom_id,a.TOT_STK,a.REJ_STK,a.RWK_STK,DATE_FORMAT(a.USR_ID_MOD_DT,'%Y-%m-%d'),a.ITM_ID FROM pos_intermediate_10.intrm$mm$stk$summ$bin a INNER JOIN iffco_ebiz_loc.sma_bin c ON ((a.BIN_ID = c.bin_id) AND (a.ORG_ID = c.org_id) AND (a.WH_ID = c.wh_id)) INNER JOIN iffco_ebiz_loc.sma_item_stk_lot k ON (k.code = a.ITM_ID AND a.ORG_ID = k.org_id AND a.WH_ID = k.wh_id) WHERE a.POS_BIN_ID IS NULL OR a.POS_BIN_ID = ''
       $w = "INSERT INTO ".$posDB.".sma_item_stk_lot_bin(org_id,segment_id,warehouse_id,wh_id,item_id,lot_id,bin_id,itm_uom_id,total_stk,rejected_stk,reworkable_stk,created_date,code) SELECT a.ORG_ID,k.segment_id,k.warehouse_id,a.WH_ID,k.item_id,k.id,c.id,k.itm_uom_id,a.TOT_STK,a.REJ_STK,a.RWK_STK,DATE_FORMAT(a.USR_ID_MOD_DT,'%Y-%m-%d'),a.ITM_ID FROM ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."bin a INNER JOIN ".$posDB.".sma_bin c ON ((a.BIN_ID = c.bin_id) AND (a.ORG_ID = c.org_id) AND (a.WH_ID = c.wh_id)) INNER JOIN ".$posDB.".sma_item_stk_lot k ON (k.code = a.ITM_ID AND a.ORG_ID = k.org_id AND a.WH_ID = k.wh_id AND k.lot_no = a.LOT_ID) WHERE a.POS_BIN_ID IS NULL OR a.POS_BIN_ID = ''";
        //$this->db->query($w);
        //return true;
        //INNER JOIN ".$posDB.".sma_products b ON ((a.ITM_ID = b.code) AND (a.ORG_ID = b.org_id) AND (a.WH_ID = b.wh_id)) 
        if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."bin a INNER JOIN ".$posDB.".sma_item_stk_lot_bin b ON ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id) AND (a.ITM_ID = b.code) AND (a.TOT_STK = b.total_stk)) SET a.POS_BIN_ID = b.id WHERE ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id) AND (a.ITM_ID = b.code) AND (a.TOT_STK = b.total_stk))";
             if($this->db->query($w1))
             {
                $msg = "Stock Bin Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    ////////////////////////////////////////////

     public function emrsMaster(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        //INNER JOIN ".$posDB.".sma_warehouses b ON (a.ORG_ID = b.ORG_ID AND a.WH_ID_REQ_TO = b.wh_id AND a.PRJ_ID = b.prj_id)
        $w= "INSERT INTO ".$posDB.".sma_emrs(org_id,segment_id,warehouse_id,wh_id,fy_id,doc_id,emrs_type,supplier_id,emrs_no,emrs_dt,org_id_req_to,wh_id_required_to,reqd_dt,emrs_status,emrs_status_dt,remarks,emrs_mode) SELECT a.ORG_ID,b.segment_id,b.id,a.WH_ID_REQ_TO,a.FY_ID,a.DOC_ID,a.EMRS_TYPE,c.id,a.EMRS_NO,DATE_FORMAT(a.EMRS_DT, '%Y-%m-%d'),a.ORG_ID_REQ_TO,a.WH_ID_REQ_TO,DATE_FORMAT(a.REQD_DT,'%Y-%m-%d'),a.EMRS_STAT,DATE_FORMAT(a.EMRS_STAT_DT,'%Y-%m-%d'),a.REMARKS,a.EMRS_MODE FROM ".$intrDB.".intrm".$n."mm".$n."emrs a INNER JOIN ".$posDB.".sma_warehouses b ON (a.ORG_ID = b.ORG_ID AND a.WH_ID_REQ_TO = b.wh_id) INNER JOIN ".$posDB.".sma_companies c ON (a.EO_ID = c.eo_id) WHERE a.POS_MM_EMRS_ID IS NULL OR a.POS_MM_EMRS_ID = ''";
         if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."mm".$n."emrs a INNER JOIN ".$posDB.".sma_emrs b ON ((a.WH_ID_REQ_TO = b.wh_id_required_to) AND (a.ORG_ID = b.org_id) AND (a.DOC_ID = b.doc_id)) SET a.POS_MM_EMRS_ID = b.id WHERE ((a.WH_ID_REQ_TO = b.wh_id_required_to) AND (a.ORG_ID = b.org_id) AND (a.DOC_ID = b.doc_id))";
             if($this->db->query($w1))
             {
                $msg = "EMRS Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    ////////////////////////////////////////////

     public function emrsDocSrcMaster(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_emrs_doc_src(org_id,segment_id,warehouse_id,wh_id,doc_id,doc_no,doc_date) SELECT a.ORG_ID,b.segment_id,b.warehouse_id,b.wh_id,a.DOC_ID,a.DOC_NO_SRC,DATE_FORMAT(a.DOC_DT_SRC,'%Y-%m-%d') FROM ".$intrDB.".intrm".$n."mm".$n."emrs".$n."doc".$n."src a INNER JOIN ".$posDB.".sma_emrs b ON (a.DOC_ID = b.doc_id) WHERE a.POS_MM_EMRS_DOC_SRC_ID IS NULL OR a.POS_MM_EMRS_DOC_SRC_ID = ''";
         if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."mm".$n."emrs".$n."doc".$n."src a INNER JOIN ".$posDB.".sma_emrs_doc_src b ON ((a.DOC_ID = b.doc_id) AND (a.DOC_NO_SRC = b.doc_no)) SET a.POS_MM_EMRS_DOC_SRC_ID = b.id WHERE ((a.DOC_ID = b.doc_id) AND (a.DOC_NO_SRC = b.doc_no))";
             if($this->db->query($w1))
             {
                $msg = "EMRS DOC SRC Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    ////////////////////////////////////////////

     public function emrsItemMaster(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
         $w= "INSERT INTO ".$posDB.".sma_emrs_item(org_id,segment_id,warehouse_id,wh_id,doc_id,doc_no_src_id,item_id,req_qty,bal_qty,itm_uom_bs,uom_conv_fctr,req_qty_bs,itm_price_bs,itm_amt_bs,line_seq_no,code) SELECT a.ORG_ID,b.segment_id,b.warehouse_id,b.wh_id,a.DOC_ID,a.DOC_NO_SRC,c.id,a.REQ_QTY,a.REQ_QTY,a.ITM_UOM_BS,a.UOM_CONV_FCTR,a.REQ_QTY_BS,a.ITM_PRICE_BS,a.ITM_AMT_BS,a.LINE_SEQ_NO,a.ITM_ID FROM ".$intrDB.".intrm".$n."mm".$n."emrs".$n."itm a INNER JOIN ".$posDB.".sma_emrs b ON (a.DOC_ID = b.doc_id) INNER JOIN ".$posDB.".sma_products c ON (a.ITM_ID = c.code AND a.ORG_ID = c.org_id AND b.wh_id = c.wh_id) WHERE a.POS_MM_EMRS_ITM_ID IS NULL OR a.POS_MM_EMRS_ITM_ID = ''";
        // $w= "INSERT INTO ".$posDB.".sma_emrs_item(org_id,segment_id,warehouse_id,wh_id,doc_id,doc_no_src_id,req_qty,bal_qty,itm_uom_bs,uom_conv_fctr,req_qty_bs,itm_price_bs,itm_amt_bs,line_seq_no,code) SELECT a.ORG_ID,b.segment_id,b.warehouse_id,b.wh_id,a.DOC_ID,a.DOC_NO_SRC,a.REQ_QTY,a.REQ_QTY,a.ITM_UOM_BS,a.UOM_CONV_FCTR,a.REQ_QTY_BS,a.ITM_PRICE_BS,a.ITM_AMT_BS,a.LINE_SEQ_NO,a.ITM_ID FROM ".$intrDB.".intrm".$n."mm".$n."emrs".$n."itm a INNER JOIN ".$posDB.".sma_emrs b ON (a.DOC_ID = b.doc_id) WHERE a.POS_MM_EMRS_ITM_ID IS NULL OR a.POS_MM_EMRS_ITM_ID = ''"; 
       //echo "Hi..."; $this->db->query($w); die;
         if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."mm".$n."emrs".$n."itm a INNER JOIN ".$posDB.".sma_emrs_item b ON ((a.DOC_ID = b.doc_id) AND (a.DOC_NO_SRC = b.doc_no_src_id) AND (a.ITM_ID = b.code)) SET a.POS_MM_EMRS_ITM_ID = b.id WHERE ((a.DOC_ID = b.doc_id) AND (a.DOC_NO_SRC = b.doc_no_src_id) AND (a.ITM_ID = b.code))";
             if($this->db->query($w1))
             {
                $msg = "EMRS Item Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    

    
    // I.T.-01 (Pos -> intermediate) Customer
    
    public function eoMaster()
    {
        //$this->eoMasterUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        //$w = "INSERT INTO ".$intrDB.".intrm".$n."eo( CLD_ID, SLOC_ID, HO_ORG_ID, EO_TYPE, POS_EO_ID, EO_CATG_ID, EO_NM, EO_PHONE, VAT_NO, FATHER_NAME, ID_PROOF, ID_NO, LOYALTY_ID, NM1, DOB_NM1, NM2, DOB_NM2, NM3, DOB_NM3, NM4, DOB_NM4, NM5, DOB_NM5, USR_ID_CREATE, USR_ID_CREATE_DT, USR_ID_MOD, USR_ID_MOD_DT ) SELECT '0000', '1', '01', 'C', a.id, 'T002', a.name, a.phone, a.vat_no, a.father_name, a.id_proof, a.id_proof_no, a.loyalty_card_id, a.nominees1, a.dob_nominees1, a.nominees2, a.dob_nominees2, a.nominees3, a.dob_nominees3, a.nominees4, a.dob_nominees4, a.nominees5, a.dob_nominees5, '1', CURDATE(), '1', CURDATE() FROM ".$posDB.".sma_companies a WHERE a.id != '1' AND a.group_name != 'biller' AND( a.sync_flg IS NULL OR a.sync_flg = '' )";
        $w = "INSERT INTO ".$intrDB.".intrm".$n."eo( CLD_ID, SLOC_ID, HO_ORG_ID,EO_ORG_ID,EO_TYPE, POS_EO_ID, EO_CATG_ID, EO_NM, EO_PHONE, VAT_NO, USR_ID_CREATE, USR_ID_CREATE_DT, USR_ID_MOD, USR_ID_MOD_DT ) SELECT '0000', '1', '01', b.org_id,'C', a.id, 'T002', a.name, a.phone, a.vat_no, '1', CURDATE(), '1', CURDATE() FROM ".$posDB.".sma_companies a INNER JOIN ".$posDB.".sma_segment b ON (a.segment_id = b.id) WHERE a.id != '1' AND a.group_name != 'biller' AND( a.sync_flg IS NULL OR a.sync_flg = '' )";
     if ($this->db->query($w)) {
          $w1= "INSERT INTO ".$intrDB.".intrm".$n."eo".$n."add(CLD_ID,SLOC_ID,HO_ORG_ID,DEF_ADD_FLG,EO_TYPE,POS_EO_ID,USR_ID_CREATE,USR_ID_CREATE_DT,USR_ID_MOD,USR_ID_MOD_DT,ADDRESS,UPD_FLG) SELECT '0000','1','01','N','C',a.id,'1',curdate(),'1',curdate(),CONCAT(a.address,' ',a.city),'1' FROM ".$posDB.".sma_companies a WHERE a.id!= '1' AND a.group_name!='biller' AND (a.sync_flg IS NULL OR a.sync_flg= '')";
          $w2= "INSERT INTO ".$intrDB.".intrm".$n."eo".$n."add".$n."org(CLD_ID,SLOC_ID,HO_ORG_ID,ORG_ID,EO_TYPE,POS_EO_ID,CREATE_FLG,USR_ID_CREATE,USR_ID_CREATE_DT,USR_ID_MOD,USR_ID_MOD_DT,UPD_FLG) SELECT '0000','1','01',b.org_id,'C',a.id,'1','1',curdate(),'1',curdate(),'1' FROM ".$posDB.".sma_companies a CROSS JOIN ".$posDB.".sma_warehouses b WHERE a.id != '1' AND a.group_name != 'biller' AND(a.sync_flg IS NULL OR a.sync_flg = '')";
          $w3= "INSERT INTO ".$intrDB.".intrm".$n."eo".$n."org(CLD_ID,SLOC_ID,HO_ORG_ID,ORG_ID,EO_TYPE,POS_EO_ID,CREATE_FLG,USR_ID_CREATE,USR_ID_CREATE_DT,USR_ID_MOD,USR_ID_MOD_DT) SELECT '0000','1','01',b.org_id,'C',a.id,'1','1',curdate(),'1',curdate() FROM ".$posDB.".sma_companies a CROSS JOIN ".$posDB.".sma_warehouses b WHERE a.id != '1' AND a.group_name != 'biller' AND(a.sync_flg IS NULL OR a.sync_flg = '')";
          $this->db->query($w1);
          $this->db->query($w2);
          if ($this->db->query($w3)) {
             $w4 = "UPDATE ".$posDB.".sma_companies a INNER JOIN ".$intrDB.".intrm".$n."eo b ON a.id = b.POS_EO_ID SET a.sync_flg='1' WHERE a.id != '1' AND a.group_name != 'biller' AND(a.sync_flg IS NULL OR a.sync_flg = '')";
            $this->db->query($w4);
            return true;

          }  else {
                   $msg = $this->db->error();
                   return $msg['message'];
          }    
      } else {
            $msg = $this->db->error();
            return $msg['message'];
      }
    }

    public function eoMasterUPD()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$intrDB.".intrm".$n."eo a INNER JOIN ".$posDB.".sma_companies b ON b.id = a.POS_EO_ID SET a.EO_NM = b.name,a.EO_PHONE = b.phone,a.VAT_NO = b.vat_no WHERE b.id!= '1' AND b.group_name!='biller' AND (b.upd_flg= '1')";
        if ($this->db->query($w)) {
            $w1= "UPDATE ".$posDB.".sma_companies a SET a.sync_flg = '0' WHERE a.id != '1' AND a.group_name != 'biller' AND (b.upd_flg= '1')";
            $this->db->query($w1);
            return true;

        } else {
            $msg = $this->db->error();
            return $msg['message']; 
        }
    }
    // I.T.-02 (Pos -> intermediate) sale

    public function slsInvMaster()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "INSERT INTO ".$intrDB.".intrm".$n."sls".$n."inv( CLD_ID, SLOC_ID, HO_ORG_ID, ORG_ID, POS_DOC_ID, DOC_ID, DOC_DT, EO_ID, POS_EO_ID, TAX_BASIS, TAX_ID, TAX_VAL, DISC_BASIS, DISC_TYPE, POS_INV_TOTAL, DISC_VAL, DISC_TOTAL_VAL, USR_ID_CREATE, USR_ID_CREATE_DT, USR_ID_MOD, USR_ID_MOD_DT,WH_ID ) SELECT '0000', '1', '01', b.org_id, a.id, a.reference_no, DATE_FORMAT(a.date, '%Y-%m-%d'), c.eo_id, a.customer_id, 'NULL', a.order_tax_id, a.total_tax, 'NULL', 'NULL', a.grand_total, a.total_discount, a.total_discount, 1, CURDATE(), 1, CURDATE(),b.wh_id FROM ".$posDB.".sma_sales a INNER JOIN ".$posDB.".sma_warehouses b ON(a.warehouse_id = b.id) INNER JOIN ".$posDB.".sma_companies c ON(a.customer_id = c.id) WHERE a.sync_flg IS NULL OR a.sync_flg = ''";
        if ($this->db->query($w)) {
            $w1  = "UPDATE ".$posDB.".sma_sales a INNER JOIN ".$intrDB.".intrm".$n."sls".$n."inv b ON a.id = b.POS_DOC_ID SET a.sync_flg = '1',b.SYNC_FLG = '1' WHERE a.id = b.POS_DOC_ID";
            $q   = $this->db->query($w1);
            $msg = "Sales Details Sync Successfully....";
            return $msg;
        } else {
            
            $msg = $this->db->error();
            return $msg['message'];
        }
    }
    
    
    // I.T.-03 (Pos -> intermediate) sale Item

    public function slsInvItmMaster()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "INSERT INTO ".$intrDB.".intrm".$n."sls".$n."inv".$n."itm(CLD_ID,SLOC_ID,HO_ORG_ID,ORG_ID,POS_DOC_ID,DOC_ID,ITM_ID,POS_ITM_ID,SR_NO,LOT_NO,ITM_QTY,   POS_ITM_UOM_BS,SLS_PRICE,TAX_VAL,DISC_TOTAL_VAL,DISC_TYPE,DISC_VAL,ITM_TOTAL_VAL, USR_ID_CREATE, USR_ID_CREATE_DT, USR_ID_MOD, USR_ID_MOD_DT,DOC_DATE,WH_ID) SELECT '0000','1','01',b.org_id,a.id,a.sale_id,a.product_code,a.product_id,c.sr_no,c.lot_no,a.quantity,a.net_unit_price,a.real_unit_price,a.item_tax,a.item_discount,'NULL',a.discount,a.net_unit_price,1, CURDATE(), 1, CURDATE(),DATE_FORMAT(m.date, '%Y-%m-%d'),b.wh_id FROM ".$posDB.".sma_sale_items a INNER JOIN ".$posDB.".sma_warehouses b ON (a.warehouse_id = b.id) INNER JOIN ".$posDB.".sma_sales m ON(m.id = a.sale_id) INNER JOIN ".$posDB.".sma_products c ON (a.product_id = c.id) WHERE a.sync_flg IS NULL OR a.sync_flg = ''";
        if ($this->db->query($w)) {
            $w1  = "UPDATE ".$posDB.".sma_sale_items a INNER JOIN ".$intrDB.".intrm".$n."sls".$n."inv".$n."itm b ON a.id = b.POS_DOC_ID SET a.sync_flg='1', b.SYNC_FLG='1' WHERE a.id = b.POS_DOC_ID";
            $q   = $this->db->query($w1);
            $msg = "Sales Item Details Sync Successfully....";
            return $msg;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }
    
    
    // I.T.-04 (Pos -> intermediate) Payment Details Sync

    
    public function PaymntMaster()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "INSERT INTO ".$intrDB.".intrm".$n."payment".$n."dtl( CLD_ID, SLOC_ID, HO_ORG_ID, ORG_ID, POS_PAYMNT_ID, POS_INV_NO, PAYMNT_DT, EO_ID, POS_EO_ID, PAID_BY, CC_NM, CC_HOLDER, CC_MONTH, CC_YEAR, CC_TYPE, AMOUNT, SYNC_FLG, USR_ID_CREATE, USR_ID_CREATE_DT, USR_ID_MOD, USR_ID_MOD_DT ) SELECT '0000', '1', '01', c.org_id, a.id, b.reference_no, DATE_FORMAT(a.date,'%Y-%m-%d'), d.eo_id, b.customer_id, a.paid_by, a.cc_no, a.cc_holder, a.cc_month, a.cc_year, a.cc_type, a.amount, '1', '1', CURDATE(), '1', CURDATE() FROM ".$posDB.".sma_payments a INNER JOIN ".$posDB.".sma_sales b ON(a.sale_id = b.id) INNER JOIN ".$posDB.".sma_warehouses c ON(b.warehouse_id = c.id) INNER JOIN ".$posDB.".sma_companies d ON(b.customer_id = d.id) WHERE a.sync_flg IS NULL OR a.sync_flg = ''";
  if ($this->db->query($w)) {
    $w1= "UPDATE ".$posDB.".sma_payments a INNER JOIN ".$intrDB.".intrm".$n."payment".$n."dtl b ON (a.id = b.POS_PAYMNT_ID) SET a.sync_flg = '1' WHERE a.id = b.POS_PAYMNT_ID";
    $q= $this->db->query($w1);
    $msg = "Payment Details Sync Successfully....";
    return $msg;
  } else {
       $msg = $this->db->error();
       return $msg['message'];
  }

    }

// I.T.-05 (Pos -> intermediate) Update PO Status
    public function poStatusUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$intrDB.".intrm".$n."drft".$n."po a INNER JOIN ".$posDB.".sma_drft_po b ON (a.PO_ID = b.po_id AND a.DOC_ID = b.doc_id AND a.POS_PO_ID = b.id) SET a.PO_STATUS = b.po_status,a.UPD_FLG='1' WHERE b.status!=219 AND (a.PO_ID = b.po_id AND a.DOC_ID = b.doc_id AND a.POS_PO_ID = b.id)";
        if ($this->db->query($w)) {
            $msg = "PO Status Sync Successfully....";
            return $msg;

        } else {
            $msg = $this->db->error();
            return $msg['message'];
     } 

    }

    public function poSHDLUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$intrDB.".intrm".$n."drft".$n."po".$n."dlv".$n."schdl a INNER JOIN ".$posDB.".sma_drft_po_dlv_schdl b ON (a.POS_PO_SCHDL_ID = b.id) INNER JOIN ".$posDB.".sma_drft_po c ON (a.POS_PO_SCHDL_ID = b.id) SET a.BAL_QTY = b.bal_qty,a.UPD_FLG='1' WHERE c.status!=1 AND (a.POS_PO_SCHDL_ID = b.id)";
        if ($this->db->query($w)) {
            $msg = "PO Schedule Sync Successfully....";
            return $msg;

        } else {
            $msg = $this->db->error();
            return $msg['message'];
     } 

    }

    // I.T.-06 (Pos -> intermediate) Update EMRS Bal_Qty
    public function emrsItmUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$intrDB.".intrm".$n."mm".$n."emrs".$n."itm a INNER JOIN ".$posDB.".sma_emrs_item b ON (a.POS_MM_EMRS_ITM_ID = b.id) SET a.SYNC_FLG = '1', a.REQ_QTY = b.bal_qty WHERE b.status= 2 AND (a.POS_MM_EMRS_ITM_ID = b.id)";
        if ($this->db->query($w)) {
            $msg = "EMRS Bal Qty ITEM Update Successfully....";
            return $msg;

        } else {
            $msg = $this->db->error();
            return $msg['message'];
     } 

    }


    // I.T.-000 (Pos -> intermediate) Update Qty


    public function updQty()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$intrDB.".intrm".$n."itm".$n."stock".$n."dtl p INNER JOIN ".$posDB.".sma_products a ON (p.ITM_ID = a.code AND p.ORG_ID = a.org_id AND p.LOT_NO = a.lot_no) SET p.USE_QTY = FLOOR(p.AVAIL_QTY - a.quantity) WHERE a.psale='1'";
        if ($this->db->query($w)) {
            $w1 = "UPDATE ".$posDB.".sma_products a SET a.psale='' WHERE a.psale='1'";
            $this->db->query($w1);
            $msg = "Product Qty Update Successfully....";
            return $msg;
        }
        
        else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }

    ////////////////////////////////////////////

    // I.T.-07 (Pos -> intermediate) Insert Stock Item to ERP

    public function InsertstockItmMaster(){
        $this->stockItmMasterUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."itm(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_STK_ITM_ID,FY_ID,ITM_ID,ITM_UOM_BS,TOT_STK,AVL_STK,ORD_STK,REJ_STK,RWK_STK,MAP_PRICE,WAP_PRICE,CREATE_FLG,UPD_FLG) SELECT '0000','1',b.org_id,b.wh_id,a.id,a.fy_id,c.code,m.uom_id,a.total_stk,a.available_stk,a.ordered_stk,a.rejected_stk,a.rework_stk,a.sales_price,a.sales_price,'1','5' from ".$posDB.".sma_item_stk_profile a INNER JOIN ".$posDB.".sma_warehouses b ON (b.id = a.warehouse_id) INNER JOIN ".$posDB.".sma_products c ON (c.id = a.item_id AND c.warehouse =a.warehouse_id) INNER JOIN ".$posDB.".sma_uom_conv_std m ON (m.uom_desc = a.item_uom_id) WHERE a.create_flg = '1'";
        if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_item_stk_profile a SET a.create_flg = '' WHERE a.create_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "Stock ITM Records Save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }

    public function stockItmMasterUPD()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."itm a INNER JOIN ".$posDB.".sma_item_stk_profile b ON (a.POS_STK_ITM_ID = b.id) SET a.TOT_STK = b.total_stk, a.AVL_STK = b.available_stk, a.REJ_STK = b.rejected_stk, a.RWK_STK = b.rework_stk, a.UPD_FLG='1' WHERE b.upd_flg='1'";
        if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_item_stk_profile a SET a.upd_flg = '' WHERE a.upd_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "Stock ITM Records Save successfully...";
                return true;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }

    }

    ////////////////////////////////////////////

    // I.T.-08 (Pos -> intermediate) Insert Stock Item LOT to ERP

    public function InsertstockItmLotMaster(){
        $this->stockItmLotUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$'; 
        $w = "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."lot(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_STK_LOT_ID,ITM_ID,BASIC_PRICE,LND_PRICE,LOT_ID,ITM_UOM_BS,TOT_STK,REJ_STK,EXPIRY_DT,RWK_STK,USR_ID_MOD_DT,LOT_DT,CREATE_FLG,UPD_FLG) SELECT '0000','1',b.org_id,b.wh_id,a.id,c.code,c.price,c.price,a.lot_no,m.uom_id,a.total_stk,a.rejected_stk,DATE_FORMAT(a.exp_date,'%Y-%m-%d'),a.reworkable_stk,DATE_FORMAT(a.created_date,'%Y-%m-%d'),DATE_FORMAT(a.created_date,'%Y-%m-%d'),'1','5' from ".$posDB.".sma_item_stk_lot a INNER JOIN ".$posDB.".sma_warehouses b ON (b.id = a.warehouse_id) INNER JOIN ".$posDB.".sma_products c ON (c.id = a.item_id AND c.warehouse =a.warehouse_id) INNER JOIN ".$posDB.".sma_uom_conv_std m ON (m.uom_desc = a.itm_uom_id) WHERE a.create_flg = '1'";
        if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_item_stk_lot a SET a.create_flg = '' WHERE a.create_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "Stock ITM LOT Records Save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }

    public function stockItmLotUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."lot a INNER JOIN ".$posDB.".sma_item_stk_lot b ON (a.POS_STK_LOT_ID = b.id) SET a.TOT_STK = b.total_stk, a.REJ_STK = b.rejected_stk, a.RWK_STK = b.reworkable_stk, a.UPD_FLG='1' WHERE b.upd_flg='1'";
        if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_item_stk_lot a SET a.upd_flg = '' WHERE a.upd_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "Stock ITM LOT Records Save successfully...";
                return true;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }

    }

    ////////////////////////////////////////////

    // I.T.-09 (Pos -> intermediate) Insert Stock Item LOT BIN to ERP

    public function InsertstockItmLotBINMaster(){
           $this->updstockLotBinMaster();
           $posDB = $this->posDbName();
           $intrDB = $this->intrDbName();
           $n = '$';
           $w = "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."bin(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_BIN_ID,ITM_ID,LOT_ID,BIN_ID,ITM_UOM_BS,TOT_STK,REJ_STK,RWK_STK,USR_ID_MOD_DT,CREATE_FLG,UPD_FLG) SELECT '0000','1',b.org_id,b.wh_id,a.id,c.code,f.lot_no,d.bin_id,m.uom_id,a.total_stk,a.rejected_stk,a.reworkable_stk,DATE_FORMAT(a.created_date,'%Y-%m-%d'),'1','5' FROM ".$posDB.".sma_item_stk_lot_bin a INNER JOIN ".$posDB.".sma_warehouses b ON(b.id = a.warehouse_id) INNER JOIN ".$posDB.".sma_products c ON(c.id = a.item_id AND c.warehouse = a.warehouse_id) INNER JOIN ".$posDB.".sma_bin d ON(d.id = a.bin_id) INNER JOIN ".$posDB.".sma_uom_conv_std m ON (m.uom_desc = a.itm_uom_id) INNER JOIN ".$posDB.".sma_item_stk_lot f ON (f.id = a.lot_id) WHERE a.create_flg = '1'";
        if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_item_stk_lot_bin a SET a.create_flg = '' WHERE a.create_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "Stock ITM LOT BIN Records Save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }


    public function updstockLotBinMaster(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."bin a INNER JOIN ".$posDB.".sma_item_stk_lot_bin b ON ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id) AND (a.ITM_ID = b.code)) SET a.TOT_STK = b.total_stk,a.UPD_FLG = '1',a.REJ_STK = b.rejected_stk,a.RWK_STK = b.reworkable_stk WHERE ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id) AND (a.ITM_ID = b.code) AND b.upd_flg = '1')";
        if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_item_stk_lot_bin a SET a.upd_flg = '' WHERE a.upd_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "Stock Bin Records Update successfully...";
                return true;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

////////////////////////////////////////////

    // I.T.-10 (Pos -> intermediate) Insert EMRS Stock Item to ERP

    public function InsertEMRSstockItmMaster(){
        $this->EMRSstockItmMasterUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."estk".$n."summ".$n."itm(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_STK_ITM_ID,FY_ID,ITM_ID,ITM_UOM_BS,TOT_STK,AVL_STK,REJ_STK,RWK_STK,ORD_STK,MAP_PRICE,WAP_PRICE,USR_ID_MOD_DT,CREATE_FLG,UPD_FLG) SELECT  '0000','1',b.org_id,b.wh_id,a.id,a.fy_id,c.code,m.uom_id,a.total_stk,a.available_stk,a.rejected_stk,a.rework_stk,a.ordered_stk,c.price,c.price,DATE_FORMAT(a.created_date,'%Y-%m-%d'),'1','5' from ".$posDB.".sma_cons_item_stk_profile a INNER JOIN ".$posDB.".sma_warehouses b ON (b.id = a.warehouse_id) INNER JOIN ".$posDB.".sma_products c ON (c.id = a.item_id AND c.warehouse =a.warehouse_id) INNER JOIN ".$posDB.".sma_uom_conv_std m ON (m.uom_desc = a.item_uom_id) WHERE a.create_flg = '1'";
        if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_cons_item_stk_profile a SET a.create_flg = '' WHERE a.create_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "EMRS Stock ITM Records Save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }

    public function EMRSstockItmMasterUPD()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$intrDB.".intrm".$n."mm".$n."estk".$n."summ".$n."itm a INNER JOIN ".$posDB.".sma_cons_item_stk_profile b ON (a.POS_STK_ITM_ID = b.id) SET a.TOT_STK = b.total_stk, a.AVL_STK = b.available_stk, a.REJ_STK = b.rejected_stk, a.RWK_STK = b.rework_stk, a.UPD_FLG='1' WHERE b.upd_flg='1'";
        if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_cons_item_stk_profile a SET a.upd_flg = '' WHERE a.upd_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "Stock ITM Records Save successfully...";
                return true;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }

    }

////////////////////////////////////////////

    // I.T.-11 (Pos -> intermediate) Insert Stock Item LOT to ERP

    public function InsertEMRSstockItmLotMaster(){
        $this->EMRSstockItmLotUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$'; 
        $w = "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."estk".$n."summ".$n."lot(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_STK_LOT_ID,ITM_ID,LOT_ID,ITM_UOM_BS,TOT_STK,REJ_STK,EXPIRY_DT,RWK_STK,USR_ID_MOD_DT,LOT_DT,CREATE_FLG,UPD_FLG) SELECT '0000','1',b.org_id,b.wh_id,a.id,c.code,a.lot_no,m.uom_id,a.total_stk,a.rejected_stk,DATE_FORMAT(a.exp_date,'%Y-%m-%d'),a.reworkable_stk,DATE_FORMAT(a.created_date,'%Y-%m-%d'),DATE_FORMAT(a.created_date,'%Y-%m-%d'),'1','5' from ".$posDB.".sma_cons_item_stk_lot a INNER JOIN ".$posDB.".sma_warehouses b ON (b.id = a.warehouse_id) INNER JOIN ".$posDB.".sma_products c ON (c.id = a.item_id AND c.warehouse =a.warehouse_id) INNER JOIN ".$posDB.".sma_uom_conv_std m ON (m.uom_desc = a.item_uom_id) WHERE a.create_flg = '1'";
        if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_cons_item_stk_lot a SET a.create_flg = '' WHERE a.create_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "EMRS Stock ITM LOT Records Save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }

    public function EMRSstockItmLotUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$intrDB.".intrm".$n."mm".$n."estk".$n."summ".$n."lot a INNER JOIN ".$posDB.".sma_cons_item_stk_lot b ON (a.POS_STK_LOT_ID = b.id) SET a.TOT_STK = b.total_stk, a.REJ_STK = b.rejected_stk, a.RWK_STK = b.reworkable_stk, a.UPD_FLG='1' WHERE b.upd_flg='1'";
        if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_cons_item_stk_lot a SET a.upd_flg = '' WHERE a.upd_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "EMRS Stock ITM LOT Records Save successfully...";
                return true;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }

    }

 ////////////////////////////////////////////

    // I.T.-12 (Pos -> intermediate) Insert Stock Item LOT BIN to ERP

    public function InsertEMRSstockItmLotBINMaster(){
           $this->updEMRSstockLotBinMaster();
           $posDB = $this->posDbName();
           $intrDB = $this->intrDbName();
           $n = '$';
           $w = "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."estk".$n."summ".$n."bin(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_BIN_ID,ITM_ID,LOT_ID,BIN_ID,ITM_UOM_BS,TOT_STK,REJ_STK,RWK_STK,USR_ID_MOD_DT,CREATE_FLG,UPD_FLG) SELECT '0000','1',b.org_id,b.wh_id,a.id,c.code,p.lot_no,d.bin_id,m.uom_id,a.total_stk,a.rejected_stk,a.reworkable_stk,DATE_FORMAT(a.created_date,'%Y-%m-%d'),'1','5' FROM ".$posDB.".sma_cons_item_stk_lot_bin a INNER JOIN ".$posDB.".sma_warehouses b ON(b.id = a.warehouse_id) INNER JOIN ".$posDB.".sma_products c ON(c.id = a.item_id AND c.warehouse = a.warehouse_id) INNER JOIN ".$posDB.".sma_bin d ON(d.id = a.bin_id) INNER JOIN ".$posDB.".sma_uom_conv_std m ON (m.uom_desc = a.item_uom_id) INNER JOIN ".$posDB.".sma_cons_item_stk_lot p ON(p.id = a.lot_id)  WHERE a.create_flg = '1'";
        if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_cons_item_stk_lot_bin a SET a.create_flg = '' WHERE a.create_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "EMRSStock ITM LOT BIN Records Save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }


    public function updEMRSstockLotBinMaster(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$intrDB.".intrm".$n."mm".$n."estk".$n."summ".$n."bin a INNER JOIN ".$posDB.".sma_cons_item_stk_lot_bin b ON (a.POS_BIN_ID = b.id) SET a.TOT_STK = b.total_stk,a.UPD_FLG = '1',a.REJ_STK = b.rejected_stk,a.RWK_STK = b.reworkable_stk WHERE (a.POS_BIN_ID = b.id) AND b.upd_flg = '1'";
        if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_cons_item_stk_lot_bin a SET a.upd_flg = '' WHERE a.upd_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "EMRS Stock Bin Records Update successfully...";
                return true;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    ////////////////////////////////////////////

    // I.T.-13 (Pos -> intermediate) MRN RCPT to ERP

    public function InsertMaterialRcptMaster(){
          // $this->updInsertMaterialRcptMaster();
           $posDB = $this->posDbName();
           $intrDB = $this->intrDbName();
           $n = '$';
           $w = "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."mtl".$n."rcpt(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_MTL_RCPT_ID,FY_ID,RCPT_NO,RCPT_DT,RCPT_SRC_TYPE,GE_DOC_ID,EO_ID_SRC,DN_NO_SRC,DN_DT_SRC,EO_ID_TPT,TPT_BILL_NO,TPT_BILL_DT,VEHICLE_NO,RMDA_STAT,ADDL_AMT,CURR_ID_SP,PRJ_ID,SUPP_INVC_RCVD_TPT_DT,SYNC_FLG,UPD_FLG) SELECT '0000','1',b.org_id,b.wh_id,a.id,1,a.rcpt_no,DATE_FORMAT(a.rcpt_dt,'%Y-%m-%d'),a.rcpt_src_type,a.ge_doc_id,c.eo_id,a.dn_no, DATE_FORMAT(a.dn_dt,'%Y-%m-%d'),d.eo_id,a.tpt_lr_no,DATE_FORMAT(a.tpt_lr_dt,'%Y-%m-%d'),a.vehicle_no,a.rmda_stat,a.addl_amt,a.curr_id,b.prj_id,DATE_FORMAT(a.created_date,'%Y-%m-%d'),'1','5' FROM ".$posDB.".sma_mrn_receipt a INNER JOIN ".$posDB.".sma_warehouses b ON(b.id = a.wh_id) INNER JOIN ".$posDB.".sma_companies c ON(c.id = a.supplier_id) INNER JOIN ".$posDB.".sma_companies d ON(d.id = a.tp_id) WHERE a.sync_flg IS NULL OR a.sync_flg = ''";

          if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_mrn_receipt a INNER JOIN ".$intrDB.".intrm".$n."mm".$n."mtl".$n."rcpt b SET a.sync_flg = '1' WHERE b.POS_MTL_RCPT_ID = a.id";
             if($this->db->query($w1))
             {
                $msg = "MRN RECIPT Records Update successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }

    /*public function updInsertMaterialRcptMaster(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$intrDB.".intrm".$n."mm".$n."mtl".$n."rcpt a INNER JOIN ".$posDB.".sma_mrn_receipt b ON (a.POS_MTL_RCPT_ID = b.id) SET a.TOT_STK = b.total_stk,a.UPD_FLG = '1',a.REJ_STK = b.rejected_stk,a.RWK_STK = b.reworkable_stk WHERE ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id) AND (a.ITM_ID = b.code) AND b.upd_flg = '1')";
        if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_mrn_receipt a SET a.upd_flg = '' WHERE a.upd_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "MTL RCPT Update successfully...";
                return true;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }*/

    ////////////////////////////////////////////

    // I.T.-14 (Pos -> intermediate) MRN RCPT SRC to ERP

    public function InsertMaterialRcptSRCMaster(){
          // $this->updInsertMaterialRcptSRCMaster();
           $posDB = $this->posDbName();
           $intrDB = $this->intrDbName();
           $n = '$';
           $w = "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."mtl".$n."rcpt".$n."src(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_MTL_RCPT_SRC_ID,RCPT_ID,DOC_ID_SRC,DOC_DT_SRC,SYNC_FLG,UPD_FLG) SELECT '0000','1',b.org_id,b.wh_id,a.id,f.id,c.doc_id,DATE_FORMAT(c.po_dt,'%Y-%m-%d'),'1','5' FROM  ".$posDB.".sma_mrn_rcpt_src a INNER JOIN ".$posDB.".sma_warehouses b ON(b.id = a.wh_id) INNER JOIN ".$posDB.".sma_drft_po c ON (c.id = a.po_id) INNER JOIN ".$posDB.".sma_mrn_receipt f ON (f.id = a.mrn_rcpt_id) WHERE a.sync_flg IS NULL OR a.sync_flg = ''";
         if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_mrn_rcpt_src a INNER JOIN ".$intrDB.".intrm".$n."mm".$n."mtl".$n."rcpt".$n."src b SET a.sync_flg = '1' WHERE b.POS_MTL_RCPT_SRC_ID = a.id";
             if($this->db->query($w1))
             {
                $msg = "MRN RECIPT SRC Records Update successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }

    ////////////////////////////////////////////

    // I.T.-15 (Pos -> intermediate) MRN Stock Item to ERP

    public function InsertMaterialRcptItmMaster(){
          // $this->updInsertMaterialRcptItmMaster();
           $posDB = $this->posDbName();
           $intrDB = $this->intrDbName();
           $n = '$';
           $w = "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."mtl".$n."rcpt".$n."itm(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_MTL_RCPT_ITM_ID,SRC_RCPT_ID,DOC_ID_SRC,DOC_DT_SRC,DLV_SCHDL_NO,ITM_ID,PEND_QTY,DLV_NOTE_QTY,TOT_RCPT_QTY,RWK_QTY,REJ_QTY,RCPT_QTY,SYNC_FLG,UPD_FLG) SELECT '0000','1',b.org_id,b.wh_id,a.id,a.mrn_rcpt_src_id,c.doc_id,DATE_FORMAT(c.po_dt,'%Y-%m-%d'),a.dlv_schdl_no,d.code,a.pending_qty,a.dlv_note_qty,a.tot_rcpt_qty,a.rwk_qty,a.rej_qty,a.rcpt_qty,'1','5' FROM  ".$posDB.".sma_mrn_rcpt_item a INNER JOIN ".$posDB.".sma_warehouses b ON(b.id = a.wh_id) INNER JOIN ".$posDB.".sma_drft_po c ON (c.id = a.po_id) INNER JOIN ".$posDB.".sma_products d ON (d.id = a.item_id) WHERE a.sync_flg IS NULL OR a.sync_flg = ''";
         if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_mrn_rcpt_item a INNER JOIN ".$intrDB.".intrm".$n."mm".$n."mtl".$n."rcpt".$n."itm b SET a.sync_flg = '1' WHERE b.POS_MTL_RCPT_ITM_ID = a.id";
             if($this->db->query($w1))
             {
                $msg = "MRN RECIPT ITM Records Update successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }

 ////////////////////////////////////////////

    // I.T.-16 (Pos -> intermediate) MRN Stock Item to ERP

    public function InsertMaterialRcptItmLOTMaster(){
          // $this->updInsertMaterialRcptItmLOTMaster();
           $posDB = $this->posDbName();
           $intrDB = $this->intrDbName();
           $n = '$';
           $w = "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."mtl".$n."rcpt".$n."lot(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_MTL_RCPT_LOT_ID,ITM_SRC_ID,DOC_ID_SRC,DOC_DT_SRC,DLV_SCHDL_NO,LOT_ID,ITM_ID,LOT_QTY,LOT_PRICE,EXPIRY_DT,MFG_DT,BATCH_NO,SYNC_FLG,UPD_FLG) SELECT '0000','1',b.org_id,b.wh_id,a.id,
           a.mrn_rcpt_item_id,c.doc_id,DATE_FORMAT(c.po_dt,'%Y-%m-%d'),a.dlv_schdl_no,a.lot_no,d.code,a.lot_qty,d.price,DATE_FORMAT(a.expiry_dt,'%Y-%m-%d'),DATE_FORMAT(a.mfg_dt,'%Y-%m-%d'),a.batch_no,'1','5' FROM  ".$posDB.".sma_mrn_rcpt_item_lot a INNER JOIN ".$posDB.".sma_warehouses b ON(b.id = a.wh_id) INNER JOIN ".$posDB.".sma_mrn_rcpt_item z ON (z.id = a.mrn_rcpt_item_id) INNER JOIN ".$posDB.".sma_drft_po c ON (c.id = z.po_id) INNER JOIN ".$posDB.".sma_products d ON (d.id = a.item_id) WHERE a.sync_flg IS NULL OR a.sync_flg = ''";
         if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_mrn_rcpt_item_lot a INNER JOIN ".$intrDB.".intrm".$n."mm".$n."mtl".$n."rcpt".$n."lot b SET a.sync_flg = '1' WHERE b.POS_MTL_RCPT_LOT_ID = a.id";
             if($this->db->query($w1))
             {
                $msg = "MRN RECIPT LOT Records Update successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }   

 ////////////////////////////////////////////

    // I.T.-17 (Pos -> intermediate) MRN Stock Item to ERP

    public function InsertMaterialRcptItmBinMaster(){
          // $this->updInsertMaterialRcptItmBinMaster();
           $posDB = $this->posDbName();
           $intrDB = $this->intrDbName();
           $n = '$';
           $w = "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."mtl".$n."rcpt".$n."bin(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_MTL_RCPT_BIN_ID,LOT_SRC_ID,DOC_ID_SRC,DOC_DT_SRC,LOT_ID,BIN_ID,ITM_ID,BIN_QTY,SYNC_FLG,UPD_FLG) SELECT '0000','1',b.org_id,b.wh_id,a.id,a.lot_id,c.doc_id,DATE_FORMAT(c.po_dt,'%Y-%m-%d'),n.lot_no,m.bin_id,d.code,a.bin_qty,'1','5' FROM  ".$posDB.".sma_mrn_rcpt_lot_bin a INNER JOIN ".$posDB.".sma_warehouses b ON(b.id = a.wh_id) INNER JOIN ".$posDB.".sma_bin m ON (m.id = a.bin_id) INNER JOIN ".$posDB.".sma_products d ON (d.id = a.item_id) INNER JOIN ".$posDB.".sma_mrn_rcpt_item_lot n ON (n.id = a.lot_id) INNER JOIN ".$posDB.".sma_mrn_rcpt_item z ON (z.id = n.mrn_rcpt_item_id) INNER JOIN ".$posDB.".sma_drft_po c ON (c.id = z.po_id) WHERE a.sync_flg IS NULL OR a.sync_flg = ''";
           if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_mrn_rcpt_lot_bin a INNER JOIN ".$intrDB.".intrm".$n."mm".$n."mtl".$n."rcpt".$n."bin b SET a.sync_flg = '1' WHERE b.POS_MTL_RCPT_BIN_ID = a.id";
             if($this->db->query($w1))
             {
                $msg = "MRN RECIPT BIN Records Update successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }  
    

    
    
    // Sync all intermediate table to POS data base

    /* --------------Please must be Follow Below Sequence of Function Otherwise data may be corrupt----------------------*/
    public function syncToPos()
    {
       $this->currencyMaster();
        $this->segmentMaster(); // Segment Master Should be run before Warehouse Master
        $this->warehouseMaster(); // Warehouse master should be run before userMaster
            //$this->whORGMaster(); 
        $this->userMaster();
        $this->taxMaster(); // Tax master should be run before categoryMaster
        $this->categoryMaster();
        $this->customerMaster();
        $this->binMaster();
        $this->uomClsMaster(); // UOM Cls Master should be run before UOM Cls Stdv Master
        $this->uomConStdvMaster();
        $this->dsAttMaster(); // DS ATT Master should be run before ATT RELN Master And ATT TYPE Master
        $this->dsAttRelnMaster(); // ATT RELN Master should be run before ATT TYPE Master
        $this->dsAttTypeMaster();
       $this->productMaster(); // Product master should be run before productWarehouseMaster
       $this->productWarehouseMaster();
       $this->poMaster(); // Po Master should be run befor PO Schedule Master
        $this->poSchdlMaster();
        $this->stockItmMaster(); // Item Stock Master should be run before Stock lot Master
        $this->stockLotMaster(); // Item Stock lot Master should be run before Stock lot bin Master
        $this->stockLotBinMaster();

        

        $this->emrsMaster(); // EMRS Master should be run before EMRS DOC SRC Master
        $this->emrsDocSrcMaster(); // EMRS DOC SRC Master should be run before EMRC Item Master
        $this->emrsItemMaster();

        
         
        
        return true;
    }

    
    // Sync all pos data table to intermediate data base

    /* --------------Please must be follow Below Sequence of Function Otherwise data may be corrupt----------------------*/
    public function syncToIntermediate()
    {
        $this->eoMaster();
        $this->slsInvMaster();
        $this->slsInvItmMaster();
        $this->PaymntMaster();
                               //$this->updStockItmMaster();

        $this->poStatusUPD(); // postatus Master should be run before poSHDL Master
        $this->poSHDLUPD();
        $this->emrsItmUPD();
        $this->InsertstockItmMaster(); // Item STK should be run before STK LOT Master
        $this->InsertstockItmLotMaster(); // Item STK LOT should be run before STK LOT BIN Master
        $this->InsertstockItmLotBINMaster(); 

        $this->InsertEMRSstockItmMaster(); // EMRS Stock master should be run before EMRS Stock LOT Master
        $this->InsertEMRSstockItmLotMaster(); // EMRS Stock LOT master should be run before EMRSLOT BIN Master
        $this->InsertEMRSstockItmLotBINMaster(); 

        $this->InsertMaterialRcptMaster(); //MRN RCPT Should be run before All MRN Fuction
        $this->InsertMaterialRcptSRCMaster();
        $this->InsertMaterialRcptItmMaster();
        $this->InsertMaterialRcptItmLOTMaster();
        $this->InsertMaterialRcptItmBinMaster();

        //$this->updQty();
        return true;
    }

   public function emptyPOS()
   {
     $q= "CALL emptyPOSdb()"; // Call Procedure
     if($this->db->query($q))
     {
        return true;
     }
       return false;
   }

   public function intrmPOSIDempty()
    {
        $CI = &get_instance();
        $this->db2 = $CI->load->database('db2', TRUE);
        $q = "CALL emptyIntermdb()"; // Call procedure
        if($this->db2->query($q))
        {
            return TRUE;
        }
        return false;
    }


    public function syncMRN()
    {
        $this->poMaster(); // Po Master should be run befor PO Schedule Master
        $this->poSchdlMaster();
        $this->emrsMaster(); // EMRS Master should be run before EMRS DOC SRC Master
        $this->emrsDocSrcMaster(); // EMRS DOC SRC Master should be run before EMRC Item Master
        $this->emrsItemMaster();
        return true;
    }

	public function syncData(){
        
        $segmentList = $this->db->select('COUNT(*) as num')
            ->from('sync_table')
            ->get()->row();
        $total_segments = $segmentList->num;
        $this->load->model('site');
        $segmentList = $this->site->getAllSegments();
       // print_r($segmentList); die;
        $directory = $_SERVER["DOCUMENT_ROOT"]."/iffcov9/sync-folder/".date("Y-m-d")."/";

        foreach($segmentList as $row){
            $warehouseList = $this->site->getWarehouseList($row->id);
            $segment = $row->id;
            $org = $row->org_id;
            $whidgod = '\'' . implode( '\', \'',explode(",",$warehouseList->wh_id)). '\'';
            //echo $segment."<br>";
            $projName = explode(" ",$row->proj_name)[0]."_".$row->id;
            //echo $projName;exit;
            $this->db->select('COUNT(*)')
            ->from('sync_table')
            ->get()->row();
             for ($x = 1; $x <= $total_segments; $x++) {
                  
                 $this->db->select("*");
                 $this->db->from("sync_table");       
                 $tableName = $this->db->where("id",$x)->get()->row();               
                 $table = $tableName->table_name;
                 // fetch mysql table rows
                 $csv_filename = $table."_".date("Y-m-d").".csv";
                 $filePath = $_SERVER["DOCUMENT_ROOT"]."/iffcov9/sync-folder/".date("Y-m-d")."/".$projName."/";
                  
                 switch ($table) {
                    case "sma_segment":                   
                        $whr=" WHERE id='".$segment."' AND ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1')";
                        break;
                    case "sma_warehouses":
                               $whr=" WHERE segment_id='".$segment."' AND ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1')";
                        break;
                    case "sma_users":
                                $whr=" WHERE segment_id='".$segment."' AND ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1') ";
                        break;
                    case "sma_products":
                        $whr=" WHERE wh_id IN (".$whidgod.") AND org_id='".$org."' AND ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1')";                       
                       
                       break;
                    case "sma_purchase_items":
                        $whr=" WHERE wh_id IN (".$whidgod.") AND org_id='".$org."' AND ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1')";
                       break;
                    case "sma_warehouses_products":
                        $whr=" WHERE wh_id IN (".$whidgod.") AND org_id='".$org."' AND ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1')";
                       break;
                    case "sma_item_stk_profile":
                        $whr=" WHERE segment_id='".$segment."' AND ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1') ";
                       break;
                    case "sma_item_stk_lot":
                        $whr=" WHERE segment_id='".$segment."' AND ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1') ";
                       break;
                    case "sma_item_stk_lot_bin":
                        $whr=" WHERE segment_id='".$segment."' AND ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1') ";
                       break;
                    case "sma_tax_rates":
                        $whr=" WHERE wh_id IN (".$whidgod.") AND org_id='".$org."' AND (pos_create_flg IS NULL OR pos_create_flg = '')";
                      break;
                    case "sma_emrs":
                        $whr=" WHERE segment_id='".$segment."' AND (pos_create_flg IS NULL OR pos_create_flg = '') ";
                       
                       break;
                    case "sma_emrs_doc_src":
                        $whr=" WHERE segment_id='".$segment."' AND (pos_create_flg IS NULL OR pos_create_flg = '') ";
                    break;
                    case "sma_emrs_item":
                        $whr=" WHERE segment_id='".$segment."' AND (pos_create_flg IS NULL OR pos_create_flg = '') ";
                        break;
                    case "sma_bin":
                        $whr=" WHERE segment_id='".$segment."' AND ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1') ";
                        break;
                    case "sma_categories":
                        $whr=" WHERE (pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1'";
                        break;
                    case "sma_companies":
                        $whr=" WHERE (pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1'";
                        break;
                    case "sma_currencies":
                        $whr=" WHERE (pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1'";
                        break;
                    case "sma_drft_po_dlv_schdl":
                     $whr=" WHERE segment_id='".$segment."' AND ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1')";
                        break;
                    case "sma_drft_po":
                        $whr=" LEFT OUTER JOIN sma_drft_po_dlv_schdl ON sma_drft_po_dlv_schdl.po_id=sma_drft_po.id WHERE segment_id='".$segment."' AND (sma_drft_po_dlv_schdl.pos_create_flg IS NULL OR sma_drft_po_dlv_schdl.pos_create_flg = '') ";
                        break;              
                    default:
                        $whr='';
                }
                $host = $this->db->hostname;//"localhost";
                $username = $this->db->username;//"root";
                $password = $this->db->password;//"";
                $dbname = $this->db->database;//"iffco_live";
                $posDB = $this->posDbName();
                $connection = mysqli_connect($host, $username, $password, $dbname) or die("Connection Error " . mysqli_error($connection));
                //$sql = 'select * from '.$table. $whr;
			if($table=='sma_drft_po'){
                $sql = 'select sma_drft_po.* from '.$table. $whr;    
                }else{
                    $sql = 'select * from '.$table. $whr;
                }                


		  $result = mysqli_query($connection, $sql) or die("Selection Error " . mysqli_error($connection));

                if (!is_dir($directory)) { //create the folder if it's not already exists
                      mkdir($directory, 0755, TRUE);  
                 }
                $locDir = $directory."/".$projName;
                if (!is_dir($locDir)) { 
                    
                    mkdir($locDir, 0755, TRUE); 
                } 
                //$filePaths =  $dateWise."/".$directory."/";
                $fp = fopen($filePath. $csv_filename, 'w');
                while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
                {   
                    fputcsv($fp, $row);
                    // if($table!='sma_categories' && $table!='sma_companies' && $table!='sma_currencies' && $table!='sma_drft_po'){
                    // $q1 = "UPDATE ".$table." SET pos_create_flg = '1',pos_upd_flg = NULL WHERE id = ".$row[id]." ";
                    // $r1 = mysqli_query($connection, $q1) or die("Selection Error " . mysqli_error($connection));
                    // }
                }
                fclose($fp); 

                              
             }   

            $source_dir = $directory.$projName.'/';
           
            $zip_file = $directory.$projName.'.zip';
            $zip = new ZipArchive();

            if ($zip->open($zip_file, ZIPARCHIVE::CREATE) === true) {
             foreach (glob($source_dir."*") as $file) {

                $files = explode("/",$file)[count(explode("/",$file))-1];
                $zip->addFile($file,$files);                
               // if ($file != 'target_folder/important.txt') unlink($file);
            }
              $zip->close();
              rmdir($directory.$projName);
              //exit;

             }

           
        }

         $host = $this->db->hostname;//"localhost";
         $username = $this->db->username;//"root";
         $password = $this->db->password;//"";
         $dbname = $this->db->database;//"iffco_live";
         $posDB = $this->posDbName();
         $connection = mysqli_connect($host, $username, $password, $dbname) or die("Connection Error " . mysqli_error($connection));
                /* disable autocommit */
                  mysqli_autocommit($connection, FALSE);    

    		$this->db->select('COUNT(*)')
            	->from('sync_table')
            	->get()->row();
            for ($x = 1; $x <= $total_segments; $x++) {
              
                $this->db->select("*");
                $this->db->from("sync_table");       
                $tableName = $this->db->where("id",$x)->get()->row();               
                $table = $tableName->table_name;
                              if($table!='sma_categories' && $table!='sma_companies' && $table!='sma_currencies' && $table!='sma_drft_po'){
                 $q1 = "UPDATE ".$table." SET pos_create_flg = '1',pos_upd_flg = NULL WHERE 1 ";
                    if(mysqli_query($connection, $q1))
                     {
                                                 mysqli_commit($connection);
                     }
                      else{
                                                           mysqli_rollback($connection);

                     } 
                    }
            } 

      echo "zip created successfully";exit;  
        
     }
        
}
