<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pos_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function getSetting()
    {
        $q = $this->db->get('pos_settings');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateSetting($data)
    {
        $this->db->where('pos_id', '1');
        if ($this->db->update('pos_settings', $data)) {
            return true;
        }
        return false;
    }

    public function products_count($category_id, $subcategory_id = NULL)
    {
        $this->db->where('category_id', $category_id)->from('products');
        if ($subcategory_id) {
            $this->db->where('subcategory_id', $subcategory_id);
        }
        return $this->db->count_all_results();
    }

  /*  public function fetch_products($category_id, $limit, $start, $subcategory_id = NULL)
    {
       //echo "cat=>".$category_id.'##'.$subcategory_id.'##'.$_SESSION['warehouse_id'].'##'.$start;die;
         $this->db->limit($limit, $start);
         $this->db->where('category_id', $category_id);
         $this->db->where('quantity >', 0);
           if ($subcategory_id) {
              $this->db->where('subcategory_id', $subcategory_id);
          }
		
		 // // if(!empty($_SESSION['warehouse_id'])){
		 // // 	$this->db->where('warehouse', $_SESSION['warehouse_id']);	
		 // // }
		
          $this->db->group_by("name", "asc");
         $query = $this->db->get("products");
         if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
             
                
    }*/


    public function fetch_products($category_id, $limit, $start, $subcategory_id = NULL)
    {
       
   //echo "cat=>".$category_id.'##'.$subcategory_id.'##'.$_SESSION['warehouse_id'].'##'.$start;die;
         $this->db->limit($limit, $start);
         $this->db->where('category_id', $category_id);
         $this->db->where('quantity >', 0);
           if ($subcategory_id) {
              $this->db->where('subcategory_id', $subcategory_id);
          }
    
     // // if(!empty($_SESSION['warehouse_id'])){
     // //  $this->db->where('warehouse', $_SESSION['warehouse_id']); 
     // // }
    
          $this->db->group_by("name", "asc");
         $query = $this->db->get("products");
         if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
       
             
                
    }


    public function convertNumber($number){
       $no = floor($number);
       $point = round($number - $no, 2) * 100;
        if($point < 0){
           $point = 100 + $point;
       }
       $hundred = null;
       $digits_1 = strlen($no);
       $i = 0;
       $str = array();
       $words = array('0' => '', '1' => 'One', '2' => 'Two',
        '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
        '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
        '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
        '13' => 'Thirteen', '14' => 'Fourteen',
        '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
        '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
        '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
        '60' => 'Sixty', '70' => 'Seventy',
        '80' => 'Eighty', '90' => 'Ninety');
       $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
       while ($i < $digits_1) {
         $divider = ($i == 2) ? 10 : 100;
         $number = floor($no % $divider);
         $no = floor($no / $divider);
         $i += ($divider == 10) ? 1 : 2;
         if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number] .
                " " . $digits[$counter] . $plural . " " . $hundred
                :
                $words[floor($number / 10) * 10]
                . " " . $words[$number % 10] . " "
                . $digits[$counter] . $plural . " " . $hundred;
         } else $str[] = null;
      }
      $str = array_reverse($str);
      $result = implode('', $str);
      $points = ($point) ?
        "and " . $words[$point / 10] . " " . 
              $words[$point = $point % 10] : '';
      if($points!=null){
      
      echo $result . " " . $points;}
      else {
          echo $result;
      }
                 
    }
    
    public function convertWords($number){
       $no = floor($number);
       $point = round($number - $no, 2) * 100;
        if($point < 0){
           $point = 100 + $point;
       }
       $hundred = null;
       $digits_1 = strlen($no);
       $i = 0;
       $str = array();
       $words = array('0' => '', '1' => 'One', '2' => 'Two',
        '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
        '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
        '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
        '13' => 'Thirteen', '14' => 'Fourteen',
        '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
        '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
        '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
        '60' => 'Sixty', '70' => 'Seventy',
        '80' => 'Eighty', '90' => 'Ninety');
       $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
       while ($i < $digits_1) {
         $divider = ($i == 2) ? 10 : 100;
         $number = floor($no % $divider);
         $no = floor($no / $divider);
         $i += ($divider == 10) ? 1 : 2;
         if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number] .
                " " . $digits[$counter] . $plural . " " . $hundred
                :
                $words[floor($number / 10) * 10]
                . " " . $words[$number % 10] . " "
                . $digits[$counter] . $plural . " " . $hundred;
         } else $str[] = null;
      }
      $str = array_reverse($str);
      $result = implode('', $str);
      $points = ($point) ?
        "and " . $words[$point / 10] . " " . 
              $words[$point = $point % 10] : '';
      if($points!=null){
      
      echo $result . "Rupees  " . $points . " Paise";}
      else {
          echo $result . "Rupees  ";
      }
                 
    }

    public function registerData($user_id)
    {
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $q = $this->db->get_where('pos_register', array('user_id' => $user_id, 'status' => 'open'), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function openRegister($data)
    {
        if ($this->db->insert('pos_register', $data)) {
            return true;
        }
        return FALSE;
    }

    public function getOpenRegisters()
    {
        $this->db->select("date, user_id, cash_in_hand, CONCAT(" . $this->db->dbprefix('users') . ".first_name, ' ', " . $this->db->dbprefix('users') . ".last_name, ' - ', " . $this->db->dbprefix('users') . ".email) as user", FALSE)
            ->join('users', 'users.id=pos_register.user_id', 'left');
        $q = $this->db->get_where('pos_register', array('status' => 'open'));
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;

    }

    public function closeRegister($rid, $user_id, $data)
    {
        if (!$rid) {
            $rid = $this->session->userdata('register_id');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        if ($data['transfer_opened_bills'] == -1) {
            $this->db->delete('suspended_bills', array('created_by' => $user_id));
        } elseif ($data['transfer_opened_bills'] != 0) {
            $this->db->update('suspended_bills', array('created_by' => $data['transfer_opened_bills']), array('created_by' => $user_id));
        }
        if ($this->db->update('pos_register', $data, array('id' => $rid, 'user_id' => $user_id))) {
            return true;
        }
        return FALSE;
    }

    public function getUsers()
    {
        $q = $this->db->get_where('users', array('company_id' => NULL));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductsByCode($code)
    {
        $this->db->like('code', $code, 'both')->order_by("code");
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }


   /* public function getWHProduct($code, $warehouse_id, $customer_id, $cat_id)
    
    public function getWHProduct($code, $warehouse_id, $customer_id)
    {
        
          $this->db->select('products.id, code, name, type, warehouses_products.quantity, price, tax_rate, tax_method')
            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->group_by('products.id');
            $q = $this->db->get_where("products", array('products.id' => $code));


        // $this->db->select('products.id, code, name, type, warehouses_products.quantity, price, tax_rate, tax_method')
        //     ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
        //     ->group_by('products.id');
        //     $where = '(products.id=4 or products.id = 1)';
        //     $this->db->where($where);
        //      $q = $this->db->get("products");
         }
         else{
            // echo "code=>".$code;die;
            //  $this->db->select('products.id, code, name, type, warehouses_products.quantity, price, tax_rate, tax_method')
            // ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            // ->group_by('products.id');
            // $where = '(products.id=4 or products.id = 1)';
            // $this->db->where($where);
            //  $q = $this->db->get("products");

             $w= "SELECT a.id,a.code,a.name,a.type,b.quantity,a.price,a.tax_rate,a.tax_method FROM sma_kit k LEFT JOIN
  sma_products a ON (k.product_id = a.id) LEFT JOIN sma_warehouses_products b ON (b.product_id = a.id) WHERE 
    k.kit_id = ".$code." AND b.warehouse_id = ".$warehouse_id."";
              $q=$this->db->query($w);
              //print_r($q->result());

         }   
       

        // $q= "SELECT a.id,a.CODE,a.NAME,a.TYPE,b.quantity,a.price,a.tax_rate,a.tax_method FROM sma_products a LEFT JOIN sma_warehouses_products b ON(b.product_id = a.id) WHERE b.warehouse_id = 1 AND (a.id = 4 OR a.id = 1) ";
        if ($q->num_rows() > 0) {
           // print_r($q->result()); die;
           // return $q->row();
            //print_r($q->result()); die;
            return $q->result();
        }
         return FALSE;
    }*/

    public function getWHProduct($code, $warehouse_id, $customer_id)
    {
     
        
//          $this->db->select('products.id, code, name, type, warehouses_products.quantity, price, tax_rate, tax_method')
//            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
//            ->group_by('products.id');
//            $q = $this->db->get_where("products", array('products.id' => $code));
        
           $this->db->select("pi.*,isp.*,isl.lot_no as nlot_no,islb.bin_id as nbid");
           $this->db->from("purchase_items pi");
           $this->db->join("item_stk_profile isp","pi.id = isp.item_id AND pi.warehouse_id = isp.warehouse_id","inner");
           $this->db->join("item_stk_lot isl","isl.item_stk_id = isp.id AND isl.warehouse_id = isp.warehouse_id","inner");
           $this->db->join("item_stk_lot_bin islb","islb.lot_id = isl.id AND islb.warehouse_id = isl.warehouse_id","inner");
           $this->db->where_in('pi.warehouse_id',explode(",",$warehouse_id));
           $this->db->where("pi.id",$code);
           $this->db->where("isp.available_stk >", 0);
           $this->db->where("isl.total_stk >", 0);
           $this->db->where("pi.subtotal >", 0);
           $this->db->group_by("pi.product_id");
           $this->db->order_by("isl.created_date","ASC");
           $q = $this->db->get();
        
              if ($q->num_rows() > 0) {
                  return $q->row();
               }
           return FALSE;
      }
    
    public function getProductOptions($product_id, $warehouse_id)
    {
        $this->db->select('product_variants.id as id, product_variants.name as name, product_variants.price as price, product_variants.quantity as total_quantity, warehouses_products_variants.quantity as quantity')
            ->join('warehouses_products_variants', 'warehouses_products_variants.option_id=product_variants.id', 'left')
            //->join('warehouses', 'warehouses.id=product_variants.warehouse_id', 'left')
            ->where('product_variants.product_id', $product_id)
            ->where('warehouses_products_variants.warehouse_id', $warehouse_id)
            ->group_by('product_variants.id');
            if(! $this->Settings->overselling) {
                $this->db->where('warehouses_products_variants.quantity >', 0);
            }
        $q = $this->db->get('product_variants');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductComboItems($pid, $warehouse_id)
    {
        $this->db->select('products.id as id, combo_items.item_code as code, combo_items.quantity as qty, products.name as name, products.type as type, warehouses_products.quantity as quantity')
            ->join('products', 'products.code=combo_items.item_code', 'left')
            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->where('warehouses_products.warehouse_id', $warehouse_id)
            ->group_by('combo_items.id');
        $q = $this->db->get_where('combo_items', array('combo_items.product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return FALSE;
    }

    public function updateOptionQuantity($option_id, $quantity)
    {
        if ($option = $this->getProductOptionByID($option_id)) {
            $nq = $option->quantity - $quantity;
            if ($this->db->update('product_variants', array('quantity' => $nq), array('id' => $option_id))) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function addOptionQuantity($option_id, $quantity)
    {
        if ($option = $this->getProductOptionByID($option_id)) {
            $nq = $option->quantity + $quantity;
            if ($this->db->update('product_variants', array('quantity' => $nq), array('id' => $option_id))) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function getProductOptionByID($id)
    {
        $q = $this->db->get_where('product_variants', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPurchasedItems($product_id, $warehouse_id)
    {
        $orderby = ($this->Settings->accounting_method == 1) ? 'asc' : 'desc';
        $this->db->select('id, quantity, quantity_balance, net_unit_cost, item_tax');
        $this->db->where('product_id', $product_id)->where('warehouse_id', $warehouse_id)->where('quantity_balance !=', 0);
        
        $this->db->group_by('id');
        $this->db->order_by('date', $orderby);
        $this->db->order_by('purchase_id', $orderby);
        $q = $this->db->get('purchase_items');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductWarehouseOptionQty($option_id, $warehouse_id)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateProductOptionQuantity($option_id, $warehouse_id, $quantity, $product_id)
    {
        if ($option = $this->getProductWarehouseOptionQty($option_id, $warehouse_id)) {
            $nq = $option->quantity - $quantity;
            if ($this->db->update('warehouses_products_variants', array('quantity' => $nq), array('option_id' => $option_id, 'warehouse_id' => $warehouse_id))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return TRUE;
            }
        } else {
            $nq = 0 - $quantity;
            if ($this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $nq))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return TRUE;
            }
        }
        return FALSE;
    }

    public function addSale($data = array(), $items = array(), $payments = array(), $sid = NULL, $cheque_data = array())
    {     
        $cost = $this->site->costing($items);
        
        // $this->sma->print_arrays($cost);       
        if ($this->db->insert('sales', $data)) {
            
            $sale_id = $this->db->insert_id();
             
            $this->site->updateReference('pos');
            foreach ($items as $keys=>$item) {
                $item['sale_id'] = $sale_id;
                $this->db->insert('sale_items', $item);
                $sale_item_id = $this->db->insert_id();
               // array_push($sale_item_array,$sale_item_id);
                //$data['sale_status'] == 'completed' && 
                if ($this->site->getProductByID($item['product_id'])) {
                    $item_costs = $this->site->item_costing($item);
                    foreach ($item_costs as $item_cost) {
                        $item_cost['sale_item_id'] = $sale_item_id;
                        $item_cost['sale_id'] = $sale_id;
                        if(!isset($item_cost['pi_overselling'])) {
                            $this->db->insert('costing', $item_cost);
                        }
                    }

                }
                $items[$keys]['sale_item_id'] = $sale_item_id;
            }

            if ($data['sale_status'] == 'completed') {               
                $this->site->syncPurchaseItems($cost);                
                $this->site->syncQtyPurchaseItems($items,$sale_id,$sale_item_array);
                
            }

            $msg = array();
            if(count($cheque_data)>0){
              foreach($cheque_data as $val){
                $val['sale_id'] = $sale_id; 
                $val['reference_no'] = $data['reference_no'];
                $this->db->insert('cheque_details',$val);
              }
            }
            if (!empty($payments)) {
                $paid = 0;
                foreach ($payments as $payment) {
                    if (!empty($payment) && isset($payment['amount']) && $payment['amount'] != 0) {
                        $payment['sale_id'] = $sale_id;
                        if ($payment['paid_by'] == 'ppp') {
                            $card_info = array("number" => $payment['cc_no'], "exp_month" => $payment['cc_month'], "exp_year" => $payment['cc_year'], "cvc" => $payment['cc_cvv2'], 'type' => $payment['cc_type']);
                            $result = $this->paypal($payment['amount'], $card_info);
                            if (!isset($result['error'])) {
                                $payment['transaction_id'] = $result['transaction_id'];
                                $payment['date'] = $this->sma->fld($result['created_at']);
                                $payment['amount'] = $result['amount'];
                                $payment['currency'] = $result['currency'];
                                unset($payment['cc_cvv2']);
                                $this->db->insert('payments', $payment);
                                $this->site->updateReference('pay');
                                $paid += $payment['amount'];
                            } else {
                                $msg[] = lang('payment_failed');
                                if (!empty($result['message'])) {
                                    foreach ($result['message'] as $m) {
                                        $msg[] = '<p class="text-danger">' . $m['L_ERRORCODE'] . ': ' . $m['L_LONGMESSAGE'] . '</p>';
                                    }
                                } else {
                                    $msg[] = lang('paypal_empty_error');
                                }
                            }
                        } elseif ($payment['paid_by'] == 'stripe') {
                            $card_info = array("number" => $payment['cc_no'], "exp_month" => $payment['cc_month'], "exp_year" => $payment['cc_year'], "cvc" => $payment['cc_cvv2'], 'type' => $payment['cc_type']);
                            $result = $this->stripe($payment['amount'], $card_info);
                            if (!isset($result['error'])) {
                                $payment['transaction_id'] = $result['transaction_id'];
                                $payment['date'] = $this->sma->fld($result['created_at']);
                                $payment['amount'] = $result['amount'];
                                $payment['currency'] = $result['currency'];
                                unset($payment['cc_cvv2']);
                                $this->db->insert('payments', $payment);
                                $this->site->updateReference('pay');
                                $paid += $payment['amount'];
                            } else {
                                $msg[] = lang('payment_failed');
                                $msg[] = '<p class="text-danger">' . $result['code'] . ': ' . $result['message'] . '</p>';
                            }
                        } else {
                            if ($payment['paid_by'] == 'gift_card') {
              								$q = $this->db->where('card_no',$payment['cc_no'])->get('gift_cards');
              								$balance = $q->row()->balance - $payment['pos_paid'];
								
                              $this->db->update('gift_cards', array('balance' => $balance), array('card_no' => $payment['cc_no']));
                            }
							
							             if ($payment['paid_by'] == 'credit_voucher') { 
								              $q = $this->db->where('card_no',$payment['cv_no'])->get('credit_voucher');
								              $balance = $q->row()->balance - $payment['pos_paid'];
								
                              $this->db->update('credit_voucher', array('balance' => $balance), array('card_no' => $payment['cv_no']));
                            }
							               
                            unset($payment['cc_cvv2']);
                            $this->db->insert('payments', $payment);
                            if($this->db->insert_id()){
                              // foreach($cheque_data as $val){
                              //   $val['sale_id'] = $sale_id;   
                              //   $this->db->insert('cheque_details',$val);
                              // }                                                       
                              
                            }
                            $this->site->updateReference('pay');
                            $paid += $payment['amount'];
                        }
                    }
                }
                $this->site->syncSalePayments($sale_id, $cheque_data);
            }

            $this->site->syncQuantity($sale_id);
            if ($sid) {
                $this->deleteBill($sid);
            }
            $this->sma->update_award_points($data['grand_total'], $data['customer_id'], $data['created_by']);
            return array('sale_id' => $sale_id, 'message' => $msg);

        }

        return false;
    }

    public function getProductByCode($code)
    {
        $q = $this->db->get_where('products', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductByName($name)
    {
        $q = $this->db->get_where('products', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getAllBillerCompanies()
    {
        $q = $this->db->get_where('companies', array('group_name' => 'biller'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getAllCustomerCompanies()
    {
        $q = $this->db->get_where('companies', array('group_name' => 'customer'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getCompanyByID($id)
    {

        $q = $this->db->get_where('companies', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getAllProducts()
    {
        $q = $this->db->query('SELECT * FROM products ORDER BY id');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getProductByID($id)
    {

        $q = $this->db->get_where('products', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getAllTaxRates()
    {
        $q = $this->db->get('tax_rates');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getTaxRateByID($id)
    {

        $q = $this->db->get_where('tax_rates', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function updateProductQuantity($product_id, $warehouse_id, $quantity)
    {

        if ($this->addQuantity($product_id, $warehouse_id, $quantity)) {
            return true;
        }

        return false;
    }

    public function addQuantity($product_id, $warehouse_id, $quantity)
    {
        if ($warehouse_quantity = $this->getProductQuantity($product_id, $warehouse_id)) {
            $new_quantity = $warehouse_quantity['quantity'] - $quantity;
            if ($this->updateQuantity($product_id, $warehouse_id, $new_quantity)) {
                $this->site->syncProductQty($product_id, $warehouse_id);
                return TRUE;
            }
        } else {
            if ($this->insertQuantity($product_id, $warehouse_id, -$quantity)) {
                $this->site->syncProductQty($product_id, $warehouse_id);
                return TRUE;
            }
        }
        return FALSE;
    }

    public function insertQuantity($product_id, $warehouse_id, $quantity)
    {
        if ($this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $quantity))) {
            return true;
        }
        return false;
    }

    public function updateQuantity($product_id, $warehouse_id, $quantity)
    {
        if ($this->db->update('warehouses_products', array('quantity' => $quantity), array('product_id' => $product_id, 'warehouse_id' => $warehouse_id))) {
            return true;
        }
        return false;
    }

    public function getProductQuantity($product_id, $warehouse)
    {
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse), 1);
        if ($q->num_rows() > 0) {
            return $q->row_array(); //$q->row();
        }
        return FALSE;
    }

    public function getItemByID($id)
    {
        $q = $this->db->get_where('sale_items', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllSales()
    {
        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function sales_count()
    {
        return $this->db->count_all("sales");
    }

    public function fetch_sales($limit, $start)
    {
        $this->db->limit($limit, $start);
        $this->db->order_by("id", "desc");
        $query = $this->db->get("sales");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

   public function getAllInvoiceItems($sale_id)
    {
//        $this->db->select('sale_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, '
//                . 'tax_rates.rate as tax_rate, product_variants.name as variant,'
//                . 'purchase_items.net_unit_cost as unit_cost,' 
//                . 'purchase_items.item_tax as unit_tax,  purchase_items.lot_no, '
//                . 'purchase_items.tax as tax_value, sales.date, '
//                . 'sales.total_discount as discount_value')
       //added by vikas singh
          $this->db->select('sale_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, 
                    tax_rates.rate as tax_rate, product_variants.name as variant,
                    purchase_items.net_unit_cost as unit_cost,
                    purchase_items.item_tax as unit_tax, bin.bin_nm as bin_name, 
                    purchase_items.tax as tax_value, sales.date, 
                     sales.total_discount as discount_value')
            ->join('tax_rates', 'tax_rates.id=sale_items.tax_rate_id', 'left')
            ->join('purchase_items', 'purchase_items.product_id=sale_items.product_id', 'left')
            ->join("bin","sale_items.bin_id = bin.id","left")
            ->join('product_variants', 'product_variants.id=sale_items.option_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->group_by('sale_items.id')
            ->order_by('id', 'asc');
        $q = $this->db->get_where('sale_items', array('sale_id' => $sale_id));
       
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSuspendedSaleItems($id)
    {
        $q = $this->db->get_where('suspended_items', array('suspend_id' => $id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getSuspendedSales($user_id = NULL)
    {
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $q = $this->db->get_where('suspended_bills', array('created_by' => $user_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }


    public function getOpenBillByID($id)
    {

        $q = $this->db->get_where('suspended_bills', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getInvoiceByID($id)
    {

        $q = $this->db->get_where('sales', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getInvoiceNote($id)
    {
        $this->db->select('paid_by,note')
                 ->from('payments')
                 ->where('sale_id',$id);
        $q= $this->db->get();
        
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function bills_count()
    {
        if (!$this->Owner && !$this->Admin) {
            $this->db->where('created_by', $this->session->userdata('user_id'));
        }
        return $this->db->count_all_results("suspended_bills");
    }

    public function fetch_bills($limit, $start)
    {
        if (!$this->Owner && !$this->Admin) {
            $this->db->where('created_by', $this->session->userdata('user_id'));
        }
        $this->db->limit($limit, $start);
        $this->db->order_by("id", "asc");
        $query = $this->db->get("suspended_bills");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getTodaySales()
    {
        $date = date('Y-m-d 00:00:00');
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >', $date);

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getCosting()
    {
        $date = date('Y-m-d');
        $this->db->select('SUM( COALESCE( purchase_unit_cost, 0 ) * quantity ) AS cost, SUM( COALESCE( sale_unit_price, 0 ) * quantity ) AS sales, SUM( COALESCE( purchase_net_unit_cost, 0 ) * quantity ) AS net_cost, SUM( COALESCE( sale_net_unit_price, 0 ) * quantity ) AS net_sales', FALSE)
            ->where('date', $date);

        $q = $this->db->get('costing');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getTodayCCSales()
    {
        $date = date('Y-m-d 00:00:00');
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cc_slips, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >', $date)->where('paid_by', 'CC');

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getTodayCashSales()
    {
        $date = date('Y-m-d 00:00:00');
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >', $date)->where('paid_by', 'cash');

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getTodayRefunds()
    {
        $date = date('Y-m-d 00:00:00');
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS returned', FALSE)
            ->join('return_sales', 'return_sales.id=payments.return_id', 'left')
            ->where('type', 'returned')->where('payments.date >', $date);

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getTodayExpenses()
    {
        $date = date('Y-m-d 00:00:00');
        $this->db->select('SUM( COALESCE( amount, 0 ) ) AS total', FALSE)
            ->where('date >', $date);

        $q = $this->db->get('expenses');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getTodayCashRefunds()
    {
        $date = date('Y-m-d 00:00:00');
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS returned', FALSE)
            ->join('return_sales', 'return_sales.id=payments.return_id', 'left')
            ->where('type', 'returned')->where('payments.date >', $date)->where('paid_by', 'cash');

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getTodayChSales()
    {
        $date = date('Y-m-d 00:00:00');
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >', $date)->where('paid_by', 'Cheque');

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getTodayPPPSales()
    {
        $date = date('Y-m-d 00:00:00');
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >', $date)->where('paid_by', 'ppp');

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getTodayStripeSales()
    {
        $date = date('Y-m-d 00:00:00');
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >', $date)->where('paid_by', 'stripe');

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'inner')
            ->where('type', 'received')->where('payments.date >', $date);
           // ->group_by("sale_id");
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return false;
    }


    public function getRegisterCCSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $date = $this->registerData($user_id)->date;
        
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cc_slips, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >', $date)->where('paid_by', 'cc');
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterCashSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        } 
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >', $date)->where('paid_by', 'cash');
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterRefunds($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS returned', FALSE)
            ->join('return_sales', 'return_sales.id=payments.return_id', 'left')
            ->where('type', 'returned')->where('payments.date >', $date);
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterCashRefunds($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS returned', FALSE)
            ->join('return_sales', 'return_sales.id=payments.return_id', 'left')
            ->where('type', 'returned')->where('payments.date >', $date)->where('paid_by', 'cash');
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterExpenses($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('SUM( COALESCE( amount, 0 ) ) AS total', FALSE)
            ->where('date >', $date);
        $this->db->where('created_by', $user_id);

        $q = $this->db->get('expenses');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterChSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >', $date)->where('paid_by', 'cheque');
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterPPPSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >', $date)->where('paid_by', 'ppp');
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getRegisterStripeSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('COUNT(' . $this->db->dbprefix('payments') . '.id) as total_cheques, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >', $date)->where('paid_by', 'stripe');
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getDailySales($year, $month)
    {

        $myQuery = "SELECT DATE_FORMAT( date,  '%e' ) AS date, SUM( COALESCE( total, 0 ) ) AS total
        FROM " . $this->db->dbprefix('sales') . "
        WHERE DATE_FORMAT( date,  '%Y-%m' ) =  '{$year}-{$month}'
        GROUP BY DATE_FORMAT( date,  '%e' )";
        $q = $this->db->query($myQuery, false);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getMonthlySales($year)
    {

        $myQuery = "SELECT DATE_FORMAT( date,  '%c' ) AS date, SUM( COALESCE( total, 0 ) ) AS total
        FROM " . $this->db->dbprefix('sales') . "
        WHERE DATE_FORMAT( date,  '%Y' ) =  '{$year}'
        GROUP BY date_format( date, '%c' ) ORDER BY date_format( date, '%c' ) ASC";
        $q = $this->db->query($myQuery, false);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function suspendSale($data = array(), $items = array(), $did = NULL)
    {
     
        $sData = array(
            'count' => $data['total_items'],
            'biller_id' => $data['biller_id'],
            'customer_id' => $data['customer_id'],
            'warehouse_id' => $data['warehouse_id'],
            'customer' => $data['customer'],
            'date' => $data['date'],
            'suspend_note' => $data['suspend_note'],
            'total' => $data['grand_total'],
            'order_tax_id' => $data['order_tax_id'],
            'order_discount_id' => $data['order_discount_id'],
            'created_by' => $this->session->userdata('user_id')
        );

        if ($did) {

            if ($this->db->update('suspended_bills', $sData, array('id' => $did)) && $this->db->delete('suspended_items', array('suspend_id' => $did))) {
                $addOn = array('suspend_id' => $did);
                end($addOn);
                foreach ($items as &$var) {
                    $var = array_merge($addOn, $var);
                }
                if ($this->db->insert_batch('suspended_items', $items)) {
                    return TRUE;
                }
            }

        } else {

            if ($this->db->insert('suspended_bills', $sData)) {
                $suspend_id = $this->db->insert_id();
                $addOn = array('suspend_id' => $suspend_id);
                end($addOn);
                foreach ($items as &$var) {
                    $var = array_merge($addOn, $var);
                }
                if ($this->db->insert_batch('suspended_items', $items)) {
                    return TRUE;
                }
            }

        }
        return FALSE;
    }

    public function deleteBill($id)
    {

        if ($this->db->delete('suspended_items', array('suspend_id' => $id)) && $this->db->delete('suspended_bills', array('id' => $id))) {
            return true;
        }

        return FALSE;
    }

    public function getSubCategoriesByCategoryID($category_id)
    {
        $this->db->order_by('name');
        $q = $this->db->get_where("subcategories", array('category_id' => $category_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return FALSE;
    }

    public function getInvoicePayments($sale_id)
    {
        $q = $this->db->get_where("payments", array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return FALSE;
    }

    function stripe($amount = 0, $card_info = array(), $desc = '')
    {
        $this->load->model('stripe_payments');
        //$card_info = array( "number" => "4242424242424242", "exp_month" => 1, "exp_year" => 2016, "cvc" => "314" );
        //$amount = $amount ? $amount*100 : 3000;
        $amount = $amount * 100;
        if ($amount && !empty($card_info)) {
            $token_info = $this->stripe_payments->create_card_token($card_info);
            if (!isset($token_info['error'])) {
                $token = $token_info->id;
                $data = $this->stripe_payments->insert($token, $desc, $amount, $this->default_currency->code);
                if (!isset($data['error'])) {
                    $result = array('transaction_id' => $data->id,
                        'created_at' => date($this->dateFormats['php_ldate'], $data->created),
                        'amount' => ($data->amount / 100),
                        'currency' => strtoupper($data->currency)
                    );
                    return $result;
                } else {
                    return $data;
                }
            } else {
                return $token_info;
            }
        }
        return false;
    }

    function paypal($amount = NULL, $card_info = array(), $desc = '')
    {
        $this->load->model('paypal_payments');
        //$card_info = array( "number" => "5522340006063638", "exp_month" => 2, "exp_year" => 2016, "cvc" => "456", 'type' => 'MasterCard' );
        //$amount = $amount ? $amount : 30.00;
        if ($amount && !empty($card_info)) {
            $data = $this->paypal_payments->Do_direct_payment($amount, $this->default_currency->code, $card_info, $desc);
            if (!isset($data['error'])) {
                $result = array('transaction_id' => $data['TRANSACTIONID'],
                    'created_at' => date($this->dateFormats['php_ldate'], strtotime($data['TIMESTAMP'])),
                    'amount' => $data['AMT'],
                    'currency' => strtoupper($data['CURRENCYCODE'])
                );
                return $result;
            } else {
                return $data;
            }
        }
        return false;
    }

    public function addPayment($payment = array())
    {
        if (isset($payment['sale_id']) && isset($payment['paid_by']) && isset($payment['amount'])) {
            $payment['pos_paid'] = $payment['amount'];
            $inv = $this->getInvoiceByID($payment['sale_id']);
            $paid = $inv->paid + $payment['amount'];
            if ($payment['paid_by'] == 'ppp') {
                $card_info = array("number" => $payment['cc_no'], "exp_month" => $payment['cc_month'], "exp_year" => $payment['cc_year'], "cvc" => $payment['cc_cvv2'], 'type' => $payment['cc_type']);
                $result = $this->paypal($payment['amount'], $card_info);
                if (!isset($result['error'])) {
                    $payment['transaction_id'] = $result['transaction_id'];
                    $payment['date'] = $this->sma->fld($result['created_at']);
                    $payment['amount'] = $result['amount'];
                    $payment['currency'] = $result['currency'];
                    unset($payment['cc_cvv2']);
                    $this->db->insert('payments', $payment);
                    $paid += $payment['amount'];
                } else {
                    $msg[] = lang('payment_failed');
                    if (!empty($result['message'])) {
                        foreach ($result['message'] as $m) {
                            $msg[] = '<p class="text-danger">' . $m['L_ERRORCODE'] . ': ' . $m['L_LONGMESSAGE'] . '</p>';
                        }
                    } else {
                        $msg[] = lang('paypal_empty_error');
                    }
                }
            } elseif ($payment['paid_by'] == 'stripe') {
                $card_info = array("number" => $payment['cc_no'], "exp_month" => $payment['cc_month'], "exp_year" => $payment['cc_year'], "cvc" => $payment['cc_cvv2'], 'type' => $payment['cc_type']);
                $result = $this->stripe($payment['amount'], $card_info);
                if (!isset($result['error'])) {
                    $payment['transaction_id'] = $result['transaction_id'];
                    $payment['date'] = $this->sma->fld($result['created_at']);
                    $payment['amount'] = $result['amount'];
                    $payment['currency'] = $result['currency'];
                    unset($payment['cc_cvv2']);
                    $this->db->insert('payments', $payment);
                    $paid += $payment['amount'];
                } else {
                    $msg[] = lang('payment_failed');
                    $msg[] = '<p class="text-danger">' . $result['code'] . ': ' . $result['message'] . '</p>';
                }
            } else {
                if ($payment['paid_by'] == 'gift_card') {
                    $gc = $this->site->getGiftCardByNO($payment['cc_no']);
                    $this->db->update('gift_cards', array('balance' => ($gc->balance - $payment['amount'])), array('card_no' => $payment['cc_no']));
                }
                unset($payment['cc_cvv2']);
                $this->db->insert('payments', $payment);
                $paid += $payment['amount'];
            }
            if (!isset($msg)) {
                if ($this->site->getReference('pay') == $data['reference_no']) {
                    $this->site->updateReference('pay');
                }
                $this->site->syncSalePayments($payment['sale_id']);
                return array('status' => 1, 'msg' => '');
            }
            return array('status' => 0, 'msg' => $msg);

        }
        return false;
    }
    
        // Add By Ankit/Ajay for implements N level catogory
    public function getNSubCategoriesByCategoryID($category_id)
    {
        //echo "cat=>".$category_id;
        $this->db->order_by('name');
        $q = $this->db->get_where("categories", array('parent_id' => $category_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
           // echo "<pre>";print_r($data);
            return $data;
        }

        return FALSE;
    }
    
//        // Add By Ankit/Ajay for implements N level catogory
//    public function getNSubCategoriesByCategoryID($category_id)
//    {
//        //echo "cat=>".$category_id;
//        $this->db->order_by('name');
//        $q = $this->db->get_where("categories", array('parent_id' => $category_id));
//        if ($q->num_rows() > 0) {
//            foreach (($q->result()) as $row) {
//                $data[] = $row;
//            }
//           // echo "<pre>";print_r($data);
//            return $data;
//        }
// 
//        return FALSE;
//    }
    
        // Add by Ankit for N level category
    public function getAllChildCategory($category_id)
    {
        $q = "select parent.id as parent_id, child.id as child_id, group_concat(subchild.id) as subchils from sma_categories as 
        parent inner join sma_categories as child on (parent.id=child.parent_id) left join sma_categories as subchild on 
        (child.id=subchild.parent_id) where parent.id = ".$category_id." group by parent.id";
       $res = $this->db->query($q);
       if($res->num_rows() > 0){
           return $res->row();
       }
        return false; 
    }
    public function getAllSBChildCategory($subcategory_id)
    {
        $q="SELECT parent.id AS parent_id, GROUP_CONCAT(child.id) AS child FROM sma_categories AS parent INNER JOIN sma_categories AS child ON(parent.id = child.parent_id) WHERE parent.id = ".$subcategory_id." GROUP BY parent.id ";
        $res = $this->db->query($q);
           if($res->num_rows() > 0){
               return $res->row();
           }
            return false; 
    }
    
        // For N level category implementation Add by Ankit/Ajay
    public function fetch_productsN($category_id, $limit, $start, $subcategory_id = NULL,$pwh_id)
    { 
        
     //echo "cat=>".$category_id.'#Subcat=>'.$subcategory_id.'#wid=>'.$_SESSION['warehouse_id'].'#start=>'.$start."market".$whid;die; 
          // echo "ses".$wid; 
        //die;
      if(!empty($subcategory_id))
      {
          //echo "hi.....";
          $subcat= $this->getAllSBChildCategory($subcategory_id);
          $catarr = $this->convertInArray($subcat);
          //print_r($subcat); echo "<br>";  print_r($catarr); die;
      }
      else{
           $cat = $this->getAllChildCategory($category_id);
           $catarr = $this->convertInArray($cat);
       }

       $data = array();      
      
  for($i=0; $i<count($catarr); $i++){
           
//  $query =  $this->db->query("SELECT
//  a.*,
//  b.*,
//  c.total_stk,c.lot_no,
//  d.bin_id as binid
//  FROM
//  sma_purchase_items a
//INNER JOIN
//  sma_item_stk_profile b ON( a.id = b.pr_id AND b.warehouse_id = b.warehouse_id)
//INNER JOIN
//  sma_item_stk_lot c ON(c.item_stk_id = b.id AND b.warehouse_id = c.warehouse_id)
//INNER JOIN
//  sma_item_stk_lot_bin d ON(d.lot_id = c.id AND d.warehouse_id = c.warehouse_id)
//WHERE b.available_stk > 0  and  c.total_stk > 0 and category_id = 0 and a.warehouse_id = 2 and b.sales_price > 0 GROUP BY a.product_id ORDER BY c.created_date  desc");
//         $this->db->limit($limit, $start);
//         $this->db->where('category_id', $catarr[$i]);
//         $this->db->where('quantity >', 0);
//         $this->db->where('price >', 0);
//
//         $this->db->where_in('warehouse',explode(",",$whid));
//           if ($subcategory_id) {
//             $this->db->where('subcategory_id', $subcategory_id);
////          }
//         $this->db->group_by("name", "asc");
//         $query = $this->db->get("products");
          
           $this->db->select("pi.*,isp.*,isl.lot_no as nlot_no,islb.bin_id as nbid");
           $this->db->from("purchase_items pi");
           $this->db->join("item_stk_profile isp","pi.product_id = isp.item_id AND pi.warehouse_id = isp.warehouse_id","inner");
           $this->db->join("item_stk_lot isl","isl.item_stk_id = isp.id AND isl.warehouse_id = isp.warehouse_id","inner");
           $this->db->join("item_stk_lot_bin islb","islb.lot_id = isl.id AND islb.warehouse_id = isl.warehouse_id","left");
           $this->db->where('category_id', $catarr[$i]);
           $this->db->where('pi.warehouse_id',$pwh_id);
           //if (!$this->Settings->overselling) 
           $this->db->where("isp.available_stk >", 0);
           $this->db->where("isl.total_stk >", 0);
           $this->db->where("pi.subtotal >", 0);
           $this->db->group_by("pi.product_id");
           $this->db->order_by("isl.created_date","desc");
           $query = $this->db->get();
           
  
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
               // print_r($row); echo "<br>";
            }
         }
            //return $data;
        }
        if(!empty($data)){
            //print_r($data); die;
           return $data;
        }
        else {
           return false;
       
        }      
                
    }
    
    // For N level category implementation Add by Ankit/Ajay
    public function fetch_productsN2($limit, $start, $endcategory_id,$pwh_id)
    { 
      //echo "hi........".$endcategory_id; die;
      $this->db->limit($limit, $start);
//         $this->db->where('category_id', $endcategory_id);
//         $this->db->where('quantity >', 0);
//         $this->db->where('price >', 0);
////           if ($subcategory_id) {
////              $this->db->where('subcategory_id', $subcategory_id);
////          }
//         $this->db->group_by("name", "asc");
//         $query = $this->db->get("products");
         
         
           // added by vikas singh for product listing lot and bin wise
           $this->db->select("pi.*,isp.*,isl.lot_no as nlot_no,islb.bin_id as nbid");
           $this->db->from("purchase_items pi");
           $this->db->join("item_stk_profile isp","pi.product_id = isp.item_id AND pi.warehouse_id = isp.warehouse_id","inner");
           $this->db->join("item_stk_lot isl","isl.item_stk_id = isp.id AND isl.warehouse_id = isp.warehouse_id","inner");
           $this->db->join("item_stk_lot_bin islb","islb.lot_id = isl.id AND islb.warehouse_id = isl.warehouse_id","left");
           $this->db->where('category_id', $endcategory_id);
           $this->db->where('pi.warehouse_id',$pwh_id);
           if (!$this->Settings->overselling) 
           $this->db->where("isp.available_stk >", 0);
//         $this->db->where("isl.total_stk >", 0);
           $this->db->where("pi.subtotal >", 0);
           $this->db->group_by("pi.product_id");
           $this->db->order_by("isl.created_date","desc");
           $query = $this->db->get();
         if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
               // print_r($row); echo "<br>";
            }
            return $data;
         }
            //return $data;
        
        
           return false;
       
            
                
    }
    // Add by Ankit for N level category
    public function convertInArray($ar)
    {
            $i=0;
            $cats = array();
            if(!empty($ar))
       {
       foreach($ar as $val){
           $cats[$i] = $val;
           $i++;
       }
       $str = implode(',',$cats);
       $arr = explode(',',$str);
       }
       return $arr;
 
        
    }
    
         // Add By Ankit/Ajay for implements N level catogory
    public function getNEndCategoriesByCategoryID($category_id)
    {
        $cat = array();
        $this->db->order_by('name');
        $q = $this->db->get_where("categories", array('parent_id' => $category_id));
         if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row->id;
            }
            //echo "<pre>";print_r($data);
            //$s= $data[0]->id;
        }
        for($i=0;$i<count($data);$i++)
        {
        $this->db->order_by('name');
        $q = $this->db->get_where("categories", array('parent_id' => $data[$i]));
         if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $cat[] = $row;
            }

            //return $cat;
        }
            
        }
        

        return $cat;
    }
    // Add By Ankit/Ajay for implements N level catogory
    public function getNEndsCategoriesByCategoryID($category_id)
    {
        //echo "cat=>".$category_id;
        $this->db->order_by('name');
        $q = $this->db->get_where("categories", array('parent_id' => $category_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            //echo "<pre>";print_r($data);
            return $data;
        }

        return FALSE;
    }
    
    //added by vikas singh 
    public function myfunction($a,$b)
    {
    if($a===$b)
      {
      return 0;
      }
      return ($a>$b)?1:-1;
    }

    public function getProvisionalSale($id=0){
      $this->db->select('*')
      ->from('sales')     
      ->join('payments', 'sales.id=payments.sale_id', 'inner');
      //->join('cheque_details', 'sales.id=cheque_details.sale_id', 'inner');
      if($id)
        $this->db->where("sales.id",$id);
      $provSaleList = $this->db->where('sale_status', 'pending')->where('payment_status', 'due')->where('paid_by','cheque')->get()->result();    
      return $provSaleList;   

    }

     public function getProvisionalSaleItems($saleId){
        $provSaleItemList = $this->db->select('*')
        ->from('sale_items')     
        ->where("sale_id",$saleId)->get()->result();     
        return $provSaleItemList;   

    }

    public function getProvChequeDetail($saleId){
        $provSaleCheqList = $this->db->select('*')
        ->from('cheque_details')     
        ->where("sale_id",$saleId)->get()->result();     
        return $provSaleCheqList;   

    }

     public function getRegisterDDSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'inner')
            ->where('type', 'received')->where('payments.date >', $date)->where('paid_by', 'dd');
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

     public function getRegisterRTGSSales($date, $user_id = NULL)
    {
        if (!$date) {
            $date = $this->session->userdata('register_open_time');
        }
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( amount, 0 ) ) AS paid', FALSE)
            ->join('sales', 'sales.id=payments.sale_id', 'left')
            ->where('type', 'received')->where('payments.date >', $date)->where('paid_by', 'rtgs');
        $this->db->where('payments.created_by', $user_id);

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }


    public function getProductPrice($item_code){

      if (isset($item_code)) {
       $product_details = $item_type != 'manual' ? $this->getProductByCode($item_code) : NULL;
       $purchase_items = $this->getPurchaseByCode($item_code);
     /*  echo "<pre>";
       print_r($purchase_items);
       exit;*/
       $real_unit_price = $product_details->price;  
       $unit_price = $real_unit_price;        
      
      
       $item_tax_rate = $purchase_items->tax_rate;
      // $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : NULL;
       $pr_discount = 0;
      // if (isset($item_discount)) {
      //     $discount = $item_discount;
      //     $dpos = strpos($discount, $percentage);
      //     if ($dpos !== false) {
      //         $pds = explode("%", $discount);
      //         $pr_discount = (($this->sma->formatDecimal($unit_price)) * (Float)($pds[0])) / 100;
      //     } else {
      //         $pr_discount = $this->sma->formatDecimal($discount);
      //     }
      // }

      $unit_price = $this->sma->formatDecimal($unit_price - $pr_discount);
      $item_net_price = $unit_price;
     
      $pr_tax = 0; $pr_item_tax = 0; $item_tax = 0; $tax = "";
      if (isset($item_tax_rate) && $item_tax_rate != 0) {
          $pr_tax = $item_tax_rate;  
               
          $tax_details = $this->site->getTaxRateByID($pr_tax);
         
          if ($tax_details->type == 1 && $tax_details->rate != 0) {

              if ($purchase_items && $purchase_items->tax_method == 1) {
                  $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / 100);
                  $tax = $tax_details->rate . "%";
              } else {
                  $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / (100 + $tax_details->rate));
                  $tax = $tax_details->rate . "%";
                  $item_net_price = $unit_price - $item_tax;
              }

          } elseif ($tax_details->type == 2) {

              if ($purchase_items && $purchase_items->tax_method == 1) {
                  $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / 100);
                  $tax = $tax_details->rate . "%";
              } else {
                  $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / (100 + $tax_details->rate));
                  $tax = $tax_details->rate . "%";
                  $item_net_price = $unit_price - $item_tax;
              }

              $item_tax = $this->sma->formatDecimal($tax_details->rate);
              $tax = $tax_details->rate;

          }
          $pr_item_tax = $this->sma->formatDecimal($item_tax * $item_quantity);

      }

      //$product_tax += $pr_item_tax;
      $product_tax += $ru_item_tax;
      }
      return array('item_net_price'=>$item_net_price,'per_item_tax'=>$per_item_tax);
     // $subtotal = (($item_net_price * $item_quantity) + $pr_item_tax);
    }


    public function getPurchaseByCode($code)
    {
        $q = $this->db->get_where('purchase_items', array('product_code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }



    

}

