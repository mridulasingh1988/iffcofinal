<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Csv_model extends CI_Model
{
    
    public function __construct()
    {
        parent::__construct();
        //$this->load->library('multipledb');
        $this->load->model("auth_model");
        $this->upload_path = 'assets/csv/';
        $this->load->database();
    }
    

public function poMaster()
   {

       $connect = mysql_connect('localhost','root','');
      if (!$connect)
       {
         die('Could not <span id="IL_AD1" class="IL_AD">connect to</span> MySQL: ' . mysql_error());
       }

       $halfpath = str_replace('/index.php', '/', $_SERVER['SCRIPT_NAME']);
       $fullpath = $_SERVER['DOCUMENT_ROOT'].$halfpath;

       $cid = mysql_select_db('iffco_final',$connect); //specify db name
       define('CSV_PATH',$fullpath.$this->upload_path); // specify CSV file path
       $csv_file = CSV_PATH . "sma_drft_po.csv"; // Name of your CSV file
       $csvfile = fopen($csv_file, 'r');
       $theData = fgets($csvfile);
$i = 0;
$val= array();
while (!feof($csvfile))
{
   $csv_data = fgets($csvfile);
   $csv_array = explode(";", $csv_data);
  // print_r($csv_array); die;
   $val['org_name'] = $csv_array[0]=='KULAN'?4:6;
   $val['segment_id'] = $csv_array[0]=='KULAN'?2601:2627;
   $val['po_dt'] = $csv_array[2];
   $val['customer_id'] = $csv_array[3]=='1'?2610:2628;
   $val['tlrnc_days'] = $csv_array[5];
   $val['curr_conv_fctr']=$csv_array[7];
   $val['tlrnc_qty_type']=$csv_array[8];
   $val['tlrnc_qty_val']=$csv_array[9];
   $val['usr_id_create_dt']=$csv_array[10];
   $val['auth_po_no']=$csv_array[11];
   $val['doc_id']=rtrim($csv_array[13]);

   $q= "SELECT count(auth_po_no) AS cnt FROM sma_drft_po WHERE auth_po_no LIKE '%".$val['auth_po_no']."%'";
   $n1 = mysql_query($q, $connect );
   $p= mysql_fetch_row($n1);
   if($p[0] == 0)
   {
      $query = "INSERT INTO sma_drft_po(org_id,segment_id,po_dt,customer_id,bill_adds_id,tlrnc_days,curr_id_sp,curr_name,curr_conv_fctr,tlrnc_qty_type,tlrnc_qty_val,usr_id_create_dt,auth_po_no,fy_id,flg,status,doc_id) VALUES (".$val['org_name'].",".$val['segment_id'].",'".$val["po_dt"]."',".$val['customer_id'].",".$val['customer_id'].",".$val['tlrnc_days'].",73,'INDIAN RUPEE',".$val['curr_conv_fctr'].",'1',".$val['tlrnc_qty_val'].",'".$val['usr_id_create_dt']."','".$val['auth_po_no']."',1,'1',0,'".$val['doc_id']."')"; 
      $n=mysql_query($query, $connect );
   }
  $i++;
}
fclose($csvfile);
$s= "File data successfully imported to database!!";
mysql_close($connect); // closing connection
return true;
   }  




public function poSchdlMaster()
   {

       $connect = mysql_connect('localhost','root','');
      if (!$connect)
       {
         die('Could not <span id="IL_AD1" class="IL_AD">connect to</span> MySQL: ' . mysql_error());
       }

       $halfpath = str_replace('/index.php', '/', $_SERVER['SCRIPT_NAME']);
       $fullpath = $_SERVER['DOCUMENT_ROOT'].$halfpath;

       $cid = mysql_select_db('iffco_final',$connect); //specify db name
       define('CSV_PATH',$fullpath.$this->upload_path); // specify CSV file path
       $csv_file1 = CSV_PATH . "sma_drft_po_dlv_schdl.csv"; // Name of your CSV file
       //echo $csv_file1; die;
       $csvfile1 = fopen($csv_file1, 'r');
       $theData = fgets($csvfile1);
       //echo $theData; die;
$i = 0;
$val= array();
while (!feof($csvfile1))
{
   $csv_data1 = fgets($csvfile1);
   $csv_array = explode(";", $csv_data1);
  // print_r($csv_array); die;
   $val['org_name'] = trim($csv_array[0])=='KULAN'?WH000092E:WH000062E;
   $val['po_id'] = trim($csv_array[1]);
   $val['itm_name'] = ltrim($csv_array[2]);
   $val['tot_qty'] = trim($csv_array[3]);
   $val['dlv_qty'] = trim($csv_array[5]);
   $val['tlrnc_qty_type'] = trim($csv_array[6]);
   $val['tlrnc_qty_val'] = trim($csv_array[7]);
   $val['dlv_dt'] = trim($csv_array[8]);
   $val['wh_id'] = trim($csv_array[9])=='KULAN'?4:6;
   $val['usr_id_create_dt'] = trim($csv_array[11]);
   $val['dlv_schdl_no'] = trim($csv_array[12]);
   $val['itm_uom'] = trim($csv_array[13]);
   $val['itm_uom_desc'] = ltrim($csv_array[14]);
   $val['bal_qty'] = trim($csv_array[15]);
   $val['tlrnc_days_val'] = trim($csv_array[16]);
   $val['ro_no'] = ltrim($csv_array[17]);
   $val['ro_dt'] = trim($csv_array[18]);
         
   $q= "SELECT id FROM sma_drft_po WHERE doc_id = '".$val['po_id']."' ";
   $n1 = mysql_query($q, $connect );
   $p= mysql_fetch_row($n1);
   

   $q1= "SELECT id FROM sma_products WHERE name LIKE '%".$val['itm_name']."%' ";
   $n2 = mysql_query($q1, $connect );
   $p1= mysql_fetch_row($n2);
  // print_r($p1);

   $query = "INSERT INTO sma_drft_po_dlv_schdl(org_id,po_id,itm_id,itm_name,tot_qty,dlv_mode,dlv_qty,tlrnc_qty_type,  tlrnc_qty_val,dlv_dt,wh_id,dlv_adds_id,usr_id_create_dt,dlv_schdl_no,itm_uom,itm_uom_desc,bal_qty,tmp_rcpt_qty,tlrnc_days_val,segment_id,ro_no,ro_dt,flg) VALUES ('".$val['org_name']."',".$p[0].",".$p1[0].",'".$val['itm_name']."',".$val['tot_qty'].",163,".$val['dlv_qty'].",".$val['tlrnc_qty_type'].",".$val['tlrnc_qty_val'].",'".$val['dlv_dt']."',".$val['wh_id'].",'ADDS0000000005','".$val['usr_id_create_dt']."',".$val['dlv_schdl_no'].",'".$val['itm_uom']."','".$val['itm_uom_desc']."',".$val['bal_qty'].",0,".$val['tlrnc_days_val'].",1,'".$val['ro_no']."','".$val['ro_dt']."','0')";
 //echo "<br>";
 $n=mysql_query($query, $connect );
//die;
  $i++;
}
fclose($csvfile1);
$s= "File data successfully imported to database!!";
mysql_close($connect); // closing connection
return true;
   }   

  



    
}
?>