<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msync_model extends CI_Model
{
    
    public function __construct()
    {
        parent::__construct();
        //$this->load->library('multipledb');
        $this->load->model("auth_model");
        $this->load->database();
       // $this->restructureData();
    }
    
    public function intrDbName()
    {
        $CI = &get_instance();
        $this->db2 = $CI->load->database('db2', TRUE);
        $intrDB = $this->db2->database;
        return $intrDB;

    }
    public function posDbName()
    {
        $posDB = $this->db->database;
        return $posDB;

    }
    // S.No.-01 (Intermediate -> POS)
    
    public function currencyMaster()
    {
       // $ar = $this->currencyUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n  = '$';
        $w  = "INSERT INTO ".$posDB.".sma_currencies(code,name,rate,curr_id) SELECT a.CURR_NOTATION,a.CURR_NM,'1',a.CURR_ID FROM ".$intrDB.".intrm".$n."curr a WHERE a.POS_CURR_ID IS NULL OR a.POS_CURR_ID = ''";
        if ($this->db->query($w)) {
            $w1 = "UPDATE ".$intrDB.".intrm".$n."curr a INNER JOIN ".$posDB.".sma_currencies b ON a.CURR_ID = b.curr_id SET a.POS_CURR_ID = b.id,a.CREATE_FLG = '1' WHERE a.CURR_ID = b.curr_id";
            $this->db->query($w1);
            $msg = "Currency Records save successfully...";
            return $msg;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }
    
    public function currencyUPD()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_currencies a INNER JOIN ".$intrDB.".intrm".$n."curr b ON (a.curr_id = b.CURR_ID AND a.id = b.POS_CURR_ID) SET a.code = b.CURR_NOTATION,a.name = b.CURR_NM,a.pos_upd_flg='1' WHERE b.UPD_FLG= '3'";
        if ($this->db->query($w)) {
            $w1 = "UPDATE ".$intrDB.".intrm".$n."curr a INNER JOIN ".$posDB.".sma_currencies b ON (a.CURR_ID = b.curr_id AND b.id = a.POS_CURR_ID) SET a.UPD_FLG = '' WHERE (a.CURR_ID = b.curr_id AND b.id = a.POS_CURR_ID) AND a.UPD_FLG= '3'";
            $this->db->query($w1);
            return true;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }
    
    
    // S.NO-02 (Intermediate -> POS)
    public function warehouseMaster()
    {
        $ar = $this->warehouseMasterUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w1= "INSERT INTO ".$posDB.".sma_warehouses (org_id,wh_id,wh_type,usr_id_create,usr_id_create_dt,name,wh_onrshp_type,wh_strg_type,adds_id,wh_desc,actv,inactv_resn,inactv_dt,wh_ent_id,prj_id,segment_id,address) SELECT a.ORG_ID,a.WH_ID,'1',a.USR_ID_CREATE,DATE_FORMAT(a.USR_ID_CREATE_DT,'%Y-%m-%d'),a.WH_NM,a.WH_ONRSHP_TYPE,a.WH_STRG_TYPE,a.ADDS_ID,a.WH_DESC,a.ACTV,a.INACTV_RESN,DATE_FORMAT(a.INACTV_DT,'%Y-%m-%d'),a.WH_ENT_ID,a.PRJ_ID,z.id,'INDIA' FROM ".$intrDB.".intrm".$n."wh".$n."org a INNER JOIN ".$posDB.".sma_segment z ON ((a.PRJ_ID = z.prj_doc_id) AND (a.ORG_ID = z.org_id))  WHERE a.POS_WH_ID IS NULL OR a.POS_WH_ID = ''";
        $w1ex= "INSERT INTO ".$posDB.".sma_warehouses (org_id,wh_id,wh_type,usr_id_create,usr_id_create_dt,name,wh_onrshp_type,wh_strg_type,adds_id,wh_desc,actv,inactv_resn,inactv_dt,wh_ent_id,prj_id,segment_id,address) SELECT a.ORG_ID,a.WH_ID,'2',a.USR_ID_CREATE,DATE_FORMAT(a.USR_ID_CREATE_DT,'%Y-%m-%d'),a.WH_NM,a.WH_ONRSHP_TYPE,a.WH_STRG_TYPE,a.ADDS_ID,a.WH_DESC,a.ACTV,a.INACTV_RESN,DATE_FORMAT(a.INACTV_DT,'%Y-%m-%d'),a.WH_ENT_ID,a.PRJ_ID,z.id,'INDIA'  FROM ".$intrDB.".intrm".$n."app".$n."wh".$n."org".$n."ext a INNER JOIN ".$posDB.".sma_segment z ON ((a.PRJ_ID = z.prj_doc_id) AND (a.ORG_ID = z.org_id)) WHERE a.POS_APP_WH_ORG_ID IS NULL OR a.POS_APP_WH_ORG_ID = ''";
        if($this->db->query($w1)){
            $this->db->query($w1ex);
            $w2= "UPDATE ".$intrDB.".intrm".$n."wh".$n."org a INNER JOIN ".$posDB.".sma_warehouses b ON ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id)) SET a.POS_WH_ID = b.id,a.SYNC_FLG='1' WHERE ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id))";
            $w2ex= "UPDATE ".$intrDB.".intrm".$n."app".$n."wh".$n."org".$n."ext a INNER JOIN ".$posDB.".sma_warehouses b ON ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id)) SET a.POS_APP_WH_ORG_ID = b.id WHERE ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id))";
             if($this->db->query($w2))
             {
                $this->db->query($w2ex);
                $msg = "Warehouse ORG Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
           }
         
     }


public function warehouseMasterUPD()
     {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_warehouses a INNER JOIN ".$intrDB.".intrm".$n."wh".$n."org b ON (a.id = b.POS_WH_ID) INNER JOIN ".$posDB.".sma_segment z ON ((b.PRJ_ID = z.prj_doc_id) AND (b.ORG_ID = z.org_id)) SET a.org_id = b.ORG_ID,a.wh_id = b.WH_ID,a.name = b.WH_NM,a.wh_onrshp_type = b.WH_ONRSHP_TYPE,a.wh_strg_type = b.WH_STRG_TYPE, a.actv = b.ACTV,a.prj_id=b.PRJ_ID,a.segment_id = z.id WHERE b.SYNC_FLG = '3' ";
        if ($this->db->query($w)) {
            $w1 = "UPDATE ".$posDB.".sma_warehouses a INNER JOIN ".$intrDB.".intrm".$n."app".$n."wh".$n."org".$n."ext b ON (a.id = b.POS_APP_WH_ORG_ID) INNER JOIN ".$posDB.".sma_segment z ON ((b.PRJ_ID = z.prj_doc_id) AND (b.ORG_ID = z.org_id)) SET a.org_id = b.ORG_ID,a.wh_id = b.WH_ID,a.name = b.WH_NM,a.wh_onrshp_type = b.WH_ONRSHP_TYPE,a.wh_strg_type = b.WH_STRG_TYPE, a.actv = b.ACTV,a.prj_id=b.PRJ_ID,a.segment_id = z.id WHERE b.UPD_FLG = '3' ";
        if($this->db->query($w1))
        {
           /* $w2 = "UPDATE ".$intrDB.".intrm".$n."wh".$n."org a INNER JOIN ".$posDB.".sma_warehouses b ON (a.POS_WH_ID = b.id) SET a.SYNC_FLG='' WHERE (a.POS_WH_ID = b.id) AND a.SYNC_FLG='3'";
            $w3 = "UPDATE ".$intrDB.".intrm".$n."app".$n."wh".$n."org".$n."ext a INNER JOIN ".$posDB.".sma_warehouses b ON (a.POS_APP_WH_ORG_ID = b.id) SET a.UPD_FLG='' WHERE (a.POS_APP_WH_ORG_ID = b.id) AND a.UPD_FLG='3'"; */
            $w2 = "UPDATE ".$intrDB.".intrm".$n."wh".$n."org a SET a.SYNC_FLG='' WHERE a.SYNC_FLG='3'";
            $w3 = "UPDATE ".$intrDB.".intrm".$n."app".$n."wh".$n."org".$n."ext a SET a.UPD_FLG='' WHERE   a.UPD_FLG='3'";
            $this->db->query($w2);
            if($this->db->query($w3))
            {
                return true;
            } else {
              $msg = $this->db->error();
              return $msg['message'];
            }
            
        } else {
              $msg = $this->db->error();
              return $msg['message'];
        }    
      } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
        
     }
    
    // S.NO.-03 (Intermediate -> POS)    

    public function userMaster()
    {
       // $this->userUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        /*$userupd = "UPDATE ".$posDB.".sma_users u1,(SELECT GROUP_CONCAT(b.id SEPARATOR ',') as warehouse,b.segment_id FROM ".$intrDB.".intrm".$n."usr a INNER JOIN ".$posDB.".sma_warehouses b ON ((a.ORG_ID = b.org_id) AND (a.WH_ID = b.wh_id)) WHERE (a.POS_USR_ID IS NULL OR a.POS_USR_ID = '') GROUP BY a.ORG_ID,b.prj_id) u2 set u1.warehouse_id= CONCAT(u1.warehouse_id, ',', u2.warehouse),u1.pwid= CONCAT(u1.pwid, ',', u2.warehouse) WHERE u1.segment_id = u2.segment_id";
        $this->db->query($userupd);
        $ws = "UPDATE ".$intrDB.".intrm".$n."usr a INNER JOIN ".$posDB.".sma_warehouses c ON (c.org_id = a.ORG_ID AND c.wh_id = a.WH_ID) INNER JOIN ".$posDB.".sma_users d ON (d.segment_id = c.segment_id AND d.org_id = c.org_id) SET a.POS_USR_ID = d.id,a.CREATE_FLG='1' WHERE (d.segment_id = c.segment_id AND d.org_id = c.org_id) AND (a.POS_USR_ID IS NULL OR a.POS_USR_ID = '')";
        $this->db->query($ws);*/
       $w = "INSERT INTO ".$posDB.".sma_users(usr_id,username,password,salt,email,first_name,last_name,active,avatar,gender,phone,created_on,company,group_id,warehouse_id,segment_id,show_discount,org_id,pwid) SELECT a.USR_ID,a.USR_NAME,sha1(a.USR_PWD),'Null','Null',a.USR_FST_NAME,a.USR_LST_NAME, CASE WHEN a.USR_ACTV = 'Y' THEN '1' ELSE '0' END AS Activ,a.USR_IMG,a.USR_GNDR,a.USR_CONTACT_NO,a.USR_ID_CREATE_DT,'IFFCO', '5', group_concat(b.id,''),b.segment_id,a.USR_DISC,a.ORG_ID,group_concat(b.id,'') FROM ".$intrDB.".intrm".$n."usr a INNER JOIN ".$posDB.".sma_warehouses b ON ((a.ORG_ID = b.org_id) AND (a.WH_ID = b.wh_id)) WHERE (a.POS_USR_ID IS NULL OR a.POS_USR_ID = '') GROUP BY a.ORG_ID,b.prj_id ";
        if ($this->db->query($w)) {
            $msg = "User Records save successfully...";
            // $w1  = "UPDATE ".$intrDB.".intrm".$n."usr a INNER JOIN ".$posDB.".sma_users b ON (a.USR_ID = b.usr_id AND a.ORG_ID = b.org_id) SET a.POS_USR_ID= b.id, a.CREATE_FLG='1' WHERE (a.USR_ID = b.usr_id AND a.ORG_ID = b.org_id)";
            $w1  = "UPDATE ".$intrDB.".intrm".$n."usr a INNER JOIN ".$posDB.".sma_warehouses c ON (c.org_id = a.ORG_ID AND c.wh_id = a.WH_ID) INNER JOIN ".$posDB.".sma_users d ON (d.segment_id = c.segment_id AND d.org_id = c.org_id) SET a.POS_USR_ID = d.id,a.CREATE_FLG='1' WHERE (d.segment_id = c.segment_id AND d.org_id = c.org_id) AND (a.POS_USR_ID IS NULL OR a.POS_USR_ID = '')";
            $this->db->query($w1);
            return $msg;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
        
    }
    public function userUPD()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_users a INNER JOIN ".$intrDB.".intrm".$n."usr b ON (a.id = b.POS_USR_ID AND a.usr_id = b.USR_ID) SET a.username = b.USR_NAME, a.password = SHA1(b.USR_PWD), a.email = 'NULL', a.first_name = b.USR_FST_NAME, a.last_name = b.USR_LST_NAME, a.active = CASE WHEN b.USR_ACTV = 'Y' THEN '1' ELSE '0' END, a.avatar = b.USR_IMG,a.phone = b.USR_CONTACT_NO, a.show_discount = b.USR_DISC,a.pos_upd_flg='1' WHERE b.UPD_FLG = '3'";
        if ($this->db->query($w)) {
            
            $w1 = "UPDATE ".$intrDB.".intrm".$n."usr a INNER JOIN ".$posDB.".sma_users b ON (a.POS_USR_ID = b.id AND a.USR_ID= b.usr_id) SET a.UPD_FLG='' WHERE (a.POS_USR_ID = b.id AND a.USR_ID= b.usr_id) AND a.UPD_FLG='3'";
            $this->db->query($w1);
            return true;
        }
        
        else {
            $msg = $this->db->error();
            return $msg['message'];
        }
        
    }
    
    
    // S.NO.-04 (Intermediate -> POS)
    
    public function taxMaster()
    {
        //$ar = $this->taxUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n  = '$';
        $w  = "INSERT INTO ".$posDB.".sma_tax_rates(name,rate,org_id,wh_id,grp_id) SELECT CONCAT(a.TAX_VAL,'% TAX'),a.TAX_VAL,a.ORG_ID,a.WH_ID,a.GRP_ID FROM ".$intrDB.".intrm".$n."itm".$n."grp".$n."org a WHERE a.POS_GRP_ID IS NULL OR a.POS_GRP_ID = ''";
        if ($this->db->query($w)) {
            $msg = "Tax Records save successfully...";
            return $msg;
        }
    }
    
    public function taxUPD()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_tax_rates a INNER JOIN ".$intrDB.".intrm".$n."itm".$n."grp".$n."org b ON (a.org_id = b.ORG_ID AND a.grp_id = b.GRP_ID AND a.wh_id = b.WH_ID) SET a.name = CONCAT(b.TAX_VAL,'% TAX'),a.rate = b.TAX_VAL WHERE b.UPD_FLG='3'";
        if ($this->db->query($w)) {
            return true;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
        
    }
    
    // S.NO.-05 (Intermediate -> POS)
    
    public function categoryMaster()
    {
        $ar = $this->categoryUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n  = '$';
        $w  = "INSERT INTO ".$posDB.".sma_categories(code, name, parent_id, grp_id_parent, tax_rate_id) SELECT a.GRP_ID, a.GRP_NM, CASE WHEN a.GRP_ID_PARENT = '0' THEN '0' ELSE 'NULL' END AS asd, a.GRP_ID_PARENT, '0' FROM ".$intrDB.".intrm".$n."itm".$n."grp a WHERE a.POS_GRP_ID IS NULL OR a.POS_GRP_ID = ''";
        if ($this->db->query($w)) {
            $ws = "UPDATE ".$posDB.".sma_categories a INNER JOIN ".$posDB.".sma_categories b ON (a.grp_id_parent = b.code) SET a.parent_id = b.id WHERE a.grp_id_parent = b.code";
            $this->db->query($ws);
            $ws1= "UPDATE ".$posDB.".sma_categories a INNER JOIN ".$posDB.".sma_categories b ON (a.id = b.parent_id) SET b.level=2 WHERE a.parent_id=0 AND b.parent_id<>0";
            $this->db->query($ws1);
            $ws2= "UPDATE ".$posDB.".sma_categories a INNER JOIN ".$posDB.".sma_categories b ON (a.id = b.parent_id) SET b.level=3 WHERE a.parent_id<>0 AND b.parent_id<>0";
            $this->db->query($ws2);

            $w1 = "UPDATE ".$intrDB.".intrm".$n."itm".$n."grp a INNER JOIN ".$posDB.".sma_categories b ON a.GRP_ID = b.code SET a.POS_GRP_ID = b.id WHERE a.GRP_ID = b.code";
            $this->db->query($w1);
             $w2 = "UPDATE ".$intrDB.".intrm".$n."itm".$n."grp".$n."org a INNER JOIN ".$posDB.".sma_categories b ON a.GRP_ID = b.code SET a.POS_GRP_ID = b.id, a.CREATE_FLG = '1' WHERE a.GRP_ID = b.code";
            $this->db->query($w2);
            $msg = "category Records save successfully...";
            return $msg;
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
        
    }
    public function categoryUPD()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        //$w = "UPDATE ".$posDB.".sma_categories a INNER JOIN ".$intrDB.".intrm".$n."itm".$n."grp b ON (b.POS_GRP_ID = a.id AND b.GRP_ID = a.code) INNER JOIN ".$intrDB.".intrm".$n."itm".$n."grp".$n."org c ON (c.POS_GRP_ID = b.POS_GRP_ID AND c.GRP_ID = b.GRP_ID) SET a.code = b.GRP_ID,a.name = b.GRP_NM, a.grp_id_parent = b.GRP_ID_PARENT,a.pos_upd_flg='1' WHERE c.UPD_FLG='3'";
        $w = "UPDATE ".$posDB.".sma_categories a INNER JOIN ".$intrDB.".intrm".$n."itm".$n."grp b ON (b.POS_GRP_ID = a.id AND b.GRP_ID = a.code) SET a.code = b.GRP_ID,a.name = b.GRP_NM, a.grp_id_parent = b.GRP_ID_PARENT,a.pos_upd_flg='1' WHERE b.UPD_FLG='3'";
        if ($this->db->query($w)) {
            //$w1 = "UPDATE ".$intrDB.".intrm".$n."itm".$n."grp".$n."org a INNER JOIN ".$posDB.".sma_categories b ON (a.GRP_ID = b.code AND a.POS_GRP_ID = b.id) SET a.UPD_FLG = 'NULL' WHERE (a.GRP_ID = b.code AND a.POS_GRP_ID = b.id)";
            $w1 = "UPDATE ".$intrDB.".intrm".$n."itm".$n."grp a INNER JOIN ".$posDB.".sma_categories b ON (a.GRP_ID = b.code AND a.POS_GRP_ID = b.id) SET a.UPD_FLG = 'NULL' WHERE a.UPD_FLG = '3'";
            $this->db->query($w1);
            return true;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
        
        
    }
    
    // S.NO.-06 (Intermediate -> POS)
    
    public function customerMaster()
    {
        // B For ----------- Bank Acount (Replicate Org + Segment wise)  
        // D For ----------- Cash Acount (Replicate Org + Segment wise)
        $ar = $this->customerUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n  = '$';
        $w  = "INSERT INTO ".$posDB.".sma_companies(group_id,group_name,customer_group_id,customer_group_name,name,phone,email,vat_no,address,country,eo_type,eo_id,cf1,cf2,cf3,gst_no,gst_effect_date,sync_flg) SELECT CASE WHEN a.EO_TYPE = 'C' THEN '3' WHEN a.EO_TYPE = 'S' THEN '4' WHEN a.EO_TYPE = 'B' THEN '8' WHEN a.EO_TYPE = 'D' THEN '9' ELSE '7' END AS GP_ID,CASE WHEN a.EO_TYPE = 'C' THEN 'customer' WHEN a.EO_TYPE = 'B' THEN 'Bank' WHEN a.EO_TYPE = 'D' THEN 'Cash' WHEN a.EO_TYPE = 'T' THEN 'Transporter' ELSE 'Supplier' END AS GP_NM,CASE WHEN a.EO_TYPE = 'C' THEN '1' WHEN a.EO_TYPE = 'B' THEN '1' WHEN a.EO_TYPE = 'D' THEN '1' ELSE 'NULL' END AS GP_id_cus,CASE WHEN a.EO_TYPE = 'C' THEN 'General' ELSE 'NULL' END AS CGP_IDNM,a.EO_NM,a.EO_PHONE,'Not Available',a.VAT_NO,b.ADDRESS,'INDIA',a.EO_TYPE,a.EO_ID, z.BANK_NM,z.BANK_AC_NO,z.IFSC_CODE,a.GST_NO,DATE_FORMAT(a.GST_EFFECT_DATE,'%Y-%m-%d'),'1' FROM ".$intrDB.".intrm".$n."eo a LEFT JOIN ".$intrDB.".intrm".$n."eo".$n."add b ON(b.EO_ID = a.EO_ID AND b.EO_TYPE = a.EO_TYPE) LEFT JOIN ".$intrDB.".intrm".$n."eo".$n."bank z ON (z.EO_ID = a.EO_ID AND z.EO_TYPE = a.EO_TYPE)  WHERE (a.POS_EO_ID IS NULL OR a.POS_EO_ID = '')";
        if ($this->db->query($w)) {
            $msg = "customer Records save successfully...";
            $w1  = "UPDATE ".$intrDB.".intrm".$n."eo a INNER JOIN ".$posDB.".sma_companies z ON (z.eo_id = a.EO_ID AND z.eo_type = a.EO_TYPE) SET a.POS_EO_ID = z.id WHERE z.eo_id = a.EO_ID AND z.eo_type = a.EO_TYPE";
            $this->db->query($w1);
            //$w2  = "UPDATE ".$intrDB.".intrm".$n."eo".$n."org a INNER JOIN ".$posDB.".sma_companies z ON (z.eo_id = a.EO_ID AND z.eo_type = a.EO_TYPE) SET a.POS_EO_ID = z.id,a.CREATE_FLG = '1' WHERE z.eo_id = a.EO_ID AND z.eo_type = a.EO_TYPE";
           // $this->db->query($w2);
            return $msg;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }
    
    public function customerUPD()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();   // ,GST_NO,GST_EFFECT_DATE
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_companies z INNER JOIN ".$intrDB.".intrm".$n."eo a ON (a.POS_EO_ID = z.id) LEFT JOIN ".$intrDB.".intrm".$n."eo".$n."add b ON (b.EO_ID = a.EO_ID AND b.EO_TYPE = a.EO_TYPE) SET z.name = a.EO_NM,z.vat_no = a.VAT_NO,z.phone = a.EO_PHONE,z.email = 'Not Available',z.address = b.ADDRESS,z.gst_no = a.GST_NO,z.gst_effect_date = DATE_FORMAT(a.GST_EFFECT_DATE,'%Y-%m-%d'),z.sync_flg = '1',z.pos_upd_flg='1' WHERE (a.POS_EO_ID = z.id AND a.UPD_FLG = '3')";
        if ($this->db->query($w)) {
            //$w1 = "UPDATE ".$intrDB.".intrm".$n."eo a INNER JOIN ".$posDB.".sma_companies b ON a.POS_EO_ID = b.id SET a.UPD_FLG='' WHERE a.POS_EO_ID = b.id";
            $w1 = "UPDATE ".$intrDB.".intrm".$n."eo a SET a.UPD_FLG='' WHERE a.UPD_FLG='3'";
            $this->db->query($w1);
            return true;
        }
        
        else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }
    
    // S.NO.-07 (Intermediate -> POS)
    
    public function productMaster()
    {
        //$ar = $this->productUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n  = '$';
        
     $w  = "INSERT INTO ".$posDB.".sma_products(code,name,unit,price,category_id,tax_rate,details,warehouse,barcode_symbology,product_details,serialized,org_id,wh_id,grp_id,bat_item_id,uom_basic,uom_sls,uom_purchase,hsn_code) SELECT a.ITM_ID,b.ITM_DESC,z.uom_desc,b.PRICE_SLS,f.id,1,b.ITM_LONG_DESC,e.id,b.ITM_LEGACY_CODE,b.ITM_LONG_DESC,b.SERIALIZED_FLG,a.ORG_ID,a.WH_ID,b.GRP_ID,a.BAT_ITM_FLG,b.UOM_BASIC,b.UOM_SLS,b.UOM_PURCHASE,b.HSN_CODE FROM ".$intrDB.".intrm".$n."prod".$n."org a INNER JOIN ".$intrDB.".intrm".$n."prod b ON (b.ITM_ID = a.ITM_ID) INNER JOIN ".$posDB.".sma_uom_conv_std z ON (z.uom_id = b.UOM_BASIC) INNER JOIN ".$posDB.".sma_categories f ON (f.code = b.GRP_ID) INNER JOIN ".$posDB.".sma_warehouses e ON (e.org_id = a.ORG_ID AND e.wh_id = a.WH_ID) WHERE a.POS_ITM_ID IS NULL OR a.POS_ITM_ID = ''";        
       if ($this->db->query($w)) {
            $msg = "Product Records save successfully..."; 
           // $w2  = "UPDATE ".$intrDB.".intrm".$n."prod".$n."org a INNER JOIN ".$posDB.".sma_products b ON (a.ITM_ID = b.code AND a.ORG_ID = b.org_id AND a.WH_ID = b.wh_id) SET a.POS_ITM_ID = b.id,a.CREATE_FLG='1' WHERE a.POS_ITM_ID IS NULL OR a.POS_ITM_ID = '' ";
            $w2  = "UPDATE ".$intrDB.".intrm".$n."prod".$n."org a SET a.POS_ITM_ID = 1,a.CREATE_FLG='1' WHERE a.POS_ITM_ID IS NULL OR a.POS_ITM_ID = '' ";
            $this->db->query($w2);
            return $msg;
        }
        else{
              $msg = $this->db->error();
            return $msg['message'];
        }

    
    }
    
    public function productUPD()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        //$w = "UPDATE ".$posDB.".sma_products q INNER JOIN ".$intrDB.".intrm".$n."prod".$n."org a ON (a.ITM_ID = q.code AND a.ORG_ID = q.org_id AND a.WH_ID = q.wh_id) INNER JOIN ".$intrDB.".intrm".$n."prod b ON (b.ITM_ID = a.ITM_ID) LEFT JOIN ".$posDB.".sma_warehouses e ON (e.org_id = a.ORG_ID AND e.wh_id = a.WH_ID) INNER JOIN ".$posDB.".sma_categories f ON (f.code = b.GRP_ID) SET q.name= b.ITM_DESC,q.price= b.PRICE_SLS,q.category_id= f.id,q.details= b.ITM_LONG_DESC,q.warehouse= e.id,q.barcode_symbology= b.ITM_LEGACY_CODE,q.product_details= b.ITM_LONG_DESC,q.serialized= b.SERIALIZED_FLG,q.org_id= a.ORG_ID,q.grp_id=b.GRP_ID,q.bat_item_id = a.BAT_ITM_FLG, q.tupd_flg='1',q.pos_upd_flg='1' WHERE a.UPD_FLG='3'";
        $w = "UPDATE ".$posDB.".sma_products q INNER JOIN ".$intrDB.".intrm".$n."prod".$n."org a ON (a.ITM_ID = q.code AND a.ORG_ID = q.org_id AND a.WH_ID = q.wh_id) INNER JOIN ".$intrDB.".intrm".$n."prod b ON (b.ITM_ID = a.ITM_ID) SET q.name= b.ITM_DESC,q.price= b.PRICE_SLS,q.details= b.ITM_LONG_DESC,q.barcode_symbology= b.ITM_LEGACY_CODE,q.product_details= b.ITM_LONG_DESC,q.serialized= b.SERIALIZED_FLG,q.org_id= a.ORG_ID,q.grp_id=b.GRP_ID,q.bat_item_id = a.BAT_ITM_FLG,q.hsn_code= b.HSN_CODE, q.tupd_flg='1',q.pos_upd_flg='1' WHERE a.UPD_FLG='3'";
        if ($this->db->query($w)) {
            
            // $w1 = "UPDATE ".$intrDB.".intrm".$n."prod".$n."org a INNER JOIN ".$posDB.".sma_products b ON (a.ITM_ID = b.code AND a.ORG_ID = b.org_id AND a.WH_ID = b.wh_id) SET a.UPD_FLG='' WHERE a.UPD_FLG='3'";
            $w1 = "UPDATE ".$intrDB.".intrm".$n."prod".$n."org a SET a.UPD_FLG='' WHERE a.UPD_FLG='3'";
            $this->db->query($w1);
            return true;
        }
        
        else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }
    
    // S.NO.-08 (Intermediate -> POS)
    
    public function purchaseItemMaster()
    {
        //$ar = $this->purchaseItemUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        //$w = "INSERT INTO ".$posDB.".sma_purchase_items(product_id,product_code,product_name,itm_desc,net_unit_cost,quantity,warehouse_id,item_tax,tax_rate_id,tax,subtotal,quantity_balance,date,status,unit_cost,lot_no,sr_no,org_id,wh_id,grp_id,category_id,is_serialized,image,tax_rate,tax_method,bat_item_id) SELECT a.id,a.code,a.name,a.product_details,(a.price -((a.price * b.rate) /(100 + b.rate))) AS unit_cost,a.quantity,a.warehouse,((a.price * b.rate) /(100 + b.rate)) AS item_tax,a.tax_rate,b.rate,a.price,a.quantity,DATE('Y-m-d'),'received',a.price,a.lot_no,a.sr_no,a.org_id,a.wh_id,a.grp_id,a.category_id,a.serialized,a.image,a.tax_rate,a.tax_method,a.bat_item_id FROM ".$posDB.".sma_products a INNER JOIN ".$posDB.".sma_tax_rates b ON(b.id = a.tax_rate) WHERE a.trsfr_flg IS NULL OR a.trsfr_flg = ''";
        $w = "INSERT INTO ".$posDB.".sma_purchase_items(product_id,product_code,product_name,itm_desc,net_unit_cost,quantity,warehouse_id,item_tax,tax_rate_id,tax,subtotal,quantity_balance,date,status,unit_cost,lot_no,sr_no,org_id,wh_id,grp_id,category_id,is_serialized,image,tax_rate,tax_method,bat_item_id,hsn_code) SELECT a.id,a.code,a.name,a.product_details,(a.price -((a.price * 1) /(100 + 1))) AS unit_cost,a.quantity,a.warehouse,((a.price * 1) /(100 + 1)) AS item_tax,a.tax_rate,1,a.price,a.quantity,DATE('Y-m-d'),'received',a.price,a.lot_no,a.sr_no,a.org_id,a.wh_id,a.grp_id,a.category_id,a.serialized,
        a.image,a.tax_rate,a.tax_method,a.bat_item_id,a.hsn_code FROM ".$posDB.".sma_products a WHERE (a.trsfr_flg IS NULL OR a.trsfr_flg = '')";
        
        if ($this->db->query($w)) {
            $asd = "UPDATE ".$posDB.".sma_purchase_items a INNER JOIN ".$posDB.".sma_warehouses b ON (a.warehouse_id = b.id) SET a.prj_id = b.prj_id WHERE a.warehouse_id = b.id AND (a.prj_id IS NULL OR a.prj_id = '')";
            $this->db->query($asd);
            $msg = "Product Records save in warehouse successfully...";
            $w1  = "UPDATE ".$posDB.".sma_products a INNER JOIN ".$posDB.".sma_purchase_items b ON a.id = b.product_id SET a.trsfr_flg= '1' WHERE a.id = b.product_id";
            $this->db->query($w1);
            return $msg;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
        
    }
    public function purchaseItemUPD()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        //$w = "UPDATE ".$posDB.".sma_purchase_items z INNER JOIN ".$posDB.".sma_products a ON (a.id = z.product_id) INNER JOIN ".$posDB.".sma_tax_rates b ON(b.id = a.tax_rate) SET z.product_code = a.code, z.product_name = a.name, z.net_unit_cost =(a.price -((a.price * b.rate) /(100 + b.rate))), z.quantity = a.quantity, z.item_tax =((a.price * b.rate) /(100 + b.rate)), z.tax_rate_id = a.tax_rate, z.tax = b.rate, z.subtotal = a.price, z.quantity_balance = a.quantity, z.lot_no = a.lot_no, z.sr_no = a.sr_no, z.org_id = a.org_id, z.wh_id = a.wh_id, z.grp_id = a.grp_id,z.category_id = a.category_id,z.itm_desc = a.details,z.bat_item_id = a.bat_item_id, z.pos_upd_flg='1' WHERE a.tupd_flg = '1'";
        $w = "UPDATE ".$posDB.".sma_purchase_items z INNER JOIN ".$posDB.".sma_products a ON (a.id = z.product_id) INNER JOIN ".$posDB.".sma_tax_rates b ON (b.id = a.tax_rate) SET z.product_name = a.name, z.net_unit_cost =(a.price -((a.price * 1) /(100 + 1))), z.item_tax =((a.price * 1) /(100 + 1)), z.tax_rate_id = a.tax_rate, z.tax = 1, z.subtotal = a.price,z.sr_no = a.sr_no, z.grp_id = a.grp_id,z.bat_item_id = a.bat_item_id,z.hsn_code= a.hsn_code, z.pos_upd_flg='1' WHERE a.tupd_flg = '1'";
        
        if ($this->db->query($w)) {
            $w1 = "UPDATE ".$posDB.".sma_products a INNER JOIN ".$posDB.".sma_purchase_items b ON a.id = b.product_id SET a.tupd_flg='' WHERE a.id = b.product_id";
            $this->db->query($w1);
            return true;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }

    /////////////////////////////
    public function binMaster(){
        $this->binUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_bin(org_id,segment_id,warehouse_id,wh_id,bin_id,bin_nm,bin_desc,storage_type,blocked,blk_resn,blk_dt_frm,blk_dt_to,bin_ent_id,create_date) SELECT a.ORG_ID,b.segment_id,b.id,a.WH_ID,a.BIN_ID,a.BIN_NM,a.BIN_DESC,a.STORAGE_TYPE,a.BLOCKED,a.BLK_RESN,a.BLK_DT_FRM,a.BLK_DT_TO,a.BIN_ENT_ID,a.USR_ID_CREATE_DT FROM ".$intrDB.".intrm".$n."bin a INNER JOIN ".$posDB.".sma_warehouses b ON (a.WH_ID = b.wh_id AND a.ORG_ID = b.org_id) WHERE a.POS_BIN_ID IS NULL OR a.POS_BIN_ID = ''";
        if ($this->db->query($w)) {
            $msg = "Bin Records save successfully...";
            $w1  = "UPDATE ".$intrDB.".intrm".$n."bin a INNER JOIN ".$posDB.".sma_bin b ON (a.WH_ID = b.wh_id AND a.ORG_ID = b.org_id) SET a.POS_BIN_ID= b.id WHERE (a.WH_ID = b.wh_id AND a.ORG_ID = b.org_id)";
            $this->db->query($w1);
            return $msg;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }

    }

    public function binUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_bin a INNER JOIN ".$intrDB.".intrm".$n."bin b ON (b.POS_BIN_ID = a.id) SET a.bin_nm = b.BIN_NM,a.bin_desc = b.BIN_DESC,a.storage_type= b.STORAGE_TYPE,a.blocked=b.BLOCKED,a.blk_resn =b.BLK_RESN,a.blk_dt_frm=b.BLK_DT_FRM,a.blk_dt_to=b.BLK_DT_TO,a.bin_ent_id = b.BIN_ENT_ID,a.pos_upd_flg='1' WHERE b.UPD_FLG = '3' AND (b.POS_BIN_ID = a.id)";
        if($this->db->query($w)){
            $w1 = "UPDATE ".$intrDB.".intrm".$n."bin a INNER JOIN ".$posDB.".sma_bin b ON (a.POS_BIN_ID = b.id) SET a.UPD_FLG = '' WHERE (a.POS_BIN_ID = b.id) AND a.UPD_FLG = '3'";
            if($this->db->query($w)){
                   $msg = "Bin Records Update successfully...";
                   return true;

            } else {
               $msg = $this->db->error();
               return $msg['message'];
       }

        } else {
            $msg = $this->db->error();
            return $msg['message'];
       }

    }

    /////////////////////////////
    public function poMaster(){
        $this->poMasterUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_drft_po(org_id,po_id,po_dt,customer_id,bill_adds_id,tlrnc_days,curr_id_sp,curr_name,curr_conv_fctr,tlrnc_qty_type,tlrnc_qty_val,usr_id_create_dt,auth_po_no,fy_id,po_status,po_mode,doc_id,flg) SELECT a.ORG_ID,a.PO_ID,a.PO_DT,a.EO_ID,a.BILL_ADDS_ID,a.TLRNC_DAYS,a.CURR_ID_SP,c.name,a.CURR_CONV_FCTR,a.TLRNC_QTY_TYPE,a.TLRNC_QTY_VAL,a.USR_ID_CREATE_DT,a.AUTH_PO_NO,a.FY_ID,a.PO_STATUS,a.PO_MODE,a.DOC_ID,'1' FROM ".$intrDB.".intrm".$n."drft".$n."po a INNER JOIN ".$posDB.".sma_currencies c ON (c.curr_id = a.CURR_ID_SP) WHERE (a.POS_PO_ID IS NULL OR a.POS_PO_ID = '')";
      if($this->db->query($w)){
          $msg = "PO Records save successfully...";
          $w1= "UPDATE ".$intrDB.".intrm".$n."drft".$n."po a INNER JOIN ".$posDB.".sma_drft_po b ON(a.PO_ID = b.po_id AND a.ORG_ID = b.org_id AND a.DOC_ID = b.doc_id) SET a.POS_PO_ID = b.id WHERE (a.PO_ID = b.po_id AND a.ORG_ID = b.org_id AND a.DOC_ID = b.doc_id)";
          $this->db->query($w1);
            return $msg;
  } else {
          $msg = $this->db->error();
            return $msg['message'];
   }

    }

    public function poMasterUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$posDB.".sma_drft_po m INNER JOIN ".$intrDB.".intrm".$n."drft".$n."po a ON (m.id = a.POS_PO_ID) INNER JOIN ".$posDB.".sma_currencies c ON (c.curr_id = a.CURR_ID_SP) SET m.po_id = a.PO_ID,m.po_dt = a.PO_DT,m.customer_id = a.EO_ID,m.bill_adds_id=a.BILL_ADDS_ID,m.tlrnc_days=a.TLRNC_DAYS,m.curr_id_sp=a.CURR_ID_SP,m.curr_name=c.name,m.curr_conv_fctr=a.CURR_CONV_FCTR,m.tlrnc_qty_type=a.TLRNC_QTY_TYPE,m.tlrnc_qty_val=a.TLRNC_QTY_VAL,m.auth_po_no=a.AUTH_PO_NO,m.fy_id = a.FY_ID,m.po_status = a.PO_STATUS,m.po_mode=a.PO_MODE,m.doc_id=a.DOC_ID WHERE a.UPD_FLG='3'";
      if($this->db->query($w)){
          $msg = "Update PO Records save successfully...";
          $w1= "UPDATE ".$intrDB.".intrm".$n."drft".$n."po a INNER JOIN ".$posDB.".sma_drft_po b ON(a.POS_PO_ID = b.id) SET a.UPD_FLG = '' WHERE (a.POS_PO_ID = b.id)";
          $this->db->query($w1);
            return TRUE;
  } else {
          $msg = $this->db->error();
            return $msg['message'];
   }

    }

/////////////////////////////////

    public function poSchdlMaster(){
        $this->poSchdlMasterUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        //$w= "INSERT INTO ".$posDB.".sma_drft_po_dlv_schdl(org_id,po_id,itm_id,itm_name,tot_qty,dlv_mode,dlv_qty,tlrnc_qty_type,tlrnc_qty_val,dlv_dt,wh_id,dlv_adds_id,usr_id_create_dt,dlv_schdl_no,itm_uom_desc,bal_qty,tmp_rcpt_qty, tlrnc_days_val,segment_id,warehouse_id,prj_id,ro_no,ro_dt,item_id,use_rented_wh) SELECT a.ORG_ID,b.id,f.id,f.name,a.TOT_QTY,a.DLV_MODE,a.DLV_QTY,a.TLRNC_QTY_TYPE,a.TLRNC_QTY_VAL,a.DLV_DT,a.WH_ID,a.DLV_ADDS_ID,a.USR_ID_CREATE_DT,a.DLV_SCHDL_NO,a.ITM_UOM,a.BAL_QTY,a.TMP_RCPT_QTY,a.TLRNC_DAYS_VAL,z.id,c.id,a.PRJ_ID,a.RO_NO,a.RO_DT,a.ITM_ID, CASE WHEN a.USE_RENT_WH = 'Y' THEN '1' ELSE '0' END AS rentwh FROM ".$intrDB.".intrm".$n."drft".$n."po".$n."dlv".$n."schdl a INNER JOIN ".$posDB.".sma_drft_po b ON ((a.ORG_ID = b.org_id) AND (b.doc_id = a.DOC_ID)) INNER JOIN ".$posDB.".sma_warehouses c ON (c.org_id = a.ORG_ID AND c.wh_id = a.WH_ID) INNER JOIN ".$posDB.".sma_segment z ON (z.prj_doc_id = a.PRJ_ID AND z.org_id = a.ORG_ID) INNER JOIN ".$posDB.".sma_products f ON (f.org_id = a.ORG_ID AND f.wh_id = a.WH_ID AND f.code = a.ITM_ID) WHERE a.POS_PO_SCHDL_ID IS NULL OR a.POS_PO_SCHDL_ID = ''";
        $w= "INSERT INTO ".$posDB.".sma_drft_po_dlv_schdl(org_id,po_id,itm_id,itm_name,tot_qty,dlv_mode,dlv_qty,tlrnc_qty_type,tlrnc_qty_val,dlv_dt,wh_id,dlv_adds_id,usr_id_create_dt,dlv_schdl_no,itm_uom_desc,bal_qty,tmp_rcpt_qty, tlrnc_days_val,segment_id,warehouse_id,prj_id,ro_no,ro_dt,item_id,use_rented_wh,flg) SELECT a.ORG_ID,b.id,f.id,f.name,a.TOT_QTY,a.DLV_MODE,a.DLV_QTY,a.TLRNC_QTY_TYPE,a.TLRNC_QTY_VAL,a.DLV_DT,a.WH_ID,a.DLV_ADDS_ID,a.USR_ID_CREATE_DT,a.DLV_SCHDL_NO,a.ITM_UOM,a.BAL_QTY,a.TMP_RCPT_QTY,a.TLRNC_DAYS_VAL,z.id,f.warehouse,a.PRJ_ID,a.RO_NO,a.RO_DT,a.ITM_ID, CASE WHEN a.USE_RENT_WH = 'Y' THEN '1' ELSE '0' END AS rentwh,'1' FROM ".$intrDB.".intrm".$n."drft".$n."po".$n."dlv".$n."schdl a INNER JOIN ".$posDB.".sma_drft_po b ON ((a.ORG_ID = b.org_id) AND (b.doc_id = a.DOC_ID)) INNER JOIN ".$posDB.".sma_segment z ON (z.prj_doc_id = a.PRJ_ID AND z.org_id = a.ORG_ID) INNER JOIN ".$posDB.".sma_products f ON (f.org_id = a.ORG_ID AND f.wh_id = a.WH_ID AND f.code = a.ITM_ID) WHERE a.POS_PO_SCHDL_ID IS NULL OR a.POS_PO_SCHDL_ID = ''";
         if($this->db->query($w)){
          $msg = "PO schdl Records save successfully...";
           $w1= "UPDATE ".$intrDB.".intrm".$n."drft".$n."po".$n."dlv".$n."schdl a INNER JOIN ".$posDB.".sma_drft_po_dlv_schdl b ON ((a.ORG_ID = b.org_id) AND (a.ITM_ID = b.item_id) AND (a.TOT_QTY = b.tot_qty) AND (a.RO_NO = b.ro_no)) INNER JOIN ".$posDB.".sma_drft_po c ON (b.po_id=c.id) SET a.POS_PO_SCHDL_ID = b.id WHERE ((a.ORG_ID = b.org_id) AND (a.ITM_ID = b.item_id) AND (a.TOT_QTY = b.tot_qty) AND (a.RO_NO = b.ro_no) AND (a.doc_id=c.doc_id))";
           $this->db->query($w1);
            return $msg;
  } else {
          $msg = $this->db->error();
            return $msg['message'];
   }
    }

    public function poSchdlMasterUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        //$w= "UPDATE ".$posDB.".sma_drft_po_dlv_schdl m INNER JOIN ".$intrDB.".intrm".$n."drft".$n."po".$n."dlv".$n."schdl a ON (m.id = a.POS_PO_SCHDL_ID) INNER JOIN ".$posDB.".sma_drft_po b ON ((a.ORG_ID = b.org_id) AND (b.doc_id = a.DOC_ID)) INNER JOIN ".$posDB.".sma_warehouses c ON (c.org_id = a.ORG_ID AND c.wh_id = a.WH_ID) INNER JOIN ".$posDB.".sma_segment z ON (z.prj_doc_id = a.PRJ_ID AND z.org_id = a.ORG_ID) INNER JOIN ".$posDB.".sma_products f ON (f.org_id = a.ORG_ID AND f.wh_id = a.WH_ID AND f.code = a.ITM_ID) SET m.po_id=b.id,m.itm_id=f.id,m.itm_name=f.name,m.tot_qty=a.TOT_QTY,m.dlv_mode=a.DLV_MODE,m.dlv_qty=a.DLV_QTY,m.tlrnc_qty_type=a.TLRNC_QTY_TYPE,m.tlrnc_qty_val=a.TLRNC_QTY_VAL,m.dlv_dt=a.DLV_DT,m.wh_id=a.WH_ID,m.dlv_adds_id=a.DLV_ADDS_ID,m.dlv_schdl_no=a.DLV_SCHDL_NO,m.itm_uom_desc=a.ITM_UOM,m.bal_qty=a.BAL_QTY,m.tmp_rcpt_qty=a.TMP_RCPT_QTY, m.tlrnc_days_val=a.TLRNC_DAYS_VAL,m.segment_id=z.id,m.warehouse_id=c.id,m.prj_id=a.PRJ_ID,m.ro_no=a.RO_NO,m.ro_dt=a.RO_DT,m.item_id=a.ITM_ID,m.pos_upd_flg='1' WHERE a.UPD_FLG='3'";
        $w= "UPDATE ".$posDB.".sma_drft_po_dlv_schdl m INNER JOIN ".$intrDB.".intrm".$n."drft".$n."po".$n."dlv".$n."schdl a ON (m.id = a.POS_PO_SCHDL_ID) INNER JOIN ".$posDB.".sma_drft_po b ON ((a.ORG_ID = b.org_id) AND (b.doc_id = a.DOC_ID)) INNER JOIN ".$posDB.".sma_segment z ON (z.prj_doc_id = a.PRJ_ID AND z.org_id = a.ORG_ID) INNER JOIN ".$posDB.".sma_products f ON (f.org_id = a.ORG_ID AND f.wh_id = a.WH_ID AND f.code = a.ITM_ID) SET m.po_id=b.id,m.itm_id=f.id,m.itm_name=f.name,m.tot_qty=a.TOT_QTY,m.dlv_mode=a.DLV_MODE,m.dlv_qty=a.DLV_QTY,m.tlrnc_qty_type=a.TLRNC_QTY_TYPE,m.tlrnc_qty_val=a.TLRNC_QTY_VAL,m.dlv_dt=a.DLV_DT,m.wh_id=a.WH_ID,m.dlv_adds_id=a.DLV_ADDS_ID,m.dlv_schdl_no=a.DLV_SCHDL_NO,m.itm_uom_desc=a.ITM_UOM,m.bal_qty=a.BAL_QTY,m.tmp_rcpt_qty=a.TMP_RCPT_QTY, m.tlrnc_days_val=a.TLRNC_DAYS_VAL,m.segment_id=z.id,m.warehouse_id=f.warehouse,m.prj_id=a.PRJ_ID,m.ro_no=a.RO_NO,m.ro_dt=a.RO_DT,m.item_id=a.ITM_ID,m.pos_upd_flg='1' WHERE a.UPD_FLG='3'";
         if($this->db->query($w)){
          $msg = "Update PO schdl Records save successfully...";
           $w1= "UPDATE ".$intrDB.".intrm".$n."drft".$n."po".$n."dlv".$n."schdl a INNER JOIN ".$posDB.".sma_drft_po_dlv_schdl b ON ((a.ORG_ID = b.org_id) AND (a.ITM_ID = b.item_id) AND (a.TOT_QTY = b.tot_qty) AND (a.RO_NO = b.ro_no)) SET a.UPD_FLG = '' WHERE ((a.ORG_ID = b.org_id) AND (a.ITM_ID = b.item_id) AND (a.TOT_QTY = b.tot_qty) AND (a.RO_NO = b.ro_no) ) AND a.UPD_FLG='3'";
           $this->db->query($w1);
            return TRUE;
  } else {
          $msg = $this->db->error();
            return $msg['message'];
   }
    }

    /////////////////////////////////

    public function dsAttMaster(){
        //$this->dsAttUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_ds_att(att_id,att_name,att_type_id,att_actv) SELECT a.ATT_ID,a.ATT_NM,a.ATT_TYPE_ID,a.ATT_ACTV FROM ".$intrDB.".intrm".$n."ds".$n."att a WHERE a.POS_ATT_ID IS NULL OR a.POS_ATT_ID = ''";
        if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."ds".$n."att a INNER JOIN ".$posDB.".sma_ds_att b ON (a.ATT_ID = b.att_id) SET a.POS_ATT_ID = b.id,a.SYNC_FLG='1' WHERE (a.ATT_ID = b.att_id)";
             if($this->db->query($w1))
             {
                $msg = "DS ATT Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    public function dsAttUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_ds_att a INNER JOIN ".$intrDB.".intrm".$n."ds".$n."att b ON (b.POS_ATT_ID = a.id) SET a.att_id = b.ATT_ID, a.att_name = b.ATT_NM, a.att_type_id = b.ATT_TYPE_ID, a.att_actv= b.ATT_ACTV WHERE b.SYNC_FLG = '3' AND (b.POS_ATT_ID = a.id)";
        if($this->db->query($w)){
            $w1 = "UPDATE ".$intrDB.".intrm".$n."ds".$n."att a INNER JOIN ".$posDB.".sma_ds_att b ON (a.POS_ATT_ID = b.id) SET a.SYNC_FLG = '' WHERE (a.POS_ATT_ID = b.id) AND a.SYNC_FLG = '3'";
            if($this->db->query($w)){
                   $msg = "DS ATT Records Update successfully...";
                   return true;

            } else {
               $msg = $this->db->error();
               return $msg['message'];
       }

        } else {
            $msg = $this->db->error();
            return $msg['message'];
       }

    }

    /////////////////////////////////

    public function dsAttRelnMaster(){
        //$this->dsAttRelnUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_ds_att_reln(att_id,att_type_id,att_id_reln, att_type_id_reln,actv) SELECT a.ATT_ID,a.ATT_TYPE_ID,a.ATT_ID_RELN,a.ATT_TYPE_ID_RELN,a.ACTV FROM ".$intrDB.".intrm".$n."ds".$n."att".$n."reln a WHERE a.POS_ATT_ID_RELN IS NULL OR a.POS_ATT_ID_RELN = ''";
        if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."ds".$n."att".$n."reln a INNER JOIN ".$posDB.".sma_ds_att_reln b ON (a.ATT_ID = b.att_id AND a.ATT_ID_RELN = b.att_id_reln) SET a.POS_ATT_ID_RELN = b.id,a.SYNC_FLG='1' WHERE (a.ATT_ID = b.att_id AND a.ATT_ID_RELN = b.att_id_reln)";
             if($this->db->query($w1))
             {
                $msg = "DS ATT RELN Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    public function dsAttRelnUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_ds_att_reln a INNER JOIN ".$intrDB.".intrm".$n."ds".$n."att".$n."reln b ON (b.POS_ATT_ID_RELN = a.id) SET a.att_id = b.ATT_ID, a.att_type_id = b.ATT_TYPE_ID, a.att_id_reln= b.ATT_ID_RELN, a.att_type_id_reln = b.ATT_TYPE_ID_RELN,a.actv = b.ACTV WHERE b.SYNC_FLG = '3' AND (b.POS_ATT_ID_RELN = a.id)";
        if($this->db->query($w)){
            $w1 = "UPDATE ".$intrDB.".intrm".$n."ds".$n."att".$n."reln a INNER JOIN ".$posDB.".sma_ds_att_reln b ON (a.POS_ATT_ID_RELN = b.id) SET a.SYNC_FLG = '' WHERE (a.POS_ATT_ID_RELN = b.id) AND a.SYNC_FLG = '3'";
            if($this->db->query($w)){
                   $msg = "DS ATT RELN Records Update successfully...";
                   return true;

            } else {
               $msg = $this->db->error();
               return $msg['message'];
       }

        } else {
            $msg = $this->db->error();
            return $msg['message'];
       }

    }

    /////////////////////////////////

    public function dsAttTypeMaster(){
       // $this->dsAttTypeUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_ds_att_type(att_type_id,att_type_name,att_type_actv) SELECT a.ATT_TYPE_ID,a.ATT_TYPE_NM,a.ATT_TYPE_ACTV FROM ".$intrDB.".intrm".$n."ds".$n."att".$n."type a WHERE a.POS_ATT_TYPE_ID IS NULL OR a.POS_ATT_TYPE_ID = ''";
        if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."ds".$n."att".$n."type a INNER JOIN ".$posDB.".sma_ds_att_type b ON (a.ATT_TYPE_ID = b.att_type_id) SET a.POS_ATT_TYPE_ID = b.id,a.SYNC_FLG='1' WHERE (a.ATT_TYPE_ID = b.att_type_id)";
             if($this->db->query($w1))
             {
                $msg = "DS ATT TYPE Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    public function dsAttTypeUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_ds_att_type a INNER JOIN ".$intrDB.".intrm".$n."ds".$n."att".$n."type b ON (b.POS_ATT_TYPE_ID = a.id) SET a.att_type_id = b.ATT_TYPE_ID, a.att_type_name = b.ATT_TYPE_NM, a.att_type_actv = b.ATT_TYPE_ACTV  WHERE b.SYNC_FLG = '3' AND (b.POS_ATT_TYPE_ID = a.id)";
        if($this->db->query($w)){
            $w1 = "UPDATE ".$intrDB.".intrm".$n."ds".$n."att".$n."type a INNER JOIN ".$posDB.".sma_ds_att_type b ON (a.POS_ATT_TYPE_ID = b.id) SET a.SYNC_FLG = '' WHERE (a.POS_ATT_TYPE_ID = b.id) AND a.SYNC_FLG = '3'";
            if($this->db->query($w)){
                   $msg = "DS ATT Type Records Update successfully...";
                   return true;

            } else {
               $msg = $this->db->error();
               return $msg['message'];
       }

        } else {
            $msg = $this->db->error();
            return $msg['message'];
       }

    }

    ////////////////////////////////////////////

   public function uomClsMaster(){
        //$this->uomClsUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_uom_cls(uom_class_id,uom_class_name,usr_id_create,usr_id_create_date) SELECT a. UOM_CLASS_ID,a.UOM_CLASS_NM,a.USR_ID_CREATE,DATE_FORMAT(a.USR_ID_CREATE_DT,'%Y-%m-%d') FROM ".$intrDB.".intrm".$n."uom".$n."cls a WHERE a.POS_UOM_CLASS_ID IS NULL OR a.POS_UOM_CLASS_ID = ''";
        if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."uom".$n."cls a INNER JOIN ".$posDB.".sma_uom_cls b ON (a.UOM_CLASS_ID = b.uom_class_id) SET a.POS_UOM_CLASS_ID = b.id,a.SYNC_FLG='1' WHERE (a.UOM_CLASS_ID = b.uom_class_id)";
             if($this->db->query($w1))
             {
                $msg = "UOM CLASS Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    public function uomClsUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_uom_cls a INNER JOIN ".$intrDB.".intrm".$n."uom".$n."cls b ON (b.POS_UOM_CLASS_ID = a.id) SET a.uom_class_id = b.UOM_CLASS_ID,a.uom_class_name = b.UOM_CLASS_NM WHERE b.SYNC_FLG = '3' AND (b.POS_UOM_CLASS_ID = a.id)";
        if($this->db->query($w)){
            $w1 = "UPDATE ".$intrDB.".intrm".$n."uom".$n."cls a INNER JOIN ".$posDB.".sma_uom_cls b ON (a.POS_UOM_CLASS_ID = b.id) SET a.SYNC_FLG = '' WHERE (a.POS_UOM_CLASS_ID = b.id) AND a.SYNC_FLG = '3'";
            if($this->db->query($w)){
                   $msg = "UOM CLS Records Update successfully...";
                   return true;

            } else {
               $msg = $this->db->error();
               return $msg['message'];
       }

        } else {
            $msg = $this->db->error();
            return $msg['message'];
       }

    }

    ////////////////////////////////////////////

   public function uomConStdvMaster(){
        //$this->uomConStdvUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_uom_conv_std(uom_id,uom_name,uom_desc,uom_class,base_uom,conv_fctr,actv,inactv_resn,inactv_date,uom_std_ent_id,usr_id_create,usr_id_create_dt) SELECT a.UOM_ID,a.UOM_NM,a.UOM_DESC,a.UOM_CLASS,a.BASE_UOM,a.CONV_FCTR,a.ACTV,a.INACTV_RESN,DATE_FORMAT(a.INACTV_DT,'%Y-%m-%d'),a.UOM_STD_ENT_ID,a.USR_ID_CREATE,DATE_FORMAT(a.USR_ID_CREATE_DT,'%Y-%m-%d') FROM ".$intrDB.".intrm".$n."uom".$n."conv".$n."std a WHERE a.POS_UOM_ID IS NULL OR a.POS_UOM_ID = ''";
        if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."uom".$n."conv".$n."std a INNER JOIN ".$posDB.".sma_uom_conv_std b ON (a.UOM_ID = b.uom_id) SET a.POS_UOM_ID = b.id,a.SYNC_FLG='1' WHERE (a.UOM_ID = b.uom_id)";
             if($this->db->query($w1))
             {
                $msg = "UOM CONV STD Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    public function uomConStdvUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_uom_conv_std a INNER JOIN ".$intrDB.".intrm".$n."uom".$n."conv".$n."std b ON (b.POS_UOM_ID = a.id) SET a.uom_name = b.UOM_NM,a.uom_desc=b.UOM_DESC,a.uom_class=b.UOM_CLASS,a.base_uom=b.BASE_UOM, a.conv_fctr=b.CONV_FCTR,a.actv= b.ACTV,a.inactv_resn=b.INACTV_RESN,a.inactv_date=DATE_FORMAT(b.INACTV_DT,'%Y-%m-%d'),a.uom_std_ent_id=b.UOM_STD_ENT_ID WHERE b.SYNC_FLG = '3' AND (b.POS_UOM_ID = a.id)";
        if($this->db->query($w)){
            $w1 = "UPDATE ".$intrDB.".intrm".$n."uom".$n."conv".$n."std a INNER JOIN ".$posDB.".sma_uom_conv_std b ON (a.POS_UOM_ID = b.id) SET a.SYNC_FLG = '' WHERE (a.POS_UOM_ID = b.id) AND a.SYNC_FLG = '3'";
            if($this->db->query($w)){
                   $msg = "UOM CLS Records Update successfully...";
                   return true;

            } else {
               $msg = $this->db->error();
               return $msg['message'];
       }

        } else {
            $msg = $this->db->error();
            return $msg['message'];
       }

    }


    ////////////////////////////////////////////

   public function segmentMaster(){
        $ar = $this->segmentMasterUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_segment(org_id,prj_doc_id,parent_proj_id,proj_name,prj_actv,adds) SELECT a.ORG_ID,a.PRJ_DOC_ID,a.PARENT_PROJ_ID,a.PROJ_NM,a.PRJ_ACTV,a.ADDRESS FROM ".$intrDB.".intrm".$n."segment a WHERE a.POS_PRJ_ID IS NULL OR a.POS_PRJ_ID = ''";
        if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."segment a INNER JOIN ".$posDB.".sma_segment b ON ((a.PRJ_DOC_ID = b.prj_doc_id) AND (a.PARENT_PROJ_ID = b.parent_proj_id) AND (a.ORG_ID = b.org_id)) SET a.POS_PRJ_ID = b.id,a.SYNC_FLG='1' WHERE ((a.PRJ_DOC_ID = b.prj_doc_id) AND (a.PARENT_PROJ_ID = b.parent_proj_id) AND (a.ORG_ID = b.org_id))";
             if($this->db->query($w1))
             {
                $msg = "Segment Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    public function segmentMasterUPD()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$posDB.".sma_segment a INNER JOIN ".$intrDB.".intrm".$n."segment b ON (a.id = b.POS_PRJ_ID) SET a.org_id = b.ORG_ID,a.prj_doc_id = b.PRJ_DOC_ID,a.parent_proj_id = b.PARENT_PROJ_ID,a.proj_name = b.PROJ_NM,a.prj_actv= b.PRJ_ACTV,a.adds = b.ADDRESS,a.pos_upd_flg='1' WHERE b.UPD_FLG= '3'";
        
        if ($this->db->query($w)) {
           // $w1 = "UPDATE ".$intrDB.".intrm".$n."segment a INNER JOIN ".$posDB.".sma_segment b ON (b.id = a.POS_PRJ_ID) SET a.SYNC_FLG = '' WHERE (b.id = a.POS_PRJ_ID) AND a.SYNC_FLG= '3'";
            $w1 = "UPDATE ".$intrDB.".intrm".$n."segment a SET a.SYNC_FLG = '' WHERE a.SYNC_FLG= '3'";
            $this->db->query($w1);
            return true;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }

    }

    public function gstinfoupd(){

        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        //---------------------Fro Gst Inpliment -------------------------------
        //$w = "UPDATE ".$posDB.".sma_segment a INNER JOIN ".$intrDB.".intrm".$n."org b ON (a.org_id = b.ORG_ID) SET a.gst_regn_no = b.GST_REGN_NO,a.gst_regn_dt = b.GST_REGN_DT,a.gst_regn_flg = b.GST_REGN_FLG, a.org_desc = b.ORG_DESC,a.tax_code = b.TAX_CODE,a.adds_loc = b.ADDS_LOC,a.adds = b.ADDS, a.pos_upd_flg='1' WHERE b.UPD_FLG= '3' AND (a.org_id = b.ORG_ID) ";
        $w = "UPDATE ".$posDB.".sma_segment a INNER JOIN ".$intrDB.".intrm".$n."org b ON (a.org_id = b.ORG_ID) SET a.gst_regn_no = b.GST_REGN_NO,a.gst_regn_dt = b.GST_REGN_DT,a.gst_regn_flg = b.GST_REGN_FLG, a.org_desc = b.ORG_DESC,a.tax_code = b.TAX_CODE,a.adds_loc = b.ADDS_LOC,a.pos_upd_flg='1' WHERE b.UPD_FLG= '3' AND (a.org_id = b.ORG_ID) ";
        
		if ($this->db->query($w)) {

            $w1 = "UPDATE ".$intrDB.".intrm".$n."org a SET a.UPD_FLG = '' WHERE a.UPD_FLG= '3'";
            $this->db->query($w1);
            return true;

        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }

    }


   ////////////////////////////////////////////

  /* public function segmentEntMaster(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_segment_ent(org_id,prj_doc_id,prj_ent_type_id,prj_ent_id) SELECT a.ORG_ID,a.PRJ_DOC_ID,a.PRJ_ENT_TYP_ID,a.PRJ_ENT_ID FROM ".$intrDB.".intrm".$n."segment".$n."ent a WHERE a.POS_PRJ_ID IS NULL OR a.POS_PRJ_ID = ''";
        if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."segment".$n."ent a INNER JOIN ".$posDB.".sma_segment_ent b ON ((a.PRJ_DOC_ID = b.prj_doc_id) AND (a.PRJ_ENT_ID = b.prj_ent_id) AND (a.ORG_ID = b.org_id)) SET a.POS_PRJ_ID = b.id,a.SYNC_FLG='1' WHERE ((a.PRJ_DOC_ID = b.prj_doc_id) AND (a.PRJ_ENT_ID = b.prj_ent_id) AND (a.ORG_ID = b.org_id))";
             if($this->db->query($w1))
             {
                $msg = "Segment Ent Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    } */

    ////////////////////////////////////////////

   public function whORGMaster(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_wh_org(org_id,wh_id,usr_id_create,usr_id_create_dt,wh_name,wh_onrshp_type,wh_strg_type,adds_id,wh_desc,actv,inactv_resn,inactv_dt,wh_ent_id,prj_id,segment_id) SELECT a.ORG_ID,a.WH_ID,a.USR_ID_CREATE,DATE_FORMAT(a.USR_ID_CREATE_DT,'%Y-%m-%d'),a.WH_NM,a.WH_ONRSHP_TYPE,a.WH_STRG_TYPE,a.ADDS_ID,a.WH_DESC,a.ACTV,a.INACTV_RESN,DATE_FORMAT(a.INACTV_DT,'%Y-%m-%d'),a.WH_ENT_ID,a.PRJ_ID,z.id FROM ".$intrDB.".intrm".$n."wh".$n."org a INNER JOIN ".$posDB.".sma_segment z ON (a.PRJ_ID = z.prj_doc_id) ";
        if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."wh".$n."org a INNER JOIN ".$posDB.".sma_wh_org b ON ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id)) SET a.POS_WH_ID = b.id,a.SYNC_FLG='1' WHERE ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id))";
             if($this->db->query($w1))
             {
                $msg = "Warehouse ORG Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    ////////////////////////////////////////////

    public function stockItmMaster(){  
        $ar = $this->stockItmMasterUPD123();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        //$w= "INSERT INTO ".$posDB.".sma_item_stk_profile(org_id,segment_id,warehouse_id,wh_id,fy_id,item_id,item_uom_id,total_stk,available_stk,rejected_stk,rework_stk,ordered_stk,sales_price,created_date,code) SELECT a.ORG_ID,z.segment_id,z.id,a.WH_ID,a.FY_ID,b.id,q.uom_desc,a.TOT_STK,a.AVL_STK,a.REJ_STK,a.RWK_STK,a.ORD_STK,b.price,DATE_FORMAT(a.USR_ID_MOD_DT,'%Y-%m-%d'),a.ITM_ID FROM ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."itm a INNER JOIN ".$posDB.".sma_products b ON ((a.ITM_ID = b.code) AND (a.ORG_ID = b.org_id) AND (a.WH_ID = b.wh_id)) INNER JOIN ".$posDB.".sma_warehouses z ON ((a.WH_ID = z.wh_id) AND (a.ORG_ID = z.org_id)) INNER JOIN ".$posDB.".sma_uom_conv_std q ON (a.ITM_UOM_BS = q.uom_id) WHERE (a.POS_STK_ITM_ID IS NULL OR a.POS_STK_ITM_ID = '')";
        $w= "INSERT INTO ".$posDB.".sma_item_stk_profile(org_id,segment_id,warehouse_id,wh_id,fy_id,item_id,item_uom_id,total_stk,available_stk,rejected_stk,rework_stk,ordered_stk,sales_price,created_date,code,o_qty) SELECT a.ORG_ID,z.segment_id,z.id,a.WH_ID,a.FY_ID,b.id,b.unit,a.TOT_STK,a.AVL_STK,a.REJ_STK,a.RWK_STK,a.ORD_STK,b.price,DATE_FORMAT(a.USR_ID_MOD_DT,'%Y-%m-%d'),a.ITM_ID,a.AVL_STK FROM ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."itm a INNER JOIN ".$posDB.".sma_products b ON ((a.ITM_ID = b.code) AND (a.ORG_ID = b.org_id) AND (a.WH_ID = b.wh_id)) INNER JOIN ".$posDB.".sma_warehouses z ON ((a.WH_ID = z.wh_id) AND (a.ORG_ID = z.org_id)) WHERE (a.POS_STK_ITM_ID IS NULL OR a.POS_STK_ITM_ID = '')";
       if($this->db->query($w)){
             $wpos = "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."itm".$n."pos SELECT * FROM ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."itm b WHERE (b.POS_STK_ITM_ID IS NULL OR b.POS_STK_ITM_ID = '')";
             $this->db->query($wpos);
             $w1= "UPDATE ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."itm a INNER JOIN ".$posDB.".sma_item_stk_profile b ON ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id) AND (a.ITM_ID = b.code)) SET a.POS_STK_ITM_ID = b.id WHERE ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id) AND (a.ITM_ID = b.code))";
             $w4= "UPDATE ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."itm".$n."pos a INNER JOIN ".$posDB.".sma_item_stk_profile b ON ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id) AND (a.ITM_ID = b.code)) SET a.POS_STK_ITM_ID = b.id WHERE ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id) AND (a.ITM_ID = b.code))";
             $this->db->query($w4);
             if($this->db->query($w1))
             {
                $msg = "Stock Item Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    public function stockItmMasterUPD123()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$posDB.".sma_item_stk_profile b INNER JOIN ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."itm a ON (a.POS_STK_ITM_ID = b.id) SET b.total_stk = a.TOT_STK, b.available_stk = a.AVL_STK, b.rejected_stk = a.REJ_STK, b.rework_stk = a.RWK_STK,b.pos_upd_flg = '1' WHERE a.UPD_FLG='3'";
        if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."itm a INNER JOIN ".$posDB.".sma_item_stk_profile b ON (a.POS_STK_ITM_ID = b.id) SET a.UPD_FLG = '' WHERE a.UPD_FLG = '3'";
             if($this->db->query($w1))
             {
                $msg = "Stock ITM PROFILE Records Update successfully...";
                return true;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }

    }


    ////////////////////////////////////////////

     public function stockLotMaster(){
       $ar = $this->stockItmLotMasterUPD123(); 
       $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "INSERT INTO ".$posDB.".sma_item_stk_lot(org_id,segment_id,warehouse_id,wh_id,lot_no,item_stk_id,item_id,batch_no, itm_uom_id,total_stk,rejected_stk,mfg_date,exp_date,reworkable_stk,created_date,code) SELECT a.ORG_ID,d.segment_id,d.warehouse_id,a.WH_ID,a.LOT_ID,d.id,d.item_id,a.BATCH_NO,d.item_uom_id,a.TOT_STK,a.REJ_STK,DATE_FORMAT(a.MFG_DT,'%Y-%m-%d'),DATE_FORMAT(a.EXPIRY_DT,'%Y-%m-%d'),a.RWK_STK,DATE_FORMAT(a.LOT_DT,'%Y-%m-%d'),a.ITM_ID FROM ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."lot a INNER JOIN ".$posDB.".sma_item_stk_profile d ON (a.ITM_ID = d.code AND a.ORG_ID = d.org_id AND a.WH_ID = d.wh_id) WHERE a.POS_STK_LOT_ID IS NULL OR a.POS_STK_LOT_ID = ''";
       if($this->db->query($w)){
             $wpos = "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."lot".$n."pos SELECT * FROM ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."lot b WHERE (b.POS_STK_LOT_ID IS NULL OR b.POS_STK_LOT_ID = '')";
             $this->db->query($wpos);
             $w1= "UPDATE ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."lot a INNER JOIN ".$posDB.".sma_item_stk_lot b ON ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id) AND (a.ITM_ID = b.code) AND (a.TOT_STK = b.total_stk)) SET a.POS_STK_LOT_ID = b.id WHERE ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id) AND (a.ITM_ID = b.code) AND (a.TOT_STK = b.total_stk))";
             $w4= "UPDATE ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."lot".$n."pos a INNER JOIN ".$posDB.".sma_item_stk_lot b ON ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id) AND (a.ITM_ID = b.code) AND (a.TOT_STK = b.total_stk)) SET a.POS_STK_LOT_ID = b.id WHERE ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id) AND (a.ITM_ID = b.code) AND (a.TOT_STK = b.total_stk))";
             $this->db->query($w4);
             if($this->db->query($w1))
             {
                $msg = "Stock Bin Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    } 

    public function stockItmLotMasterUPD123()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$posDB.".sma_item_stk_lot b INNER JOIN ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."lot a ON (a.POS_STK_LOT_ID = b.id) SET b.lot_no = a.LOT_ID, b.batch_no = a.BATCH_NO,b.total_stk =a.TOT_STK, b.rejected_stk = a.REJ_STK, b.reworkable_stk = a.RWK_STK,b.mfg_date = DATE_FORMAT(a.MFG_DT,'%Y-%m-%d'),b.exp_date = DATE_FORMAT(a.EXPIRY_DT,'%Y-%m-%d'),b.pos_upd_flg = '1' WHERE a.UPD_FLG='3'";
        if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."lot a INNER JOIN ".$posDB.".sma_item_stk_lot b ON (a.POS_STK_LOT_ID = b.id) SET a.UPD_FLG = '' WHERE a.UPD_FLG = '3'";
             if($this->db->query($w1))
             {
                $msg = "Stock ITM PROFILE LOT Records Update successfully...";
                return true;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }

    }


    ////////////////////////////////////////////

     public function stockLotBinMaster(){
        $ar = $this->stockLotBinMaster123();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "INSERT INTO ".$posDB.".sma_item_stk_lot_bin(org_id,segment_id,warehouse_id,wh_id,item_id,lot_id,bin_id,itm_uom_id,total_stk,rejected_stk,reworkable_stk,created_date,code) SELECT a.ORG_ID,k.segment_id,k.warehouse_id,a.WH_ID,k.item_id,k.id,c.id,k.itm_uom_id,a.TOT_STK,a.REJ_STK,a.RWK_STK,DATE_FORMAT(a.USR_ID_MOD_DT,'%Y-%m-%d'),a.ITM_ID FROM ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."bin a INNER JOIN ".$posDB.".sma_bin c ON ((a.BIN_ID = c.bin_id) AND (a.ORG_ID = c.org_id) AND (a.WH_ID = c.wh_id)) INNER JOIN ".$posDB.".sma_item_stk_lot k ON (k.code = a.ITM_ID AND a.ORG_ID = k.org_id AND a.WH_ID = k.wh_id AND k.lot_no = a.LOT_ID) WHERE a.POS_BIN_ID IS NULL OR a.POS_BIN_ID = ''";
         if($this->db->query($w)){
            $wpos = "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."bin".$n."pos SELECT * FROM ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."bin b WHERE (b.POS_BIN_ID IS NULL OR b.POS_BIN_ID = '')";
             $this->db->query($wpos);
             $w1= "UPDATE ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."bin a INNER JOIN ".$posDB.".sma_item_stk_lot_bin b ON ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id) AND (a.ITM_ID = b.code) AND (a.TOT_STK = b.total_stk)) SET a.POS_BIN_ID = b.id WHERE ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id) AND (a.ITM_ID = b.code) AND (a.TOT_STK = b.total_stk))";
             $w4= "UPDATE ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."bin".$n."pos a INNER JOIN ".$posDB.".sma_item_stk_lot_bin b ON ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id) AND (a.ITM_ID = b.code) AND (a.TOT_STK = b.total_stk)) SET a.POS_BIN_ID = b.id WHERE ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id) AND (a.ITM_ID = b.code) AND (a.TOT_STK = b.total_stk))";
             $this->db->query($w4);
             if($this->db->query($w1))
             {
                $msg = "Stock Bin Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    public function stockLotBinMaster123()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$posDB.".sma_item_stk_lot_bin b INNER JOIN ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."bin a ON (a.POS_BIN_ID = b.id) INNER JOIN ".$posDB.".sma_bin c ON ((a.BIN_ID = c.bin_id) AND (a.ORG_ID = c.org_id) AND (a.WH_ID = c.wh_id)) SET b.total_stk = a.TOT_STK, b.rejected_stk = a.REJ_STK, b.reworkable_stk = a.RWK_STK,b.bin_id = c.id, b.pos_upd_flg = '1' WHERE a.UPD_FLG='3'";
        if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."bin a INNER JOIN ".$posDB.".sma_item_stk_lot_bin b ON (a.POS_BIN_ID = b.id) SET a.UPD_FLG = '' WHERE a.UPD_FLG = '3'";
             if($this->db->query($w1))
             {
                $msg = "Stock ITM PROFILE LOT Bin Records Update successfully...";
                return true;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }

    }

    ////////////////////////////////////////////

     public function emrsMaster(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_emrs(org_id,segment_id,warehouse_id,wh_id,fy_id,doc_id,emrs_type,supplier_id,emrs_no,emrs_dt,org_id_req_to,wh_id_required_to,reqd_dt,emrs_status,emrs_status_dt,remarks,emrs_mode) SELECT a.ORG_ID,b.segment_id,b.id,a.WH_ID_REQ_TO,a.FY_ID,a.DOC_ID,a.EMRS_TYPE,a.EO_ID,a.EMRS_NO,DATE_FORMAT(a.EMRS_DT, '%Y-%m-%d'),a.ORG_ID_REQ_TO,a.WH_ID_REQ_TO,DATE_FORMAT(a.REQD_DT,'%Y-%m-%d'),a.EMRS_STAT,DATE_FORMAT(a.EMRS_STAT_DT,'%Y-%m-%d'),a.REMARKS,a.EMRS_MODE FROM ".$intrDB.".intrm".$n."mm".$n."emrs a INNER JOIN ".$posDB.".sma_warehouses b ON (a.ORG_ID = b.ORG_ID AND a.WH_ID_REQ_TO = b.wh_id) WHERE a.POS_MM_EMRS_ID IS NULL OR a.POS_MM_EMRS_ID = ''";
         if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."mm".$n."emrs a INNER JOIN ".$posDB.".sma_emrs b ON ((a.WH_ID_REQ_TO = b.wh_id_required_to) AND (a.ORG_ID = b.org_id) AND (a.DOC_ID = b.doc_id)) SET a.POS_MM_EMRS_ID = b.id WHERE ((a.WH_ID_REQ_TO = b.wh_id_required_to) AND (a.ORG_ID = b.org_id) AND (a.DOC_ID = b.doc_id))";
             if($this->db->query($w1))
             {
                $msg = "EMRS Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    ////////////////////////////////////////////

     public function emrsDocSrcMaster(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$posDB.".sma_emrs_doc_src(org_id,segment_id,warehouse_id,wh_id,doc_id,doc_no,doc_date) SELECT a.ORG_ID,b.segment_id,b.warehouse_id,b.wh_id,a.DOC_ID,a.DOC_NO_SRC,DATE_FORMAT(a.DOC_DT_SRC,'%Y-%m-%d') FROM ".$intrDB.".intrm".$n."mm".$n."emrs".$n."doc".$n."src a INNER JOIN ".$posDB.".sma_emrs b ON (a.DOC_ID = b.doc_id) WHERE a.POS_MM_EMRS_DOC_SRC_ID IS NULL OR a.POS_MM_EMRS_DOC_SRC_ID = ''";
         if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."mm".$n."emrs".$n."doc".$n."src a INNER JOIN ".$posDB.".sma_emrs_doc_src b ON ((a.DOC_ID = b.doc_id) AND (a.DOC_NO_SRC = b.doc_no)) SET a.POS_MM_EMRS_DOC_SRC_ID = b.id WHERE ((a.DOC_ID = b.doc_id) AND (a.DOC_NO_SRC = b.doc_no))";
             if($this->db->query($w1))
             {
                $msg = "EMRS DOC SRC Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    ////////////////////////////////////////////

     public function emrsItemMaster(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
         $w= "INSERT INTO ".$posDB.".sma_emrs_item(org_id,segment_id,warehouse_id,wh_id,doc_id,doc_no_src_id,item_id,req_qty,bal_qty,itm_uom_bs,uom_conv_fctr,req_qty_bs,itm_price_bs,itm_amt_bs,line_seq_no,code) SELECT a.ORG_ID,b.segment_id,b.warehouse_id,b.wh_id,a.DOC_ID,a.DOC_NO_SRC,c.id,a.REQ_QTY,a.REQ_QTY,a.ITM_UOM_BS,a.UOM_CONV_FCTR,a.REQ_QTY_BS,a.ITM_PRICE_BS,a.ITM_AMT_BS,a.LINE_SEQ_NO,a.ITM_ID FROM ".$intrDB.".intrm".$n."mm".$n."emrs".$n."itm a INNER JOIN ".$posDB.".sma_emrs b ON (a.DOC_ID = b.doc_id) INNER JOIN ".$posDB.".sma_products c ON (a.ITM_ID = c.code AND a.ORG_ID = c.org_id AND b.wh_id = c.wh_id) WHERE a.POS_MM_EMRS_ITM_ID IS NULL OR a.POS_MM_EMRS_ITM_ID = ''";
        if($this->db->query($w)){
             $w1= "UPDATE ".$intrDB.".intrm".$n."mm".$n."emrs".$n."itm a INNER JOIN ".$posDB.".sma_emrs_item b ON ((a.DOC_ID = b.doc_id) AND (a.DOC_NO_SRC = b.doc_no_src_id) AND (a.ITM_ID = b.code)) SET a.POS_MM_EMRS_ITM_ID = b.id WHERE ((a.DOC_ID = b.doc_id) AND (a.DOC_NO_SRC = b.doc_no_src_id) AND (a.ITM_ID = b.code))";
             if($this->db->query($w1))
             {
                $msg = "EMRS Item Records save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    

    
    // I.T.-01 (Pos -> intermediate) Customer
    
    public function eoMaster()
    {
        $this->eoMasterUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "INSERT INTO ".$intrDB.".intrm".$n."eo( CLD_ID, SLOC_ID, HO_ORG_ID,EO_ORG_ID,EO_TYPE, POS_EO_ID, EO_CATG_ID, EO_NM, EO_PHONE, VAT_NO, USR_ID_CREATE, USR_ID_CREATE_DT, USR_ID_MOD, USR_ID_MOD_DT ) SELECT '0000', '1', '01', b.org_id,'C', a.id, 'T002', a.name, a.phone, a.vat_no, '1', CURDATE(), '1', CURDATE() FROM ".$posDB.".sma_companies a INNER JOIN ".$posDB.".sma_segment b ON (a.segment_id = b.id) WHERE a.id != '1' AND a.group_name != 'biller' AND( a.sync_flg IS NULL OR a.sync_flg = '' )";
     if ($this->db->query($w)) {
          $w1= "INSERT INTO ".$intrDB.".intrm".$n."eo".$n."add(CLD_ID,SLOC_ID,HO_ORG_ID,DEF_ADD_FLG,EO_TYPE,POS_EO_ID,USR_ID_CREATE,USR_ID_CREATE_DT,USR_ID_MOD,USR_ID_MOD_DT,ADDRESS,UPD_FLG) SELECT '0000','1','01','N','C',a.id,'1',curdate(),'1',curdate(),CONCAT(a.address,' ',a.city),'1' FROM ".$posDB.".sma_companies a WHERE a.id!= '1' AND a.group_name!='biller' AND (a.sync_flg IS NULL OR a.sync_flg= '')";
          //$w2= "INSERT INTO ".$intrDB.".intrm".$n."eo".$n."add".$n."org(CLD_ID,SLOC_ID,HO_ORG_ID,ORG_ID,EO_TYPE,POS_EO_ID,CREATE_FLG,USR_ID_CREATE,USR_ID_CREATE_DT,USR_ID_MOD,USR_ID_MOD_DT,UPD_FLG) SELECT '0000','1','01',b.org_id,'C',a.id,'1','1',curdate(),'1',curdate(),'1' FROM ".$posDB.".sma_companies a CROSS JOIN ".$posDB.".sma_warehouses b WHERE a.id != '1' AND a.group_name != 'biller' AND(a.sync_flg IS NULL OR a.sync_flg = '')";
          //$w3= "INSERT INTO ".$intrDB.".intrm".$n."eo".$n."org(CLD_ID,SLOC_ID,HO_ORG_ID,ORG_ID,EO_TYPE,POS_EO_ID,CREATE_FLG,USR_ID_CREATE,USR_ID_CREATE_DT,USR_ID_MOD,USR_ID_MOD_DT) SELECT '0000','1','01',b.org_id,'C',a.id,'1','1',curdate(),'1',curdate() FROM ".$posDB.".sma_companies a CROSS JOIN ".$posDB.".sma_warehouses b WHERE a.id != '1' AND a.group_name != 'biller' AND(a.sync_flg IS NULL OR a.sync_flg = '')";
         // $this->db->query($w1);
          //$this->db->query($w2);
          if ($this->db->query($w1)) {
             $w4 = "UPDATE ".$posDB.".sma_companies a INNER JOIN ".$intrDB.".intrm".$n."eo b ON a.id = b.POS_EO_ID SET a.sync_flg='1' WHERE a.id != '1' AND a.group_name != 'biller' AND(a.sync_flg IS NULL OR a.sync_flg = '')";
            $this->db->query($w4);
            return true;

          }  else {
                   $msg = $this->db->error();
                   return $msg['message'];
          }    
      } else {
            $msg = $this->db->error();
            return $msg['message'];
      }
    }

    public function eoMasterUPD()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$intrDB.".intrm".$n."eo a INNER JOIN ".$posDB.".sma_companies b ON b.id = a.POS_EO_ID SET a.EO_NM = b.name,a.EO_PHONE = b.phone,a.VAT_NO = b.vat_no WHERE b.id!= '1' AND b.group_name!='biller' AND (b.upd_flg= '1')";
        if ($this->db->query($w)) {
            $w1= "UPDATE ".$posDB.".sma_companies a SET a.upd_flg = null WHERE a.id != '1' AND a.group_name != 'biller' AND (a.upd_flg= '1')";
            $this->db->query($w1);
            return true;

        } else {
            $msg = $this->db->error();
            return $msg['message']; 
        }
    }
    // I.T.-02 (Pos -> intermediate) sale

    public function slsInvMaster()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "INSERT INTO ".$intrDB.".intrm".$n."sls".$n."inv( CLD_ID, SLOC_ID, HO_ORG_ID, ORG_ID, POS_DOC_ID, DOC_ID, DOC_DT, EO_ID, POS_EO_ID, TAX_BASIS, TAX_ID, TAX_VAL, DISC_BASIS, DISC_TYPE, POS_INV_TOTAL, DISC_VAL, DISC_TOTAL_VAL, USR_ID_CREATE,USR_ID_CREATE_DT, USR_ID_MOD, USR_ID_MOD_DT,WH_ID,PRJ_ID,SYNC_FLG ) SELECT '0000', '1', '01', b.org_id, a.id, a.reference_no, DATE_FORMAT(a.date, '%Y-%m-%d'), c.eo_id, a.customer_id, 'NULL', a.order_tax_id, a.total_tax, 'NULL', 'NULL', a.grand_total, a.total_discount, a.total_discount, 1, CURDATE(), 1, CURDATE(),b.wh_id,s.prj_doc_id,'1' FROM ".$posDB.".sma_sales a INNER JOIN ".$posDB.".sma_warehouses b ON(a.warehouse_id = b.id) LEFT JOIN ".$posDB.".sma_companies c ON(a.customer_id = c.id) INNER JOIN ".$posDB.".sma_segment s ON (s.id = b.segment_id) WHERE a.sync_flg IS NULL OR a.sync_flg = '' ORDER BY a.id ASC";
        if ($this->db->query($w)) {
            $w1  = "UPDATE ".$posDB.".sma_sales a INNER JOIN ".$intrDB.".intrm".$n."sls".$n."inv b ON a.id = b.POS_DOC_ID SET a.sync_flg = '1' WHERE a.id = b.POS_DOC_ID AND (a.sync_flg IS NULL OR a.sync_flg = '')";
            $q   = $this->db->query($w1);
            $msg = "Sales Details Sync Successfully....";
            return $msg;
        } else {
            
            $msg = $this->db->error();
            return $msg['message'];
        }
    }
    
    
    // I.T.-03 (Pos -> intermediate) sale Item

    public function slsInvItmMaster()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "INSERT INTO ".$intrDB.".intrm".$n."sls".$n."inv".$n."itm(CLD_ID,SLOC_ID,HO_ORG_ID,ORG_ID,POS_DOC_ID,DOC_ID,ITM_ID,POS_ITM_ID,SR_NO,LOT_NO,ITM_QTY,ITM_UOM,POS_ITM_UOM,ITM_UOM_BS,POS_ITM_UOM_BS,SLS_PRICE,TAX_VAL,DISC_TOTAL_VAL,DISC_TYPE,DISC_VAL,ITM_TOTAL_VAL, USR_ID_CREATE, USR_ID_CREATE_DT,USR_ID_MOD, USR_ID_MOD_DT,DOC_DATE,WH_ID,PRJ_ID,SYNC_FLG) SELECT '0000','1','01',b.org_id,a.id,a.sale_id,a.product_code,a.product_id,a.serial_no,a.lot_no,a.quantity,z.uom_id,z.id,z.base_uom,a.net_unit_price,
        a.real_unit_price,a.item_tax,a.item_discount,'NULL',a.discount,a.subtotal,1, CURDATE(), 1, CURDATE(),
        DATE_FORMAT(m.date, '%Y-%m-%d'),b.wh_id,b.prj_id,'1' FROM ".$posDB.".sma_sale_items a INNER JOIN ".$posDB.".sma_warehouses b ON (a.warehouse_id = b.id) INNER JOIN ".$posDB.".sma_sales m ON(m.id = a.sale_id) LEFT JOIN ".$posDB.".sma_uom_conv_std z ON (z.uom_desc = a.item_uom_id) WHERE a.sync_flg IS NULL OR a.sync_flg = '' ORDER BY a.id ASC";

      if ($this->db->query($w)) {
            $w1  = "UPDATE ".$posDB.".sma_sale_items a INNER JOIN ".$intrDB.".intrm".$n."sls".$n."inv".$n."itm b ON a.id = b.POS_DOC_ID SET a.sync_flg='1' WHERE a.id = b.POS_DOC_ID AND (a.sync_flg IS NULL OR a.sync_flg = '')";
            $q   = $this->db->query($w1);
            $msg = "Sales Item Details Sync Successfully....";
            return $msg;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }

    ///////////////////////////////////////

    public function slsInvLotMaster()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "INSERT INTO ".$intrDB.".intrm".$n."sls".$n."inv".$n."itm".$n."lot(ORG_ID,POS_SALE_ITM_LOT_ID, WH_ID,ITM_ID,SALE_POS_ID,SALE_ITM_POS_ID,LOT_ID, LOT_NO,QTY, DATE,PRJ_ID,SYNC_FLG) SELECT a.org_id,a.id, a.wh_id, a.code, a.sale_id,a.sale_item_id,a.lot_id,a.lot_no,a.quantity,DATE_FORMAT(a.created_date, '%Y-%m-%d'),d.prj_doc_id,'1' FROM ".$posDB.".sma_sale_item_lot a INNER JOIN ".$posDB.".sma_warehouses c ON (c.id = a.warehouse_id) INNER JOIN ".$posDB.".sma_segment d ON (d.id = c.segment_id) WHERE a.sync_flg IS NULL OR a.sync_flg = ''";

          /*$w21 = "INSERT INTO ".$intrDB.".intrm".$n."sls".$n."inv".$n."itm".$n."bin(ORG_ID,POS_SALE_ITM_BIN_ID,WH_ID,ITM_ID,SALE_POS_ID,SALE_ITM_POS_ID,LOT_ID,BIN_ID,BIN_NM,QTY,DATE,PRJ_ID,SYNC_FLG) SELECT a.org_id,a.id,a.wh_id,a.code,a.sale_id,a.sale_item_id,a.lot_no, asd.bin_id,asd.bin_nm, a.quantity,
          DATE_FORMAT(a.created_date,'%Y-%m-%d'), n.prj_doc_id,'1' FROM ".$posDB.".sma_sale_item_lot a INNER JOIN (SELECT c.org_id,c.warehouse_id, c.wh_id,c.bin_id,c.bin_nm FROM ".$posDB.".sma_bin c GROUP BY c.warehouse_id) AS asd ON (asd.warehouse_id = a.warehouse_id AND asd.org_id = a.org_id) INNER JOIN ".$posDB.".sma_warehouses m ON (m.id = a.warehouse_id) INNER JOIN ".$posDB.".sma_segment n ON (n.id = m.segment_id) WHERE a.sync_flg IS NULL OR a.sync_flg = ''";*/


        if ($this->db->query($w)) {
            //$this->db->query($w21);
            $w1  = "UPDATE ".$posDB.".sma_sale_item_lot a INNER JOIN ".$intrDB.".intrm".$n."sls".$n."inv".$n."itm".$n."lot b ON a.id = b.POS_SALE_ITM_LOT_ID SET a.sync_flg='1' WHERE a.id = b.POS_SALE_ITM_LOT_ID AND (a.sync_flg IS NULL OR a.sync_flg = '')";
            $q   = $this->db->query($w1);
            $msg = "Sales Item Lot Details Sync Successfully....";
            return $msg;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }

    }
    
    ///////////////////////////////////////


    public function slsInvBinMaster()
    {
         $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "INSERT INTO ".$intrDB.".intrm".$n."sls".$n."inv".$n."itm".$n."bin(ORG_ID,POS_SALE_ITM_BIN_ID,WH_ID,ITM_ID,SALE_POS_ID,SALE_ITM_POS_ID,LOT_ID,BIN_ID,BIN_NM,QTY,DATE,PRJ_ID,SYNC_FLG) SELECT a.org_id,a.id,a.wh_id,a.code,a.sale_id,a.sale_item_id,d.lot_no,c.bin_id,a.bin_name,a.quantity,DATE_FORMAT(a.created_date, '%Y-%m-%d'),n.prj_doc_id,'1' FROM ".$posDB.".sma_sale_item_bin a INNER JOIN ".$posDB.".sma_item_stk_lot_bin b ON (b.id = a.bin_id AND b.warehouse_id = a.warehouse_id) INNER JOIN ".$posDB.".sma_bin c ON (c.id = b.bin_id AND c.warehouse_id = b.warehouse_id) INNER JOIN ".$posDB.".sma_item_stk_lot d ON (d.id = a.lot_id AND d.warehouse_id = a.warehouse_id) INNER JOIN ".$posDB.".sma_warehouses m ON (m.id = a.warehouse_id) INNER JOIN ".$posDB.".sma_segment n ON (n.id = m.segment_id)  WHERE a.sync_flg IS NULL OR a.sync_flg = '' ";
        if ($this->db->query($w)) {
            $w1  = "UPDATE ".$posDB.".sma_sale_item_bin a INNER JOIN ".$intrDB.".intrm".$n."sls".$n."inv".$n."itm".$n."bin b ON a.id = b.POS_SALE_ITM_BIN_ID SET a.sync_flg='1' WHERE a.id = b.POS_SALE_ITM_BIN_ID AND (a.sync_flg IS NULL OR a.sync_flg = '')";
            $q   = $this->db->query($w1);
            $msg = "Sales Item Bin Details Sync Successfully....";
            return $msg;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        } 
        
    }


    // I.T.-04 (Pos -> intermediate) Payment Details Sync

    
    public function PaymntMaster()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "INSERT INTO ".$intrDB.".intrm".$n."payment".$n."dtl( CLD_ID, SLOC_ID, HO_ORG_ID, ORG_ID, POS_PAYMNT_ID, POS_INV_NO, PAYMNT_DT, EO_ID, POS_EO_ID, PAID_BY, CC_NM, CC_HOLDER, CC_MONTH, CC_YEAR, CC_TYPE, AMOUNT, SYNC_FLG, USR_ID_CREATE, USR_ID_CREATE_DT, USR_ID_MOD, USR_ID_MOD_DT ) SELECT '0000', '1', '01', c.org_id, a.id, b.reference_no, DATE_FORMAT(a.date,'%Y-%m-%d'), d.eo_id, b.customer_id, a.paid_by, a.cc_no, a.cc_holder, a.cc_month, a.cc_year, a.cc_type, a.amount, '1', '1', CURDATE(), '1', CURDATE() FROM ".$posDB.".sma_payments a INNER JOIN ".$posDB.".sma_sales b ON(a.sale_id = b.id) INNER JOIN ".$posDB.".sma_warehouses c ON(b.warehouse_id = c.id) LEFT JOIN ".$posDB.".sma_companies d ON(b.customer_id = d.id) WHERE a.sync_flg IS NULL OR a.sync_flg = ''";
  if ($this->db->query($w)) {
    $w1= "UPDATE ".$posDB.".sma_payments a INNER JOIN ".$intrDB.".intrm".$n."payment".$n."dtl b ON (a.id = b.POS_PAYMNT_ID) SET a.sync_flg = '1' WHERE a.id = b.POS_PAYMNT_ID";
    $q= $this->db->query($w1);
    $msg = "Payment Details Sync Successfully....";
    return $msg;
  } else {
       $msg = $this->db->error();
       return $msg['message'];
  }

    }



// I.T.-011 (Pos -> intermediate) Payment Details Sync

    
    public function ChequeDtl()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $this->ChequeDtlUPD();
        $n = '$';
        $w = "INSERT INTO ".$intrDB.".intrm".$n."fin".$n."chq".$n."stat".$n."pos(CLD_ID, SLOC_ID, HO_ORG_ID, ORG_ID,WH_ID,POS_CHQ_DTL_ID, POS_SALE_ID, CHEQUE_NO, EO_NAME, BANK_ID,BANK_NAME, CHEQUE_DATE,AMOUNT, CLEARANCE_DATE,TRANSACTION_DATE,REMARKS,PRJ_ID,STATUS,SYNC_FLG,UPD_FLG) SELECT '0000', '1', '01', a.org_id,a.wh_id, a.id,a.sale_id,a.cheque_no,a.customer_name,a.bank_name,c.name,DATE_FORMAT(a.cheque_date,'%Y-%m-%d'), a.amount, DATE_FORMAT(a.clearance_date,'%Y-%m-%d'), DATE_FORMAT(a.transaction_date,'%Y-%m-%d'), a.remarks,b.prj_doc_id,a.status,'1','5' FROM ".$posDB.".sma_cheque_details a INNER JOIN ".$posDB.".sma_segment b ON(a.segment_id = b.id) INNER JOIN ".$posDB.".sma_companies c ON(a.bank_name = c.eo_id AND c.group_name='Bank' AND c.eo_type='B') WHERE a.sync_flg IS NULL OR a.sync_flg = ''";
      /*  $w = "INSERT INTO ".$intrDB.".intrm".$n."fin".$n."chq".$n."stat".$n."pos(CLD_ID, SLOC_ID, HO_ORG_ID, ORG_ID,WH_ID,POS_CHQ_DTL_ID, POS_SALE_ID, CHEQUE_NO, EO_NAME, BANK_NAME,BANK_ID, CHEQUE_DATE,AMOUNT, CLEARANCE_DATE,TRANSACTION_DATE,REMARKS,PRJ_ID,STATUS,SYNC_FLG,UPD_FLG) SELECT '0000', '1', '01', a.org_id,a.wh_id, a.id,a.sale_id,a.cheque_no,a.customer_name,a.bank_name,a.eo_id,DATE_FORMAT(a.cheque_date,'%Y-%m-%d'), a.amount, DATE_FORMAT(a.clearance_date,'%Y-%m-%d'), DATE_FORMAT(a.transaction_date,'%Y-%m-%d'), a.remarks,b.prj_doc_id,a.status,'1','5' FROM ".$posDB.".sma_cheque_details a INNER JOIN ".$posDB.".sma_segment b ON(a.segment_id = b.id) WHERE a.sync_flg IS NULL OR a.sync_flg = ''";*/
  if ($this->db->query($w)) {
    $w1= "UPDATE ".$posDB.".sma_cheque_details a INNER JOIN ".$intrDB.".intrm".$n."fin".$n."chq".$n."stat".$n."pos b ON (a.id = b.POS_CHQ_DTL_ID) SET a.sync_flg = '1' WHERE a.id = b.POS_CHQ_DTL_ID";
    $q= $this->db->query($w1);
    $msg = "Payment Details Sync Successfully....";
    return $msg;
  } else {
       $msg = $this->db->error();
       return $msg['message'];
  }

    }

  public function ChequeDtlUPD()
  {

        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$intrDB.".intrm".$n."fin".$n."chq".$n."stat".$n."pos a INNER JOIN ".$posDB.".sma_cheque_details b ON (a.POS_CHQ_DTL_ID = b.id AND a.WH_ID = b.wh_id AND a.ORG_ID = b.org_id) SET  b.status = a.STATUS,b.clearance_date= DATE_FORMAT(a.CLEARANCE_DATE,'%Y-%m-%d'),b.upd_flg='1' WHERE (a.POS_CHQ_DTL_ID = b.id AND a.WH_ID = b.wh_id AND a.ORG_ID = b.org_id) AND a.UPD_FLG= '3'";
        if ($this->db->query($w)) {
            $w1= "UPDATE ".$intrDB.".intrm".$n."fin".$n."chq".$n."stat".$n."pos b SET b.UPD_FLG= '' WHERE b.UPD_FLG='3'";
            $this->db->query($w1);
            $msg = "Cheque Details Sync Successfully....";
            return $msg;

        } else {
            $msg = $this->db->error();
            return $msg['message'];
     }

  }



// I.T.-05 (Pos -> intermediate) Update PO Status
    public function poStatusUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        /*$w= "UPDATE ".$intrDB.".intrm".$n."drft".$n."po a INNER JOIN ".$posDB.".sma_drft_po b ON (a.PO_ID = b.po_id AND a.DOC_ID = b.doc_id AND a.POS_PO_ID = b.id AND a.AUTH_PO_NO = b.auth_po_no) SET a.PO_STATUS = b.po_status,a.UPD_FLG='1' WHERE b.po_status=218 AND (a.PO_ID = b.po_id AND a.DOC_ID = b.doc_id AND a.POS_PO_ID = b.id AND a.AUTH_PO_NO = b.auth_po_no) ";*/
        $w= "UPDATE ".$intrDB.".intrm".$n."drft".$n."po a INNER JOIN ".$posDB.".sma_drft_po b ON (a.DOC_ID = b.doc_id AND a.AUTH_PO_NO = b.auth_po_no) SET a.PO_STATUS = b.po_status,a.UPD_FLG='1' WHERE b.po_status=218 AND (a.DOC_ID = b.doc_id AND a.AUTH_PO_NO = b.auth_po_no) AND b.status=0 ";
        if ($this->db->query($w)) {
            $w1= "UPDATE ".$posDB.".sma_drft_po b SET b.status=5 WHERE b.po_status=218 AND b.status=0";
            $this->db->query($w1);
            $msg = "PO Status Sync Successfully....";
            return $msg;

        } else {
            $msg = $this->db->error();
            return $msg['message'];
     } 

    }

    public function poSHDLUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        /*$w= "UPDATE ".$intrDB.".intrm".$n."drft".$n."po".$n."dlv".$n."schdl a INNER JOIN ".$posDB.".sma_drft_po_dlv_schdl b ON (a.POS_PO_SCHDL_ID = b.id) INNER JOIN ".$posDB.".sma_drft_po c ON (a.POS_PO_SCHDL_ID = b.id) SET a.BAL_QTY = b.bal_qty,a.UPD_FLG='1' WHERE c.status!=1 AND (a.POS_PO_SCHDL_ID = b.id)";*/
        $w= "UPDATE ".$intrDB.".intrm".$n."drft".$n."po".$n."dlv".$n."schdl a INNER JOIN ".$posDB.".sma_drft_po_dlv_schdl b ON (a.ITM_ID = b.item_id AND a.DLV_QTY = b.dlv_qty AND a.RO_NO = b.ro_no AND date(a.DLV_DT) = date(b.dlv_dt) AND a.ORG_ID = b.org_id AND a.WH_ID = b.wh_id ) INNER JOIN ".$posDB.".sma_drft_po c ON (c.id = b.po_id) INNER JOIN ".$intrDB.".intrm".$n."drft".$n."po z ON (z.DOC_ID = c.doc_id) SET a.BAL_QTY = b.bal_qty,a.UPD_FLG='1' WHERE b.status=1 AND (a.ITM_ID = b.item_id AND a.DLV_QTY = b.dlv_qty AND a.RO_NO = b.ro_no AND date(a.DLV_DT) = date(b.dlv_dt) AND a.ORG_ID = b.org_id AND a.WH_ID = b.wh_id) AND (z.DOC_ID = c.doc_id)";
        if ($this->db->query($w)) {
            $msg = "PO Schedule Sync Successfully....";
            //$w1= "UPDATE ".$posDB.".sma_drft_po_dlv_schdl b SET b.status = 5 WHERE b.status=1";
            //$this->db->query($w1);
            return $msg;

        } else {
            $msg = $this->db->error();
            return $msg['message'];
     } 

    }

    // I.T.-06 (Pos -> intermediate) Update EMRS Bal_Qty
    public function emrsItmUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$intrDB.".intrm".$n."mm".$n."emrs".$n."itm a INNER JOIN ".$posDB.".sma_emrs_item b ON (a.POS_MM_EMRS_ITM_ID = b.id) SET a.SYNC_FLG = '1', a.REQ_QTY = b.bal_qty WHERE b.status= 2 AND (a.POS_MM_EMRS_ITM_ID = b.id)";
        if ($this->db->query($w)) {
            $msg = "EMRS Bal Qty ITEM Update Successfully....";
            return $msg;

        } else {
            $msg = $this->db->error();
            return $msg['message'];
     } 

    }


    // I.T.-000 (Pos -> intermediate) Update Qty


    public function updQty()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$intrDB.".intrm".$n."itm".$n."stock".$n."dtl p INNER JOIN ".$posDB.".sma_products a ON (p.ITM_ID = a.code AND p.ORG_ID = a.org_id AND p.LOT_NO = a.lot_no) SET p.USE_QTY = FLOOR(p.AVAIL_QTY - a.quantity) WHERE a.psale='1'";
        if ($this->db->query($w)) {
            $w1 = "UPDATE ".$posDB.".sma_products a SET a.psale='' WHERE a.psale='1'";
            $this->db->query($w1);
            $msg = "Product Qty Update Successfully....";
            return $msg;
        }
        
        else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }

    ////////////////////////////////////////////

    // I.T.-07 (Pos -> intermediate) Insert Stock Item to ERP

    public function InsertstockItmMaster(){
        $this->stockItmMasterUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."itm(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_STK_ITM_ID,FY_ID,ITM_ID,ITM_UOM_BS,TOT_STK,AVL_STK,ORD_STK,REJ_STK,RWK_STK,MAP_PRICE,WAP_PRICE,CUM_TOT_ISSU_QTY,CREATE_FLG,UPD_FLG) SELECT '0000','1',a.org_id,a.wh_id,a.id,a.fy_id,a.code,m.uom_id,a.total_stk,a.available_stk,a.ordered_stk,a.rejected_stk,a.rework_stk,a.sales_price,a.sales_price,(a.total_stk - a.available_stk),'1','5' from ".$posDB.".sma_item_stk_profile a  INNER JOIN ".$posDB.".sma_uom_conv_std m ON (m.uom_desc = a.item_uom_id) WHERE a.create_flg = '1'";
        $w6= "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."itm".$n."pos(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_STK_ITM_ID,FY_ID,ITM_ID,ITM_UOM_BS,TOT_STK,AVL_STK,ORD_STK,REJ_STK,RWK_STK,MAP_PRICE,WAP_PRICE,CUM_TOT_ISSU_QTY) SELECT '0000','1',a.org_id,a.wh_id,a.id,a.fy_id,a.code,m.uom_id,a.total_stk,a.available_stk,a.ordered_stk,a.rejected_stk,a.rework_stk,a.sales_price,a.sales_price,(a.total_stk - a.available_stk) from ".$posDB.".sma_item_stk_profile a INNER JOIN ".$posDB.".sma_uom_conv_std m ON (m.uom_desc = a.item_uom_id) WHERE a.create_flg = '1'";
        $this->db->query($w6);
        if($this->db->query($w)){
            /* $w5 = "UPDATE ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."itm".$n."pos a, (SELECT ORG_ID,WH_ID,ITM_ID,SUM(ITM_QTY) as sqty FROM ".$intrDB.".intrm".$n."sls".$n."inv".$n."itm GROUP BY ITM_ID,ORG_ID,WH_ID) AS b SET a.CUM_TOT_ISSU_QTY = b.sqty,a.UPD_FLG='1' WHERE (a.ITM_ID = b.ITM_ID AND a.WH_ID = b.WH_ID AND a.ORG_ID = b.ORG_ID)";
            $this->db->query($w5);*/
             $w1= "UPDATE ".$posDB.".sma_item_stk_profile a SET a.create_flg = '' WHERE a.create_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "Stock ITM Records Save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }

    public function stockItmMasterUPD()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
       $w= "UPDATE ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."itm".$n."pos a INNER JOIN ".$posDB.".sma_item_stk_profile b ON (a.ITM_ID = b.code AND a.WH_ID = b.wh_id AND a.ORG_ID = b.org_id) SET a.TOT_STK = b.total_stk, a.AVL_STK = b.available_stk, a.REJ_STK = b.rejected_stk, a.RWK_STK = b.rework_stk,a.CUM_TOT_ISSU_QTY = (b.total_stk - b.available_stk) , a.UPD_FLG='1' WHERE b.upd_flg='1'";
        if($this->db->query($w)){
           /* $w5 = "UPDATE ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."itm".$n."pos a, (SELECT ORG_ID,WH_ID,ITM_ID,SUM(ITM_QTY) as sqty FROM ".$intrDB.".intrm".$n."sls".$n."inv".$n."itm GROUP BY ITM_ID,ORG_ID,WH_ID) AS b SET a.CUM_TOT_ISSU_QTY = b.sqty WHERE (a.ITM_ID = b.ITM_ID AND a.WH_ID = b.WH_ID AND a.ORG_ID = b.ORG_ID)";
            $this->db->query($w5);*/
             $w1= "UPDATE ".$posDB.".sma_item_stk_profile a SET a.upd_flg = '' WHERE a.upd_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "Stock ITM Records Save successfully...";
                return true;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }

    }

    ////////////////////////////////////////////

    // I.T.-08 (Pos -> intermediate) Insert Stock Item LOT to ERP

    public function InsertstockItmLotMaster(){
        $this->stockItmLotUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$'; 
        $w = "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."lot(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_STK_LOT_ID,ITM_ID,BASIC_PRICE,LND_PRICE,LOT_ID,ITM_UOM_BS,TOT_STK,REJ_STK,EXPIRY_DT,RWK_STK,USR_ID_MOD_DT,LOT_DT,BATCH_NO,CREATE_FLG,UPD_FLG) SELECT '0000','1',a.org_id,a.wh_id,a.id,a.code,c.sales_price,c.sales_price,a.lot_no,m.uom_id,a.total_stk,a.rejected_stk,DATE_FORMAT(a.exp_date,'%Y-%m-%d'), a.reworkable_stk, DATE_FORMAT(a.created_date,'%Y-%m-%d'), DATE_FORMAT(a.created_date,'%Y-%m-%d'),a.batch_no,'1','5' from ".$posDB.".sma_item_stk_lot a INNER JOIN ".$posDB.".sma_item_stk_profile c ON (c.id = a.item_stk_id) INNER JOIN ".$posDB.".sma_uom_conv_std m ON (m.uom_desc = a.itm_uom_id) WHERE a.create_flg = '1'";
        $w6 = "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."lot".$n."pos(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_STK_LOT_ID,ITM_ID,BASIC_PRICE,LND_PRICE,LOT_ID,ITM_UOM_BS,TOT_STK,REJ_STK,EXPIRY_DT,RWK_STK,USR_ID_MOD_DT,LOT_DT,BATCH_NO) SELECT '0000','1',a.org_id,a.wh_id,a.id,a.code,c.sales_price,c.sales_price,a.lot_no,m.uom_id,a.total_stk,a.rejected_stk,DATE_FORMAT(a.exp_date,'%Y-%m-%d'),a.reworkable_stk,DATE_FORMAT(a.created_date,'%Y-%m-%d'),DATE_FORMAT(a.created_date,'%Y-%m-%d'),a.batch_no from ".$posDB.".sma_item_stk_lot a INNER JOIN ".$posDB.".sma_item_stk_profile c ON (c.id = a.item_stk_id) INNER JOIN ".$posDB.".sma_uom_conv_std m ON (m.uom_desc = a.itm_uom_id) WHERE a.create_flg = '1'";
        $this->db->query($w6);
        if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_item_stk_lot a SET a.create_flg = '' WHERE a.create_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "Stock ITM LOT Records Save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }

    public function stockItmLotUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."lot".$n."pos a INNER JOIN ".$posDB.".sma_item_stk_lot b ON (a.WH_ID = b.wh_id AND a.ORG_ID = b.org_id AND a.ITM_ID = b.code AND a.LOT_ID = b.lot_no) SET a.TOT_STK = b.total_stk, a.REJ_STK = b.rejected_stk, a.RWK_STK = b.reworkable_stk, a.UPD_FLG='1' WHERE b.upd_flg='1'";
        if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_item_stk_lot a SET a.upd_flg = '' WHERE a.upd_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "Stock ITM LOT Records Save successfully...";
                return true;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }

    }

    ////////////////////////////////////////////

    // I.T.-09 (Pos -> intermediate) Insert Stock Item LOT BIN to ERP

    public function InsertstockItmLotBINMaster(){
           $this->updstockLotBinMaster();
           $posDB = $this->posDbName();
           $intrDB = $this->intrDbName();
           $n = '$';
           $w = "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."bin(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_BIN_ID,ITM_ID,LOT_ID,BIN_ID,ITM_UOM_BS,TOT_STK,REJ_STK,RWK_STK,USR_ID_MOD_DT,CREATE_FLG,UPD_FLG) SELECT '0000','1',a.org_id,a.wh_id,a.id,a.code,f.lot_no,d.bin_id,m.uom_id,a.total_stk,a.rejected_stk,a.reworkable_stk,DATE_FORMAT(a.created_date,'%Y-%m-%d'),'1','5' FROM ".$posDB.".sma_item_stk_lot_bin a INNER JOIN ".$posDB.".sma_bin d ON (d.id = a.bin_id) INNER JOIN ".$posDB.".sma_uom_conv_std m ON (m.uom_desc = a.itm_uom_id) INNER JOIN ".$posDB.".sma_item_stk_lot f ON (f.id = a.lot_id) WHERE a.create_flg = '1'";
           $w6 = "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."bin".$n."pos(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_BIN_ID,ITM_ID,LOT_ID,BIN_ID,ITM_UOM_BS,TOT_STK,REJ_STK,RWK_STK,USR_ID_MOD_DT) SELECT '0000','1',a.org_id,a.wh_id,a.id,a.code,f.lot_no,d.bin_id,m.uom_id,a.total_stk,a.rejected_stk,a.reworkable_stk,DATE_FORMAT(a.created_date,'%Y-%m-%d') FROM ".$posDB.".sma_item_stk_lot_bin a INNER JOIN ".$posDB.".sma_bin d ON (d.id = a.bin_id) INNER JOIN ".$posDB.".sma_uom_conv_std m ON (m.uom_desc = a.itm_uom_id) INNER JOIN ".$posDB.".sma_item_stk_lot f ON (f.id = a.lot_id) WHERE a.create_flg = '1'";
           $this->db->query($w6);
        if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_item_stk_lot_bin a SET a.create_flg = '' WHERE a.create_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "Stock ITM LOT BIN Records Save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }


    public function updstockLotBinMaster(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$intrDB.".intrm".$n."mm".$n."stk".$n."summ".$n."bin".$n."pos a INNER JOIN ".$posDB.".sma_item_stk_lot_bin b ON ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id) AND (a.ITM_ID = b.code) AND (a.POS_BIN_ID = b.id)) SET a.TOT_STK = b.total_stk,a.UPD_FLG = '1',a.REJ_STK = b.rejected_stk,a.RWK_STK = b.reworkable_stk WHERE ((a.WH_ID = b.wh_id) AND (a.ORG_ID = b.org_id) AND (a.ITM_ID = b.code) AND (a.POS_BIN_ID = b.id) AND b.upd_flg = '1')";
        if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_item_stk_lot_bin a SET a.upd_flg = '' WHERE a.upd_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "Stock Bin Records Update successfully...";
                return true;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

////////////////////////////////////////////

    // I.T.-10 (Pos -> intermediate) Insert EMRS Stock Item to ERP

    public function InsertEMRSstockItmMaster(){
        $this->EMRSstockItmMasterUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        //$w= "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."estk".$n."summ".$n."itm(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_STK_ITM_ID,FY_ID,ITM_ID,ITM_UOM_BS,TOT_STK,AVL_STK,REJ_STK,RWK_STK,ORD_STK,MAP_PRICE,WAP_PRICE,USR_ID_MOD_DT,CREATE_FLG,UPD_FLG) SELECT  '0000','1',b.org_id,b.wh_id,a.id,a.fy_id,c.code,m.uom_id,a.total_stk,a.available_stk,a.rejected_stk,a.rework_stk,a.ordered_stk,c.price,c.price,DATE_FORMAT(a.created_date,'%Y-%m-%d'),'1','5' from ".$posDB.".sma_cons_item_stk_profile a INNER JOIN ".$posDB.".sma_warehouses b ON (b.id = a.warehouse_id) INNER JOIN ".$posDB.".sma_products c ON (c.id = a.item_id AND c.warehouse =a.warehouse_id) INNER JOIN ".$posDB.".sma_uom_conv_std m ON (m.uom_desc = a.item_uom_id) WHERE a.create_flg = '1'";
        $w= "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."estk".$n."summ".$n."itm(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_STK_ITM_ID,FY_ID,ITM_ID,ITM_UOM_BS,TOT_STK,AVL_STK,REJ_STK,RWK_STK,ORD_STK,USR_ID_MOD_DT,CREATE_FLG,UPD_FLG) SELECT  '0000','1',a.org_id,a.wh_id,a.id,a.fy_id,a.code,m.uom_id,a.total_stk,a.available_stk,a.rejected_stk,a.rework_stk,a.ordered_stk,DATE_FORMAT(a.created_date,'%Y-%m-%d'),'1','5' from ".$posDB.".sma_cons_item_stk_profile a INNER JOIN ".$posDB.".sma_uom_conv_std m ON (m.uom_desc = a.itm_uom_id) WHERE a.create_flg = '1'";
        if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_cons_item_stk_profile a SET a.create_flg = '' WHERE a.create_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "EMRS Stock ITM Records Save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }

    public function EMRSstockItmMasterUPD()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$intrDB.".intrm".$n."mm".$n."estk".$n."summ".$n."itm a INNER JOIN ".$posDB.".sma_cons_item_stk_profile b ON (a.POS_STK_ITM_ID = b.id) SET a.TOT_STK = b.total_stk, a.AVL_STK = b.available_stk, a.REJ_STK = b.rejected_stk, a.RWK_STK = b.rework_stk, a.UPD_FLG='1' WHERE b.upd_flg='1'";
        if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_cons_item_stk_profile a SET a.upd_flg = '' WHERE a.upd_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "Stock ITM Records Save successfully...";
                return true;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }

    }

////////////////////////////////////////////

    // I.T.-11 (Pos -> intermediate) Insert Stock Item LOT to ERP

    public function InsertEMRSstockItmLotMaster(){
        $this->EMRSstockItmLotUPD();
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$'; 
        $w = "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."estk".$n."summ".$n."lot(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_STK_LOT_ID,ITM_ID,LOT_ID,ITM_UOM_BS,TOT_STK,REJ_STK,EXPIRY_DT,RWK_STK,USR_ID_MOD_DT,LOT_DT,CREATE_FLG,UPD_FLG) SELECT '0000','1',a.org_id,a.wh_id,a.id,a.code,a.lot_no,m.uom_id,a.total_stk,a.rejected_stk,DATE_FORMAT(a.exp_date,'%Y-%m-%d'),a.reworkable_stk,DATE_FORMAT(a.created_date,'%Y-%m-%d'),DATE_FORMAT(a.created_date,'%Y-%m-%d'),'1','5' from ".$posDB.".sma_cons_item_stk_lot a INNER JOIN ".$posDB.".sma_uom_conv_std m ON (m.uom_desc = a.item_uom_id) WHERE a.create_flg = '1'";
        if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_cons_item_stk_lot a SET a.create_flg = '' WHERE a.create_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "EMRS Stock ITM LOT Records Save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }

    public function EMRSstockItmLotUPD(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$intrDB.".intrm".$n."mm".$n."estk".$n."summ".$n."lot a INNER JOIN ".$posDB.".sma_cons_item_stk_lot b ON (a.POS_STK_LOT_ID = b.id) SET a.TOT_STK = b.total_stk, a.REJ_STK = b.rejected_stk, a.RWK_STK = b.reworkable_stk, a.UPD_FLG='1' WHERE b.upd_flg='1'";
        if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_cons_item_stk_lot a SET a.upd_flg = '' WHERE a.upd_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "EMRS Stock ITM LOT Records Save successfully...";
                return true;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }

    }

 ////////////////////////////////////////////

    // I.T.-12 (Pos -> intermediate) Insert Stock Item LOT BIN to ERP

    public function InsertEMRSstockItmLotBINMaster(){
           $this->updEMRSstockLotBinMaster();
           $posDB = $this->posDbName();
           $intrDB = $this->intrDbName();
           $n = '$';
           $w = "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."estk".$n."summ".$n."bin(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_BIN_ID,ITM_ID,LOT_ID,BIN_ID,ITM_UOM_BS,TOT_STK,REJ_STK,RWK_STK,USR_ID_MOD_DT,CREATE_FLG,UPD_FLG) SELECT '0000','1',a.org_id,a.wh_id,a.id,a.code,a.lot_no,d.bin_id,m.uom_id,a.total_stk,a.rejected_stk,a.reworkable_stk,DATE_FORMAT(a.created_date,'%Y-%m-%d'),'1','5' FROM ".$posDB.".sma_cons_item_stk_lot_bin a INNER JOIN ".$posDB.".sma_bin d ON(d.id = a.bin_id) INNER JOIN ".$posDB.".sma_uom_conv_std m ON (m.uom_desc = a.item_uom_id) WHERE a.create_flg = '1'";
        if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_cons_item_stk_lot_bin a SET a.create_flg = '' WHERE a.create_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "EMRSStock ITM LOT BIN Records Save successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }


    public function updEMRSstockLotBinMaster(){
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "UPDATE ".$intrDB.".intrm".$n."mm".$n."estk".$n."summ".$n."bin a INNER JOIN ".$posDB.".sma_cons_item_stk_lot_bin b ON (a.POS_BIN_ID = b.id) SET a.TOT_STK = b.total_stk,a.UPD_FLG = '1',a.REJ_STK = b.rejected_stk,a.RWK_STK = b.reworkable_stk WHERE (a.POS_BIN_ID = b.id) AND b.upd_flg = '1'";
        if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_cons_item_stk_lot_bin a SET a.upd_flg = '' WHERE a.upd_flg = '1'";
             if($this->db->query($w1))
             {
                $msg = "EMRS Stock Bin Records Update successfully...";
                return true;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
   }
    }

    ////////////////////////////////////////////

    // I.T.-13 (Pos -> intermediate) MRN RCPT to ERP

    public function InsertMaterialRcptMaster(){
           $posDB = $this->posDbName();
           $intrDB = $this->intrDbName();
           $n = '$';
           $w = "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."mtl".$n."rcpt(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_MTL_RCPT_ID,FY_ID,RCPT_NO,RCPT_DT,RCPT_SRC_TYPE,GE_DOC_ID,EO_ID_SRC,DN_NO_SRC,DN_DT_SRC,EO_ID_TPT,TPT_BILL_NO,TPT_BILL_DT,VEHICLE_NO,RMDA_STAT,ADDL_AMT,CURR_ID_SP,PRJ_ID,SUPP_INVC_RCVD_TPT_DT,SYNC_FLG,UPD_FLG) SELECT '0000','1',a.org_id,a.warehouse_id,a.id,1,a.rcpt_no,DATE_FORMAT(a.rcpt_dt,'%Y-%m-%d'),a.rcpt_src_type,a.ge_doc_id,a.supplier_id,a.dn_no, DATE_FORMAT(a.dn_dt,'%Y-%m-%d'),a.tp_id,a.tpt_lr_no,DATE_FORMAT(a.tpt_lr_dt,'%Y-%m-%d'), a.vehicle_no, a.rmda_stat, a.addl_amt,73,b.prj_id,DATE_FORMAT(a.created_date,'%Y-%m-%d'),'1','5' FROM ".$posDB.".sma_mrn_receipt a INNER JOIN ".$posDB.".sma_warehouses b ON(b.id = a.wh_id) WHERE a.status = 1 AND (a.sync_flg IS NULL OR a.sync_flg = '')";

          if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_mrn_receipt a INNER JOIN ".$intrDB.".intrm".$n."mm".$n."mtl".$n."rcpt b SET a.sync_flg = '1' WHERE b.POS_MTL_RCPT_ID = a.id";
             if($this->db->query($w1))
             {
                $msg = "MRN RECIPT Records Update successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }

    ////////////////////////////////////////////

    // I.T.-14 (Pos -> intermediate) MRN RCPT SRC to ERP

    public function InsertMaterialRcptSRCMaster(){
           $posDB = $this->posDbName();
           $intrDB = $this->intrDbName();
           $n = '$';
           $w = "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."mtl".$n."rcpt".$n."src(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_MTL_RCPT_SRC_ID,RCPT_ID,DOC_ID_SRC,DOC_DT_SRC,SYNC_FLG,UPD_FLG) SELECT '0000','1',a.org_id,a.warehouse_id,a.id,f.id,c.doc_id,DATE_FORMAT(c.po_dt,'%Y-%m-%d'),'1','5' FROM  ".$posDB.".sma_mrn_rcpt_src a INNER JOIN ".$posDB.".sma_drft_po c ON (c.id = a.po_id) INNER JOIN ".$posDB.".sma_mrn_receipt f ON (f.id = a.mrn_rcpt_id) WHERE f.status = 1 AND (a.sync_flg IS NULL OR a.sync_flg = '')";
         if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_mrn_rcpt_src a INNER JOIN ".$intrDB.".intrm".$n."mm".$n."mtl".$n."rcpt".$n."src b SET a.sync_flg = '1' WHERE b.POS_MTL_RCPT_SRC_ID = a.id";
             if($this->db->query($w1))
             {
                $msg = "MRN RECIPT SRC Records Update successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }

    ////////////////////////////////////////////

    // I.T.-15 (Pos -> intermediate) MRN Stock Item to ERP

    public function InsertMaterialRcptItmMaster(){
           $posDB = $this->posDbName();
           $intrDB = $this->intrDbName();
           $n = '$';
           $w = "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."mtl".$n."rcpt".$n."itm(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_MTL_RCPT_ITM_ID,SRC_RCPT_ID,DOC_ID_SRC,DOC_DT_SRC,DLV_SCHDL_NO,ITM_ID,ITM_UOM,PEND_QTY,DLV_NOTE_QTY,TOT_RCPT_QTY,RWK_QTY,REJ_QTY,RCPT_QTY,RCPT_QTY_BS,PUR_PRICE,PUR_PRICE_BS,SYNC_FLG,UPD_FLG) SELECT '0000','1',a.org_id,a.warehouse_id,a.id,a.mrn_rcpt_src_id,c.doc_id,DATE_FORMAT(c.po_dt,'%Y-%m-%d'),a.dlv_schdl_no,a.code,m.uom_id,(a.dlv_note_qty - (a.rcpt_qty + a.rej_qty)) AS pqtys,a.dlv_note_qty,a.tot_rcpt_qty,a.rwk_qty,a.rej_qty,a.rcpt_qty,a.rcpt_qty,p.price,p.price,'1','5' FROM  ".$posDB.".sma_mrn_rcpt_item a INNER JOIN ".$posDB.".sma_drft_po c ON (c.id = a.po_id) INNER JOIN ".$posDB.".sma_mrn_rcpt_src v ON (v.id = a.mrn_rcpt_src_id) INNER JOIN ".$posDB.".sma_mrn_receipt k ON (k.id = v.mrn_rcpt_id) LEFT JOIN ".$posDB.".sma_uom_conv_std m ON (m.uom_desc = a.item_uom_id) INNER JOIN ".$posDB.".sma_products p ON (p.id = a.item_id) WHERE k.status = 1 AND (a.sync_flg IS NULL OR a.sync_flg = '')";
         if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_mrn_rcpt_item a INNER JOIN ".$intrDB.".intrm".$n."mm".$n."mtl".$n."rcpt".$n."itm b SET a.sync_flg = '1' WHERE b.POS_MTL_RCPT_ITM_ID = a.id";
             if($this->db->query($w1))
             {
                $msg = "MRN RECIPT ITM Records Update successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }

 ////////////////////////////////////////////

    // I.T.-16 (Pos -> intermediate) MRN Stock Item to ERP

    public function InsertMaterialRcptItmLOTMaster(){
           $posDB = $this->posDbName();
           $intrDB = $this->intrDbName();
           $n = '$';
           $w = "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."mtl".$n."rcpt".$n."lot(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_MTL_RCPT_LOT_ID,ITM_SRC_ID,DOC_ID_SRC,DOC_DT_SRC,DLV_SCHDL_NO,LOT_ID,ITM_ID,ITM_UOM,LOT_QTY,LOT_PRICE,EXPIRY_DT,MFG_DT,BATCH_NO,SYNC_FLG,UPD_FLG) SELECT '0000','1',a.org_id,a.warehouse_id,a.id,
           a.mrn_rcpt_item_id,c.doc_id,DATE_FORMAT(c.po_dt,'%Y-%m-%d'),a.dlv_schdl_no,a.lot_no,a.code,m.uom_id,a.lot_qty,d.price,DATE_FORMAT(a.expiry_dt,'%Y-%m-%d'),DATE_FORMAT(a.mfg_dt,'%Y-%m-%d'),a.batch_no,'1','5' FROM  ".$posDB.".sma_mrn_rcpt_item_lot a INNER JOIN ".$posDB.".sma_mrn_rcpt_item z ON (z.id = a.mrn_rcpt_item_id) INNER JOIN ".$posDB.".sma_mrn_rcpt_src v ON (v.id = z.mrn_rcpt_src_id) INNER JOIN ".$posDB.".sma_mrn_receipt k ON (k.id = v.mrn_rcpt_id) INNER JOIN ".$posDB.".sma_drft_po c ON (c.id = z.po_id) INNER JOIN ".$posDB.".sma_products d ON (d.id = a.item_id) LEFT JOIN ".$posDB.".sma_uom_conv_std m ON (m.uom_desc = a.itm_uom_id) WHERE k.status = 1 AND (a.sync_flg IS NULL OR a.sync_flg = '')";
         if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_mrn_rcpt_item_lot a INNER JOIN ".$intrDB.".intrm".$n."mm".$n."mtl".$n."rcpt".$n."lot b SET a.sync_flg = '1' WHERE b.POS_MTL_RCPT_LOT_ID = a.id";
             if($this->db->query($w1))
             {
                $msg = "MRN RECIPT LOT Records Update successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }   

 ////////////////////////////////////////////

    // I.T.-17 (Pos -> intermediate) MRN Stock Item to ERP

    public function InsertMaterialRcptItmBinMaster(){
           $posDB = $this->posDbName();
           $intrDB = $this->intrDbName();
           $n = '$';
           $w = "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."mtl".$n."rcpt".$n."bin(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_MTL_RCPT_BIN_ID,LOT_SRC_ID,DOC_ID_SRC,DOC_DT_SRC,LOT_ID,BIN_ID,ITM_ID,ITM_UOM,BIN_QTY,BIN_QTY_BS,SYNC_FLG,UPD_FLG) SELECT '0000','1',a.org_id,a.warehouse_id,a.id,a.lot_id,c.doc_id,DATE_FORMAT(c.po_dt,'%Y-%m-%d'),n.lot_no,m.bin_id,a.code,h.uom_id,a.bin_qty,a.bin_qty,'1','5' FROM  ".$posDB.".sma_mrn_rcpt_lot_bin a INNER JOIN ".$posDB.".sma_bin m ON (m.id = a.bin_id) INNER JOIN ".$posDB.".sma_mrn_rcpt_item_lot n ON (n.id = a.lot_id) INNER JOIN ".$posDB.".sma_mrn_rcpt_item z ON (z.id = n.mrn_rcpt_item_id)  INNER JOIN ".$posDB.".sma_mrn_rcpt_src v ON (v.id = z.mrn_rcpt_src_id) INNER JOIN ".$posDB.".sma_mrn_receipt k ON (k.id = v.mrn_rcpt_id)  INNER JOIN ".$posDB.".sma_drft_po c ON (c.id = z.po_id) LEFT JOIN ".$posDB.".sma_uom_conv_std h ON (h.uom_desc = a.itm_uom_id) WHERE k.status = 1 AND (a.sync_flg IS NULL OR a.sync_flg = '')";
           if($this->db->query($w)){
             $w1= "UPDATE ".$posDB.".sma_mrn_rcpt_lot_bin a INNER JOIN ".$intrDB.".intrm".$n."mm".$n."mtl".$n."rcpt".$n."bin b SET a.sync_flg = '1' WHERE b.POS_MTL_RCPT_BIN_ID = a.id";
             if($this->db->query($w1))
             {
                $msg = "MRN RECIPT BIN Records Update successfully...";
                return $msg;
             }
             else{
                $msg = $this->db->error();
                return $msg['message'];
                 }
            
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }

      

    public function CheckSyncFLG()
    {

        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "SELECT COUNT(SYNC_FLG) AS cnt FROM ".$intrDB.".intrm".$n."sync".$n."info WHERE SYNC_FLG = '0' ";
        $re = $this->db->query($w);
        $aa = $re->result();
        $result = $aa[0]->cnt;
        if($result == 3)
             {
                return $result;
             }
             else{
                   return 0;
                 }
    }

    public function UpdSyncFLG()
    {

        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w = "UPDATE ".$intrDB.".intrm".$n."sync".$n."info SET SYNC_FLG = '1' WHERE SYNC_FLG = '0' ";
        if($this->db->query($w))
             {
                return TRUE;
             }
             else{
                   $msg = $this->db->error();
                   return $msg['message'];
                 }
    }

    public function Delete_duplicate_data()
    {

        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        // ----------------------- Sale Data Delete Replicate Entry ------------------------
        $w1 = "UPDATE ".$posDB.".sma_sales a INNER JOIN ".$posDB.".sma_warehouses b ON (b.id = a.warehouse_id) SET a.reference_no = CONCAT('INV-',b.segment_id,"-",a.sid) WHERE a.reference_no NOT LIKE '%INV%'";
        $w1 = "DELETE n1 FROM ".$posDB.".sma_sales n1,".$posDB.".sma_sales n2 WHERE n1.id > n2.id AND n1.reference_no = n2.reference_no AND n1.warehouse_id = n1.warehouse_id AND (n1.sync_flg IS NULL OR n1.sync_flg = '')";
        $w2  = "UPDATE ".$posDB.".sma_sales a INNER JOIN ".$posDB.".sma_companies b ON (b.name = a.customer) INNER JOIN ".$posDB.".sma_warehouses c ON (c.id = a.warehouse_id) SET a.customer_id = b.id,a.biller_id = b.phone WHERE b.segment_id = c.segment_id AND a.customer = b.name AND (a.sync_flg IS NULL OR a.sync_flg = '')";
        $w3 = "DELETE n1 FROM ".$posDB.".sma_payments n1,".$posDB.".sma_payments n2 WHERE n1.id > n2.id AND n1.sale_id = n2.sale_id AND n1.date = n1.date AND (n1.sync_flg IS NULL OR n1.sync_flg = '')";
       
    }

    // public function Update_price_sls()
    // {
    //    //echo "hi...";
    //     $dt = date('Y-m-d');
    //     $pdt = date('Y-m-d', strtotime(' -1 day'));

    //      //die;
    //     $posDB = $this->posDbName();
    //     $intrDB = $this->intrDbName();
    //     $n  = '$';
    //     $w  = "UPDATE ".$posDB.".sma_products a INNER JOIN ".$intrDB.".intrm".$n."sls".$n."price b ON ( a.org_id = b.ORG_ID AND a.code = b.ITM_ID ) INNER JOIN ".$posDB.".sma_warehouses w ON (b.ORG_ID = w.org_id AND b.PRJ_ID = w.prj_id) SET a.price = b.MRP_RT,a.pos_upd_flg =  '1' WHERE DATE_FORMAT( b.EFFECTIVE_DT,'%Y-%m-%d' ) =  '".$pdt."' AND (a.warehouse = w.id)";

    //    if ($this->db->query($w)) {
    //       $w1  = "UPDATE ".$posDB.".sma_products a INNER JOIN ".$intrDB.".intrm".$n."sls".$n."price b ON ( a.org_id = b.ORG_ID AND a.code = b.ITM_ID ) INNER JOIN ".$posDB.".sma_warehouses w ON (b.ORG_ID = w.org_id AND b.PRJ_ID = w.prj_id) SET a.price = b.MRP_RT,a.pos_upd_flg =  '1' WHERE DATE_FORMAT( b.EFFECTIVE_DT,'%Y-%m-%d' ) =  '".$dt."' AND (a.warehouse = w.id)";

    //      if ($this->db->query($w1)) {

    //         $w2 = "Update ".$posDB.".sma_purchase_items a INNER JOIN ".$intrDB.".intrm".$n."sls".$n."price b ON (a.org_id = b.ORG_ID AND a.product_code = b.ITM_ID) INNER JOIN ".$posDB.".sma_warehouses w ON (b.ORG_ID = w.org_id AND b.PRJ_ID = w.prj_id) SET a.net_unit_cost = b.BASE_PRICE,a.item_tax = b.TAX_AMT, a.tax = b.TAX_PRT ,a.subtotal = b.MRP_RT , a.unit_cost= b.MRP_RT,a.pos_upd_flg = '1' WHERE DATE_FORMAT(b.EFFECTIVE_DT, '%Y-%m-%d') = '".$pdt."' AND (a.warehouse_id = w.id) ";
        
    //     if ($this->db->query($w2)) {

    //          $w3 = "Update ".$posDB.".sma_purchase_items a INNER JOIN ".$intrDB.".intrm".$n."sls".$n."price b ON (a.org_id = b.ORG_ID AND a.product_code = b.ITM_ID) INNER JOIN ".$posDB.".sma_warehouses w ON (b.ORG_ID = w.org_id AND b.PRJ_ID = w.prj_id) SET a.net_unit_cost = b.BASE_PRICE,a.item_tax = b.TAX_AMT, a.tax = b.TAX_PRT ,a.subtotal = b.MRP_RT , a.unit_cost= b.MRP_RT,a.pos_upd_flg = '1' WHERE DATE_FORMAT(b.EFFECTIVE_DT, '%Y-%m-%d') = '".$dt."' AND (a.warehouse_id = w.id) ";
         
    //        if ($this->db->query($w3)) {

    //                       return TRUE;
    //               }
    //            }
    //          }
    //     }
    // }


    public function Update_price_sls()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n  = '$';

        //$asd = "UPDATE ".$posDB.".sma_purchase_items a INNER JOIN ".$posDB.".sma_warehouses b ON (a.warehouse_id = b.id) SET a.prj_id = b.prj_id WHERE a.warehouse_id = b.id AND (a.prj_id IS NULL OR a.prj_id = '')";
        //$this->db->query($asd);
        //$this->sync_price();

         $a1 = "SELECT * FROM ".$posDB.".sma_sls_price WHERE (sync_flg IS NULL OR sync_flg = '')";
		//$a1 = "SELECT * FROM ".$posDB.".sma_sls_price WHERE (sync_flg IS NULL OR sync_flg = '') AND prj_id='0000.01.03.0001.00XR.00.1UNW6AtBiP'";
        $r = $this->db->query($a1);

        
        $ar = $r->result();
		
        if(!empty($ar))
         {
        $cnt = count($ar);
		//$cnt = 100;
        for($i=0;$i<$cnt;$i++)
         {
        $p1 = "Update ".$posDB.".sma_purchase_items a SET a.net_unit_cost = ".$ar[$i]->base_price.",a.item_tax = ".$ar[$i]->tax_amt.", a.tax = ".$ar[$i]->tax_prt.",a.subtotal = ".$ar[$i]->mrp_rt." , a.unit_cost= ".$ar[$i]->mrp_rt.",a.price_flg = '1' WHERE a.product_code = '".$ar[$i]->itm_id."' AND a.org_id= '".$ar[$i]->org_id."' AND a.prj_id = '".$ar[$i]->prj_id."' ";
            $this->db->query($p1);

            $p2 = "Update ".$posDB.".sma_sls_price SET sync_flg = '1' WHERE sn = ".$ar[$i]->sn." ";
            $this->db->query($p2);
         }


       return true;
         } 

         return false;
         //return true;
 
     }



    public function InsertStkAdj(){
         
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."stk".$n."adjt(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_STK_ADJT_ID,DOC_ID,DOC_DT,FY_ID,USR_ID_CREATE,USR_ID_CREATE_DT,USR_ID_MOD,USR_ID_MOD_DT,REMARKS,PRJ_ID,STK_ADJT_TYPE,STK_ADJT_STAT,AUTH_STAT,SYNC_FLG,STK_ADJT_DT) SELECT '0000',1,a.org_id,a.wh_id,a.id,a.doc_id,DATE_FORMAT(a.doc_dt,'%Y-%m-%d'),1,1,CURDATE(),1,CURDATE(),a.note,a.prj_id,351,353,'Y','1',DATE_FORMAT(a.doc_dt,'%Y-%m-%d') FROM ".$posDB.".sma_stk_adjt a WHERE (a.sync_flg IS NULL OR a.sync_flg ='')";
       if($this->db->query($w)){
          $msg = "ADJT Records save successfully...";
          $w1= "UPDATE ".$posDB.".sma_stk_adjt a SET a.sync_flg ='1' WHERE (a.sync_flg IS NULL OR a.sync_flg ='')";

          $this->db->query($w1);
            return $msg;
         } else {
               $msg = $this->db->error();
               return $msg['message'];
         }
    } 

     public function InsertStkAdjItm(){
         
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."stk".$n."adjt".$n."itm(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_STK_ADJT_ITM_ID,DOC_ID,ITM_ID,ITM_UOM,ADJT_QTY,ADJT_TYPE,SYNC_FLG) SELECT '0000',1,a.org_id,a.wh_id,a.id,a.doc_id,a.itm_id,m.uom_id,a.qty,CASE WHEN a.type = 'subtraction' THEN 'S' ELSE 'A' END,'1' FROM ".$posDB.".sma_stk_adjt_itm a INNER JOIN ".$posDB.".sma_uom_conv_std m ON (m.uom_desc = a.itm_uom) WHERE (a.sync_flg IS NULL OR a.sync_flg ='') ";
       if($this->db->query($w)){
          $msg = "ADJT ITM Records save successfully...";
          $w1= "UPDATE ".$posDB.".sma_stk_adjt_itm a SET a.sync_flg ='1' WHERE (a.sync_flg IS NULL OR a.sync_flg ='')";
          $this->db->query($w1);
            return $msg;
         } else {
               $msg = $this->db->error();
               return $msg['message'];
         }
    }

    public function InsertStkAdjLot(){
         
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."stk".$n."adjt".$n."lot(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_STK_ADJT_LOT_ID,DOC_ID,LOT_ID,ITM_ID,ITM_UOM,ADJT_QTY,ADJT_TYPE,SYNC_FLG) SELECT '0000',1,a.org_id,a.wh_id,a.id,a.doc_id,a.lot_no,a.itm_id,m.uom_id,a.qty,CASE WHEN a.type = 'subtraction' THEN 'S' ELSE 'A' END,'1' FROM ".$posDB.".sma_stk_adjt_lot a INNER JOIN ".$posDB.".sma_uom_conv_std m ON (m.uom_desc = a.itm_uom) WHERE (a.sync_flg IS NULL OR a.sync_flg ='') ";

       if($this->db->query($w)){
          $msg = "ADJT ITM LOT Records save successfully...";
          $w1= "UPDATE ".$posDB.".sma_stk_adjt_lot a SET a.sync_flg ='1' WHERE (a.sync_flg IS NULL OR a.sync_flg ='')";
          $this->db->query($w1);
            return $msg;
         } else {
               $msg = $this->db->error();
               return $msg['message'];
         }
    } 

    public function InsertStkAdjBin(){
         
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n = '$';
        $w= "INSERT INTO ".$intrDB.".intrm".$n."mm".$n."stk".$n."adjt".$n."bin(CLD_ID,SLOC_ID,ORG_ID,WH_ID,POS_STK_ADJT_BIN_ID,DOC_ID,LOT_ID,BIN_ID,ITM_ID,ITM_UOM,ADJT_QTY,ADJT_TYPE,SYNC_FLG) SELECT '0000',1,a.org_id,a.wh_id,a.id,a.doc_id,a.lot_no,a.bin_id,a.itm_id,m.uom_id,a.qty,CASE WHEN a.type = 'subtraction' THEN 'S' ELSE 'A' END,'1' FROM ".$posDB.".sma_stk_adjt_bin a INNER JOIN ".$posDB.".sma_uom_conv_std m ON (m.uom_desc = a.itm_uom) WHERE (a.sync_flg IS NULL OR a.sync_flg ='') ";

       if($this->db->query($w)){
          $msg = "ADJT ITM LOT Records save successfully...";
          $w1= "UPDATE ".$posDB.".sma_stk_adjt_bin a SET a.sync_flg ='1' WHERE (a.sync_flg IS NULL OR a.sync_flg ='')";
          $this->db->query($w1);
            return $msg;
         } else {
               $msg = $this->db->error();
               return $msg['message'];
         }
    }

    public function sync_price()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n  = '$';
        $w  = "INSERT INTO ".$posDB.".sma_sls_price(org_id,itm_id,date,base_price,add_tot_mrp_rate,tax_prt,tax_amt,mrp_rt,prj_id,sn) SELECT a.ORG_ID,a.ITM_ID,DATE_FORMAT(a.EFFECTIVE_DT,'%Y-%m-%d'),a.BASE_PRICE,a.ADD_TOT_MRP_RATE,a.TAX_PRT,a.TAX_AMT,a.MRP_RT,a.PRJ_ID,a.SN FROM ".$intrDB.".intrm".$n."sls".$n."price a WHERE (a.POS_SLS_PRICE_ID IS NULL OR a.POS_SLS_PRICE_ID = '') ORDER BY a.EFFECTIVE_DT ASC";
        if ($this->db->query($w)) {
            $w1 = "UPDATE ".$intrDB.".intrm".$n."sls".$n."price a INNER JOIN ".$posDB.".sma_sls_price b ON (a.SN = b.sn) SET a.POS_SLS_PRICE_ID = b.id,a.CREATE_FLG = '1' WHERE (a.SN = b.sn)";
            $this->db->query($w1);
            $msg = "SLS Price save successfully...";
            return $msg;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }

    public function trford()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n  = '$';
        $w  = "INSERT INTO ".$posDB.".sma_trf_ord(org_id,wh_id,doc_id,doc_dt,fy_id,trf_no,trf_dt,trf_src_type,trf_type,org_id_src,wh_id_src,ord_id_dest,wh_id_dest,trf_stat,remark,prj_id_src,prj_id_desc,drft_trf_no) SELECT a.ORG_ID,a.WH_ID,a.DOC_ID,DATE_FORMAT(a.DOC_DT,'%Y-%m-%d'),a.FY_ID,a.TRF_NO,DATE_FORMAT(a.TRF_DT,'%Y-%m-%d'),a.TRF_SRC_TYPE,a.TRF_TYPE,a.ORG_ID_SRC,a.WH_ID_SRC,a.ORG_ID_DEST,a.WH_ID_DEST,a.TRF_STAT,a.REMARKS,a.PRJ_ID,a.PRJ_ID_DEST,a.DRFT_TRF_NO FROM ".$intrDB.".intrm".$n."mm".$n."trf".$n."ord a WHERE (a.POS_TRF_ORD_ID IS NULL OR a.POS_TRF_ORD_ID ='')";

          if ($this->db->query($w)) {
            $w1 = "UPDATE ".$intrDB.".intrm".$n."mm".$n."trf".$n."ord a INNER JOIN ".$posDB.".sma_trf_ord b ON (a.DOC_ID = b.doc_id) SET a.POS_TRF_ORD_ID = b.id,a.SYNC_FLG = '1' WHERE (a.DOC_ID = b.doc_id) AND (a.POS_TRF_ORD_ID IS NULL OR a.POS_TRF_ORD_ID= '')";
            $this->db->query($w1);
            $msg = "Transfer Order save successfully...";
            return $msg;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }

    public function trforditm()
    {
        $posDB = $this->posDbName();
        $intrDB = $this->intrDbName();
        $n  = '$';
        $w  = "INSERT INTO ".$posDB.".sma_trf_ord_itm(org_id,doc_id,itm_id,itm_uom,ord_qty,auth_qty,stk_resv_qty,trf_qty,pend_qty,tot_trf_qty,tot_trf_qty_bs,itm_remarks,itm_price) SELECT a.ORG_ID,a.DOC_ID,a.ITM_ID,m.uom_desc,a.ORD_QTY,a.AUTH_QTY,a.STK_RESV_QTY,a.TRF_QTY,a.PEND_QTY,a.TOT_TRF_QTY,a.TOT_TRF_QTY_BS,a.ITM_REMARKS,a.ITM_PRICE FROM ".$intrDB.".intrm".$n."mm".$n."trf".$n."ord".$n."itm a INNER JOIN ".$posDB.".sma_uom_conv_std m ON (m.uom_id = a.ITM_UOM) WHERE (a.POS_TRF_ORD_ITM_ID IS NULL OR a.POS_TRF_ORD_ITM_ID ='')";

          if ($this->db->query($w)) {
            $w1 = "UPDATE ".$intrDB.".intrm".$n."mm".$n."trf".$n."ord".$n."itm a INNER JOIN ".$posDB.".sma_trf_ord_itm b ON (a.DOC_ID = b.doc_id AND a.ITM_ID = b.itm_id AND a.ORD_QTY = b.ord_qty) SET a.POS_TRF_ORD_ITM_ID = b.id,a.SYNC_FLG = '1' WHERE (a.DOC_ID = b.doc_id AND a.ITM_ID = b.itm_id AND a.ORD_QTY = b.ord_qty) AND (a.POS_TRF_ORD_ITM_ID IS NULL OR a.POS_TRF_ORD_ITM_ID= '')";
            $this->db->query($w1);
            $msg = "Transfer Order ITM save successfully...";
            return $msg;
        } else {
            $msg = $this->db->error();
            return $msg['message'];
        }
    }
    
    // Sync all intermediate table to POS data base

    /* --------------Please must be Follow Below Sequence of Function Otherwise data may be corrupt----------------------*/
    public function syncToPos()
    {
        $this->currencyMaster();
        $this->segmentMaster(); // Segment Master Should be run before Warehouse Master
        $this->gstinfoupd(); // For Gst Update it
        $this->warehouseMaster(); // Warehouse master should be run before userMaster
        //$this->whORGMaster(); 
        $this->userMaster();
        // -----------$this->taxMaster(); // Tax master should be run before categoryMaster
        $this->categoryMaster();
        $this->customerMaster();
        $this->binMaster();
        $this->uomClsMaster(); // UOM Cls Master should be run before UOM Cls Stdv Master
        $this->uomConStdvMaster();
        $this->dsAttMaster(); // DS ATT Master should be run before ATT RELN Master And ATT TYPE Master
        $this->dsAttRelnMaster(); // ATT RELN Master should be run before ATT TYPE Master
        $this->dsAttTypeMaster();
        $this->productMaster(); // Product master should be run before productWarehouseMaster
        $this->purchaseItemMaster();
        $this->poMaster(); // Po Master should be run befor PO Schedule Master
        $this->poSchdlMaster();


        if($this->CheckSyncFLG() == 3){
            $this->stockItmMaster(); // Item Stock Master should be run before Stock lot Master
            $this->stockLotMaster(); // Item Stock lot Master should be run before Stock lot bin Master
            $this->stockLotBinMaster();
          }

        $this->emrsMaster(); // EMRS Master should be run before EMRS DOC SRC Master
        $this->emrsDocSrcMaster(); // EMRS DOC SRC Master should be run before EMRC Item Master
        $this->emrsItemMaster(); 

        $this->sync_price(); 

       

        return true;
    }

    
    // Sync all pos data table to intermediate data base

    /* --------------Please must be follow Below Sequence of Function Otherwise data may be corrupt----------------------*/
    public function syncToIntermediate()
    {

        
       // $this->eoMaster();
        $this->slsInvMaster();
        $this->slsInvItmMaster();
        $this->slsInvLotMaster();
        $this->slsInvBinMaster();
        $this->PaymntMaster(); 
           //$this->ChequeDtl(); // For Check Details
                                //$this->updStockItmMaster();
        $this->poStatusUPD(); // postatus Master should be run before poSHDL Master
        $this->poSHDLUPD();
         /*$this->emrsItmUPD();*/
         $this->InsertstockItmMaster(); // Item STK should be run before STK LOT Master
         $this->InsertstockItmLotMaster(); // Item STK LOT should be run before STK LOT BIN Master
         $this->InsertstockItmLotBINMaster(); 
         $this->UpdSyncFLG();

        /*$this->InsertEMRSstockItmMaster(); // EMRS Stock master should be run before EMRS Stock LOT Master
        $this->InsertEMRSstockItmLotMaster(); // EMRS Stock LOT master should be run before EMRSLOT BIN Master
        $this->InsertEMRSstockItmLotBINMaster();*/

        $this->InsertMaterialRcptMaster(); //MRN RCPT Should be run before All MRN Fuction
        $this->InsertMaterialRcptSRCMaster();
        $this->InsertMaterialRcptItmMaster();
        $this->InsertMaterialRcptItmLOTMaster();
        $this->InsertMaterialRcptItmBinMaster();

        /* $this->InsertStkAdj();  
        $this->InsertStkAdjItm();  
        $this->InsertStkAdjLot();  
        $this->InsertStkAdjBin();  */

        //$this->updQty();
        return true;
    }

   public function emptyPOS()
   {
     $q= "CALL emptyPOSdb()"; // Call Procedure
     if($this->db->query($q))
     {
        return true;
     }
       return false;
   }

   public function intrmPOSIDempty()
    {
        $CI = &get_instance();
        $this->db2 = $CI->load->database('db2', TRUE);
        $q = "CALL emptyIntermdb()"; // Call procedure
        if($this->db2->query($q))
        {
            return TRUE;
        }
        return false;
    }


    public function syncMRN()
    {
        $this->poMaster(); // Po Master should be run befor PO Schedule Master
        $this->poSchdlMaster();
        $this->emrsMaster(); // EMRS Master should be run before EMRS DOC SRC Master
        $this->emrsDocSrcMaster(); // EMRS DOC SRC Master should be run before EMRC Item Master
        $this->emrsItemMaster();
        return true;
    }

    public function syncData(){
        $ip = 'http://'.getHostByName(getHostName());
        $ip = 'http://129.191.22.72';
        //$ip = 'http://localhost';
        $segmentList = $this->db->select('COUNT(*) as num')
            ->from('sync_table')
            ->get()->row();
        $total_segments = $segmentList->num;
        $this->load->model('site');
        $segmentList = $this->site->getAllSegments();
        $check_dir = $_SERVER["DOCUMENT_ROOT"]."/".PROJECT_NAME."/sync-folder/".date("Y-m-d");
        
        $list = count(glob($check_dir."*"));
        if($list>0){
            $serial = $list+1;
            $directory = $_SERVER["DOCUMENT_ROOT"]."/".PROJECT_NAME."/sync-folder/".date("Y-m-d")."_".$serial."/";
            $zip_directory = $_SERVER["DOCUMENT_ROOT"]."/".PROJECT_NAME."/sync-folder/".date("Y-m-d")."_".$serial;
            $r_directory = $ip."/".PROJECT_NAME."/sync-folder/".date("Y-m-d")."_".$serial."/";
        }else{
            $directory = $_SERVER["DOCUMENT_ROOT"]."/".PROJECT_NAME."/sync-folder/".date("Y-m-d")."_1"."/";
            $zip_directory = $_SERVER["DOCUMENT_ROOT"]."/".PROJECT_NAME."/sync-folder/".date("Y-m-d")."_1";
            $r_directory = $ip."/".PROJECT_NAME."/sync-folder/".date("Y-m-d")."_1"."/";
            
        }  
      //  echo $directory."zip".$zip_directory."r direct".$r_directory;exit;      

        foreach($segmentList as $row){
            $warehouseList = $this->site->getWarehouseList($row->id);
            $segment = $row->id;
            $org = $row->org_id;
            $whidgod = '\'' . implode( '\', \'',explode(",",$warehouseList->wh_id)). '\'';
            $projName = explode(" ",$row->proj_name)[0]."_".$row->id;
            $this->db->select('COUNT(*)')
            ->from('sync_table')
            ->get()->row();
             for ($x = 1; $x <= $total_segments; $x++) {
                 $this->db->select("*");
                 $this->db->from("sync_table");       
                 $tableName = $this->db->where("id",$x)->get()->row();               
                 $table = $tableName->table_name;
                 // fetch mysql table rows
                 $csv_filename = $table.".csv";
                 $filePath = $directory.$projName."/";
                  
                 switch ($table) {
                    case "sma_segment":                   
                        $whr=" WHERE id='".$segment."' AND ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1')";
                        break;
                    case "sma_warehouses":
                               $whr=" WHERE segment_id='".$segment."' AND ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1')";
                        break;
                    case "sma_users":
                                $whr=" WHERE segment_id='".$segment."' AND ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1') ";
                        break;
                    case "sma_products":
                        $whr=" WHERE wh_id IN (".$whidgod.") AND org_id='".$org."' AND ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1')";                       
                       
                       break;
                    case "sma_purchase_items":
                        $whr=" WHERE wh_id IN (".$whidgod.") AND org_id='".$org."' AND ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1')";
                       break;
                    case "sma_warehouses_products":
                        $whr=" WHERE wh_id IN (".$whidgod.") AND org_id='".$org."' AND ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1')";
                       break;
                    case "sma_item_stk_profile":
                        $whr=" WHERE segment_id='".$segment."' AND ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1') ";
                       break;
                    case "sma_item_stk_lot":
                       
                        $whr=" WHERE segment_id='".$segment."' AND ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1') ";
                       break;
                    case "sma_item_stk_lot_bin":
                        $whr=" WHERE segment_id='".$segment."' AND ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1') ";
                       break;
                    case "sma_tax_rates":
                        $whr=" WHERE wh_id IN (".$whidgod.") AND org_id='".$org."' AND (pos_create_flg IS NULL OR pos_create_flg = '')";
                      break;
                    case "sma_emrs":
                        $whr=" WHERE segment_id='".$segment."' AND (pos_create_flg IS NULL OR pos_create_flg = '') ";          
                       break;
                    case "sma_emrs_doc_src":
                        $whr=" WHERE segment_id='".$segment."' AND (pos_create_flg IS NULL OR pos_create_flg = '') ";
                    break;
                    case "sma_emrs_item":
                        $whr=" WHERE segment_id='".$segment."' AND (pos_create_flg IS NULL OR pos_create_flg = '') ";
                        break;
                    case "sma_bin":
                        $whr=" WHERE segment_id='".$segment."' AND ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1') ";
                        break;
                    case "sma_categories":
                        $whr=" WHERE ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1')";
                        break;
                        
                    case "sma_companies":
                        $whr=" WHERE ((group_name='Supplier' OR group_name='Transporter' OR eo_type='B') AND ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1'))";
                        break;
                    case "sma_currencies":
                        $whr=" WHERE ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1')";
                        break;
                    case "sma_drft_po_dlv_schdl":
                     $whr=" WHERE segment_id='".$segment."' AND ((pos_create_flg IS NULL OR pos_create_flg = '') OR pos_upd_flg = '1')";
                        break;
                    case "sma_drft_po":
                        $whr=" LEFT OUTER JOIN sma_drft_po_dlv_schdl ON sma_drft_po_dlv_schdl.po_id=sma_drft_po.id WHERE segment_id='".$segment."' AND (sma_drft_po_dlv_schdl.pos_create_flg IS NULL OR sma_drft_po_dlv_schdl.pos_create_flg = '') ";
                        break;
                     case "sma_cheque_details":
                        $whr=" WHERE (upd_flg = '1') ";
                        break;              
                    default:
                        $whr='';
                }
                $host = $this->db->hostname;
                $username = $this->db->username;
                $password = $this->db->password;
                $dbname = $this->db->database;
                $posDB = $this->posDbName();
                $connection = mysqli_connect($host, $username, $password, $dbname) or die("Connection Error " . mysqli_error($connection));
                if($table=='sma_drft_po'){
                    $sql = 'select sma_drft_po.* from '.$table. $whr;
                }else{
                    $sql = 'select * from '.$table. $whr;
                }
                $result = mysqli_query($connection, $sql) or die("Selection Error " . mysqli_error($connection));
                if (!is_dir($directory)) { //create the folder if it's not already exists
                      mkdir($directory, 0755, TRUE);  
                 }
                $locDir = $directory."/".$projName;
                if (!is_dir($locDir)) { 
                    
                    mkdir($locDir, 0755, TRUE); 
                } 
                $fp = fopen($filePath. $csv_filename, 'w');
                while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
                {   
                    fputcsv($fp, $row);
                    // if($table!='sma_categories' && $table!='sma_companies' && $table!='sma_currencies' && $table!='sma_drft_po'){
                    // $q1 = "UPDATE ".$table." SET pos_create_flg = '1',pos_upd_flg = NULL WHERE id = ".$row[id]." ";
                    // $r1 = mysqli_query($connection, $q1) or die("Selection Error " . mysqli_error($connection));
                    // }
                }
                fclose($fp); 
             }   
            $source_dir = $directory.$projName.'/';
            $zip_file = $directory.$projName.'.zip';
            $zip = new ZipArchive();
            if ($zip->open($zip_file, ZIPARCHIVE::CREATE) === true) {
                foreach (glob($source_dir."*") as $file) {
                    $files = explode("/",$file)[count(explode("/",$file))-1];
                    $zip->addFile($file,$files);                
                }
              $zip->close();
              $this->site->rrmdir($directory.$projName);
              rmdir($directory.$projName);

              $parent_source_dir = $zip_directory;
              $parent_zip_file = $zip_directory.'.zip';
              $r_file_path = $r_directory.$projName.'.zip';
              $csv_data_insertion = array(
                'file_name' => $r_file_path, 
                'segment_id' => $segment,                   
                'created_date'=>date('Y-m-d H:i:s'),
                'flag' => 0,
            );

                $this->db->insert('csv_history',$csv_data_insertion);  
            }
             

           /* $pzip = new ZipArchive();
            if ($pzip->open($parent_zip_file, ZIPARCHIVE::CREATE) === true) {
                foreach (glob($parent_source_dir."/*") as $file1) {
                    $filess = explode("/",$file1)[count(explode("/",$file1))-1];
                    $pzip->addFile($file1,$filess);                
                }
               
              $pzip->close();
            //  $this->site->rrmdir($directory.$projName);
              //$this->site->rrmdir($directory.$projName);
              rmdir($directory.$projName);
            }*/
            
          }
         $host = $this->db->hostname;
         $username = $this->db->username;
         $password = $this->db->password;
         $dbname = $this->db->database;
         $posDB = $this->posDbName();
         $connection = mysqli_connect($host, $username, $password, $dbname) or die("Connection Error " . mysqli_error($connection));
                /* disable autocommit */
                  mysqli_autocommit($connection, FALSE);    
         $this->db->select('COUNT(*)')
            ->from('sync_table')
            ->get()->row();
            for ($x = 1; $x <= $total_segments; $x++) {
                $this->db->select("*");
                $this->db->from("sync_table");       
                $tableName = $this->db->where("id",$x)->get()->row();               
                $table = $tableName->table_name;
                if($table!='sma_categories' && $table!='sma_companies' && $table!='sma_currencies' && $table!='sma_drft_po'){
                    $q1 = "UPDATE ".$table." SET pos_create_flg = '1',pos_upd_flg = null WHERE 1 ";
                    if(mysqli_query($connection, $q1))
                     {
                         /* commit insert */
                         mysqli_commit($connection);
                      }
                      else{
                             /* Rollback */
                              mysqli_rollback($connection);

                      }   
                    } 
            }
            $this->session->set_flashdata('message', "Csv created Successfully");
            redirect('welcome');
           //echo "zip created successfully";exit;  
     } 

// ADD BY ANKIT 
public function syncoutData(){

    $ip = 'http://192.168.1.231';
    $ip = 'http://129.191.22.72';

    
    $remote_host = REMOTE_HOST;
    $remote_username = REMOTE_USERNAME;
    $remote_password = REMOTE_PWD;
    $remote_dbname = REMOTE_DB;

    /*$remote_host = 'localhost';
    $remote_username = 'root';
    $remote_password = '';
    $remote_dbname = 'iffco_final_live';*/

    $remote_connection = mysqli_connect($remote_host, $remote_username, $remote_password, $remote_dbname) or die("Connection Error " . mysqli_error($remote_connection));
    
  
    $segmentList = $this->db->select('COUNT(*) as num')
        ->from('syncout_table')
        ->get()->row();
    $total_segments = $segmentList->num;     
    $this->db->select("*");
    $this->db->from("segment");
    $segmentList = $this->db->get()->row();
    $directory = $_SERVER["DOCUMENT_ROOT"]."/".PROJECT_NAME."/syncout-folder/".date("Y-m-d")."_".$_SESSION['segment_id']."/";
    $segment = $segmentList->id;
    $org_id = $segmentList->org_id;
    $warehouseList = $this->site->getWarehouseList($row->id);
            
    $whidgod = '\'' . implode( '\', \'',explode(",",$warehouseList->wh_id)). '\'';
    $projName = explode(" ",$segmentList->proj_name)[0]."_".$segment;
        $this->db->select('COUNT(*)')
        ->from('syncout_table')
        ->get()->row();
    $check_dir = $_SERVER["DOCUMENT_ROOT"]."/".PROJECT_NAME."/syncout-folder/".date("Y-m-d")."_".$_SESSION['segment_id'];
        
    $list = count(glob($check_dir."*"));
    if($list>0){
        $serial = $list+1;
        $directory = $_SERVER["DOCUMENT_ROOT"]."/".PROJECT_NAME."/syncout-folder/".date("Y-m-d")."_".$_SESSION['segment_id']."_".$serial."/";
        $zip_directory = $_SERVER["DOCUMENT_ROOT"]."/".PROJECT_NAME."/syncout-folder/".date("Y-m-d")."_".$_SESSION['segment_id']."_".$serial;
       // $r_directory = $ip."/".PROJECT_NAME."/syncout-folder/".date("Y-m-d")."_".$_SESSION['segment_id']."_".$serial."/";
        $r_directory = "uploads/".date("Y-m-d")."_".$_SESSION['segment_id']."_".$serial."/";
    }else{
        $directory = $_SERVER["DOCUMENT_ROOT"]."/".PROJECT_NAME."/syncout-folder/".date("Y-m-d")."_".$_SESSION['segment_id']."_1"."/";
        $zip_directory = $_SERVER["DOCUMENT_ROOT"]."/".PROJECT_NAME."/syncout-folder/".date("Y-m-d")."_".$_SESSION['segment_id']."_1";
        //$r_directory = $ip."/".PROJECT_NAME."/syncout-folder/".date("Y-m-d")."_".$_SESSION['segment_id']."_1"."/";
        $r_directory = "uploads/".date("Y-m-d")."_".$_SESSION['segment_id']."_1"."/";
    }     
  //  echo $directory."<br>zip directory".$zip_directory."<br>r directory".$r_directory;exit;
        for ($x = 1; $x <= $total_segments; $x++) {
            $this->db->select("*");
            $this->db->from("syncout_table");       
            $tableName = $this->db->where("id",$x)->get()->row();               
            $table = $tableName->table_name;
            // fetch mysql table rows
            $csv_filename = $table.".csv";
           // $filePath = $_SERVER["DOCUMENT_ROOT"]."/".PROJECT_NAME."/syncout-folder/".date("Y-m-d")."/".$projName."/";
           // $filePath = $directory.$projName."/";
            $filePath = $directory;
         //   echo $filePath;exit;
            switch ($table) {
                case "sma_sales":                   
                    $whr=" WHERE ((sync_flg IS NULL OR sync_flg = '') AND sale_status='completed')";
                    break;
                case "sma_sale_items":
                    $whr=" INNER JOIN sma_sales ON (sma_sales.id = sma_sale_items.sale_id) WHERE ((sma_sale_items.sync_flg IS NULL OR sma_sale_items.sync_flg = '') AND sma_sales.sale_status='completed')";
                    break;
                case "sma_return_sales":
                    $whr=" WHERE (sync_flg IS NULL OR sync_flg = '')";
                    break;
                case "sma_return_items":
                    $whr=" INNER JOIN sma_return_sales ON (sma_return_sales.return_reference_no = sma_return_items.ret_ref_no)WHERE (sma_return_items.sync_flg IS NULL OR sma_return_items.sync_flg = '')";
                break;
                case "sma_payments":
                    $whr=" INNER JOIN sma_sales ON (sma_sales.id = sma_payments.sale_id) WHERE ((sma_payments.sync_flg IS NULL OR sma_payments.sync_flg = '') AND sma_sales.sale_status='completed')";
                break;
                case "sma_item_stk_lot":

                    $whr=" WHERE (create_flg ='1' OR upd_flg ='1')";

                     //$whr = " INNER JOIN sma_item_stk_profile ON sma_item_stk_profile.id=sma_item_stk_lot.item_stk_id WHERE sma_item_stk_lot.segment_id='".$segment."' AND (sma_item_stk_profile.create_flg = '1' OR sma_item_stk_profile.upd_flg = '1')";
                   
                break;
                case "sma_item_stk_lot_bin":
                    $whr=" WHERE (create_flg ='1' OR upd_flg ='1')";
                   // $whr = " INNER JOIN sma_item_stk_lot ON sma_item_stk_lot.id=sma_item_stk_lot_bin.lot_id INNER JOIN sma_item_stk_profile ON sma_item_stk_profile.id=sma_item_stk_lot.item_stk_id WHERE sma_item_stk_lot_bin.segment_id='".$segment."' AND (sma_item_stk_profile.create_flg = '1' OR sma_item_stk_profile.upd_flg = '1')";    

                       break;
                case "sma_item_stk_profile":
                    $whr=" WHERE (create_flg ='1' OR upd_flg ='1')";
                  
                break;
                case "sma_drft_po":
                    $whr=" INNER JOIN sma_drft_po_dlv_schdl ON sma_drft_po_dlv_schdl.po_id=sma_drft_po.id WHERE (sma_drft_po_dlv_schdl.po_id=sma_drft_po.id AND sma_drft_po.po_status=218 AND (sma_drft_po_dlv_schdl.upd_flg IS NULL OR sma_drft_po_dlv_schdl.upd_flg='')) ";
                break;
                case "sma_drft_po_dlv_schdl":

                    $whr=" WHERE (status=1 AND (upd_flg IS NULL OR upd_flg=''))";
                break;
                case "sma_cons_item_stk_lot":
                    $whr=" WHERE (create_flg ='1' OR upd_flg ='1')";                   
                break;
                case "sma_cons_item_stk_lot_bin":
                    $whr=" WHERE (create_flg ='1' OR upd_flg ='1')";
                break;
                case "sma_cons_item_stk_profile":
                    $whr=" WHERE (create_flg ='1' OR upd_flg ='1')";
                    break;
                case "sma_gp":
                    $whr=" WHERE (sync_flg IS NULL OR sync_flg = '') ";
                    break;
                case "sma_gp_item":
                    $whr=" WHERE (sync_flg IS NULL OR sync_flg = '')";
                    break;
                case "sma_gp_item_lot":
                    $whr=" WHERE (sync_flg IS NULL OR sync_flg = '')";
                    break;
                case "sma_gp_lot_bin":
                    $whr=" WHERE (sync_flg IS NULL OR sync_flg = '')";
                    break;
                case "sma_gp_src":
                    $whr=" WHERE (sync_flg IS NULL OR sync_flg = '')";
                    break;  
                case "sma_mrn_receipt":
                 $whr=" INNER JOIN sma_mrn_rcpt_src ON (sma_mrn_receipt.id=sma_mrn_rcpt_src.mrn_rcpt_id) INNER JOIN sma_mrn_rcpt_item_lot ON (sma_mrn_rcpt_item_lot.mrn_rcpt_item_id=sma_mrn_rcpt_src.id) INNER JOIN sma_mrn_rcpt_lot_bin ON (sma_mrn_rcpt_item_lot.id=sma_mrn_rcpt_lot_bin.lot_id) WHERE (sma_mrn_receipt.sync_flg IS NULL OR sma_mrn_receipt.sync_flg = '')";
                    break;
                case "sma_mrn_rcpt_src":
                    $whr=" INNER JOIN sma_mrn_receipt ON (sma_mrn_receipt.id=sma_mrn_rcpt_src.mrn_rcpt_id) INNER JOIN sma_mrn_rcpt_item_lot ON (sma_mrn_rcpt_item_lot.mrn_rcpt_item_id=sma_mrn_rcpt_src.id) INNER JOIN sma_mrn_rcpt_lot_bin ON (sma_mrn_rcpt_item_lot.id=sma_mrn_rcpt_lot_bin.lot_id) WHERE (sma_mrn_rcpt_src.sync_flg IS NULL OR sma_mrn_rcpt_src.sync_flg = '')";
                    break;
                case "sma_mrn_rcpt_item":
                    $whr=" INNER JOIN sma_mrn_rcpt_item_lot ON (sma_mrn_rcpt_item_lot.mrn_rcpt_item_id=sma_mrn_rcpt_item.id) INNER JOIN sma_mrn_rcpt_lot_bin ON (sma_mrn_rcpt_item_lot.id=sma_mrn_rcpt_lot_bin.lot_id) WHERE (sma_mrn_rcpt_item.sync_flg IS NULL OR sma_mrn_rcpt_item.sync_flg = '')";
                    break;
                case "sma_mrn_rcpt_item_lot":
                    $whr=" INNER JOIN sma_mrn_rcpt_lot_bin ON (sma_mrn_rcpt_item_lot.id=sma_mrn_rcpt_lot_bin.lot_id) WHERE (sma_mrn_rcpt_item_lot.sync_flg IS NULL OR sma_mrn_rcpt_item_lot.sync_flg = '')";
                    break;
                case "sma_mrn_rcpt_lot_bin":
                    $whr=" WHERE (sync_flg IS NULL OR sync_flg = '')";
                    break;
                case "sma_emrs_item":
                    $whr=" WHERE status='2'";
                    break;
                case "sma_companies":
                    $whr=" WHERE group_name='customer' AND segment_id IS NOT NULL AND ((sync_flg IS NULL OR sync_flg = '') OR upd_flg ='1')";
                    break;
                case "sma_costing":
                    $whr=" WHERE ((sync_flg IS NULL OR sync_flg = '') OR upd_flg = '1')";
                    break;                 
                case "sma_pos_register":
                    $whr=" WHERE ((sync_flg IS NULL OR sync_flg = '') AND status = 'close')";
                    break; 

                // added by vikas singh

                 case "sma_expenses":
                    $whr=" WHERE ((sync_flg IS NULL OR sync_flg = '') OR upd_flg = '1')";
                    break; 
                 // end       
                case "sma_sale_item_lot":
                    $whr=" INNER JOIN sma_sales ON (sma_sales.id = sma_sale_item_lot.sale_id) WHERE ((sma_sale_item_lot.sync_flg IS NULL OR sma_sale_item_lot.sync_flg = '') AND sma_sales.sale_status='completed')";
                    break;
                case "sma_sale_item_bin":
                    $whr=" INNER JOIN sma_sales ON (sma_sales.id = sma_sale_item_bin.sale_id) WHERE ((sma_sale_item_bin.sync_flg IS NULL OR sma_sale_item_bin.sync_flg = '') AND sma_sales.sale_status='completed')";
                    break; 
                case "sma_cheque_details":
                    $whr=" WHERE (sync_flg IS NULL OR sync_flg = '')";
                    break;   
                // added by Ankit

                case "sma_stk_adjt":
                    $whr=" WHERE (sync_flg IS NULL OR sync_flg = '')";
                    break; 
                case "sma_stk_adjt_itm":
                    $whr=" WHERE (sync_flg IS NULL OR sync_flg = '')";
                    break; 
                case "sma_stk_adjt_lot":
                    $whr=" WHERE (sync_flg IS NULL OR sync_flg = '')";
                    break; 
                case "sma_stk_adjt_bin":
                    $whr=" WHERE (sync_flg IS NULL OR sync_flg = '')";
                    break;                 
                                     
                default:
                    $whr='';
            }
            $host = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;
            $posDB = $this->posDbName();
            $connection = mysqli_connect($host, $username, $password, $dbname) or die("Connection Error " . mysqli_error($connection));

            if($whr!=''){
               
                $sql = 'select '.$table.'.* from '.$table. $whr;
                                            
                $result = mysqli_query($connection, $sql) or die("Selection Error " . mysqli_error($connection));
            }
            if (!is_dir($directory)) { //create the folder if it's not already exists
                mkdir($directory, 0755, TRUE);  
            }
           // $locDir = $directory."/".$projName;
             $locDir = $directory;
            if (!is_dir($locDir)) { 
                mkdir($locDir, 0755, TRUE); 
            } 
            $fp = fopen($filePath. $csv_filename, 'w');
            while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
            {   
                fputcsv($fp, $row);
              if($table!='sma_drft_po' and $table!='sma_costing' and $table!='sma_companies' and $table!='sma_emrs_item' and $table!='sma_cons_item_stk_profile' and $table!='sma_cons_item_stk_lot_bin' and $table!='sma_cons_item_stk_lot' and $table!='sma_drft_po_dlv_schdl' and $table!='sma_item_stk_profile' and $table!='sma_item_stk_lot_bin' and $table!='sma_item_stk_lot'){
                   $q1 = "UPDATE ".$table." SET sync_flg = '1' WHERE id = ".$row[id]." ";
                   //$r1 = mysqli_query($connection, $q1) or die("Selection Error " . mysqli_error($connection));
                 }
            if($table=='sma_costing' OR $table=='sma_companies')
              {
                 $q1 = "UPDATE ".$table." SET sync_flg = '1',upd_flg=null WHERE id = ".$row[id]." ";
                //$r1 = mysqli_query($connection, $q1) or die("Selection Error " . mysqli_error($connection));
              }
             if($table=='sma_cons_item_stk_profile' || $table=='sma_cons_item_stk_lot_bin' || $table=='sma_cons_item_stk_lot' || $table=='sma_item_stk_lot_bin' || $table=='sma_item_stk_lot' || $table=='sma_item_stk_profile' )
              {
                 $q1 = "UPDATE ".$table." SET create_flg = null,upd_flg=null WHERE id = ".$row[id]." ";
               // $r1 = mysqli_query($connection, $q1) or die("Selection Error " . mysqli_error($connection));
              } 
              if($table=='sma_drft_po_dlv_schdl')
              {
                 $q1 = "UPDATE ".$table." SET upd_flg='1' WHERE id = ".$row[id]." ";
                 //$r1 = mysqli_query($connection, $q1) or die("Selection Error " . mysqli_error($connection));
              }
            } 
            fclose($fp); 
        }   
       // $source_dir = $directory.$projName.'/';
       // $zip_file = $directory.$projName.'.zip';
        // creating zip file and inserting the record on remote server after is copied on remote server
        
        $source_dir = $directory.'/';
        
        $zip_file = substr($directory, 0,strlen($directory)-1).'.zip';        
        $zip = new ZipArchive();
        if ($zip->open($zip_file, ZIPARCHIVE::CREATE) === true) {
              foreach (glob($source_dir."*") as $file) {
                  $files = explode("/",$file)[count(explode("/",$file))-1];
                  $zip->addFile($file,$files);                
            }
        $zip->close();
        $this->site->rrmdir($directory);
        rmdir($directory);
        //rmdir($directory.$projName);
        $r_file_path = substr($r_directory, 0,strlen($r_directory)-1).'.zip';
		
       // $r_file_path='uploads/2017-04-04_49_20.zip';
        $remote_path = $r_directory;
        $ftp_host = FTP_HOST; /* host */
        $ftp_user_name = FTP_USERNAME; /* username */
        $ftp_user_pass = FTP_PWD; /* password */        
        $remote_file = $remote_path;
        //$local_file = $_SERVER["DOCUMENT_ROOT"]."/".PROJECT_NAME."/syncout-folder/2017-03-28_49_3.zip";
        $local_file = $zip_file;
        //$local_file = 'C:/xampp/htdocs/".PROJECT_NAME."/syncout-folder/2017-04-04_49_20.zip';
       //  echo $remote_file;exit;;
        $connect_it = ftp_connect( $ftp_host );
        ftp_pasv($connect_it, true);

        /* Login to FTP */
        $login_result = ftp_login( $connect_it, $ftp_user_name, $ftp_user_pass );
       
        /* Add notice for success/failure */
        if ( ftp_put( $connect_it, $r_file_path,$local_file, FTP_BINARY ) ) {
        //if(ftp_put( $connect_it,$r_file_path,$local_file,FTP_BINARY )) {
            ftp_chmod($connect_it, 0777, $r_file_path);
            echo "Successfully written to $local_file\n";
            //ftp_close( $connect_it );
            $file_path = ltrim(explode($ip,$r_file_path)[1],'/');
           // echo $file_path;exit;
           //  $r_file_path = substr_compare(main_str, str, offset)
            $sql = "INSERT INTO `sma_syncout_history`(`file_name`, `segment_id`, `created_date`, `flag`) VALUES ('".$r_file_path."','".$segment."','".date('Y-m-d H:i:s')."',0)";
            $csv_data = mysqli_query($remote_connection, $sql) or die("Selection Error " . mysqli_error($remote_connection));
           // print_r(mysql_insert_id());exit;
            if($csv_data){  

           /* $sql22 = "UPDATE sma_item_stk_profile SET create_flg = null,upd_flg=null WHERE create_flg='1' OR upd_flg= '1'";
            mysqli_query($connection, $sql22) or die("Update STK PROFILE FLG Error " . mysqli_error($connection)); */    
            //---------------------------------------------------------
               
               
$a1 = "UPDATE sma_sales SET sync_flg='1' WHERE (sync_flg IS NULL OR sync_flg = '') AND sale_status='completed' ";

$a2 = "UPDATE sma_sale_items INNER JOIN sma_sales ON (sma_sales.id = sma_sale_items.sale_id) SET sma_sale_items.sync_flg='1' WHERE ((sma_sale_items.sync_flg IS NULL OR sma_sale_items.sync_flg = '') AND sma_sales.sale_status='completed') ";

$a3 = "UPDATE sma_sale_item_lot INNER JOIN sma_sales ON (sma_sales.id = sma_sale_item_lot.sale_id) SET sma_sale_item_lot.sync_flg='1'  WHERE ((sma_sale_item_lot.sync_flg IS NULL OR sma_sale_item_lot.sync_flg = '') AND sma_sales.sale_status='completed')";

$a4 = "UPDATE sma_sale_item_bin INNER JOIN sma_sales ON (sma_sales.id = sma_sale_item_bin.sale_id) SET sma_sale_item_bin.sync_flg='1' WHERE ((sma_sale_item_bin.sync_flg IS NULL OR sma_sale_item_bin.sync_flg = '') AND sma_sales.sale_status='completed') ";

$a5 = "UPDATE sma_return_sales SET sync_flg='1' WHERE sync_flg IS NULL OR sync_flg = ''";

$a6 = "UPDATE sma_return_items SET sync_flg='1' WHERE sync_flg IS NULL OR sync_flg = ''";

$a7 = "UPDATE sma_payments INNER JOIN sma_sales ON (sma_sales.id = sma_payments.sale_id) SET sma_payments.sync_flg='1'  WHERE ((sma_payments.sync_flg IS NULL OR sma_payments.sync_flg = '') AND sma_sales.sale_status='completed')";

$a8 = "UPDATE sma_item_stk_profile SET create_flg =NULL, upd_flg =NULL WHERE create_flg ='1' OR upd_flg ='1'";

$a9 = "UPDATE sma_item_stk_lot SET create_flg =NULL, upd_flg =NULL WHERE create_flg ='1' OR upd_flg ='1'";

$a10 = "UPDATE sma_item_stk_lot_bin SET create_flg =NULL, upd_flg =NULL WHERE create_flg ='1' OR upd_flg ='1'";

$a11 = "UPDATE sma_cons_item_stk_profile SET create_flg =NULL, upd_flg =NULL WHERE create_flg ='1' OR upd_flg ='1'";

$a12 = "UPDATE sma_cons_item_stk_lot SET create_flg =NULL, upd_flg =NULL WHERE create_flg ='1' OR upd_flg ='1'";

$a13 = "UPDATE sma_cons_item_stk_lot_bin SET create_flg =NULL, upd_flg =NULL WHERE create_flg ='1' OR upd_flg ='1'";

$a14 = "UPDATE sma_gp SET sync_flg='1' WHERE sync_flg IS NULL OR sync_flg = ''";

$a15 = "UPDATE sma_gp_src SET sync_flg='1' WHERE sync_flg IS NULL OR sync_flg = ''";

$a16 = "UPDATE sma_gp_item SET sync_flg='1' WHERE sync_flg IS NULL OR sync_flg = ''";

$a17 = "UPDATE sma_gp_item_lot SET sync_flg='1' WHERE sync_flg IS NULL OR sync_flg = ''";

$a18 = "UPDATE sma_gp_lot_bin SET sync_flg='1' WHERE sync_flg IS NULL OR sync_flg = ''";

$a19 = "UPDATE sma_mrn_receipt SET sync_flg='1' WHERE sync_flg IS NULL OR sync_flg = ''";

$a20 = "UPDATE sma_mrn_rcpt_src SET sync_flg='1' WHERE sync_flg IS NULL OR sync_flg = ''";

$a21 = "UPDATE sma_mrn_rcpt_item SET sync_flg='1' WHERE sync_flg IS NULL OR sync_flg = ''";

$a22 = "UPDATE sma_mrn_rcpt_item_lot SET sync_flg='1' WHERE sync_flg IS NULL OR sync_flg = ''";

$a23 = "UPDATE sma_mrn_rcpt_lot_bin SET sync_flg='1' WHERE sync_flg IS NULL OR sync_flg = ''";

$a24 = "UPDATE sma_drft_po_dlv_schdl SET upd_flg='1' WHERE status=1 AND (upd_flg IS NULL OR upd_flg='')";

$a25 = "UPDATE sma_cheque_details SET sync_flg='1' WHERE sync_flg IS NULL OR sync_flg = ''";

$a26 = "UPDATE sma_expenses SET sync_flg='1',upd_flg = NULL WHERE ((sync_flg IS NULL OR sync_flg = '') OR upd_flg = '1')";

$a27 = "UPDATE sma_pos_register SET sync_flg='1' WHERE ((sync_flg IS NULL OR sync_flg = '') AND status = 'close')";

$a28 = "UPDATE sma_costing SET sync_flg='1',upd_flg = NULL WHERE ((sync_flg IS NULL OR sync_flg = '') OR upd_flg = '1')";

$a29 = "UPDATE sma_companies SET sync_flg='1',upd_flg = NULL WHERE group_name='customer' AND segment_id IS NOT NULL AND ((sync_flg IS NULL OR sync_flg = '') OR upd_flg ='1')";

$a30 = "UPDATE sma_stk_adjt SET sync_flg='1' WHERE sync_flg IS NULL OR sync_flg = ''";

$a31 = "UPDATE sma_stk_adjt_itm SET sync_flg='1' WHERE sync_flg IS NULL OR sync_flg = ''";

$a32 = "UPDATE sma_stk_adjt_lot SET sync_flg='1' WHERE sync_flg IS NULL OR sync_flg = ''";

$a33 = "UPDATE sma_stk_adjt_bin SET sync_flg='1' WHERE sync_flg IS NULL OR sync_flg = ''";

mysqli_query($connection, $a1) or die("Sales Master Error " . mysqli_error($connection));
mysqli_query($connection, $a2) or die("Sales Item Master Error " . mysqli_error($connection));
mysqli_query($connection, $a3) or die("Sales Item Lot Master Error " . mysqli_error($connection));
mysqli_query($connection, $a4) or die("Sales Item Bin Master Error " . mysqli_error($connection));
mysqli_query($connection, $a5) or die("Retuen Sales Master Error " . mysqli_error($connection));
mysqli_query($connection, $a6) or die("Retuen Sales Item Master Error " . mysqli_error($connection));
mysqli_query($connection, $a7) or die("Payment Master Error " . mysqli_error($connection));
mysqli_query($connection, $a8) or die("Stock Profile Master Error " . mysqli_error($connection));
mysqli_query($connection, $a9) or die("Stock Lot Master Error " . mysqli_error($connection));
mysqli_query($connection, $a10) or die("Stock Bin Master Error " . mysqli_error($connection));
mysqli_query($connection, $a11) or die("Cons Stock Profile Master Error " . mysqli_error($connection));
mysqli_query($connection, $a12) or die("Cons Stock Lot Master Error " . mysqli_error($connection));
mysqli_query($connection, $a13) or die("Cons Stock Bin Master Error " . mysqli_error($connection));
mysqli_query($connection, $a14) or die("GP Master Error " . mysqli_error($connection));
mysqli_query($connection, $a15) or die("GP SRC Master Error " . mysqli_error($connection));
mysqli_query($connection, $a16) or die("GP Item Master Error " . mysqli_error($connection));
mysqli_query($connection, $a17) or die("GP Lot Master Error " . mysqli_error($connection));
mysqli_query($connection, $a18) or die("GP Bin Master Error " . mysqli_error($connection));
mysqli_query($connection, $a19) or die("MRN RECIPT Master Error " . mysqli_error($connection));
mysqli_query($connection, $a20) or die("MRN SRC Master Error " . mysqli_error($connection));
mysqli_query($connection, $a21) or die("MRN Item Master Error " . mysqli_error($connection));
mysqli_query($connection, $a22) or die("MRN Lot Master Error " . mysqli_error($connection));
mysqli_query($connection, $a23) or die("MRN Bin Master Error " . mysqli_error($connection));
mysqli_query($connection, $a24) or die("PO Schedule Master Error " . mysqli_error($connection));
mysqli_query($connection, $a25) or die("Cheque Master Error " . mysqli_error($connection));
mysqli_query($connection, $a26) or die("Expense Master Error " . mysqli_error($connection));
mysqli_query($connection, $a27) or die("Register Master Error " . mysqli_error($connection));
mysqli_query($connection, $a28) or die("Costing Master Error " . mysqli_error($connection));
mysqli_query($connection, $a29) or die("Company Master Error " . mysqli_error($connection));

mysqli_query($connection, $a30) or die("Company Master Error " . mysqli_error($connection));
mysqli_query($connection, $a31) or die("Company Master Error " . mysqli_error($connection));
mysqli_query($connection, $a32) or die("Company Master Error " . mysqli_error($connection));
mysqli_query($connection, $a33) or die("Company Master Error " . mysqli_error($connection));


            
 // local store backup process
        $dbhost=$this->db->hostname;
        $dbuser=$this->db->username;
        $dbpass=$this->db->password;
        $db=$this->db->database;
        $select = "SELECT id,proj_name FROM `sma_segment` LIMIT 1";
        $selectseg = $this->db->query($select);
        $segmentdetails= $selectseg->result();
        $seg_id = $segmentdetails[0]->id;
        $seg_name = str_replace(' ','_',$segmentdetails[0]->proj_name);
        $full_path= $_SERVER['DOCUMENT_ROOT'].'/'.PROJECT_NAME.'/db_backup/';
        if (!file_exists($full_path)) {
        mkdir($full_path, 0777, true);
        }

        $d= date("Y-m-d_H-i-s");
        $path=$full_path.PROJECT_NAME.'_'.$seg_name.'_'.$seg_id.'_'.$d.'.sql';
        $drive = explode('/',$_SERVER['DOCUMENT_ROOT']);
        $final_drive= $drive[0];
        $path_to_mysqldump = $final_drive."\\xampp\mysql\bin";
        //$query= "$path_to_mysqldump\mysqldump.exe -u $dbuser -p$dbpass $db > $path";
        $query= "$path_to_mysqldump\mysqldump.exe -u $dbuser $db > $path";
        exec($query);
        
        // transfer sql file to central server
        $r_directory = 'db_backup/'.PROJECT_NAME.'_'.$seg_name.'_'.$seg_id.'_'.$d.'.sql';
        $remote_path = $r_directory;
        $ftp_host = FTP_HOST; /* host */
        $ftp_user_name = FTP_USERNAME; /* username */
        $ftp_user_pass = FTP_PWD; /* password */         
        $remote_file = $remote_path;
        $local_file = $path;
        $connect_it = ftp_connect( $ftp_host );
        ftp_pasv($connect_it, true);

        /* Login to FTP */
        $login_result = ftp_login( $connect_it, $ftp_user_name, $ftp_user_pass );
        if ( ftp_put( $connect_it, $r_directory,$local_file, FTP_BINARY ) ) {
            ftp_chmod($connect_it, 0777, $r_directory);
            }

        // end local store backup process


            //=========================================================




                $this->session->set_flashdata('message', "Zip created Successfully!!");
                redirect('welcome');
            }
           // $this->db->insert('csv_history',$csv_data_insertion);  
        
        }// creating zip file and inserting the record on remote server ends
        else{
            $this->session->set_flashdata('message', "There was a problem\n");
           
            redirect('welcome');
        }
     }  

       //         $this->session->set_flashdata('message', "Failed to create Zip");
    redirect('welcome');

  } 

  public function restructureData($warehouseLists){
    if(count($warehouseLists)>0){

        $host = $this->db->hostname;
        $username = $this->db->username;
        $password = $this->db->password;
        $dbname = $this->db->database;
        $posDB = $this->posDbName();
        $connection = mysqli_connect($host, $username, $password, $dbname) or die("Connection Error " . mysqli_error($connection));

        // mysqli_query($connection, 'DROP TABLE IF EXISTS sma_sale_item_lot_real') or die("Drop sma_sale_item_lot_real Master Error " . mysqli_error($connection));
        // mysqli_query($connection, 'DROP TABLE IF EXISTS sma_sale_item_bin_real') or die("Drop sma_sale_item_bin_real Schedule Error " . mysqli_error($connection));
        // mysqli_query($connection, 'DROP TABLE IF EXISTS sma_sales_real') or die("Drop sma_sales_real Master Error " . mysqli_error($connection));
        // mysqli_query($connection, 'DROP TABLE IF EXISTS sma_sale_items_real') or die("Drop sma_sale_items_real Schedule Error " . mysqli_error($connection));

        //  mysqli_query($connection, 'DROP TABLE IF EXISTS sma_item_stk_profile_real') or die("Drop sma_item_stk_profile_real Schedule Error " . mysqli_error($connection));
        // mysqli_query($connection, 'DROP TABLE IF EXISTS sma_item_stk_lot_real') or die("Drop sma_item_stk_lot_real Master Error " . mysqli_error($connection));
        // mysqli_query($connection, 'DROP TABLE IF EXISTS sma_item_stk_lot_bin_real') or die("Drop sma_item_stk_lot_bin_real Schedule Error " . mysqli_error($connection));


        // // Created temp table to take back up of sale and stock data
        // mysqli_query($connection, 'create table sma_sale_item_lot_real (select * from sma_sale_item_lot)') or die("Create sma_sale_item_lot_real Master Error " . mysqli_error($connection));
        // mysqli_query($connection, 'create table sma_sale_item_bin_real (select * from sma_sale_item_bin)') or die("Create sma_sale_item_bin_real Master Error " . mysqli_error($connection));
        // mysqli_query($connection, 'create table sma_sales_real (select * from sma_sales)') or die("Create sma_sales_real Master Error " . mysqli_error($connection));
        // mysqli_query($connection, 'create table sma_sale_items_real (select * from sma_sale_items)') or die("Create sma_sale_items_real Master Error " . mysqli_error($connection));
        
        // mysqli_query($connection, 'create table sma_item_stk_profile_real (select * from sma_item_stk_profile)') or die("Create sma_sale_items_real Master Error " . mysqli_error($connection));
        // mysqli_query($connection, 'create table sma_item_stk_lot_real (select * from sma_item_stk_lot)') or die("Create sma_sale_items_real Master Error " . mysqli_error($connection));
        // mysqli_query($connection, 'create table sma_item_stk_lot_bin_real (select * from sma_item_stk_lot_bin)') or die("Create sma_sale_items_real Master Error " . mysqli_error($connection));
        $where = 'AND warehouse_id = 108';


        // Deleted sale lot and bin data
       mysqli_query($connection, 'DELETE FROM sma_sale_item_lot WHERE 1') or die("Create sma_sale_item_lot Master Error " . mysqli_error($connection));
       mysqli_query($connection, 'DELETE FROM sma_sale_item_bin WHERE 1') or die("Create sma_sale_item_bin Master Error " . mysqli_error($connection));
       foreach($warehouseLists as $wh_id){
        $warehouseList = $wh_id->warehouseList;

        // Placing lot no. to stock and mrn bin table
            mysqli_query($connection, 'UPDATE sma_item_stk_lot_bin INNER JOIN sma_item_stk_lot AS c ON (sma_item_stk_lot_bin.lot_id=c.id AND sma_item_stk_lot_bin.item_id=c.item_id) 
                SET sma_item_stk_lot_bin.lot_no=c.lot_no WHERE sma_item_stk_lot_bin.lot_id=c.id AND sma_item_stk_lot_bin.item_id=c.item_id AND c.warehouse_id IN ('.$warehouseList.') ') or die("Create sma_sale_item_lot Master Error " . mysqli_error($connection));


            mysqli_query($connection, 'UPDATE sma_mrn_rcpt_lot_bin INNER JOIN sma_mrn_rcpt_item_lot AS c ON (sma_mrn_rcpt_lot_bin.lot_id=c.id AND sma_mrn_rcpt_lot_bin.item_id=c.item_id) SET sma_mrn_rcpt_lot_bin.lot_no=c.lot_no WHERE sma_mrn_rcpt_lot_bin.lot_id=c.id AND sma_mrn_rcpt_lot_bin.item_id=c.item_id AND c.wh_id = 136') or die("Create sma_sale_item_lot Master Error " . mysqli_error($connection));
            
            mysqli_query($connection, 'UPDATE sma_item_stk_lot INNER JOIN sma_mrn_rcpt_item_lot AS c ON (sma_item_stk_lot.item_id=c.item_id AND sma_item_stk_lot.lot_no=c.lot_no) INNER JOIN sma_mrn_rcpt_item AS d ON (c.mrn_rcpt_item_id=d.id) INNER JOIN sma_mrn_rcpt_src as e on (d.mrn_rcpt_src_id=e.id) INNER JOIN sma_mrn_receipt AS f ON (f.id=e.mrn_rcpt_id)
                SET sma_item_stk_lot.total_stk=c.lot_qty WHERE sma_item_stk_lot.item_id=c.item_id AND sma_item_stk_lot.lot_no=c.lot_no AND f.status=1 AND c.wh_id IN ('.$warehouseList.')') or die("Create sma_sale_item_lot Master Error " . mysqli_error($connection));


            mysqli_query($connection, 'UPDATE sma_item_stk_lot_bin INNER JOIN sma_mrn_rcpt_lot_bin AS z ON (sma_item_stk_lot_bin.item_id=z.item_id AND sma_item_stk_lot_bin.bin_id=z.bin_id AND sma_item_stk_lot_bin.lot_no=z.lot_no) INNER JOIN sma_mrn_rcpt_item_lot AS c ON (z.item_id=c.item_id AND z.lot_id=c.id) INNER JOIN sma_mrn_rcpt_item AS d ON (c.mrn_rcpt_item_id=d.id) INNER JOIN sma_mrn_rcpt_src as e on (d.mrn_rcpt_src_id=e.id) INNER JOIN sma_mrn_receipt AS f ON (f.id=e.mrn_rcpt_id) SET sma_item_stk_lot_bin.total_stk=z.bin_qty WHERE sma_item_stk_lot_bin.item_id=z.item_id AND sma_item_stk_lot_bin.bin_id=z.bin_id AND sma_item_stk_lot_bin.lot_no=z.lot_no AND f.status=1 AND c.wh_id IN ('.$warehouseList.')') or die("Create sma_sale_item_lot Master Error " . mysqli_error($connection));

          /*  mysqli_query($connection, 'UPDATE sma_item_stk_lot INNER JOIN ( SELECT a.total_stk,asd.*,(a.total_stk-asd.lot_stk) AS remaing_stk FROM `sma_item_stk_profile` as a INNER JOIN (SELECT SUM(total_stk) as lot_stk,item_id FROM sma_item_stk_lot where lot_no not like "%0001" group by item_id) as asd ON asd.item_id = a.item_id group by a.item_id ) as ad on sma_item_stk_lot.item_id=ad.item_id SET sma_item_stk_lot.total_stk= ad.remaing_stk where sma_item_stk_lot.lot_no like "%0001" AND sma_item_stk_lot.item_id=ad.item_id') or die("Create sma_sale_item_lot Master Error " . mysqli_error($connection));*/

            
            mysqli_query($connection, 'UPDATE sma_item_stk_lot AS a INNER JOIN sma_item_stk_profile AS b ON a.item_id=b.item_id SET a.total_stk=b.o_qty WHERE a.lot_no LIKE "%0001" AND a.item_id=b.item_id AND a.warehouse_id IN ('.$warehouseList.')') or die("Updating default lot master Error " . mysqli_error($connection));


            mysqli_query($connection, 'UPDATE sma_item_stk_lot_bin AS a INNER JOIN sma_item_stk_lot AS b ON a.lot_id=b.id SET a.total_stk=b.total_stk WHERE a.lot_no LIKE "%0001" AND b.lot_no LIKE "%0001" AND a.item_id=b.item_id AND a.warehouse_id IN ('.$warehouseList.')') or die("Updating default bin master Error " . mysqli_error($connection));

             
            mysqli_query($connection, 'UPDATE sma_item_stk_profile INNER JOIN ( SELECT SUM(total_stk) AS total_stk,item_id FROM `sma_item_stk_lot` group by item_id ) as ad on sma_item_stk_profile.item_id=ad.item_id SET sma_item_stk_profile.available_stk=ad.total_stk,sma_item_stk_profile.total_stk=ad.total_stk where sma_item_stk_profile.item_id=ad.item_id AND sma_item_stk_profile.warehouse_id IN ('.$warehouseList.')') or die("Create sma_sale_item_lot Master Error " . mysqli_error($connection));

       // $warehouse_ids = explode(",",$warehouseList);
        
        //mysqli_query($connection,'')
                $sale_item_list = $this->db->select("product_id,quantity,sale_id,sale_ref_no,product_code,id")->from('sale_items')->where('warehouse_id',$warehouseList)->order_by('id',ASC)->get()->result();       
                foreach($sale_item_list as $keyy=>$row){
                   $prod_id = $row->product_id;
                   $quantity = $row->quantity;
                   $sale_id = $row->sale_id;
                   $reference_no = $row->sale_ref_no;
                   $prod_code = $row->product_code;
                   $sale_item_id = $row->id;
                $items_list = $this->db->select("item_stk_profile.segment_id,products.wh_id,item_stk_profile.item_id,item_stk_profile.warehouse_id,item_stk_profile.org_id,item_stk_profile.available_stk,item_stk_lot.total_stk as slot_total,item_stk_lot_bin.total_stk as sbin_total,item_stk_profile.total_stk");
                $this->db->from("item_stk_profile");
                $this->db->join("products","products.id = item_stk_profile.item_id","inner");
                 
                $this->db->join("item_stk_lot","item_stk_lot.item_stk_id = item_stk_profile.id AND item_stk_lot.warehouse_id = item_stk_profile.warehouse_id","inner");
                $this->db->join("item_stk_lot_bin","item_stk_lot_bin.lot_id = item_stk_lot.id AND item_stk_lot_bin.warehouse_id = item_stk_lot.warehouse_id","inner");
                $this->db->where("item_stk_profile.item_id",$prod_id);
               // $this->db->where("item_stk_lot.lot_no",$items['lot_no']);
              //  $this->db->where("item_stk_lot_bin.bin_id",$items['bin_id']);
                $q = $this->db->get();
                $qtydata = $q->result();
                $segment_id = $qtydata[0]->segment_id;
                $available_stk= $qtydata[0]->available_stk - $quantity;       
                // echo "total stk===>".$qtydata[0]->total_stk."available_stk====>".$available_stk."item_id===>".$prod_id."quantity".$quantity."================================<br>";

                if(count($qtydata) >0)
                {      
                     // if($lot_data!='' && $bin_data!=''){
                    $lot_Data = $this->db->select("*")->from("item_stk_lot")->where("item_id",$prod_id)->where("total_stk >",0)->order_by("id",ASC)->get()->result();
                    $minus_qty = 0;                
                    $lots = array();
                    $stock_sale = array();
                    $bins = array();
                    $stock_saleb = array();
                    $bin_sum = $quantity;
                    $sum = $quantity;

                    //echo "sum".$sum;
                    foreach ($lot_Data as $key => $value) {
                     $bin_data = $this->db->select("bin.bin_nm,item_stk_lot_bin.*")->from("item_stk_lot_bin")->join('bin','bin.id=item_stk_lot_bin.bin_id','inner')->where("item_id",$prod_id)->where("lot_id",$value->id)->where("total_stk >",0)->order_by("id",ASC)->get()->result();           
                        foreach($bin_data  as $bin_val){
                           // echo "bin cum".$bin_sum."<br>";
                            if(($bin_sum<=$bin_val->total_stk) && ($bin_sum!=0)){
                                
                                $left_qty_bin = $bin_val->total_stk-$bin_sum;
                                $dtb['left_quantity'] = $left_qty_bin;
                                $dtb['id'] = $bin_val->id;
                                $stock_sale_bin['bin_id'] = $bin_val->id;
                                $stock_sale_bin['lot_id'] = $bin_val->lot_id;
                                $stock_sale_bin['bin_name'] = $bin_val->bin_nm;
                               // $stock_sale_bin['lot_no'] = $bin_val->lot_no;
                                $stock_sale_bin['quantity'] = $bin_sum;                                     
                                array_push($bins,$dtb);
                                array_push($stock_saleb,$stock_sale_bin);                                  
                                
                                break; 
                            }
                            else if($bin_sum > $bin_val->total_stk){                
                               
                                $dtb['left_quantity'] = 0;
                                $dtb['id'] = $bin_val->id;
                                
                                $stock_sale_bin['bin_id'] = $bin_val->id;
                                $stock_sale_bin['lot_id'] = $bin_val->lot_id;
                                $stock_sale_bin['bin_name'] = $bin_val->bin_nm;
                               // $stock_sale_lot['lot_no'] = $value->lot_no;
                                
                                $stock_sale_bin['quantity'] = $bin_val->total_stk; 
                                $bin_sum = $bin_sum - $bin_val->total_stk;

                                array_push($bins,$dtb);
                                array_push($stock_saleb,$stock_sale_bin);
                               //  echo "asd".$sum;exit;
                            }
                        }

                    // lot quantity calculation


                        if($sum<=$value->total_stk){
                           //  echo $value->total_stk."------".$sum;
                            $left_qty = $value->total_stk-$sum;
                            $dt['left_quantity'] = $left_qty;
                            $dt['id'] = $value->id;
                            $stock_sale_lot['lot_id'] = $value->id;
                            $stock_sale_lot['lot_no'] = $value->lot_no;
                            $stock_sale_lot['quantity'] = $sum;                                     
                            array_push($lots,$dt);
                            array_push($stock_sale,$stock_sale_lot);
                            break; 
                        }
                        if($sum > $value->total_stk){                
                           
                            $dt['left_quantity'] = 0;
                            $dt['id'] = $value->id;
                            
                            $stock_sale_lot['lot_id'] = $value->id;
                            $stock_sale_lot['lot_no'] = $value->lot_no;
                            $stock_sale_lot['quantity'] = $value->total_stk; 
                            $sum = $sum - $value->total_stk;
                            array_push($lots,$dt);
                            array_push($stock_sale,$stock_sale_lot);
                           //  echo "asd".$sum;exit;
                        }
                    } 

                    // echo "key".$keyy."item".$prod_id."<br>";
                    // print_r(count($lots));
                    // echo "==================<br>";
                    /*echo "<pre>";
                    print_r(count($bins));
                    echo "=================================================";
                    print_r($stock_sale);
                    echo "============";
                    print_r($stock_saleb);*/
                   

                    if(count($lots)>0){
                        $this->db->query("update sma_item_stk_profile SET available_stk = ".$available_stk.", upd_flg=1 where item_id= ".$prod_id." and segment_id= ".$segment_id."");
             
                        foreach($lots as $lot_val){
                           
                            $this->db->query("update sma_item_stk_lot SET total_stk = ".$lot_val['left_quantity'].",upd_flg=1 where item_id= ".$prod_id." and id = '".$lot_val['id']."' and segment_id= ".$segment_id.""); 

                        }
                        foreach($bins as $bins_val){
                           
                            $this->db->query("update sma_item_stk_lot_bin SET total_stk = ".$bins_val['left_quantity'].",upd_flg=1 where item_id= ".$prod_id." and id= ".$bins_val['id']." and segment_id= ".$segment_id.""); 

                        }
                       

                      //  print_r($stock_sale_lot);exit;    
                        // inserting lot and bin data into sale invoice
                        foreach($stock_sale as $lot){
                           // echo "<pre>";
                            //print_r($lots_data);exit;
                            $sale_lot = array("org_id"=>$qtydata[0]->org_id,"wh_id"=>$qtydata[0]->wh_id,"warehouse_id"=>$qtydata[0]->warehouse_id,"code"=>$prod_code,"sale_id"=>$sale_id,"sale_item_id"=>$sale_item_id,"lot_id"=>$lot['lot_id'],"lot_no"=>$lot["lot_no"],"quantity"=>$lot['quantity'],"created_date"=>date("Y-m-d H:i:s"),'sale_ref_no'=>$reference_no);
                          
                            $this->db->insert('sale_item_lot',$sale_lot);

                        }
                        foreach($stock_saleb as $bin){
                            $sale_bin = array("org_id"=>$qtydata[0]->org_id,"wh_id"=>$qtydata[0]->wh_id,"warehouse_id"=>$qtydata[0]->warehouse_id,"code"=>$prod_code,"sale_id"=>$sale_id,"sale_item_id"=>$sale_item_id,"lot_id"=>$bin['lot_id'],"bin_id"=>$bin["bin_id"],"bin_name"=>$bin["bin_name"],"quantity"=>$bin['quantity'],"created_date"=>date("Y-m-d H:i:s"),'sale_ref_no'=>$reference_no,'lot_no'=>$lot["lot_no"]);
                         
                            $this->db->insert('sale_item_bin',$sale_bin);                                

                        }
                    }
                    
                }                       

            }
            mysqli_query($connection, 'UPDATE sma_sale_item_lot INNER JOIN sma_sale_items AS c ON (sma_sale_item_lot.sale_item_id=c.id AND sma_sale_item_lot.code=c.product_code AND sma_sale_item_lot.warehouse_id=c.warehouse_id) 
                        SET sma_sale_item_lot.sync_flg=c.sync_flg WHERE sma_sale_item_lot.sale_item_id=c.id AND sma_sale_item_lot.code=c.product_code AND sma_sale_item_lot.warehouse_id=c.warehouse_id ') or die("Update sma_sale_item_lot Master Error " . mysqli_error($connection));

            mysqli_query($connection, 'UPDATE sma_sale_item_bin INNER JOIN sma_sale_items AS c ON (sma_sale_item_bin.sale_item_id=c.id AND sma_sale_item_bin.code=c.product_code AND sma_sale_item_bin.warehouse_id=c.warehouse_id) 
            SET sma_sale_item_bin.sync_flg=c.sync_flg WHERE sma_sale_item_bin.sale_item_id=c.id AND sma_sale_item_bin.code=c.product_code AND sma_sale_item_bin.warehouse_id=c.warehouse_id') or die("Update sma_sale_item_bin Master Error " . mysqli_error($connection));

        }

        redirect('welcome');
    }
   // exit;
  }
        
}

