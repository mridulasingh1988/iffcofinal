<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mrn_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function getSetting()
    {
        $q = $this->db->get('pos_settings');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getWareHouseDetail($warehouse_id=0)
    {       
        $this->load->model('site');
        $result = array('warehouse_name'=>'','po_data'=>array(),'tp_details'=>array(),'supplier_details'=>array());       
        $warehouse_detail = $this->site->get_warehouse_ids(1);        
        $result['warehouse_name'] = $warehouse_detail;
       // $where = '(drft_po_dlv_schdl.status=0 OR drft_po';
        $this->db->select('drft_po.*,drft_po_dlv_schdl.itm_uom_desc');
        $this->db->from('drft_po')
        ->join('drft_po_dlv_schdl', 'drft_po_dlv_schdl.po_id=drft_po.id', 'inner')
        //commented by vikas singh as suggested by chomu to list last dates PO as well.
       // ->where("ro_dt", 'CURDATE()', FALSE)
        ->where_in('wh_id', implode(",",$warehouse_id))
        ->where('drft_po_dlv_schdl.segment_id',$_SESSION['segment_id'])
        ->where('drft_po.po_status',217)    
        ->where('drft_po_dlv_schdl.status',0) 
        //->where('drft_po_dlv_schdl.ro_dt !=',1)       
        //  ->where($where)
        ->group_by('drft_po_dlv_schdl.po_id');

        $po_detail = $this->db->get()->result();
         
        $po_list = array();
        if(count($po_detail)>0){
            foreach($po_detail as $row){              
                array_push($po_list, $row);           
            }
            $result['po_data'] = $po_list;
        }       

        $this->db->select('*');
        $tp_details = $this->db->from('companies')
        ->where("eo_type", 'T');       
        $transporter_detail = $this->db->get()->result();
        $result['tp_details'] = $transporter_detail;

        $this->db->select('*');
        $tp_details = $this->db->from('companies')
        ->where("group_id", 4);       
        $supplier_detail = $this->db->get()->result();
        $result['supplier_details'] = $supplier_detail;
        
        return $result;
    }

    public function getDlvData($po_id,$ro_no='',$item_id=''){
       // echo "po".$po_id."ro".$ro_no."item id".$item_id;exit;
        $this->db->from('drft_po')
        ->join('drft_po_dlv_schdl', 'drft_po_dlv_schdl.po_id=drft_po.id', 'inner')
        ->join('products', 'drft_po_dlv_schdl.itm_id=products.id', 'inner')
        //commented by vikas singh as suggested by chomu to list last dates RO as well
        //->where("ro_dt =", 'CURDATE()', FALSE)
        ->where('drft_po_dlv_schdl.status!=',1)
        //->where('drft_po_dlv_schdl.dlv_qty')
        ->where('drft_po_dlv_schdl.po_id', $po_id)
        ->where('drft_po_dlv_schdl.segment_id', $_SESSION['segment_id']);
        if($ro_no!=''){  
            $this->db->select("*");                      
            $this->db->where('ro_no', $ro_no);
        }

        if($item_id!=''){                      
            $this->db->where('itm_id', $item_id);
        }else{
            $this->db->select('bat_item_id,ro_no,ro_dt,drft_po_dlv_schdl.id,use_rented_wh,itm_id,dlv_qty,bal_qty');
            //$this->db->group_by('drft_po_dlv_schdl.ro_no');
        }
        $this->db->order_by('drft_po_dlv_schdl.ro_no',ASC);
        $roList = $this->db->get()->result();
    
        return $roList;
    }


    public function getRoDetail($po_id,$ro_no='',$sup_id){
        $roList = $this->getDlvData($po_id,$ro_no,'');            
          if($ro_no!=''){
              //echo "sdsa";exit;
            
            if($roList[0]->use_rented_wh==1){ 

                foreach($roList as $key => $value) {                    
                    $billQty = $this->getItemFromMrn($value->itm_id, $value->dlv_qty, $value->bal_qty, $ro_no,$po_id);
                    
                    $itemVal = $this->getItemFromGp($value->itm_id, $value->dlv_qty, $value->bal_qty, $ro_no, $billQty['receivedQty']);
                    $value->from_gp = 1;
                   /* echo "<pre>";
                    print_r($billQty);
                    exit;*/
                    if($itemVal!=0){
                    $value->recv_qty = $itemVal;
                    if($billQty['receivedQty']!=0)
                        $value->dlv_qty = $billQty['remainQty'];

                                    
                    }else{
                        unset($roList[$key]);
                    }

                }
            }else{               
                foreach ($roList as $key => $value) {
                   // $this->mrn_model->getItemDetails($value->ro_no);
                //    if($value->use_rented_wh==1){
                        $billQty = $this->getItemFromMrn($value->itm_id,$value->dlv_qty,$value->bal_qty,$value->ro_no,$po_id);
                        //$itemVal = $this->getItemFromGp($value->itm_id, $value->dlv_qty, $value->bal_qty, $value->ro_no, $billQty['receivedQty']);
                        // echo "<pre>"; 
                        // print_r($billQty);exit;
                        if($billQty['receivedQty']!=0){
                            if($billQty['remainQty']<=0){
                                unset($roList[$key]);
                            }else{
                                $value->dlv_qty = $billQty['remainQty'];
                            }
                        }elseif($billQty['receivedQty']==0 && $billQty['remainQty']>0){
                            $value->dlv_qty = $billQty['remainQty'];
                        }
                    }
              //  }

            }
        }else{            

           $roArray = array();
        
           foreach($roList as $key=>$value){
                $billQty = $this->getItemFromMrn($value->itm_id, $value->dlv_qty, $value->bal_qty, $value->ro_no,$po_id);
                
                if($value->use_rented_wh==1){                   
                    $itemVal = $this->getItemFromGp($value->itm_id, $value->dlv_qty, $value->bal_qty, $value->ro_no, $billQty['receivedQty']);
                    if($itemVal!=0){                     
                        $value->recv_qty = $itemVal;
                        $value->dlv_qty = $billQty['remainQty'];
                        $value->from_gp = 1;
                        if(!in_array($value->ro_no,$roArray)){
                            array_push($roArray, $value->ro_no);
                        }else{
                            unset($roList[$key]);
                        }                    
                    }else{
                        unset($roList[$key]);
                    }

                }else{                   
                //    $billQty = $this->getItemFromMrn($value->itm_id, $value->dlv_qty, $value->bal_qty, $value->ro_no);
                  // echo "<pre>";print_r($billQty);exit;
                    if($billQty['receivedQty']!=0){
                        if($billQty['remainQty']==0 || $billQty['remainQty']<0){
                            unset($roList[$key]);
                        }else{
                            $value->dlv_qty = $billQty['remainQty'];
                           
                            if(!in_array($value->ro_no,$roArray)){                                
                                array_push($roArray, $value->ro_no);
                            }else{
                                unset($roList[$key]);
                            }
                        }
                    }else{
                        
                        if(!in_array($value->ro_no,$roArray)){                                
                                array_push($roArray, $value->ro_no);
                            }else{
                                unset($roList[$key]);
                            }                           

                    }
                }

           }        
          
           
        }
        

        if(count($roList)==0){
            return array();
        }
    
        return $roList;        
    }

    public function getPoDetail($wh_id){  
        $this->db->from('drft_po')
        ->join('drft_po_dlv_schdl', 'drft_po_dlv_schdl.po_id=drft_po.id', 'inner')
        //  ->where("ro_dt =", 'CURDATE()', FALSE)
        ->where('drft_po_dlv_schdl.wh_id', $wh_id);
            $this->db->select('ro_no,ro_dt');
        $poList = $this->db->get()->result();       
       /* echo "<pre>";
        print_r($poList);
        exit;*/
        if(count($roList)==0){
            return array();
        }
        return $roList;
    }
    public function getAllBin($warehouse_id){       
        $allBin = $this->db->select('bin.*')->from('bin')
        ->join('warehouses', 'warehouses.id=bin.warehouse_id', 'inner')
        ->where_in("warehouse_id", $warehouse_id)
        ->get()->result();
       
        return $allBin;
    }

    public function saveMrnData($postData,$poData,$mrn_receipt_id=NULL){ 

        if(isset($postData)){
                      
           /* echo "<pre>";
            print_r($postData);
            exit;*/
            $warehouse_code = $this->site->getWarehouse($_POST['warehouse_id'],$_SESSION['segment_id']);
          /*  echo "<pre>";
            print_r($_POST['warehouse_id']);
            exit;*/
            $count = 0;
            if(!$mrn_receipt_id){
                $maxId = $this->db->select('id')
                ->from('mrn_receipt')
                ->order_by('id','desc')
                ->limit(1);
                $maxId = $this->db->get()->row();
                //added by vikas singh
                //$mrnId = $maxId;
                $mrnId = isset($maxId)?($maxId->id+1):1;
                $rcpt_no = $_SESSION['segment_id']."/MRN/".$mrnId;
                $this->db->trans_begin();
                $mrnData = array(
                'ho_id' => 1,
                'org_id'=>$warehouse_code->org_id,
                'segment_id'=>$_SESSION['segment_id'],
                'wh_id' => $_POST['warehouse_id'],
                'warehouse_id' => $warehouse_code->wh_id,
                'rcpt_no'=> $rcpt_no,
                'rcpt_dt'=> isset($_POST['rcpt_dt'])?$_POST['rcpt_dt']:date('y-m-d'),
                'rcpt_src_type' => 1, // for supplier,
                'supplier_id'=> $_POST['supplier_id'],
                'dn_no' => $_POST['del_note_no'],
                'dn_dt'  => date('Y-m-d',strtotime($_POST['del_note_date'])),
                'tp_id' => $_POST['lr_name'],
                'tpt_lr_no' => $_POST['lr_no'],
                'tpt_lr_dt' => date('Y-m-d',strtotime($_POST['lr_date'])),
                'vehicle_no'=> $_POST['vehicle_no'],
                'status' => $_POST['submit-type']==1?1:0,
                'rmda_stat'=>0,
                'addl_amt' => $_POST['other_charges'],
                'curr_id' => $_POST['curr_id'],
                'user_id' => $_SESSION['user_id'],
                'created_date' => date('Y-m-d H:i:s'),
                'remarks' => $_POST['remarks']
            );
                //echo "<pre>";print_r($mrnData);exit;
            $this->db->insert('mrn_receipt', $mrnData) ;
            $mrn_receipt_id = $this->db->insert_id(); 
        }
        
        if ($mrn_receipt_id) {            
            $po_list = array_unique($_POST['po_list']);
            
            foreach($po_list as $val){
               
                foreach($postData['ro_list'][$val] as $key=>$ro_val){
                     $this->db->trans_begin();
                     $mrnPoData = array(
                        'org_id'=>$warehouse_code->org_id,
                        'warehouse_id' => $warehouse_code->wh_id,
                        'wh_id' => $_POST['warehouse_id'],
                        'segment_id'=>$_SESSION['segment_id'],
                        'mrn_rcpt_id' => $mrn_receipt_id,
                        'po_id' => $val,
                        'po_date' => date('Y-m-d'), // to be changed
                        'ro_no' => $ro_val,
                        'ro_date' => date('Y-m-d',strtotime($postData['ro_date_list'][$val][$key])),
                        'use_rented_wh'=>isset($_POST['gp_list'][$ro_val])?1:0,
                        'rcpt_no' => $rcpt_no,

                    );                    

                    $ro_vals = trim(str_replace(' ','',preg_replace('/[`~!@#$%\^&*()+={}[\]\\\\|;:\'",.><?\/]/', '', $ro_val)));
                    $ro_cal = $val."-".$ro_vals;
                    //echo $ro_cal;exit;
                    // echo $ro_vals;exit;

                    if($this->db->insert('mrn_rcpt_src',$mrnPoData)){
                       
                         $mrn_rcpt_src_id = $this->db->insert_id();                          
                         $i = 0;

                        foreach($_POST['bill_qty'][$ro_cal] as $bill_key=>$bill_val){
                            //$i++;
                            if((((int) $_POST['rec_qty'][$ro_cal][$bill_key]!=0) && ($_POST['rec_qty'][$ro_cal][$bill_key]!='')) || (((int) $_POST['rej_qty'][$ro_cal][$bill_key]!=0) && ($_POST['rej_qty'][$ro_cal][$bill_key]!=''))){
                                    
                                 $prod_data = $this->db->from("products")
                                //->join('uom_conv_std', 'purchase_items.id=uom_conv_std.id', 'inner')
                                ->where("warehouse",$_POST['warehouse_id'])
                               // ->where("segment_id",$_SESSION['segment_id'])
                                ->where("id",$_POST['item_list'][$ro_cal][$i])->get()->row();
                                $this->db->trans_begin();
                                $mrnPoItemData = array(
                                    'org_id'=>$warehouse_code->org_id,
                                    'warehouse_id' => $warehouse_code->wh_id,
                                    'wh_id' => $_POST['warehouse_id'],
                                    'segment_id'=>$_SESSION['segment_id'],
                                    'mrn_rcpt_src_id' => $mrn_rcpt_src_id,
                                    'po_id' => $val,
                                    'po_date' => date('Y-m-d'), // to be changed
                                    'item_id' => $_POST['item_list'][$ro_cal][$i],//$bill_key,
                                    'pending_qty' => $_POST['short_qty'][$ro_cal][$bill_key],
                                    'dlv_note_qty' => $_POST['bill_qty'][$ro_cal][$bill_key],
                                    'tot_rcpt_qty' => $_POST['bill_qty'][$ro_cal][$bill_key],
                                    'rej_qty' => $_POST['rej_qty'][$ro_cal][$bill_key],
                                    'rcpt_qty' => $_POST['rec_qty'][$ro_cal][$bill_key],
                                    'code' => $prod_data->code,
                                    'item_uom_id' => $prod_data->unit,
                                    'rcpt_no' => $rcpt_no,
                                    'ro_no' => $ro_val
                                ); 
                                $this->db->insert('mrn_rcpt_item',$mrnPoItemData);
                                $mrn_rcpt_item_id = $this->db->insert_id();                               
                                   // echo "<pre>";
                                    //print_r($mrnPoItemData);exit;  
                                if($mrn_rcpt_item_id){
                                   
                                     $lotRecord = array();
                                     $binRecord = array();
                                    //   $lot_data = array_values($_POST['lot_data']);
                                    if(isset($_POST['lot_data'][$bill_key])){
                                       // echo "asdsa";exit;
                                        foreach($_POST['lot_data'][$bill_key] as $lot_key=>$lot_val){
                                           //echo
                                           $lot_data = json_decode($lot_val,true)[0];
                                          
                                           $mrnPoLotData = array(
                                                
                                                'org_id'=>$warehouse_code->org_id,
                                                'warehouse_id' => $warehouse_code->wh_id,
                                                'wh_id' => $_POST['warehouse_id'],
                                                'segment_id'=>$_SESSION['segment_id'],
                                                'mrn_rcpt_item_id' => $mrn_rcpt_item_id,
                                                'item_id' => $_POST['item_list'][$ro_cal][$i],
                                                'lot_no' => $lot_data['lot_no'],
                                                'lot_qty' => $lot_data['lot_qty'],
                                                'batch_no' => $lot_data['batch_no'],
                                                'mfg_dt' => $lot_data['mfg_date'],
                                                'expiry_dt' => $lot_data['exp_date'],
                                                'code' => $prod_data->code,
                                                'itm_uom_id' => $prod_data->unit,
                                                'rcpt_no' => $rcpt_no,
                                                'ro_no' => $ro_val     
                                            ); 

                                           if($this->db->insert('mrn_rcpt_item_lot',$mrnPoLotData)){
                                                
                                                $lot_id = $this->db->insert_id();
                                                
                                                $lotRecord[$lot_data['lot_no']] = $mrnPoLotData;


                                            foreach($_POST['bin_data'][$lot_data['lot_no']] as $bin_key=>$bin_val){
                                                //foreach($_POST['bin_data'][$ro_val][$bill_key] as $bin_key=>$bin_val){
                                                    $bin_data = json_decode($bin_val,true)[0];
                                                    $mrnPoBinData = array(
                                                        'org_id'=>$warehouse_code->org_id,
                                                        'warehouse_id' => $warehouse_code->wh_id,
                                                        'segment_id'=>$_SESSION['segment_id'],
                                                        'wh_id' => $_POST['warehouse_id'],
                                                        'lot_id' => $lot_id,
                                                        'item_id' => $_POST['item_list'][$ro_cal][$i],
                                                        'bin_qty' => $bin_data['bin_qty'],
                                                        'bin_id' => $bin_data['bin_id'],
                                                        'code' => $prod_data->code,
                                                        'itm_uom_id' => $prod_data->unit,
                                                        'rcpt_no' => $rcpt_no,
                                                        'ro_no' => $ro_val,
                                                        'lot_no'=> $lot_data['lot_no']
                                                                     
                                                    );
                                                    $binId = $this->db->insert('mrn_rcpt_lot_bin',$mrnPoBinData);

                                                        if($binId){  
                                                            $binRecord[$lot_data['lot_no']][] = $mrnPoBinData;                        

                                                          //  echo "counter".$count;
                                                            $count++;
                                                        }
                                                    }
                                                }                                          
                                            }
                                        }

                                        if ($this->db->trans_status() === FALSE)
                                        {
                                            $this->db->trans_rollback();
                                            $this->session->set_flashdata('error','Error in saving data. Please try again!!' );
                                            redirect('mrn/add');
                                        }
                                        else
                                        {
                                            $this->db->trans_commit();
                                        }

                                        if($_POST['submit-type']==1){   
                                          $this->updateStock($_POST['item_list'][$ro_cal][$i],$_POST['rec_qty'][$ro_cal][$bill_key], $ro_val, $val, $_POST['rec_qty'][$ro_cal][$bill_key], $mrnPoItemData, $lotRecord, $binRecord, $poData->itm_uom_desc,$_POST['rej_qty'][$ro_cal][$bill_key]);                                           
                                            $r = $this->updateItemStock($_POST['item_list'][$ro_cal][$i],$_POST['rec_qty'][$ro_cal][$bill_key], $ro_val, $val, $_POST['rec_qty'][$ro_cal][$bill_key], $mrnPoItemData, $_POST['warehouse_id']);
                                             

                                        }else{
                                            $this->updatePoStatus($val, $ro_val, $_POST['item_list'][$ro_cal][$i]);

                                        }


                                    }           // lot ends  
                                    $i++;             
                                }
                            }
                        }             
                    }
                }

            }
            
        }
     //   exit;
        
        if($r=='e')
            return $mrn_receipt_id;
        else
            return 0;
    }

    public function editMrnData($id,$postData){
        if(isset($id) && (isset($postData) && !empty($postData))){
           
            $count = 0;

            /*if(isset($postData['mrn_bin_data']) && (!isset($postData['mrn_lot_data']))){
                foreach ($postData['mrn_bin_data'][''] as $key => $value) {
                    # code...
                }

            }else{*/
                $mrnData = array(
                    'supplier_id'=> $_POST['supplier_id'],
                    'dn_no' => $_POST['del_note_no'],
                    'dn_dt'  => date('Y-m-d',strtotime($_POST['del_note_date'])),
                    'tp_id' => $_POST['lr_name'],
                    'tpt_lr_no' => $_POST['lr_no'],
                    'tpt_lr_dt' => date('Y-m-d',strtotime($_POST['lr_date'])),
                    'vehicle_no'=> $_POST['vehicle_no'],
                    'addl_amt' => $_POST['other_charges'],               
                    'mod_date' => date('Y-m-d H:i:s'),
                    'remarks' => $_POST['remarks']
                );
               
                $this->db->where("id",$id);
                $this->db->update("mrn_receipt",$mrnData);
                foreach($postData['mrn_list'] as $val){
                  
                    foreach($postData['ro_list'][$val] as $key=>$ro_val){
                        $i = 0;

                       // if(isset($_POST['mrn_lot_data'][$bill_key])){
                        foreach($_POST['mrn_bill_qty'][$ro_val] as $bill_key=>$bill_val){

                                    //$i++;
                            if(((int) $_POST['mrn_rec_qty'][$ro_val][$bill_key]!=0) && ($_POST['mrn_rec_qty'][$ro_val][$bill_key]!='')){

                                $this->db->where('id',$bill_key);
                                $poDetails = $this->db->from('mrn_rcpt_item')->get()->row();
                                $po_id = $poDetails->po_id;
                                $this->db->where('id',$bill_key);
                                $mrnPoItemData = array(
                                    'pending_qty' => $_POST['mrn_short_qty'][$ro_val][$bill_key],
                                    'dlv_note_qty' => $_POST['mrn_bill_qty'][$ro_val][$bill_key],
                                    'tot_rcpt_qty' => $_POST['mrn_bill_qty'][$ro_val][$bill_key],
                                    'rej_qty' => $_POST['mrn_rej_qty'][$ro_val][$bill_key],
                                    'rcpt_qty' => $_POST['mrn_rec_qty'][$ro_val][$bill_key]
                                );
                                
                                if($this->db->update('mrn_rcpt_item',$mrnPoItemData)){
                                    
                                    $mrn_rcpt_item_id = $bill_key;
                                    
                                    if(isset($postData['mrn_lot_data'][$bill_key])){

                                    //   $lot_data = array_values($_POST['lot_data']);
                                        foreach($postData['mrn_lot_data'][$bill_key] as $lot_key=>$lot_val){                                           
                                           //echo
                                           $lot_data = json_decode($lot_val,true)[0];
                                           // $this->db->where('mrn_rcpt_item_id', $mrn_rcpt_item_id);
                                           // $this->db->delete("mrn_rcpt_item_lot");

                                           $mrnPoLotData = array(
                                                'org_id' => $poData->org_id,
                                                'wh_id' => $_POST['warehouse_id'],
                                                'segment_id'=>$_SESSION['segment_id'],
                                                'mrn_rcpt_item_id' => $mrn_rcpt_item_id,
                                                'item_id' => $_POST['item_list'][$ro_val][$i],
                                                'lot_no' => $lot_data['lot_no'],
                                                'lot_qty' => $lot_data['lot_qty'],
                                                'batch_no' => $lot_data['batch_no'],
                                                'mfg_dt' => $lot_data['mfg_date'],
                                                'expiry_dt' => $lot_data['exp_date']
                                            );

                                           if($this->db->insert('mrn_rcpt_item_lot',$mrnPoLotData)){
                                            
                                            $lot_id = $this->db->insert_id();
                                            
                                            $lotRecord[$lot_data['lot_no']] = $mrnPoLotData;


                                            foreach($_POST['mrn_bin_data'][$lot_data['lot_no']] as $bin_key=>$bin_val){
                                            //     //foreach($_POST['bin_data'][$ro_val][$bill_key] as $bin_key=>$bin_val){
                                                    $bin_data = json_decode($bin_val,true)[0];
                                                    $mrnPoBinData = array(
                                                        'org_id' => $poData->org_id,
                                                        'segment_id'=>$_SESSION['segment_id'],
                                                        'wh_id' => $_POST['warehouse_id'],
                                                        'lot_id' => $lot_id,
                                                        'item_id' => $_POST['item_list'][$ro_val][$i],
                                                        'bin_qty' => $bin_data['bin_qty'],
                                                        'bin_id' => $bin_data['bin_id'],
                                                                     
                                                    );
                                                /*foreach($_POST['bin_data'][$lot_data['lot_no']] as $bin_key=>$bin_val){
                                                    //foreach($_POST['bin_data'][$ro_val][$bill_key] as $bin_key=>$bin_val){
                                                        $bin_data = json_decode($bin_val,true)[0];
                                                        $mrnPoBinData = array(
                                                            'org_id' => $poData->org_id,
                                                            'segment_id'=>$_SESSION['segment_id'],
                                                            'wh_id' => $_POST['warehouse_id'],
                                                            'lot_id' => $lot_id,
                                                            'item_id' => $_POST['item_list'][$ro_vals][$i],
                                                            'bin_qty' => $bin_data['bin_qty'],
                                                            'bin_id' => $bin_data['bin_id'],
                                                                         
                                                        );*/
                                                        $binId = $this->db->insert('mrn_rcpt_lot_bin',$mrnPoBinData);

                                                            if($binId){  
                                                                $binRecord[$lot_data['lot_no']][] = $mrnPoBinData;                                  

                                                              //  echo "counter".$count;
                                                                $count++;
                                                            }
                                                        }
                                                    }  
                                            // if($this->db->insert('mrn_rcpt_item_lot',$mrnPoLotData)){
                                            
                                            //     $lot_id = $this->db->insert_id();
                                            //     // $this->db->where('lot_id', $lot_id);
                                            //     // $this->db->delete("mrn_rcpt_lot_bin");
                                            //     foreach($_POST['mrn_bin_data'][$lot_data['lot_no']] as $bin_key=>$bin_val){
                                            //     //foreach($_POST['bin_data'][$ro_val][$bill_key] as $bin_key=>$bin_val){
                                            //         $bin_data = json_decode($bin_val,true)[0];
                                            //         $mrnPoBinData = array(
                                            //             'org_id' => $poData->org_id,
                                            //             'segment_id'=>$_SESSION['segment_id'],
                                            //             'wh_id' => $_POST['warehouse_id'],
                                            //             'lot_id' => $lot_id,
                                            //             'item_id' => $_POST['item_list'][$ro_val][$i],
                                            //             'bin_qty' => $bin_data['bin_qty'],
                                            //             'bin_id' => $bin_data['bin_id'],
                                                                     
                                            //         );
                                                    
                                            //         $binId = $this->db->insert('mrn_rcpt_lot_bin',$mrnPoBinData);
                                                   
                                            //         if($binId){

                                            //             if($_POST['submit-type']==1){
                                            //                 $this->updateStock($_POST['item_list'][$ro_val][$i],$_POST['mrn_rec_qty'][$ro_val][$_POST['item_list'][$ro_val][$i]], $ro_val, $po_id, $_POST['mrn_rec_qty'][$ro_val][$bill_key], $mrnPoItemData, $mrnPoLotData, $mrnPoBinData, $poData->itm_uom_desc,$_POST['mrn_rej_qty'][$ro_val][$bill_key]);
                                            //                 //  echo "counter".$count;
                                                       
                                            //             }
                                            //             $count++;
                                            //         }
                                            //     }
                                            // }
                                        }        
                                    }else{


                                        $lot_data = $this->db->from('mrn_rcpt_item_lot')
                                        ->where('mrn_rcpt_item_id',$bill_key)->get()->result();

                                        foreach($lot_data as $lot_value){
                                            $mrnPoLotData = array(
                                                'org_id' => $lot_value->org_id,
                                                'wh_id' => $_POST['warehouse_id'],
                                                'segment_id'=>$lot_value->segment_id,
                                                'mrn_rcpt_item_id' => $lot_value->mrn_rcpt_item_id,
                                                'item_id' => $lot_value->item_id,
                                                'lot_no' => $lot_value->lot_no,
                                                'lot_qty' => $lot_value->lot_qty,
                                                'batch_no' => $lot_value->batch_no,
                                                'mfg_dt' => $lot_value->mfg_dt,
                                                'expiry_dt' => $lot_value->expiry_dt,                                            
                                            );
                                            $bin_data = $this->db->from('mrn_rcpt_lot_bin')
                                        ->where('lot_id',$lot_value->id)->get()->result();
                                            foreach($bin_data as $bin_value){
                                                $mrnPoBinData = array(
                                                    'org_id' => $bin_value->org_id,
                                                    'segment_id'=>$_SESSION['segment_id'],
                                                    'wh_id' => $_POST['warehouse_id'],
                                                    'lot_id' => $bin_value->lot_id,
                                                    'item_id' => $bin_value->item_id,
                                                    'bin_qty' => $bin_value->bin_qty,
                                                    'bin_id' => $bin_value->bin_id,
                                                                 
                                                );
                                               /* $this->updateStock($_POST['item_list'][$ro_val][$i],$_POST['mrn_rec_qty'][$ro_val][$bill_key], $ro_val, $po_id, $_POST['mrn_rec_qty'][$ro_val][$bill_key], $mrnPoItemData, $mrnPoLotData, $mrnPoBinData, $poData->itm_uom_desc,$_POST['mrn_rej_qty'][$ro_val][$bill_key]);*/
                                             
                                                $count++;
                                            }
                                        // updatestock for rest of data
                              
                                        }                                        
                                            
                                    }
                                    if($_POST['submit-type']==1){
                                        $this->updateStock($_POST['item_list'][$ro_vals][$i],$_POST['rec_qty'][$ro_vals][$_POST['item_list'][$ro_vals][$i]], $ro_val, $val, $_POST['rec_qty'][$ro_vals][$bill_key], $mrnPoItemData, $lotRecord, $binRecord, $poData->itm_uom_desc,$_POST['rej_qty'][$ro_vals][$bill_key]);                                           
                                            $this->updateItemStock($_POST['item_list'][$ro_vals][$i],$_POST['rec_qty'][$ro_vals][$_POST['item_list'][$ro_val][$i]], $ro_val, $val, $_POST['rec_qty'][$ro_vals][$bill_key], $mrnPoItemData, $_POST['warehouse_id']);

                                        /*$this->updateItemStock($_POST['item_list'][$ro_val][$i],$_POST['mrn_rec_qty'][$ro_val][$bill_key], $ro_val, $po_id, $_POST['mrn_rec_qty'][$ro_val][$bill_key], $mrnPoItemData, $_POST['warehouse_id']);*/
                                    }
                                }           // lot ends  
                                $i++;             
                            }
                        }
                    }
               // }
            }
        }
        if($count>0)
            return 2;
        else
            return 1;
    }


    public function updatePoStatus($po_id, $ro_val, $item_id){
        $status = 2;
        $data = array('status'=>$status); // balance qty to be updated
        $this->db->where(array('itm_id'=>$item_id,'po_id'=>$po_id,'ro_no'=>$ro_val));
        if($this->db->update('drft_po_dlv_schdl',$data))
            return true;
        return false;
    }

     public function updateItemStock($item_id,$stock,$ro_val,$po_id,$rec_qty,$itemData,$warehouse_id){

        $stk_prof_data = $this->db->from("item_stk_profile")
             ->where("segment_id",$_SESSION['segment_id'])
             ->where("warehouse_id",$_POST['warehouse_id'])
             ->where("item_id",$item_id)->get()->row();
        $stk_id = $stk_prof_data->id;
         

        $this->db->where(array('id'=>$stk_id)); // updating stock data
        $total_stock = $stk_prof_data->total_stk + $itemData['rcpt_qty'];
        
        $avl_stock = $stk_prof_data->available_stk + $itemData['rcpt_qty'];
        $rejected_stk = $stk_prof_data->rejected_stk + $itemData['rej_qty'];

        $this->db->where(array('item_id'=>$item_id,'warehouse_id'=>$warehouse_id,'segment_id'=>$_SESSION['segment_id']));    
        $datas = array('total_stk'=>$total_stock,'available_stk'=>$avl_stock,'rejected_stk'=>$rejected_stk,'upd_flg'=>1);
        

        if($this->db->update("item_stk_profile",$datas)){
            return "e";
        }else{
            return "f";
        }
     }
    

    public function updateStock($item_id,$stock,$ro_val,$po_id,$rec_qty,$itemData, $itemLotData, $itemLotBinData,$itmUomDesc,$rej_qty){
        $warehouse_code = $this->site->getWarehouse($_POST['warehouse_id'],$_SESSION['segment_id']);
        //$uomName = $this->getUomName($itmUomDesc);
        $last_bal_detail = $this->getPoItemQty(1,$ro_val,$po_id,$item_id);    
        // echo "<pre>";
        // print_r($last_bal_detail);
        // exit;
        if(count($last_bal_detail)>0){
            $total_qty = $last_bal_detail[0]->dlv_qty;
            if(count($last_bal_detail)==0)
                $total_qty = $itemData['dlv_note_qty'];
            else
                $total_qty = $last_bal_detail[0]->dlv_qty;     
       

            if(count($last_bal_detail)==1)           
                $last_bal_qty = $total_qty;            
            else
                $last_bal_qty = $last_bal_detail[0]->bal_qty;           
            
            // echo "total qty".$total_qty."last ".$last_bal_qty;exit;
            $bal_qty = $last_bal_qty - ($rec_qty+$rej_qty);
            

            if(($bal_qty==0) || ($bal_qty>$total_qty) || ($bal_qty<0))

                $status = 1;
            else
                $status = 2; 

        }else{ 
           $dlvQtyDetail = $this->getPoItemQty(0,$ro_val,$po_id,$item_id);
          /* echo "<pre>";
           print_r($dlvQtyDetail);exit;*/
           $dlvQty = $dlvQtyDetail[0]->dlv_qty;
           
           $this->db->select('SUM(rcpt_qty) AS rcpt_qty,SUM(rej_qty) AS rej_qty')
            ->from('mrn_receipt')
            ->join('mrn_rcpt_src', 'mrn_receipt.id=mrn_rcpt_src.mrn_rcpt_id', 'inner')
            ->join('mrn_rcpt_item', 'mrn_rcpt_src.id=mrn_rcpt_item.mrn_rcpt_src_id', 'inner')
            ->where("mrn_rcpt_item.po_id",$po_id)
            ->where('mrn_rcpt_src.ro_no',$ro_val);           
            $this->db->where('item_id',$item_id);
            $this->db->where('status',1);         
            $mrnItemData = $this->db->group_by('item_id')->get()->row();
            $total_rcpt_qty = 0;
            if(count($mrnItemData)>0){
               $total_rcpt_qty = $mrnItemData->rcpt_qty; 
               $total_rcpt_qty = $total_rcpt_qty+$mrnItemData->rej_qty;
            }

            
          //  echo "rcpt".$dlvQty."rec".$rec_qty."|||".$rej_qty."|||".$total_rcpt_qty;exit;
            if($total_rcpt_qty>0){
                //$total_rcpt_qt = $total_rcpt_qty + $rec_qty;
                $total_rcpt_qt = $total_rcpt_qty;
                $bal_qty =  $dlvQty-($total_rcpt_qt);
            }else{
                $bal_qty =  $dlvQty-($rec_qty+$rej_qty);
            }          
          //  echo "====>".$bal_qty;exit;
            if(($bal_qty==0) || ($bal_qty>$dlvQty) || ($bal_qty<0))
                $status = 1;
            else
                $status = 2;
        }
       
       // echo "bal qty".$bal_qty."status".$status;exit;
        $data = array('bal_qty'=>$bal_qty,'status'=>$status); // balance qty to be updated
        
        $this->db->where(array('itm_id'=>$item_id,'po_id'=>$po_id,'ro_no'=>$ro_val));

        if($this->db->update('drft_po_dlv_schdl',$data)){     // Inserting/Updating stock in stock items   
           //  echo "88888888888888888888888888";exit;
             $stk_prof_data = $this->db->from("item_stk_profile")
             ->join('item_stk_lot','item_stk_profile.id=item_stk_lot.item_stk_id','INNER')
             ->join('item_stk_lot_bin','item_stk_lot.id=item_stk_lot_bin.lot_id','INNER')
             ->where("item_stk_profile.segment_id",$_SESSION['segment_id'])
             ->where("item_stk_profile.warehouse_id",$_POST['warehouse_id'])
             ->where("item_stk_profile.item_id",$item_id)->get()->row();

             $prod_data = $this->db->from("products")
            //->join('uom_conv_std', 'purchase_items.id=uom_conv_std.id', 'inner')
            ->where("warehouse",$_POST['warehouse_id'])
           // ->where("segment_id",$_SESSION['segment_id'])
            ->where("id",$item_id)->get()->row();
            
            if(count($stk_prof_data)>0){ // If stock data exists then update the quantity
                $stk_id = $stk_prof_data->id;
               // echo "stk".$stk_id;
            }else{
                $this->db->where('item_id', $item_id);
                $this->db->delete('item_stk_profile');
                $stockSaveData = array(
                    'org_id'=>$warehouse_code->org_id,
                    'wh_id' => $warehouse_code->wh_id,                  
                    'segment_id'=>$_SESSION['segment_id'],
                    'warehouse_id' => $_POST['warehouse_id'],
                    'fy_id'=>1,
                    'item_id'=>$item_id,
                    'item_uom_id'=>$prod_data->unit,
                    'total_stk' => 0,                   
                    'available_stk' => 0, // to be updated
                    'rejected_stk' => $itemData['rej_qty'],
                    'rework_stk'    => 0,
                    'ordered_stk'   => 0,
                    'purchase_price' => $prod_data->price,
                    'sales_price' => $prod_data->price,                   
                    'created_date' => date("Y-m-d H:i:s"),
                    'create_flg' => 1,
                    'code' => $prod_data->code,                                     
                );
                
                $this->db->insert('item_stk_profile',$stockSaveData);
               
                $stk_id = $this->db->insert_id();

            }
            //echo "=====>".$stk_id;
             
            if($stk_id){
                if((isset($itemLotData)) && (count($itemLotData)>0)){
  
              
                    foreach($itemLotData as $lot_key=>$lotData){
                        $stockLotData = array(
                            'org_id'=>$warehouse_code->org_id,
                            'wh_id' => $warehouse_code->wh_id,
                            'segment_id'=>$_SESSION['segment_id'],
                            'warehouse_id' => $_POST['warehouse_id'],
                            'item_stk_id'=>$stk_id,
                            'item_id'=>$item_id,
                            //'fy_id' => 1,
                            'total_stk' => $lotData['lot_qty'],
                            'lot_no' => $lotData['lot_no'],
                            'itm_uom_id' => $prod_data->unit, // to be updated
                            'batch_no' => $lotData['batch_no'],
                            'mfg_date' => $lotData['mfg_date'],
                            'exp_date' => $lotData['exp_date'],
                            'created_date' => date("Y-m-d H:i:s"),
                            'create_flg' => 1,
                            'code' => $prod_data->code,                                           
                        ); 
                   
                        if($this->db->insert("item_stk_lot",$stockLotData)){
                             
                            $lot_id = $this->db->insert_id();
                            foreach($itemLotBinData[$lot_key] as $binData){
                                $check_exist_lot = $this->db->select("*")->from("item_stk_lot_bin")->where("lot_id",$lot_id)->where("bin_id",$binData['bin_id'])->get()->row();
                                if(count($check_exist_lot)>0){
                                    $bin_qty = $binData['bin_qty']+$check_exist_lot->total_stk;
                                    $this->db->where('lot_id',$lot_id);
                                    $this->db->update('item_stk_lot_bin',array('total_stk'=>$bin_qty));
                                }else{
                                    $stkBinData = array(
                                        'org_id'=>$warehouse_code->org_id,
                                        'wh_id' => $warehouse_code->wh_id,
                                        'segment_id'=>$_SESSION['segment_id'],
                                        'warehouse_id' => $_POST['warehouse_id'],
                                        'lot_id' => $lot_id,
                                        'bin_id' => $binData['bin_id'],
                                        'item_id' => $item_id,
                                        'itm_uom_id' => $prod_data->unit,
                                        'total_stk' => $binData['bin_qty'], 
                                        'created_date' => date("Y-m-d H:i:s"),
                                        'create_flg' => 1,
                                        'code' => $prod_data->code,
                                        'lot_no'=> $lotData['lot_no']                                                     
                                    );

                                    $this->db->insert("item_stk_lot_bin",$stkBinData);
                                }
                                             
                            }
                        }
                    }
                }

                
            }
            // $this->db->where('id',$item_id);
            $this->db->select("quantity");
            $prod_quant = $this->db->from("products")->where('id',$item_id)->get()->row();        

            $quant = $prod_quant->quantity + $stock;
            if($this->db->update('products',array('quantity'=>$quant))){               
              //  $this->updateStockData($item_id) ;     
                $this->db->where('product_id',$item_id);
                $this->db->update('purchase_items',array('quantity'=>$quant));                
            }

            return 1;
        }
        return 0;      
    }
    public function setPoFlag($poList,$submit_type,$mrn_id=0){
        
        if($submit_type==1 && ($mrn_id!=0)){

            $mrnStatus = array('status'=>1);
            $this->db->where('id',$mrn_id);
            $this->db->update('mrn_receipt',$mrnStatus);

        }
        foreach($poList as $val){
            $pendingPo = $this->db->from("drft_po_dlv_schdl")
            ->where('po_id',$val)           
             ->where_in("status",array(0,2))
            ->get()->result();
            if(count($pendingPo)>0){
                $status = 217;
            }else{
                $status = 218;
            }
            $this->db->where('id',$val);
            $this->db->update('drft_po',array('po_status'=>$status));
        }
        return true;
    }

    public function getMrnDetails($id){
        $this->db->from('mrn_receipt')
        ->join('mrn_rcpt_src', 'mrn_receipt.id=mrn_rcpt_src.mrn_rcpt_id', 'inner')
        ->join('mrn_rcpt_item', 'mrn_rcpt_src.id=mrn_rcpt_item.mrn_rcpt_src_id', 'inner')
        ->join('products', 'mrn_rcpt_item.item_id=products.id', 'inner')
        ->join('drft_po', 'mrn_rcpt_src.po_id=drft_po.id', 'inner')
        //->where("ro_dt >=", 'CURDATE()', FALSE)
        ->where('mrn_receipt.id',$id)
        ->order_by("mrn_rcpt_src.po_id",ASC);
        //->group_by('po_id');
        $po_detail = $this->db->get()->result();
        $tmp_po_id = $po_detail[0]->po_id; 
        $data = array();
        foreach($po_detail as $row){
            if($row->po_id!=$tmp_po_id){
               // echo $row->po_id;exit;
                $data[$row->po_id][] = $row;
                $tmp_po_id = $row->po_id;
            }else{
               // echo $row->po_id;exit;
                $data[$tmp_po_id][] = $row;
            }
        }   
        return array_values($data);        
    }
    public function getMrnData($id){
        $data = $this->db->from('mrn_receipt')
        ->join('mrn_rcpt_src', 'mrn_receipt.id=mrn_rcpt_src.mrn_rcpt_id', 'inner')
        ->join('mrn_rcpt_item', 'mrn_rcpt_src.id=mrn_rcpt_item.mrn_rcpt_src_id', 'inner')
        ->join('sma_mrn_rcpt_item_lot', 'mrn_rcpt_item.id=sma_mrn_rcpt_item_lot.mrn_rcpt_item_id', 'inner')
        ->join('sma_mrn_rcpt_lot_bin', 'sma_mrn_rcpt_item_lot.id=sma_mrn_rcpt_lot_bin.lot_id', 'inner')
        ->where('mrn_receipt.id',17)
        ->get()->result();
        /*echo "<pre>";
        echo $this->db->last_query();*/
        print_r($data);
        exit;

    }

    public function deletebin($bin_id){
        $this->db->where('id', $bin_id);
        $this->db->delete('mrn_rcpt_lot_bin');
        if($this->db->affected_rows())
            return 1;
        else
            return 0;
    }

    public function deletelot($lot_id){
        $this->db->where('lot_no', $lot_id);
        $this->db->delete('mrn_rcpt_item_lot');
        if($this->db->affected_rows())
            return 1;
        else
            return 0;
    }

    public function saveBinData($bin_data){
        $binData = json_decode($bin_data,true)[0];
        $lotDetail = $this->db->select("*")
        ->from('mrn_rcpt_item_lot')
        ->where('lot_no',$binData['bin_lot_no'])->get()->row();
        $mrnPoBinData = array(
            'org_id' => $lotDetail->org_id,
            'segment_id'=>$lotDetail->segment_id,
            'wh_id' => $lotDetail->wh_id,
            'lot_id' => $lotDetail->id,
            'item_id' => $lotDetail->item_id,
            'bin_qty' => $binData['bin_qty'],
            'bin_id' => $binData['bin_id'],
                         
        );
        $binId = $this->db->insert('mrn_rcpt_lot_bin',$mrnPoBinData);
        if($this->db->insert_id())
            return true;
        
        return false;       

    }

    public function deleteMrnData($id=0,$ro_val='',$rcpt_id=0){
       // echo "sjds".$rcpt_id;exit;
        $src_ids = array();
        if((($id) && ($ro_val!='')) || ($rcpt_id)){
          
            if($id && $ro_val){
                $rec = $this->db->select("GROUP_CONCAT(id) as ids, GROUP_CONCAT(item_id) as item_ids")
                ->from("mrn_rcpt_item")
                ->where("mrn_rcpt_src_id",$id)
                ->group_by("mrn_rcpt_src_id")
                ->get()->row();
                //$item_ids = explode(",",$rec->ids);
                $item_ids = explode(",",$rec->ids);        
                $item_list = explode(",", $rec->item_ids);
            }else{
                $src_id = $this->db->select("GROUP_CONCAT(id) as ids, GROUP_CONCAT(ro_no) as ro_no,GROUP_CONCAT(po_id) as po_id")
                ->from("mrn_rcpt_src")
                ->where("mrn_rcpt_id",$id)
                ->group_by("mrn_rcpt_id")
                ->get()->row();
                //$item_ids = explode(",",$rec->ids);
                $src_ids = explode(",",$src_id->ids);     
                $ro_no = explode(",",$src_id->ro_no);
                $po_id = explode(",",$src_id->po_id);
                
                if(count($src_ids)>0){
                    $rec = $this->db->select("GROUP_CONCAT(id) as ids, GROUP_CONCAT(item_id) as item_ids")
                    ->from("mrn_rcpt_item")
                    ->where_in("mrn_rcpt_src_id",$src_ids)
                    ->group_by("mrn_rcpt_src_id")
                    ->get()->row();
                    //$item_ids = explode(",",$rec->ids);
                    $item_ids = explode(",",$rec->ids);        
                    $item_list = explode(",", $rec->item_ids);
                }
            }

            $lots = $this->db->select("GROUP_CONCAT(id) as ids")
            ->from("mrn_rcpt_item_lot")
            ->where_in("mrn_rcpt_item_id",$item_ids)
           // ->group_by("mrn_rcpt_src_id")
            ->get()->row();

            $lot_ids = explode(",",$lots->ids);
            $bin = $this->db->select("GROUP_CONCAT(id) as ids")
            ->from("mrn_rcpt_lot_bin")
            ->where_in("lot_id",$lot_ids)
           // ->group_by("mrn_rcpt_src_id")
            ->get()->row();
                
            $bin_ids = explode(",",$bin->ids);

            if(count($bin_ids)>0){
                $this->db->where_in("id",$bin_ids);
                $this->db->delete("mrn_rcpt_lot_bin");
            }
            if(count($lot_ids)>0){
                $this->db->where_in("id",$lot_ids);
                $this->db->delete("mrn_rcpt_item_lot");
            }
            if(count($item_ids)>0){
                $this->db->where_in("id",$item_ids);
                $this->db->delete("mrn_rcpt_item");
            }
            if(count($src_ids)>0){
                $this->db->where_in("id",$src_ids);
                $this->db->delete("mrn_rcpt_src");
            }

            if($id){
                $this->db->where("id",$id);
                $this->db->delete("mrn_rcpt_src");
            }else{
                $this->db->where("id",$rcpt_id);
                $this->db->delete("mrn_receipt");                
            }
            if($this->db->affected_rows()){
                if($ro_val!=''){
                    foreach($item_list as $row){
                        $this->db->where(array('itm_id'=>$row,'ro_no'=>$ro_val));
                        $data = array('status'=>0);
                        $this->db->update("drft_po_dlv_schdl",$data);    
                    }
                }else{
                    foreach($ro_no as $kk=>$row){
                        $this->db->where(array('po_id'=>$po_id[$kk],'ro_no'=>$row));
                        $data = array('status'=>0);
                        $this->db->update("drft_po_dlv_schdl",$data);    
                    }
                }
                return 1;
            }
            else{
                return 0;
            }
        }

        
    }

    //added by vikas singh to get PO list.
    
     function getWarehouseListbyGatepassType($supplier_id) {
        
        if($supplier_id > 0){
            $this->db->select('drft_po.po_no,drft_po.date');
            $this->db->from('drft_po_dlv_schdl');
            $this->db->join('drft_po','drft_po_dlv_schdl.doc_id = drft_po.doc_id','inner');
            $q = $this->db->get();
           
            return $q->result();   
        }
    }

    function getPoList($sup_id=''){       
        $this->db->select('drft_po.*,drft_po_dlv_schdl.use_rented_wh');
        $this->db->from('drft_po')
        ->join('drft_po_dlv_schdl', 'drft_po_dlv_schdl.po_id=drft_po.id', 'inner')
        //commented by vikas singh as suggested by chomu to list last dates PO as well.
        // ->where("ro_dt", 'CURDATE()', FALSE)
       // ->where_in('wh_id', implode(",",$_SESSION['warehouse_id']))
        ->where('drft_po_dlv_schdl.segment_id',$_SESSION['segment_id'])
        ->where('drft_po.po_status',217)    
        ->where('drft_po_dlv_schdl.status!=',1);
        if($sup_id!='')
            $this->db->where('drft_po.customer_id',$sup_id);
        $this->db->group_by('drft_po_dlv_schdl.po_id');

        $po_detail = $this->db->get()->result();
       
        $po_list = array();
        if(count($po_detail)>0){
            foreach($po_detail as $row){    

                array_push($po_list, $row);           
            }            
        }
        return $po_list;      
    }

    function getItemFromGp($item_id, $dlv_qty, $bal_qty, $ro_no, $tot_recvd_qty=0){
       $remainQty = 0;
        /*echo "item".$item_id."dlv".$dlv_qty."bal".$bal_qty."ro".$ro_no;
        exit;*/
        //SELECT SUM(act_rcpt_qty) FROM `sma_gp_src` as gs inner join sma_gp_item as ei on gs.id = ei.gp_src_id WHERE doc_no='RO00003012' AND item_id=4 group by ei.item_id 
        if($item_id!=''){
            $gpItemData = $this->db->select('SUM(act_rcpt_qty) AS rcpt_qty')
            ->from('gp_src')
            ->join('gp_item', 'gp_src.id=gp_item.gp_src_id', 'inner')
            ->where('doc_no',$ro_no)
            //  if($item_id!='')
            ->where('item_id',$item_id)
            ->group_by('item_id')->get()->row();
            $releaseQty = $gpItemData->rcpt_qty;           
            $alrdyRcvdQty = 0; 
            // echo "<pre>";
            // echo($releaseQty."total".$tot_recvd_qty);
            // exit;
            //echo "bal qty".$bal_qty."dlv qty".$dlv_qty;exit;
            if($tot_recvd_qty!=0){
                $alrdyRcvdQty =  $tot_recvd_qty;
            }else{
                if($bal_qty < $dlv_qty)
                    $alrdyRcvdQty = $dlv_qty - $bal_qty;
            }
            $remainQty = $releaseQty - $alrdyRcvdQty;
           
        }
        return $remainQty;
    }

    function getItemFromMrn($item_id='', $dlv_qty, $bal_qty, $ro_no,$po_id){
     /*  echo "items".$item_id."dlv qty".$dlv_qty."bal qty".$bal_qty."ro no".$ro_no;
       exit;*/
       // echo "item".$dlv_qty."bal".$bal_qty."ro".$ro_no;
        //SELECT SUM(b.rcpt_qty) AS rcpt_qty FROM sma_mrn_receipt as x inner join sma_mrn_rcpt_src as a on x.id=a.`mrn_rcpt_id` inner join sma_mrn_rcpt_item as b on a.id=b.`mrn_rcpt_src_id` where ro_no = 'RO00003012' and item_id = 3 and x.status=0 
        $remainQty = 0;
        $releaseQty = 0;
        if($item_id!=''){

            $this->db->select('SUM(rcpt_qty) AS rcpt_qty,SUM(rej_qty) AS rej_qty')
            ->from('mrn_receipt')
            ->join('mrn_rcpt_src', 'mrn_receipt.id=mrn_rcpt_src.mrn_rcpt_id', 'inner')
            ->join('mrn_rcpt_item', 'mrn_rcpt_src.id=mrn_rcpt_item.mrn_rcpt_src_id', 'inner')
            ->where('mrn_rcpt_src.ro_no',$ro_no)
            ->where('mrn_rcpt_src.po_id',$po_id);
          //  ->where('status!=',1);
            if($item_id!='')
                $this->db->where('item_id',$item_id);
            //->where('status',0)

            $mrnItemData = $this->db->group_by('item_id')->get()->row(); 
        

            if(count($mrnItemData)>0)
                $releaseQty = $mrnItemData->rcpt_qty; 
                $rejQty = $mrnItemData->rej_qty;           
           
            $alrdyRcvdQty = 0; 
            
            //if($dlv_qty<$releaseQty)
            $remainQty = $dlv_qty - ($releaseQty+$rejQty);
            
            //echo "remain".$remainQty."relae".$releaseQty;exit;
        }
        

        return array('remainQty'=>$remainQty,'receivedQty'=>$releaseQty);
    }


    function getItemDetails($po_no, $ro_no){
        $result = $this->getRoDetail($po_no, $ro_no, 0);
        echo "<pre>";
        print_r($result);
        exit;
    }

     public function getPoItemQty($status,$ro_val,$po_id,$item_id){

         $this->db->select('itm_id,bal_qty,tot_qty,dlv_qty')->from('drft_po_dlv_schdl')
        ->where('po_id',$po_id)
        ->where('ro_no',$ro_val)
        ->where('itm_id',$item_id);        
        if($status==0){      
            $stat = '0,2';   
            $this->db->where_in('status',implode(",",$stat));     
        }else {

            $this->db->where('status',$status);
        }        
        $last_bal_detail = $this->db->order_by('id',DESC)->get()->result();
        
        return $last_bal_detail;
     }

     public function getUomName($uomId){
        $uomName = $this->db->select("uom_desc")
        ->from("uom_conv_std")
        ->where("uom_id",$uomId)->get()->row();
        return $uomName->uom_desc;

     }


}
