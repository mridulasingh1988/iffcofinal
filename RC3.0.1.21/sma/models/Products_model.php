<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Products_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAllProducts()
    {
        
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getProductOptions($pid)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
   //AK
    public function adjgetstock($pid)
    {
        $q = $this->db->get_where('item_stk_profile', array('item_id' => $pid));
        $d = 'NOT_IN_STOCK';
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return $d;
    }

    public function warehousedtl($wid)
    {
        $q = $this->db->get_where('warehouses', array('id' => $wid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getProductOptionsWithWH($pid)
    {
        $this->db->select($this->db->dbprefix('product_variants') . '.*, ' . $this->db->dbprefix('warehouses') . '.name as wh_name, ' . $this->db->dbprefix('warehouses') . '.id as warehouse_id, ' . $this->db->dbprefix('warehouses_products_variants') . '.quantity as wh_qty')
            ->join('warehouses_products_variants', 'warehouses_products_variants.option_id=product_variants.id', 'left')
            ->join('warehouses', 'warehouses.id=warehouses_products_variants.warehouse_id', 'left')
            ->group_by(array('' . $this->db->dbprefix('product_variants') . '.id', '' . $this->db->dbprefix('warehouses_products_variants') . '.warehouse_id'))
            ->order_by('product_variants.id');
        $q = $this->db->get_where('product_variants', array('product_variants.product_id' => $pid, 'warehouses_products_variants.quantity !=' => NULL));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getProductComboItems($pid)
    {
        $this->db->select($this->db->dbprefix('products') . '.id as id, ' . $this->db->dbprefix('products') . '.code as code, ' . $this->db->dbprefix('combo_items') . '.quantity as qty, ' . $this->db->dbprefix('products') . '.name as name, ' . $this->db->dbprefix('combo_items') . '.unit_price as price')->join('products', 'products.code=combo_items.item_code', 'left')->group_by('combo_items.id');
        $q = $this->db->get_where('combo_items', array('product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return FALSE;
    }

    public function getProductByID($id)
    {   

        $this->db->select("products.*, item_stk_profile.available_stk as available_stk")
                 ->from("products")
                 ->join("item_stk_profile" , " products.id = item_stk_profile.item_id", "LEFT")
                 ->where("products.id",$id);
                 $q= $this->db->get();
                // echo "<pre>";
          //print_r($q->result()); die;
        //$q = $this->db->get_where('products', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function has_purchase($product_id, $warehouse_id = NULL)
    {
        if($warehouse_id) { $this->db->where('warehouse_id', $warehouse_id); }
        $q = $this->db->get_where('purchase_items', array('product_id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function getProductDetails($id)
    {
        $this->db->select($this->db->dbprefix('products') . '.code, ' . $this->db->dbprefix('products') . '.name, ' . $this->db->dbprefix('categories') . '.code as category_code, cost, price, quantity, alert_quantity')
            ->join('categories', 'categories.id=products.category_id', 'left');
        $q = $this->db->get_where('products', array('products.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductDetail($id)
    {
        $this->db->select($this->db->dbprefix('products') . '.*, ' . $this->db->dbprefix('tax_rates') . '.code as tax_rate_code, ' . $this->db->dbprefix('categories') . '.code as category_code, ' . $this->db->dbprefix('subcategories') . '.code as subcategory_code')
            ->join('tax_rates', 'tax_rates.id=products.tax_rate', 'left')
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->join('subcategories', 'subcategories.id=products.subcategory_id', 'left');
        $q = $this->db->get_where('products', array('products.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductByCategoryID($id)
    {

        $q = $this->db->get_where('products', array('category_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return true;
        }

        return FALSE;
    }

    public function getAllWarehousesWithPQ($product_id)
    {
        // $this->db->select('' . $this->db->dbprefix('warehouses') . '.*, ' . $this->db->dbprefix('warehouses_products') . '.quantity, ' . $this->db->dbprefix('warehouses_products') . '.rack')
        //     ->join('warehouses_products', 'warehouses_products.warehouse_id=warehouses.id', 'left')
        //     ->where('warehouses_products.product_id', $product_id)
        //     ->group_by('warehouses.id');
        // $q = $this->db->get('warehouses');

        $this->db->select('' . $this->db->dbprefix('warehouses') . '.*, ' . $this->db->dbprefix('item_stk_profile') . '.available_stk as quantity')
                 ->from('warehouses')
                 ->join('item_stk_profile','item_stk_profile.warehouse_id=warehouses.id','left')
                 ->where('item_stk_profile.item_id', $product_id);
        $q = $this->db->get();

        //print_r($q->result()); die;
        
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductPhotos($id)
    {
        $q = $this->db->get_where("product_photos", array('product_id' => $id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getProductByCode($code)
    {

        $q = $this->db->get_where('products', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }
    // Add by ankit @ 31/8/2016
    public function addKit($data)
    { 
       $w= "select kit_id from sma_kit order by id DESC limit 1";
       $q= $this->db->query($w);
        if ($q->result() > 0) {
            
                  $row = $q->result();
            }
        $kid= $row[0]->kit_id;
        if($kid<=0 OR $kid==NULL){
            $kid=1;
        }
        else{$kid= $kid+1;}
        if ($data) {
                foreach ($data as $data) {
                    $data['kit_id'] = $kid;
                    $this->db->insert('kit', $data);
                }
                    return true;
                }
             return false;   
            


    }

    public function addProduct($data, $items, $warehouse_qty, $product_attributes, $photos)
    {

        if ($this->db->insert('products', $data)) {
            $product_id = $this->db->insert_id();

            if ($items) {
                foreach ($items as $item) {
                    $item['product_id'] = $product_id;
                    $this->db->insert('combo_items', $item);
                }
            }

            if ($data['type'] == 'combo' || $data['type'] == 'service') {
                $warehouses = $this->site->getAllWarehouses();
                foreach ($warehouses as $warehouse) {
                    $this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse->id, 'quantity' => 0));
                }
            }

            $tax_rate = $this->site->getTaxRateByID($data['tax_rate']);

            if ($warehouse_qty && !empty($warehouse_qty)) {
                foreach ($warehouse_qty as $wh_qty) {
                    if (isset($wh_qty['quantity']) && ! empty($wh_qty['quantity'])) {
                        $this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $wh_qty['warehouse_id'], 'quantity' => $wh_qty['quantity'], 'rack' => $wh_qty['rack']));

                        if (!$product_attributes) {
                            $tax_rate_id = $tax_rate ? $tax_rate->id : NULL;
                            $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : NULL;
                            $unit_cost = $data['cost'];
                            if ($tax_rate) {
                                if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                                    if ($data['tax_method'] == '0') {
                                        $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                        $net_item_cost = $data['cost'] - $pr_tax_val;
                                        $item_tax = $pr_tax_val * $wh_qty['quantity'];
                                    } else {
                                        $net_item_cost = $data['cost'];
                                        $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                        $unit_cost = $data['cost'] + $pr_tax_val;
                                        $item_tax = $pr_tax_val * $wh_qty['quantity'];
                                    }
                                } else {
                                    $net_item_cost = $data['cost'];
                                    $item_tax = $tax_rate->rate;
                                }
                            } else {
                                $net_item_cost = $data['cost'];
                                $item_tax = 0;
                            }

                            $subtotal = (($net_item_cost * $wh_qty['quantity']) + $item_tax);

                            $item = array(
                                'product_id' => $product_id,
                                'product_code' => $data['code'],
                                'product_name' => $data['name'],
                                'net_unit_cost' => $net_item_cost,
                                'unit_cost' => $unit_cost,
                                'quantity' => $wh_qty['quantity'],
                                'quantity_balance' => $wh_qty['quantity'],
                                'item_tax' => $item_tax,
                                'tax_rate_id' => $tax_rate_id,
                                'tax' => $tax,
                                'subtotal' => $subtotal,
                                'warehouse_id' => $wh_qty['warehouse_id'],
                                'date' => date('Y-m-d'),
                                'status' => 'received',
                            );
                            $this->db->insert('purchase_items', $item);
                            $this->site->syncProductQty($product_id, $wh_qty['warehouse_id']);
                        }
                    }
                }
            }

            if ($product_attributes) {
                foreach ($product_attributes as $pr_attr) {
                    $pr_attr_details = $this->getPrductVariantByPIDandName($product_id, $pr_attr['name']);

                    $pr_attr['product_id'] = $product_id;
                    $variant_warehouse_id = $pr_attr['warehouse_id'];
                    unset($pr_attr['warehouse_id']);
                    if ($pr_attr_details) {
                        $option_id = $pr_attr_details->id;
                    } else {
                        $this->db->insert('product_variants', $pr_attr);
                        $option_id = $this->db->insert_id();
                    }
                    if ($pr_attr['quantity'] != 0) {
                        $this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $variant_warehouse_id, 'quantity' => $pr_attr['quantity']));

                        $tax_rate_id = $tax_rate ? $tax_rate->id : NULL;
                        $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : NULL;
                        $unit_cost = $data['cost'];
                        if ($tax_rate) {
                            if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                                if ($data['tax_method'] == '0') {
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                    $net_item_cost = $data['cost'] - $pr_tax_val;
                                    $item_tax = $pr_tax_val * $pr_attr['quantity'];
                                } else {
                                    $net_item_cost = $data['cost'];
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                    $unit_cost = $data['cost'] + $pr_tax_val;
                                    $item_tax = $pr_tax_val * $pr_attr['quantity'];
                                }
                            } else {
                                $net_item_cost = $data['cost'];
                                $item_tax = $tax_rate->rate;
                            }
                        } else {
                            $net_item_cost = $data['cost'];
                            $item_tax = 0;
                        }

                        $subtotal = (($net_item_cost * $pr_attr['quantity']) + $item_tax);
                        $item = array(
                            'product_id' => $product_id,
                            'product_code' => $data['code'],
                            'product_name' => $data['name'],
                            'net_unit_cost' => $net_item_cost,
                            'unit_cost' => $unit_cost,
                            'quantity' => $pr_attr['quantity'],
                            'option_id' => $option_id,
                            'quantity_balance' => $pr_attr['quantity'],
                            'item_tax' => $item_tax,
                            'tax_rate_id' => $tax_rate_id,
                            'tax' => $tax,
                            'subtotal' => $subtotal,
                            'warehouse_id' => $variant_warehouse_id,
                            'date' => date('Y-m-d'),
                            'status' => 'received',
                        );
                        $this->db->insert('purchase_items', $item);

                    }

                    $this->site->syncVariantQty($option_id, $variant_warehouse_id);
                }
            }

            if ($photos) {
                foreach ($photos as $photo) {
                    $this->db->insert('product_photos', array('product_id' => $product_id, 'photo' => $photo));
                }
            }

            return true;
        }

        return false;

    }

    public function getPrductVariantByPIDandName($product_id, $name)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $product_id, 'name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function addAjaxProduct($data)
    {

        if ($this->db->insert('products', $data)) {
            $product_id = $this->db->insert_id();
            return $this->getProductByID($product_id);
        }

        return false;

    }

    public function add_products($products = array())
    {
        if (!empty($products)) {
            foreach ($products as $product) {
                $variants = explode('|', $product['variants']);
                unset($product['variants']);
                if ($this->db->insert('products', $product)) {
                    $product_id = $this->db->insert_id();
                    foreach ($variants as $variant) {
                        if ($variant && trim($variant) != '') {
                            $vat = array('product_id' => $product_id, 'name' => trim($variant));
                            $this->db->insert('product_variants', $vat);
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    public function getProductNames($term, $limit = 5)
    {
        $this->db->select('' . $this->db->dbprefix('products') . '.id, code, ' . $this->db->dbprefix('products') . '.name as name, ' . $this->db->dbprefix('products') . '.price as price, ' . $this->db->dbprefix('product_variants') . '.name as vname')
            ->where("type != 'combo' AND "
                . "(" . $this->db->dbprefix('products') . ".name LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%' OR
                concat(" . $this->db->dbprefix('products') . ".name, ' (', code, ')') LIKE '%" . $term . "%')");
        $this->db->join('product_variants', 'product_variants.product_id=products.id', 'left')
            ->where('' . $this->db->dbprefix('product_variants') . '.name', NULL)
            ->group_by('products.id')->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function updateProduct($id, $data, $items, $warehouse_qty, $product_attributes, $photos, $update_variants)
    {
        if ($this->db->update('products', $data, array('id' => $id))) {

            if ($items) {
                $this->db->delete('combo_items', array('product_id' => $id));
                foreach ($items as $item) {
                    $item['product_id'] = $id;
                    $this->db->insert('combo_items', $item);
                }
            }

            $tax_rate = $this->site->getTaxRateByID($data['tax_rate']);

            if ($warehouse_qty && !empty($warehouse_qty)) {
                foreach ($warehouse_qty as $wh_qty) {
                    $this->db->update('warehouses_products', array('rack' => $wh_qty['rack']), array('product_id' => $id, 'warehouse_id' => $wh_qty['warehouse_id']));
                }
            }

            if ($update_variants) {
                $this->db->update_batch('product_variants', $update_variants, 'id');
            }

            if ($photos) {
                foreach ($photos as $photo) {
                    $this->db->insert('product_photos', array('product_id' => $id, 'photo' => $photo));
                }
            }

            if ($product_attributes) {
                foreach ($product_attributes as $pr_attr) {

                    $pr_attr['product_id'] = $id;
                    $variant_warehouse_id = $pr_attr['warehouse_id'];
                    unset($pr_attr['warehouse_id']);
                    $this->db->insert('product_variants', $pr_attr);
                    $option_id = $this->db->insert_id();

                    if ($pr_attr['quantity'] != 0) {
                        $this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $id, 'warehouse_id' => $variant_warehouse_id, 'quantity' => $pr_attr['quantity']));

                        $tax_rate_id = $tax_rate ? $tax_rate->id : NULL;
                        $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : NULL;
                        $unit_cost = $data['cost'];
                        if ($tax_rate) {
                            if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                                if ($data['tax_method'] == '0') {
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                    $net_item_cost = $data['cost'] - $pr_tax_val;
                                    $item_tax = $pr_tax_val * $pr_attr['quantity'];
                                } else {
                                    $net_item_cost = $data['cost'];
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                    $unit_cost = $data['cost'] + $pr_tax_val;
                                    $item_tax = $pr_tax_val * $pr_attr['quantity'];
                                }
                            } else {
                                $net_item_cost = $data['cost'];
                                $item_tax = $tax_rate->rate;
                            }
                        } else {
                            $net_item_cost = $data['cost'];
                            $item_tax = 0;
                        }

                        $subtotal = (($net_item_cost * $pr_attr['quantity']) + $item_tax);
                        $item = array(
                            'product_id' => $id,
                            'product_code' => $data['code'],
                            'product_name' => $data['name'],
                            'net_unit_cost' => $net_item_cost,
                            'unit_cost' => $unit_cost,
                            'quantity' => $pr_attr['quantity'],
                            'option_id' => $option_id,
                            'quantity_balance' => $pr_attr['quantity'],
                            'item_tax' => $item_tax,
                            'tax_rate_id' => $tax_rate_id,
                            'tax' => $tax,
                            'subtotal' => $subtotal,
                            'warehouse_id' => $variant_warehouse_id,
                            'date' => date('Y-m-d'),
                            'status' => 'received',
                        );
                        $this->db->insert('purchase_items', $item);

                    }
                }
            }

            $this->site->syncQuantity(NULL, NULL, NULL, $id);
            return true;
        } else {
            return false;
        }
    }

    public function updateProductOptionQuantity($option_id, $warehouse_id, $quantity, $product_id)
    {
        if ($option = $this->getProductWarehouseOptionQty($option_id, $warehouse_id)) {
            if ($this->db->update('warehouses_products_variants', array('quantity' => $quantity), array('option_id' => $option_id, 'warehouse_id' => $warehouse_id))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return TRUE;
            }
        } else {
            if ($this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $quantity))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return TRUE;
            }
        }
        return FALSE;
    }

    public function getPurchasedItemDetails($product_id, $warehouse_id, $option_id = NULL)
    {
        $q = $this->db->get_where('purchase_items', array('product_id' => $product_id, 'option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPurchasedItemDetailsWithOption($product_id, $warehouse_id, $option_id)
    {
        $q = $this->db->get_where('purchase_items', array('product_id' => $product_id, 'purchase_id' => NULL, 'transfer_id' => NULL, 'warehouse_id' => $warehouse_id, 'option_id' => $option_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updatePrice($data = array())
    {

        if ($this->db->update_batch('products', $data, 'code')) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteProduct($id)
    {
        if ($this->db->delete('products', array('id' => $id)) && $this->db->delete('warehouses_products', array('product_id' => $id)) && $this->db->delete('warehouses_products_variants', array('product_id' => $id))) {
            return true;
        }
        return FALSE;
    }


    public function totalCategoryProducts($category_id)
    {
        $q = $this->db->get_where('products', array('category_id' => $category_id));

        return $q->num_rows();
    }

    public function getSubcategoryByID($id)
    {
        $q = $this->db->get_where('subcategories', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getCategoryByCode($code)
    {
        $q = $this->db->get_where('categories', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getSubcategoryByCode($code)
    {

        $q = $this->db->get_where('subcategories', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getTaxRateByName($name)
    {
        $q = $this->db->get_where('tax_rates', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getSubCategories()
    {
        $this->db->select('id as id, name as text');
        $q = $this->db->get("subcategories");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return FALSE;
    }

    public function getSubCategoriesForCategoryID($category_id)
    {
        $this->db->select('id as id, name as text');
        $q = $this->db->get_where("subcategories", array('category_id' => $category_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return FALSE;
    }

    public function getSubCategoriesByCategoryID($category_id)
    {
        $q = $this->db->get_where("subcategories", array('category_id' => $category_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return FALSE;
    }

    public function getAdjustmentByID($id)
    {
        $q = $this->db->get_where('adjustments', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function syncAdjustment($data = array())
    {
        if(! empty($data)) {

            if ($purchase_item = $this->getPurchasedItemDetails($data['product_id'], $data['warehouse_id'], $data['option_id'])) {
                $quantity_balance = $data['type'] == 'subtraction' ? $purchase_item->quantity_balance - $data['quantity'] : $purchase_item->quantity_balance + $data['quantity'];
                $this->db->update('purchase_items', array('quantity_balance' => $quantity_balance), array('id' => $purchase_item->id));
            } else {
                $pr = $this->site->getProductByID($data['product_id']);
                $item = array(
                    'product_id' => $data['product_id'],
                    'product_code' => $pr->code,
                    'product_name' => $pr->name,
                    'net_unit_cost' => 0,
                    'unit_cost' => 0,
                    'quantity' => 0,
                    'option_id' => $data['option_id'],
                    'quantity_balance' => $data['type'] == 'subtraction' ? (0 - $data['quantity']) : $data['quantity'],
                    'item_tax' => 0,
                    'tax_rate_id' => 0,
                    'tax' => '',
                    'subtotal' => 0,
                    'warehouse_id' => $data['warehouse_id'],
                    'date' => date('Y-m-d'),
                    'status' => 'received',
                );
                $this->db->insert('purchase_items', $item);
            }

            $this->site->syncProductQty($data['product_id'], $data['warehouse_id']);
            if ($data['option_id']) {
                $this->site->syncVariantQty($data['option_id'], $data['warehouse_id'], $data['product_id']);
            }
        }
    }

    public function reverseAdjustment($id)
    {
        if ($adjustment = $this->getAdjustmentByID($id)) {

            if ($purchase_item = $this->getPurchasedItemDetails($adjustment->product_id, $adjustment->warehouse_id, $adjustment->option_id)) {
                $quantity_balance = $adjustment->type == 'subtraction' ? $purchase_item->quantity_balance + $adjustment->quantity : $purchase_item->quantity_balance - $adjustment->quantity;
                $this->db->update('purchase_items', array('quantity_balance' => $quantity_balance), array('id' => $purchase_item->id));
            }

            $this->site->syncProductQty($adjustment->product_id, $adjustment->warehouse_id);
            if ($adjustment->option_id) {
                $this->site->syncVariantQty($adjustment->option_id, $adjustment->warehouse_id, $adjustment->product_id);
            }
        }
    }

    public function addAdjustment($data)
    {
        if ($this->db->insert('adjustments', $data)) {
            $this->syncAdjustment($data);
            return true;
        }
        return false;
    }
    // AK
     public function addstockadj($data)
    {  
       if($data['unit']=='')
       {
         $w1 = "SELECT * FROM sma_products WHERE id = ".$data['product_id']." ";
         $ap1 = $this->db->query($w1);
         $q = $ap1->result(); 
         $data['unit'] = $q[0]->unit;
       }

       //echo "<pre>"; print_r($data); echo "<br/>"; die;
        if (!empty($data)) {
        $w1= "DELETE n1 FROM sma_item_stk_profile n1, sma_item_stk_profile n2 WHERE n1.id > n2.id AND n1.item_id = n2.item_id";
        $w2 ="DELETE n1 FROM sma_item_stk_lot n1, sma_item_stk_lot n2 WHERE n1.id > n2.id AND n1.lot_no = n2.lot_no AND n1.code = n2.code";
        $w3 = "DELETE n1 FROM sma_item_stk_lot_bin n1, sma_item_stk_lot_bin n2 WHERE n1.id > n2.id AND n1.lot_id = n2.lot_id AND n1.total_stk = n2.total_stk AND n1.bin_id = n2.bin_id";
        $this->db->query($w1);
        $this->db->query($w2);
        $this->db->query($w3);
            $adjdata = array(); $adjitm = array(); $adjlot = array(); $adjbin = array(); $updstkbin = array(); $stkitm = array(); $stklot = array(); $stkbin = array();
           // echo "<pre>"; print_r($data); echo "<br/>"; die;
            $adjdata = array(
                   'org_id' => $data['org_id'],
                   'wh_id' => $data['wh_id'],
                   'doc_id' => $data['adj_no'],
                   'doc_dt' => $data['date'],
                   'prj_id' => $data['prj_id'],
                   'note' => $data['note']
                );
           // echo "<pre>";print_r($adjdata); echo "<br/>"; //die;
             if($data['type'] == 'subtraction' && $data['stock'] >=  $data['qty'])
               {
                    $adjitm = array(
                         'org_id' => $data['org_id'],
                         'wh_id' => $data['wh_id'],
                         'doc_id' => $data['adj_no'],
                         'itm_id' => $data['code'],
                         'itm_uom' => $data['unit'],
                         'qty' => $data['quantity'],
                         'type' => $data['type']
                      );
                    //echo "<pre>";print_r($adjitm); echo "<br/>";
                   
                    $adjlotdata = $this->get_adjlot_info($data['org_id'],$data['wh_id'],$data['code'],$data['quantity']);
                    if(!empty($adjlotdata)){ 
                            $adjbindata = array();
                            foreach ($adjlotdata as $key) { 
                                             $adjlot[] = array(
                                                    'org_id' => $data['org_id'],
                                                    'wh_id' => $data['wh_id'],
                                                    'doc_id' => $data['adj_no'],
                                                    'lot_no' => $key->lot_no,
                                                    'itm_id' => $data['code'],
                                                    'itm_uom' => $data['unit'],
                                                    'qty' => $key->total_stk,
                                                    'type' => $data['type']
                                                 );

                                        $adjbindata1[] = $this->get_adjbin_info($data['org_id'],$data['wh_id'],$data['code'],$key->id,$key->total_stk);
                                         } // End of Foreach loop of $adjlot

                                       foreach ($adjbindata1 as $key => $value) {
                                                 foreach ($value as $key1 => $value1) {
                                                          $adjbindata[] = $value1;
                                                 }
                                           }

                                foreach ($adjbindata as $key) {
                                               
                                               $adjbin[] = array(
                                                    'org_id' => $data['org_id'],
                                                    'wh_id' => $data['wh_id'],
                                                    'doc_id' => $data['adj_no'],
                                                    'lot_no' => $key->lot_no,
                                                    'bin_id' => $this->getbindlt($key->bin_id),
                                                    'itm_id' => $data['code'],
                                                    'itm_uom' => $data['unit'],
                                                    'qty' => $key->total_stk,
                                                    'type' => $data['type']
                                                 );

                                               $updstkbin[] = array(
                                                    'org_id' => $data['org_id'],
                                                    'wh_id' => $data['wh_id'],
                                                    'lot_id' => $key->lot_id,
                                                    'lot_no' => $key->lot_no,
                                                    'bin_id' => $key->bin_id,
                                                    'itm_id' => $data['code'],
                                                    'qty' => $key->total_stk
                                                    
                                                 );
                                           } // End adjbindata foreach loop

                                 $this->db->insert('stk_adjt',$adjdata);
                                 $this->db->insert('stk_adjt_itm',$adjitm);
                                 $this->db->insert_batch('stk_adjt_lot',$adjlot);
                                 $this->db->insert_batch('stk_adjt_bin',$adjbin);   

                                    
                                    if(!empty($adjitm))
                                       {
                                          $this->update_adjstk($data['org_id'],$data['wh_id'],$data['code'],$data['quantity']);
                                       } // end adjitm if                
                                    if(!empty($adjlot))
                                       {
                                         $this->update_adjstklot($adjlot);
                                       } // end adjlot if 
                                      if(!empty($updstkbin))
                                       {
                                          $this->update_adjstkbin($updstkbin);
                                       } // end adjbin if 
                                    
   
                                    return true;
                                    } // End of $adjlot if
                    //echo "<pre>";print_r($adjlot); die;


               } // end substraction condition if

           if($data['type'] == 'addition')
            {
                //echo "unit=> ".$data['unit']."<br>";
                //$lt= $this->generateLotNo($data['segment_id']);
                // $bd = $this->getbin($data['org_id'],$data['wh_id']);
                //echo "<pre>";print_r($data); echo "<br/>"; die;
                //echo "<pre>";print_r($adjdata); echo "<br/>"; //die;
                $lt = $this->get_adjlot_info_in($data['org_id'],$data['wh_id'],$data['code']);// print_r($lt);die;
               if(!empty($lt))
               {
                  $bds= $this->get_adjbin_info_in($data['org_id'],$data['wh_id'],$data['code'],$lt[0]->id); 
                  $bd = $this->getbindlt($bds[0]->bin_id); //echo $bd; die;
               }
                $adjitm = array(
                         'org_id' => $data['org_id'],
                         'wh_id' => $data['wh_id'],
                         'doc_id' => $data['adj_no'],
                         'itm_id' => $data['code'],
                         'itm_uom' => $data['unit'],
                         'qty' => $data['quantity'],
                         'type' => $data['type']
                      );
                 //echo "<pre>";print_r($adjitm); echo "<br/>"; //die;
                $adjlot = array(
                           'org_id' => $data['org_id'],
                           'wh_id' => $data['wh_id'],
                            'doc_id' => $data['adj_no'],
                            'lot_no' => $lt[0]->lot_no,
                            'itm_id' => $data['code'],
                            'itm_uom' => $data['unit'],
                            'qty' => $data['quantity'],
                            'type' => $data['type']
                       );
                //echo "<pre>";print_r($adjlot); echo "<br/>"; //die;
                $adjbin = array(
                          'org_id' => $data['org_id'],
                          'wh_id' => $data['wh_id'],
                          'doc_id' => $data['adj_no'],
                          'lot_no' => $lt[0]->lot_no,
                          'bin_id' => $bd,
                          'itm_id' => $data['code'],
                          'itm_uom' => $data['unit'],
                          'qty' => $data['quantity'],
                          'type' => $data['type']
                        );
                  //echo "<pre>";print_r($adjbin); echo "<br/>"; die;
                // --------------------------------------------------------------------------------

                  $w = "SELECT * FROM sma_item_stk_profile WHERE org_id = '".$data['org_id']."' AND wh_id = '".$data['wh_id']."' AND code = '".$data['code']."' ";
                  $ap = $this->db->query($w);
                  $res= $ap->num_rows(); 
                  if($res >=1)
                  {
                       // echo "products entry in stk table ";  die;
                    if($lt[0]->id > 0 && !empty($bd))
                    { 
                        
                    $this->db->insert('stk_adjt',$adjdata);
                                 $this->db->insert('stk_adjt_itm',$adjitm);
                                 $this->db->insert('stk_adjt_lot',$adjlot);
                                 $this->db->insert('stk_adjt_bin',$adjbin);

                              if(!empty($adjitm))
                                       {
                                          $this->update_adjstk_in($data['org_id'],$data['wh_id'],$data['code'],$data['quantity']);
                                       } // end adjitm if                
                                    if(!empty($adjlot))
                                       {
                                         $this->update_adjstklot_in($data['org_id'],$data['wh_id'],$data['code'],$data['quantity'],$lt[0]->id);
                                       } // end adjlot if 
                                      if(!empty($adjbin))
                                       {
                                          $this->update_adjstkbin_in($data['org_id'],$data['wh_id'],$data['code'],$data['quantity'],$lt[0]->id);
                                       } // end adjbin if 
                                    
   
                                    return true;   
                    } // end if condition of $lt[0]->lot_no $$ $bd

                    return false;
                       /* $w1 = "SELECT * FROM sma_products WHERE id = ".$data['product_id']." ";
                            $ap1 = $this->db->query($w1);
                            $q = $ap1->result(); 
                        $this->db->insert('stk_adjt',$adjdata);
                                 $this->db->insert('stk_adjt_itm',$adjitm);
                                 $this->db->insert('stk_adjt_lot',$adjlot);
                                 $this->db->insert('stk_adjt_bin',$adjbin);
                    $ups = $this->updstkitm($data['org_id'],$data['wh_id'],$data['code'],$data['quantity']);
                    if($ups)
                    {
                           $stklot = array(
                                                'org_id' => $data['org_id'],
                                                'segment_id' => $data['segment_id'],
                                                 'warehouse_id' => $data['warehouse_id'],
                                                 'wh_id' => $data['wh_id'],
                                                 'lot_no' => $lt,
                                            'item_stk_id' => $this->getstkitm_id($data['org_id'],$data['wh_id'],$data['code']),
                                                 'item_id' => $data['product_id'],
                                                 'batch_no' => 'Batch123',
                                                 'itm_uom_id' => $q[0]->unit,
                                                 'total_stk' => $data['quantity'],
                                                 'rejected_stk' => '0.000000',
                                                 'reworkable_stk' => '0.000000',
                                                 'created_date' => $data['date'],
                                                 'code' => $data['code'],
                                                 'create_flg' => '1'
                                     
                                             );

                                            $this->db->insert('item_stk_lot',$stklot);
                                            $lid = $this->db->insert_id();
                                            if($lid)
                                            {
                                                $stkbin = array(
                                                    'org_id' => $data['org_id'],
                                                    'segment_id' => $data['segment_id'],
                                                    'warehouse_id' => $data['warehouse_id'],
                                                    'wh_id' => $data['wh_id'],
                                                    'item_id' => $data['product_id'],
                                                    'lot_id' => $lid,
                                                    'bin_id' => $bd[0]->id,
                                                    'itm_uom_id' => $q[0]->unit,
                                                    'total_stk' => $data['quantity'],
                                                    'rejected_stk' => '0.000000',
                                                    'reworkable_stk' => '0.000000',
                                                    'created_date' => $data['date'],
                                                    'code' => $data['code'],
                                                    'create_flg' => '1',
                                                    'lot_no' => $lt
                                     
                                                   );
                                                if($this->db->insert('item_stk_lot_bin',$stkbin))
                                                {
                                                    return true;
                                                }
                                              

                                            } // end lid if


                    }  */ // end $ups if     
                  } // END $res if 

                  // else{
                            //echo "products entry not in stk table "; //die;
                            /*$w1 = "SELECT * FROM sma_products WHERE id = ".$data['product_id']." ";
                            $ap1 = $this->db->query($w1);
                            $q = $ap1->result(); 
                            //echo $q[0]->name;
                            $this->db->insert('stk_adjt',$adjdata);
                                 $this->db->insert('stk_adjt_itm',$adjitm);
                                 $this->db->insert('stk_adjt_lot',$adjlot);
                                 $this->db->insert('stk_adjt_bin',$adjbin);

                            $stkitm = array(
                                    'org_id' => $data['org_id'],
                                    'segment_id' => $data['segment_id'],
                                    'warehouse_id' => $data['warehouse_id'],
                                    'wh_id' => $data['wh_id'],
                                    'fy_id' => '1',
                                    'item_id' => $data['product_id'],
                                    'item_uom_id' => $q[0]->unit,
                                    'total_stk' => $data['quantity'],
                                    'available_stk' => $data['quantity'],
                                    'rejected_stk' => '0.000000',
                                    'rework_stk' => '0.000000',
                                    'ordered_stk' => '0.000000',
                                    'purchase_price' => $q[0]->price,
                                    'sales_price' => $q[0]->price,
                                    'created_date' => $data['date'],
                                    'code' => $data['code'],
                                    'create_flg' => '1'
                                     
                                 );
                            if(!empty($stkitm))
                                       {
                                          $this->db->insert('item_stk_profile',$stkitm);
                                          $tid = $this->db->insert_id();
                                          if($tid)
                                          {

                                            $stklot = array(
                                                'org_id' => $data['org_id'],
                                                'segment_id' => $data['segment_id'],
                                                 'warehouse_id' => $data['warehouse_id'],
                                                 'wh_id' => $data['wh_id'],
                                                 'lot_no' => $lt,
                                                 'item_stk_id' => $tid,
                                                 'item_id' => $data['product_id'],
                                                 'batch_no' => 'Batch123',
                                                 'itm_uom_id' => $q[0]->unit,
                                                 'total_stk' => $data['quantity'],
                                                 'rejected_stk' => '0.000000',
                                                 'reworkable_stk' => '0.000000',
                                                 'created_date' => $data['date'],
                                                 'code' => $data['code'],
                                                 'create_flg' => '1'
                                     
                                             );

                                            $this->db->insert('item_stk_lot',$stklot);
                                            $lid = $this->db->insert_id();
                                            if($lid)
                                            {
                                                $stkbin = array(
                                                    'org_id' => $data['org_id'],
                                                    'segment_id' => $data['segment_id'],
                                                    'warehouse_id' => $data['warehouse_id'],
                                                    'wh_id' => $data['wh_id'],
                                                    'item_id' => $data['product_id'],
                                                    'lot_id' => $lid,
                                                    'bin_id' => $bd[0]->id,
                                                    'itm_uom_id' => $q[0]->unit,
                                                    'total_stk' => $data['quantity'],
                                                    'rejected_stk' => '0.000000',
                                                    'reworkable_stk' => '0.000000',
                                                    'created_date' => $data['date'],
                                                    'code' => $data['code'],
                                                    'create_flg' => '1',
                                                    'lot_no' => $lt
                                     
                                                   );
                                                if($this->db->insert('item_stk_lot_bin',$stkbin))
                                                {
                                                    return true;
                                                }
                                              

                                            } // end lid if

                                          } // end tid if
                                       } // end stkitm if  */
                            
                            
                                 
                           
                           
                //  }  // End $res else condition     

 
                  
            } //  end addition condition if

         return false;
            
        } // end main if
        return false;
    }
 
 // AK
 function generateLotNo($sid) {
    $x=6;    
    for ($i = 0; $i<6; $i++) 
    {
    $a .= mt_rand(0,9);
    }
   
   //return $sid."-LOTADJ-IN".$a;
   return $sid."-LOTADJ".$a;
} 

// AK
 function getstkitm_id($org_id,$wid,$code) {

    if($code)
    {
         $w= "SELECT id FROM sma_item_stk_profile WHERE org_id = '".$org_id."' AND wh_id = '".$wid."' AND code = '".$code."' LIMIT 1";
         $a= $this->db->query($w);
         $p = $a->result();
         //print_r($p); die;
         return $p[0]->id;
    }
      return false;
 }

// AK
 function getbin($org_id,$wh_id) {
    $q = "SELECT * FROM sma_bin WHERE org_id = '".$org_id."' AND wh_id = '".$wh_id."' ORDER BY id ASC LIMIT 1";
    $a= $this->db->query($q);
    $p = $a->result();
    if($p)
    {
        return $p;
    }
   
   return false;
} 



    // AK
    public function update_adjstk($org_id,$wid,$code,$qtys)
    {
        //echo "Qty=>".$qtys."=>wid=>".$wid."=>code=>".$code_id;
    $qty =  number_format($qtys, 6);    
    $q = "SELECT available_stk FROM sma_item_stk_profile WHERE org_id = '".$org_id."' AND wh_id = '".$wid."' AND code = '".$code."'";
            if($a= $this->db->query($q))
               {      $ar= $a->result();
                      //print_r($ar); die;
                      $p = $ar[0]->available_stk;
                      $total = $p - $qty;
                      //echo $total; die;
                      $q1 = "UPDATE sma_item_stk_profile SET available_stk = ".$total.", upd_flg='1'  WHERE org_id = '".$org_id."' AND wh_id = '".$wid."' AND code = '".$code."'";
                      $this->db->query($q1);
                }
    }

  // AK
    public function update_adjstklot($ar)
    {
        if(!empty($ar))
       {
        for($i=0;$i<count($ar);$i++)
        {
             $q1 = "UPDATE sma_item_stk_lot SET total_stk = (total_stk - ".$ar[$i]['qty']."),upd_flg='1'  WHERE org_id = '".$ar[$i]['org_id']."' AND wh_id = '".$ar[$i]['wh_id']."' AND code = '".$ar[$i]['itm_id']."' AND lot_no = '".$ar[$i]['lot_no']."' ";
            $this->db->query($q1);

        }
        return true;
     }
     return false;
    } 


 // AK
    public function update_adjstk_in($org_id,$wid,$code,$qtys)
    {
        //echo "Qty=>".$qtys."=>wid=>".$wid."=>code=>".$code_id;
    $qty =  number_format($qtys, 6);    
    $q = "SELECT available_stk,total_stk FROM sma_item_stk_profile WHERE org_id = '".$org_id."' AND wh_id = '".$wid."' AND code = '".$code."'";
            if($a= $this->db->query($q))
               {      $ar= $a->result();
                      //print_r($ar); die;
                      $p = $ar[0]->available_stk;
                      $total = $p + $qty;
                      $p1 = $ar[0]->total_stk;
                      $total123 = $p1 + $qty;
                      //echo $total; die;
                      $q1 = "UPDATE sma_item_stk_profile SET available_stk = ".$total.", total_stk = ".$total123.", upd_flg='1'  WHERE org_id = '".$org_id."' AND wh_id = '".$wid."' AND code = '".$code."'";
                      $this->db->query($q1);
                      return true;
                }
    }

  // AK 
    public function update_adjstklot_in($org_id,$wid,$code,$qtys,$lid)
    {
        if(!empty($lid))
       {

        $qty =  number_format($qtys, 6);    
     $q = "SELECT total_stk FROM sma_item_stk_lot WHERE org_id = '".$org_id."' AND wh_id = '".$wid."' AND code = '".$code."' AND id = ".$lid." ";
            if($a= $this->db->query($q))
               {      $ar= $a->result();
                      $p = $ar[0]->total_stk;
                      $total = $p + $qty;
                      
                      //echo $total; die;
                      $q1 = "UPDATE sma_item_stk_lot SET total_stk = ".$total.", upd_flg='1'  WHERE org_id = '".$org_id."' AND wh_id = '".$wid."' AND code = '".$code."' AND id = ".$lid." ";
                      $this->db->query($q1);
                      return true;
                }
        
      }
     return false;
    }  

  // AK 
    public function update_adjstkbin_in($org_id,$wid,$code,$qtys,$lid)
    {
        if(!empty($lid))
       {

        $qty =  number_format($qtys, 6);    
        $q = "SELECT * FROM sma_item_stk_lot_bin WHERE org_id = '".$org_id."' AND wh_id = '".$wid."' AND code = '".$code."' AND lot_id = ".$lid." ORDER BY id ASC LIMIT 1 ";
            if($a= $this->db->query($q))
               {      $ar= $a->result();
                      //print_r($ar); die;
                      $p = $ar[0]->total_stk;
                      $total = $p + $qty;
                      
                      //echo $total; die;
                    $q1 = "UPDATE sma_item_stk_lot_bin SET total_stk = ".$total.", upd_flg='1'  WHERE id = ".$ar[0]->id." "; 
                      $this->db->query($q1);
                      return true;
                }
        
      }
     return false;
    }   

  // AK
    public function updstkitm($org_id,$wh_id,$code,$qtys)
    {
        if($qtys)
        {
            $qty =  number_format($qtys, 6);
            $q= "UPDATE sma_item_stk_profile SET total_stk= (total_stk + ".$qty."),available_stk= (available_stk + ".$qty."),upd_flg = '1' WHERE org_id= '".$org_id."' AND wh_id= '".$wh_id."' AND code= '".$code."' ";
            $this->db->query($q);
            return true;
        }
        return false;
        
    }   

  // AK
    public function update_adjstkbin($ar)
    {
        if(!empty($ar))
       {
        for($i=0;$i<count($ar);$i++)
        {
            echo $q1 = "UPDATE sma_item_stk_lot_bin SET total_stk = (total_stk - ".$ar[$i]['qty']."),upd_flg='1'  WHERE org_id = '".$ar[$i]['org_id']."' AND wh_id = '".$ar[$i]['wh_id']."' AND code = '".$ar[$i]['itm_id']."' AND lot_no = '".$ar[$i]['lot_no']."' AND lot_id = ".$ar[$i]['lot_id']." AND bin_id = ".$ar[$i]['bin_id']." ";
            $this->db->query($q1);

        }
        return true;
     }
     return false;
    }   

  //AK
    public function get_adjlot_info($org_id,$wid,$code,$qtys)
    {

        $sr = array();
        if(!empty($code) && !empty($qtys))
        {
           $qty =  number_format($qtys, 6);
           $q = "SELECT a.* FROM sma_item_stk_lot a WHERE org_id = '".$org_id."' AND wh_id = '".$wid."' AND code = '".$code."' AND a.total_stk > 0  ORDER BY a.id ASC ";
           if($a= $this->db->query($q))
           {     
               $ar= $a->result();
              // print_r($ar); die;
           }
           if(!empty($ar))
           {
              foreach ($ar as $key) {
                   if($key->total_stk >= $qty)
                     { 
                        $key->total_stk = number_format($qty, 6);
                         $sr[]= $key;
                         //print_r($sr); die;
                         return $sr;
                     }
                  else {
                      $sr[]= $key;
                      $qty = $qty - $key->total_stk;
                   }
                 
              }

           }
           
          return false;
           

       }
    }

//AK
    public function get_adjlot_info_in($org_id,$wid,$code)
    {
         if(!empty($code))
        {
           $q = "SELECT a.* FROM sma_item_stk_lot a WHERE org_id = '".$org_id."' AND wh_id = '".$wid."' AND code = '".$code."'  ORDER BY a.id ASC LIMIT 1 ";
           
           if($a= $this->db->query($q))
           {     
               $ar= $a->result();
              // print_r($ar); die;
               return $ar;
           }
           
          return false;
       }
       return false;
    }    

//AK 

    public function get_adjbin_info($org_id,$wid,$code,$lot_id,$qtys)
    { 
        $sr = array(); //echo $qtys;
        if(!empty($lot_id) && !empty($qtys))
        {
           $qty =  number_format($qtys, 6);
           $q = "SELECT a.* FROM sma_item_stk_lot_bin a WHERE org_id = '".$org_id."' AND wh_id = '".$wid."' AND code = '".$code."' AND lot_id = ".$lot_id." AND a.total_stk > 0  ORDER BY a.id ASC ";
           if($a= $this->db->query($q))
           {     
               $ar= $a->result();
              // print_r($ar); die;
           }
           if(!empty($ar))
           {
              foreach ($ar as $key) {
                   if($key->total_stk >= $qty)
                     { 
                        $key->total_stk = number_format($qty, 6);
                         $sr[]= $key;
                         //print_r($sr); die;
                         return $sr;
                     }
                  else {
                      $sr[]= $key;
                      $qty = $qty - $key->total_stk;
                   }
                 
              }

           }
           
          return false;
           

       }
    }

 //AK 

    public function get_adjbin_info_in($org_id,$wid,$code,$lot_id)
    { 
        if(!empty($lot_id))
        {
           $q = "SELECT a.* FROM sma_item_stk_lot_bin a WHERE org_id = '".$org_id."' AND wh_id = '".$wid."' AND code = '".$code."' AND lot_id = ".$lot_id." ORDER BY a.id ASC LIMIT 1 ";
           if($a= $this->db->query($q))
           {     
               $ar= $a->result();
              // print_r($ar); die;
               return $ar;
           }
           
        }

       return false;
    }   

    // AK

    public function getbindlt($bid)
    {
        if(!empty($bid))
        {
             $q = "SELECT a.bin_id FROM sma_bin a WHERE id = ".$bid." ";
             $a = $this->db->query($q);
             $ar= $a->result();
             return $ar[0]->bin_id;
        }

        return false;
    }


    public function updateAdjustment($id, $data)
    {
        $this->reverseAdjustment($id);
        if ($this->db->update('adjustments', $data, array('id' => $id))) {
            $this->syncAdjustment($data);
            return true;
        }
        return false;
    }

    public function deleteAdjustment($id)
    {
        $this->reverseAdjustment($id);
        if ( $this->db->delete('adjustments', array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function getProductQuantity($product_id, $warehouse)
    {
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse), 1);
        if ($q->num_rows() > 0) {
            return $q->row_array(); //$q->row();
        }
        return FALSE;
    }

    public function addQuantity($product_id, $warehouse_id, $quantity, $rack = NULL)
    {

        if ($this->getProductQuantity($product_id, $warehouse_id)) {
            if ($this->updateQuantity($product_id, $warehouse_id, $quantity, $rack)) {
                return TRUE;
            }
        } else {
            if ($this->insertQuantity($product_id, $warehouse_id, $quantity, $rack)) {
                return TRUE;
            }
        }

        return FALSE;
    }

    public function insertQuantity($product_id, $warehouse_id, $quantity, $rack = NULL)
    {
        if ($this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $quantity, 'rack' => $rack))) {
            $this->site->syncProductQty($product_id, $warehouse_id);
            return true;
        }
        return false;
    }

    public function updateQuantity($product_id, $warehouse_id, $quantity, $rack = NULL)
    {
        $data = $rack ? array('quantity' => $quantity, 'rack' => $rack) : $data = array('quantity' => $quantity);
        if ($this->db->update('warehouses_products', $data, array('product_id' => $product_id, 'warehouse_id' => $warehouse_id))) {
            $this->site->syncProductQty($product_id, $warehouse_id);
            return true;
        }
        return false;
    }

    public function products_count($category_id, $subcategory_id = NULL)
    {
        if ($category_id) {
            $this->db->where('category_id', $category_id);
        }
        if ($subcategory_id) {
            $this->db->where('subcategory_id', $subcategory_id);
        }
        $this->db->from('products');
        return $this->db->count_all_results();
    }

    public function fetch_products($category_id, $limit, $start, $subcategory_id = NULL)
    {

        $this->db->limit($limit, $start);
        if ($category_id) {
            $this->db->where('category_id', $category_id);
        }
        if ($subcategory_id) {
            $this->db->where('subcategory_id', $subcategory_id);
        }
        $this->db->order_by("id", "asc");
        $query = $this->db->get("products");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getProductWarehouseOptionQty($option_id, $warehouse_id)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function syncVariantQty($option_id)
    {
        $wh_pr_vars = $this->getProductWarehouseOptions($option_id);
        $qty = 0;
        foreach ($wh_pr_vars as $row) {
            $qty += $row->quantity;
        }
        if ($this->db->update('product_variants', array('quantity' => $qty), array('id' => $option_id))) {
            return TRUE;
        }
        return FALSE;
    }

    public function getProductWarehouseOptions($option_id)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function setRack($data)
    {
        if ($this->db->update('warehouses_products', array('rack' => $data['rack']), array('product_id' => $data['product_id'], 'warehouse_id' => $data['warehouse_id']))) {
            return TRUE;
        }
        return FALSE;
    }

    public function getSoldQty($id)
    {
        $this->db->select("date_format(" . $this->db->dbprefix('sales') . ".date, '%Y-%M') month, SUM( " . $this->db->dbprefix('sale_items') . ".quantity ) as sold, SUM( " . $this->db->dbprefix('sale_items') . ".subtotal ) as amount")
            ->from('sales')
            ->join('sale_items', 'sales.id=sale_items.sale_id', 'left')
            ->group_by("date_format(" . $this->db->dbprefix('sales') . ".date, '%Y-%m')")
            ->where($this->db->dbprefix('sale_items') . '.product_id', $id)
            //->where('DATE(NOW()) - INTERVAL 1 MONTH')
            ->where('DATE_ADD(curdate(), INTERVAL 1 MONTH)')
            ->order_by("date_format(" . $this->db->dbprefix('sales') . ".date, '%Y-%m') desc")->limit(3);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPurchasedQty($id)
    {
        $this->db->select("date_format(" . $this->db->dbprefix('purchases') . ".date, '%Y-%M') month, SUM( " . $this->db->dbprefix('purchase_items') . ".quantity ) as purchased, SUM( " . $this->db->dbprefix('purchase_items') . ".subtotal ) as amount")
            ->from('purchases')
            ->join('purchase_items', 'purchases.id=purchase_items.purchase_id', 'left')
            ->group_by("date_format(" . $this->db->dbprefix('purchases') . ".date, '%Y-%m')")
            ->where($this->db->dbprefix('purchase_items') . '.product_id', $id)
            //->where('DATE(NOW()) - INTERVAL 1 MONTH')
            ->where('DATE_ADD(curdate(), INTERVAL 1 MONTH)')
            ->order_by("date_format(" . $this->db->dbprefix('purchases') . ".date, '%Y-%m') desc")->limit(3);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllVariants()
    {
        $q = $this->db->get('variants');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    // Add BY Chitra
    public function getLab()
    {
        $q = $this->db->get('laboratory');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

public function getLabByID($id)
    {
        $q = $this->db->get_where('laboratory', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

public function addLaboratory($data){
       if($this->db->insert('laboratory', $data)){
            return true;
        }
        return false;
    }
    
    public function editLab($data,$id){
        if($this->db->update('laboratory', $data, array('id' => $id)))
        {
            return true;
        }
        return false;
    }
    
    public function getLabProduct($pr_wh_id){
        $q = $this->db->get_where('laboratory', array('warehouse_id'=>$pr_wh_id));
       $tr[""] = "";
       foreach ($q->result() as $lab) {
           $tr[$lab->id] = ucwords($lab->lab_name).' - '.ucwords($lab->lab_person);
        }
        return form_dropdown('ptax', $tr, "", 'id="ptax" class="form-control pos-input-tip" style="width:100%;"');

        //return FALSE;
    }
    
    /*
     * added by ajay 
     * on 20-09-2016
     * To get all warehouses of a biller by id
     */
    
    public function getWarehousesByBillerId($biller){
       $q = $this->db->select('id,name')->get_where('warehouses', array('biller_id'=>$biller));
       if($q->num_rows() > 0){
           return $q->result();
       }
       return FALSE;
    }
}

